/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/Builder/mlir_fbs_builder.h"

#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/Dialect/Tosa/IR/TosaOps.h"
#include "mlir/IR/Verifier.h"
#include "mlir/Parser/Parser.h"
#include "mlir/Support/FileUtilities.h"

#include "llvm/Support/CommandLine.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/ToolOutputFile.h"

#include "vosa/Dialect/Vosa/IR/VosaDialect.h"
#include "vosa/Dialect/Vosa/IR/VosaOps.h"

#include <cstdint>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

using namespace mlir;

namespace cl = llvm::cl;

static cl::opt<std::string> inputFilename(cl::Positional,
                                          cl::desc("Input filename"),
                                          cl::value_desc("filename"),
                                          cl::init("-"));

static cl::opt<std::string> customOpsFilename("custom_ops",
                                              cl::desc("Custom ops filename"),
                                              cl::value_desc("filename"),
                                              cl::Optional);

static cl::opt<std::string> outputFilename("o", cl::desc("Output filename"),
                                           cl::value_desc("filename"),
                                           cl::init("-"));

static cl::opt<std::string> pipelineFuncName("f",
                                             cl::desc("Pipline function name"),
                                             cl::value_desc("name"),
                                             cl::init("pipeline"));

static cl::list<std::string> pipelineInputs("input",
                                            cl::desc("Pipline input nodes"),
                                            cl::value_desc("node name"),
                                            cl::CommaSeparated, cl::ZeroOrMore);

static cl::list<std::string> pipelineOutputs("output",
                                             cl::desc("Pipline output nodes"),
                                             cl::value_desc("node name"),
                                             cl::CommaSeparated,
                                             cl::ZeroOrMore);

static cl::opt<bool> verifyModule("verify", cl::desc("Verify generated module"),
                                  cl::init(true));

namespace {
enum Action { None, DumpMLIR, DumpLLVMIR, RunJIT };
}
static cl::opt<enum Action> emitAction(
    "emit", cl::desc("Select the kind of output desired"),
    cl::values(clEnumValN(DumpMLIR, "mlir", "output the MLIR dump")),
    cl::values(clEnumValN(DumpLLVMIR, "llvm", "output the LLVM IR dump")),
    cl::values(
        clEnumValN(RunJIT, "jit",
                   "JIT the code and run it by invoking the main function")));

static cl::opt<bool> enableOpt("opt", cl::desc("Enable optimizations"));

int loadCustomOpsMLIR(mlir::MLIRContext &context,
                      mlir::OwningOpRef<mlir::ModuleOp> &module,
                      llvm::StringRef filename) {
  module = mlir::parseSourceFile<mlir::ModuleOp>(filename, &context);
  if (!module) {
    llvm::errs() << "Error can't load custom ops file " << filename << "\n";
    return 3;
  }
  return 0;
}

int generateMLIR(const uint8_t *flatBufferData) {
  std::string errorMessage;
  auto output = mlir::openOutputFile(outputFilename, &errorMessage);
  if (!output) {
    llvm::errs() << errorMessage << "\n";
    return 1;
  }

  mlir::MLIRContext context;
  context.getOrLoadDialect<mlir::vosa::VosaDialect>();
  context.getOrLoadDialect<mlir::func::FuncDialect>();
  context.getOrLoadDialect<mlir::tensor::TensorDialect>();
  context.getOrLoadDialect<mlir::tosa::TosaDialect>();

  mlir::OwningOpRef<mlir::ModuleOp> customOpsModule;
  if (!customOpsFilename.empty()) {
    if (int error =
            loadCustomOpsMLIR(context, customOpsModule, customOpsFilename))
      return error;
  }

  mlir::OwningOpRef<mlir::ModuleOp> module;
  if (int error = ::vosa::buildMLIRPipeline(
          context, customOpsModule, module, inputFilename, flatBufferData,
          pipelineFuncName, pipelineInputs, pipelineOutputs))
    return error;

  if (verifyModule) {
    if (failed(mlir::verify(*module))) {
      module->emitError("module verification error");
      return 1;
    }
  }

  OpPrintingFlags printFlags;
  printFlags.enableDebugInfo();
  module->print(output->os(), printFlags);
  output->keep();

  return 0;
}

int main(int argc, char *argv[]) {
  cl::ParseCommandLineOptions(argc, argv, "vosa-fbs-builder\n");

  // load the input flatbuffer
  std::string errorMessage;
  auto inputFile = mlir::openInputFile(inputFilename, &errorMessage);
  if (!inputFile) {
    llvm::errs() << errorMessage << "\n";
    return 1;
  }
  const uint8_t *flatBufferData =
      reinterpret_cast<const uint8_t *>(inputFile->getBufferStart());

  if (emitAction == Action::DumpMLIR) {
    return generateMLIR(flatBufferData);
  }

  return 0;
}
