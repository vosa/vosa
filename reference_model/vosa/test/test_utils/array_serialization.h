/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_ARRAY_SERIALIZATION_H
#define VOSA_ARRAY_SERIALIZATION_H

#include "vosa/vosa.h"
#include "vosa_test_artifacts_generated.h"

namespace vosa::test {

size_t get_dtype_size(vosa::ArrayBase::DType dtype);

vosa_test_serialization::ArrayT serialize(const std::string &name,
                                          const vosa::ArrayBase &array);

vosa::ArrayBase deserialize(const vosa_test_serialization::ArrayT &);

std::string deserialize_name(const vosa_test_serialization::ArrayT &);

bool write_artifacts_flatbuffer(
    const vosa_test_serialization::TestArtifactsT &test_artifacts,
    const std::string &filename);

bool write_artifacts_json(
    const vosa_test_serialization::TestArtifactsT &test_artifacts,
    const std::string &filename);

void serialize_array(vosa_test_serialization::TestArtifactsT &test_artifacts,
                     const std::string &name, const vosa::ArrayBase &array,
                     bool input = true);

} // namespace vosa::test

#endif // VOSA_ARRAY_SERIALIZATION_H
