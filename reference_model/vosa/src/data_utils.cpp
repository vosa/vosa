/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "data_utils.h"

namespace vosa::utils {

template <typename T>
vosa::EigenScalar<T> DataUtils<T>::scalarFromCPP(T value) {
  vosa::EigenScalar<T> scalar{};
  scalar.setValues(value);
  return scalar;
}

template <typename T>
vosa::EigenVector<T> DataUtils<T>::vectorFromCPP(const std::vector<T> &input) {

  uint32_t vector_length = input.size();
  vosa::EigenVector<T> output{vector_length};

  for (uint32_t input_index = 0; input_index < vector_length; input_index++)
    output(input_index) = input[input_index];
  return output;
}

template <typename T>
vosa::EigenPlane<T>
DataUtils<T>::planeFromCPP(const std::vector<std::vector<T>> &value) {

  uint32_t dim1 = value.size();
  uint32_t dim2 = value[0].size();
  vosa::EigenPlane<T> output{dim1, dim2};
  for (uint32_t dim1_index = 0; dim1_index < dim1; dim1_index++) {
    for (uint32_t dim2_index = 0; dim2_index < dim2; dim2_index++) {
      output(dim1_index, dim2_index) = value[dim1_index][dim2_index];
    }
  }

  return output;
}

template <typename T>
vosa::EigenImage<T>
DataUtils<T>::imageFromCPP(T value, const std::vector<uint32_t> &spacial_dims,
                           const uint32_t &channels) {

  vosa::EigenImage<T> image{channels, spacial_dims[0], spacial_dims[1]};
  image.setConstant(value);
  return image;
}

template <typename T>
vosa::EigenPlane<T>
DataUtils<T>::transpose_plane(const vosa::EigenPlane<T> &input_plane) {
  uint32_t dim_0 = input_plane.dimension(0);
  uint32_t dim_1 = input_plane.dimension(1);
  vosa::EigenPlane<T> output(dim_1, dim_0);
  for (uint32_t dim_0_index = 0; dim_0_index < dim_0; dim_0_index++) {
    for (uint32_t dim_1_index = 0; dim_1_index < dim_1; dim_1_index++) {
      output(dim_1_index, dim_0_index) = input_plane(dim_0_index, dim_1_index);
    }
  }
  return output;
}

template class DataUtils<float>;

template class DataUtils<uint8_t>;

template class DataUtils<uint16_t>;

template class DataUtils<uint32_t>;

} // namespace vosa::utils
