add_library(vosa_sample_utils
        flatbuffer_utils.h
        ppm_utils.h
        ppm_utils.cpp)

target_include_directories(vosa_sample_utils
        PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR})
