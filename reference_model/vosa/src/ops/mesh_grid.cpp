/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "mesh_grid.h"

std::shared_ptr<vosa::Op> vosa::MeshGridBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_mesh_grid = vosa_operator_union.AsMESH_GRID();
  assert(serialized_mesh_grid);
  auto mode = serialized_mesh_grid->mode;
  auto dimensions = serialized_mesh_grid->attr->dimensions;
  switch (mode) {
  case vosa_serialization::MESH_GRID_MODE_I_UINT32: {
    return std::make_shared<vosa::MeshGrid<uint32_t>>(dimensions);
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::ArrayBase::DType vosa::MeshGrid<T>::output_dtype() const {
  return vosa::ArrayBase::UINT32;
}

template <>
vosa_serialization::MESH_GRID_MODE
vosa::MeshGrid<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::MESH_GRID_MODE_I_UINT32;
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::MeshGrid<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &) const {
  auto output_image = vosa::EigenImage<T>(dimensions[0], dimensions[1], 2);

  for (uint32_t height_index = 0; height_index < dimensions[0];
       height_index++) {
    for (uint32_t width_index = 0; width_index < dimensions[1]; width_index++) {
      output_image(height_index, width_index, 0) = height_index;
      output_image(height_index, width_index, 1) = width_index;
    }
  }
  return std::make_shared<vosa::ArrayBase>(output_image);
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::MeshGrid<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  auto mesh_grid_attribute =
      std::make_unique<vosa_serialization::MESH_GRID_ATTRIBUTET>();
  mesh_grid_attribute->dimensions = dimensions;

  vosa_serialization::MESH_GRIDT mesh_grid;
  mesh_grid.mode = get_serialization_mode();
  mesh_grid.attr = std::move(mesh_grid_attribute);

  auto mesh_grid_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  mesh_grid_operator->op.Set(std::move(mesh_grid));
  mesh_grid_operator->name = op_name;
  mesh_grid_operator->inputs = input_names;
  return mesh_grid_operator;
}

template class vosa::MeshGrid<uint32_t>;