/* -*- c++ -*-
 *
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_COMPILER_H
#define VOSA_COMPILER_H

#include <memory>

#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/OwningOpRef.h"
#include "llvm/Support/CommandLine.h"

namespace vosa::Compiler {

llvm::cl::OptionCategory &get_compiler_category();

bool compileMain(int argc, char *argv[]);

bool compile(mlir::MLIRContext &context,
             mlir::OwningOpRef<mlir::ModuleOp> &module,
             const std::string &pipeline_name,
             const std::string &output_filename);

} // namespace vosa::Compiler
#endif // VOSA_COMPILER_H
