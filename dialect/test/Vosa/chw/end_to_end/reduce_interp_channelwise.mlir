// run command without FileCheck to see output (or add "--dump-input always" FileCheck option)
// RUN: vosa-opt -vosa-data-format=chw -vosa-to-tosa %s | %test_src_root/e2e-runner | FileCheck %s

func.func @main() {
  %input0_si32 = vosa.constant dense<[
    [[1, 1, 1],
     [1, 1, 1],
     [1, 1, 1],
     [1, 1, 1]],
    [[2, 2, 2],
     [2, 2, 2],
     [2, 2, 2],
     [2, 2, 2]],
    [[3, 3, 3],
     [3, 3, 3],
     [3, 3, 3],
     [3, 3, 3]]]> : tensor<3x4x3xsi32>

  %input1_f32 = vosa.constant dense<[
    [[0.5, 1.5, 1.25],
     [0.0, 1.0, 2.0],
     [-0.5, -1.5, -2.0],
     [2.5, 3.5, 4.0]]]> : tensor<1x4x3xf32>

  //CHECK: sizes = [4, 3]
  //CHECK: [1.5, 2.5, 2.25]
  //CHECK: [1, 2, 3]
  //CHECK: [1, 1, 1]
  //CHECK: [3, 3, 3]
  %res_f32 = vosa.reduce_interp_channelwise %input0_si32, %input1_f32 : tensor<3x4x3xsi32>, tensor<1x4x3xf32> -> tensor<1x4x3xf32>
  %unranked_res_f32 = tensor.cast %res_f32 : tensor<1x4x3xf32> to tensor<?x?x?xf32>
  func.call @print_img_f32(%unranked_res_f32) : (tensor<?x?x?xf32>) -> ()
  return
}

func.func @print_img_f32(%a : tensor<?x?x?xf32>) {
  // %unranked_all = tensor.cast %a : tensor<?x?x?xf32> to tensor<*xf32>
  // func.call @printMemrefF32(%unranked_all) : (tensor<*xf32>) -> ()
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c1 : tensor<?x?x?xf32>
  %w = tensor.dim %a, %c2 : tensor<?x?x?xf32>
  %nch = tensor.dim %a, %c0 : tensor<?x?x?xf32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[%c, 0, 0][1, %h, %w][1, 1, 1]  : tensor<?x?x?xf32> to tensor<1x?x?xf32>
     %chdata_c = tensor.collapse_shape %chdata [[0, 1], [2]] : tensor<1x?x?xf32> into tensor<?x?xf32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xf32> to tensor<*xf32>
     func.call @printMemrefF32(%unranked_chdata) : (tensor<*xf32>) -> ()
  }
  return
}

func.func private @printMemrefF32(%ptr : tensor<*xf32> {bufferization.access = "read"})
