// run command without FileCheck to see output (or add "--dump-input always" FileCheck option)
// RUN: vosa-opt -vosa-data-format=chw -vosa-to-tosa %s | %test_src_root/e2e-runner | FileCheck %s

func.func @main() {
    
    //FP TEST
    %input_f32 = vosa.constant dense<[
      [[1., 1., 1.],
       [1., 1., 1.],
       [1., 1., 1.]],
      [[4., 4., 4.],
       [4., 4., 4.],
       [4., 4., 4.]],
      [[7., 7., 7.],
       [7., 7., 7.],
       [7., 7., 7.]]]> : tensor<3x3x3xf32>

    %dyn_input = tensor.cast %input_f32 : tensor<3x3x3xf32> to tensor<?x?x?xf32>
    func.call @print_img_f32(%dyn_input) : (tensor<?x?x?xf32>) -> ()

    // Cases
    //First channel below nodes[0]
    //Second channel middle path between 2 nodes
    //Third channel aboves nodes[0]

    //CHECK: sizes = [3, 3]
    //CHECK-COUNT-3: [20, 20, 20] 
    //CHECK: sizes = [3, 3]
    //CHECK-COUNT-3: [35, 35, 35] 
    //CHECK: sizes = [3, 3]
    //CHECK-COUNT-3: [50, 50, 50] 
    %interp0_f32 = vosa.piecewise_linear %input_f32 {nodes=dense<[2.0, 3.0, 5.0, 6.0]> : tensor<4xf32>, values=dense<[20.0, 30.0, 40.0, 50.0]> : tensor<4xf32>} : tensor<3x3x3xf32> -> tensor<3x3x3xf32>

    %dyn_interp0_f32 = tensor.cast %interp0_f32 : tensor<3x3x3xf32> to tensor<?x?x?xf32>
    func.call @print_img_f32(%dyn_interp0_f32) : (tensor<?x?x?xf32>) -> ()

    // Cases
    //First channel equal to nodes[0]
    //Second channel equal to nodes[n]
    //Third channel equal to nodes[N]
    //CHECK: sizes = [3, 3]
    //CHECK-COUNT-3: [20, 20, 20]
    //CHECK: sizes = [3, 3]
    //CHECK-COUNT-3: [30, 30, 30]
    //CHECK: sizes = [3, 3]
    //CHECK-COUNT-3: [50, 50, 50]
    %interp1_f32 = vosa.piecewise_linear %input_f32 {nodes=dense<[1.0, 4.0, 6.0, 7.0]> : tensor<4xf32>, values=dense<[20.0, 30.0, 40.0, 50.0]> : tensor<4xf32>} : tensor<3x3x3xf32> -> tensor<3x3x3xf32>

    %dyn_interp1_f32 = tensor.cast %interp1_f32 : tensor<3x3x3xf32> to tensor<?x?x?xf32>
    func.call @print_img_f32(%dyn_interp1_f32) : (tensor<?x?x?xf32>) -> ()


    //INT TEST

    %input_si32 = vosa.constant dense<
      [[[ 1,  1, 1],
        [ 1,  1, 1],
        [ 1,  1, 1]],
       [[ 5,  5, 5],
        [ 5,  5, 5],
        [ 5,  5, 5]],
       [[ 10,  10, 10],
        [ 10,  10, 10],
        [ 10,  10, 10]]]> : tensor<3x3x3xsi32>
    %input_i32 = builtin.unrealized_conversion_cast %input_si32 : tensor<3x3x3xsi32> to tensor<3x3x3xi32>
    %dyn_input_i32 = tensor.cast %input_i32 : tensor<3x3x3xi32> to tensor<?x?x?xi32>
    func.call @print_img_i32(%dyn_input_i32) : (tensor<?x?x?xi32>) -> ()

    
    // Cases
    //First channel below nodes[0]
    //Second channel middle path between 2 nodes
    //Third channel aboves nodes[0]
    //CHECK: sizes = [3, 3]
    //CHECK-COUNT-3: [20, 20, 20]
    //CHECK: sizes = [3, 3]
    //CHECK-COUNT-3: [35, 35, 35]
    //CHECK: sizes = [3, 3]
    //CHECK-COUNT-3: [50, 50, 50]
    %interp0_si32 = vosa.piecewise_linear %input_si32 {nodes=dense<[2, 3, 7, 8]> : tensor<4xsi32>, values=dense<[20, 30, 40, 50]> : tensor<4xsi32>} : tensor<3x3x3xsi32> -> tensor<3x3x3xsi32>

    %interp0_i32 = builtin.unrealized_conversion_cast %interp0_si32 : tensor<3x3x3xsi32> to tensor<3x3x3xi32>
    %dyn_interp0_si32 = tensor.cast %interp0_i32 : tensor<3x3x3xi32> to tensor<?x?x?xi32>
    func.call @print_img_i32(%dyn_interp0_si32) : (tensor<?x?x?xi32>) -> ()


    // Cases
    //First channel equal to nodes[0]
    //Second channel equal to nodes[n]
    //Third channel equal to nodes[N]
    //CHECK: sizes = [3, 3]
    //CHECK-COUNT-3: [20, 20, 20]
    //CHECK: sizes = [3, 3]
    //CHECK-COUNT-3: [40, 40, 40]
    //CHECK: sizes = [3, 3]
    //CHECK-COUNT-3: [50, 50, 50]
    %interp1_si32 = vosa.piecewise_linear %input_si32 {nodes=dense<[1, 2, 5, 10]> : tensor<4xsi32>, values=dense<[20, 30, 40, 50]> : tensor<4xsi32>} : tensor<3x3x3xsi32> -> tensor<3x3x3xsi32>
    %interp1_i32 = builtin.unrealized_conversion_cast %interp1_si32 : tensor<3x3x3xsi32> to tensor<3x3x3xi32>
    %dyn_interp1_si32 = tensor.cast %interp1_i32 : tensor<3x3x3xi32> to tensor<?x?x?xi32>
    func.call @print_img_i32(%dyn_interp1_si32) : (tensor<?x?x?xi32>) -> ()

    // Real usage from VLPP pipeline
    %vlpp_data = vosa.constant dense<[[[1627, 1035, 1517, 2246, 2145, 2123, 2123, 2109, 2115, 2103]]]> : tensor<1x1x10xsi32>
    %vlpp_interp = vosa.piecewise_linear %vlpp_data {values = dense<"0x000000000000000000000000000000000000000001000000010000000100000001000000020000000200000003000000030000000400000004000000050000000600000006000000060000000700000007000000080000000800000009000000090000000A0000000A0000000B0000000B0000000C0000000C0000000D0000000E0000000E0000000F000000100000001000000011000000120000001300000013000000140000001500000016000000170000001800000018000000190000001A0000001E00000023000000280000002D000000330000003A00000040000000480000005000000059000000620000006C00000077000000820000008F0000009C000000B9000000DA000000FF00000028010000570100008B010000C6010000060200004E0200009E020000F602000058030000C303000039040000BA040000480500008E06000014080000E3090000090C0000920E00008F110000111500002F190000FF1D00009D230000292A0000C7310000A13A0000E5440000CC500000935E0000836E0000EF80000036960000C6AE00001BCB0000C8EB000072110100DB3C0100DF6E01007DA80100DDEA0100533702006A8F0200E7F40200DC690300ACF003001D8C0400683F05004B0E060025FD06000A110800EB4F0900B6C00A008B6B0C00EC590E00FFFF0F00FFFF0F00FFFF0F00FFFF0F00FFFF0F00FFFF0F00FFFF0F00"> : tensor<129xsi32>, nodes = dense<"0x000000000800000010000000180000002000000028000000300000003800000040000000480000005000000058000000600000006800000070000000780000008000000084000000880000008C0000009000000094000000980000009C000000A0000000A4000000A8000000AC000000B0000000B4000000B8000000BC000000C0000000C4000000C8000000CC000000D0000000D4000000D8000000DC000000E0000000E4000000E8000000EC000000F0000000F4000000F8000000FC00000000010000100100002001000030010000400100005001000060010000700100008001000090010000A0010000B0010000C0010000D0010000E0010000F00100000002000020020000400200006002000080020000A0020000C0020000E00200000003000020030000400300006003000080030000A0030000C0030000E0030000000400004004000080040000C0040000000500004005000080050000C0050000000600004006000080060000C0060000000700004007000080070000C0070000000800004008000080080000C0080000000900004009000080090000C0090000000A0000400A0000800A0000C00A0000000B0000400B0000800B0000C00B0000000C0000400C0000800C0000C00C0000000D0000400D0000800D0000C00D0000000E0000400E0000800E0000C00E0000000F0000400F0000800F0000C00F000000100000"> : tensor<129xsi32>} : tensor<1x1x10xsi32> -> tensor<1x1x10xsi32>
    %vlpp_interp_i32 = builtin.unrealized_conversion_cast %vlpp_interp : tensor<1x1x10xsi32> to tensor<1x1x10xi32>
    %dyn_vlpp_interp_si32 = tensor.cast %vlpp_interp_i32 : tensor<1x1x10xi32> to tensor<?x?x?xi32>
    //CHECK: [8286, 1408, 6134, 39044, 30723, 29102, 29102, 28100, 28512, 27717]
    func.call @print_img_i32(%dyn_vlpp_interp_si32) : (tensor<?x?x?xi32>) -> ()

    return
}


func.func @print_img_i32(%a : tensor<?x?x?xi32>) {
  // %unranked_all = tensor.cast %a : tensor<?x?x?xi32> to tensor<*xi32>
  // func.call @printMemrefI32(%unranked_all) : (tensor<*xi32>) -> ()
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c1 : tensor<?x?x?xi32>
  %w = tensor.dim %a, %c2 : tensor<?x?x?xi32>
  %nch = tensor.dim %a, %c0 : tensor<?x?x?xi32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[%c, 0, 0][1, %h, %w][1, 1, 1]  : tensor<?x?x?xi32> to tensor<1x?x?xi32>
     %chdata_c = tensor.collapse_shape %chdata [[0, 1], [2]] : tensor<1x?x?xi32> into tensor<?x?xi32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xi32> to tensor<*xi32>
     func.call @printMemrefI32(%unranked_chdata) : (tensor<*xi32>) -> ()
  }
  return
}

func.func @print_img_f32(%a : tensor<?x?x?xf32>) {
  // %unranked_all = tensor.cast %a : tensor<?x?x?xf32> to tensor<*xf32>
  // func.call @printMemrefF32(%unranked_all) : (tensor<*xf32>) -> ()
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c1 : tensor<?x?x?xf32>
  %w = tensor.dim %a, %c2 : tensor<?x?x?xf32>
  %nch = tensor.dim %a, %c0 : tensor<?x?x?xf32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[%c, 0, 0][1, %h, %w][1, 1, 1]  : tensor<?x?x?xf32> to tensor<1x?x?xf32>
     %chdata_c = tensor.collapse_shape %chdata [[0, 1], [2]] : tensor<1x?x?xf32> into tensor<?x?xf32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xf32> to tensor<*xf32>
     func.call @printMemrefF32(%unranked_chdata) : (tensor<*xf32>) -> ()
  }
  return
}

func.func private @printMemrefI32(%ptr : tensor<*xi32> {bufferization.access = "read"})
func.func private @printMemrefF32(%ptr : tensor<*xf32> {bufferization.access = "read"})
