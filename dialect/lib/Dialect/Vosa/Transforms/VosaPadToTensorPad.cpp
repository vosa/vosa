/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/Dialect/Vosa/IR/VosaDialect.h"
#include "vosa/Dialect/Vosa/IR/VosaOps.h"
#include "vosa/Dialect/Vosa/Transforms/Passes.h"
#include "vosa/Dialect/Vosa/Utils/Conversion.h"
#include "vosa/Dialect/Vosa/Utils/DataFormat.h"

#include "mlir/Dialect/Arith/IR/Arith.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/SCF/IR/SCF.h"
#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/PatternMatch.h"
#include "mlir/Transforms/DialectConversion.h"
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"

#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#define DEBUG_TYPE "vosa-pad-to-tensor-pad"

#include "PassDetail.h"

using namespace mlir;
using namespace mlir::vosa;
using namespace mlir::arith;

namespace {

struct PadOpConverter : public OpConversionPattern<vosa::PadOp> {
  using OpConversionPattern<vosa::PadOp>::OpConversionPattern;

  PadOpConverter(TypeConverter &typeConverter, MLIRContext *context,
                 bool constZero)
      : OpConversionPattern(typeConverter, context), constZero(constZero) {}

  LogicalResult
  matchAndRewrite(vosa::PadOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto loc = op.getLoc();
    auto input = adaptor.getInput();
    auto inputTy = adaptor.getInput().getType().cast<ShapedType>();
    auto outputTy =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    auto elTy = outputTy.getElementType();
    auto pad_mode = op.getMode();
    auto height = getHeight(inputTy);
    auto width = getWidth(inputTy);

    // verifier has ensured this is 4 integer elements:
    auto getPadElem = [&](int i) {
      return op.getPadSize()
          .getValue()[i]
          .cast<IntegerAttr>()
          .getValue()
          .getSExtValue();
    };
    int64_t yBefore = getPadElem(0);
    int64_t yAfter = getPadElem(1);
    int64_t xBefore = getPadElem(2);
    int64_t xAfter = getPadElem(3);

    // early return if nothing to pad
    if (yBefore == 0 && yAfter == 0 && xBefore == 0 && xAfter == 0) {
      rewriter.replaceOp(op, adaptor.getInput());
      return success();
    }

    // convert optional padding constant attr to constant value
    Value padValue;
    if (op.getPadConstant()) {
      Type constTy;
      auto v = cast<TypedAttr>(*op.getPadConstant());
      if (auto intAttr = v.dyn_cast<IntegerAttr>()) {
        constTy = getTypeConverter()->convertType(intAttr.getType());
        v = rewriter.getIntegerAttr(constTy, intAttr.getValue());
      } else if (auto floatAttr = v.dyn_cast<FloatAttr>())
        constTy = getTypeConverter()->convertType(floatAttr.getType());
      else
        op.emitError("Unsupported pad constant type");
      padValue =
          rewriter.create<arith::ConstantOp>(loc, constTy, v).getResult();
    };

    TypedAttr zeroAttr;
    if (elTy.isa<IntegerType>()) {
      zeroAttr =
          rewriter.getIntegerAttr(elTy, APInt(elTy.getIntOrFloatBitWidth(), 0));
    } else if (elTy.isa<FloatType>()) {
      zeroAttr = rewriter.getFloatAttr(
          elTy,
          APFloat::getZero(elTy.cast<FloatType>().getFloatSemantics(), false));
    }
    Value zeroVal =
        rewriter.create<arith::ConstantOp>(loc, zeroAttr).getResult();

    if (constZero) {
      pad_mode = PadMode::constant;
      padValue = zeroVal;
    }

    if (pad_mode != PadMode::constant && pad_mode != PadMode::replicate &&
        pad_mode != PadMode::reflect) {
      return op.emitError("unhandled padding mode");
    }

    SmallVector<OpFoldResult> lowIndices{rewriter.getIndexAttr(yBefore),
                                         rewriter.getIndexAttr(xBefore)};
    lowIndices.insert(lowIndices.begin() + getChannelDimension(),
                      rewriter.getIndexAttr(0));
    SmallVector<OpFoldResult> highIndices{rewriter.getIndexAttr(yAfter),
                                          rewriter.getIndexAttr(xAfter)};
    highIndices.insert(highIndices.begin() + getChannelDimension(),
                       rewriter.getIndexAttr(0));

    auto padOp = rewriter.create<tensor::PadOp>(
        loc, outputTy, adaptor.getInput(), lowIndices, highIndices);

    OpBuilder::InsertionGuard guard(rewriter);
    SmallVector<Type> blockArgTypes(3, rewriter.getIndexType());
    SmallVector<Location> blockArgLocs(3, loc);
    Block *padBody =
        rewriter.createBlock(&padOp.getRegion(), padOp.getRegion().end(),
                             blockArgTypes, blockArgLocs);

    if (pad_mode == PadMode::constant) {
      rewriter.create<tensor::YieldOp>(loc, padValue);
    } else {
      auto y = padBody->getArgument(getHeightDimension());
      auto x = padBody->getArgument(getWidthDimension());
      auto c = padBody->getArgument(getChannelDimension());

      // TODO: handle dynamic size
      auto cstZero =
          rewriter.create<arith::ConstantOp>(loc, rewriter.getIndexAttr(0))
              .getResult();
      auto cstOne =
          rewriter.create<arith::ConstantOp>(loc, rewriter.getIndexAttr(1))
              .getResult();

      auto cstTrue =
          rewriter.create<arith::ConstantOp>(loc, rewriter.getBoolAttr(true))
              .getResult();
      auto cstFalse =
          rewriter.create<arith::ConstantOp>(loc, rewriter.getBoolAttr(false))
              .getResult();

      auto cstYBefore =
          rewriter
              .create<arith::ConstantOp>(loc, rewriter.getIndexAttr(yBefore))
              .getResult();
      auto cstYAfter =
          rewriter.create<arith::ConstantOp>(loc, rewriter.getIndexAttr(yAfter))
              .getResult();
      auto cstYMax =
          rewriter
              .create<arith::ConstantOp>(loc, rewriter.getIndexAttr(height - 1))
              .getResult();
      auto cstYEnd = rewriter
                         .create<arith::ConstantOp>(
                             loc, rewriter.getIndexAttr(yBefore + height))
                         .getResult();

      auto cstXBefore =
          rewriter
              .create<arith::ConstantOp>(loc, rewriter.getIndexAttr(xBefore))
              .getResult();
      auto cstXAfter =
          rewriter.create<arith::ConstantOp>(loc, rewriter.getIndexAttr(xAfter))
              .getResult();
      auto cstXMax =
          rewriter
              .create<arith::ConstantOp>(loc, rewriter.getIndexAttr(width - 1))
              .getResult();
      auto cstXEnd = rewriter
                         .create<arith::ConstantOp>(
                             loc, rewriter.getIndexAttr(xBefore + width))
                         .getResult();

      auto isAbove = (yBefore > 0)
                         ? rewriter.create<arith::CmpIOp>(
                               loc, arith::CmpIPredicate::slt, y, cstYBefore)
                         : cstFalse;
      auto isBelow = (yAfter > 0)
                         ? rewriter.create<arith::CmpIOp>(
                               loc, arith::CmpIPredicate::sge, y, cstYEnd)
                         : cstFalse;
      auto isLeft = (xBefore > 0)
                        ? rewriter.create<arith::CmpIOp>(
                              loc, arith::CmpIPredicate::slt, x, cstXBefore)
                        : cstFalse;
      auto isRight = (xAfter > 0)
                         ? rewriter.create<arith::CmpIOp>(
                               loc, arith::CmpIPredicate::sge, x, cstXEnd)
                         : cstFalse;

      auto extractFromInput = [&](OpBuilder &b, Location l, Value y, Value x) {
        SmallVector<Value, 3> extractIndices{y, x};
        extractIndices.insert(extractIndices.begin() + getChannelDimension(),
                              c);
        return b.create<tensor::ExtractOp>(l, input, extractIndices)
            .getResult();
      };

      auto padReplLower = [&](Value idx, Value padSz, Value inputMax) {
        return cstZero;
      };
      auto padReplUpper = [&](Value idx, Value padSz, Value inputMax) {
        return inputMax;
      };
      auto padReflectLower = [&](Value idx, Value padSz, Value inputMax) {
        return rewriter.create<arith::SubIOp>(loc, padSz, idx).getResult();
      };
      auto padReflectUpper = [&](Value idx, Value padSz, Value inputMax) {
        auto maxMinusOne =
            rewriter.create<arith::SubIOp>(loc, inputMax, cstOne);
        return rewriter.create<arith::SubIOp>(loc, maxMinusOne, idx)
            .getResult();
      };

      function_ref<Value(Value, Value, Value)> lowerFn = padReplLower;
      function_ref<Value(Value, Value, Value)> upperFn = padReplUpper;
      if (pad_mode == PadMode::reflect) {
        lowerFn = padReflectLower;
        upperFn = padReflectUpper;
      }

      // For Y and X dimensions determine this point is in the padding region
      // and if so, the index

      auto yPadIndex = rewriter.create<mlir::scf::IfOp>(
          loc, isAbove,
          [&](OpBuilder &b, Location l) {
            // pad before - compute extract location
            Value yIndex = lowerFn(y, cstYBefore, cstYMax);
            b.create<scf::YieldOp>(l, ValueRange{cstTrue, yIndex});
          },
          [&](OpBuilder &b, Location l) {
            auto ifIsBelow = b.create<mlir::scf::IfOp>(
                l, isBelow,
                [&](OpBuilder &b, Location l) {
                  Value padY = b.create<arith::SubIOp>(l, y, cstYEnd);
                  Value yIndex = upperFn(padY, cstYAfter, cstYMax);
                  b.create<scf::YieldOp>(l, ValueRange{cstTrue, yIndex});
                },
                [&](OpBuilder &b, Location l) {
                  // central band
                  Value yIndex = b.create<arith::SubIOp>(l, y, cstYBefore);
                  b.create<scf::YieldOp>(l, ValueRange{cstFalse, yIndex});
                });
            b.create<scf::YieldOp>(l, ifIsBelow.getResults());
          });

      auto xPadIndex = rewriter.create<mlir::scf::IfOp>(
          loc, isLeft,
          [&](OpBuilder &b, Location l) {
            // pad before - compute extract location
            Value xIndex = lowerFn(x, cstXBefore, cstXMax);
            b.create<scf::YieldOp>(l, ValueRange{cstTrue, xIndex});
          },
          [&](OpBuilder &b, Location l) {
            auto ifIsRight = b.create<mlir::scf::IfOp>(
                l, isRight,
                [&](OpBuilder &b, Location l) {
                  Value padX = b.create<arith::SubIOp>(l, x, cstXEnd);
                  Value xIndex = upperFn(padX, cstXAfter, cstXMax);
                  b.create<scf::YieldOp>(l, ValueRange{cstTrue, xIndex});
                },
                [&](OpBuilder &b, Location l) {
                  // central band
                  Value xIndex = b.create<arith::SubIOp>(l, x, cstXBefore);
                  b.create<scf::YieldOp>(l, ValueRange{cstFalse, xIndex});
                });
            b.create<scf::YieldOp>(l, ifIsRight.getResults());
          });

      // extract the value if in padding area
      auto isYPad = yPadIndex.getResults()[0];
      auto isXPad = xPadIndex.getResults()[0];
      auto isPad = rewriter.create<arith::OrIOp>(loc, isYPad, isXPad);
      auto ifIsPad = rewriter.create<mlir::scf::IfOp>(
          loc, isPad,
          [&](OpBuilder &b, Location l) {
            auto yIndex = yPadIndex.getResults()[1];
            auto xIndex = xPadIndex.getResults()[1];
            Value v = extractFromInput(b, l, yIndex, xIndex);
            b.create<scf::YieldOp>(l, v);
          },
          [&](OpBuilder &b, Location l) {
            // source will be inserted over this region
            b.create<scf::YieldOp>(l, zeroVal);
          });

      rewriter.create<tensor::YieldOp>(loc, ifIsPad.getResults()[0]);
    }

    rewriter.replaceOp(op, padOp.getResult());
    return success();
  }

  bool constZero;
};

class VosaPadToTensorPadPass
    : public mlir::vosa::VosaPadToTensorPadBase<VosaPadToTensorPadPass> {
public:
  explicit VosaPadToTensorPadPass() = default;

  explicit VosaPadToTensorPadPass(bool constZero) {
    this->constZero = constZero;
  }

  void runOnOperation() override {
    auto func = getOperation();
    VosaTypeConverter typeConverter;

    RewritePatternSet patterns(&getContext());
    patterns.add<PadOpConverter>(typeConverter, patterns.getContext(),
                                 constZero);

    ConversionTarget target(getContext());
    target
        .addLegalDialect<vosa::VosaDialect, mlir::arith::ArithDialect,
                         mlir::scf::SCFDialect, mlir::tensor::TensorDialect>();
    target.addIllegalOp<PadOp>();

    if (failed(applyPartialConversion(func, target, std::move(patterns))))
      signalPassFailure();
  }
};
} // namespace

std::unique_ptr<mlir::Pass>
mlir::vosa::createVosaPadToTensorPadPass(bool constZero) {
  LLVM_DEBUG(llvm::dbgs() << "createVosaPadToTensorPadPass: " << constZero
                          << "\n";);
  return std::make_unique<VosaPadToTensorPadPass>(constZero);
}
