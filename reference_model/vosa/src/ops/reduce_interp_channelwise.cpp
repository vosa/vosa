/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "reduce_interp_channelwise.h"

std::shared_ptr<vosa::Op> vosa::ReduceInterpChannelwiseBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_reduce_interp_channelwise =
      vosa_operator_union.AsREDUCE_INTERP_CHANNELWISE();
  assert(serialized_reduce_interp_channelwise);
  auto mode = serialized_reduce_interp_channelwise->mode;
  switch (mode) {
  case vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE_I_I_UINT8_FLOAT32: {
    return std::make_shared<vosa::ReduceInterpChannelwise<uint8_t>>();
  }
  case vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE_I_I_UINT16_FLOAT32: {
    return std::make_shared<vosa::ReduceInterpChannelwise<uint16_t>>();
  }
  case vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE_I_I_UINT32_FLOAT32: {
    return std::make_shared<vosa::ReduceInterpChannelwise<uint32_t>>();
  }
  case vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE_I_I_INT8_FLOAT32: {
    return std::make_shared<vosa::ReduceInterpChannelwise<int8_t>>();
  }
  case vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE_I_I_INT16_FLOAT32: {
    return std::make_shared<vosa::ReduceInterpChannelwise<int16_t>>();
  }
  case vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE_I_I_INT32_FLOAT32: {
    return std::make_shared<vosa::ReduceInterpChannelwise<int32_t>>();
  }
  case vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::ReduceInterpChannelwise<float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::EigenImage<float> vosa::ReduceInterpChannelwise<T>::runme(
    const EigenImage<T> &input_0, const EigenImage<float> &input_1) const {

  uint32_t input_1_channels = input_1.dimension(2);
  assert(input_1_channels == 1);

  uint32_t height = input_0.dimension(0);
  uint32_t width = input_0.dimension(1);
  uint32_t channels = input_0.dimension(2);

  vosa::EigenImage<float> output{height, width, 1};
  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {

      float interp_value = input_1(height_index, width_index, 0);
      auto cb = (int)std::floor(interp_value);

      // Clamp to valid bounds
      uint32_t cb_safe = std::max(0, std::min(cb, (int)(channels)-1));
      uint32_t cb_p1_safe = std::max(0, std::min(cb + 1, (int)(channels)-1));

      // Linear Interpolations (std::lerp)
      float w = interp_value - (float)cb;
      output(height_index, width_index, 0) =
          input_0(height_index, width_index, cb_safe) * (1.0f - w) +
          w * input_0(height_index, width_index, cb_p1_safe);
    }
  }
  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::ReduceInterpChannelwise<T>::output_dtype() const {
  return output_dtype_helper<float>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::ReduceInterpChannelwise<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input_0 = inputs[0]->template get_image<T>();
  const EigenImage<float> &input_1 = inputs[1]->template get_image<float>();
  auto typed_output = this->runme(input_0, input_1);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE
vosa::ReduceInterpChannelwise<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE_I_I_UINT8_FLOAT32;
}

template <>
vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE
vosa::ReduceInterpChannelwise<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE_I_I_UINT16_FLOAT32;
}

template <>
vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE
vosa::ReduceInterpChannelwise<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE_I_I_UINT32_FLOAT32;
}

template <>
vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE
vosa::ReduceInterpChannelwise<int8_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE_I_I_INT8_FLOAT32;
}

template <>
vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE
vosa::ReduceInterpChannelwise<int16_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE_I_I_INT16_FLOAT32;
}

template <>
vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE
vosa::ReduceInterpChannelwise<int32_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE_I_I_INT32_FLOAT32;
}

template <>
vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE
vosa::ReduceInterpChannelwise<float>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_INTERP_CHANNELWISE_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::ReduceInterpChannelwise<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  vosa_serialization::REDUCE_INTERP_CHANNELWISET reduce_interp_channelwise;
  reduce_interp_channelwise.mode = get_serialization_mode();

  auto reduce_interp_channelwise_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  reduce_interp_channelwise_operator->op.Set(reduce_interp_channelwise);
  reduce_interp_channelwise_operator->name = op_name;
  reduce_interp_channelwise_operator->inputs = input_names;
  return reduce_interp_channelwise_operator;
}

template class vosa::ReduceInterpChannelwise<uint8_t>;

template class vosa::ReduceInterpChannelwise<uint16_t>;

template class vosa::ReduceInterpChannelwise<uint32_t>;

template class vosa::ReduceInterpChannelwise<int8_t>;

template class vosa::ReduceInterpChannelwise<int16_t>;

template class vosa::ReduceInterpChannelwise<int32_t>;

template class vosa::ReduceInterpChannelwise<float>;