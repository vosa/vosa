
/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "bokeh.h"

namespace vosa::samples::bokeh {

std::shared_ptr<vosa::api::Op>
vosaDepthMapAverage(vosa::api::GraphBuilder &graph_builder,
                    const std::shared_ptr<vosa::api::Op> &depth_map_sum,
                    const std::shared_ptr<vosa::api::Op> &binary_mask_sum) {
  auto depth_map_sum_float =
      vosa::api::cast<float>(graph_builder, depth_map_sum);
  auto binary_mask_sum_float =
      vosa::api::cast<float>(graph_builder, binary_mask_sum);
  return vosa::api::div(graph_builder, depth_map_sum_float,
                        binary_mask_sum_float);
}

std::shared_ptr<vosa::api::Op>
vosaScaleFActor(vosa::api::GraphBuilder &graph_builder,
                const std::shared_ptr<vosa::api::Op> &depth_map_max) {
  auto two_five_five =
      vosa::api::const_image<float>(graph_builder, {1, 1, 1}, 255);
  return vosa::api::div(graph_builder, two_five_five, depth_map_max);
}

std::shared_ptr<vosa::api::Op>
vosaGraphDepthNormalization(vosa::api::GraphBuilder &graph_builder,
                            const std::shared_ptr<vosa::api::Op> &depth_map,
                            const std::shared_ptr<vosa::api::Op> &binary_mask) {

  auto depth_map_int32 = vosa::api::cast<int32_t>(graph_builder, depth_map);
  auto shifted_binary_mask =
      vosa::api::logical_shift_right(graph_builder, binary_mask, 7);
  auto binary_mask_int32 =
      vosa::api::cast<int32_t>(graph_builder, shifted_binary_mask);

  auto binary_mask_sum =
      vosa::api::reduce_sum_planewise(graph_builder, binary_mask_int32);
  auto depth_map_mult =
      vosa::api::mult(graph_builder, depth_map_int32, binary_mask_int32);

  auto depth_map_sum =
      vosa::api::reduce_sum_planewise(graph_builder, depth_map_mult);

  auto depth_map_average =
      vosaDepthMapAverage(graph_builder, depth_map_sum, binary_mask_sum);

  std::vector<uint32_t> image_dim = {540, 960};
  auto depth_map_average_as_image = vosa::api::broadcast_single_pixel(
      graph_builder, depth_map_average, image_dim);

  auto depth_map_as_float =
      vosa::api::cast<float>(graph_builder, depth_map_int32);
  auto depth_diff = vosa::api::sub(graph_builder, depth_map_average_as_image,
                                   depth_map_as_float);
  auto depth_map_shifted = vosa::api::abs(graph_builder, depth_diff);

  auto depth_map_max =
      vosa::api::reduce_max_planewise(graph_builder, depth_map_shifted);

  auto scale_factor = vosaScaleFActor(graph_builder, depth_map_max);

  auto scale_factor_as_image =
      vosa::api::broadcast_single_pixel(graph_builder, scale_factor, image_dim);

  auto T_MAX = vosa::api::const_image<float>(graph_builder, {1, 1, 1}, 255);

  auto T_MAX_as_image =
      vosa::api::broadcast_single_pixel(graph_builder, T_MAX, image_dim);

  auto scaled_depth_map_shifted =
      vosa::api::mult(graph_builder, depth_map_shifted, scale_factor_as_image);
  auto offset_scaled_depth_map_shifted =
      vosa::api::sub(graph_builder, T_MAX_as_image, scaled_depth_map_shifted);

  auto output_image =
      vosa::api::cast<uint8_t>(graph_builder, offset_scaled_depth_map_shifted);
  return output_image;
}

template <typename T>
std::shared_ptr<vosa::api::Op>
bokehGraphShift(vosa::api::GraphBuilder &graph_builder,
                const std::shared_ptr<vosa::api::Op> &);

template <>
std::shared_ptr<vosa::api::Op>
bokehGraphShift<uint32_t>(vosa::api::GraphBuilder &graph_builder,
                          const std::shared_ptr<vosa::api::Op> &input) {
  return vosa::api::logical_shift_right(graph_builder, input, 16);
}

template <>
std::shared_ptr<vosa::api::Op>
bokehGraphShift<uint16_t>(vosa::api::GraphBuilder &graph_builder,
                          const std::shared_ptr<vosa::api::Op> &input) {
  return vosa::api::logical_shift_right(graph_builder, input, 8);
}

template <typename T>
std::shared_ptr<vosa::api::Op>
bokehGraphConv(vosa::api::GraphBuilder &graph_builder,
               const std::shared_ptr<vosa::api::Op> &input,
               const vosa::api::EigenPlane<T> &kernel) {
  uint32_t padding = kernel.dimension(1) / 2;

  Eigen::array<uint32_t, 2> shuffling({1, 0});
  auto kernel_transpose = kernel.shuffle(shuffling);

  std::vector<uint32_t> first_padding_vector = {0, 0, padding, padding};

  auto input_padded = vosa::api::pad<uint8_t>(
      graph_builder, input, 0, vosa::api::PadBase::VOSA_PAD_MODE_REPLICATE,
      first_padding_vector);
  auto input_padded_T = vosa::api::cast<T>(graph_builder, input_padded);

  auto blur_h_32 = vosa::api::conv<T>(graph_builder, input_padded_T, kernel);

  auto blur_h_32_shifted = bokehGraphShift<T>(graph_builder, blur_h_32);

  std::vector<uint32_t> second_padding_vector = {padding, padding, 0, 0};
  auto blur_h_32_padded = vosa::api::pad(
      graph_builder, blur_h_32_shifted, 0,
      vosa::api::PadBase::VOSA_PAD_MODE_REPLICATE, second_padding_vector);
  auto blur_v_32 =
      vosa::api::conv<T>(graph_builder, blur_h_32_padded, kernel_transpose);
  auto blur_v_32_shifted = bokehGraphShift<T>(graph_builder, blur_v_32);
  auto blur_v_8 = vosa::api::cast<uint8_t>(graph_builder, blur_v_32_shifted);
  return blur_v_8;
}

template <int H, int W>
std::shared_ptr<vosa::api::Op>
interpolateGraphBlurs(vosa::api::GraphBuilder &graph_builder,
                      const std::shared_ptr<vosa::api::Op> &depth_map_as_float,
                      const std::shared_ptr<vosa::api::Op> &concat_blurs) {

  std::vector<uint32_t> depth_map_dimensions = {H, W, 1};
  auto bounds_image = vosa::api::const_image(
      graph_builder, depth_map_dimensions, CONSTANTS::BOUNDS);
  auto piecewise_index_image =
      vosa::api::div(graph_builder, depth_map_as_float, bounds_image);

  auto blended_image = vosa::api::reduce_interp_channelwise(
      graph_builder, concat_blurs, piecewise_index_image);

  return blended_image;
}

std::shared_ptr<vosa::api::Op>
interpolateGraphIFBF(vosa::api::GraphBuilder &graph_builder,
                     const std::shared_ptr<vosa::api::Op> &image,
                     const std::shared_ptr<vosa::api::Op> &blended_image,
                     const std::shared_ptr<vosa::api::Op> &depth_map_channel) {

  std::vector<uint32_t> IFBF_plane_size = {540, 960, 2};
  auto IFBF_image = vosa::api::const_image<float>(
      graph_builder, IFBF_plane_size, CONSTANTS::IFBF);

  auto depth_map =
      vosa::api::broadcast_channelwise(graph_builder, depth_map_channel, 2);

  auto IFBF_times_depth = vosa::api::mult(graph_builder, IFBF_image, depth_map);

  float wlow_const = (CONSTANTS::BOUNDS * (1 + 3 * CONSTANTS::IFBF));
  auto wlow_const_image =
      vosa::api::const_image<float>(graph_builder, IFBF_plane_size, wlow_const);
  float whigh_const = (CONSTANTS::BOUNDS * 3);
  auto whigh_const_image = vosa::api::const_image<float>(
      graph_builder, IFBF_plane_size, whigh_const);

  auto wlow_unclamped =
      vosa::api::sub(graph_builder, wlow_const_image, IFBF_times_depth);
  auto whigh_unclamped_sub =
      vosa::api::sub(graph_builder, depth_map, whigh_const_image);
  auto whigh_unclamped =
      vosa::api::mult(graph_builder, whigh_unclamped_sub, IFBF_image);

  auto zero_scalar = vosa::api::const_scalar<float>(graph_builder, 0);
  auto bounds_scalar =
      vosa::api::const_scalar<float>(graph_builder, CONSTANTS::BOUNDS);
  auto wlow_clamped = vosa::api::clamp(graph_builder, wlow_unclamped,
                                       zero_scalar, bounds_scalar);
  auto whigh_clamped = vosa::api::clamp(graph_builder, whigh_unclamped,
                                        zero_scalar, bounds_scalar);

  auto blended_wlow =
      vosa::api::mult(graph_builder, wlow_clamped, blended_image);
  auto Y_as_float_image = vosa::api::cast<float>(graph_builder, image);
  auto blended_whigh =
      vosa::api::mult(graph_builder, whigh_clamped, Y_as_float_image);

  auto blended_pre_div =
      vosa::api::add(graph_builder, blended_wlow, blended_whigh);
  auto bounds_as_image =
      vosa::api::const_image(graph_builder, IFBF_plane_size, CONSTANTS::BOUNDS);
  auto blended =
      vosa::api::div(graph_builder, blended_pre_div, bounds_as_image);

  return blended;
}

std::shared_ptr<vosa::api::Op> vosaGraphGaussianBlurUV(
    vosa::api::GraphBuilder &graph_builder,
    const std::shared_ptr<vosa::api::Op> &depth_map_normalized,
    const std::shared_ptr<vosa::api::Op> &UV) {
  vosa::api::EigenPlane<uint16_t> kernel_540p_7_w{1, 7};
  kernel_540p_7_w.setValues({{9, 28, 56, 70, 56, 28, 9}});
  auto blur_3 = bokehGraphConv<uint16_t>(graph_builder, UV, kernel_540p_7_w);

  vosa::api::EigenPlane<uint16_t> kernel_540p_11_w{1, 11};
  kernel_540p_11_w.setValues({{5, 11, 20, 31, 39, 44, 39, 31, 20, 11, 5}});
  auto blur_5 = bokehGraphConv<uint16_t>(graph_builder, UV, kernel_540p_11_w);

  vosa::api::EigenPlane<uint16_t> kernel_540p_15_w{1, 15};
  kernel_540p_15_w.setValues(
      {{3, 6, 10, 15, 21, 26, 30, 34, 30, 26, 21, 15, 10, 6, 3}});
  auto blur_7 = bokehGraphConv<uint16_t>(graph_builder, UV, kernel_540p_15_w);

  vosa::api::EigenPlane<uint16_t> kernel_540p_21_w{1, 21};
  kernel_540p_21_w.setValues({{1,  2,  4,  6,  9,  12, 16, 19, 22, 24, 26,
                               24, 22, 19, 16, 12, 9,  6,  4,  2,  1}});
  auto blur_11 = bokehGraphConv<uint16_t>(graph_builder, UV, kernel_540p_21_w);

  auto depth_map_as_float =
      vosa::api::cast<float>(graph_builder, depth_map_normalized);

  std::vector<std::shared_ptr<vosa::api::Op>> blurs = {blur_11, blur_7, blur_5,
                                                       blur_3, UV};
  std::vector<std::shared_ptr<vosa::api::Op>> blur_u_palette{};
  std::vector<std::shared_ptr<vosa::api::Op>> blur_v_palette{};

  for (auto &blur : blurs) {
    auto blur_u_channel = vosa::api::channel_extract(graph_builder, blur, 0);
    auto blur_v_channel = vosa::api::channel_extract(graph_builder, blur, 1);

    blur_u_palette.push_back(blur_u_channel);
    blur_v_palette.push_back(blur_v_channel);
  }

  auto blur_u_pallete = vosa::api::concat(graph_builder, blur_u_palette);
  auto blurred_u = interpolateGraphBlurs<540, 960>(
      graph_builder, depth_map_as_float, blur_u_pallete);

  auto blur_v_pallete = vosa::api::concat(graph_builder, blur_v_palette);
  auto blurred_v = interpolateGraphBlurs<540, 960>(
      graph_builder, depth_map_as_float, blur_v_pallete);

  auto blended = vosa::api::concat(graph_builder, {blurred_u, blurred_v});

  auto blended_hr =
      interpolateGraphIFBF(graph_builder, UV, blended, depth_map_as_float);

  auto zero_scalar = vosa::api::const_scalar<float>(graph_builder, 0);
  auto two55_scalar = vosa::api::const_scalar<float>(graph_builder, 255);
  auto clamped_blended =
      vosa::api::clamp(graph_builder, blended_hr, zero_scalar, two55_scalar);
  auto output_as_image =
      vosa::api::cast<uint8_t>(graph_builder, clamped_blended);

  return output_as_image;
}

std::shared_ptr<vosa::api::Op> vosaGraphGaussianBlurY(
    vosa::api::GraphBuilder &graph_builder,
    const std::shared_ptr<vosa::api::Op> &depth_map_normalized,
    const std::shared_ptr<vosa::api::Op> &Y_channel) {

  vosa::api::EigenPlane<uint32_t> kernel_1080p_13_w{1, 13};
  kernel_1080p_13_w.setValues({{1087, 2090, 3569, 5411, 7284, 8707, 9240, 8707,
                                7284, 5411, 3569, 2090, 1087}});
  auto blur_13 =
      bokehGraphConv<uint32_t>(graph_builder, Y_channel, kernel_1080p_13_w);

  vosa::api::EigenPlane<uint32_t> kernel_1080p_21_w{1, 21};
  kernel_1080p_21_w.setValues(
      {{640,  967,  1398, 1936, 2567, 3259, 3962, 4612, 5140, 5486, 5602,
        5486, 5140, 4612, 3962, 3259, 2567, 1936, 1398, 967,  640}});
  auto blur_21 =
      bokehGraphConv<uint32_t>(graph_builder, Y_channel, kernel_1080p_21_w);

  vosa::api::EigenPlane<uint32_t> kernel_1080p_29_w{1, 29};
  kernel_1080p_29_w.setValues(
      {{453,  613,  809,  1046, 1321, 1633, 1973, 2332, 2695, 3046,
        3367, 3640, 3849, 3979, 4024, 3979, 3849, 3640, 3367, 3046,
        2695, 2332, 1973, 1633, 1321, 1046, 809,  613,  453}});
  auto blur_29 =
      bokehGraphConv<uint32_t>(graph_builder, Y_channel, kernel_1080p_29_w);

  vosa::api::EigenPlane<uint32_t> kernel_1080p_43_w{1, 43};
  kernel_1080p_43_w.setValues(
      {{157,  206,  268,  344,  436,  545,  673,  818,  982,  1163, 1358,
        1566, 1780, 1997, 2210, 2413, 2600, 2763, 2897, 2996, 3057, 3078,
        3057, 2996, 2897, 2763, 2600, 2413, 2210, 1997, 1780, 1566, 1358,
        1163, 982,  818,  673,  545,  436,  344,  268,  206,  157}});
  auto blur_43 =
      bokehGraphConv<uint32_t>(graph_builder, Y_channel, kernel_1080p_43_w);

  std::vector<std::shared_ptr<vosa::api::Op>> blurs{blur_43, blur_29, blur_21,
                                                    blur_13, Y_channel};
  auto concat_blurs = vosa::api::concat(graph_builder, blurs);

  auto depth_map_as_float_low_res =
      vosa::api::cast<float>(graph_builder, depth_map_normalized);
  auto depth_map_as_float = vosa::api::resize_bilinear(
      graph_builder, depth_map_as_float_low_res, {1080, 1920});

  auto blended_image = interpolateGraphBlurs<1080, 1920>(
      graph_builder, depth_map_as_float, concat_blurs);

  std::vector<uint32_t> IFBF_image_size = {1080, 1920, 1};
  auto IFBF_image =
      vosa::api::const_image(graph_builder, IFBF_image_size, CONSTANTS::IFBF);

  auto IFBF_times_depth =
      vosa::api::mult(graph_builder, IFBF_image, depth_map_as_float);

  float wlow_const = (CONSTANTS::BOUNDS * (1 + 3 * CONSTANTS::IFBF));
  auto wlow_const_image =
      vosa::api::const_image<float>(graph_builder, IFBF_image_size, wlow_const);
  float whigh_const = (CONSTANTS::BOUNDS * 3);
  auto whigh_const_image = vosa::api::const_image<float>(
      graph_builder, IFBF_image_size, whigh_const);

  auto wlow_unclamped =
      vosa::api::sub(graph_builder, wlow_const_image, IFBF_times_depth);
  auto whigh_unclamped_sub =
      vosa::api::sub(graph_builder, depth_map_as_float, whigh_const_image);
  auto whigh_unclamped =
      vosa::api::mult(graph_builder, whigh_unclamped_sub, IFBF_image);

  auto zero_scalar = vosa::api::const_scalar<float>(graph_builder, 0);
  auto bounds_scalar =
      vosa::api::const_scalar<float>(graph_builder, CONSTANTS::BOUNDS);
  auto wlow_clamped = vosa::api::clamp(graph_builder, wlow_unclamped,
                                       zero_scalar, bounds_scalar);
  auto whigh_clamped = vosa::api::clamp(graph_builder, whigh_unclamped,
                                        zero_scalar, bounds_scalar);

  auto blended_wlow =
      vosa::api::mult(graph_builder, wlow_clamped, blended_image);
  auto Y_as_float_image = vosa::api::cast<float>(graph_builder, Y_channel);
  auto blended_whigh =
      vosa::api::mult(graph_builder, whigh_clamped, Y_as_float_image);

  auto blended_pre_div =
      vosa::api::add(graph_builder, blended_wlow, blended_whigh);
  auto bounds_as_image = vosa::api::const_image<float>(
      graph_builder, IFBF_image_size, CONSTANTS::BOUNDS);
  auto blended =
      vosa::api::div(graph_builder, blended_pre_div, bounds_as_image);

  auto two55_scalar = vosa::api::const_scalar<float>(graph_builder, 255);
  auto clamped_blended =
      vosa::api::clamp(graph_builder, blended, zero_scalar, two55_scalar);
  auto output_as_image =
      vosa::api::cast<uint8_t>(graph_builder, clamped_blended);

  return output_as_image;
}

std::vector<std::shared_ptr<vosa::api::Op>>
make_test_graph(vosa::api::GraphBuilder &graph_builder,
                std::shared_ptr<vosa::api::Op> &depth_map,
                std::shared_ptr<vosa::api::Op> &binary_mask,
                std::shared_ptr<vosa::api::Op> &uv,
                std::shared_ptr<vosa::api::Op> &y) {

  auto depth_map_normalized =
      vosaGraphDepthNormalization(graph_builder, depth_map, binary_mask);

  auto gaussian_blur_uv =
      vosaGraphGaussianBlurUV(graph_builder, depth_map_normalized, uv);

  auto gaussian_blur_y =
      vosaGraphGaussianBlurY(graph_builder, depth_map_normalized, y);
  return {depth_map_normalized, gaussian_blur_uv, gaussian_blur_y};
}

} // namespace vosa::samples::bokeh