// RUN: vosa-opt -vosa-to-tosa -split-input-file -verify-diagnostics %s | FileCheck %s

module {
    // CHECK-LABEL: func.func @constants()
    func.func @constants() {
        // CHECK: %{{.*}} = arith.constant 1 : i8
        %c1_si8 = vosa.constant 1 : si8

        // CHECK: %{{.*}} = arith.constant -128 : i8
        %cn128_si8 = vosa.constant -128 : si8

        // CHECK: %{{.*}} = arith.constant 127 : i8
        %c127_si8 = vosa.constant 127 : si8

        // CHECK: %{{.*}} = arith.constant 1 : i8
        %c1_ui8 = vosa.constant 1 : ui8

        // CHECK: %{{.*}} = arith.constant 127 : i8
        %c127_ui8 = vosa.constant 127 : ui8

        // CHECK: %{{.*}} = arith.constant -128 : i8
        %c128_ui8 = vosa.constant 128 : ui8

        // CHECK: %{{.*}} = arith.constant -1 : i8
        %c255_ui8 = vosa.constant 255 : ui8

        // CHECK: %{{.*}} = arith.constant 1.230000e+02 : f32
        %c123_f32 = vosa.constant 123.0 : f32

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @tensor_constants()
    func.func @tensor_constants() {
        // small splats (<1000 elements) are inline
        // CHECK: %{{.*}} = arith.constant dense<0> : tensor<100xi8>
        %cst_100_si8 = vosa.constant dense<0> : tensor<100xsi8>
        // CHECK: %{{.*}} = arith.constant dense<0.000000e+00> : tensor<100xf32>
        %cst_100_f32 = vosa.constant dense<0.0> : tensor<100xf32>

        // large splats (>= 1000 elements) are filled at runtime
        // CHECK: %{{.*}} = arith.constant 0 : i8
        // CHECK: %{{.*}} = tensor.empty() : tensor<1000xi8>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: i8) outs(%{{.*}}: tensor<1000xi8>) -> tensor<1000xi8>
        %cst_1000_si8 = vosa.constant dense<0> : tensor<1000xsi8>

        // CHECK: %{{.*}} = arith.constant 0 : i16
        // CHECK: %{{.*}} = tensor.empty() : tensor<1000xi16>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: i16) outs(%{{.*}}: tensor<1000xi16>) -> tensor<1000xi16>
        %cst_1000_si16 = vosa.constant dense<0> : tensor<1000xsi16>

        // CHECK: %{{.*}} = arith.constant 0 : i32
        // CHECK: %{{.*}} = tensor.empty() : tensor<1000xi32>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: i32) outs(%{{.*}}: tensor<1000xi32>) -> tensor<1000xi32>
        %cst_1000_si32 = vosa.constant dense<0> : tensor<1000xsi32>

        // CHECK: %{{.*}} = arith.constant 0 : i32
        // CHECK: %{{.*}} = tensor.empty() : tensor<1000xi32>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: i32) outs(%{{.*}}: tensor<1000xi32>) -> tensor<1000xi32>
        %cst_1000_ui32 = vosa.constant dense<0> : tensor<1000xui32>

        // CHECK: %{{.*}} = arith.constant 1.230000e+02 : f32
        // CHECK: %{{.*}} = tensor.empty() : tensor<1000xf32>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: f32) outs(%{{.*}}: tensor<1000xf32>) -> tensor<1000xf32>
        %cst_1000_sf32 = vosa.constant dense<123.0> : tensor<1000xf32>

        // CHECK: %{{.*}} = arith.constant 0 : i8
        // CHECK: %{{.*}} = tensor.empty() : tensor<200x100x1xi8>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: i8) outs(%{{.*}}: tensor<200x100x1xi8>) -> tensor<200x100x1xi8>
        %cst_200x100_si8 = vosa.constant dense<0> : tensor<200x100x1xsi8>

        // CHECK: %{{.*}} = arith.constant 0 : i16
        // CHECK: %{{.*}} = tensor.empty() : tensor<200x100x1xi16>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: i16) outs(%{{.*}}: tensor<200x100x1xi16>) -> tensor<200x100x1xi16>
        %cst_200x100_si16 = vosa.constant dense<0> : tensor<200x100x1xsi16>

        // CHECK: %{{.*}} = arith.constant 0 : i32
        // CHECK: %{{.*}} = tensor.empty() : tensor<200x100x1xi32>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: i32) outs(%{{.*}}: tensor<200x100x1xi32>) -> tensor<200x100x1xi32>
        %cst_200x100_si32 = vosa.constant dense<0> : tensor<200x100x1xsi32>

        // CHECK: %{{.*}} = arith.constant 1.230000e+02 : f32
        // CHECK: %{{.*}} = tensor.empty() : tensor<200x100x1xf32>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: f32) outs(%{{.*}}: tensor<200x100x1xf32>) -> tensor<200x100x1xf32>
        %cst_200x100_f32 = vosa.constant dense<123.0> : tensor<200x100x1xf32>

        // CHECK: %{{.*}} = arith.constant 0 : i8
        // CHECK: %{{.*}} = tensor.empty() : tensor<200x100x3xi8>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: i8) outs(%{{.*}}: tensor<200x100x3xi8>) -> tensor<200x100x3xi8>
        %cst_200x100x3_si8 = vosa.constant dense<0> : tensor<200x100x3xsi8>

        // CHECK: %{{.*}} = arith.constant 0 : i16
        // CHECK: %{{.*}} = tensor.empty() : tensor<200x100x3xi16>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: i16) outs(%{{.*}}: tensor<200x100x3xi16>) -> tensor<200x100x3xi16>
        %cst_200x100x3_si16 = vosa.constant dense<0> : tensor<200x100x3xsi16>

        // CHECK: %{{.*}} = arith.constant 0 : i32
        // CHECK: %{{.*}} = tensor.empty() : tensor<200x100x3xi32>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: i32) outs(%{{.*}}: tensor<200x100x3xi32>) -> tensor<200x100x3xi32>
        %cst_200x100x3_si32 = vosa.constant dense<0> : tensor<200x100x3xsi32>

        // CHECK: %{{.*}} = arith.constant 1.230000e+02 : f32
        // CHECK: %{{.*}} = tensor.empty() : tensor<200x100x3xf32>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: f32) outs(%{{.*}}: tensor<200x100x3xf32>) -> tensor<200x100x3xf32>
        %cst_200x100x3_f32 = vosa.constant dense<123.0> : tensor<200x100x3xf32>

        // CHECK: %{{.*}} = arith.constant dense<{{.*}}> : tensor<2xi8>
        %cst_2_si8 = vosa.constant dense<[1, 2]> : tensor<2xsi8>
        // CHECK: %{{.*}} = arith.constant dense<{{.*}}> : tensor<2x2xi8>
        %cst_2x2_si8 = vosa.constant dense<[[1, 2], [3, 4]]> : tensor<2x2xsi8>
        // CHECK: %{{.*}} = arith.constant dense<{{.*}}> : tensor<1x2x2xi8>
        %cst_1x2x2_si8 = vosa.constant dense<[[[1, 2], [3, 4]]]> : tensor<1x2x2xsi8>

        // TODO: float
        return
    }
}

// -----

module {
    func.func @broadcast_planewise(%arg0 : tensor<1x1x1xsi8>) -> tensor<100x200x1xsi8>{
        %c0_si8 = vosa.constant dense<1> : tensor<1x1x1xsi8>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: i8) outs(%{{.*}}: tensor<100x200x1xi8>) -> tensor<100x200x1xi8>
        %res_si8 = vosa.broadcast_planewise %c0_si8 { size = [ 100 : i32, 200 : i32 ] } : tensor<1x1x1xsi8> -> tensor<100x200x1xsi8>
        return %res_si8 : tensor<100x200x1xsi8>
    }
}

// -----

module {
    // CHECK: func.func @broadcast_channelwise_1(%[[ARG:.*]]: tensor<100x200x1xsi8>) -> tensor<100x200x1xsi8>
    func.func @broadcast_channelwise_1(%arg0 : tensor<100x200x1xsi8>) -> tensor<100x200x1xsi8> {
        %res0 = vosa.broadcast_channelwise %arg0 { size = 1 : i32 } : tensor<100x200x1xsi8> -> tensor<100x200x1xsi8>
        // CHECK return %[[ARG]] : tensor<100x200x1xsi8>
        return %res0 : tensor<100x200x1xsi8>
    }

    func.func @broadcast_channelwise_3(%arg0 : tensor<100x200x1xsi8>) -> tensor<100x200x3xsi8> {
        // CHECK: %{{.*}} = tosa.tile %{{.*}} {multiples = array<i64: 1, 1, 3>} : (tensor<100x200x1xi8>) -> tensor<100x200x3xi8>
        %res0 = vosa.broadcast_channelwise %arg0 { size = 3 : i32 } : tensor<100x200x1xsi8> -> tensor<100x200x3xsi8>
        return %res0 : tensor<100x200x3xsi8>
    }

    func.func @broadcast_channelwise_fill() -> tensor<100x200x3xsi8> {
        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x3xi8>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: i8) outs(%{{.*}}: tensor<100x200x3xi8>) -> tensor<100x200x3xi8>
        %p = vosa.constant dense<42> : tensor<100x200x1xsi8>
        %res0 = vosa.broadcast_channelwise %p { size = 3 : i32 } : tensor<100x200x1xsi8> -> tensor<100x200x3xsi8>
        return %res0 : tensor<100x200x3xsi8>
    }
}

// -----

module {
    // CHECK-LABEL: func.func @cast()
    func.func @cast() {
        %t_bool = vosa.constant dense<1> : tensor<100x200x2xi1>
        %t_ui8 = vosa.constant dense<42> : tensor<100x200x2xui8>
        %t_si8 = vosa.constant dense<42> : tensor<100x200x2xsi8>
        %t_ui32 = vosa.constant dense<42> : tensor<100x200x2xui32>
        %t_si32 = vosa.constant dense<42> : tensor<100x200x2xsi32>
        %t_f32 = vosa.constant dense<42.0> : tensor<100x200x2xf32>

        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: linalg.yield %in
        %res_si8_si8 = vosa.cast %t_si8 wrap : tensor<100x200x2xsi8> -> tensor<100x200x2xsi8>

        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: arith.bitcast
        // CHECK: linalg.yield
        %res_si8_ui8 = vosa.cast %t_si8 wrap : tensor<100x200x2xsi8> -> tensor<100x200x2xui8>

        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xi32>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi32>)
        // CHECK: arith.extsi
        // CHECK: linalg.yield
        %res_si8_si32 = vosa.cast %t_si8 wrap : tensor<100x200x2xsi8> -> tensor<100x200x2xsi32>

        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xi32>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi32>)
        // CHECK: arith.extsi
        // CHECK: linalg.yield
        %res_si8_ui32 = vosa.cast %t_si8 wrap : tensor<100x200x2xsi8> -> tensor<100x200x2xui32>

        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xi8>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi32>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: arith.trunci
        // CHECK: linalg.yield
        %res_si32_si8 = vosa.cast %t_si32 wrap : tensor<100x200x2xsi32> -> tensor<100x200x2xsi8>

        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xf32>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi32>) outs(%{{.*}} : tensor<100x200x2xf32>)
        // CHECK: arith.sitofp
        // CHECK: linalg.yield
        %res_si32_f32 = vosa.cast %t_si32 wrap : tensor<100x200x2xsi32> -> tensor<100x200x2xf32>

        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi1>)
        // CHECK: arith.const
        // CHECK: arith.cmpi
        // CHECK: linalg.yield
        %res_si8_bool = vosa.cast %t_si8 wrap : tensor<100x200x2xsi8> -> tensor<100x200x2xi1>

        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: linalg.yield %in
        %res_ui8_ui8 = vosa.cast %t_ui8 wrap : tensor<100x200x2xui8> -> tensor<100x200x2xui8>

        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xi8>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: arith.bitcast
        // CHECK: linalg.yield
        %res_ui8_si8 = vosa.cast %t_ui8 wrap : tensor<100x200x2xui8> -> tensor<100x200x2xsi8>

        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xi32>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi32>)
        // CHECK: arith.extui
        // CHECK: linalg.yield
        %res_ui8_si32 = vosa.cast %t_ui8 wrap : tensor<100x200x2xui8> -> tensor<100x200x2xsi32>

        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xi32>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi32>)
        // CHECK: arith.extui
        // CHECK: linalg.yield
        %res_ui8_ui32 = vosa.cast %t_ui8 wrap : tensor<100x200x2xui8> -> tensor<100x200x2xui32>

        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xi8>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi32>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: arith.trunci
        // CHECK: linalg.yield
        %res_ui32_ui8 = vosa.cast %t_ui32 wrap : tensor<100x200x2xui32> -> tensor<100x200x2xui8>

        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xf32>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi32>) outs(%{{.*}} : tensor<100x200x2xf32>)
        // CHECK: arith.uitofp
        // CHECK: linalg.yield
        %res_ui32_f32 = vosa.cast %t_ui32 wrap : tensor<100x200x2xui32> -> tensor<100x200x2xf32>

        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xf32>) outs(%{{.*}} : tensor<100x200x2xf32>)
        // CHECK: linalg.yield %in
        %res_f32_f32 = vosa.cast %t_f32 wrap : tensor<100x200x2xf32> -> tensor<100x200x2xf32>

        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi1>)
        // CHECK: arith.const
        // CHECK: arith.cmpi
        // CHECK: linalg.yield
        %res_ui8_bool = vosa.cast %t_ui8 wrap : tensor<100x200x2xui8> -> tensor<100x200x2xi1>

        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xi8>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xf32>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: arith.fptosi
        // CHECK: linalg.yield
        %res_f32_si8 = vosa.cast %t_f32 wrap : tensor<100x200x2xf32> -> tensor<100x200x2xsi8>

        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xi8>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xf32>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: arith.fptoui
        // CHECK: linalg.yield
        %res_f32_ui8 = vosa.cast %t_f32 wrap : tensor<100x200x2xf32> -> tensor<100x200x2xui8>

        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xi32>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi1>) outs(%{{.*}} : tensor<100x200x2xi32>)
        // CHECK: arith.extui
        // CHECK: linalg.yield
        %res_bool_si32 = vosa.cast %t_bool wrap : tensor<100x200x2xi1> -> tensor<100x200x2xsi32>

        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xi8>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi1>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: arith.extui
        // CHECK: linalg.yield
        %res_bool_ui8 = vosa.cast %t_bool wrap : tensor<100x200x2xi1> -> tensor<100x200x2xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @cast_saturate_si()
    func.func @cast_saturate_si() {
        %t_si8 = vosa.constant dense<42> : tensor<100x200x2xsi8>
        %t_si16 = vosa.constant dense<42> : tensor<100x200x2xsi16>
        %t_si32 = vosa.constant dense<42> : tensor<100x200x2xsi32>

        // SI -> SI
        //  same
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: linalg.yield %in : i8
        %res_si8_si8 = vosa.cast %t_si8 saturate : tensor<100x200x2xsi8> -> tensor<100x200x2xsi8>

        //  widening
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi32>)
        // CHECK: arith.extsi
        // CHECK: linalg.yield
        %res_si8_si32 = vosa.cast %t_si8 saturate : tensor<100x200x2xsi8> -> tensor<100x200x2xsi32>

        //  narrowing
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi32>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: %[[MIN:.*]] = arith.constant -128 : i32
        // CHECK: %[[CMP:.*]] = arith.cmpi slt, %in, %[[MIN]] : i32
        // CHECK: %[[V1:.*]] = arith.select %[[CMP]], %[[MIN]], %in : i32
        // CHECK: %[[MAX:.*]] = arith.constant 127 : i32
        // CHECK: %[[CMP:.*]] = arith.cmpi slt, %[[V1]], %[[MAX]] : i32
        // CHECK: %[[V2:.*]] = arith.select %[[CMP]], %[[V1]], %[[MAX]] : i32
        // CHECK: %[[RES:.*]] = arith.trunci %[[V2]] : i32 to i8
        // CHECK: linalg.yield %[[RES]] : i8
        %res_si32_si8 = vosa.cast %t_si32 saturate : tensor<100x200x2xsi32> -> tensor<100x200x2xsi8>

        // SI -> UI
        //  same
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: %[[ZERO:.*]] = arith.constant 0 : i8
        // CHECK: %[[CMP:.*]] = arith.cmpi slt, %in, %[[ZERO]] : i8
        // CHECK: %[[V:.*]] = arith.select %[[CMP]], %[[ZERO]], %in : i8
        // CHECK: %[[RES:.*]] = arith.bitcast %[[V]] : i8 to i8
        // CHECK: linalg.yield %[[RES]] : i8
        %res_si8_ui8 = vosa.cast %t_si8 saturate : tensor<100x200x2xsi8> -> tensor<100x200x2xui8>

        //  widening
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi32>)
        // CHECK: %[[ZERO:.*]] = arith.constant 0 : i8
        // CHECK: %[[CMP:.*]] = arith.cmpi slt, %in, %[[ZERO]] : i8
        // CHECK: %[[V:.*]] = arith.select %[[CMP]], %[[ZERO]], %in : i8
        // CHECK: %[[RES:.*]] = arith.extsi %[[V]] : i8 to i32
        // CHECK: linalg.yield %[[RES]] : i32
        %res_si8_ui32 = vosa.cast %t_si8 saturate : tensor<100x200x2xsi8> -> tensor<100x200x2xui32>

        //  narrowing
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi32>) outs(%{{.*}} : tensor<100x200x2xi16>)
        // CHECK: %[[ZERO:.*]] = arith.constant 0 : i32
        // CHECK: %[[CMP:.*]] = arith.cmpi slt, %in, %[[ZERO]] : i32
        // CHECK: %[[V1:.*]] = arith.select %[[CMP]], %[[ZERO]], %in : i32
        // CHECK: %[[MAX:.*]] = arith.constant 65535 : i32
        // CHECK: %[[CMP:.*]] = arith.cmpi slt, %[[V1]], %[[MAX]] : i32
        // CHECK: %[[V2:.*]] = arith.select %[[CMP]], %[[V1]], %[[MAX]] : i32
        // CHECK: %[[RES:.*]] = arith.trunci %[[V2]] : i32 to i16
        // CHECK: linalg.yield %[[RES]] : i16
        %res_si32_ui16 = vosa.cast %t_si32 saturate : tensor<100x200x2xsi32> -> tensor<100x200x2xui16>

        // SI -> BOOL
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi1>)
        // CHECK: %[[ZERO:.*]] = arith.constant 0 : i8
        // CHECK: %[[RES:.*]] = arith.cmpi ne, %in, %[[ZERO]] : i8
        // CHECK: linalg.yield %[[RES]] : i1
        %res_si8_bool = vosa.cast %t_si8 saturate : tensor<100x200x2xsi8> -> tensor<100x200x2xi1>

        // SI -> FLOAT
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi32>) outs(%{{.*}} : tensor<100x200x2xf32>)
        // CHECK: arith.sitofp
        // CHECK: linalg.yield
        %res_si32_f32 = vosa.cast %t_si32 saturate : tensor<100x200x2xsi32> -> tensor<100x200x2xf32>

        return
    }

    // CHECK-LABEL: func.func @cast_saturate_ui()
    func.func @cast_saturate_ui() {
        %t_ui8 = vosa.constant dense<42> : tensor<100x200x2xui8>
        %t_ui32 = vosa.constant dense<42> : tensor<100x200x2xui32>

        // UI -> SI
        //  same
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: %[[MAX:.*]] = arith.constant 127 : i8
        // CHECK: %[[CMP:.*]] = arith.cmpi ult, %in, %[[MAX]] : i8
        // CHECK: %[[V:.*]] = arith.select %[[CMP]], %in, %[[MAX]] : i8
        // CHECK: %[[RES:.*]] = arith.bitcast %[[V]] : i8 to i8
        // CHECK: linalg.yield %[[RES]] : i8
        %res_ui8_si8 = vosa.cast %t_ui8 saturate : tensor<100x200x2xui8> -> tensor<100x200x2xsi8>

        //  widening
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi32>)
        // CHECK: arith.extui
        // CHECK: linalg.yield
        %res_ui8_si32 = vosa.cast %t_ui8 saturate : tensor<100x200x2xui8> -> tensor<100x200x2xsi32>

        //  narrowing
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi32>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: %[[MAX:.*]] = arith.constant 127 : i32
        // CHECK: %[[CMP:.*]] = arith.cmpi ult, %in, %[[MAX]] : i32
        // CHECK: %[[V:.*]] = arith.select %[[CMP]], %in, %[[MAX]] : i32
        // CHECK: %[[RES:.*]] = arith.trunci %[[V]] : i32 to i8
        // CHECK: linalg.yield %[[RES]] : i8
        %res_ui32_si8 = vosa.cast %t_ui32 saturate : tensor<100x200x2xui32> -> tensor<100x200x2xsi8>

        // UI -> UI
        //  same
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: linalg.yield %in : i8
        %res_ui8_ui8 = vosa.cast %t_ui8 saturate : tensor<100x200x2xui8> -> tensor<100x200x2xui8>

        //  widening
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi32>)
        // CHECK: arith.extui
        // CHECK: linalg.yield
        %res_ui8_ui32 = vosa.cast %t_ui8 saturate : tensor<100x200x2xui8> -> tensor<100x200x2xui32>

        //  narrowing
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi32>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: %[[MAX:.*]] = arith.constant 255 : i32
        // CHECK: %[[CMP:.*]] = arith.cmpi ult, %in, %[[MAX]] : i32
        // CHECK: %[[V:.*]] = arith.select %[[CMP]], %in, %[[MAX]] : i32
        // CHECK: %[[RES:.*]] = arith.trunci %[[V]] : i32 to i8
        // CHECK: linalg.yield %[[RES]] : i8
        %res_ui32_ui8 = vosa.cast %t_ui32 saturate : tensor<100x200x2xui32> -> tensor<100x200x2xui8>

        // UI -> BOOL
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi8>) outs(%{{.*}} : tensor<100x200x2xi1>)
        // CHECK: %[[ZERO:.*]] = arith.constant 0 : i8
        // CHECK: %[[RES:.*]] = arith.cmpi ne, %in, %[[ZERO]] : i8
        // CHECK: linalg.yield %[[RES]] : i1
        %res_ui8_bool = vosa.cast %t_ui8 saturate : tensor<100x200x2xui8> -> tensor<100x200x2xi1>

        // UI -> FLOAT
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi32>) outs(%{{.*}} : tensor<100x200x2xf32>)
        // CHECK: arith.uitofp
        // CHECK: linalg.yield
        %res_ui32_f32 = vosa.cast %t_ui32 saturate : tensor<100x200x2xui32> -> tensor<100x200x2xf32>

        return
    }

    // CHECK-LABEL: func.func @cast_saturate_float()
    func.func @cast_saturate_float() {
        %t_f32 = vosa.constant dense<42.0> : tensor<100x200x2xf32>
        %t_f16 = vosa.constant dense<42.0> : tensor<100x200x2xf16>

        // FLOAT -> SI
        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xi8>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xf32>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: %[[MIN:.*]] = arith.constant -1.280000e+02 : f32
        // CHECK: %[[MAX:.*]] = arith.constant 1.270000e+02 : f32
        // CHECK: %[[CMP:.*]] = arith.cmpf olt, %in, %[[MIN]] : f32
        // CHECK: %[[V1:.*]] = arith.select %[[CMP]], %[[MIN]], %in : f32
        // CHECK: %[[CMP:.*]] = arith.cmpf olt, %[[V1]], %[[MAX]] : f32
        // CHECK: %[[V2:.*]] = arith.select %[[CMP]], %[[V1]], %[[MAX]] : f32
        // CHECK: %[[RES:.*]] = arith.fptosi %[[V2]] : f32 to i8
        // CHECK: linalg.yield %[[RES]] : i8
        %res_f32_si8 = vosa.cast %t_f32 saturate : tensor<100x200x2xf32> -> tensor<100x200x2xsi8>

        // FLOAT -> UI
        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x2xi8>
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xf32>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: %[[MIN:.*]] = arith.constant 0.000000e+00 : f32
        // CHECK: %[[MAX:.*]] = arith.constant 2.550000e+02 : f32
        // CHECK: %[[CMP:.*]] = arith.cmpf olt, %in, %[[MIN]] : f32
        // CHECK: %[[V1:.*]] = arith.select %[[CMP]], %[[MIN]], %in : f32
        // CHECK: %[[CMP:.*]] = arith.cmpf olt, %[[V1]], %[[MAX]] : f32
        // CHECK: %[[V2:.*]] = arith.select %[[CMP]], %[[V1]], %[[MAX]] : f32
        // CHECK: %[[RES:.*]] = arith.fptoui %[[V2]] : f32 to i8
        // CHECK: linalg.yield %[[RES]] : i8
        %res_f32_ui8 = vosa.cast %t_f32 saturate : tensor<100x200x2xf32> -> tensor<100x200x2xui8>

        // FLOAT -> FLOAT
        //  same
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xf32>) outs(%{{.*}} : tensor<100x200x2xf32>)
        // CHECK: linalg.yield %in
        %res_f32_f32 = vosa.cast %t_f32 saturate : tensor<100x200x2xf32> -> tensor<100x200x2xf32>

        //  widening
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xf16>) outs(%{{.*}} : tensor<100x200x2xf32>)
        // CHECK: arith.extf
        // CHECK: linalg.yield
        %res_f16_f32 = vosa.cast %t_f16 saturate : tensor<100x200x2xf16> -> tensor<100x200x2xf32>

        //  narrowing
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xf32>) outs(%{{.*}} : tensor<100x200x2xf16>)
        // CHECK: %[[MIN:.*]] = arith.constant -6.550400e+04 : f32
        // CHECK: %[[MAX:.*]] = arith.constant 6.550400e+04 : f32
        // CHECK: %[[CMP:.*]] = arith.cmpf olt, %in, %[[MIN]] : f32
        // CHECK: %[[V1:.*]] = arith.select %[[CMP]], %[[MIN]], %in : f32
        // CHECK: %[[CMP:.*]] = arith.cmpf olt, %[[V1]], %[[MAX]] : f32
        // CHECK: %[[V2:.*]] = arith.select %[[CMP]], %[[V1]], %[[MAX]] : f32
        // CHECK: %[[RES:.*]] = arith.truncf %[[V2]] : f32 to f16
        // CHECK: linalg.yield %[[RES]] : f16
        %res_f32_f16 = vosa.cast %t_f32 saturate : tensor<100x200x2xf32> -> tensor<100x200x2xf16>

        return
    }

    // CHECK-LABEL: func.func @cast_saturate_bool()
    func.func @cast_saturate_bool() {
        %t_bool = vosa.constant dense<1> : tensor<100x200x2xi1>

        // BOOL -> SI
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi1>) outs(%{{.*}} : tensor<100x200x2xi32>)
        // CHECK: arith.extui
        // CHECK: linalg.yield
        %res_bool_si32 = vosa.cast %t_bool saturate : tensor<100x200x2xi1> -> tensor<100x200x2xsi32>

        // BOOL -> UI
        // CHECK: %{{.*}} = linalg.generic {{.*}} ins(%{{.*}} : tensor<100x200x2xi1>) outs(%{{.*}} : tensor<100x200x2xi8>)
        // CHECK: arith.extui
        // CHECK: linalg.yield
        %res_bool_ui8 = vosa.cast %t_bool saturate : tensor<100x200x2xi1> -> tensor<100x200x2xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @channel_extract()
    func.func @channel_extract() {
        %t_si8 = vosa.constant dense<42> : tensor<100x200x6xsi8>
        // CHECK: %{{.*}} = tosa.slice %{{.*}} {size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 0>} : (tensor<100x200x6xi8>) -> tensor<100x200x1xi8>
        %res0 = vosa.channel_extract %t_si8 { channels = [ 0 : i32 ] } : tensor<100x200x6xsi8> -> tensor<100x200x1xsi8>

        // CHECK: %{{.*}} = tosa.slice %{{.*}} {size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 1>} : (tensor<100x200x6xi8>) -> tensor<100x200x1xi8>
        %res1 = vosa.channel_extract %t_si8 { channels = [ 1 : i32 ] } : tensor<100x200x6xsi8> -> tensor<100x200x1xsi8>

        %t1_si8 = vosa.constant dense<42> : tensor<100x200x1xsi8>
        // CHECK: %{{.*}} = tosa.slice %{{.*}} {size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 0>} : (tensor<100x200x1xi8>) -> tensor<100x200x1xi8>
        %res2 = vosa.channel_extract %t1_si8 { channels = [ 0 : i32 ] } : tensor<100x200x1xsi8> -> tensor<100x200x1xsi8>

        // contiguous channels
        // CHECK: %{{.*}} = tosa.slice %{{.*}} {size = array<i64: 100, 200, 2>, start = array<i64: 0, 0, 0>} : (tensor<100x200x6xi8>) -> tensor<100x200x2xi8>
        %res3 = vosa.channel_extract %t_si8 { channels = [ 0 : i32, 1 : i32 ] } : tensor<100x200x6xsi8> -> tensor<100x200x2xsi8>
        // CHECK: %{{.*}} = tosa.slice %{{.*}} {size = array<i64: 100, 200, 3>, start = array<i64: 0, 0, 1>} : (tensor<100x200x6xi8>) -> tensor<100x200x3xi8>
        %res4 = vosa.channel_extract %t_si8 { channels = [ 1 : i32, 2 : i32, 3 : i32 ] } : tensor<100x200x6xsi8> -> tensor<100x200x3xsi8>

        // alternate channels
        // CHECK: %[[C1:.*]] = tosa.slice %{{.*}} {size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 1>} : (tensor<100x200x6xi8>) -> tensor<100x200x1xi8>
        // CHECK: %[[C3:.*]] = tosa.slice %{{.*}} {size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 3>} : (tensor<100x200x6xi8>) -> tensor<100x200x1xi8>
        // CHECK: %[[C5:.*]] = tosa.slice %{{.*}} {size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 5>} : (tensor<100x200x6xi8>) -> tensor<100x200x1xi8>
        // CHECK: %{{.*}} = tosa.concat %[[C1]], %[[C3]], %[[C5]] {axis = 2 : i32} : (tensor<100x200x1xi8>, tensor<100x200x1xi8>, tensor<100x200x1xi8>) -> tensor<100x200x3xi8>
        %res5 = vosa.channel_extract %t_si8 { channels = [ 1 : i32, 3 : i32, 5 : i32 ] } : tensor<100x200x6xsi8> -> tensor<100x200x3xsi8>

        // two blocks of contiguous channels
        // CHECK: %[[CA:.*]] = tosa.slice %{{.*}} {size = array<i64: 100, 200, 2>, start = array<i64: 0, 0, 0>} : (tensor<100x200x6xi8>) -> tensor<100x200x2xi8>
        // CHECK: %[[CB:.*]] = tosa.slice %{{.*}} {size = array<i64: 100, 200, 3>, start = array<i64: 0, 0, 3>} : (tensor<100x200x6xi8>) -> tensor<100x200x3xi8>
        // CHECK: %{{.*}} = tosa.concat %[[CA]], %[[CB]] {axis = 2 : i32} : (tensor<100x200x2xi8>, tensor<100x200x3xi8>) -> tensor<100x200x5xi8>
        %res6 = vosa.channel_extract %t_si8 { channels = [ 0 : i32, 1 : i32, 3 : i32, 4 : i32, 5 : i32 ] } : tensor<100x200x6xsi8> -> tensor<100x200x5xsi8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @concat()
    func.func @concat() {
        // CHECK: %[[C1:.*]] = linalg.fill{{.*}}
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x1xsi8>
        // CHECK: %[[C2A:.*]] = linalg.fill{{.*}}
        %t2a_si8 = vosa.constant dense<42> : tensor<100x200x2xsi8>
        // CHECK: %[[C2B:.*]] = linalg.fill{{.*}}
        %t2b_si8 = vosa.constant dense<43> : tensor<100x200x2xsi8>

        // nops - input values are used directly
        %res1 = vosa.concat %t1_si8 : tensor<100x200x1xsi8> -> tensor<100x200x1xsi8>
        %res2 = vosa.concat %t2a_si8 : tensor<100x200x2xsi8> -> tensor<100x200x2xsi8>

        // CHECK: %{{.*}} = tosa.add %[[C2A]], %[[C2B]] : (tensor<100x200x2xi8>, tensor<100x200x2xi8>) -> tensor<100x200x2xi8>
        %res0 = vosa.add %res2, %t2b_si8 : tensor<100x200x2xsi8>, tensor<100x200x2xsi8> -> tensor<100x200x2xsi8>

        // CHECK: %{{.*}} = tosa.concat %[[C1]], %[[C2A]] {axis = 2 : i32} : (tensor<100x200x1xi8>, tensor<100x200x2xi8>) -> tensor<100x200x3xi8>
        %res3 = vosa.concat %t1_si8, %t2a_si8 : tensor<100x200x1xsi8>, tensor<100x200x2xsi8> -> tensor<100x200x3xsi8>

        // CHECK: %{{.*}} = tosa.concat %[[C2A]], %[[C2B]] {axis = 2 : i32} : (tensor<100x200x2xi8>, tensor<100x200x2xi8>) -> tensor<100x200x4xi8>
        %res4 = vosa.concat %t2a_si8, %t2b_si8 : tensor<100x200x2xsi8>, tensor<100x200x2xsi8> -> tensor<100x200x4xsi8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @export_channel(%{{.*}})
    func.func @export_channel(%d_20000_si8: tensor<20000xsi8>, %d_40000_si8: tensor<40000xsi8>, %d_20001_si8: tensor<20001xsi8>,
                       %d_20000_si16: tensor<20000xsi16>, %d_40000_si16: tensor<40000xsi16>, %d_20001_si16: tensor<20001xsi16>,
                       %d_20000_f32: tensor<20000xf32>, %d_40000_f32: tensor<40000xf32>, %d_20001_f32: tensor<20001xf32>) {
        %c1_si8 = vosa.constant dense<42> : tensor<100x200x1xsi8>
        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 20000>} : (tensor<100x200x1xi8>) -> tensor<20000xi8>
        %res1_si8 = vosa.export_channel %c1_si8, %d_20000_si8 { stride = 1 : i32, offset = 0 : i32 } : tensor<100x200x1xsi8>, tensor<20000xsi8> -> tensor<20000xsi8>

        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 20000>} : (tensor<100x200x1xi8>) -> tensor<20000xi8>
        // CHECK: %{{.*}} = tensor.insert_slice %{{.*}} into %{{.*}}[0] [20000] [2] : tensor<20000xi8> into tensor<40000xi8>
        %res2_si8 = vosa.export_channel %c1_si8, %d_40000_si8 { stride = 2 : i32, offset = 0 : i32 } : tensor<100x200x1xsi8>, tensor<40000xsi8> -> tensor<40000xsi8>

        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 20000>} : (tensor<100x200x1xi8>) -> tensor<20000xi8>
        // CHECK: %{{.*}} = tensor.insert_slice %{{.*}} into %{{.*}}[1] [20000] [1] : tensor<20000xi8> into tensor<20001xi8>
        %res3_si8 = vosa.export_channel %c1_si8, %d_20001_si8 { stride = 1 : i32, offset = 1 : i32 } : tensor<100x200x1xsi8>, tensor<20001xsi8> -> tensor<20001xsi8>

        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 20000>} : (tensor<100x200x1xi8>) -> tensor<20000xi8>
        // CHECK: %{{.*}} = tensor.insert_slice %{{.*}} into %{{.*}}[1] [20000] [2] : tensor<20000xi8> into tensor<40000xi8>
        %res4_si8 = vosa.export_channel %c1_si8, %d_40000_si8 { stride = 2 : i32, offset = 1 : i32 } : tensor<100x200x1xsi8>, tensor<40000xsi8> -> tensor<40000xsi8>

        %c1_si16 = vosa.constant dense<42> : tensor<100x200x1xsi16>
        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 20000>} : (tensor<100x200x1xi16>) -> tensor<20000xi16>
        %res1_si16 = vosa.export_channel %c1_si16, %d_20000_si16 { stride = 2 : i32, offset = 0 : i32 } : tensor<100x200x1xsi16>, tensor<20000xsi16> -> tensor<20000xsi16>

        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 20000>} : (tensor<100x200x1xi16>) -> tensor<20000xi16>
        // CHECK: %{{.*}} = tensor.insert_slice %{{.*}} into %{{.*}}[0] [20000] [2] : tensor<20000xi16> into tensor<40000xi16>
        %res2_si16 = vosa.export_channel %c1_si16, %d_40000_si16 { stride = 4 : i32, offset = 0 : i32 } : tensor<100x200x1xsi16>, tensor<40000xsi16> -> tensor<40000xsi16>

        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 20000>} : (tensor<100x200x1xi16>) -> tensor<20000xi16>
        // CHECK: %{{.*}} = tensor.insert_slice %{{.*}} into %{{.*}}[1] [20000] [1] : tensor<20000xi16> into tensor<20001xi16>
        %res3_si16 = vosa.export_channel %c1_si16, %d_20001_si16 { stride = 2 : i32, offset = 2 : i32 } : tensor<100x200x1xsi16>, tensor<20001xsi16> -> tensor<20001xsi16>

        %c1_f32 = vosa.constant dense<42.0> : tensor<100x200x1xf32>
        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 20000>} : (tensor<100x200x1xf32>) -> tensor<20000xf32>
        %res1_f32 = vosa.export_channel %c1_f32, %d_20000_f32 { stride = 4 : i32, offset = 0 : i32 } : tensor<100x200x1xf32>, tensor<20000xf32> -> tensor<20000xf32>

        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 20000>} : (tensor<100x200x1xf32>) -> tensor<20000xf32>
        // CHECK: %{{.*}} = tensor.insert_slice %{{.*}} into %{{.*}}[0] [20000] [2] : tensor<20000xf32> into tensor<40000xf32>
        %res2_f32 = vosa.export_channel %c1_f32, %d_40000_f32 { stride = 8 : i32, offset = 0 : i32 } : tensor<100x200x1xf32>, tensor<40000xf32> -> tensor<40000xf32>

        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 20000>} : (tensor<100x200x1xf32>) -> tensor<20000xf32>
        // CHECK: %{{.*}} = tensor.insert_slice %{{.*}} into %{{.*}}[1] [20000] [1] : tensor<20000xf32> into tensor<20001xf32>
        %res3_f32 = vosa.export_channel %c1_f32, %d_20001_f32 { stride = 4 : i32, offset = 4 : i32 } : tensor<100x200x1xf32>, tensor<20001xf32> -> tensor<20001xf32>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @import_channel({{.*}})
    func.func @import_channel(%raw_si8: tensor<?xsi8>) {
        %c1_si8 = vosa.constant dense<1> : tensor<20000xsi8>
        // CHECK: %{{.*}} = tensor.extract_slice %{{.*}}[0] [20000] [1] : tensor<20000xi8> to tensor<20000xi8>
        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 100, 200, 1>} : (tensor<20000xi8>) -> tensor<100x200x1xi8>
        %res1_si8 = vosa.import_channel %c1_si8 { stride = 1 : i32, offset = 0 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<20000xsi8> -> tensor<100x200x1xsi8>

        %c2_si8 = vosa.constant dense<1> : tensor<40000xsi8>
        // CHECK: %{{.*}} = tensor.extract_slice %{{.*}}[0] [20000] [2] : tensor<40000xi8> to tensor<20000xi8>
        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 100, 200, 1>} : (tensor<20000xi8>) -> tensor<100x200x1xi8>
        %res2_si8 = vosa.import_channel %c2_si8 { stride = 2 : i32, offset = 0 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<40000xsi8> -> tensor<100x200x1xsi8>

        %c3_si8 = vosa.constant dense<1> : tensor<20001xsi8>
        // CHECK: %{{.*}} = tensor.extract_slice %{{.*}}[1] [20000] [1] : tensor<20001xi8> to tensor<20000xi8>
        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 100, 200, 1>} : (tensor<20000xi8>) -> tensor<100x200x1xi8>
        %res3_si8 = vosa.import_channel %c3_si8 { stride = 1 : i32, offset = 1 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<20001xsi8> -> tensor<100x200x1xsi8>

        // CHECK: %{{.*}} = tensor.extract_slice %{{.*}}[1] [20000] [2] : tensor<40000xi8> to tensor<20000xi8>
        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 100, 200, 1>} : (tensor<20000xi8>) -> tensor<100x200x1xi8>
        %res4_si8 = vosa.import_channel %c2_si8 { stride = 2 : i32, offset = 1 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<40000xsi8> -> tensor<100x200x1xsi8>

        // CHECK: %{{.*}} = tensor.extract_slice %{{.*}}[0] [20000] [1] : tensor<?xi8> to tensor<20000xi8>
        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 100, 200, 1>} : (tensor<20000xi8>) -> tensor<100x200x1xi8>
        %res5_si8 = vosa.import_channel %raw_si8 { stride = 1 : i32, offset = 0 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<?xsi8> -> tensor<100x200x1xsi8>

        %c1_si16 = vosa.constant dense<1> : tensor<20000xsi16>
        // CHECK: %{{.*}} = tensor.extract_slice %{{.*}}[0] [20000] [1] : tensor<20000xi16> to tensor<20000xi16>
        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 100, 200, 1>} : (tensor<20000xi16>) -> tensor<100x200x1xi16>
        %res1_si16 = vosa.import_channel %c1_si16 { stride = 2 : i32, offset = 0 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<20000xsi16> -> tensor<100x200x1xsi16>

        %c2_si16 = vosa.constant dense<1> : tensor<40000xsi16>
        // CHECK: %{{.*}} = tensor.extract_slice %{{.*}}[0] [20000] [2] : tensor<40000xi16> to tensor<20000xi16>
        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 100, 200, 1>} : (tensor<20000xi16>) -> tensor<100x200x1xi16>
        %res2_si16 = vosa.import_channel %c2_si16 { stride = 4 : i32, offset = 0 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<40000xsi16> -> tensor<100x200x1xsi16>

        %c3_si16 = vosa.constant dense<1> : tensor<20001xsi16>
        // CHECK: %{{.*}} = tensor.extract_slice %{{.*}}[1] [20000] [1] : tensor<20001xi16> to tensor<20000xi16>
        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 100, 200, 1>} : (tensor<20000xi16>) -> tensor<100x200x1xi16>
        %res3_si16 = vosa.import_channel %c3_si16 { stride = 2 : i32, offset = 2 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<20001xsi16> -> tensor<100x200x1xsi16>

        %c1_f32 = vosa.constant dense<1.0> : tensor<20000xf32>
        // CHECK: %{{.*}} = tensor.extract_slice %{{.*}}[0] [20000] [1] : tensor<20000xf32> to tensor<20000xf32>
        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 100, 200, 1>} : (tensor<20000xf32>) -> tensor<100x200x1xf32>
        %res1_f32 = vosa.import_channel %c1_f32 { stride = 4 : i32, offset = 0 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<20000xf32> -> tensor<100x200x1xf32>

        %c2_f32 = vosa.constant dense<1.0> : tensor<40000xf32>
        // CHECK: %{{.*}} = tensor.extract_slice %{{.*}}[0] [20000] [2] : tensor<40000xf32> to tensor<20000xf32>
        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 100, 200, 1>} : (tensor<20000xf32>) -> tensor<100x200x1xf32>
        %res2_f32 = vosa.import_channel %c2_f32 { stride = 8 : i32, offset = 0 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<40000xf32> -> tensor<100x200x1xf32>

        %c3_f32 = vosa.constant dense<1.0> : tensor<20001xf32>
        // CHECK: %{{.*}} = tensor.extract_slice %{{.*}}[1] [20000] [1] : tensor<20001xf32> to tensor<20000xf32>
        // CHECK: %{{.*}} = tosa.reshape %{{.*}} {new_shape = array<i64: 100, 200, 1>} : (tensor<20000xf32>) -> tensor<100x200x1xf32>
        %res3_f32 = vosa.import_channel %c3_f32 { stride = 4 : i32, offset = 4 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<20001xf32> -> tensor<100x200x1xf32>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @mesh_grid()
    func.func @mesh_grid() {
        // CHECK: %{{.*}} = tensor.empty() : tensor<100x1x1xi32>
        // CHECK: %{{.*}} = linalg.generic
        // CHECK: %{{.*}} = linalg.index
        // CHECK: %{{.*}} = arith.index_cast
        // CHECK: linalg.yield
        // CHECK: %{{.*}} = tosa.tile %{{.*}} {multiples = array<i64: 1, 200, 1>} : (tensor<100x1x1xi32>) -> tensor<100x200x1xi32>
        // CHECK: %{{.*}} = tensor.empty() : tensor<1x200x1xi32>
        // CHECK: %{{.*}} = linalg.generic
        // CHECK: %{{.*}} = linalg.index
        // CHECK: %{{.*}} = arith.index_cast
        // CHECK: linalg.yield
        // CHECK: %{{.*}} = tosa.tile %{{.*}} {multiples = array<i64: 100, 1, 1>} : (tensor<1x200x1xi32>) -> tensor<100x200x1xi32>
        // CHECK: %{{.*}} = tosa.concat {{.*}} {axis = 2 : i32} : (tensor<100x200x1xi32>, tensor<100x200x1xi32>) -> tensor<100x200x2xi32>
        %res = vosa.mesh_grid {shape = [ 100 : i32, 200 : i32 ]} : tensor<100x200x2xui32>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @add()
    func.func @add() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // CHECK: %{{.*}} = tosa.add %{{.*}}, %{{.*}} : (tensor<100x200x3xi8>, tensor<100x200x3xi8>) -> tensor<100x200x3xi8>
        %res0 = vosa.add %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @div()
    func.func @div() {
        %t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        %t1_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>

        // CHECK: tensor.empty
        // CHECK: linalg.generic
        // CHECK: arith.divf
        // CHECK: linalg.yield
        %res_f16 = vosa.div %t0_f16, %t1_f16 : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xf16>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @sub()
    func.func @sub() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // CHECK: %{{.*}} = tosa.sub %{{.*}}, %{{.*}} : (tensor<100x200x3xi8>, tensor<100x200x3xi8>) -> tensor<100x200x3xi8>
        %res0 = vosa.sub %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @mult()
    func.func @mult() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // CHECK: %{{.*}} = tosa.mul %{{.*}}, %{{.*}} {shift = 0 : i8} : (tensor<100x200x3xi8>, tensor<100x200x3xi8>) -> tensor<100x200x3xi8>
        %res0 = vosa.mult %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @and()
    func.func @and() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        // CHECK: %{{.*}} = tosa.bitwise_and %{{.*}}, %{{.*}} : (tensor<100x200x3xi8>, tensor<100x200x3xi8>) -> tensor<100x200x3xi8>
        %res_ui8 = vosa.and %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @max()
    func.func @max() {
	%t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	%t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	// CHECK: tosa.maximum
	%res_ui8 = vosa.max %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>
	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @min()
    func.func @min() {
	%t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	%t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	// CHECK: tosa.minimum
	%res_ui8 = vosa.min %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>
	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @abs_diff()
    func.func @abs_diff() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        
        // CHECK: %{{.*}} = tosa.sub %{{.*}}, %{{.*}} : (tensor<100x200x3xi8>, tensor<100x200x3xi8>) -> tensor<100x200x3xi8>
        // CHECK: %{{.*}} = tosa.abs %{{.*}} : (tensor<100x200x3xi8>) -> tensor<100x200x3xi8>
        %res0 = vosa.abs_diff %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @where()
    func.func @where() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t2_bool = vosa.constant dense<true> : tensor<100x200x3xi1>

        // CHECK: %{{.*}} = tosa.select %{{.*}}, %{{.*}}, %{{.*}} : (tensor<100x200x3xi1>, tensor<100x200x3xi8>, tensor<100x200x3xi8>) -> tensor<100x200x3xi8>
        %res = vosa.where %t0_ui8, %t1_ui8, %t2_bool : tensor<100x200x3xui8>, tensor<100x200x3xui8>, tensor<100x200x3xi1> -> tensor<100x200x3xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @or()
    func.func @or() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>

        // CHECK: tosa.bitwise_or
        %res = vosa.or %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @not()
    func.func @not() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>

        // CHECK: tosa.bitwise_not
        %res = vosa.not %t0_ui8 : tensor<100x200x3xui8> -> tensor<100x200x3xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @round()
    func.func @round() {
        %t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>

        // CHECK: tensor.empty
        // CHECK: linalg.generic
        // CHECK: %[[F:.*]] = math.floor %in
        // CHECK: %[[I:.*]] = arith.fptosi %[[F]]
        // CHECK: linalg.yield %[[I]]
        %res1 = vosa.round %t0_f32 floor : tensor<100x200x3xf32> -> tensor<100x200x3xsi32>

        // CHECK: tensor.empty
        // CHECK: linalg.generic
        // CHECK: %[[F:.*]] = math.round %in
        // CHECK: %[[I:.*]] = arith.fptosi %[[F]]
        // CHECK: linalg.yield %[[I]]
        %res2 = vosa.round %t0_f32 half_away_from_zero : tensor<100x200x3xf32> -> tensor<100x200x3xsi32>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @sqrt()
    func.func @sqrt() {
        %t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>

        // CHECK: tensor.empty
        // CHECK: math.sqrt
        // CHECK: linalg.yield
	%res = vosa.sqrt %t0_f16 : tensor<100x200x3xf16> -> tensor<100x200x3xf16>

	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @gamma_correction()
    func.func @gamma_correction() {
	%t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
	
	// CHECK: tensor.empty
	// CHECK: tosa.pow
	%res = vosa.gamma_correction %t0_f16 { gamma = 2.2 : f16 } : tensor<100x200x3xf16> -> tensor<100x200x3xf16>

	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @power()
    func.func @power() {
	%t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>

	// CHECK: tensor.empty
	// CHECK: tosa.pow
	%res = vosa.power %t0_f16 { base = 2.0 : f16 } : tensor<100x200x3xf16> -> tensor<100x200x3xf16>

	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @equal()
    func.func @equal() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>

        // CHECK: tosa.equal
        %res = vosa.equal %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xi1>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @greater()
    func.func @greater() {
	%t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	%t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	
	// CHECK: tosa.greater
	%res = vosa.greater %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xi1>
	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @greater_equal()
    func.func @greater_equal() {
	%t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	%t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	
	// CHECK: tosa.greater_equal
	%res = vosa.greater_equal %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xi1>
	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @abs()
    func.func @abs() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // CHECK: %{{.*}} = tosa.abs %{{.*}} : (tensor<100x200x3xi8>) -> tensor<100x200x3xi8>
        %res0 = vosa.abs %t0_si8 : tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @arithmetic_shift_right()
    func.func @arithmetic_shift_right() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // CHECK: %{{.*}} = tensor.empty() : tensor<1x1x1xi8>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: i8) outs(%{{.*}}: tensor<1x1x1xi8>) -> tensor<1x1x1xi8>
        // CHECK: %{{.*}} = tosa.arithmetic_right_shift %{{.*}}, %{{.*}} {round = false} : (tensor<100x200x3xi8>, tensor<1x1x1xi8>) -> tensor<100x200x3xi8>
        %res0 = vosa.arithmetic_shift_right %t0_si8 { shift = 4 : i32 } : tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @logical_shift_right()
    func.func @logical_shift_right() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        // CHECK: %{{.*}} = tensor.empty() : tensor<1x1x1xi8>
        // CHECK: %{{.*}} = linalg.fill ins(%{{.*}}: i8) outs(%{{.*}}: tensor<1x1x1xi8>) -> tensor<1x1x1xi8>
        // CHECK: %{{.*}} = tosa.logical_right_shift %{{.*}}, %{{.*}} : (tensor<100x200x3xi8>, tensor<1x1x1xi8>) -> tensor<100x200x3xi8>
        %res0 = vosa.logical_shift_right %t0_ui8 { shift = 4 : i32 } : tensor<100x200x3xui8> -> tensor<100x200x3xui8>
        return
    }
}

// -----

// CHECK: #map = affine_map<(d0, d1, d2) -> (d0, d1, d2)>
module {
    // CHECK-LABEL: func.func @clamp()
    func.func @clamp() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %min_si8 = vosa.constant -10 : si8
        %max_si8 = vosa.constant 100 : si8
        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x3xi8>
        // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map], iterator_types = ["parallel", "parallel", "parallel"]} ins(%{{.*}} : tensor<100x200x3xi8>) outs(%{{.*}} : tensor<100x200x3xi8>) {
        // CHECK: cmpi slt
        // CHECK: select
        // CHECK: cmpi slt
        // CHECK: select
        // CHECK: linalg.yield
        %res0 = vosa.clamp %t0_si8, %min_si8, %max_si8 : tensor<100x200x3xsi8>, si8, si8 -> tensor<100x200x3xsi8>

        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %min_ui8 = vosa.constant 10 : ui8
        %max_ui8 = vosa.constant 100 : ui8
        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x3xi8>
        // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map], iterator_types = ["parallel", "parallel", "parallel"]} ins(%{{.*}} : tensor<100x200x3xi8>) outs(%{{.*}} : tensor<100x200x3xi8>) {
        // CHECK: cmpi ult
        // CHECK: select
        // CHECK: cmpi ult
        // CHECK: select
        // CHECK: linalg.yield
        %res1 = vosa.clamp %t0_ui8, %min_ui8, %max_ui8 : tensor<100x200x3xui8>, ui8, ui8 -> tensor<100x200x3xui8>

        %t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        %min_f32 = vosa.constant 10.0 : f32
        %max_f32 = vosa.constant 100.0 : f32
        // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x3xf32>
        // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map], iterator_types = ["parallel", "parallel", "parallel"]} ins(%{{.*}} : tensor<100x200x3xf32>) outs(%{{.*}} : tensor<100x200x3xf32>) {
        // CHECK: cmpf
        // CHECK: select
        // CHECK: cmpf
        // CHECK: select
        // CHECK: linalg.yield
        %res2 = vosa.clamp %t0_f32, %min_f32, %max_f32 : tensor<100x200x3xf32>, f32, f32 -> tensor<100x200x3xf32>
        return
    }
}

// -----

module {
    func.func @conv_2d_int() {
        %t_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // CHECK: %{{.*}} = linalg.depthwise_conv_2d_nhwc_hwc {dilations = dense<1> : tensor<2xi64>, strides = dense<1> : tensor<2xi64>} ins(%{{.*}}, %{{.*}} : tensor<1x100x200x3xi8>, tensor<3x3x3xi8>) outs(%{{.*}} : tensor<1x98x198x3xi8>) -> tensor<1x98x198x3xi8>
        %res_si8 = vosa.conv_2d %t_si8 { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi8> } : tensor<100x200x3xsi8> -> tensor<98x198x3xsi8>
        return
    }

    func.func @conv_2d_float() {

        %t_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        // CHECK: %{{.*}} = linalg.depthwise_conv_2d_nhwc_hwc {dilations = dense<1> : tensor<2xi64>, strides = dense<1> : tensor<2xi64>} ins(%{{.*}}, %{{.*}} : tensor<1x100x200x3xf32>, tensor<3x3x3xf32>) outs(%{{.*}} : tensor<1x98x198x3xf32>) -> tensor<1x98x198x3xf32>
        %res_f32 = vosa.conv_2d %t_f32 { filter = dense<[[0.125, 0.25, 0.125], [0.25, 1.0, 0.25], [0.125, 0.25, 0.125]]> : tensor<3x3xf32> } : tensor<100x200x3xf32> -> tensor<98x198x3xf32>
        return
    }
}

// -----

module {
    func.func @decimate() {
        %t_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // CHECK: tensor.extract_slice %{{.*}}[1, 3, 0] [50, 50, 3] [2, 4, 1] : tensor<100x200x3xi8> to tensor<50x50x3xi8>
        %res0 = vosa.decimate %t_si8 { N = [ 2 : i32, 4 : i32 ], offsets = [ 1 : i32, 3 : i32 ] } : tensor<100x200x3xsi8> -> tensor<50x50x3xsi8>
        return
    }
}


// -----

// CHECK-LABEL: func.func @pad_nop()
func.func @pad_nop() -> tensor<100x200x3xsi8> {
    // constant pad for si32, f16
    // CHECK: %[[IN:.*]] = linalg.fill
    %t_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>

    // nothing to pad - forward input value
    %res0 = vosa.pad %t_si8 constant { pad_constant = 17 : si8, pad_size = [ 0 : i32, 0 : i32, 0 : i32, 0 : i32 ] } : tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>

    // CHECK: return %[[V:.*]] : tensor<100x200x3xsi8>
    return %res0 : tensor<100x200x3xsi8>
}

// -----

// CHECK-LABEL: func.func @pad_constant()
func.func @pad_constant() {
    // constant pad for si32, f16
    %t_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
    // expect constant blocks above, left, right, below input
    // CHECK: %[[V:.*]] = arith.constant 17 : i8
    // CHECK: %[[R1:.*]] = linalg.fill ins(%[[V]] : i8) {{.*}} -> tensor<1x212x3xi8>
    // CHECK: %[[L:.*]] = linalg.fill ins(%[[V]] : i8) {{.*}} -> tensor<100x4x3xi8>
    // CHECK: %[[R:.*]] = linalg.fill ins(%[[V]] : i8) {{.*}} -> tensor<100x8x3xi8>
    // CHECK: %[[R2:.*]] = tosa.concat %[[L]], %{{.*}}, %[[R]] {axis = 1 : i32} : (tensor<100x4x3xi8>, tensor<100x200x3xi8>, tensor<100x8x3xi8>) -> tensor<100x212x3xi8>
    // CHECK: %[[R3:.*]] = linalg.fill ins(%[[V]] : i8) {{.*}} -> tensor<2x212x3xi8>
    // CHECK: %{{.*}} = tosa.concat %[[R1]], %[[R2]], %[[R3]] {axis = 0 : i32} : (tensor<1x212x3xi8>, tensor<100x212x3xi8>, tensor<2x212x3xi8>) -> tensor<103x212x3xi8>
    %res0 = vosa.pad %t_si8 constant { pad_constant = 17 : si8, pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<100x200x3xsi8> -> tensor<103x212x3xsi8>

    return
}

// -----

// CHECK-LABEL: func.func @pad_replicate()
func.func @pad_replicate() {
    // constant pad for si32, f16
    %t_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
    // generate 8 blocks around input
    // CHECK: %[[TL:.*]] = linalg.generic
    // CHECK: tensor.extract
    // CHECK: } -> tensor<1x4x3xi8>
    // CHECK: %[[T:.*]] = linalg.generic
    // CHECK: tensor.extract
    // CHECK: } -> tensor<1x200x3xi8>
    // CHECK: %[[TR:.*]] = linalg.generic
    // CHECK: tensor.extract
    // CHECK: } -> tensor<1x8x3xi8>
    // CHECK: %[[R1:.*]] = tosa.concat %[[TL]], %[[T]], %[[TR]] {axis = 1 : i32} : (tensor<1x4x3xi8>, tensor<1x200x3xi8>, tensor<1x8x3xi8>) -> tensor<1x212x3xi8>
    // CHECK: %[[L:.*]] = linalg.generic
    // CHECK: tensor.extract
    // CHECK: } -> tensor<100x4x3xi8>
    // CHECK: %[[R:.*]] = linalg.generic
    // CHECK: tensor.extract
    // CHECK: } -> tensor<100x8x3xi8>
    // CHECK: %[[R2:.*]] = tosa.concat %[[L]], %{{.*}}, %[[R]] {axis = 1 : i32} : (tensor<100x4x3xi8>, tensor<100x200x3xi8>, tensor<100x8x3xi8>) -> tensor<100x212x3xi8>
    // CHECK: %[[BL:.*]] = linalg.generic
    // CHECK: tensor.extract
    // CHECK: } -> tensor<2x4x3xi8>
    // CHECK: %[[B:.*]] = linalg.generic
    // CHECK: tensor.extract
    // CHECK: } -> tensor<2x200x3xi8>
    // CHECK: %[[BR:.*]] = linalg.generic
    // CHECK: tensor.extract
    // CHECK: } -> tensor<2x8x3xi8>
    // CHECK: %[[R3:.*]] = tosa.concat %[[BL]], %[[B]], %[[BR]] {axis = 1 : i32} : (tensor<2x4x3xi8>, tensor<2x200x3xi8>, tensor<2x8x3xi8>) -> tensor<2x212x3xi8>
    // CHECK: %{{.*}} = tosa.concat %[[R1]], %[[R2]], %[[R3]] {axis = 0 : i32} : (tensor<1x212x3xi8>, tensor<100x212x3xi8>, tensor<2x212x3xi8>) -> tensor<103x212x3xi8>
    %res0 = vosa.pad %t_si8 replicate { pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<100x200x3xsi8> -> tensor<103x212x3xsi8>

    return
}

// -----

//CHECK-LABEL: func.func @resize_nearest_neighbour()
func.func @resize_nearest_neighbour() {
    %t_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
    // CHECK: %{{.*}} = tosa.resize %{{.*}} {border = array<i64: 0, 0>, mode = "NEAREST_NEIGHBOR", offset = array<i64: -25, -50>, scale = array<i64: 150, 100, 300, 200>} : (tensor<1x100x200x3xf32>) -> tensor<1x150x300x3xf32>
    %res0 = vosa.resize_nearest_neighbour %t_f32 { size = [ 150 : i32, 300 : i32] } : tensor<100x200x3xf32> -> tensor<150x300x3xf32>
 
    return
}


// -----


// CHECK-LABEL: func.func @resize_bilinear()
func.func @resize_bilinear() {
    %t_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
    // CHECK: %{{.*}} = tosa.resize %{{.*}} {border = array<i64: 0, 0>, mode = "BILINEAR", offset = array<i64: -25, -50>, scale = array<i64: 150, 100, 300, 200>} : (tensor<1x100x200x3xf16>) -> tensor<1x150x300x3xf16>
    %res0 = vosa.resize_bilinear %t_f16 { size = [ 150 : i32, 300 : i32 ] } : tensor<100x200x3xf16> -> tensor<150x300x3xf16>

    %t_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
    // CHECK: %{{.*}} = tosa.resize %{{.*}}  {border = array<i64: 0, 0>, mode = "BILINEAR", offset = array<i64: -25, -50>, scale = array<i64: 150, 100, 300, 200>} : (tensor<1x100x200x3xf32>) -> tensor<1x150x300x3xf32>
    %res1 = vosa.resize_bilinear %t_f32 { size = [ 150 : i32, 300 : i32 ] } : tensor<100x200x3xf32> -> tensor<150x300x3xf32>

    return
}

// -----

// CHECK-LABEL: func.func @arg_max_channelwise({{.*}})
func.func @arg_max_channelwise(%t_si32 : tensor<100x200x3xsi32>, %t_ui16 : tensor<100x200x3xui16>, %t_f32 : tensor<100x200x3xf32>) {

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi32>
    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xi32>) outs(%{{.*}} : tensor<100x200x1xi32>, tensor<100x200x1xi32>) {
    // CHECK: %[[C:.*]] = linalg.index 2
    // CHECK: %[[CI:.*]] = arith.index_cast %[[C]]
    // CHECK: cmpi sgt
    // CHECK: select %{{.*}}, %[[CI]]
    // CHECK: select
    // CHECK: linalg.yield
    %res_si32 = vosa.arg_max_channelwise %t_si32 : tensor<100x200x3xsi32> -> tensor<100x200x1xui32>

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi16>
    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xi16>) outs(%{{.*}} : tensor<100x200x1xi32>, tensor<100x200x1xi16>) {
    // CHECK: linalg.index 2
    // CHECK: cmpi ugt
    // CHECK: select
    // CHECK: select
    // CHECK: linalg.yield
    %res_ui32 = vosa.arg_max_channelwise %t_ui16 : tensor<100x200x3xui16> -> tensor<100x200x1xui32>

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xf32>
    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xf32>) outs(%{{.*}} : tensor<100x200x1xi32>, tensor<100x200x1xf32>) {
    // CHECK: linalg.index 2
    // CHECK: cmpf ogt
    // CHECK: select
    // CHECK: select
    // CHECK: linalg.yield
    %res_f32 = vosa.arg_max_channelwise %t_f32 : tensor<100x200x3xf32> -> tensor<100x200x1xui32>

    return
}

// -----

// CHECK-LABEL: func.func @arg_max_planewise({{.*}})
func.func @arg_max_planewise(%t_si8: tensor<100x200x1xsi8>, %t_si32: tensor<100x200x1xsi32>, %t_ui8: tensor<100x200x1xui8>, %t_f32: tensor<100x200x1xf32>) {

    // CHECK: %{{.*}} = arith.constant -128 : i8
    // CHECK: %{{.*}} = tensor.empty() : tensor<i8>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1, #map1, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xi8>) outs(%{{.*}} : tensor<i32>, tensor<i32>, tensor<i8>) {
    // CHECK: %[[Y:.*]] = linalg.index 0
    // CHECK: %[[YI:.*]] = arith.index_cast %[[Y]]
    // CHECK: %[[X:.*]] = linalg.index 1
    // CHECK: %[[XI:.*]] = arith.index_cast %[[X]]
    // CHECK: cmpi sgt
    // CHECK: select %{{.*}}, %[[YI]]
    // CHECK: select %{{.*}}, %[[XI]]
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.from_elements %{{.*}} : tensor<2xi32>
    %res0 = vosa.arg_max_planewise %t_si8 : tensor<100x200x1xsi8> -> tensor<2xui32>

    // CHECK: %{{.*}} = arith.constant -2147483648 : i32
    // CHECK: %{{.*}} = tensor.empty() : tensor<i32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1, #map1, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xi32>) outs(%{{.*}} : tensor<i32>, tensor<i32>, tensor<i32>) {
    // CHECK: linalg.index 0
    // CHECK: linalg.index 1
    // CHECK: cmpi sgt
    // CHECK: select
    // CHECK: select
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.from_elements %{{.*}} : tensor<2xi32>
    %res1 = vosa.arg_max_planewise %t_si32 : tensor<100x200x1xsi32> -> tensor<2xui32>

    // CHECK: %{{.*}} = arith.constant 0 : i8
    // CHECK: %{{.*}} = tensor.empty() : tensor<i8>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1, #map1, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xi8>) outs(%{{.*}} : tensor<i32>, tensor<i32>, tensor<i8>) {
    // CHECK: linalg.index 0
    // CHECK: linalg.index 1
    // CHECK: cmpi ugt
    // CHECK: select
    // CHECK: select
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.from_elements %{{.*}} : tensor<2xi32>
    %res_ui8 = vosa.arg_max_planewise %t_ui8 : tensor<100x200x1xui8> -> tensor<2xui32>

    // CHECK: %{{.*}} = arith.constant -3.{{.*}}E+38 : f32
    // CHECK: %{{.*}} = tensor.empty() : tensor<f32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1, #map1, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xf32>) outs(%{{.*}} : tensor<i32>, tensor<i32>, tensor<f32>) {
    // CHECK: linalg.index 0
    // CHECK: linalg.index 1
    // CHECK: cmpf ogt
    // CHECK: select
    // CHECK: select
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.from_elements %{{.*}} : tensor<2xi32>
    %res3 = vosa.arg_max_planewise %t_f32 : tensor<100x200x1xf32> -> tensor<2xui32>

    return
}

// -----

// CHECK-LABEL: func.func @arg_min_channelwise({{.*}})
func.func @arg_min_channelwise(%t_si32 : tensor<100x200x3xsi32>, %t_ui16 : tensor<100x200x3xui16>, %t_f32 : tensor<100x200x3xf32>) {

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi32>
    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xi32>) outs(%{{.*}} : tensor<100x200x1xi32>, tensor<100x200x1xi32>) {
    // CHECK: linalg.index 2
    // CHECK: cmpi slt
    // CHECK: select
    // CHECK: select
    // CHECK: linalg.yield
    %res_si32 = vosa.arg_min_channelwise %t_si32 : tensor<100x200x3xsi32> -> tensor<100x200x1xui32>

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi16>
    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xi16>) outs(%{{.*}} : tensor<100x200x1xi32>, tensor<100x200x1xi16>) {
    // CHECK: linalg.index 2
    // CHECK: cmpi ult
    // CHECK: select
    // CHECK: select
    // CHECK: linalg.yield
    %res_ui32 = vosa.arg_min_channelwise %t_ui16 : tensor<100x200x3xui16> -> tensor<100x200x1xui32>

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xf32>
    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xf32>) outs(%{{.*}} : tensor<100x200x1xi32>, tensor<100x200x1xf32>) {
    // CHECK: linalg.index 2
    // CHECK: cmpf
    // CHECK: select
    // CHECK: select
    // CHECK: linalg.yield
    %res_f32 = vosa.arg_min_channelwise %t_f32 : tensor<100x200x3xf32> -> tensor<100x200x1xui32>

    return
}

// -----

// CHECK-LABEL: func.func @arg_min_planewise({{.*}})
func.func @arg_min_planewise(%t_si8: tensor<100x200x1xsi8>, %t_si32: tensor<100x200x1xsi32>, %t_ui8: tensor<100x200x1xui8>, %t_f32: tensor<100x200x1xf32>) {

    // CHECK: %{{.*}} = arith.constant 127 : i8
    // CHECK: %{{.*}} = tensor.empty() : tensor<i8>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1, #map1, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xi8>) outs(%{{.*}} : tensor<i32>, tensor<i32>, tensor<i8>) {
    // CHECK: linalg.index 0
    // CHECK: linalg.index 1
    // CHECK: cmpi slt
    // CHECK: select
    // CHECK: select
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.from_elements %{{.*}} : tensor<2xi32>
    %res0 = vosa.arg_min_planewise %t_si8 : tensor<100x200x1xsi8> -> tensor<2xui32>

    // CHECK: %{{.*}} = arith.constant 2147483647 : i32
    // CHECK: %{{.*}} = tensor.empty() : tensor<i32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1, #map1, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xi32>) outs(%{{.*}} : tensor<i32>, tensor<i32>, tensor<i32>) {
    // CHECK: linalg.index 0
    // CHECK: linalg.index 1
    // CHECK: cmpi slt
    // CHECK: select
    // CHECK: select
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.from_elements %{{.*}} : tensor<2xi32>
    %res1 = vosa.arg_min_planewise %t_si32 : tensor<100x200x1xsi32> -> tensor<2xui32>

    // CHECK: %{{.*}} = arith.constant -1 : i8
    // CHECK: %{{.*}} = tensor.empty() : tensor<i8>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1, #map1, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xi8>) outs(%{{.*}} : tensor<i32>, tensor<i32>, tensor<i8>) {
    // CHECK: linalg.index 0
    // CHECK: linalg.index 1
    // CHECK: cmpi ult
    // CHECK: select
    // CHECK: select
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.from_elements %{{.*}} : tensor<2xi32>
    %res_ui8 = vosa.arg_min_planewise %t_ui8 : tensor<100x200x1xui8> -> tensor<2xui32>

    // CHECK: %{{.*}} = arith.constant 3.{{.*}}E+38 : f32
    // CHECK: %{{.*}} = tensor.empty() : tensor<f32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1, #map1, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xf32>) outs(%{{.*}} : tensor<i32>, tensor<i32>, tensor<f32>) {
    // CHECK: linalg.index 0
    // CHECK: linalg.index 1
    // CHECK: cmpf
    // CHECK: select
    // CHECK: select
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.extract %{{.*}}[] : tensor<i32>
    // CHECK: tensor.from_elements %{{.*}} : tensor<2xi32>
    %res3 = vosa.arg_min_planewise %t_f32 : tensor<100x200x1xf32> -> tensor<2xui32>

    return
}

// -----

// CHECK-LABEL: func.func @reduce_interp_channelwise({{.*}})
func.func @reduce_interp_channelwise(%t_si32 : tensor<100x200x3xsi32>, %t_ui32 : tensor<100x200x3xui32>, %t_f32 : tensor<100x200x3xf32>, %s : tensor<100x200x1xf32>) {

    // CHECK: tensor.empty() : tensor<100x200x1xf32>
    // CHECK: tosa.slice %{{.*}}{size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 0>} : (tensor<100x200x3xi32>) -> tensor<100x200x1xi32> 
    // CHECK: tosa.slice %{{.*}}{size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 1>} : (tensor<100x200x3xi32>) -> tensor<100x200x1xi32> 
    // CHECK: tosa.slice %{{.*}}{size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 2>} : (tensor<100x200x3xi32>) -> tensor<100x200x1xi32> 
    // CHECK: tosa.slice %{{.*}}{size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 0>} : (tensor<100x200x1xf32>) -> tensor<100x200x1xf32> 
    // CHECK: tensor.empty() : tensor<100x200x1xf32> 
    // CHECK: linalg.generic {indexing_maps = [#map, #map, #map, #map, #map], iterator_types = ["parallel", "parallel", "parallel"]} ins({{.*}}, {{.*}}, {{.*}}, {{.*}}  : tensor<100x200x1xi32>, tensor<100x200x1xi32>, tensor<100x200x1xi32>, tensor<100x200x1xf32>) outs({{.*}} : tensor<100x200x1xf32>) { 
    // CHECK: ^bb0({{.*}}: i32, {{.*}}: i32, {{.*}}: i32, {{.*}}: f32, {{.*}}: f32):
    // CHECK: arith.constant 0
    // CHECK: arith.constant 1
    // CHECK: arith.constant 1.000000e+00
    // CHECK: arith.constant 2
    // CHECK: math.floor
    // CHECK: arith.fptosi
    // CHECK: arith.addi
    // CHECK: arith.cmpi
    // CHECK: arith.cmpi
    // CHECK: arith.select
    // CHECK: arith.select
    // CHECK: arith.cmpi
    // CHECK: arith.cmpi
    // CHECK: arith.select
    // CHECK: arith.select
    // CHECK: arith.addf
    // CHECK: arith.subf
    // CHECK: arith.subf
    // CHECK: arith.sitofp
    // CHECK: arith.sitofp
    // CHECK: arith.sitofp
    // CHECK: arith.constant 1
    // CHECK: arith.cmpi
    // CHECK: arith.select
    // CHECK: arith.cmpi
    // CHECK: arith.select
    // CHECK: arith.sitofp
    // CHECK: arith.constant
    // CHECK: arith.cmpi
    // CHECK: arith.select
    // CHECK: arith.cmpi
    // CHECK: arith.select
    // CHECK: arith.mulf
    // CHECK: arith.mulf
    // CHECK: arith.addf
    // CHECK: linalg.yield
    %res_si32 = vosa.reduce_interp_channelwise %t_si32, %s : tensor<100x200x3xsi32>, tensor<100x200x1xf32> -> tensor<100x200x1xf32>

    // CHECK: tensor.empty() : tensor<100x200x1xf32>
    // CHECK: tosa.slice %{{.*}}{size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 0>} : (tensor<100x200x3xi32>) -> tensor<100x200x1xi32> 
    // CHECK: tosa.slice %{{.*}}{size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 1>} : (tensor<100x200x3xi32>) -> tensor<100x200x1xi32> 
    // CHECK: tosa.slice %{{.*}}{size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 2>} : (tensor<100x200x3xi32>) -> tensor<100x200x1xi32> 
    // CHECK: tosa.slice %{{.*}}{size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 0>} : (tensor<100x200x1xf32>) -> tensor<100x200x1xf32> 
    // CHECK: tensor.empty() : tensor<100x200x1xf32> 
    // CHECK: linalg.generic {indexing_maps = [#map, #map, #map, #map, #map], iterator_types = ["parallel", "parallel", "parallel"]} ins({{.*}}, {{.*}}, {{.*}}, {{.*}}  : tensor<100x200x1xi32>, tensor<100x200x1xi32>, tensor<100x200x1xi32>, tensor<100x200x1xf32>) outs({{.*}} : tensor<100x200x1xf32>) { 
    // CHECK: ^bb0({{.*}}: i32, {{.*}}: i32, {{.*}}: i32, {{.*}}: f32, {{.*}}: f32):
    // CHECK: arith.constant 0
    // CHECK: arith.constant 1
    // CHECK: arith.constant 1.000000e+00
    // CHECK: arith.constant 2
    // CHECK: math.floor
    // CHECK: arith.fptosi
    // CHECK: arith.addi
    // CHECK: arith.cmpi slt
    // CHECK: arith.cmpi slt
    // CHECK: arith.select
    // CHECK: arith.select
    // CHECK: arith.cmpi slt
    // CHECK: arith.cmpi slt
    // CHECK: arith.select
    // CHECK: arith.select
    // CHECK: arith.addf
    // CHECK: arith.subf
    // CHECK: arith.subf
    // CHECK: arith.uitofp
    // CHECK: arith.uitofp
    // CHECK: arith.uitofp
    // CHECK: arith.constant 1
    // CHECK: arith.cmpi eq
    // CHECK: arith.select
    // CHECK: arith.cmpi eq
    // CHECK: arith.select
    // CHECK: arith.uitofp
    // CHECK: arith.constant 2
    // CHECK: arith.cmpi eq
    // CHECK: arith.select
    // CHECK: arith.cmpi eq
    // CHECK: arith.select
    // CHECK: arith.mulf
    // CHECK: arith.mulf
    // CHECK: arith.addf
    // CHECK: linalg.yield
    %res_ui32 = vosa.reduce_interp_channelwise %t_ui32, %s : tensor<100x200x3xui32>, tensor<100x200x1xf32> -> tensor<100x200x1xf32>

    // CHECK: tensor.empty() : tensor<100x200x1xf32>
    // CHECK: tosa.slice %{{.*}}{size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 0>} : (tensor<100x200x3xf32>) -> tensor<100x200x1xf32> 
    // CHECK: tosa.slice %{{.*}}{size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 1>} : (tensor<100x200x3xf32>) -> tensor<100x200x1xf32> 
    // CHECK: tosa.slice %{{.*}}{size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 2>} : (tensor<100x200x3xf32>) -> tensor<100x200x1xf32> 
    // CHECK: tosa.slice %{{.*}}{size = array<i64: 100, 200, 1>, start = array<i64: 0, 0, 0>} : (tensor<100x200x1xf32>) -> tensor<100x200x1xf32> 
    // CHECK: tensor.empty() : tensor<100x200x1xf32> 
    // CHECK: linalg.generic {indexing_maps = [#map, #map, #map, #map, #map], iterator_types = ["parallel", "parallel", "parallel"]} ins({{.*}}, {{.*}}, {{.*}}, {{.*}}  : tensor<100x200x1xf32>, tensor<100x200x1xf32>, tensor<100x200x1xf32>, tensor<100x200x1xf32>) outs({{.*}} : tensor<100x200x1xf32>) { 
    // CHECK: ^bb0({{.*}}: f32, {{.*}}: f32, {{.*}}: f32, {{.*}}: f32, {{.*}}: f32):
    // CHECK: arith.constant 0
    // CHECK: arith.constant 1
    // CHECK: arith.constant 1.000000e+00
    // CHECK: arith.constant 2
    // CHECK: math.floor
    // CHECK: arith.fptosi
    // CHECK: arith.addi
    // CHECK: arith.cmpi slt
    // CHECK: arith.cmpi slt
    // CHECK: arith.select
    // CHECK: arith.select
    // CHECK: arith.cmpi slt
    // CHECK: arith.cmpi slt
    // CHECK: arith.select
    // CHECK: arith.select
    // CHECK: arith.addf
    // CHECK: arith.subf
    // CHECK: arith.subf
    // CHECK: arith.constant 1
    // CHECK: arith.cmpi eq
    // CHECK: arith.select
    // CHECK: arith.cmpi eq
    // CHECK: arith.select
    // CHECK: arith.constant 2
    // CHECK: arith.cmpi eq
    // CHECK: arith.select
    // CHECK: arith.cmpi eq
    // CHECK: arith.select
    // CHECK: arith.mulf
    // CHECK: arith.mulf
    // CHECK: arith.addf
    // CHECK: linalg.yield
    %res_f32 = vosa.reduce_interp_channelwise %t_f32, %s : tensor<100x200x3xf32>, tensor<100x200x1xf32> -> tensor<100x200x1xf32>

    return
}

// -----

// CHECK-LABEL: func.func @reduce_max_channelwise({{.*}})
func.func @reduce_max_channelwise(%t_si32 : tensor<100x200x3xsi32>, %t_ui32 : tensor<100x200x3xui32>, %t_f32 : tensor<100x200x3xf32>) {

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xi32>) outs(%{{.*}} : tensor<100x200x1xi32>) {
    // CHECK: cmpi sgt
    // CHECK: select
    // CHECK: linalg.yield
    %res_si32 = vosa.reduce_max_channelwise %t_si32 : tensor<100x200x3xsi32> -> tensor<100x200x1xsi32>

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xi32>) outs(%{{.*}} : tensor<100x200x1xi32>) {
    // CHECK: cmpi ugt
    // CHECK: select
    // CHECK: linalg.yield
    %res_ui32 = vosa.reduce_max_channelwise %t_ui32 : tensor<100x200x3xui32> -> tensor<100x200x1xui32>

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xf32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xf32>) outs(%{{.*}} : tensor<100x200x1xf32>) {
    // CHECK: cmpf
    // CHECK: select
    // CHECK: linalg.yield
    %res_f32 = vosa.reduce_max_channelwise %t_f32 : tensor<100x200x3xf32> -> tensor<100x200x1xf32>

    return
}

// -----

// CHECK-LABEL: func.func @reduce_max_planewise({{.*}})
func.func @reduce_max_planewise(%t_si8: tensor<100x200x1xsi8>, %t_si32: tensor<100x200x1xsi32>, %t_ui8: tensor<100x200x1xui8>, %t_f32: tensor<100x200x1xf32>) {

    // CHECK: %{{.*}} = arith.constant -128 : i8
    // CHECK: %{{.*}} = tensor.empty() : tensor<i8>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xi8>) outs(%{{.*}} : tensor<i8>) {
    // CHECK: cmpi sgt
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.expand_shape %{{.*}} [] : tensor<i8> into tensor<1x1x1xi8>
    %res0 = vosa.reduce_max_planewise %t_si8 : tensor<100x200x1xsi8> -> tensor<1x1x1xsi8>

    // CHECK: %{{.*}} = arith.constant -2147483648 : i32
    // CHECK: %{{.*}} = tensor.empty() : tensor<i32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xi32>) outs(%{{.*}} : tensor<i32>) {
    // CHECK: cmpi sgt
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.expand_shape %{{.*}} [] : tensor<i32> into tensor<1x1x1xi32>
    %res1 = vosa.reduce_max_planewise %t_si32 : tensor<100x200x1xsi32> -> tensor<1x1x1xsi32>

    // CHECK: %{{.*}} = arith.constant 0 : i8
    // CHECK: %{{.*}} = tensor.empty() : tensor<i8>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xi8>) outs(%{{.*}} : tensor<i8>) {
    // CHECK: cmpi ugt
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.expand_shape %{{.*}} [] : tensor<i8> into tensor<1x1x1xi8>
    %res_ui8 = vosa.reduce_max_planewise %t_ui8 : tensor<100x200x1xui8> -> tensor<1x1x1xui8>

    // CHECK: %{{.*}} = arith.constant -3.{{.*}}E+38 : f32
    // CHECK: %{{.*}} = tensor.empty() : tensor<f32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xf32>) outs(%{{.*}} : tensor<f32>) {
    // CHECK: cmpf
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.expand_shape %{{.*}} [] : tensor<f32> into tensor<1x1x1xf32>
    %res3 = vosa.reduce_max_planewise %t_f32 : tensor<100x200x1xf32> -> tensor<1x1x1xf32>

    return
}

// -----

// CHECK-LABEL: func.func @reduce_min_channelwise({{.*}})
func.func @reduce_min_channelwise(%t_si32 : tensor<100x200x3xsi32>, %t_ui32 : tensor<100x200x3xui32>, %t_f32 : tensor<100x200x3xf32>) {

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xi32>) outs(%{{.*}} : tensor<100x200x1xi32>) {
    // CHECK: cmpi slt
    // CHECK: select
    // CHECK: linalg.yield
    %res_si32 = vosa.reduce_min_channelwise %t_si32 : tensor<100x200x3xsi32> -> tensor<100x200x1xsi32>

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xi32>) outs(%{{.*}} : tensor<100x200x1xi32>) {
    // CHECK: cmpi ult
    // CHECK: select
    // CHECK: linalg.yield
    %res_ui32 = vosa.reduce_min_channelwise %t_ui32 : tensor<100x200x3xui32> -> tensor<100x200x1xui32>

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xf32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xf32>) outs(%{{.*}} : tensor<100x200x1xf32>) {
    // CHECK: cmpf
    // CHECK: select
    // CHECK: linalg.yield
    %res_f32 = vosa.reduce_min_channelwise %t_f32 : tensor<100x200x3xf32> -> tensor<100x200x1xf32>

    return
}

// -----

// CHECK-LABEL: func.func @reduce_min_planewise({{.*}})
func.func @reduce_min_planewise(%t_si8: tensor<100x200x1xsi8>, %t_si32: tensor<100x200x1xsi32>, %t_ui8: tensor<100x200x1xui8>, %t_f32: tensor<100x200x1xf32>) {

    // CHECK: %{{.*}} = arith.constant 127 : i8
    // CHECK: %{{.*}} = tensor.empty() : tensor<i8>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xi8>) outs(%{{.*}} : tensor<i8>) {
    // CHECK: cmpi slt
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.expand_shape %{{.*}} [] : tensor<i8> into tensor<1x1x1xi8>
    %res0 = vosa.reduce_min_planewise %t_si8 : tensor<100x200x1xsi8> -> tensor<1x1x1xsi8>

    // CHECK: %{{.*}} = arith.constant 2147483647 : i32
    // CHECK: %{{.*}} = tensor.empty() : tensor<i32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xi32>) outs(%{{.*}} : tensor<i32>) {
    // CHECK: cmpi slt
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.expand_shape %{{.*}} [] : tensor<i32> into tensor<1x1x1xi32>
    %res1 = vosa.reduce_min_planewise %t_si32 : tensor<100x200x1xsi32> -> tensor<1x1x1xsi32>

    // CHECK: %{{.*}} = arith.constant -1 : i8
    // CHECK: %{{.*}} = tensor.empty() : tensor<i8>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xi8>) outs(%{{.*}} : tensor<i8>) {
    // CHECK: cmpi ult
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.expand_shape %{{.*}} [] : tensor<i8> into tensor<1x1x1xi8>
    %res_ui8 = vosa.reduce_min_planewise %t_ui8 : tensor<100x200x1xui8> -> tensor<1x1x1xui8>

    // CHECK: %{{.*}} = arith.constant 3.{{.*}}E+38 : f32
    // CHECK: %{{.*}} = tensor.empty() : tensor<f32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xf32>) outs(%{{.*}} : tensor<f32>) {
    // CHECK: cmpf
    // CHECK: select
    // CHECK: linalg.yield
    // CHECK: tensor.expand_shape %{{.*}} [] : tensor<f32> into tensor<1x1x1xf32>
    %res3 = vosa.reduce_min_planewise %t_f32 : tensor<100x200x1xf32> -> tensor<1x1x1xf32>

    return
}

// -----

// CHECK-LABEL: func.func @reduce_sum_channelwise({{.*}})
func.func @reduce_sum_channelwise(%t_si32 : tensor<100x200x3xsi32>, %t_ui32 : tensor<100x200x3xui32>, %t_f32 : tensor<100x200x3xf32>) {

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xi32>) outs(%{{.*}} : tensor<100x200x1xi32>) {
    // CHECK: addi
    // CHECK: linalg.yield
    %res_si32 = vosa.reduce_sum_channelwise %t_si32 : tensor<100x200x3xsi32> -> tensor<100x200x1xsi32>

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xi32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xi32>) outs(%{{.*}} : tensor<100x200x1xi32>) {
    // CHECK: addi
    // CHECK: linalg.yield
    %res_ui32 = vosa.reduce_sum_channelwise %t_ui32 : tensor<100x200x3xui32> -> tensor<100x200x1xui32>

    // CHECK: %{{.*}} = tensor.empty() : tensor<100x200x1xf32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["parallel", "parallel", "reduction"]} ins(%{{.*}} : tensor<100x200x3xf32>) outs(%{{.*}} : tensor<100x200x1xf32>) {
    // CHECK: addf
    // CHECK: linalg.yield
    %res_f32 = vosa.reduce_sum_channelwise %t_f32 : tensor<100x200x3xf32> -> tensor<100x200x1xf32>

    return
}

// -----

// CHECK-LABEL: func.func @reduce_sum_planewise({{.*}})
func.func @reduce_sum_planewise(%t_si8: tensor<100x200x1xsi8>, %t_si32: tensor<100x200x1xsi32>, %t_f32: tensor<100x200x1xf32>) {

    // CHECK: %{{.*}} = arith.constant 0 : i8
    // CHECK: %{{.*}} = tensor.empty() : tensor<i8>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xi8>) outs(%{{.*}} : tensor<i8>) {
    // CHECK: addi
    // CHECK: linalg.yield
    // CHECK: tensor.expand_shape %{{.*}} [] : tensor<i8> into tensor<1x1x1xi8>
    %res0 = vosa.reduce_sum_planewise %t_si8 : tensor<100x200x1xsi8> -> tensor<1x1x1xsi8>

    // CHECK: %{{.*}} = arith.constant 0 : i32
    // CHECK: %{{.*}} = tensor.empty() : tensor<i32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xi32>) outs(%{{.*}} : tensor<i32>) {
    // CHECK: addi
    // CHECK: linalg.yield
    // CHECK: tensor.expand_shape %{{.*}} [] : tensor<i32> into tensor<1x1x1xi32>
    %res1 = vosa.reduce_sum_planewise %t_si32 : tensor<100x200x1xsi32> -> tensor<1x1x1xsi32>

    // CHECK: %{{.*}} = arith.constant 0.{{.*}}e+00 : f32
    // CHECK: %{{.*}} = tensor.empty() : tensor<f32>
    // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["reduction", "reduction", "reduction"]} ins(%{{.*}} : tensor<100x200x1xf32>) outs(%{{.*}} : tensor<f32>) {
    // CHECK: addf
    // CHECK: linalg.yield
    // CHECK: tensor.expand_shape %{{.*}} [] : tensor<f32> into tensor<1x1x1xf32>
    %res3 = vosa.reduce_sum_planewise %t_f32 : tensor<100x200x1xf32> -> tensor<1x1x1xf32>

    return
}

// -----

module {
    // CHECK-LABEL: func.func @pointwise_matrix_multiply()
    func.func @pointwise_matrix_multiply() {
        //CHECK: arith.constant {{.*}} : tensor<3xi64>
        //CHECK: arith.constant {{.*}} : tensor<3x3xf32>
        //CHECK: tosa.reshape %{{.*}} {new_shape = array<i64: 1, 3, 3>} : (tensor<3x3xf32>) -> tensor<1x3x3xf32>
        //CHECK: arith.constant {{.*}} : tensor<3xf32>
        //CHECK: tosa.reshape %{{.*}} {new_shape = array<i64: 3, 1>} : (tensor<3xf32>) -> tensor<3x1xf32>
        //CHECK: arith.constant {{.*}} : tensor<3xf32>
        //CHECK: tosa.reshape %{{.*}} {new_shape = array<i64: 3, 1>} : (tensor<3xf32>) -> tensor<3x1xf32>
        //CHECK: tosa.tile %{{.*}} {multiples = array<i64: 1, 1, 20000>} : (tensor<1x3x1xf32>) -> tensor<1x3x20000xf32>
        //CHECK: tosa.tile %{{.*}} {multiples = array<i64: 1, 1, 20000>} : (tensor<1x3x1xf32>) -> tensor<1x3x20000xf32>
        //CHECK: tosa.reshape %{{.*}} {new_shape = array<i64: 1, 20000, 3>} : (tensor<100x200x3xf32>) -> tensor<1x20000x3xf32>
        //CHECK: tosa.transpose %{{.*}}, %{{.*}} : (tensor<1x20000x3xf32>, tensor<3xi64>) -> tensor<1x3x20000xf32>
        //CHECK: tosa.sub
        //CHECK: tosa.matmul
        //CHECK: tosa.add
        //CHECK: tosa.transpose %{{.*}}, %{{.*}} : (tensor<1x3x20000xf32>, tensor<3xi64>) -> tensor<1x20000x3xf32>
        //CHECK: tosa.reshape %{{.*}} {new_shape = array<i64: 100, 200, 3>} : (tensor<1x20000x3xf32>) -> tensor<100x200x3xf32>
        %t0 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        %res = vosa.pointwise_matrix_multiply %t0 {M=dense<[[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]> : tensor<3x3xf32>, K_1=dense<[0.0, 0.0, 0.0]> : tensor<3xf32>, K_2=dense<[0.0, 0.0, 0.0]> : tensor<3xf32>} : tensor<100x200x3xf32> -> tensor<100x200x3xf32>

        // Sizes other than 3
        %t1 = vosa.constant dense<42.0> : tensor<100x200x2xf32>
        //CHECK: arith.constant {{.*}} : tensor<3xi64>
        //CHECK: arith.constant {{.*}} : tensor<4x2xf32>
        //CHECK: tosa.reshape %{{.*}} {new_shape = array<i64: 1, 4, 2>} : (tensor<4x2xf32>) -> tensor<1x4x2xf32>
        //CHECK: arith.constant {{.*}} : tensor<2xf32>
        //CHECK: tosa.reshape %{{.*}} {new_shape = array<i64: 2, 1>} : (tensor<2xf32>) -> tensor<2x1xf32>
        //CHECK: arith.constant {{.*}} : tensor<4xf32>
        //CHECK: tosa.reshape %{{.*}} {new_shape = array<i64: 4, 1>} : (tensor<4xf32>) -> tensor<4x1xf32>
        //CHECK: tosa.tile %{{.*}} {multiples = array<i64: 1, 1, 20000>} : (tensor<1x2x1xf32>) -> tensor<1x2x20000xf32>
        //CHECK: tosa.tile %{{.*}} {multiples = array<i64: 1, 1, 20000>} : (tensor<1x4x1xf32>) -> tensor<1x4x20000xf32>
        //CHECK: tosa.reshape %{{.*}} {new_shape = array<i64: 1, 20000, 2>} : (tensor<100x200x2xf32>) -> tensor<1x20000x2xf32>
        //CHECK: tosa.transpose %{{.*}}, %{{.*}} : (tensor<1x20000x2xf32>, tensor<3xi64>) -> tensor<1x2x20000xf32>
        //CHECK: tosa.sub %{{.*}}, %{{.*}} : (tensor<1x2x20000xf32>, tensor<1x2x20000xf32>) -> tensor<1x2x20000xf32>
        //CHECK: tosa.matmul %{{.*}}, %{{.*}} : (tensor<1x4x2xf32>, tensor<1x2x20000xf32>) -> tensor<1x4x20000xf32>
        //CHECK: tosa.add %{{.*}}, %{{.*}} : (tensor<1x4x20000xf32>, tensor<1x4x20000xf32>) -> tensor<1x4x20000xf32>
        //CHECK: tosa.transpose %{{.*}}, %{{.*}} : (tensor<1x4x20000xf32>, tensor<3xi64>) -> tensor<1x20000x4xf32>
        //CHECK: tosa.reshape %{{.*}} {new_shape = array<i64: 100, 200, 4>} : (tensor<1x20000x4xf32>) -> tensor<100x200x4xf32>
        %res1 = vosa.pointwise_matrix_multiply %t1 {M=dense<[[1.0, 0.0], [0.0, 1.0], [0.5, 0.5], [1.5, 11.5]]> : tensor<4x2xf32>, K_1=dense<[0.0, 0.0]> : tensor<2xf32>, K_2=dense<[0.0, 0.0, 0.0, 0.0]> : tensor<4xf32>} : tensor<100x200x2xf32> -> tensor<100x200x4xf32>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @piecewise_linear()
    func.func @piecewise_linear() {
        // CHECK: tensor.empty
        // CHECK-COUNT-5: arith.constant
        // CHECK: linalg.generic
        // CHECK: tensor.extract
        // CHECK: arith.cmpi
        // CHECK: scf.if
        // CHECK:   tensor.extract
        // CHECK:   scf.yield
        // CHECK: else
        // CHECK:   arith.index_cast
        // CHECK:   tensor.extract
        // CHECK:   arith.cmpi
        // CHECK:   scf.if
        // CHECK:       arith.index_cast
        // CHECK:       tensor.extract
        // CHECK:       scf.yield
        // CHECK:   else
        // CHECK:       arith.shrsi
        // CHECK:       math.ctlz
        // CHECK:       arith.subi
        // CHECK:       arith.subi
        // CHECK:       arith.subi
        // CHECK:       arith.cmpi
        // CHECK:       arith.select
        // CHECK:       arith.cmpi
        // CHECK:       arith.select
        // CHECK-DAG:   arith.addi
        // CHECK-DAG:   arith.addi
        // CHECK-DAG:   arith.shli
        // CHECK-DAG:   arith.cmpi
        // CHECK:       arith.select
        // CHECK:       arith.index_cast
        // CHECK:       tensor.extract
        // CHECK:       arith.index_cast
        // CHECK:       tensor.extract
        // CHECK:       arith.maxui
        // CHECK:       arith.divui
        // CHECK:       arith.index_cast
        // CHECK:       tensor.extract
        // CHECK:       arith.addi
        // CHECK:       arith.cmpi
        // CHECK:       arith.select
        // CHECK:       arith.cmpi
        // CHECK:       arith.select
        // CHECK:       arith.addi
        // CHECK:       arith.index_cast
        // CHECK:       tensor.extract
        // CHECK:       arith.index_cast
        // CHECK:       tensor.extract
        // CHECK:       arith.index_cast
        // CHECK:       tensor.extract
        // CHECK:       arith.index_cast
        // CHECK:       tensor.extract
        // CHECK:       arith.subi
        // CHECK:       arith.subi
        // CHECK:       arith.subi
        // CHECK:       arith.muli
        // CHECK:       arith.shli
        // CHECK:       arith.divsi
        // CHECK:       arith.addi
        // CHECK:       arith.addi
        // CHECK:       scf.yield
        // CHECK:   scf.yield
        // CHECK: linalg.yield
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %res_si8 = vosa.piecewise_linear %t0_si8 {nodes=dense<[1,2,3]> : tensor<3xsi8>, values=dense<[10, 22, 53]> : tensor<3xsi8>} : tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>

        // CHECK: tensor.empty
        // CHECK-COUNT-5: arith.constant
        // CHECK: linalg.generic
        // CHECK: tensor.extract
        // CHECK: arith.cmpf
        // CHECK: scf.if
        // CHECK:   tensor.extract
        // CHECK:   scf.yield
        // CHECK: else
        // CHECK:   arith.index_cast
        // CHECK:   tensor.extract
        // CHECK:   arith.cmpf
        // CHECK:   scf.if
        // CHECK:       arith.index_cast
        // CHECK:       tensor.extract
        // CHECK:       scf.yield
        // CHECK:   else
        // CHECK:       scf.while
        // CHECK:           arith.index_cast
        // CHECK:           tensor.extract
        // CHECK:           arith.cmpi
        // CHECK:           arith.cmpf
        // CHECK:           arith.andi
        // CHECK:       scf.condition
        // CHECK:       do
        // CHECK:           arith.addi
        // CHECK:           scf.yield
        // CHECK:       arith.subi
        // CHECK:       arith.index_cast
        // CHECK:       tensor.extract
        // CHECK:       arith.index_cast
        // CHECK:       tensor.extract
        // CHECK:       arith.index_cast
        // CHECK:       tensor.extract
        // CHECK:       arith.index_cast
        // CHECK:       tensor.extract
        // CHECK:       arith.subf
        // CHECK:       arith.subf
        // CHECK:       arith.subf
        // CHECK:       arith.mulf
        // CHECK:       arith.divf
        // CHECK:       arith.addf
        // CHECK:       scf.yield
        // CHECK:   scf.yield
        // CHECK: linalg.yield

        %t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        %res_f16 = vosa.piecewise_linear %t0_f16 {nodes=dense<[1.0,2.0,3.0]> : tensor<3xf16>, values=dense<[10.0, 22.0, 53.0]> : tensor<3xf16>} : tensor<100x200x3xf16> -> tensor<100x200x3xf16>

        return
    }
}

// -----

module {
    func.func private @tosa_graph(%arg0: tensor<1x256x256x3xf32>) -> tensor<1x16x16x8x12xf32>

    func.func @call_tosa_graph() {
        %a = arith.constant dense<42.0> : tensor<256x256x3xf32>
        // CHECK: %[[A:.*]] = tosa.reshape %{{.*}} {new_shape = array<i64: 1, 256, 256, 3>} : (tensor<256x256x3xf32>) -> tensor<1x256x256x3xf32>
        // CHECK: %[[B:.*]] = call @tosa_graph(%[[A]]) : (tensor<1x256x256x3xf32>) -> tensor<1x16x16x8x12xf32>
        // CHECK: %{{.*}} = tosa.reshape %[[B]] {new_shape = array<i64: 96, 16, 16>} : (tensor<1x16x16x8x12xf32>) -> tensor<96x16x16xf32>
        %res_si8 = vosa.tosa_graph %a { function = @tosa_graph } :  (tensor<256x256x3xf32>) -> tensor<96x16x16xf32>

        return
    }
}

// -----

module {
    func.func private @tosa_graph(%arg0: tensor<1x256x256x3xui8>) -> tensor<1x256x256x1xui8> {
        %0 = tosa.slice %arg0 {size = array<i64: 1, 256, 256, 1>, start = array<i64: 0, 0, 0, 0>} : (tensor<1x256x256x3xui8>) -> tensor<1x256x256x1xui8>
        return %0 : tensor<1x256x256x1xui8>
    }

    func.func @call_tosa_graph_ui() {
        %a = arith.constant dense<42> : tensor<256x256x3xui8>
        // CHECK: %[[A:.*]] = tosa.reshape %{{.*}} {new_shape = array<i64: 1, 256, 256, 3>} : (tensor<256x256x3xui8>) -> tensor<1x256x256x3xui8>
        // CHECK: %[[B:.*]] = call @tosa_graph(%[[A]]) : (tensor<1x256x256x3xui8>) -> tensor<1x256x256x1xui8>
        // CHECK: %{{.*}} = tosa.reshape %[[B]] {new_shape = array<i64: 256, 256, 1>} : (tensor<1x256x256x1xui8>) -> tensor<256x256x1xui8>
        %res = vosa.tosa_graph %a { function = @tosa_graph } :  (tensor<256x256x3xui8>) -> tensor<256x256x1xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @mod()
    func.func @mod() {
        %t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        %t1_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
 
        // CHECK: tensor.empty
        // CHECK: arith.remui
        // CHECK: linalg.yield
        %res_ui16 = vosa.mod %t0_ui16, %t1_ui16 : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xui16>

        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        %t1_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
 
        // CHECK: tensor.empty
        // CHECK: arith.remsi
        // CHECK: linalg.yield
        %res_si16 = vosa.mod %t0_si16, %t1_si16 : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>

        %t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        %t1_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
 
        // CHECK: tensor.empty
        // CHECK: arith.remf
        // CHECK: linalg.yield
        %res_f16 = vosa.mod %t0_f16, %t1_f16 : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xf16>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @expand()
    func.func @expand() {
        %t0 = vosa.constant dense<42.0> : tensor<4x4x3xf32>
        %expand_kernel = vosa.constant dense<[[[0], [1], [3]], [[2], [3], [0]]]> : tensor<2x3x1xui8>

        // CHECK-COUNT-8: arith.constant
        // CHECK: %{{.*}} = arith.constant dense<{{\[\[-1, -1], \[0, 0], \[1, 1], \[1, 0], \[-1, -1]]}}> : tensor<5x2xi32>
        // CHECK: %{{.*}} = tensor.empty() : tensor<4x6x3xf32>
        // CHECK: %{{.*}} = linalg.generic {indexing_maps = [#map, #map1], iterator_types = ["parallel", "parallel", "parallel"]} ins(%{{.*}} : tensor<2x3x1xi8>) outs(%{{.*}} : tensor<4x6x3xf32>) {
        // CHECK: %{{.*}} = arith.index_cast
        // CHECK: %{{.*}} = tensor.extract
        // CHECK: %{{.*}} = arith.index_cast
        // CHECK: %{{.*}} = tensor.extract
        // CHECK: %{{.*}} = arith.index_cast
        // CHECK: %{{.*}} = arith.cmpi eq
        // CHECK: %{{.*}} = scf.if %{{.*}} -> (f32) {
        // CHECK: scf.yield %{{.*}} : f32
        // CHECK: } else {
        // CHECK: %{{.*}} = arith.divui
        // CHECK: %{{.*}} = arith.divui
        // CHECK: %{{.*}} = arith.muli
        // CHECK: %{{.*}} = arith.addi
        // CHECK: %{{.*}} = arith.muli
        // CHECK: %{{.*}} = arith.addi
        // CHECK: %{{.*}} = tensor.extract
        // CHECK: scf.yield %{{.*}} : f32
        // CHECK: linalg.yield %{{.*}} : f32
        %res = vosa.expand %t0, %expand_kernel {gather_kernel = dense<[[1, 0], [3, 2]]> : tensor<2x2xui8>, fill_value = 0.0 : f32} : tensor<4x4x3xf32>, tensor<2x3x1xui8> -> tensor<4x6x3xf32>

        return
    }
}

// -----


func.func @foo() -> si32 {
    %v = vosa.constant 1234 : si32
    return %v : si32
}

func.func @bar(%arg0: si32) -> si32 {
    %a = arith.constant 1234 : i32
    %b = builtin.unrealized_conversion_cast %arg0 : si32 to i32
    %v = arith.addi %a, %b : i32
    %r = builtin.unrealized_conversion_cast %v : i32 to si32
    return %r : si32
}

func.func @baz(%arg0: si32, %arg1: f32) -> si32 {
    %a = builtin.unrealized_conversion_cast %arg0 : si32 to i32
    %b = arith.fptosi %arg1 : f32 to i32
    %v = arith.addi %a, %b : i32
    %r = builtin.unrealized_conversion_cast %v : i32 to si32
    return %r : si32
}

func.func @custom_scalar() {
    %i = vosa.constant 1234 : si32
    %f = vosa.constant 1234.0 : f32

    // CHECK: %{{.*}} = call @foo() : () -> si32
    %a = vosa.custom_scalar @foo() : () -> si32
    // CHECK: %{{.*}} = call @bar(%{{.*}}) : (si32) -> si32
    %b = vosa.custom_scalar @bar(%i) : (si32) -> si32
    // CHECK: %{{.*}} = call @baz(%{{.*}}, %{{.*}}) : (si32, f32) -> si32
    %c = vosa.custom_scalar @baz(%i, %f) : (si32, f32) -> si32

    return
}
