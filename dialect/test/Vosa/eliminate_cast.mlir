// RUN: vosa-opt -vosa-eliminate-cast -split-input-file %s | FileCheck %s

// CHECK-LABEL:   func.func @remove_cast(
// CHECK-SAME:                           %[[VAL_0:.*]]: tensor<1080x1920x1xui8>) -> tensor<1080x1920x1xui32> {
// CHECK:           %[[VAL_1:.*]] = vosa.pad %[[VAL_0]] replicate {pad_size = [0 : i32, 0 : i32, 21 : i32, 21 : i32]} : tensor<1080x1920x1xui8> -> tensor<1080x1962x1xui8>
// CHECK:           %[[VAL_2:.*]] = vosa.conv_2d %[[VAL_1]] {filter = dense<{{\[\[}}157, 206, 268, 344, 436, 545, 673, 818, 982, 1163, 1358, 1566, 1780, 1997, 2210, 2413, 2600, 2763, 2897, 2996, 3057, 3078, 3057, 2996, 2897, 2763, 2600, 2413, 2210, 1997, 1780, 1566, 1358, 1163, 982, 818, 673, 545, 436, 344, 268, 206, 157]]> : tensor<1x43xui32>} : tensor<1080x1962x1xui8> -> tensor<1080x1920x1xui32>
// CHECK:           return %[[VAL_2]] : tensor<1080x1920x1xui32>
// CHECK:         }
func.func @remove_cast(%arg0: tensor<1080x1920x1xui8>) -> (tensor<1080x1920x1xui32>) {
  %11 = vosa.pad %arg0 replicate {pad_size = [0 : i32, 0 : i32, 21 : i32, 21 : i32]} : tensor<1080x1920x1xui8> -> tensor<1080x1962x1xui8>
  %12 = vosa.cast %11 wrap : tensor<1080x1962x1xui8> -> tensor<1080x1962x1xui32>
  %13 = vosa.conv_2d %12 {filter = dense<[[157, 206, 268, 344, 436, 545, 673, 818, 982, 1163, 1358, 1566, 1780, 1997, 2210, 2413, 2600, 2763, 2897, 2996, 3057, 3078, 3057, 2996, 2897, 2763, 2600, 2413, 2210, 1997, 1780, 1566, 1358, 1163, 982, 818, 673, 545, 436, 344, 268, 206, 157]]> : tensor<1x43xui32>} : tensor<1080x1962x1xui32> -> tensor<1080x1920x1xui32>
  return %13 : tensor<1080x1920x1xui32> 
}

// -----

// CHECK-LABEL:   func.func @remove_cast(
// CHECK-SAME:                           %[[VAL_0:.*]]: tensor<1080x1962x1xui8>) -> tensor<1080x1920x1xui32> {
// CHECK:           %[[VAL_1:.*]] = vosa.conv_2d %[[VAL_0]] {filter = dense<{{\[\[}}157, 206, 268, 344, 436, 545, 673, 818, 982, 1163, 1358, 1566, 1780, 1997, 2210, 2413, 2600, 2763, 2897, 2996, 3057, 3078, 3057, 2996, 2897, 2763, 2600, 2413, 2210, 1997, 1780, 1566, 1358, 1163, 982, 818, 673, 545, 436, 344, 268, 206, 157]]> : tensor<1x43xui32>} : tensor<1080x1962x1xui8> -> tensor<1080x1920x1xui32>
// CHECK:           return %[[VAL_1]] : tensor<1080x1920x1xui32>
// CHECK:         }
func.func @remove_cast(%arg0: tensor<1080x1962x1xui8>) -> (tensor<1080x1920x1xui32>) {
  %12 = vosa.cast %arg0 wrap : tensor<1080x1962x1xui8> -> tensor<1080x1962x1xui32>
  %13 = vosa.conv_2d %12 {filter = dense<[[157, 206, 268, 344, 436, 545, 673, 818, 982, 1163, 1358, 1566, 1780, 1997, 2210, 2413, 2600, 2763, 2897, 2996, 3057, 3078, 3057, 2996, 2897, 2763, 2600, 2413, 2210, 1997, 1780, 1566, 1358, 1163, 982, 818, 673, 545, 436, 344, 268, 206, 157]]> : tensor<1x43xui32>} : tensor<1080x1962x1xui32> -> tensor<1080x1920x1xui32>
  return %13 : tensor<1080x1920x1xui32>
}
