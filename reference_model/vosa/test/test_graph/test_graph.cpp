/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "custom.h"
#include "ops.h"
#include "test_bench.h"
#include "vosa/vosa.h"
#include "vosa_generated.h"
#include <gtest/gtest.h>

TEST(test_graph, test_array_base) {

  uint32_t SIZE = 3;

  vosa::EigenImage<float> input(SIZE, SIZE, SIZE);
  input.setRandom();
  vosa::ArrayBase array_base(input);

  ASSERT_EQ(array_base.get_atype(), vosa::ArrayBase::IMAGE);

  vosa::EigenImage<float> output = array_base.get_image<float>();

  const auto eigen_dimensions = output.dimensions();
  ASSERT_EQ(eigen_dimensions.size(), 3);

  for (const auto &d : eigen_dimensions) {
    ASSERT_EQ(d, SIZE);
  }

  for (uint32_t i = 0; i < SIZE; i++) {
    for (uint32_t j = 0; j < SIZE; j++) {
      for (uint32_t k = 0; k < SIZE; k++) {
        ASSERT_EQ(input(i, j, k), output(i, j, k));
      }
    }
  }
}

struct TEST_GRAPH_CONSTANTS {
  inline static std::string INPUT_NAME = "INPUT_NAME";
  inline static std::string OP_NAME = "OP_NAME";
  inline static std::vector<uint32_t> RESIZE{5, 5};
};

void test_graph(vosa::Graph &graph) {
  const auto &graph_ops = graph.get_ops();
  EXPECT_EQ(graph_ops.size(), 1);
  for (const auto &kv : graph_ops) {
    std::string name = kv.first;
    EXPECT_EQ(name, TEST_GRAPH_CONSTANTS::OP_NAME);
    const std::shared_ptr<vosa::Op> &op_ptr = kv.second;
    const auto &resize_ptr =
        std::dynamic_pointer_cast<vosa::ResizeBilinear<float>>(op_ptr);
    EXPECT_TRUE(resize_ptr != nullptr);
  }

  vosa::EigenImage<float> test_image(1, 3, 3);
  test_image.setValues({{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}});

  vosa::ArrayBase array_base_input(test_image);
  std::unordered_map<std::string, vosa::ArrayBase> graph_input{
      {TEST_GRAPH_CONSTANTS::INPUT_NAME, array_base_input}};

  std::vector<std::string> output_names = {TEST_GRAPH_CONSTANTS::OP_NAME};
  std::vector<std::shared_ptr<vosa::ArrayBase>> execution_outputs =
      graph.execute(graph_input, output_names);

  EXPECT_EQ(execution_outputs.size(), 1);

  auto golden_output = vosa::ResizeBilinear<float>(TEST_GRAPH_CONSTANTS::RESIZE)
                           .runme(test_image);
  auto graph_output = execution_outputs[0].get()->get_image<float>();

  bool output = vosa::test::TestBench<float, int32_t>::compareEigenImages(
      golden_output, graph_output, 0, false);
  EXPECT_TRUE(output);
}

vosa_serialization::VosaGraphT make_serialized_graph() {
  auto resize_bilinear_attribute =
      std::make_unique<vosa_serialization::RESIZE_BILINEAR_ATTRIBUTET>();

  resize_bilinear_attribute->size = TEST_GRAPH_CONSTANTS::RESIZE;

  vosa_serialization::RESIZE_BILINEART resize_bilinear;
  resize_bilinear.mode =
      vosa_serialization::RESIZE_BILINEAR_MODE_I_I_FLOAT32_FLOAT32;
  resize_bilinear.attr = std::move(resize_bilinear_attribute);

  auto resize_bilinear_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  resize_bilinear_operator->op.Set(std::move(resize_bilinear));
  resize_bilinear_operator->name = TEST_GRAPH_CONSTANTS::OP_NAME;
  std::vector<std::string> input_names{TEST_GRAPH_CONSTANTS::INPUT_NAME};
  resize_bilinear_operator->inputs = input_names;

  auto operators =
      std::vector<std::unique_ptr<vosa_serialization::VosaGraphOperatorT>>();
  operators.push_back(std::move(resize_bilinear_operator));
  vosa_serialization::VosaGraphT serialized_vosa_graph;
  serialized_vosa_graph.operators = std::move(operators);

  return serialized_vosa_graph;
}

TEST(test_graph, test_graph_data_pointer_constructor) {

  auto serialized_vosa_graph = make_serialized_graph();
  flatbuffers::FlatBufferBuilder fbb;
  fbb.Finish(vosa_serialization::VosaGraph::Pack(fbb, &serialized_vosa_graph));
  uint8_t *buffer = fbb.GetBufferPointer();

  auto graph = vosa::Graph(buffer);

  test_graph(graph);
}

TEST(test_graph, test_graph_serialized_constructor) {

  auto serialized_vosa_graph = make_serialized_graph();

  auto graph = vosa::Graph(serialized_vosa_graph);

  test_graph(graph);
}

TEST(test_graph, test_custom_scalar_operator) {

  const std::string TIMES_TWO = "times_two";
  const std::string TIMES_FOUR = "times_four";

  vosa::load_custom_operators("./libtimes_two_custom_operator.so");
  vosa::load_custom_operators("./libtimes_four_custom_operator.so");

  auto &registry = vosa::MasterRegistry::get_instance();
  auto registry_ops = registry.get_ops();

  ASSERT_EQ(registry_ops.size(), 2);

  ASSERT_NE(registry_ops.find(TIMES_TWO), registry_ops.end());
  ASSERT_NE(registry_ops.find(TIMES_FOUR), registry_ops.end());

  vosa::Graph graph;

  auto input = std::make_shared<vosa::Input>(vosa::ArrayBase::DType::FLOAT32);
  graph.add_op(input, std::vector<std::string>{});

  auto times_two_op_creation_function = vosa::MasterRegistry::get_op(TIMES_TWO);
  auto times_four_op_creation_function =
      vosa::MasterRegistry::get_op(TIMES_FOUR);

  auto times_two = times_two_op_creation_function();
  graph.add_op(times_two, {input});
  auto times_four = times_four_op_creation_function();
  graph.add_op(times_four, {input});

  auto input_scalar = vosa::EigenScalar<float>();
  input_scalar() = 1.f;
  vosa::ArrayBase input_array(input_scalar);

  auto graph_outputs =
      graph.execute({{input, input_array}}, {times_two, times_four});

  auto output_times_two = graph_outputs[0]->get_scalar<float>()();
  auto output_times_four = graph_outputs[1]->get_scalar<float>()();

  ASSERT_FLOAT_EQ(output_times_two, 2.0f);
  ASSERT_FLOAT_EQ(output_times_four, 4.0f);
}