/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/arg_min_channelwise.h"
#include "test_bench.h"
#include "vosa/vosa.h"

TEST(test_arg_min_channelwise, test_arg_min_channelwise) {

  auto input_array = vosa::EigenImage<uint8_t>(2, 2, 3);
  input_array.setValues({{{0, 2, 2}, {1, 0, 2}}, {{2, 2, 0}, {0, 2, 0}}});

  auto expected_output_image = vosa::EigenImage<uint32_t>(2, 2, 1);
  expected_output_image.setValues({{{0}, {1}}, {{2}, {0}}});

  auto arg_min_channelwise = vosa::ArgMinChannelwise<uint8_t>();

  auto input = std::make_shared<vosa::ArrayBase>(input_array);
  auto output = arg_min_channelwise.eval({input});

  auto output_image = output->get_image<uint32_t>();

  bool eq = vosa::test::TestBench<uint32_t, int32_t>::compareEigenImages(
      output_image, expected_output_image);
  EXPECT_TRUE(eq);
}
