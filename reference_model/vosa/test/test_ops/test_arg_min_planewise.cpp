/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/arg_min_planewise.h"
#include "test_bench.h"
#include "vosa/vosa.h"

TEST(test_arg_min_planewise, test_arg_min_planewise) {

  // Set up Input
  auto input_array = vosa::EigenImage<uint8_t>(3, 3, 1);
  input_array.setValues(
      {{{5}, {1}, {10}}, {{20}, {35}, {0}}, {{90}, {81}, {156}}});

  // Run the Operator
  auto arg_min_planewise = vosa::ArgMinPlanewise<uint8_t>();
  auto input = std::make_shared<vosa::ArrayBase>(input_array);
  auto output = arg_min_planewise.eval({input});

  // Get the actual results
  auto actual = output->get_vector<uint32_t>();

  // Set up expected results
  auto expected = vosa::EigenVector<uint32_t>(2);
  expected.setValues({1, 2});

  bool eq = vosa::test::TestBench<uint32_t, int32_t>::compareEigenVectors(
      actual, expected);
  EXPECT_TRUE(eq);
}
