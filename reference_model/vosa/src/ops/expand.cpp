/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "expand.h"
#include <unordered_map>

std::shared_ptr<vosa::Op> vosa::ExpandBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_expand = vosa_operator_union.AsEXPAND();
  assert(serialized_expand);
  auto mode = serialized_expand->mode;
  auto serialized_attributes = &serialized_expand->attr;
  std::vector<uint8_t> raw_fill_value =
      serialized_attributes->get()->fill_value;
  std::vector<uint8_t> raw_gather_kernel =
      serialized_attributes->get()->gather_kernel_value;
  std::vector<uint32_t> gather_kernel_dimension =
      serialized_attributes->get()->gather_kernel_dimension;

  switch (mode) {
  case vosa_serialization::EXPAND_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::Expand<uint8_t>>(
        unpack_value<uint8_t>(raw_fill_value),
        unpack_plane<uint8_t>(raw_gather_kernel, gather_kernel_dimension));
  }
  case vosa_serialization::EXPAND_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::Expand<uint16_t>>(
        unpack_value<uint16_t>(raw_fill_value),
        unpack_plane<uint8_t>(raw_gather_kernel, gather_kernel_dimension));
  }
  case vosa_serialization::EXPAND_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::Expand<uint32_t>>(
        unpack_value<uint32_t>(raw_fill_value),
        unpack_plane<uint8_t>(raw_gather_kernel, gather_kernel_dimension));
  }
  case vosa_serialization::EXPAND_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::Expand<int8_t>>(
        unpack_value<int8_t>(raw_fill_value),
        unpack_plane<uint8_t>(raw_gather_kernel, gather_kernel_dimension));
  }
  case vosa_serialization::EXPAND_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::Expand<int16_t>>(
        unpack_value<int16_t>(raw_fill_value),
        unpack_plane<uint8_t>(raw_gather_kernel, gather_kernel_dimension));
  }
  case vosa_serialization::EXPAND_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::Expand<int32_t>>(
        unpack_value<int32_t>(raw_fill_value),
        unpack_plane<uint8_t>(raw_gather_kernel, gather_kernel_dimension));
  }
  case vosa_serialization::EXPAND_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::Expand<float>>(
        unpack_value<float>(raw_fill_value),
        unpack_plane<uint8_t>(raw_gather_kernel, gather_kernel_dimension));
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::ArrayBase::DType vosa::Expand<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::Expand<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {

  const EigenImage<T> &input_image = inputs[0]->template get_image<T>();
  const EigenImage<uint8_t> &expand_kernel =
      inputs[1]->template get_image<uint8_t>();

  uint32_t input_height = input_image.dimension(0);
  uint32_t input_width = input_image.dimension(1);
  uint32_t channels = input_image.dimension(2);

  uint32_t expand_kernel_height = expand_kernel.dimension(0);
  uint32_t expand_kernel_width = expand_kernel.dimension(1);
  uint32_t expand_kernel_channels = expand_kernel.dimension(2);
  assert(expand_kernel_channels == 1);

  uint32_t gather_kernel_height = gather_kernel.dimension(0);
  uint32_t gather_kernel_width = gather_kernel.dimension(1);

  assert(input_height % gather_kernel_height == 0);
  assert(input_width % gather_kernel_width == 0);

  uint32_t output_height =
      input_height * expand_kernel_height / gather_kernel_height;
  uint32_t output_width =
      input_width * expand_kernel_width / gather_kernel_width;

  auto output_image = EigenImage<T>(output_height, output_width, channels);
  output_image.setConstant(fill_value);

  uint32_t height_tiles = input_height / gather_kernel_height;
  uint32_t width_tiles = input_width / gather_kernel_width;

  for (uint32_t height_tile_index = 0; height_tile_index < height_tiles;
       height_tile_index++) {
    for (uint32_t width_tile_index = 0; width_tile_index < width_tiles;
         width_tile_index++) {
      for (uint32_t channel_index = 0; channel_index < channels;
           channel_index++) {

        std::unordered_map<uint8_t, T> gather_map;
        for (uint32_t gather_height_index = 0;
             gather_height_index < gather_kernel_height;
             gather_height_index++) {
          for (uint32_t gather_width_index = 0;
               gather_width_index < gather_kernel_width; gather_width_index++) {
            uint8_t gather_map_index =
                gather_kernel(gather_height_index, gather_width_index);
            assert(gather_map.find(gather_map_index) ==
                   gather_map.end()); // Key not already in map
            if (gather_map_index > 0) {
              uint32_t input_height_coordinate =
                  height_tile_index * gather_kernel_height +
                  gather_height_index;
              uint32_t input_width_coordinate =
                  width_tile_index * gather_kernel_width + gather_width_index;
              gather_map[gather_map_index] =
                  input_image(input_height_coordinate, input_width_coordinate,
                              channel_index);
            }
          }
        }

        for (uint32_t expand_height_index = 0;
             expand_height_index < expand_kernel_height;
             expand_height_index++) {
          for (uint32_t expand_width_index = 0;
               expand_width_index < expand_kernel_width; expand_width_index++) {
            uint8_t gather_map_index =
                expand_kernel(expand_height_index, expand_width_index, 0);
            if (gather_map_index > 0) {
              assert(gather_map.find(gather_map_index) !=
                     gather_map.end()); // Key set
              uint32_t output_height_coordinate =
                  height_tile_index * expand_kernel_height +
                  expand_height_index;
              uint32_t output_width_coordinate =
                  width_tile_index * expand_kernel_width + expand_width_index;
              output_image(output_height_coordinate, output_width_coordinate,
                           channel_index) = gather_map.at(gather_map_index);
            }
          }
        }
      }
    }
  }

  auto output = std::make_shared<vosa::ArrayBase>(output_image);
  return output;
}

template <>
vosa_serialization::EXPAND_MODE
vosa::Expand<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::EXPAND_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::EXPAND_MODE
vosa::Expand<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::EXPAND_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::EXPAND_MODE
vosa::Expand<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::EXPAND_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::EXPAND_MODE
vosa::Expand<int8_t>::get_serialization_mode() const {
  return vosa_serialization::EXPAND_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::EXPAND_MODE
vosa::Expand<int16_t>::get_serialization_mode() const {
  return vosa_serialization::EXPAND_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::EXPAND_MODE
vosa::Expand<int32_t>::get_serialization_mode() const {
  return vosa_serialization::EXPAND_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::EXPAND_MODE
vosa::Expand<float>::get_serialization_mode() const {
  return vosa_serialization::EXPAND_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Expand<T>::serialize(const std::string &op_name,
                           const std::vector<std::string> &input_names) const {
  auto expand_attribute =
      std::make_unique<vosa_serialization::EXPAND_ATTRIBUTET>();

  expand_attribute->gather_kernel_dimension = {
      (uint32_t)gather_kernel.dimension(0),
      (uint32_t)gather_kernel.dimension(1)};
  auto gather_kernel_size = gather_kernel.size();
  std::vector<uint8_t> value_byte_vector;
  auto byte_filter_ptr = (const uint8_t *)gather_kernel.data();
  value_byte_vector.insert(value_byte_vector.end(), byte_filter_ptr,
                           byte_filter_ptr +
                               sizeof(uint8_t) * gather_kernel_size);
  expand_attribute->gather_kernel_value = value_byte_vector;

  expand_attribute->fill_value = pack_value(fill_value);

  vosa_serialization::EXPANDT expand;
  expand.mode = get_serialization_mode();
  expand.attr = std::move(expand_attribute);

  auto expand_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  expand_operator->op.Set(std::move(expand));
  expand_operator->name = op_name;
  expand_operator->inputs = input_names;
  return expand_operator;
}

template class vosa::Expand<uint8_t>;

template class vosa::Expand<uint16_t>;

template class vosa::Expand<uint32_t>;

template class vosa::Expand<int8_t>;

template class vosa::Expand<int16_t>;

template class vosa::Expand<int32_t>;

template class vosa::Expand<float>;