/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "crop.h"

std::shared_ptr<vosa::Op> vosa::CropBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_crop = vosa_operator_union.AsCROP();
  assert(serialized_crop);

  std::vector<uint32_t> crop_window = serialized_crop->attr->window;

  switch (serialized_crop->mode) {
  case vosa_serialization::CROP_MODE_I_I_UINT8_UINT8:
    return std::make_shared<vosa::Crop<uint8_t>>(crop_window);
  case vosa_serialization::CROP_MODE_I_I_UINT16_UINT16:
    return std::make_shared<vosa::Crop<uint16_t>>(crop_window);
  case vosa_serialization::CROP_MODE_I_I_UINT32_UINT32:
    return std::make_shared<vosa::Crop<uint32_t>>(crop_window);
  case vosa_serialization::CROP_MODE_I_I_INT8_INT8:
    return std::make_shared<vosa::Crop<int8_t>>(crop_window);
  case vosa_serialization::CROP_MODE_I_I_INT16_INT16:
    return std::make_shared<vosa::Crop<int16_t>>(crop_window);
  case vosa_serialization::CROP_MODE_I_I_INT32_INT32:
    return std::make_shared<vosa::Crop<int32_t>>(crop_window);
  case vosa_serialization::CROP_MODE_I_I_FLOAT32_FLOAT32:
    return std::make_shared<vosa::Crop<float>>(crop_window);
  default:
    throw OperatorModeNotImplementedError();
  }
}

template <typename T>
vosa::ArrayBase::DType vosa::Crop<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::Crop<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::CROP_MODE
vosa::Crop<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::CROP_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::CROP_MODE
vosa::Crop<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::CROP_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::CROP_MODE
vosa::Crop<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::CROP_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::CROP_MODE
vosa::Crop<int8_t>::get_serialization_mode() const {
  return vosa_serialization::CROP_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::CROP_MODE
vosa::Crop<int16_t>::get_serialization_mode() const {
  return vosa_serialization::CROP_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::CROP_MODE
vosa::Crop<int32_t>::get_serialization_mode() const {
  return vosa_serialization::CROP_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::CROP_MODE
vosa::Crop<float>::get_serialization_mode() const {
  return vosa_serialization::CROP_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Crop<T>::serialize(const std::string &op_name,
                         const std::vector<std::string> &input_names) const {
  auto crop_attribute = std::make_unique<vosa_serialization::CROP_ATTRIBUTET>();
  crop_attribute->window = crop_window;

  vosa_serialization::CROPT crop;
  crop.mode = get_serialization_mode();
  crop.attr = std::move(crop_attribute);

  auto crop_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  crop_operator->op.Set(std::move(crop));
  crop_operator->name = op_name;
  crop_operator->inputs = input_names;
  return crop_operator;
}

template <typename T>
vosa::EigenImage<T> vosa::Crop<T>::runme(const EigenImage<T> &input) const {

  vosa::EigenImage<T> output{crop_window[2], crop_window[3],
                             input.dimension(2)};

  for (uint32_t h = 0; h < crop_window[2]; ++h) {
    for (uint32_t w = 0; w < crop_window[3]; ++w) {
      for (uint32_t ch = 0; ch < input.dimension(0); ++ch) {
        output(h, w, ch) = input(h + crop_window[0], w + crop_window[1], ch);
      }
    }
  }

  return output;
}

template class vosa::Crop<uint8_t>;

template class vosa::Crop<uint16_t>;

template class vosa::Crop<uint32_t>;

template class vosa::Crop<int8_t>;

template class vosa::Crop<int16_t>;

template class vosa::Crop<int32_t>;

template class vosa::Crop<float>;