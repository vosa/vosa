/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "reduce_max_channelwise.h"

std::shared_ptr<vosa::Op> vosa::ReduceMaxChannelwiseBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_reduce_max_channelwise =
      vosa_operator_union.AsREDUCE_MAX_CHANNELWISE();
  assert(serialized_reduce_max_channelwise);
  auto mode = serialized_reduce_max_channelwise->mode;
  switch (mode) {
  case vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::ReduceMaxChannelwise<uint8_t>>();
  }
  case vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::ReduceMaxChannelwise<uint16_t>>();
  }
  case vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::ReduceMaxChannelwise<uint32_t>>();
  }
  case vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::ReduceMaxChannelwise<int8_t>>();
  }
  case vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::ReduceMaxChannelwise<int16_t>>();
  }
  case vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::ReduceMaxChannelwise<int32_t>>();
  }
  case vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::ReduceMaxChannelwise<float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::ReduceMaxChannelwise<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();

  uint32_t height = input.dimension(0);
  uint32_t width = input.dimension(1);
  uint32_t channels = input.dimension(2);

  auto typed_output = EigenImage<T>(height, width, 1);

  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < channels;
           channel_index++) {
        T input_value = input(height_index, width_index, channel_index);
        if (channel_index == 0) {
          typed_output(height_index, width_index, 0) = input_value;
        } else {
          T current_value = typed_output(height_index, width_index, 0);
          T next_value = std::max(current_value, input_value);
          typed_output(height_index, width_index, 0) = next_value;
        }
      }
    }
  }

  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::ReduceMaxChannelwise<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <>
vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE
vosa::ReduceMaxChannelwise<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE
vosa::ReduceMaxChannelwise<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE
vosa::ReduceMaxChannelwise<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE
vosa::ReduceMaxChannelwise<int8_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE
vosa::ReduceMaxChannelwise<int16_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE
vosa::ReduceMaxChannelwise<int32_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE
vosa::ReduceMaxChannelwise<float>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::ReduceMaxChannelwise<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  vosa_serialization::REDUCE_MAX_CHANNELWISET reduce_max_channelwise;
  reduce_max_channelwise.mode = get_serialization_mode();

  auto reduce_max_channelwise_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  reduce_max_channelwise_operator->op.Set(reduce_max_channelwise);
  reduce_max_channelwise_operator->name = op_name;
  reduce_max_channelwise_operator->inputs = input_names;
  return reduce_max_channelwise_operator;
}

template class vosa::ReduceMaxChannelwise<uint8_t>;

template class vosa::ReduceMaxChannelwise<uint16_t>;

template class vosa::ReduceMaxChannelwise<uint32_t>;

template class vosa::ReduceMaxChannelwise<int8_t>;

template class vosa::ReduceMaxChannelwise<int16_t>;

template class vosa::ReduceMaxChannelwise<int32_t>;

template class vosa::ReduceMaxChannelwise<float>;