// run command without FileCheck to see output (or add "--dump-input always" FileCheck option)
// RUN: vosa-opt -vosa-to-tosa %s | %test_src_root/e2e-runner | FileCheck %s

func.func @main() {
  // CHECK: sizes = [5, 10]
  // CHECK-NEXT:   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  // CHECK-NEXT:   [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  // CHECK-NEXT:   [2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
  // CHECK-NEXT:   [3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
  // CHECK-NEXT:   [4, 4, 4, 4, 4, 4, 4, 4, 4, 4]]
  // CHECK: sizes = [5, 10]
  // CHECK-NEXT:   [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
  // CHECK-NEXT:   [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
  // CHECK-NEXT:   [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
  // CHECK-NEXT:   [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
  // CHECK-NEXT:   [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]

  %res = vosa.mesh_grid {shape = [ 5 : i32, 10 : i32 ]} : tensor<5x10x2xui32>
  %res1 = builtin.unrealized_conversion_cast %res : tensor<5x10x2xui32> to tensor<5x10x2xi32>
  %unranked = tensor.cast %res1 : tensor<5x10x2xi32> to tensor<?x?x?xi32>
  func.call @print_img_i32(%unranked) : (tensor<?x?x?xi32>) -> ()

  return
}

func.func @print_img_i32(%a : tensor<?x?x?xi32>) {
  // %unranked_all = tensor.cast %a : tensor<?x?x?xi32> to tensor<*xi32>
  // func.call @print_memref_i32(%unranked_all) : (tensor<*xi32>) -> ()
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c0 : tensor<?x?x?xi32>
  %w = tensor.dim %a, %c1 : tensor<?x?x?xi32>
  %nch = tensor.dim %a, %c2 : tensor<?x?x?xi32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[0, 0, %c][%h, %w, 1][1, 1, 1]  : tensor<?x?x?xi32> to tensor<?x?x1xi32>
     %chdata_c = tensor.collapse_shape %chdata [[0], [1, 2]] : tensor<?x?x1xi32> into tensor<?x?xi32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xi32> to tensor<*xi32>
     func.call @printMemrefI32(%unranked_chdata) : (tensor<*xi32>) -> ()
  }
  return
}

func.func private @printMemrefI32(%ptr : tensor<*xi32> {bufferization.access = "read"})
