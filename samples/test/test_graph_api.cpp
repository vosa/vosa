/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/api/graph_builder.h"
#include "vosa/api/ops.h"

int main() {

  const std::string infile = "";
  const std::string pipeline_name = "pipeline_name";
  vosa::api::GraphBuilder graph_builder(infile, pipeline_name);
  //
  auto input_0 = vosa::api::input(
      graph_builder, vosa::api::ArrayBase::DType::UINT8, {1920, 1080, 3});
  auto input_1 = vosa::api::input(
      graph_builder, vosa::api::ArrayBase::DType::UINT8, {1920, 1080, 3});
  auto add = vosa::api::add(graph_builder, input_0, input_1);

  auto add_name = graph_builder.get_name_for_op(add);
  return graph_builder.compile({add_name}, "test-graph-api.vmfb");
}