/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/Builder/mlir_fbs_builder.h"
#include "vosa/Builder/mlir_builder.h"
#include "vosa_generated.h"

#include "vosa/Dialect/Vosa/IR/VosaDialect.h"
#include "vosa/Dialect/Vosa/IR/VosaOps.h"
#include "vosa/Dialect/Vosa/Utils/DataFormat.h"

#include "mlir/Dialect/Arith/IR/Arith.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/Dialect/Tosa/IR/TosaOps.h"
#include "mlir/IR/Builders.h"

#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#define DEBUG_TYPE "vosa-fbs-builder"

#include <cstdint>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace mlir::vosa;

namespace vosa {

void addInputs(MLIRBuilder &builder, const std::vector<std::string> &inputs) {
  for (const auto &inp : inputs) {
    llvm::SmallVector<llvm::StringRef, 3> parts;
    llvm::StringRef inpRef(inp);
    inpRef.split(parts, ':');
    auto name = parts[0];
    dtype_t ty = dtype_t::VOSA_NONE;
    if (parts.size() > 1) {
      auto tyName = parts[1];
      if (tyName == "float32")
        ty = dtype_t::VOSA_FLOAT32;
      else if (tyName == "uint8")
        ty = dtype_t::VOSA_UINT8;
      else if (tyName == "uint16")
        ty = dtype_t::VOSA_UINT16;
      else if (tyName == "uint32")
        ty = dtype_t::VOSA_UINT32;
      else if (tyName == "int8")
        ty = dtype_t::VOSA_INT8;
      else if (tyName == "int16")
        ty = dtype_t::VOSA_INT16;
      else if (tyName == "int32")
        ty = dtype_t::VOSA_INT32;
      else if (tyName == "bool")
        ty = dtype_t::VOSA_BOOL;
      else {
        llvm::errs() << "Unsupported data type " << tyName << "\n";
      }
    }
    if (parts.size() > 2) {
      llvm::SmallVector<int64_t> shape;
      llvm::SmallVector<llvm::StringRef> sizeParts;
      parts[2].split(sizeParts, 'x');
      bool ok = true;
      for (auto d : sizeParts) {
        int64_t n = -1;
        if (d.getAsInteger(0, n)) {
          llvm::errs() << "Cannot parse dimension " << d << "," << n << "\n";
          ok = false;
          break;
        }
        shape.push_back(n);
      }
      if (ok) {
        builder.Input(name.str(), ty, shape);
      }
    } else {
      builder.Input(name.str(), ty);
    }
  }
}

/// Determine the order that operators must be evaluated to ensure that each
/// operators inputs are computed before the operator is evaluated.
/// Returns a vector of indexes into the operator list
std::vector<unsigned>
get_ordered_ops(const vosa_serialization::VosaGraphT &graph) {
  const auto &ops = graph.operators;

  // generate topological sort from outputs to inputs
  // Use Kahn's algorithm
  // <https://en.wikipedia.org/wiki/Topological_sorting>
  // as this is reverse order (output towards input), the edge order is
  // also reversed, so we start with the set of nodes with no *outgoing*
  // edges - i.e. the output nodes

  // map op names to indexes
  std::unordered_map<std::string, unsigned> op_indexes;
  for (unsigned i = 0; i < ops.size(); ++i) {
    const auto &op = ops[i];
    op_indexes[op->name] = i;
  }

  // build map of uses (i.e. output -> input edges) of each node
  std::unordered_map<unsigned, std::set<unsigned>> uses;
  for (unsigned i = 0; i < ops.size(); ++i) {
    const auto &op = ops[i];
    for (const auto &inp : op->inputs) {
      uses[op_indexes[inp]].insert(i);
    }
  }

  // initialise the search set with nodes that have no uses (i.e. outputs)
  std::set<unsigned> search_nodes;
  for (unsigned i = 0; i < ops.size(); ++i) {
    if (uses.find(i) == uses.end()) {
      search_nodes.insert(i);
    }
  }

  // Use Kahn's algorithm
  std::vector<unsigned> sorted_ops;
  while (!search_nodes.empty()) {
    // take a node from the search list
    auto iNode = search_nodes.begin();
    auto n = *iNode;
    // move it to the output list
    search_nodes.erase(iNode);
    sorted_ops.push_back(n);
    const auto &op = ops[n];
    for (const auto &input : op->inputs) {
      // remove that node from the uses of each of it's inputs
      unsigned inputIndex = op_indexes[input];
      uses[inputIndex].erase(n);
      if (uses[inputIndex].empty()) {
        // no other uses of that input: add to search list
        search_nodes.insert(inputIndex);
        uses.erase(inputIndex);
      }
    }
  }

  // reverse to get order of evaluation required
  std::reverse(sorted_ops.begin(), sorted_ops.end());
  return sorted_ops;
}

/// Implementation of flatbuffer deserializer
///
/// Parses the serialized pipeline.
///
/// Templated `OpHandler` is called for each operator in the graph
class Deserializer {
public:
  static bool process(const uint8_t *flatbuffer_data, MLIRBuilder &handler);

private:
  Deserializer(MLIRBuilder &h);
  bool process(const uint8_t *flatbuffer_data);

  // Handlers for each operator - each extracts the parameters from the
  // deserialized data and calls the corresponding function in the handler
  bool process_ImportChannel(const std::string &name,
                             const std::vector<std::string> &inputs,
                             const vosa_serialization::VosaOperatorUnion &op);
  bool process_ResizeBilinear(const std::string &name,
                              const std::vector<std::string> &inputs,
                              const vosa_serialization::VosaOperatorUnion &op);
  bool process_ResizeNearestNeighbour(
      const std::string &name, const std::vector<std::string> &inputs,
      const vosa_serialization::VosaOperatorUnion &op);
  bool process_ConstScalar(const std::string &name,
                           const std::vector<std::string> &inputs,
                           const vosa_serialization::VosaOperatorUnion &op);
  bool process_Conv2D(const std::string &name,
                      const std::vector<std::string> &inputs,
                      const vosa_serialization::VosaOperatorUnion &op);
  bool process_ConstImage(const std::string &name,
                          const std::vector<std::string> &inputs,
                          const vosa_serialization::VosaOperatorUnion &op);
  bool process_Concat(const std::string &name,
                      const std::vector<std::string> &inputs,
                      const vosa_serialization::VosaOperatorUnion &op);
  bool process_MeshGrid(const std::string &name,
                        const std::vector<std::string> &inputs,
                        const vosa_serialization::VosaOperatorUnion &op);
  bool process_Mult(const std::string &name,
                    const std::vector<std::string> &inputs,
                    const vosa_serialization::VosaOperatorUnion &op);
  bool process_Add(const std::string &name,
                   const std::vector<std::string> &inputs,
                   const vosa_serialization::VosaOperatorUnion &op);
  bool process_Sub(const std::string &name,
                   const std::vector<std::string> &inputs,
                   const vosa_serialization::VosaOperatorUnion &op);
  bool process_Pad(const std::string &name,
                   const std::vector<std::string> &inputs,
                   const vosa_serialization::VosaOperatorUnion &op);
  bool process_Clamp(const std::string &name,
                     const std::vector<std::string> &inputs,
                     const vosa_serialization::VosaOperatorUnion &op);
  bool process_Cast(const std::string &name,
                    const std::vector<std::string> &inputs,
                    const vosa_serialization::VosaOperatorUnion &op);
  bool process_Div(const std::string &name,
                   const std::vector<std::string> &inputs,
                   const vosa_serialization::VosaOperatorUnion &op);
  bool
  process_BroadcastChannelwise(const std::string &name,
                               const std::vector<std::string> &inputs,
                               const vosa_serialization::VosaOperatorUnion &op);
  bool
  process_BroadcastPlanewise(const std::string &name,
                             const std::vector<std::string> &inputs,
                             const vosa_serialization::VosaOperatorUnion &op);
  bool process_ChannelExtract(const std::string &name,
                              const std::vector<std::string> &inputs,
                              const vosa_serialization::VosaOperatorUnion &op);
  bool
  process_LogicalShiftRight(const std::string &name,
                            const std::vector<std::string> &inputs,
                            const vosa_serialization::VosaOperatorUnion &op);
  bool process_Abs(const std::string &name,
                   const std::vector<std::string> &inputs,
                   const vosa_serialization::VosaOperatorUnion &op);
  bool
  process_ArgMaxChannelwise(const std::string &name,
                            const std::vector<std::string> &inputs,
                            const vosa_serialization::VosaOperatorUnion &op);
  bool process_ArgMaxPlanewise(const std::string &name,
                               const std::vector<std::string> &inputs,
                               const vosa_serialization::VosaOperatorUnion &op);
  bool
  process_ArgMinChannelwise(const std::string &name,
                            const std::vector<std::string> &inputs,
                            const vosa_serialization::VosaOperatorUnion &op);
  bool process_ArgMinPlanewise(const std::string &name,
                               const std::vector<std::string> &inputs,
                               const vosa_serialization::VosaOperatorUnion &op);
  bool process_ReduceInterpChannelwise(
      const std::string &name, const std::vector<std::string> &inputs,
      const vosa_serialization::VosaOperatorUnion &op);
  bool
  process_ReduceMaxChannelwise(const std::string &name,
                               const std::vector<std::string> &inputs,
                               const vosa_serialization::VosaOperatorUnion &op);
  bool
  process_ReduceMaxPlanewise(const std::string &name,
                             const std::vector<std::string> &inputs,
                             const vosa_serialization::VosaOperatorUnion &op);
  bool
  process_ReduceMinChannelwise(const std::string &name,
                               const std::vector<std::string> &inputs,
                               const vosa_serialization::VosaOperatorUnion &op);
  bool
  process_ReduceMinPlanewise(const std::string &name,
                             const std::vector<std::string> &inputs,
                             const vosa_serialization::VosaOperatorUnion &op);
  bool
  process_ReduceSumChannelwise(const std::string &name,
                               const std::vector<std::string> &inputs,
                               const vosa_serialization::VosaOperatorUnion &op);
  bool
  process_ReduceSumPlanewise(const std::string &name,
                             const std::vector<std::string> &inputs,
                             const vosa_serialization::VosaOperatorUnion &op);
  bool process_AbsDiff(const std::string &name,
                       const std::vector<std::string> &inputs,
                       const vosa_serialization::VosaOperatorUnion &op);
  bool
  process_ArithmeticShiftRight(const std::string &name,
                               const std::vector<std::string> &inputs,
                               const vosa_serialization::VosaOperatorUnion &op);
  bool process_Where(const std::string &name,
                     const std::vector<std::string> &inputs,
                     const vosa_serialization::VosaOperatorUnion &op);
  bool process_Equal(const std::string &name,
                     const std::vector<std::string> &inputs,
                     const vosa_serialization::VosaOperatorUnion &op);
  bool process_PiecewiseLinear(const std::string &name,
                               const std::vector<std::string> &inputs,
                               const vosa_serialization::VosaOperatorUnion &op);
  bool process_Or(const std::string &name,
                  const std::vector<std::string> &inputs,
                  const vosa_serialization::VosaOperatorUnion &op);
  bool process_Mod(const std::string &name,
                   const std::vector<std::string> &inputs,
                   const vosa_serialization::VosaOperatorUnion &op);
  bool process_Not(const std::string &name,
                   const std::vector<std::string> &inputs,
                   const vosa_serialization::VosaOperatorUnion &op);
  bool process_Round(const std::string &name,
                     const std::vector<std::string> &inputs,
                     const vosa_serialization::VosaOperatorUnion &op);
  bool process_Decimate(const std::string &name,
                        const std::vector<std::string> &inputs,
                        const vosa_serialization::VosaOperatorUnion &op);
  bool process_Expand(const std::string &name,
                      const std::vector<std::string> &inputs,
                      const vosa_serialization::VosaOperatorUnion &op);
  bool process_PointwiseMatrixMultiply(
      const std::string &name, const std::vector<std::string> &inputs,
      const vosa_serialization::VosaOperatorUnion &op);
  bool process_Greater(const std::string &name,
                       const std::vector<std::string> &inputs,
                       const vosa_serialization::VosaOperatorUnion &op);
  bool process_GreaterEqual(const std::string &name,
                            const std::vector<std::string> &inputs,
                            const vosa_serialization::VosaOperatorUnion &op);
  bool process_Max(const std::string &name,
                   const std::vector<std::string> &inputs,
                   const vosa_serialization::VosaOperatorUnion &op);
  bool process_Min(const std::string &name,
                   const std::vector<std::string> &inputs,
                   const vosa_serialization::VosaOperatorUnion &op);
  bool process_Sqrt(const std::string &name,
                    const std::vector<std::string> &inputs,
                    const vosa_serialization::VosaOperatorUnion &op);
  bool process_GammaCorrection(const std::string &name,
                               const std::vector<std::string> &inputs,
                               const vosa_serialization::VosaOperatorUnion &op);
  bool process_Power(const std::string &name,
                     const std::vector<std::string> &inputs,
                     const vosa_serialization::VosaOperatorUnion &op);
  bool process_Rotate90(const std::string &name,
                        const std::vector<std::string> &inputs,
                        const vosa_serialization::VosaOperatorUnion &op);
  bool process_TosaGraph(const std::string &name,
                         const std::vector<std::string> &inputs,
                         const vosa_serialization::VosaOperatorUnion &op);

  bool process_Input(const std::string &name,
                     const std::vector<std::string> &inputs,
                     const vosa_serialization::VosaOperatorUnion &op);
  bool process_CustomScalar(const std::string &name,
                            const std::vector<std::string> &inputs,
                            const vosa_serialization::VosaOperatorUnion &op);
  bool process_UnknownOperator(const std::string &name,
                               const std::vector<std::string> &inputs,
                               const vosa_serialization::VosaOperatorUnion &op);

  MLIRBuilder &builder;
};

template <typename T>
T unpackValue(const std::vector<uint8_t> &raw_value) {
  auto uint8_ptr = new uint8_t[sizeof(T)];
  std::copy(raw_value.begin(), raw_value.end(), uint8_ptr);
  T output = *((T *)uint8_ptr);
  delete[] uint8_ptr;
  return output;
}

// copy to a 1D vector of T
template <typename T>
std::vector<T> unpackValues(const std::vector<uint8_t> &raw_value) {
  std::vector<T> output(raw_value.size() / sizeof(T));
  assert(output.size() * sizeof(T) == raw_value.size());
  std::copy(raw_value.begin(), raw_value.end(), (uint8_t *)output.data());
  return output;
}

// copy to a ND vector of T
template <typename T>
std::vector<T> unpackValues(const std::vector<uint32_t> &dimension,
                            const std::vector<uint8_t> &raw_value) {
  assert(dimension[0] * dimension[1] * sizeof(T) == raw_value.size());
  std::vector<T> output(dimension[0] * dimension[1]);
  std::copy(raw_value.begin(), raw_value.end(), (uint8_t *)output.data());
  return output;
}

inline dtype_t mapDType(vosa_serialization::DTYPE ty) {
  switch (ty) {
  case vosa_serialization::DTYPE_BOOL: {
    return dtype_t::VOSA_BOOL;
  }
  case vosa_serialization::DTYPE_UINT8: {
    return dtype_t::VOSA_UINT8;
  }
  case vosa_serialization::DTYPE_UINT16: {
    return dtype_t::VOSA_UINT16;
  }
  case vosa_serialization::DTYPE_UINT32: {
    return dtype_t::VOSA_UINT32;
  }
  case vosa_serialization::DTYPE_INT8: {
    return dtype_t::VOSA_INT8;
  }
  case vosa_serialization::DTYPE_INT16: {
    return dtype_t::VOSA_INT16;
  }
  case vosa_serialization::DTYPE_INT32: {
    return dtype_t::VOSA_INT32;
  }
  case vosa_serialization::DTYPE_FLOAT32: {
    return dtype_t::VOSA_FLOAT32;
  }
  default:
    return dtype_t::VOSA_NONE;
  }
}

bool Deserializer::process_ImportChannel(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_import_channel = op.AsIMPORT_CHANNEL();
  assert(serialized_import_channel);
  auto mode = serialized_import_channel->mode;
  auto serialized_attributes = &serialized_import_channel->attr;
  uint32_t stride = serialized_attributes->get()->stride;
  uint32_t offset = serialized_attributes->get()->offset;
  std::vector<uint32_t> shape = serialized_attributes->get()->shape;
  switch (mode) {
  case vosa_serialization::IMPORT_CHANNEL_MODE_D_I_UINT8_UINT8: {
    return builder.ImportChannel(name, dtype_t::VOSA_UINT8, inputs, stride,
                                 offset, shape);
  }
  case vosa_serialization::IMPORT_CHANNEL_MODE_D_I_UINT16_UINT16: {
    return builder.ImportChannel(name, dtype_t::VOSA_UINT16, inputs, stride,
                                 offset, shape);
  }
  case vosa_serialization::IMPORT_CHANNEL_MODE_D_I_UINT32_UINT32: {
    return builder.ImportChannel(name, dtype_t::VOSA_UINT32, inputs, stride,
                                 offset, shape);
  }
  case vosa_serialization::IMPORT_CHANNEL_MODE_D_I_INT8_INT8: {
    return builder.ImportChannel(name, dtype_t::VOSA_INT8, inputs, stride,
                                 offset, shape);
  }
  case vosa_serialization::IMPORT_CHANNEL_MODE_D_I_INT16_INT16: {
    return builder.ImportChannel(name, dtype_t::VOSA_INT16, inputs, stride,
                                 offset, shape);
  }
  case vosa_serialization::IMPORT_CHANNEL_MODE_D_I_INT32_INT32: {
    return builder.ImportChannel(name, dtype_t::VOSA_INT32, inputs, stride,
                                 offset, shape);
  }
  case vosa_serialization::IMPORT_CHANNEL_MODE_D_I_FLOAT32_FLOAT32: {
    return builder.ImportChannel(name, dtype_t::VOSA_FLOAT32, inputs, stride,
                                 offset, shape);
  }
  default: {
    return builder.ReportError("Unsupported mode for ImportChannel");
  }
  }
}

bool Deserializer::process_ResizeBilinear(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {

  auto serialized_resize_bilinear = op.AsRESIZE_BILINEAR();
  assert(serialized_resize_bilinear);
  auto mode = serialized_resize_bilinear->mode;
  auto serialized_attributes = &serialized_resize_bilinear->attr;
  std::vector<uint32_t> resize_size = serialized_attributes->get()->size;
  switch (mode) {
  case vosa_serialization::RESIZE_BILINEAR_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.ResizeBilinear(name, dtype_t::VOSA_FLOAT32, inputs,
                                  resize_size);
  }
  default: {
    return builder.ReportError("Unsupported mode for ResizeBilinear");
  }
  }
}

bool Deserializer::process_ResizeNearestNeighbour(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_resize_nn = op.AsRESIZE_NEAREST_NEIGHBOUR();
  assert(serialized_resize_nn);
  auto mode = serialized_resize_nn->mode;
  auto serialized_attributes = &serialized_resize_nn->attr;
  std::vector<uint32_t> resize_size = serialized_attributes->get()->size;
  switch (mode) {
  case vosa_serialization::RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.ResizeNearestNeighbour(name, dtype_t::VOSA_FLOAT32, inputs,
                                          resize_size);
  }
  default: {
    return builder.ReportError("Unsupported mode for ResizeNearestNeighbour");
  }
  }
}

bool Deserializer::process_ConstScalar(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_const_scalar = op.AsCONST_SCALAR();
  assert(serialized_const_scalar);
  auto mode = serialized_const_scalar->mode;
  auto serialized_attributes = &serialized_const_scalar->attr;
  std::vector<uint8_t> raw_value = serialized_attributes->get()->value;
  switch (mode) {
  case vosa_serialization::CONST_SCALAR_MODE_S_UINT8: {
    return builder.ConstScalar(name, dtype_t::VOSA_UINT8,
                               unpackValue<uint8_t>(raw_value));
  }
  case vosa_serialization::CONST_SCALAR_MODE_S_UINT16: {
    return builder.ConstScalar(name, dtype_t::VOSA_UINT16,
                               unpackValue<uint16_t>(raw_value));
  }
  case vosa_serialization::CONST_SCALAR_MODE_S_UINT32: {
    return builder.ConstScalar(name, dtype_t::VOSA_UINT32,
                               unpackValue<uint32_t>(raw_value));
  }
  case vosa_serialization::CONST_SCALAR_MODE_S_INT8: {
    return builder.ConstScalar(name, dtype_t::VOSA_INT8,
                               unpackValue<int8_t>(raw_value));
  }
  case vosa_serialization::CONST_SCALAR_MODE_S_INT16: {
    return builder.ConstScalar(name, dtype_t::VOSA_INT16,
                               unpackValue<int16_t>(raw_value));
  }
  case vosa_serialization::CONST_SCALAR_MODE_S_INT32: {
    return builder.ConstScalar(name, dtype_t::VOSA_INT32,
                               unpackValue<int32_t>(raw_value));
  }
  case vosa_serialization::CONST_SCALAR_MODE_S_FLOAT32: {
    return builder.ConstScalar(name, dtype_t::VOSA_FLOAT32,
                               unpackValue<float>(raw_value));
  }
  default: {
    return builder.ReportError("Unsupported mode for ConstScalar");
  }
  }
}

bool Deserializer::process_Conv2D(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_conv = op.AsCONV_2D();
  assert(serialized_conv);
  auto mode = serialized_conv->mode;
  auto serialized_attributes = &serialized_conv->attr;
  std::vector<uint8_t> filter_value =
      serialized_attributes->get()->filter_value;
  std::vector<uint32_t> filter_dimension =
      serialized_attributes->get()->filter_dimension;

  switch (mode) {
  case vosa_serialization::CONV_2D_MODE_I_I_UINT8_UINT8: {
    return builder.template Conv2D<uint8_t>(
        name, dtype_t::VOSA_UINT8, dtype_t::VOSA_UINT8, inputs,
        filter_dimension,
        unpackValues<uint8_t>(filter_dimension, filter_value));
  }
  case vosa_serialization::CONV_2D_MODE_I_I_UINT16_UINT16: {
    return builder.template Conv2D<uint16_t>(
        name, dtype_t::VOSA_UINT16, dtype_t::VOSA_UINT16, inputs,
        filter_dimension,
        unpackValues<uint16_t>(filter_dimension, filter_value));
  }
  case vosa_serialization::CONV_2D_MODE_I_I_UINT32_UINT32: {
    return builder.template Conv2D<uint32_t>(
        name, dtype_t::VOSA_UINT32, dtype_t::VOSA_UINT32, inputs,
        filter_dimension,
        unpackValues<uint32_t>(filter_dimension, filter_value));
  }
  case vosa_serialization::CONV_2D_MODE_I_I_INT8_INT8: {
    return builder.template Conv2D<int8_t>(
        name, dtype_t::VOSA_INT8, dtype_t::VOSA_INT8, inputs, filter_dimension,
        unpackValues<int8_t>(filter_dimension, filter_value));
  }
  case vosa_serialization::CONV_2D_MODE_I_I_INT16_INT16: {
    return builder.template Conv2D<int16_t>(
        name, dtype_t::VOSA_INT16, dtype_t::VOSA_INT16, inputs,
        filter_dimension,
        unpackValues<int16_t>(filter_dimension, filter_value));
  }
  case vosa_serialization::CONV_2D_MODE_I_I_INT32_INT32: {
    return builder.template Conv2D<int32_t>(
        name, dtype_t::VOSA_INT32, dtype_t::VOSA_INT32, inputs,
        filter_dimension,
        unpackValues<int32_t>(filter_dimension, filter_value));
  }
  case vosa_serialization::CONV_2D_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.template Conv2D<float>(
        name, dtype_t::VOSA_FLOAT32, dtype_t::VOSA_FLOAT32, inputs,
        filter_dimension, unpackValues<float>(filter_dimension, filter_value));
  }
  default: {
    return builder.ReportError("Unsupported mode for Conv2D");
  }
  }
}

bool Deserializer::process_ConstImage(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_const_image = op.AsCONST_IMAGE();
  assert(serialized_const_image);
  auto mode = serialized_const_image->mode;
  auto serialized_attributes = &serialized_const_image->attr;
  std::vector<uint8_t> raw_value = serialized_attributes->get()->value;
  std::vector<uint32_t> dimensions = serialized_attributes->get()->dimension;
  switch (mode) {
  case vosa_serialization::CONST_IMAGE_MODE_I_FLOAT32: {
    return builder.template ConstImage(name, dtype_t::VOSA_FLOAT32, dimensions,
                                       unpackValue<float>(raw_value));
  }
  default: {
    return builder.ReportError("Unsupported mode for ConstImage");
  }
  }
}

bool Deserializer::process_Concat(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_concat = op.AsCONCAT();
  assert(serialized_concat);
  auto mode = serialized_concat->mode;
  switch (mode) {
  case vosa_serialization::CONCAT_MODE_I_I_UINT8_UINT8: {
    return builder.Concat(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::CONCAT_MODE_I_I_UINT16_UINT16: {
    return builder.Concat(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::CONCAT_MODE_I_I_UINT32_UINT32: {
    return builder.Concat(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::CONCAT_MODE_I_I_INT8_INT8: {
    return builder.Concat(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::CONCAT_MODE_I_I_INT16_INT16: {
    return builder.Concat(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::CONCAT_MODE_I_I_INT32_INT32: {
    return builder.Concat(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::CONCAT_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Concat(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported mode for Concat");
  }
  }
}

bool Deserializer::process_MeshGrid(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_mesh_grid = op.AsMESH_GRID();
  assert(serialized_mesh_grid);
  auto mode = serialized_mesh_grid->mode;
  auto serialized_attributes = &serialized_mesh_grid->attr;
  std::vector<uint32_t> shape = serialized_attributes->get()->dimensions;
  switch (mode) {
  case vosa_serialization::MESH_GRID_MODE_I_UINT32: {
    return builder.MeshGrid(name, dtype_t::VOSA_UINT32, inputs, shape);
  }
  }
}

bool Deserializer::process_Mult(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_mult = op.AsMULT();
  assert(serialized_mult);
  auto mode = serialized_mult->mode;
  switch (mode) {
  case vosa_serialization::MULT_MODE_I_I_UINT8_UINT8: {
    return builder.Mult(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::MULT_MODE_I_I_UINT16_UINT16: {
    return builder.Mult(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::MULT_MODE_I_I_UINT32_UINT32: {
    return builder.Mult(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::MULT_MODE_I_I_INT8_INT8: {
    return builder.Mult(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::MULT_MODE_I_I_INT16_INT16: {
    return builder.Mult(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::MULT_MODE_I_I_INT32_INT32: {
    return builder.Mult(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::MULT_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Mult(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported mode for Mult");
  }
  }
}

bool Deserializer::process_Add(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_add = op.AsADD();
  assert(serialized_add);
  auto mode = serialized_add->mode;
  switch (mode) {
  case vosa_serialization::ADD_MODE_I_I_UINT8_UINT8: {
    return builder.Add(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::ADD_MODE_I_I_UINT16_UINT16: {
    return builder.Add(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::ADD_MODE_I_I_UINT32_UINT32: {
    return builder.Add(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::ADD_MODE_I_I_INT8_INT8: {
    return builder.Add(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::ADD_MODE_I_I_INT16_INT16: {
    return builder.Add(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::ADD_MODE_I_I_INT32_INT32: {
    return builder.Add(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::ADD_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Add(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported mode for Add");
  }
  }
}

bool Deserializer::process_Sub(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_sub = op.AsSUB();
  assert(serialized_sub);
  auto mode = serialized_sub->mode;
  switch (mode) {
  case vosa_serialization::SUB_MODE_I_I_UINT8_UINT8: {
    return builder.Sub(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::SUB_MODE_I_I_UINT16_UINT16: {
    return builder.Sub(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::SUB_MODE_I_I_UINT32_UINT32: {
    return builder.Sub(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::SUB_MODE_I_I_INT8_INT8: {
    return builder.Sub(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::SUB_MODE_I_I_INT16_INT16: {
    return builder.Sub(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::SUB_MODE_I_I_INT32_INT32: {
    return builder.Sub(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::SUB_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Sub(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported mode for Sub");
  }
  }
}

bool Deserializer::process_Pad(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_pad = op.AsPAD();
  assert(serialized_pad);
  auto mode = serialized_pad->mode;
  auto serialized_attributes = &serialized_pad->attr;
  std::vector<uint8_t> pad_constant =
      serialized_attributes->get()->pad_constant;
  std::vector<uint32_t> pad_size = serialized_attributes->get()->pad_size;
  auto serialized_pad_mode = serialized_attributes->get()->pad_method_mode;

  pad_mode_t pad_mode;
  switch (serialized_pad_mode) {
  case vosa_serialization::PAD_METHOD_MODE_REPLICATE: {
    pad_mode = pad_mode_t::VOSA_PAD_MODE_REPLICATE;
    break;
  }
  case vosa_serialization::PAD_METHOD_MODE_CONSTANT: {
    pad_mode = pad_mode_t::VOSA_PAD_MODE_CONSTANT;
    break;
  }
  case vosa_serialization::PAD_METHOD_MODE_REFLECT: {
    pad_mode = pad_mode_t::VOSA_PAD_MODE_REFLECT;
    break;
  }
  default: {
    return builder.ReportError("Unsupported pad mode for Pad");
  }
  }

  switch (mode) {
  case vosa_serialization::PAD_MODE_I_I_UINT8_UINT8: {
    return builder.template Pad<uint8_t>(name, dtype_t::VOSA_UINT8, inputs,
                                         unpackValue<uint8_t>(pad_constant),
                                         pad_mode, pad_size);
  }
  case vosa_serialization::PAD_MODE_I_I_UINT16_UINT16: {
    return builder.template Pad<uint16_t>(name, dtype_t::VOSA_UINT16, inputs,
                                          unpackValue<uint16_t>(pad_constant),
                                          pad_mode, pad_size);
  }
  case vosa_serialization::PAD_MODE_I_I_UINT32_UINT32: {
    return builder.template Pad<uint32_t>(name, dtype_t::VOSA_UINT32, inputs,
                                          unpackValue<uint32_t>(pad_constant),
                                          pad_mode, pad_size);
  }
  case vosa_serialization::PAD_MODE_I_I_INT8_INT8: {
    return builder.template Pad<int8_t>(name, dtype_t::VOSA_INT8, inputs,
                                        unpackValue<int8_t>(pad_constant),
                                        pad_mode, pad_size);
  }
  case vosa_serialization::PAD_MODE_I_I_INT16_INT16: {
    return builder.template Pad<int16_t>(name, dtype_t::VOSA_INT16, inputs,
                                         unpackValue<int16_t>(pad_constant),
                                         pad_mode, pad_size);
  }
  case vosa_serialization::PAD_MODE_I_I_INT32_INT32: {
    return builder.template Pad<int32_t>(name, dtype_t::VOSA_INT32, inputs,
                                         unpackValue<int32_t>(pad_constant),
                                         pad_mode, pad_size);
  }
  case vosa_serialization::PAD_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.template Pad<float>(name, dtype_t::VOSA_FLOAT32, inputs,
                                       unpackValue<float>(pad_constant),
                                       pad_mode, pad_size);
  }
  default: {
    return builder.ReportError("Unsupported mode for Pad");
  }
  }
}

bool Deserializer::process_Clamp(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_clamp = op.AsCLAMP();
  assert(serialized_clamp);
  auto mode = serialized_clamp->mode;
  switch (mode) {
  case vosa_serialization::CLAMP_MODE_I_I_UINT8_UINT8: {
    return builder.Clamp(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::CLAMP_MODE_I_I_UINT16_UINT16: {
    return builder.Clamp(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::CLAMP_MODE_I_I_UINT32_UINT32: {
    return builder.Clamp(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::CLAMP_MODE_I_I_INT8_INT8: {
    return builder.Clamp(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::CLAMP_MODE_I_I_INT16_INT16: {
    return builder.Clamp(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::CLAMP_MODE_I_I_INT32_INT32: {
    return builder.Clamp(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::CLAMP_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Clamp(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported mode for Clamp");
  }
  }
}

bool Deserializer::process_Cast(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_cast = op.AsCAST();
  assert(serialized_cast);
  const auto &attr = serialized_cast->attr;
  const auto &cast_method = attr->cast_method;
  const auto &output_dtype = serialized_cast->output_type;

  rounding_mode_t rounding_mode;
  switch (cast_method) {
  case (vosa_serialization::CAST_METHOD_SATURATE): {
    rounding_mode = rounding_mode_t::VOSA_ROUNDING_MODE_SATURATE;
    break;
  }
  case (vosa_serialization::CAST_METHOD_WRAP): {
    rounding_mode = rounding_mode_t::VOSA_ROUNDING_MODE_WRAP;
    break;
  }
  }

  dtype_t outType = mapDType(output_dtype);
  return builder.Cast(name, outType, inputs, rounding_mode);
}

bool Deserializer::process_Div(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_div = op.AsDIV();
  assert(serialized_div);
  auto mode = serialized_div->mode;
  switch (mode) {
  case vosa_serialization::DIV_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Div(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported data type for Div");
  }
  }
}

bool Deserializer::process_BroadcastChannelwise(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_broadcast_channelwise = op.AsBROADCAST_CHANNELWISE();
  assert(serialized_broadcast_channelwise);
  auto mode = serialized_broadcast_channelwise->mode;
  auto serialized_attributes = &serialized_broadcast_channelwise->attr;
  uint32_t channel_size = serialized_attributes->get()->size;
  switch (mode) {
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_BOOL_BOOL: {
    return builder.BroadcastChannelwise(name, dtype_t::VOSA_BOOL, inputs,
                                        channel_size);
  }
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_UINT8_UINT8: {
    return builder.BroadcastChannelwise(name, dtype_t::VOSA_UINT8, inputs,
                                        channel_size);
  }
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_UINT16_UINT16: {
    return builder.BroadcastChannelwise(name, dtype_t::VOSA_UINT16, inputs,
                                        channel_size);
  }
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_UINT32_UINT32: {
    return builder.BroadcastChannelwise(name, dtype_t::VOSA_UINT32, inputs,
                                        channel_size);
  }
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_INT8_INT8: {
    return builder.BroadcastChannelwise(name, dtype_t::VOSA_INT8, inputs,
                                        channel_size);
  }
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_INT16_INT16: {
    return builder.BroadcastChannelwise(name, dtype_t::VOSA_INT16, inputs,
                                        channel_size);
  }
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_INT32_INT32: {
    return builder.BroadcastChannelwise(name, dtype_t::VOSA_INT32, inputs,
                                        channel_size);
  }
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.BroadcastChannelwise(name, dtype_t::VOSA_FLOAT32, inputs,
                                        channel_size);
  }
  default: {
    return builder.ReportError(
        "Unsupported output data type for BroadcastChannelwise");
  }
  }
}

bool Deserializer::process_BroadcastPlanewise(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_broadcast_single_pixel = op.AsBROADCAST_PLANEWISE();
  assert(serialized_broadcast_single_pixel);
  auto mode = serialized_broadcast_single_pixel->mode;
  auto serialized_attributes = &serialized_broadcast_single_pixel->attr;
  std::vector<uint32_t> plane_size = serialized_attributes->get()->size;
  switch (mode) {
  case vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_UINT8_UINT8: {
    return builder.BroadcastPlanewise(name, dtype_t::VOSA_UINT8, inputs,
                                      plane_size);
  }
  case vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_UINT16_UINT16: {
    return builder.BroadcastPlanewise(name, dtype_t::VOSA_UINT16, inputs,
                                      plane_size);
  }
  case vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_UINT32_UINT32: {
    return builder.BroadcastPlanewise(name, dtype_t::VOSA_UINT32, inputs,
                                      plane_size);
  }
  case vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_INT8_INT8: {
    return builder.BroadcastPlanewise(name, dtype_t::VOSA_INT8, inputs,
                                      plane_size);
  }
  case vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_INT16_INT16: {
    return builder.BroadcastPlanewise(name, dtype_t::VOSA_INT16, inputs,
                                      plane_size);
  }
  case vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_INT32_INT32: {
    return builder.BroadcastPlanewise(name, dtype_t::VOSA_INT32, inputs,
                                      plane_size);
  }
  case vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.BroadcastPlanewise(name, dtype_t::VOSA_FLOAT32, inputs,
                                      plane_size);
  }
  default: {
    return builder.ReportError(
        "Unsupported data type for BroadcastSinglePixel");
  }
  }
}

bool Deserializer::process_ChannelExtract(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_channel_extract = op.AsCHANNEL_EXTRACT();
  assert(serialized_channel_extract);
  auto mode = serialized_channel_extract->mode;
  auto serialized_attributes = &serialized_channel_extract->attr;
  std::vector<uint32_t> channels = serialized_attributes->get()->channels;
  switch (mode) {
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_BOOL_BOOL: {
    return builder.ChannelExtract(name, dtype_t::VOSA_BOOL, inputs, channels);
  }
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_UINT8_UINT8: {
    return builder.ChannelExtract(name, dtype_t::VOSA_UINT8, inputs, channels);
  }
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_UINT16_UINT16: {
    return builder.ChannelExtract(name, dtype_t::VOSA_UINT16, inputs, channels);
  }
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_UINT32_UINT32: {
    return builder.ChannelExtract(name, dtype_t::VOSA_UINT32, inputs, channels);
  }
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_INT8_INT8: {
    return builder.ChannelExtract(name, dtype_t::VOSA_INT8, inputs, channels);
  }
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_INT16_INT16: {
    return builder.ChannelExtract(name, dtype_t::VOSA_INT16, inputs, channels);
  }
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_INT32_INT32: {
    return builder.ChannelExtract(name, dtype_t::VOSA_INT32, inputs, channels);
  }
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.ChannelExtract(name, dtype_t::VOSA_FLOAT32, inputs,
                                  channels);
  }
  default: {
    return builder.ReportError("Unsupported data type for ChannelExtract");
  }
  }
}

bool Deserializer::process_LogicalShiftRight(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_logical_shift_right = op.AsLOGICAL_SHIFT_RIGHT();
  assert(serialized_logical_shift_right);
  auto mode = serialized_logical_shift_right->mode;
  auto serialized_attributes = &serialized_logical_shift_right->attr;
  uint32_t shift = serialized_attributes->get()->shift;
  switch (mode) {
  case vosa_serialization::LOGICAL_SHIFT_RIGHT_MODE_I_I_UINT8_UINT8: {
    return builder.LogicalShiftRight(name, dtype_t::VOSA_UINT8, inputs, shift);
  }
  case vosa_serialization::LOGICAL_SHIFT_RIGHT_MODE_I_I_UINT16_UINT16: {
    return builder.LogicalShiftRight(name, dtype_t::VOSA_UINT16, inputs, shift);
  }
  case vosa_serialization::LOGICAL_SHIFT_RIGHT_MODE_I_I_UINT32_UINT32: {
    return builder.LogicalShiftRight(name, dtype_t::VOSA_UINT32, inputs, shift);
  }
  }
}

bool Deserializer::process_Abs(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_abs = op.AsABS();
  assert(serialized_abs);
  auto mode = serialized_abs->mode;
  switch (mode) {
  case vosa_serialization::ABS_MODE_I_I_INT8_INT8: {
    return builder.Abs(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::ABS_MODE_I_I_INT16_INT16: {
    return builder.Abs(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::ABS_MODE_I_I_INT32_INT32: {
    return builder.Abs(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::ABS_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Abs(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported data type for Abs");
  }
  }
}

bool Deserializer::process_ArgMaxChannelwise(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  return builder.ArgMaxChannelwise(name, inputs);
}

bool Deserializer::process_ArgMaxPlanewise(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  return builder.ArgMaxPlanewise(name, inputs);
}

bool Deserializer::process_ArgMinChannelwise(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  return builder.ArgMinChannelwise(name, inputs);
}

bool Deserializer::process_ArgMinPlanewise(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  return builder.ArgMinPlanewise(name, inputs);
}

bool Deserializer::process_ReduceInterpChannelwise(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  return builder.ReduceInterpChannelwise(name, dtype_t::VOSA_FLOAT32, inputs);
}

bool Deserializer::process_ReduceMaxChannelwise(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsREDUCE_MAX_CHANNELWISE();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_UINT8_UINT8: {
    return builder.ReduceMaxChannelwise(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_UINT16_UINT16: {
    return builder.ReduceMaxChannelwise(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_UINT32_UINT32: {
    return builder.ReduceMaxChannelwise(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_INT8_INT8: {
    return builder.ReduceMaxChannelwise(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_INT16_INT16: {
    return builder.ReduceMaxChannelwise(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_INT32_INT32: {
    return builder.ReduceMaxChannelwise(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::REDUCE_MAX_CHANNELWISE_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.ReduceMaxChannelwise(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError(
        "Unsupported data type for ReduceMaxChannelwise");
  }
  }
}

bool Deserializer::process_ReduceMaxPlanewise(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsREDUCE_MAX_PLANEWISE();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_UINT8_UINT8: {
    return builder.ReduceMaxPlanewise(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_UINT16_UINT16: {
    return builder.ReduceMaxPlanewise(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_UINT32_UINT32: {
    return builder.ReduceMaxPlanewise(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_INT8_INT8: {
    return builder.ReduceMaxPlanewise(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_INT16_INT16: {
    return builder.ReduceMaxPlanewise(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_INT32_INT32: {
    return builder.ReduceMaxPlanewise(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.ReduceMaxPlanewise(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported data type for ReduceMaxPlanewise");
  }
  }
}

bool Deserializer::process_ReduceMinChannelwise(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsREDUCE_MIN_CHANNELWISE();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::REDUCE_MIN_CHANNELWISE_MODE_I_I_UINT8_UINT8: {
    return builder.ReduceMinChannelwise(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::REDUCE_MIN_CHANNELWISE_MODE_I_I_UINT16_UINT16: {
    return builder.ReduceMinChannelwise(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::REDUCE_MIN_CHANNELWISE_MODE_I_I_UINT32_UINT32: {
    return builder.ReduceMinChannelwise(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::REDUCE_MIN_CHANNELWISE_MODE_I_I_INT8_INT8: {
    return builder.ReduceMinChannelwise(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::REDUCE_MIN_CHANNELWISE_MODE_I_I_INT16_INT16: {
    return builder.ReduceMinChannelwise(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::REDUCE_MIN_CHANNELWISE_MODE_I_I_INT32_INT32: {
    return builder.ReduceMinChannelwise(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::REDUCE_MIN_CHANNELWISE_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.ReduceMinChannelwise(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError(
        "Unsupported data type for ReduceMinChannelwise");
  }
  }
}

bool Deserializer::process_ReduceMinPlanewise(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsREDUCE_MIN_PLANEWISE();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::REDUCE_MIN_PLANEWISE_MODE_I_I_UINT8_UINT8: {
    return builder.ReduceMinPlanewise(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::REDUCE_MIN_PLANEWISE_MODE_I_I_UINT16_UINT16: {
    return builder.ReduceMinPlanewise(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::REDUCE_MIN_PLANEWISE_MODE_I_I_UINT32_UINT32: {
    return builder.ReduceMinPlanewise(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::REDUCE_MIN_PLANEWISE_MODE_I_I_INT8_INT8: {
    return builder.ReduceMinPlanewise(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::REDUCE_MIN_PLANEWISE_MODE_I_I_INT16_INT16: {
    return builder.ReduceMinPlanewise(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::REDUCE_MIN_PLANEWISE_MODE_I_I_INT32_INT32: {
    return builder.ReduceMinPlanewise(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::REDUCE_MIN_PLANEWISE_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.ReduceMinPlanewise(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported data type for ReduceMinPlanewise");
  }
  }
}

bool Deserializer::process_ReduceSumChannelwise(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsREDUCE_SUM_CHANNELWISE();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::REDUCE_SUM_CHANNELWISE_MODE_I_I_UINT8_UINT8: {
    return builder.ReduceSumChannelwise(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::REDUCE_SUM_CHANNELWISE_MODE_I_I_UINT16_UINT16: {
    return builder.ReduceSumChannelwise(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::REDUCE_SUM_CHANNELWISE_MODE_I_I_UINT32_UINT32: {
    return builder.ReduceSumChannelwise(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::REDUCE_SUM_CHANNELWISE_MODE_I_I_INT8_INT8: {
    return builder.ReduceSumChannelwise(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::REDUCE_SUM_CHANNELWISE_MODE_I_I_INT16_INT16: {
    return builder.ReduceSumChannelwise(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::REDUCE_SUM_CHANNELWISE_MODE_I_I_INT32_INT32: {
    return builder.ReduceSumChannelwise(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::REDUCE_SUM_CHANNELWISE_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.ReduceSumChannelwise(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError(
        "Unsupported data type for ReduceSumChannelwise");
  }
  }
}

bool Deserializer::process_ReduceSumPlanewise(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsREDUCE_SUM_PLANEWISE();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_UINT8_UINT8: {
    return builder.ReduceSumPlanewise(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_UINT16_UINT16: {
    return builder.ReduceSumPlanewise(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_UINT32_UINT32: {
    return builder.ReduceSumPlanewise(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_INT8_INT8: {
    return builder.ReduceSumPlanewise(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_INT16_INT16: {
    return builder.ReduceSumPlanewise(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_INT32_INT32: {
    return builder.ReduceSumPlanewise(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.ReduceSumPlanewise(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported data type for ReduceSumPlanewise");
  }
  }
}

bool Deserializer::process_AbsDiff(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsABS_DIFF();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::ABS_DIFF_MODE_I_I_UINT8_UINT8: {
    return builder.AbsDiff(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::ABS_DIFF_MODE_I_I_UINT16_UINT16: {
    return builder.AbsDiff(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::ABS_DIFF_MODE_I_I_UINT32_UINT32: {
    return builder.AbsDiff(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::ABS_DIFF_MODE_I_I_INT8_UINT8: {
    return builder.AbsDiff(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::ABS_DIFF_MODE_I_I_INT16_UINT16: {
    return builder.AbsDiff(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::ABS_DIFF_MODE_I_I_INT32_UINT32: {
    return builder.AbsDiff(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::ABS_DIFF_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.AbsDiff(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported data type for AbsDiff");
  }
  }
}

bool Deserializer::process_ArithmeticShiftRight(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsARITHMETIC_SHIFT_RIGHT();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  auto serialized_attributes = &serialized_op->attr;
  uint32_t shift = serialized_attributes->get()->shift;
  switch (mode) {
  case vosa_serialization::ARITHMETIC_SHIFT_RIGHT_MODE_I_I_INT8_INT8: {
    return builder.ArithmeticShiftRight(name, dtype_t::VOSA_INT8, inputs,
                                        shift);
  }
  case vosa_serialization::ARITHMETIC_SHIFT_RIGHT_MODE_I_I_INT16_INT16: {
    return builder.ArithmeticShiftRight(name, dtype_t::VOSA_INT16, inputs,
                                        shift);
  }
  case vosa_serialization::ARITHMETIC_SHIFT_RIGHT_MODE_I_I_INT32_INT32: {
    return builder.ArithmeticShiftRight(name, dtype_t::VOSA_INT32, inputs,
                                        shift);
  }
  }
}

bool Deserializer::process_Where(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsWHERE();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::WHERE_MODE_I_I_UINT8_UINT8: {
    return builder.Where(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::WHERE_MODE_I_I_UINT16_UINT16: {
    return builder.Where(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::WHERE_MODE_I_I_UINT32_UINT32: {
    return builder.Where(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::WHERE_MODE_I_I_INT8_INT8: {
    return builder.Where(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::WHERE_MODE_I_I_INT16_INT16: {
    return builder.Where(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::WHERE_MODE_I_I_INT32_INT32: {
    return builder.Where(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::WHERE_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Where(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported data type for Where");
  }
  }
}

bool Deserializer::process_Equal(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  return builder.Equal(name, inputs);
}

bool Deserializer::process_PiecewiseLinear(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsPIECEWISE_LINEAR();
  assert(serialized_op);
  auto serialized_attributes = &serialized_op->attr;
  std::vector<uint8_t> raw_nodes = serialized_attributes->get()->nodes;
  std::vector<uint8_t> raw_values = serialized_attributes->get()->values;
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::PIECEWISE_LINEAR_MODE_I_I_INT8_INT8: {
    return builder.template PiecewiseLinear<int8_t>(
        name, dtype_t::VOSA_INT8, inputs, unpackValues<int8_t>(raw_nodes),
        unpackValues<int8_t>(raw_values));
  }
  case vosa_serialization::PIECEWISE_LINEAR_MODE_I_I_INT16_INT16: {
    return builder.template PiecewiseLinear<int16_t>(
        name, dtype_t::VOSA_INT16, inputs, unpackValues<int16_t>(raw_nodes),
        unpackValues<int16_t>(raw_values));
  }
  case vosa_serialization::PIECEWISE_LINEAR_MODE_I_I_INT32_INT32: {
    return builder.template PiecewiseLinear<int32_t>(
        name, dtype_t::VOSA_INT32, inputs, unpackValues<int32_t>(raw_nodes),
        unpackValues<int32_t>(raw_values));
  }
  case vosa_serialization::PIECEWISE_LINEAR_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.template PiecewiseLinear<float>(
        name, dtype_t::VOSA_FLOAT32, inputs, unpackValues<float>(raw_nodes),
        unpackValues<float>(raw_values));
  }
  default: {
    return builder.ReportError("Unsupported data type for PiecewiseLinear");
  }
  }
}

bool Deserializer::process_Or(const std::string &name,
                              const std::vector<std::string> &inputs,
                              const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsOR();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::OR_MODE_I_I_BOOL_BOOL: {
    return builder.Or(name, dtype_t::VOSA_BOOL, inputs);
  }
  default: {
    return builder.ReportError("Unsupported data type for Or");
  }
  }
}

bool Deserializer::process_Mod(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsMOD();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::MOD_MODE_I_I_UINT8_UINT8: {
    return builder.Mod(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::MOD_MODE_I_I_UINT16_UINT16: {
    return builder.Mod(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::MOD_MODE_I_I_UINT32_UINT32: {
    return builder.Mod(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::MOD_MODE_I_I_INT8_INT8: {
    return builder.Mod(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::MOD_MODE_I_I_INT16_INT16: {
    return builder.Mod(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::MOD_MODE_I_I_INT32_INT32: {
    return builder.Mod(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::MOD_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Mod(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported data type for Mod");
  }
  }
}

bool Deserializer::process_Not(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsNOT();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::NOT_MODE_I_I_BOOL_BOOL: {
    return builder.Not(name, dtype_t::VOSA_BOOL, inputs);
  }
  default: {
    return builder.ReportError("Unsupported data type for Not");
  }
  }
}

bool Deserializer::process_Round(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsROUND();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  auto serialized_attributes = &serialized_op->attr;
  auto serialized_round_method = serialized_attributes->get()->round_method;

  round_method_t round_method;
  switch (serialized_round_method) {
  case vosa_serialization::ROUND_METHOD_FLOOR: {
    round_method = round_method_t::VOSA_ROUND_METHOD_FLOOR;
    break;
  }
  case vosa_serialization::ROUND_METHOD_CEIL: {
    round_method = round_method_t::VOSA_ROUND_METHOD_CEIL;
    break;
  }
  case vosa_serialization::ROUND_METHOD_TOWARDS_ZERO: {
    round_method = round_method_t::VOSA_ROUND_METHOD_TOWARDS_ZERO;
    break;
  }
  case vosa_serialization::ROUND_METHOD_AWAY_FROM_ZERO: {
    round_method = round_method_t::VOSA_ROUND_METHOD_AWAY_FROM_ZERO;
    break;
  }
  case vosa_serialization::ROUND_METHOD_HALF_UP: {
    round_method = round_method_t::VOSA_ROUND_METHOD_HALF_UP;
    break;
  }
  case vosa_serialization::ROUND_METHOD_HALF_DOWN: {
    round_method = round_method_t::VOSA_ROUND_METHOD_HALF_DOWN;
    break;
  }
  case vosa_serialization::ROUND_METHOD_HALF_TOWARDS_ZERO: {
    round_method = round_method_t::VOSA_ROUND_METHOD_HALF_TOWARDS_ZERO;
    break;
  }
  case vosa_serialization::ROUND_METHOD_HALF_AWAY_FROM_ZERO: {
    round_method = round_method_t::VOSA_ROUND_METHOD_HALF_AWAY_FROM_ZERO;
    break;
  }
  case vosa_serialization::ROUND_METHOD_HALF_TO_EVEN: {
    round_method = round_method_t::VOSA_ROUND_METHOD_HALF_TO_EVEN;
    break;
  }
  case vosa_serialization::ROUND_METHOD_HALF_TO_ODD: {
    round_method = round_method_t::VOSA_ROUND_METHOD_HALF_TO_ODD;
    break;
  }
  default: {
    return builder.ReportError("Unsupported round method for Round");
  }
  }

  switch (mode) {
  case vosa_serialization::ROUND_MODE_I_I_FLOAT32_INT32: {
    return builder.Round(name, dtype_t::VOSA_INT32, inputs, round_method);
  }
  default: {
    return builder.ReportError("Unsupported data type for Round");
  }
  }
}

bool Deserializer::process_Decimate(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsDECIMATE();
  assert(serialized_op);
  auto serialized_attributes = &serialized_op->attr;
  std::vector<uint32_t> N = serialized_attributes->get()->window;
  std::vector<uint32_t> offsets = serialized_attributes->get()->offsets;

  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::DECIMATE_MODE_I_I_UINT8_UINT8: {
    return builder.Decimate(name, dtype_t::VOSA_UINT8, inputs, N, offsets);
  }
  case vosa_serialization::DECIMATE_MODE_I_I_UINT16_UINT16: {
    return builder.Decimate(name, dtype_t::VOSA_UINT16, inputs, N, offsets);
  }
  case vosa_serialization::DECIMATE_MODE_I_I_UINT32_UINT32: {
    return builder.Decimate(name, dtype_t::VOSA_UINT32, inputs, N, offsets);
  }
  case vosa_serialization::DECIMATE_MODE_I_I_INT8_INT8: {
    return builder.Decimate(name, dtype_t::VOSA_INT8, inputs, N, offsets);
  }
  case vosa_serialization::DECIMATE_MODE_I_I_INT16_INT16: {
    return builder.Decimate(name, dtype_t::VOSA_INT16, inputs, N, offsets);
  }
  case vosa_serialization::DECIMATE_MODE_I_I_INT32_INT32: {
    return builder.Decimate(name, dtype_t::VOSA_INT32, inputs, N, offsets);
  }
  case vosa_serialization::DECIMATE_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Decimate(name, dtype_t::VOSA_FLOAT32, inputs, N, offsets);
  }
  default: {
    return builder.ReportError("Unsupported data type for Decimate");
  }
  }
}

bool Deserializer::process_Expand(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsEXPAND();
  assert(serialized_op);
  auto serialized_attributes = &serialized_op->attr;
  std::vector<uint8_t> raw_fill_value =
      serialized_attributes->get()->fill_value;
  std::vector<uint8_t> raw_gather_kernel =
      serialized_attributes->get()->gather_kernel_value;
  std::vector<uint32_t> gather_kernel_dimension =
      serialized_attributes->get()->gather_kernel_dimension;
  std::vector<uint8_t> gather_kernel =
      unpackValues<uint8_t>(gather_kernel_dimension, raw_gather_kernel);

  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::EXPAND_MODE_I_I_UINT8_UINT8: {
    return builder.template Expand<uint8_t>(
        name, dtype_t::VOSA_UINT8, inputs, unpackValue<uint8_t>(raw_fill_value),
        gather_kernel_dimension, gather_kernel);
  }
  case vosa_serialization::EXPAND_MODE_I_I_UINT16_UINT16: {
    return builder.template Expand<uint16_t>(
        name, dtype_t::VOSA_UINT16, inputs,
        unpackValue<uint16_t>(raw_fill_value), gather_kernel_dimension,
        gather_kernel);
  }
  case vosa_serialization::EXPAND_MODE_I_I_UINT32_UINT32: {
    return builder.template Expand<uint32_t>(
        name, dtype_t::VOSA_UINT32, inputs,
        unpackValue<uint32_t>(raw_fill_value), gather_kernel_dimension,
        gather_kernel);
  }
  case vosa_serialization::EXPAND_MODE_I_I_INT8_INT8: {
    return builder.template Expand<int8_t>(
        name, dtype_t::VOSA_INT8, inputs, unpackValue<int8_t>(raw_fill_value),
        gather_kernel_dimension, gather_kernel);
  }
  case vosa_serialization::EXPAND_MODE_I_I_INT16_INT16: {
    return builder.template Expand<int16_t>(
        name, dtype_t::VOSA_INT16, inputs, unpackValue<int16_t>(raw_fill_value),
        gather_kernel_dimension, gather_kernel);
  }
  case vosa_serialization::EXPAND_MODE_I_I_INT32_INT32: {
    return builder.template Expand<int32_t>(
        name, dtype_t::VOSA_INT32, inputs, unpackValue<int32_t>(raw_fill_value),
        gather_kernel_dimension, gather_kernel);
  }
  case vosa_serialization::EXPAND_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.template Expand<float>(
        name, dtype_t::VOSA_FLOAT32, inputs, unpackValue<float>(raw_fill_value),
        gather_kernel_dimension, gather_kernel);
  }
  default: {
    return builder.ReportError("Unsupported data type for Expand");
  }
  }
}

bool Deserializer::process_PointwiseMatrixMultiply(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsPOINTWISE_MATRIX_MULTIPLY();
  assert(serialized_op);
  auto serialized_attributes = &serialized_op->attr;
  std::vector<uint8_t> raw_M = serialized_attributes->get()->M;
  std::vector<uint8_t> raw_K_1 = serialized_attributes->get()->K_1;
  std::vector<uint8_t> raw_K_2 = serialized_attributes->get()->K_2;
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_UINT8_UINT8: {
    return builder.template PointwiseMatrixMultiply<uint8_t>(
        name, dtype_t::VOSA_UINT8, inputs, unpackValues<uint8_t>(raw_M),
        unpackValues<uint8_t>(raw_K_1), unpackValues<uint8_t>(raw_K_2));
  }
  case vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_UINT16_UINT16: {
    return builder.template PointwiseMatrixMultiply<uint16_t>(
        name, dtype_t::VOSA_UINT16, inputs, unpackValues<uint16_t>(raw_M),
        unpackValues<uint16_t>(raw_K_1), unpackValues<uint16_t>(raw_K_2));
  }
  case vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_UINT32_UINT32: {
    return builder.template PointwiseMatrixMultiply<uint32_t>(
        name, dtype_t::VOSA_UINT32, inputs, unpackValues<uint32_t>(raw_M),
        unpackValues<uint32_t>(raw_K_1), unpackValues<uint32_t>(raw_K_2));
  }
  case vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_INT8_INT8: {
    return builder.template PointwiseMatrixMultiply<int8_t>(
        name, dtype_t::VOSA_INT8, inputs, unpackValues<int8_t>(raw_M),
        unpackValues<int8_t>(raw_K_1), unpackValues<int8_t>(raw_K_2));
  }
  case vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_INT16_INT16: {
    return builder.template PointwiseMatrixMultiply<int16_t>(
        name, dtype_t::VOSA_INT16, inputs, unpackValues<int16_t>(raw_M),
        unpackValues<int16_t>(raw_K_1), unpackValues<int16_t>(raw_K_2));
  }
  case vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_INT32_INT32: {
    return builder.template PointwiseMatrixMultiply<int32_t>(
        name, dtype_t::VOSA_INT32, inputs, unpackValues<int32_t>(raw_M),
        unpackValues<int32_t>(raw_K_1), unpackValues<int32_t>(raw_K_2));
  }
  case vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.template PointwiseMatrixMultiply<float>(
        name, dtype_t::VOSA_FLOAT32, inputs, unpackValues<float>(raw_M),
        unpackValues<float>(raw_K_1), unpackValues<float>(raw_K_2));
  }
  default: {
    return builder.ReportError(
        "Unsupported data type for PointwiseMatrixMultiply");
  }
  }
}

bool Deserializer::process_Greater(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  return builder.Greater(name, inputs);
}

bool Deserializer::process_GreaterEqual(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  return builder.GreaterEqual(name, inputs);
}

bool Deserializer::process_Max(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsMAX_();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::MAX__MODE_I_I_UINT8_UINT8: {
    return builder.Max(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::MAX__MODE_I_I_UINT16_UINT16: {
    return builder.Max(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::MAX__MODE_I_I_UINT32_UINT32: {
    return builder.Max(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::MAX__MODE_I_I_INT8_INT8: {
    return builder.Max(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::MAX__MODE_I_I_INT16_INT16: {
    return builder.Max(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::MAX__MODE_I_I_INT32_INT32: {
    return builder.Max(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::MAX__MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Max(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported data type for Max");
  }
  }
}

bool Deserializer::process_Min(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsMIN_();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::MIN__MODE_I_I_UINT8_UINT8: {
    return builder.Min(name, dtype_t::VOSA_UINT8, inputs);
  }
  case vosa_serialization::MIN__MODE_I_I_UINT16_UINT16: {
    return builder.Min(name, dtype_t::VOSA_UINT16, inputs);
  }
  case vosa_serialization::MIN__MODE_I_I_UINT32_UINT32: {
    return builder.Min(name, dtype_t::VOSA_UINT32, inputs);
  }
  case vosa_serialization::MIN__MODE_I_I_INT8_INT8: {
    return builder.Min(name, dtype_t::VOSA_INT8, inputs);
  }
  case vosa_serialization::MIN__MODE_I_I_INT16_INT16: {
    return builder.Min(name, dtype_t::VOSA_INT16, inputs);
  }
  case vosa_serialization::MIN__MODE_I_I_INT32_INT32: {
    return builder.Min(name, dtype_t::VOSA_INT32, inputs);
  }
  case vosa_serialization::MIN__MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Min(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported data type for Min");
  }
  }
}

bool Deserializer::process_Sqrt(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsSQRT();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  switch (mode) {
  case vosa_serialization::SQRT_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Sqrt(name, dtype_t::VOSA_FLOAT32, inputs);
  }
  default: {
    return builder.ReportError("Unsupported data type for Sqrt");
  }
  }
}

bool Deserializer::process_GammaCorrection(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsGAMMA_CORRECTION();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  auto serialized_attributes = &serialized_op->attr;
  float gamma = unpackValue<float>(serialized_attributes->get()->gamma);
  switch (mode) {
  case vosa_serialization::GAMMA_CORRECTION_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.GammaCorrection(name, dtype_t::VOSA_FLOAT32, inputs, gamma);
  }
  default: {
    return builder.ReportError("Unsupported data type for Gamma");
  }
  }
}

bool Deserializer::process_Power(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsPOWER();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  auto serialized_attributes = &serialized_op->attr;
  float base = unpackValue<float>(serialized_attributes->get()->base);
  switch (mode) {
  case vosa_serialization::POWER_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Power(name, dtype_t::VOSA_FLOAT32, inputs, base);
  }
  default: {
    return builder.ReportError("Unsupported data type for Power");
  }
  }
}

bool Deserializer::process_Rotate90(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsROTATE_90();
  assert(serialized_op);
  auto mode = serialized_op->mode;
  auto serialized_attributes = &serialized_op->attr;
  int32_t rotate = serialized_attributes->get()->rotate;
  switch (mode) {
  case vosa_serialization::ROTATE_90_MODE_I_I_UINT8_UINT8: {
    return builder.Rotate90(name, dtype_t::VOSA_UINT8, inputs, rotate);
  }
  case vosa_serialization::ROTATE_90_MODE_I_I_UINT16_UINT16: {
    return builder.Rotate90(name, dtype_t::VOSA_UINT16, inputs, rotate);
  }
  case vosa_serialization::ROTATE_90_MODE_I_I_UINT32_UINT32: {
    return builder.Rotate90(name, dtype_t::VOSA_UINT32, inputs, rotate);
  }
  case vosa_serialization::ROTATE_90_MODE_I_I_INT8_INT8: {
    return builder.Rotate90(name, dtype_t::VOSA_INT8, inputs, rotate);
  }
  case vosa_serialization::ROTATE_90_MODE_I_I_INT16_INT16: {
    return builder.Rotate90(name, dtype_t::VOSA_INT16, inputs, rotate);
  }
  case vosa_serialization::ROTATE_90_MODE_I_I_INT32_INT32: {
    return builder.Rotate90(name, dtype_t::VOSA_INT32, inputs, rotate);
  }
  case vosa_serialization::ROTATE_90_MODE_I_I_FLOAT32_FLOAT32: {
    return builder.Rotate90(name, dtype_t::VOSA_FLOAT32, inputs, rotate);
  }
  default: {
    return builder.ReportError("Unsupported data type for Rotate90");
  }
  }
}

bool Deserializer::process_TosaGraph(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsTOSA_GRAPH();
  assert(serialized_op);
  auto output_type = serialized_op->output_type;
  auto serialized_attributes = &serialized_op->attr;
  std::vector<uint8_t> buffer = serialized_attributes->get()->buffer;
  return builder.TosaGraph(name, mapDType(output_type), inputs, buffer);
}

bool Deserializer::process_Input(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  // auto serialized_input = op.AsINPUT();
  // assert(serialized_input);
  // auto dtype = mapDType(serialized_input->dtype);
  // return builder.Input(name, dtype);
  // inputs are handled on command line
  return true;
}

bool Deserializer::process_CustomScalar(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  auto serialized_op = op.AsCUSTOM_SCALAR();
  assert(serialized_op);
  auto op_name = serialized_op->custom_op_name;
  return builder.CustomScalar(name, inputs, op_name);
}

bool Deserializer::process_UnknownOperator(
    const std::string &name, const std::vector<std::string> &inputs,
    const vosa_serialization::VosaOperatorUnion &op) {
  const std::string op_name(EnumNameVosaOperator(op.type));
  return builder.ReportError("Unknown opeator " + op_name);
}

Deserializer::Deserializer(MLIRBuilder &b) : builder(b) {}

bool Deserializer::process(const uint8_t *flatbuffer_data, MLIRBuilder &b) {
  Deserializer d(b);
  return d.process(flatbuffer_data);
}

bool Deserializer::process(const uint8_t *flatbuffer_data) {
  vosa_serialization::VosaGraphT serialized_graph;
  auto graph =
      flatbuffers::GetRoot<vosa_serialization::VosaGraph>(flatbuffer_data);
  graph->UnPackTo(&serialized_graph);

  // perform ordered traversal of graph so that all inputs are appear before
  // their uses
  std::vector<unsigned> ordered_ops = get_ordered_ops(serialized_graph);

  for (const auto &op_index : ordered_ops) {
    const auto &serialized_graph_op = serialized_graph.operators[op_index];

    const auto &name = serialized_graph_op->name;
    const auto &inputs = serialized_graph_op->inputs;
    const auto &op = serialized_graph_op->op;
    switch (op.type) {
    case vosa_serialization::VosaOperator_RESIZE_BILINEAR: {
      if (!process_ResizeBilinear(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_RESIZE_NEAREST_NEIGHBOUR: {
      if (!process_ResizeNearestNeighbour(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_CONST_SCALAR: {
      if (!process_ConstScalar(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_CONV_2D: {
      if (!process_Conv2D(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_CONST_IMAGE: {
      if (!process_ConstImage(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_CONCAT: {
      if (!process_Concat(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_MESH_GRID: {
      if (!process_MeshGrid(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_MULT: {
      if (!process_Mult(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_ABS_DIFF: {
      if (!process_AbsDiff(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_ADD: {
      if (!process_Add(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_SUB: {
      if (!process_Sub(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_PAD: {
      if (!process_Pad(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_CLAMP: {
      if (!process_Clamp(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_CAST: {
      if (!process_Cast(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_DIV: {
      if (!process_Div(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_IMPORT_CHANNEL: {
      if (!process_ImportChannel(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_BROADCAST_CHANNELWISE: {
      if (!process_BroadcastChannelwise(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_BROADCAST_PLANEWISE: {
      if (!process_BroadcastPlanewise(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_CHANNEL_EXTRACT: {
      if (!process_ChannelExtract(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_LOGICAL_SHIFT_RIGHT: {
      if (!process_LogicalShiftRight(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_ARITHMETIC_SHIFT_RIGHT: {
      if (!process_ArithmeticShiftRight(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_ABS: {
      if (!process_Abs(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_ARG_MAX_CHANNELWISE: {
      if (!process_ArgMaxChannelwise(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_ARG_MAX_PLANEWISE: {
      if (!process_ArgMaxPlanewise(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_ARG_MIN_CHANNELWISE: {
      if (!process_ArgMinChannelwise(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_ARG_MIN_PLANEWISE: {
      if (!process_ArgMinPlanewise(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_REDUCE_INTERP_CHANNELWISE: {
      if (!process_ReduceInterpChannelwise(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_REDUCE_MAX_CHANNELWISE: {
      if (!process_ReduceMaxChannelwise(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_REDUCE_MAX_PLANEWISE: {
      if (!process_ReduceMaxPlanewise(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_REDUCE_MIN_CHANNELWISE: {
      if (!process_ReduceMinChannelwise(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_REDUCE_MIN_PLANEWISE: {
      if (!process_ReduceMinPlanewise(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_REDUCE_SUM_CHANNELWISE: {
      if (!process_ReduceSumChannelwise(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_REDUCE_SUM_PLANEWISE: {
      if (!process_ReduceSumPlanewise(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_WHERE: {
      if (!process_Where(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_EQUAL: {
      if (!process_Equal(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_PIECEWISE_LINEAR: {
      if (!process_PiecewiseLinear(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_OR: {
      if (!process_Or(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_MOD: {
      if (!process_Mod(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_NOT: {
      if (!process_Not(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_ROUND: {
      if (!process_Round(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_DECIMATE: {
      if (!process_Decimate(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_EXPAND: {
      if (!process_Expand(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_POINTWISE_MATRIX_MULTIPLY: {
      if (!process_PointwiseMatrixMultiply(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_GREATER: {
      if (!process_Greater(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_GREATER_EQUAL: {
      if (!process_GreaterEqual(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_MAX_: {
      if (!process_Max(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_MIN_: {
      if (!process_Min(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_SQRT: {
      if (!process_Sqrt(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_GAMMA_CORRECTION: {
      if (!process_GammaCorrection(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_POWER: {
      if (!process_Power(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_ROTATE_90: {
      if (!process_Rotate90(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_TOSA_GRAPH: {
      if (!process_TosaGraph(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_INPUT: {
      if (!process_Input(name, inputs, op))
        return false;
      break;
    }
    case vosa_serialization::VosaOperator_CUSTOM_SCALAR: {
      if (!process_CustomScalar(name, inputs, op))
        return false;
      break;
    }
    default: {
      if (!process_UnknownOperator(name, inputs, op))
        return false;
      break;
    }
    }
  }

  return true;
}

int buildMLIRPipeline(mlir::MLIRContext &context,
                      mlir::OwningOpRef<mlir::ModuleOp> &customOpsModule,
                      mlir::OwningOpRef<mlir::ModuleOp> &module,
                      const std::string &inputFile,
                      const uint8_t *flatbufferData,
                      const std::string &pipelineName,
                      const std::vector<std::string> &inputs,
                      const std::vector<std::string> &outputs) {

  MLIRBuilder builder(context, inputFile, pipelineName, customOpsModule);
  addInputs(builder, inputs);
  Deserializer::process(flatbufferData, builder);

  module = builder.finalize(outputs);
  if (!module)
    return 1;
  return 0;
}
} // namespace vosa
