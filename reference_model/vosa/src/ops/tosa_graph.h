/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_TOSA_GRAPH_H
#define VOSA_TOSA_GRAPH_H

#include "vosa/op.h"

class TOSA_Graph_Helper;

namespace vosa {

class TOSA_Graph_Base : public Op {
public:
  static std::shared_ptr<Op> parse_serialized(
      const vosa_serialization::VosaOperatorUnion &vosa_operator_union);
};

class TOSA_Graph : public TOSA_Graph_Base {
public:
  explicit TOSA_Graph(const std::vector<uint8_t> &buffer);
  ~TOSA_Graph() override;

  [[nodiscard]] std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &inputs) const final;
  [[nodiscard]] ArrayBase::DType output_dtype() const final;
  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

  [[maybe_unused]] void to_json(const std::string &filename);

private:
  std::vector<uint8_t> buffer;
  std::unique_ptr<TOSA_Graph_Helper> tosa_graph_helper;
};

} // namespace vosa

#endif // VOSA_TOSA_GRAPH_H
