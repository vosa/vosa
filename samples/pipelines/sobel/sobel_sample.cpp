/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <vector>

#include "iree/base/api.h"
#include "iree/hal/api.h"
#include "iree/runtime/api.h"

#include "ppm_utils.h"

#include "sobel_sample_builder_embedding.h"

int main(int argc, char *argv[]) {

  std::string input_pgm(argv[1]);
  std::string output_pgm(argv[2]);

  std::vector<uint8_t> input;
  int input_width, input_height;
  auto input_success = vosa::api::ppm::read_input<uint8_t>(
      input_pgm, input, input_width, input_height);
  if (!input_success)
    return 1;

  iree_runtime_instance_options_t instance_options;
  iree_runtime_instance_options_initialize(&instance_options);
  iree_runtime_instance_options_use_all_available_drivers(&instance_options);
  iree_runtime_instance_t *instance = nullptr;
  IREE_CHECK_OK(iree_runtime_instance_create(
      &instance_options, iree_allocator_system(), &instance));

  iree_hal_device_t *device = nullptr;
  IREE_CHECK_OK(iree_runtime_instance_try_create_default_device(
      instance, iree_make_cstring_view("local-sync"), &device));

  iree_runtime_session_options_t session_options;
  iree_runtime_session_options_initialize(&session_options);
  iree_runtime_session_t *session = nullptr;
  IREE_CHECK_OK(iree_runtime_session_create_with_device(
      instance, &session_options, device,
      iree_runtime_instance_host_allocator(instance), &session));
  iree_hal_device_release(device);

  const struct iree_file_toc_t *module_file = sobel_sample_builder_create();
  IREE_CHECK_OK(iree_runtime_session_append_bytecode_module_from_memory(
      session, iree_make_const_byte_span(module_file->data, module_file->size),
      iree_allocator_null()));

  iree_runtime_call_t call;
  IREE_CHECK_OK(iree_runtime_call_initialize_by_name(
      session, iree_make_cstring_view("module.sobel_pipeline"), &call));

  iree_hal_buffer_params_t input_buffer_params;
  input_buffer_params.usage = IREE_HAL_BUFFER_USAGE_DEFAULT;
  input_buffer_params.type = IREE_HAL_MEMORY_TYPE_DEVICE_LOCAL;

  iree_hal_dim_t input_shape[3] = {1080, 1920, 1};

  iree_hal_buffer_view_t *input_buffer_view;
  IREE_CHECK_OK(iree_hal_buffer_view_allocate_buffer_copy(
      device, iree_hal_device_allocator(device), IREE_ARRAYSIZE(input_shape),
      input_shape, IREE_HAL_ELEMENT_TYPE_UINT_8,
      IREE_HAL_ENCODING_TYPE_DENSE_ROW_MAJOR, input_buffer_params,
      iree_make_const_byte_span(input.data(), input_shape[0] * input_shape[1]),
      &input_buffer_view));

  IREE_CHECK_OK(
      iree_runtime_call_inputs_push_back_buffer_view(&call, input_buffer_view));
  iree_hal_buffer_view_release(input_buffer_view);

  IREE_CHECK_OK(iree_runtime_call_invoke(&call, /*flags=*/0))

  std::vector<uint8_t> output(input.size());

  iree_hal_buffer_view_t *ret_buffer_view = nullptr;
  IREE_CHECK_OK(
      iree_runtime_call_outputs_pop_front_buffer_view(&call, &ret_buffer_view));

  IREE_CHECK_OK(iree_hal_device_transfer_d2h(
      iree_runtime_session_device(session),
      iree_hal_buffer_view_buffer(ret_buffer_view), 0, output.data(),
      output.size(), IREE_HAL_TRANSFER_BUFFER_FLAG_DEFAULT,
      iree_infinite_timeout()));

  auto output_success =
      vosa::api::ppm::write_output(output_pgm, output, 1920, 1080, 1);
  if (!output_success)
    return 1;

  return 0;
}