/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "mod.h"

std::shared_ptr<vosa::Op> vosa::ModBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_modulo = vosa_operator_union.AsMOD();
  assert(serialized_modulo);
  auto mode = serialized_modulo->mode;
  switch (mode) {
  case vosa_serialization::MOD_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::Mod<uint8_t>>();
  }
  case vosa_serialization::MOD_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::Mod<uint16_t>>();
  }
  case vosa_serialization::MOD_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::Mod<uint32_t>>();
  }
  case vosa_serialization::MOD_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::Mod<int8_t>>();
  }
  case vosa_serialization::MOD_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::Mod<int16_t>>();
  }
  case vosa_serialization::MOD_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::Mod<int32_t>>();
  }
  case vosa_serialization::MOD_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::Mod<float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::ArrayBase::DType vosa::Mod<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <>
vosa_serialization::MOD_MODE
vosa::Mod<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::MOD_MODE_I_I_UINT8_UINT8;
}
template <>
vosa_serialization::MOD_MODE
vosa::Mod<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::MOD_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::MOD_MODE
vosa::Mod<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::MOD_MODE_I_I_UINT32_UINT32;
}
template <>
vosa_serialization::MOD_MODE vosa::Mod<int8_t>::get_serialization_mode() const {
  return vosa_serialization::MOD_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::MOD_MODE
vosa::Mod<int16_t>::get_serialization_mode() const {
  return vosa_serialization::MOD_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::MOD_MODE
vosa::Mod<int32_t>::get_serialization_mode() const {
  return vosa_serialization::MOD_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::MOD_MODE vosa::Mod<float>::get_serialization_mode() const {
  return vosa_serialization::MOD_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Mod<T>::serialize(const std::string &op_name,
                        const std::vector<std::string> &input_names) const {

  vosa_serialization::MODT modulo;
  modulo.mode = get_serialization_mode();

  auto modulo_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  modulo_operator->op.Set(std::move(modulo));
  modulo_operator->name = op_name;
  modulo_operator->inputs = input_names;
  return modulo_operator;
}

template <typename T>
T vosa_mod(T input_0, T input_1) {
  return input_0 % input_1;
}

template <>
float vosa_mod(float input_0, float input_1) {
  return fmod(input_0, input_1);
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::Mod<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {
  const EigenImage<T> &input_0 = inputs[0]->template get_image<T>();
  const EigenImage<T> &input_1 = inputs[1]->template get_image<T>();

  uint32_t input_0_height = input_0.dimension(0);
  uint32_t input_0_width = input_0.dimension(1);
  uint32_t input_0_channels = input_0.dimension(2);

  uint32_t input_1_height = input_1.dimension(0);
  uint32_t input_1_width = input_1.dimension(1);
  uint32_t input_1_channels = input_1.dimension(2);

  assert(input_0_channels == input_1_channels);
  assert(input_0_height == input_1_height);
  assert(input_0_width == input_1_width);

  auto output_image =
      EigenImage<T>(input_0_height, input_0_width, input_0_channels);

  for (uint32_t height_index = 0; height_index < input_0_height;
       height_index++) {
    for (uint32_t width_index = 0; width_index < input_0_width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < input_0_channels;
           channel_index++) {

        output_image(height_index, width_index, channel_index) =
            vosa_mod(input_0(height_index, width_index, channel_index),
                     input_1(height_index, width_index, channel_index));
      }
    }
  }
  return std::make_shared<vosa::ArrayBase>(output_image);
}

template class vosa::Mod<uint8_t>;

template class vosa::Mod<uint16_t>;

template class vosa::Mod<uint32_t>;

template class vosa::Mod<int8_t>;

template class vosa::Mod<int16_t>;

template class vosa::Mod<int32_t>;

template class vosa::Mod<float>;