"""Operations to manipulate data structures, types and shapes.
License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import enum
import typing

from vosa import types as types
from vosa.op import (
    OPERATOR_MODES,
    Op,
    ExecutionPattern,
    InputArrayPattern,
    InputPattern,
)


class BROADCAST_CHANNELWISE_MODES(OPERATOR_MODES):
    I_I_BOOL_BOOL = enum.auto()
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class BroadcastChannelwiseExecutionPattern(ExecutionPattern):
    def __init__(self, input_dtype, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    input_dtype,
                    types.AType.IMAGE,
                    description="Single Channel Input Image.",
                    shape=["H", "W", 1],
                )
            ),
            output_pattern=types.DataType(
                output_dtype,
                types.AType.IMAGE,
                description="Output Image.",
                shape=["H", "W", "size"],
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            BROADCAST_CHANNELWISE.SIZE_ATTRIBUTE: types.DataType(
                types.DType.UINT32,
                types.AType.SCALAR,
                description="Number of Output Channels",
                shape=[],
            )
        }


class BROADCAST_CHANNELWISE(Op):
    """
    Broadcast a single channel Image, replicating along channels,

    [latexmath]
    ++++
    output[y,x,c] = input[y,x,0] \; \\forall \; c \\in [0, size).
    ++++
    """

    SIZE_ATTRIBUTE = "size"

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[BROADCAST_CHANNELWISE_MODES, ExecutionPattern]:
        return {
            BROADCAST_CHANNELWISE_MODES.I_I_BOOL_BOOL: BroadcastChannelwiseExecutionPattern(
                types.DType.BOOL, types.DType.BOOL
            ),
            BROADCAST_CHANNELWISE_MODES.I_I_UINT8_UINT8: BroadcastChannelwiseExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            BROADCAST_CHANNELWISE_MODES.I_I_UINT16_UINT16: BroadcastChannelwiseExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            BROADCAST_CHANNELWISE_MODES.I_I_UINT32_UINT32: BroadcastChannelwiseExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
            BROADCAST_CHANNELWISE_MODES.I_I_INT8_INT8: BroadcastChannelwiseExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            BROADCAST_CHANNELWISE_MODES.I_I_INT16_INT16: BroadcastChannelwiseExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            BROADCAST_CHANNELWISE_MODES.I_I_INT32_INT32: BroadcastChannelwiseExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            BROADCAST_CHANNELWISE_MODES.I_I_FLOAT16_FLOAT16: BroadcastChannelwiseExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            BROADCAST_CHANNELWISE_MODES.I_I_FLOAT32_FLOAT32: BroadcastChannelwiseExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class BROADCAST_PLANEWISE_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class BroadcastPlanewiseExecutionPattern(ExecutionPattern):
    def __init__(self, input_dtype, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    input_dtype,
                    types.AType.IMAGE,
                    description="Input Image.",
                    shape=[1, 1, 1],
                )
            ),
            output_pattern=types.DataType(
                output_dtype,
                types.AType.IMAGE,
                description="Output Image.",
                shape=["size[0]", "size[1]", 1],
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            BROADCAST_PLANEWISE.SIZE_ATTRIBUTE: types.DataType(
                types.DType.UINT32,
                types.AType.VECTOR,
                shape=[2],
                description="Output shape of the Image",
            )
        }


class BROADCAST_PLANEWISE(Op):
    """
    Broadcast a scalar to a single channel image, usually for onward use in image operations,

    [latexmath]
    ++++
    output[y, x, 0] = input \; \\forall \; y \\in [0, size[0]), x \\in [0, size[1]).
    ++++
    """

    SIZE_ATTRIBUTE = "size"

    def to_flatbuffer(self):
        import vosa_serialization.BROADCAST_PLANEWISE
        import vosa_serialization.BROADCAST_PLANEWISE_MODE
        import vosa_serialization.BROADCAST_PLANEWISE_ATTRIBUTE

        broadcast_planewise_modes = {
            BROADCAST_PLANEWISE_MODES.I_I_UINT8_UINT8: vosa_serialization.BROADCAST_PLANEWISE_MODE.BROADCAST_PLANEWISE_MODE.I_I_UINT8_UINT8,
            BROADCAST_PLANEWISE_MODES.I_I_UINT16_UINT16: vosa_serialization.BROADCAST_PLANEWISE_MODE.BROADCAST_PLANEWISE_MODE.I_I_UINT16_UINT16,
            BROADCAST_PLANEWISE_MODES.I_I_UINT32_UINT32: vosa_serialization.BROADCAST_PLANEWISE_MODE.BROADCAST_PLANEWISE_MODE.I_I_UINT32_UINT32,
            BROADCAST_PLANEWISE_MODES.I_I_INT8_INT8: vosa_serialization.BROADCAST_PLANEWISE_MODE.BROADCAST_PLANEWISE_MODE.I_I_INT8_INT8,
            BROADCAST_PLANEWISE_MODES.I_I_INT16_INT16: vosa_serialization.BROADCAST_PLANEWISE_MODE.BROADCAST_PLANEWISE_MODE.I_I_INT16_INT16,
            BROADCAST_PLANEWISE_MODES.I_I_INT32_INT32: vosa_serialization.BROADCAST_PLANEWISE_MODE.BROADCAST_PLANEWISE_MODE.I_I_INT32_INT32,
            BROADCAST_PLANEWISE_MODES.I_I_FLOAT16_FLOAT16: vosa_serialization.BROADCAST_PLANEWISE_MODE.BROADCAST_PLANEWISE_MODE.I_I_FLOAT16_FLOAT16,
            BROADCAST_PLANEWISE_MODES.I_I_FLOAT32_FLOAT32: vosa_serialization.BROADCAST_PLANEWISE_MODE.BROADCAST_PLANEWISE_MODE.I_I_FLOAT32_FLOAT32,
        }

        broadcast_scalar_t = (
            vosa_serialization.BROADCAST_PLANEWISE.BROADCAST_PLANEWISET()
        )
        broadcast_scalar_t.mode = broadcast_planewise_modes[self.get_operator_mode()]
        broadcast_scalar_t.attr = (
            vosa_serialization.BROADCAST_PLANEWISE_ATTRIBUTE.BROADCAST_PLANEWISE_ATTRIBUTET()
        )
        broadcast_scalar_t.attr.size = self._attributes[self.SIZE_ATTRIBUTE].value()

        return broadcast_scalar_t

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[BROADCAST_PLANEWISE_MODES, ExecutionPattern]:
        return {
            BROADCAST_PLANEWISE_MODES.I_I_UINT8_UINT8: BroadcastPlanewiseExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            BROADCAST_PLANEWISE_MODES.I_I_UINT16_UINT16: BroadcastPlanewiseExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            BROADCAST_PLANEWISE_MODES.I_I_UINT32_UINT32: BroadcastPlanewiseExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
            BROADCAST_PLANEWISE_MODES.I_I_INT8_INT8: BroadcastPlanewiseExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            BROADCAST_PLANEWISE_MODES.I_I_INT16_INT16: BroadcastPlanewiseExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            BROADCAST_PLANEWISE_MODES.I_I_INT32_INT32: BroadcastPlanewiseExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            BROADCAST_PLANEWISE_MODES.I_I_FLOAT16_FLOAT16: BroadcastPlanewiseExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            BROADCAST_PLANEWISE_MODES.I_I_FLOAT32_FLOAT32: BroadcastPlanewiseExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class _CastExecutionPattern(ExecutionPattern):
    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            CAST.ROUNDING_ATTRIBUTE: types.DataType(
                None,
                CAST.ROUNDING_MODE,
                description="Selects the behaviour of the operation when downcasting.",
            )
        }


class ScalarCastExecutionPattern(_CastExecutionPattern):
    def __init__(self, input_dtype, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    input_dtype, types.AType.SCALAR, description="Input.", shape=[]
                )
            ),
            output_pattern=types.DataType(
                output_dtype, types.AType.SCALAR, description="Output.", shape=[]
            ),
        )


class ImageCastExecutionPattern(_CastExecutionPattern):
    def __init__(self, input_dtype, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    input_dtype,
                    types.AType.IMAGE,
                    description="Input.",
                    shape=["C", "H", "W"],
                )
            ),
            output_pattern=types.DataType(
                output_dtype,
                types.AType.IMAGE,
                description="Output.",
                shape=["C", "H", "W"],
            ),
        )


class CAST_MODES(OPERATOR_MODES):
    I_I_ANY_ANY = enum.auto()


# NOTE: This class is only here to provide documentation for the VOSA Spec. The actual Op logic is encapsulated
#       within :func:`vosa.ops.cast_image`
class CAST(Op):
    class ROUNDING_MODE(types.AttributeEnum):
        "Enumeration for the selection  of behaviour during casting."
        _init_ = "value __doc__"
        WRAP = enum.auto(), "Naive C style casting is employed."
        SATURATE = (
            enum.auto(),
            """The output is the closest representable value in the output DType,
[latexmath]
++++
M = \\textrm{max_representable}(output\_t), \\\\
m = \\textrm{lowest_representable}(output\_t), \\\\
output[c,y,x] = \\max(\\min(input[c,y,x], M), m).
++++
""",
        )
        REINTERPET = (
            enum.auto(),
            "Naive bitwise reinterpretation. Preserves LSBs on casting to lower bitwidths and populates LSBs on casting to higher bitwidths.",
        )

    ROUNDING_ATTRIBUTE = "rounding_mode"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[CAST_MODES, ExecutionPattern]:
        return {
            CAST_MODES.I_I_ANY_ANY: ImageCastExecutionPattern(
                types.DType.ANY, types.DType.ANY
            ),
        }


CAST.__doc__ = """
Cast an Image from one Dtype to another.

In cases where a loss of precision is possible (due to the ouput DType not being able to represent values of the input DType), the rounding_mode attribute defines the behaviour. In upcasting the attribute is unused.

%s

""" % "\n".join(
    types.aenum_to_table(CAST.ROUNDING_MODE)
)


class ChannnelExtractExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image.",
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                shape=["H", "W", "N"],
                description="Output Channel Image.",
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            CHANNEL_EXTRACT.CHANNELS_ATTRIBUTE: types.DataType(
                types.DType.UINT32,
                types.AType.VECTOR,
                shape=["N"],
                description="Indices of the channels to extract.",
            )
        }


class CHANNEL_EXTRACT_MODES(OPERATOR_MODES):
    I_I_BOOL_BOOL = enum.auto()
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class CHANNEL_EXTRACT(Op):
    """
    Extract a list of channels from an Image and return the concatenated result,
    [latexmath]
    ++++
    output[y,x,c] = input[y,x,channels[c]].
    ++++

    NOTE: All indices `c` in `channels` should be valid (i.e. `c` < `C`)
    """

    CHANNELS_ATTRIBUTE = "channels"

    def to_flatbuffer(self):
        import vosa_serialization.CHANNEL_EXTRACT
        import vosa_serialization.CHANNEL_EXTRACT_MODE
        import vosa_serialization.CHANNEL_EXTRACT_ATTRIBUTE

        channel_extract_modes = {
            CHANNEL_EXTRACT_MODES.I_I_BOOL_BOOL: vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_BOOL_BOOL,
            CHANNEL_EXTRACT_MODES.I_I_UINT8_UINT8: vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_UINT8_UINT8,
            CHANNEL_EXTRACT_MODES.I_I_UINT16_UINT16: vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_UINT16_UINT16,
            CHANNEL_EXTRACT_MODES.I_I_UINT32_UINT32: vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_UINT32_UINT32,
            CHANNEL_EXTRACT_MODES.I_I_INT8_INT8: vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_INT8_INT8,
            CHANNEL_EXTRACT_MODES.I_I_INT16_INT16: vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_INT16_INT16,
            CHANNEL_EXTRACT_MODES.I_I_INT32_INT32: vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_INT32_INT32,
            CHANNEL_EXTRACT_MODES.I_I_FLOAT16_FLOAT16: vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_FLOAT16_FLOAT16,
            CHANNEL_EXTRACT_MODES.I_I_FLOAT32_FLOAT32: vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_FLOAT32_FLOAT32,
        }

        channel_extract_t = vosa_serialization.CHANNEL_EXTRACT.CHANNEL_EXTRACTT()
        channel_extract_t.mode = channel_extract_modes[self.get_operator_mode()]
        channel_extract_t.attr = (
            vosa_serialization.CHANNEL_EXTRACT_ATTRIBUTE.CHANNEL_EXTRACT_ATTRIBUTET()
        )
        channel_extract_t.attr.channels = self._attributes[
            self.CHANNELS_ATTRIBUTE
        ].value()

        return channel_extract_t

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[CHANNEL_EXTRACT_MODES, ExecutionPattern]:
        return {
            CHANNEL_EXTRACT_MODES.I_I_BOOL_BOOL: ChannnelExtractExecutionPattern(
                types.DType.BOOL
            ),
            CHANNEL_EXTRACT_MODES.I_I_UINT8_UINT8: ChannnelExtractExecutionPattern(
                types.DType.UINT8
            ),
            CHANNEL_EXTRACT_MODES.I_I_UINT16_UINT16: ChannnelExtractExecutionPattern(
                types.DType.UINT16
            ),
            CHANNEL_EXTRACT_MODES.I_I_UINT32_UINT32: ChannnelExtractExecutionPattern(
                types.DType.UINT32
            ),
            CHANNEL_EXTRACT_MODES.I_I_INT8_INT8: ChannnelExtractExecutionPattern(
                types.DType.INT8
            ),
            CHANNEL_EXTRACT_MODES.I_I_INT16_INT16: ChannnelExtractExecutionPattern(
                types.DType.INT16
            ),
            CHANNEL_EXTRACT_MODES.I_I_INT32_INT32: ChannnelExtractExecutionPattern(
                types.DType.INT32
            ),
            CHANNEL_EXTRACT_MODES.I_I_FLOAT16_FLOAT16: ChannnelExtractExecutionPattern(
                types.DType.FLOAT16
            ),
            CHANNEL_EXTRACT_MODES.I_I_FLOAT32_FLOAT32: ChannnelExtractExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class ConcatExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputArrayPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    description="Array of input Images",
                    shape=["[A]", "H", "W", "C"],
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                description="Output Image",
                shape=["H", "W", "C[0]+...+C[-1]"],
            ),
        ),


class CONCAT_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class CONCAT(Op):
    """
    Perform a channelwise concatenation of two or more Images.

    Images must have the same height and width but may have arbitrary number of channels. The number of channels in the output Image is the sum of the number of channels in the input Images.
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[CONCAT_MODES, ExecutionPattern]:
        return {
            CONCAT_MODES.I_I_UINT8_UINT8: ConcatExecutionPattern(types.DType.UINT8),
            CONCAT_MODES.I_I_UINT16_UINT16: ConcatExecutionPattern(types.DType.UINT16),
            CONCAT_MODES.I_I_UINT32_UINT32: ConcatExecutionPattern(types.DType.UINT32),
            CONCAT_MODES.I_I_INT8_INT8: ConcatExecutionPattern(types.DType.INT8),
            CONCAT_MODES.I_I_INT16_INT16: ConcatExecutionPattern(types.DType.INT16),
            CONCAT_MODES.I_I_INT32_INT32: ConcatExecutionPattern(types.DType.INT32),
            CONCAT_MODES.I_I_FLOAT16_FLOAT16: ConcatExecutionPattern(
                types.DType.FLOAT16
            ),
            CONCAT_MODES.I_I_FLOAT32_FLOAT32: ConcatExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class ConstScalarExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(),
            output_pattern=types.DataType(
                dtype, types.AType.SCALAR, description="Output Scalar", shape=[]
            ),
            dynamic_attribute_pattern={
                "static_scalar": types.DataType(
                    dtype,
                    types.AType.SCALAR,
                    description="Statically defined Scalar.",
                    shape=[],
                )
            },
        ),


class CONST_SCALAR_MODES(OPERATOR_MODES):
    S_UINT8 = enum.auto()
    S_UINT16 = enum.auto()
    S_UINT32 = enum.auto()
    S_INT8 = enum.auto()
    S_INT16 = enum.auto()
    S_INT32 = enum.auto()
    S_FLOAT16 = enum.auto()
    S_FLOAT32 = enum.auto()


class CONST_SCALAR(Op):
    """
    Return a statically defined Scalar,
    [latexmath]
    ++++
    output = static\_scalar.
    ++++
    """

    VALUE = "value"

    def to_flatbuffer(self):
        import vosa_serialization.CONST_SCALAR
        import vosa_serialization.CONST_SCALAR_MODE
        import vosa_serialization.CONST_SCALAR_ATTRIBUTE

        const_scalar_modes = {
            CONST_SCALAR_MODES.S_UINT8: vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_UINT8,
            CONST_SCALAR_MODES.S_UINT16: vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_UINT16,
            CONST_SCALAR_MODES.S_UINT32: vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_UINT32,
            CONST_SCALAR_MODES.S_INT8: vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_INT8,
            CONST_SCALAR_MODES.S_INT16: vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_INT16,
            CONST_SCALAR_MODES.S_INT32: vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_INT32,
            CONST_SCALAR_MODES.S_FLOAT16: vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_FLOAT16,
            CONST_SCALAR_MODES.S_FLOAT32: vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_FLOAT32,
        }

        const_scalart = vosa_serialization.CONST_SCALAR.CONST_SCALART()
        const_scalart.mode = const_scalar_modes[self.get_operator_mode()]
        const_scalart.attr = (
            vosa_serialization.CONST_SCALAR_ATTRIBUTE.CONST_SCALAR_ATTRIBUTET()
        )
        const_scalart.attr.value = self._pack_value(self._attributes[self.VALUE])

        return const_scalart

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[CONST_SCALAR_MODES, ConstScalarExecutionPattern]:
        return {
            CONST_SCALAR_MODES.S_UINT8: ConstScalarExecutionPattern(types.DType.UINT8),
            CONST_SCALAR_MODES.S_UINT16: ConstScalarExecutionPattern(
                types.DType.UINT16
            ),
            CONST_SCALAR_MODES.S_UINT32: ConstScalarExecutionPattern(
                types.DType.UINT32
            ),
            CONST_SCALAR_MODES.S_INT8: ConstScalarExecutionPattern(types.DType.INT8),
            CONST_SCALAR_MODES.S_INT16: ConstScalarExecutionPattern(types.DType.INT16),
            CONST_SCALAR_MODES.S_INT32: ConstScalarExecutionPattern(types.DType.INT32),
            CONST_SCALAR_MODES.S_FLOAT16: ConstScalarExecutionPattern(
                types.DType.FLOAT16
            ),
            CONST_SCALAR_MODES.S_FLOAT32: ConstScalarExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class ConstImageExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                description="Output Image",
                shape=["H", "W", "C"],
            ),
            dynamic_attribute_pattern={
                "static_image": types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    description="Statically defined Image.",
                    shape=["H", "W", "C"],
                )
            },
        ),


class CONST_IMAGE_MODES(OPERATOR_MODES):
    I_BOOL = enum.auto()
    I_UINT8 = enum.auto()
    I_UINT16 = enum.auto()
    I_UINT32 = enum.auto()
    I_INT8 = enum.auto()
    I_INT16 = enum.auto()
    I_INT32 = enum.auto()
    I_FLOAT16 = enum.auto()
    I_FLOAT32 = enum.auto()


class CONST_IMAGE(Op):
    """
    Return a statically defined Image,
    [latexmath]
    ++++
    output[y,x,c] = static\_image[y,x,c].
    ++++
    """

    VALUE = "value"
    DIMENSION = "dimension"

    def to_flatbuffer(self):
        import vosa_serialization.CONST_IMAGE
        import vosa_serialization.CONST_IMAGE_MODE
        import vosa_serialization.CONST_IMAGE_ATTRIBUTE

        const_image_modes = {
            CONST_IMAGE_MODES.I_BOOL: vosa_serialization.CONST_IMAGE_MODE.CONST_IMAGE_MODE.I_BOOL,
            CONST_IMAGE_MODES.I_UINT8: vosa_serialization.CONST_IMAGE_MODE.CONST_IMAGE_MODE.I_UINT8,
            CONST_IMAGE_MODES.I_UINT16: vosa_serialization.CONST_IMAGE_MODE.CONST_IMAGE_MODE.I_UINT16,
            CONST_IMAGE_MODES.I_UINT32: vosa_serialization.CONST_IMAGE_MODE.CONST_IMAGE_MODE.I_UINT32,
            CONST_IMAGE_MODES.I_INT8: vosa_serialization.CONST_IMAGE_MODE.CONST_IMAGE_MODE.I_INT8,
            CONST_IMAGE_MODES.I_INT16: vosa_serialization.CONST_IMAGE_MODE.CONST_IMAGE_MODE.I_INT16,
            CONST_IMAGE_MODES.I_INT32: vosa_serialization.CONST_IMAGE_MODE.CONST_IMAGE_MODE.I_INT32,
            CONST_IMAGE_MODES.I_FLOAT16: vosa_serialization.CONST_IMAGE_MODE.CONST_IMAGE_MODE.I_FLOAT16,
            CONST_IMAGE_MODES.I_FLOAT32: vosa_serialization.CONST_IMAGE_MODE.CONST_IMAGE_MODE.I_FLOAT32,
        }

        const_image_t = vosa_serialization.CONST_IMAGE.CONST_IMAGET()
        const_image_t.mode = const_image_modes[self.get_operator_mode()]
        const_image_t.attr = (
            vosa_serialization.CONST_IMAGE_ATTRIBUTE.CONST_IMAGE_ATTRIBUTET()
        )
        const_image_t.attr.value = self._pack_value(self._attributes[self.VALUE])
        const_image_t.attr.dimension = self._attributes[self.DIMENSION].value()

        return const_image_t

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[CONST_IMAGE_MODES, ConstImageExecutionPattern]:
        return {
            CONST_IMAGE_MODES.I_BOOL: ConstImageExecutionPattern(types.DType.BOOL),
            CONST_IMAGE_MODES.I_UINT8: ConstImageExecutionPattern(types.DType.UINT8),
            CONST_IMAGE_MODES.I_UINT16: ConstImageExecutionPattern(types.DType.UINT16),
            CONST_IMAGE_MODES.I_UINT32: ConstImageExecutionPattern(types.DType.UINT32),
            CONST_IMAGE_MODES.I_INT8: ConstImageExecutionPattern(types.DType.INT8),
            CONST_IMAGE_MODES.I_INT16: ConstImageExecutionPattern(types.DType.INT16),
            CONST_IMAGE_MODES.I_INT32: ConstImageExecutionPattern(types.DType.INT32),
            CONST_IMAGE_MODES.I_FLOAT16: ConstImageExecutionPattern(
                types.DType.FLOAT16
            ),
            CONST_IMAGE_MODES.I_FLOAT32: ConstImageExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class ConstPlaneExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(),
            output_pattern=types.DataType(
                dtype, types.AType.PLANE, description="Output Plane", shape=["H", "W"]
            ),
            dynamic_attribute_pattern={
                "static_plane": types.DataType(
                    dtype,
                    types.AType.PLANE,
                    description="Statically defined Plane",
                    shape=["H", "W"],
                )
            },
        ),


class CONST_PLANE_MODES(OPERATOR_MODES):
    P_UINT8 = enum.auto()
    P_UINT16 = enum.auto()
    P_UINT32 = enum.auto()
    P_INT8 = enum.auto()
    P_INT16 = enum.auto()
    P_INT32 = enum.auto()
    P_FLOAT16 = enum.auto()
    P_FLOAT32 = enum.auto()


class CONST_PLANE(Op):
    """
    Return a statically defined Plane,
    [latexmath]
    ++++
    output[y,x] = static\_plane[y,x].
    ++++
    """

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[CONST_PLANE_MODES, ConstPlaneExecutionPattern]:
        return {
            CONST_PLANE_MODES.P_UINT8: ConstPlaneExecutionPattern(types.DType.UINT8),
            CONST_PLANE_MODES.P_UINT16: ConstPlaneExecutionPattern(types.DType.UINT16),
            CONST_PLANE_MODES.P_UINT32: ConstPlaneExecutionPattern(types.DType.UINT32),
            CONST_PLANE_MODES.P_INT8: ConstPlaneExecutionPattern(types.DType.INT8),
            CONST_PLANE_MODES.P_INT16: ConstPlaneExecutionPattern(types.DType.INT16),
            CONST_PLANE_MODES.P_INT32: ConstPlaneExecutionPattern(types.DType.INT32),
            CONST_PLANE_MODES.P_FLOAT16: ConstPlaneExecutionPattern(
                types.DType.FLOAT16
            ),
            CONST_PLANE_MODES.P_FLOAT32: ConstPlaneExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class ImportChannelExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.SpecialType.DATA,
                    description="Raw data to import the plane from.",
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                shape=["shape[0]", "shape[1]", 1],
                description="The imported single channel Image.",
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            IMPORT_CHANNEL.RAW_STRIDE: types.DataType(
                types.DType.UINT32,
                types.AType.SCALAR,
                shape=[],
                description="The number of bytes between values.",
            ),
            IMPORT_CHANNEL.RAW_OFFSET: types.DataType(
                types.DType.UINT32,
                types.AType.SCALAR,
                shape=[],
                description="The address of the first value relative to the start of the data object as a whole.",
            ),
            IMPORT_CHANNEL.CHANNEL_SIZE: types.DataType(
                types.DType.UINT32,
                types.AType.VECTOR,
                shape=[2],
                description="The shape of the channel to import.",
            ),
        }


class IMPORT_CHANNEL_EXECUTION_MODES(OPERATOR_MODES):
    D_I_UINT8_UINT8 = enum.auto()
    D_I_UINT16_UINT16 = enum.auto()
    D_I_UINT32_UINT32 = enum.auto()
    D_I_INT8_INT8 = enum.auto()
    D_I_INT16_INT16 = enum.auto()
    D_I_INT32_INT32 = enum.auto()
    D_I_FLOAT16_FLOAT16 = enum.auto()
    D_I_FLOAT32_FLOAT32 = enum.auto()


class IMPORT_CHANNEL(Op):
    """
    Import a channel from a raw Data object.

    The `i`th value in the raw data is accessed as,
    [source, c++]
    ----
    value[i] =  (output_t) *( ((byte*) input) + offset + i*stride)
    ----
    Values are set into the output Image in the `[H, W, 1]` order (row by row). The total nunber of values read nad set is equal to the total size of the plane (shape[0]*shape[1]).
    """

    RAW_STRIDE = "stride"
    RAW_OFFSET = "offset"
    CHANNEL_SIZE = "shape"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[OPERATOR_MODES, ExecutionPattern]:
        return {
            IMPORT_CHANNEL_EXECUTION_MODES.D_I_UINT8_UINT8: ImportChannelExecutionPattern(
                types.DType.UINT8
            ),
            IMPORT_CHANNEL_EXECUTION_MODES.D_I_UINT16_UINT16: ImportChannelExecutionPattern(
                types.DType.UINT16
            ),
            IMPORT_CHANNEL_EXECUTION_MODES.D_I_UINT32_UINT32: ImportChannelExecutionPattern(
                types.DType.UINT32
            ),
            IMPORT_CHANNEL_EXECUTION_MODES.D_I_INT8_INT8: ImportChannelExecutionPattern(
                types.DType.INT8
            ),
            IMPORT_CHANNEL_EXECUTION_MODES.D_I_INT16_INT16: ImportChannelExecutionPattern(
                types.DType.INT16
            ),
            IMPORT_CHANNEL_EXECUTION_MODES.D_I_INT32_INT32: ImportChannelExecutionPattern(
                types.DType.INT32
            ),
            IMPORT_CHANNEL_EXECUTION_MODES.D_I_FLOAT16_FLOAT16: ImportChannelExecutionPattern(
                types.DType.FLOAT16
            ),
            IMPORT_CHANNEL_EXECUTION_MODES.D_I_FLOAT32_FLOAT32: ImportChannelExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class ExportChannelExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H, W", 1],
                    description="The single channel Image to write into the raw data",
                )
            ),
            output_pattern=None,
            input_output_pattern=InputPattern(
                types.DataType(
                    None,
                    types.SpecialType.DATA,
                    description="Raw data to write the plane into.",
                )
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            EXPORT_CHANNEL.RAW_STRIDE: types.DataType(
                types.DType.UINT32,
                types.AType.SCALAR,
                shape=[],
                description="The number of bytes between values.",
            ),
            EXPORT_CHANNEL.RAW_OFFSET: types.DataType(
                types.DType.UINT32,
                types.AType.SCALAR,
                shape=[],
                description="The address of the first value relative to the start of the data object as a whole.",
            ),
        }


class EXPORT_CHANNEL_EXECUTION_MODES(OPERATOR_MODES):
    I_D_UINT8_UINT8 = enum.auto()
    I_D_UINT16_UINT16 = enum.auto()
    I_D_UINT32_UINT32 = enum.auto()
    I_D_INT8_INT8 = enum.auto()
    I_D_INT16_INT16 = enum.auto()
    I_D_INT32_INT32 = enum.auto()
    I_D_FLOAT16_FLOAT16 = enum.auto()
    I_D_FLOAT32_FLOAT32 = enum.auto()


class EXPORT_CHANNEL(Op):
    """
    Export a single channel Image into a raw Data object.

    The `i`th value in the raw data is accessed as,
    [source, c++]
    ----
    value[i] =  (input_1_t) *( ((byte*) input) + offset + i*stride)
    ----
    Values are set in the Data output reading from the input Plane in the `[H, W, 1]` order (row by row). The total nunber of values read nad set is equal to the total size of the channel (H*W).
    """

    RAW_STRIDE = "stride"
    RAW_OFFSET = "offset"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[OPERATOR_MODES, ExecutionPattern]:
        return {
            EXPORT_CHANNEL_EXECUTION_MODES.I_D_UINT8_UINT8: ExportChannelExecutionPattern(
                types.DType.UINT8
            ),
            EXPORT_CHANNEL_EXECUTION_MODES.I_D_UINT16_UINT16: ExportChannelExecutionPattern(
                types.DType.UINT16
            ),
            EXPORT_CHANNEL_EXECUTION_MODES.I_D_UINT32_UINT32: ExportChannelExecutionPattern(
                types.DType.UINT32
            ),
            EXPORT_CHANNEL_EXECUTION_MODES.I_D_INT8_INT8: ExportChannelExecutionPattern(
                types.DType.INT8
            ),
            EXPORT_CHANNEL_EXECUTION_MODES.I_D_INT16_INT16: ExportChannelExecutionPattern(
                types.DType.INT16
            ),
            EXPORT_CHANNEL_EXECUTION_MODES.I_D_INT32_INT32: ExportChannelExecutionPattern(
                types.DType.INT32
            ),
            EXPORT_CHANNEL_EXECUTION_MODES.I_D_FLOAT16_FLOAT16: ExportChannelExecutionPattern(
                types.DType.FLOAT16
            ),
            EXPORT_CHANNEL_EXECUTION_MODES.I_D_FLOAT32_FLOAT32: ExportChannelExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class MeshGridExecutionPattern(ExecutionPattern):
    def __init__(self):
        super().__init__(
            input_pattern=InputPattern(),
            output_pattern=types.DataType(
                types.DType.UINT32,
                types.AType.IMAGE,
                shape=["shape[0], shape[1], 2"],
                description="The output Image",
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            "shape": types.DataType(
                types.DType.UINT32,
                types.AType.VECTOR,
                shape=[2],
                description="The y,x shape of the output Image.",
            )
        }


class MESH_GRID_EXECUTION_MODES(OPERATOR_MODES):
    I_UINT32 = enum.auto()


class MESH_GRID(Op):
    """
    Create an Image where the values of the pixels are their coordinates in the Image,
    [latexmath]
    ++++
    \\begin{array}{c}
    output[y,x,0] = y \\\\
    output[y,x,1] = x.
    \\end{array}
    ++++
    """

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[MESH_GRID_EXECUTION_MODES, MeshGridExecutionPattern]:
        return {MESH_GRID_EXECUTION_MODES.I_UINT32: MeshGridExecutionPattern()}


class PiecewiseLinearExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    description="Input Image.",
                    shape=["H", "W", "C"],
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                description="Output Image.",
                shape=["H", "W", "C"],
            ),
            dynamic_attribute_pattern={
                PIECEWISE_LINEAR.NODES: types.DataType(
                    dtype,
                    types.AType.VECTOR,
                    shape=["N"],
                    description="Nodes at which the piecewise linear function is defined.",
                ),
                PIECEWISE_LINEAR.VALUES: types.DataType(
                    dtype,
                    types.AType.VECTOR,
                    shape=["N"],
                    description="Function values at the given nodes.",
                ),
            },
        )


class PIECEWISE_LINEAR_MODES(OPERATOR_MODES):
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class PIECEWISE_LINEAR(Op):
    """
    Apply a piecewise linear function to an Image. The function is defined by the `nodes` and `values` attributes. Given
    [latexmath]
    ++++
    \\begin{array}{c}
    delta = values[n+1] - values[n]\\\\
    offset = input[c,y,x] - nodes[n] \\\\
    norm = nodes[n+1] - nodes[n]
    \\end{array},
    ++++
    then for floating point input types,
    [latexmath]
    ++++
    output[y,x,c] = \\left\\{ \\begin{array}{lr}
    values[0] & input[y,x,c] \\leq nodes[0] \\\\
    values[n] +  \\frac{delta\\cdot offset}{norm} & nodes[n]  < input[y,x,c] \\leq nodes[n+1] \\\\
    values[N-1] & nodes[N-1] < input[y,x,c]
    \\end{array}\\right. ,
    ++++
    and for integer input types,
    [latexmath]
    ++++
    output[y,x,c] = \\left\\{ \\begin{array}{lr}
    values[0] & input[y,x,c] \\leq nodes[0] \\\\
    values[n] + \\mathrm{int\\_norm}(delta\\cdot offset, norm) & nodes[n]  < input[y,x,c] \\leq nodes[n+1]\\\\
    values[N-1] & nodes[N-1] < input[y,x,c]
    \\end{array}\\right. ,
    ++++
    where,
    [latexmath]
    ++++
    \\mathrm{int\\_norm}(a,b) = \\left(\\frac{a  \\ll 1}{b}  + 1 \\right)\\gg 1 .
    ++++

    """

    NODES = "nodes"
    VALUES = "values"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[PIECEWISE_LINEAR_MODES, ExecutionPattern]:
        return {
            PIECEWISE_LINEAR_MODES.I_I_INT8_INT8: PiecewiseLinearExecutionPattern(
                types.DType.INT8
            ),
            PIECEWISE_LINEAR_MODES.I_I_INT16_INT16: PiecewiseLinearExecutionPattern(
                types.DType.INT16
            ),
            PIECEWISE_LINEAR_MODES.I_I_INT32_INT32: PiecewiseLinearExecutionPattern(
                types.DType.INT32
            ),
            PIECEWISE_LINEAR_MODES.I_I_FLOAT16_FLOAT16: PiecewiseLinearExecutionPattern(
                types.DType.FLOAT16
            ),
            PIECEWISE_LINEAR_MODES.I_I_FLOAT32_FLOAT32: PiecewiseLinearExecutionPattern(
                types.DType.FLOAT32
            ),
        }


# class ReshapeExecutionPattern(ExecutionPattern):
#     def __init__(self, dtype):
#         super().__init__(input_pattern=InputPattern(
#             types.DataType(dtype, types.AType.ARRAY, shape=["..."],
#                            description="Input Array.")),
#             output_pattern=types.DataType(dtype, types.AType.ARRAY,
#                                           shape=["output_shape[0]", '...', 'output_shape[N-1]'],
#                                           description="Reshaped Output Array."))
#
#     def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
#         return {
#             RESHAPE.RESHAPE_ATTRIBUTE: types.DataType(types.DType.UINT32, types.AType.ARRAY, shape=["N"],
#                                                       description="The desired shape of the output array. ")
#         }
#
#
# class RESHAPE_MODES(OPERATOR_MODES):
#     A_A_BOOL_BOOL = enum.auto()
#     A_A_UINT8_UINT8 = enum.auto()
#     A_A_UINT32_UINT32 = enum.auto()
#     A_A_INT8_INT8 = enum.auto()
#     A_A_INT16_INT16 = enum.auto()
#     A_A_INT32_INT32 = enum.auto()
#     A_A_FLOAT16_FLOAT16 = enum.auto()
#     A_A_FLOAT32_FLOAT32 = enum.auto()
#
#
# class RESHAPE(Op):
#     """
#     Reshape an Array.
#
#     A new array is created with the dimension `output_shape`. The array is populated with the values from the `input` Array, in order.
#     """
#     RESHAPE_ATTRIBUTE = "output_shape"
#
#     @classmethod
#     def get_pattern_dict(cls) -> typing.Dict[RESHAPE_MODES, ReshapeExecutionPattern]:
#         return {
#             RESHAPE_MODES.A_A_BOOL_BOOL: ReshapeExecutionPattern(types.DType.BOOL),
#             RESHAPE_MODES.A_A_UINT8_UINT8: ReshapeExecutionPattern(types.DType.UINT8),
#             RESHAPE_MODES.A_A_UINT32_UINT32: ReshapeExecutionPattern(types.DType.UINT32),
#             RESHAPE_MODES.A_A_INT8_INT8: ReshapeExecutionPattern(types.DType.INT8),
#             RESHAPE_MODES.A_A_INT16_INT16: ReshapeExecutionPattern(types.DType.INT16),
#             RESHAPE_MODES.A_A_INT32_INT32: ReshapeExecutionPattern(types.DType.INT32),
#             RESHAPE_MODES.A_A_FLOAT16_FLOAT16: ReshapeExecutionPattern(types.DType.FLOAT16),
#             RESHAPE_MODES.A_A_FLOAT32_FLOAT32: ReshapeExecutionPattern(types.DType.FLOAT32)
#         }


class ShapeExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="The input Image.",
                )
            ),
            output_pattern=types.DataType(
                types.DType.UINT32,
                types.AType.SCALAR,
                shape=[],
                description="The size of the image along the selected dimension.",
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            SHAPE.AXIS_ATTRIBUTE: types.DataType(
                types.DType.UINT32,
                types.AType.SCALAR,
                shape=[],
                description="The axis of which to get the size, must equal 0,1 or 2.",
            )
        }


class SHAPE_MODES(OPERATOR_MODES):
    I_S_UINT8_UINT32 = enum.auto()
    I_S_UINT16_UINT32 = enum.auto()
    I_S_UINT32_UINT32 = enum.auto()
    I_S_INT8_UINT32 = enum.auto()
    I_S_INT16_UINT32 = enum.auto()
    I_S_INT32_UINT32 = enum.auto()
    I_S_FLOAT16_UINT32 = enum.auto()
    I_S_FLOAT32_UINT32 = enum.auto()


class SHAPE(Op):
    """
    Dynamically get the shape of an Image,
    [latexmath]
    ++++
    output = \\left\{ \\begin{array}{cc} C & axis =0  \\\\ H & axis =1 \\\\ W & axis =2 \\end{array} \\right.
    ++++
    """

    AXIS_ATTRIBUTE = "axis"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[SHAPE_MODES, ExecutionPattern]:
        return {
            SHAPE_MODES.I_S_UINT8_UINT32: ShapeExecutionPattern(types.DType.UINT8),
            SHAPE_MODES.I_S_UINT16_UINT32: ShapeExecutionPattern(types.DType.UINT16),
            SHAPE_MODES.I_S_UINT32_UINT32: ShapeExecutionPattern(types.DType.UINT32),
            SHAPE_MODES.I_S_INT8_UINT32: ShapeExecutionPattern(types.DType.INT8),
            SHAPE_MODES.I_S_INT16_UINT32: ShapeExecutionPattern(types.DType.INT16),
            SHAPE_MODES.I_S_INT32_UINT32: ShapeExecutionPattern(types.DType.INT32),
            SHAPE_MODES.I_S_FLOAT16_UINT32: ShapeExecutionPattern(types.DType.FLOAT16),
            SHAPE_MODES.I_S_FLOAT32_UINT32: ShapeExecutionPattern(types.DType.FLOAT32),
        }


class TOSAGraphExecutionPattern(ExecutionPattern):
    def __init__(self, input_dtype, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    input_dtype,
                    types.AType.ANY,
                    description="Array of Input data",
                    shape=["[A]", "ANY"],
                )
            ),
            output_pattern=types.DataType(
                output_dtype, types.AType.ANY, description="Output data", shape=["ANY"]
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            TOSA_GRAPH.BUFFER_ATTRIBUTE: types.DataType(
                types.DType.UINT8,
                types.SpecialType.DATA,
                description="TOSA serialised flatbuffer",
            )
        }


class TOSA_GRAPH_MODES(OPERATOR_MODES):
    ANY_ANY = enum.auto()


# NOTE: This class is only here to provide documentation for the VOSA Spec. The actual Op logic is encapsulated
#       within :func:`vosa.ops.tosa_graph`
class TOSA_GRAPH(Op):
    """
    Wrapper for a TOSA Graph

    The Inputs and Outputs are decided by the TOSA Graph and can be anything. Generally, outputs with more than 3 dimensions (i.e. 4D/5D Tensors) will be flattened on their inner-most dimension(s).

    The `buffer` attribute contains a serialized TOSA Graph which will be de-serialized at runtime.

    NOTE: For the time being, only TOSA Graphs with 1 Output are allowed !
    """

    BUFFER_ATTRIBUTE = "buffer"

    def to_flatbuffer(self):
        import vosa_serialization.TOSA_GRAPH
        import vosa_serialization.TOSA_GRAPH_ATTRIBUTE

        tosa_graph_t = vosa_serialization.TOSA_GRAPH.TOSA_GRAPHT()
        tosa_graph_t.outputType = types.DType.to_flatbuffer(
            self.get_return_data().dtype()
        )
        tosa_graph_t.attr = (
            vosa_serialization.TOSA_GRAPH_ATTRIBUTE.TOSA_GRAPH_ATTRIBUTET()
        )
        tosa_graph_t.attr.buffer = self._attributes[self.BUFFER_ATTRIBUTE].value()

        return tosa_graph_t

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[TOSA_GRAPH_MODES, ExecutionPattern]:
        return {
            TOSA_GRAPH_MODES.ANY_ANY: TOSAGraphExecutionPattern(
                types.DType.ANY, types.DType.ANY
            ),
        }


class TableExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    description="Input Image.",
                    shape=["H", "W", "C"],
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                description="Output Image.",
                shape=["H", "W", "C"],
            ),
            dynamic_attribute_pattern={
                TABLE.TABLE_TABLE: types.DataType(
                    dtype,
                    types.AType.VECTOR,
                    shape=["2^bidwidth(input_t)"],
                    description="Lookup array for the mapping of old to new values of the image",
                )
            },
        )


class TABLE_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()


class TABLE(Op):
    """
    [[TABLE]]
    Lookup table for pixel values,

    [latexmath]
    ++++
    table\_index[y,x,c] = input[y,x,c] - \\mathrm{min\_representable}(\mathrm{dtype}(input)) \\\\
    output[y,x,c] = table[table\_index[y,x,c]].
    ++++
    """

    TABLE_TABLE = "table"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[TABLE_MODES, ExecutionPattern]:
        return {
            TABLE_MODES.I_I_UINT8_UINT8: TableExecutionPattern(types.DType.UINT8),
            TABLE_MODES.I_I_UINT16_UINT16: TableExecutionPattern(types.DType.UINT16),
            TABLE_MODES.I_I_UINT32_UINT32: TableExecutionPattern(types.DType.UINT32),
            TABLE_MODES.I_I_INT8_INT8: TableExecutionPattern(types.DType.INT8),
            TABLE_MODES.I_I_INT16_INT16: TableExecutionPattern(types.DType.INT16),
            TABLE_MODES.I_I_INT32_INT32: TableExecutionPattern(types.DType.INT32),
        }


class MASK_TO_INDEX_MODES(OPERATOR_MODES):
    I_A_BOOL_UINT32 = enum.auto


class MaskToIndexExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    types.DType.BOOL,
                    types.AType.IMAGE,
                    description="Mask.",
                    shape=["H", "W", "C"],
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.ARRAY,
                description="Indices where mask is TRUE.",
                shape=["C", "NF", 2],
            ),
        )


class MASK_TO_INDEX(Op):
    """
    Get list of indices where mask is TRUE
    [latexmath]
    ++++
    input\_mask(output\_list(i, j, 1), output\_list(i, j, 2)) = \\mathrm{TRUE}
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[MASK_TO_INDEX_MODES, ExecutionPattern]:
        return {
            MASK_TO_INDEX_MODES.I_A_BOOL_UINT32: MaskToIndexExecutionPattern(
                types.DType.UINT32
            )
        }


class INDEX_TO_MASK_MODES(OPERATOR_MODES):
    A_I_UINT32_BOOL = enum.auto


class IndexToMaskExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    types.DType.BOOL,
                    types.AType.ARRAY,
                    description="Indices to set mask TRUE.",
                    shape=["C", "NF", 2],
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                description="Output mask.",
                shape=["H", "W", "C"],
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            INDEX_TO_MASK.MASK_SIZE_ATTRIBUTE: types.DataType(
                types.DType.UINT32,
                types.AType.VECTOR,
                description="Output shape of the mask",
                shape=["H", "W"],
            )
        }


class INDEX_TO_MASK(Op):
    """
    Generator a mask which pixel corresponding to input indices are set to true
    [latexmath]
    ++++
    output\_mask(input\_list(i, j, 1), input\_list(i, j, 2)) = \\mathrm{TRUE}
    ++++
    """

    MASK_SIZE_ATTRIBUTE = "mask_size"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[INDEX_TO_MASK_MODES, ExecutionPattern]:
        return {
            INDEX_TO_MASK_MODES.A_I_UINT32_BOOL: IndexToMaskExecutionPattern(
                types.DType.UINT32
            )
        }


class WhereExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image 0.",
                ),
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image 1.",
                ),
                types.DataType(
                    types.DType.BOOL,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Mask of input values to use.",
                ),
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                shape=["H", "W", "C"],
                description="Output Image",
            ),
        )


class WHERE_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class WHERE(Op):
    """
    Pixel-wise select between two inputs based on the values in the `where_mask`.
    [latexmath]
    ++++
    output[y,x,c] = \\left\\{ \\begin{array}{lr} input_0[y,x,c]  &  input_2[y,x,c] \\\\  input_1[y,x,c] & \\textrm{otherwise} \\end{array} \\right.
    ++++
    """

    WHERE_MASK = "where_mask"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[WHERE_MODES, ExecutionPattern]:
        return {
            WHERE_MODES.I_I_UINT8_UINT8: WhereExecutionPattern(types.DType.UINT8),
            WHERE_MODES.I_I_UINT16_UINT16: WhereExecutionPattern(types.DType.UINT16),
            WHERE_MODES.I_I_UINT32_UINT32: WhereExecutionPattern(types.DType.UINT32),
            WHERE_MODES.I_I_INT8_INT8: WhereExecutionPattern(types.DType.INT8),
            WHERE_MODES.I_I_INT16_INT16: WhereExecutionPattern(types.DType.INT16),
            WHERE_MODES.I_I_INT32_INT32: WhereExecutionPattern(types.DType.INT32),
            WHERE_MODES.I_I_FLOAT16_FLOAT16: WhereExecutionPattern(types.DType.FLOAT16),
            WHERE_MODES.I_I_FLOAT32_FLOAT32: WhereExecutionPattern(types.DType.FLOAT32),
        }
