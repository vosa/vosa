/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/pointwise_matrix_multiply.h"
#include "test_bench.h"
#include "vosa/vosa.h"

template <typename T>
vosa::EigenImage<T> run_pointwise_matrix_multiply(vosa::EigenImage<T> &image,
                                                  vosa::EigenPlane<T> &M,
                                                  vosa::EigenVector<T> &K_1,
                                                  vosa::EigenVector<T> &K_2) {
  auto Op = vosa::PointwiseMatrixMultiply<T, T>(M, K_1, K_2);
  return Op.eval({std::make_shared<vosa::ArrayBase>(image)})
      ->template get_image<T>();
}

TEST(test_pointwise_matrix_multiply,
     test_pointwise_matrix_multiply_color_space_conversion) {

  auto input_array = vosa::EigenImage<float>(1, 1, 3);
  input_array.setValues({{{100.0, 200.0, 300.0}}});

  auto M = vosa::EigenPlane<float>(3, 3);
  M.setValues({{1.0, 2.0, 3.0}, {4.0, 5.0, 6.0}, {7.0, 8.0, 9.0}});
  auto K_1 = vosa::EigenVector<float>(3);
  K_1.setValues({1.0, 2.0, 3.0});
  auto K_2 = vosa::EigenVector<float>(3);
  K_2.setValues({1.0, 2.0, 3.0});

  auto csc_op = vosa::PointwiseMatrixMultiply<float, float>(M, K_1, K_2);
  auto image_image = std::make_shared<vosa::ArrayBase>(input_array);

  auto output = csc_op.eval({image_image});
  auto csc_output = output->get_image<float>();

  vosa::EigenVector<float> D(3);
  D.setValues({input_array(0, 0, 0) - K_1(0), input_array(0, 0, 1) - K_1(1),
               input_array(0, 0, 2) - K_1(2)});
  float expected_zero =
      D(0) * M(0, 0) + D(1) * M(0, 1) + D(2) * M(0, 2) + K_2(0);
  float expected_one =
      D(0) * M(1, 0) + D(1) * M(1, 1) + D(2) * M(1, 2) + K_2(1);
  float expected_two =
      D(0) * M(2, 0) + D(1) * M(2, 1) + D(2) * M(2, 2) + K_2(2);

  auto expected_output = vosa::EigenImage<float>(1, 1, 3);
  expected_output.setValues({{{expected_zero, expected_one, expected_two}}});

  auto test_output = vosa::test::TestBench<float, int32_t>::compareEigenImages(
      expected_output, csc_output);
  EXPECT_TRUE(test_output);
}

TEST(test_pointwise_matrix_multiply,
     test_pointwise_matrix_multiply_reduce_average_channelwise) {
  constexpr uint32_t HEIGHT = 4, WIDTH = 3, CHANNELS = 5, OUTPUT_CHANNELS = 1;
  vosa::EigenImage<float> input(HEIGHT, WIDTH, CHANNELS);
  input.setValues({{{4.2f, 2.1f, 1.4f, 4.8f, 8.4f},
                    {4.7f, 7.6f, 6.0f, 0.3f, 3.1f},
                    {1.6f, 6.9f, 9.7f, 7.7f, 7.4f}},
                   {{5.8f, 8.3f, 3.4f, 4.7f, 7.3f},
                    {3.9f, 9.4f, 4.4f, 4.2f, 2.4f},
                    {4.8f, 8.2f, 2.4f, 4.1f, 1.5f}},
                   {{9.7f, 7.0f, 0.4f, 4.6f, 6.2f},
                    {2.3f, 3.1f, 1.6f, 6.6f, 6.7f},
                    {7.6f, 6.4f, 4.2f, 2.2f, 2.9f}},
                   {{2.5f, 5.9f, 9.3f, 3.2f, 2.3f},
                    {3.3f, 3.3f, 3.8f, 8.4f, 4.7f},
                    {7.2f, 2.2f, 2.2f, 2.3f, 3.2f}}});

  vosa::EigenImage<float> expected(input.dimension(0), input.dimension(1),
                                   OUTPUT_CHANNELS);
  for (uint32_t y = 0; y < input.dimension(0); y++)
    for (uint32_t x = 0; x < input.dimension(1); x++) {
      float sum = 0.0f;
      for (uint32_t c = 0; c < input.dimension(2); c++)
        sum += input(y, x, c);
      expected(y, x, 0) = sum / (float)input.dimension(2);
    }

  vosa::EigenVector<float> K_1(CHANNELS), K_2(OUTPUT_CHANNELS);
  K_1.setZero();
  K_2.setZero();

  vosa::EigenPlane<float> M(OUTPUT_CHANNELS, CHANNELS);
  M.setConstant(1.0f / (float)CHANNELS);
  vosa::EigenImage<float> actual =
      run_pointwise_matrix_multiply(input, M, K_1, K_2);

  bool result =
      vosa::test::TestBench<float, float>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}

TEST(test_pointwise_matrix_multiply,
     test_pointwise_matrix_multiply_broadcast_channelwise) {
  constexpr uint32_t HEIGHT = 5, WIDTH = 14, CHANNELS = 1, OUTPUT_CHANNELS = 11;
  vosa::EigenImage<float> input(HEIGHT, WIDTH, CHANNELS);
  input.setValues({{{4.2f},
                    {2.1f},
                    {1.4f},
                    {4.8f},
                    {8.4f},
                    {4.7f},
                    {7.6f},
                    {6.0f},
                    {0.3f},
                    {3.1f},
                    {1.6f},
                    {6.9f},
                    {9.7f},
                    {7.7f}},
                   {{5.8f},
                    {8.3f},
                    {3.4f},
                    {4.7f},
                    {7.3f},
                    {3.9f},
                    {9.4f},
                    {4.4f},
                    {4.2f},
                    {2.4f},
                    {4.8f},
                    {8.2f},
                    {2.4f},
                    {4.1f}},
                   {{4.2f},
                    {8.4f},
                    {0.3f},
                    {9.7f},
                    {8.3f},
                    {3.9f},
                    {2.4f},
                    {4.1f},
                    {0.4f},
                    {3.1f},
                    {7.6f},
                    {2.9f},
                    {3.2f},
                    {3.8f}},
                   {{9.7f},
                    {7.0f},
                    {0.4f},
                    {4.6f},
                    {6.2f},
                    {2.3f},
                    {3.1f},
                    {1.6f},
                    {6.6f},
                    {6.7f},
                    {7.6f},
                    {6.4f},
                    {4.2f},
                    {2.2f}},
                   {{2.5f},
                    {5.9f},
                    {9.3f},
                    {3.2f},
                    {2.3f},
                    {3.3f},
                    {3.3f},
                    {3.8f},
                    {8.4f},
                    {4.7f},
                    {7.2f},
                    {2.2f},
                    {2.2f},
                    {2.3f}}});

  vosa::EigenImage<float> expected(input.dimension(0), input.dimension(1),
                                   OUTPUT_CHANNELS);
  for (uint32_t y = 0; y < input.dimension(0); y++)
    for (uint32_t x = 0; x < input.dimension(1); x++)
      for (uint32_t c = 0; c < OUTPUT_CHANNELS; c++)
        expected(y, x, c) = input(y, x, 0);

  vosa::EigenVector<float> K_1(CHANNELS), K_2(OUTPUT_CHANNELS);
  K_1.setZero();
  K_2.setZero();

  vosa::EigenPlane<float> M(OUTPUT_CHANNELS, CHANNELS);
  M.setConstant(1.0f);
  vosa::EigenImage<float> actual =
      run_pointwise_matrix_multiply(input, M, K_1, K_2);

  bool result = vosa::test::TestBench<float, int32_t>::compareEigenImages(
      actual, expected);
  EXPECT_TRUE(result);
}
