/* -*- c++ -*-
 *
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_MLIR_FBS_BUILDER_H
#define VOSA_MLIR_FBS_BUILDER_H

#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/MLIRContext.h"

namespace vosa {

/// Construct a VOSA dialect module from a flatbuffer serialized pipeline
/// * `context`: The MLIR context
/// * `customOpsModule`: MLIR module containing defintions of custom
///   operations.  These will be copied into the generated module
/// * `module`: The module to contain the generated code
/// * `flatbufferData`: The serialized flatbuffer data
/// * `pipelineName`: The name for the generated function
/// * `inputs`: Vector of operator names that are the inputs to this pipeline
/// * `outputs`: Vector of operator names that are the outputs of this
///   pipeline.  The generated function will return these values in this order
int buildMLIRPipeline(mlir::MLIRContext &context,
                      mlir::OwningOpRef<mlir::ModuleOp> &customOpsModule,
                      mlir::OwningOpRef<mlir::ModuleOp> &module,
                      const std::string &inputFile,
                      const uint8_t *flatbufferData,
                      const std::string &pipelineName,
                      const std::vector<std::string> &inputs,
                      const std::vector<std::string> &outputs);
} // namespace vosa

#endif // VOSA_MLIR_FBS_BUILDER_H
