/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/api/graph_builder.h"
#include "vosa/Compiler/vosa_compiler.h"

std::string vosa::api::GraphBuilder::add_op(std::shared_ptr<vosa::api::Op> op) {
  std::string op_name = std::to_string(op_count);
  op_count++;
  ops[op_name] = std::move(op);
  return op_name;
}
std::string vosa::api::GraphBuilder::get_name_for_op(
    const std::shared_ptr<vosa::api::Op> &op) {
  for (const auto &kv : ops) {
    if (kv.second == op) {
      return kv.first;
    }
  }
  return "";
}

bool vosa::api::GraphBuilder::compile(const std::vector<std::string> &outputs,
                                      const std::string &output_filename) {
  mlir::OwningOpRef<mlir::ModuleOp> module = mlir_builder.finalize(outputs);
  return vosa::Compiler::compile(context, module, pipeline_name,
                                 output_filename);
}
