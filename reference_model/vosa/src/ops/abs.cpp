/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "abs.h"

std::shared_ptr<vosa::Op> vosa::AbsBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_abs = vosa_operator_union.AsABS();
  assert(serialized_abs);
  auto mode = serialized_abs->mode;
  switch (mode) {
  case vosa_serialization::ABS_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::Abs<int8_t>>();
  }
  case vosa_serialization::ABS_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::Abs<int16_t>>();
  }
  case vosa_serialization::ABS_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::Abs<int32_t>>();
  }
  case vosa_serialization::ABS_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::Abs<float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::EigenImage<T> vosa::Abs<T>::runme(const EigenImage<T> &input) const {

  const uint32_t height = input.dimension(0);
  const uint32_t width = input.dimension(1);
  const uint32_t channels = input.dimension(2);

  EigenImage<T> output(height, width, channels);

  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      for (uint32_t c = 0; c < channels; c++)
        output(y, x, c) = abs(input(y, x, c));

  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::Abs<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::Abs<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::ABS_MODE vosa::Abs<int8_t>::get_serialization_mode() const {
  return vosa_serialization::ABS_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::ABS_MODE
vosa::Abs<int16_t>::get_serialization_mode() const {
  return vosa_serialization::ABS_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::ABS_MODE
vosa::Abs<int32_t>::get_serialization_mode() const {
  return vosa_serialization::ABS_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::ABS_MODE vosa::Abs<float>::get_serialization_mode() const {
  return vosa_serialization::ABS_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Abs<T>::serialize(const std::string &op_name,
                        const std::vector<std::string> &input_names) const {

  vosa_serialization::ABST abs_serialization;
  abs_serialization.mode = get_serialization_mode();

  auto abs_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  abs_operator->op.Set(abs_serialization);
  abs_operator->name = op_name;
  abs_operator->inputs = input_names;
  return abs_operator;
}

template class vosa::Abs<int8_t>;

template class vosa::Abs<int16_t>;

template class vosa::Abs<int32_t>;

template class vosa::Abs<float>;