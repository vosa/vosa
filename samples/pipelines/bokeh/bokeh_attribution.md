* `input_y_channel.pgm`
* `input_v_channel.pgm`
* `input_u_channel.pgm`
* `input_depth_map.pgm`
* `input_binary_mask.pgm`
* `expected_output_y_channel.pgm`
* `expected_output_v_channel.pgm`
* `expected_output_u_channel.pgm`
are all adapted from this [original_image](https://photos.google.com/share/AF1QipPhjjwGoqPszXw6YwZSgqSfyz23d0KT-KuQrKkivhvKIkGeAn6jb8OjSB_dnGQD8g/photo/AF1QipMiBz2mYRsXtv_RDH2OS-yWhJfOdwRNK63Vl6sA?key=MFZnZUF6NnhlSXotNkxmdzNuX2hoS25vUTRnUzNR) part of the [HDR+ Burst Photography Dataset](https://hdrplusdata.org/dataset.html) licensed under [Creative Commons license (CC-BY-SA)](https://creativecommons.org/licenses/by-sa/4.0/)