"""Operations of binary type on Images

License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import enum
import typing

from vosa import types as types
from vosa.op import Op, ExecutionPattern, InputPattern, OPERATOR_MODES


class SimpleBinaryExecutionPattern(ExecutionPattern):
    def __init__(self, input_dtype, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    input_dtype,
                    types.AType.IMAGE,
                    description="First Input Image.",
                    shape=["H", "W", "C"],
                ),
                types.DataType(
                    input_dtype,
                    types.AType.IMAGE,
                    description="Second Input Image.",
                    shape=["H", "W", "C"],
                ),
            ),
            output_pattern=types.DataType(
                output_dtype,
                types.AType.IMAGE,
                description="Output Image.",
                shape=["H", "W", "C"],
            ),
        )


class ABS_DIFF_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_UINT8 = enum.auto()
    I_I_INT16_UINT16 = enum.auto()
    I_I_INT32_UINT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class ABS_DIFF(Op):
    """
    Computes the absolute difference between two Images,

    [latexmath]
    ++++
    output[y,x,c] = \\left| input_0[y,x,c] - input_1[y,x,c]\\right|.
    ++++

    In any integer based mode where the difference may exceed the supported range of the output type, the expected behaviour is that the correct value in arbitrary precision wraps to a value supported by the output type.
    """

    def to_flatbuffer(self):
        import vosa_serialization.ABS_DIFF
        import vosa_serialization.ABS_DIFF_MODE

        abs_diff_modes = {
            ABS_DIFF_MODES.I_I_UINT8_UINT8: vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_UINT8_UINT8,
            ABS_DIFF_MODES.I_I_UINT16_UINT16: vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_UINT16_UINT16,
            ABS_DIFF_MODES.I_I_UINT32_UINT32: vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_UINT32_UINT32,
            ABS_DIFF_MODES.I_I_INT8_UINT8: vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_INT8_UINT8,
            ABS_DIFF_MODES.I_I_INT16_UINT16: vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_INT16_UINT16,
            ABS_DIFF_MODES.I_I_INT32_UINT32: vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_INT32_UINT32,
            ABS_DIFF_MODES.I_I_FLOAT16_FLOAT16: vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_FLOAT16_FLOAT16,
            ABS_DIFF_MODES.I_I_FLOAT32_FLOAT32: vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_FLOAT32_FLOAT32,
        }

        abs_diff_t = vosa_serialization.ABS_DIFF.ABS_DIFFT()
        abs_diff_t.mode = abs_diff_modes[self.get_operator_mode()]

        return abs_diff_t

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[ABS_DIFF_MODES, SimpleBinaryExecutionPattern]:
        return {
            ABS_DIFF_MODES.I_I_UINT8_UINT8: SimpleBinaryExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            ABS_DIFF_MODES.I_I_UINT16_UINT16: SimpleBinaryExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            ABS_DIFF_MODES.I_I_UINT32_UINT32: SimpleBinaryExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
            ABS_DIFF_MODES.I_I_INT8_UINT8: SimpleBinaryExecutionPattern(
                types.DType.INT8, types.DType.UINT8
            ),
            ABS_DIFF_MODES.I_I_INT16_UINT16: SimpleBinaryExecutionPattern(
                types.DType.INT16, types.DType.UINT16
            ),
            ABS_DIFF_MODES.I_I_INT32_UINT32: SimpleBinaryExecutionPattern(
                types.DType.INT32, types.DType.UINT32
            ),
            ABS_DIFF_MODES.I_I_FLOAT16_FLOAT16: SimpleBinaryExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            ABS_DIFF_MODES.I_I_FLOAT32_FLOAT32: SimpleBinaryExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class ATAN2_MODES(OPERATOR_MODES):
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class ATAN2(Op):
    """
    Calculate atan2, pixelwise, between two Images,
    [latexmath]
    ++++
    output[y,x,c] = \\textrm{atan2}(input_0[y,x,c], input_1[y,x,c]),
    ++++
    where,
    [latexmath]
    ++++
    \\textrm{atan2}(y,x) = \\left\{ \\begin{array}{ll}
    \\textrm{arctan}\\left(\\frac{y}{x}\\right) & x > 0 \\\\
    \\textrm{arctan}\\left(\\frac{y}{x}\\right) + \\pi & x < 0  \\land y  \\geq 0 \\\\
    \\textrm{arctan}\\left(\\frac{y}{x}\\right) - \\pi & x < 0  \\land y < 0 \\\\
    \\frac{\\pi}{2}  & x = 0 \\land y > 0 \\\\
    -\\frac{\\pi}{2} & x = 0 \\land y < 0 \\\\
    0 & x = 0 \\land y = 0
    \\end{array}\\right.
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[ATAN2_MODES, ExecutionPattern]:
        return {
            ATAN2_MODES.I_I_FLOAT32_FLOAT32: SimpleBinaryExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
            ATAN2_MODES.I_I_FLOAT16_FLOAT16: SimpleBinaryExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
        }


class ADD_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class ADD(Op):
    """
    Compute elementwise addition of two images,

    [latexmath]
    ++++
    output[y, x, c] = input_0[y, x, c] + input_1[y, x, c].
    ++++
    In any integer based mode where the addition may exceed the supported range of the output type, the expected behaviour is that the correct value in arbitrary precision wraps to a value supported by the output type.
    """

    def to_flatbuffer(self):
        import vosa_serialization.ADD
        import vosa_serialization.ADD_MODE

        add_modes = {
            ADD_MODES.I_I_UINT8_UINT8: vosa_serialization.ADD_MODE.ADD_MODE.I_I_UINT8_UINT8,
            ADD_MODES.I_I_UINT16_UINT16: vosa_serialization.ADD_MODE.ADD_MODE.I_I_UINT16_UINT16,
            ADD_MODES.I_I_UINT32_UINT32: vosa_serialization.ADD_MODE.ADD_MODE.I_I_UINT32_UINT32,
            ADD_MODES.I_I_INT8_INT8: vosa_serialization.ADD_MODE.ADD_MODE.I_I_INT8_INT8,
            ADD_MODES.I_I_INT16_INT16: vosa_serialization.ADD_MODE.ADD_MODE.I_I_INT16_INT16,
            ADD_MODES.I_I_INT32_INT32: vosa_serialization.ADD_MODE.ADD_MODE.I_I_INT32_INT32,
            ADD_MODES.I_I_FLOAT16_FLOAT16: vosa_serialization.ADD_MODE.ADD_MODE.I_I_FLOAT16_FLOAT16,
            ADD_MODES.I_I_FLOAT32_FLOAT32: vosa_serialization.ADD_MODE.ADD_MODE.I_I_FLOAT32_FLOAT32,
        }

        addt = vosa_serialization.ADD.ADDT()
        addt.mode = add_modes[self.get_operator_mode()]

        return addt

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[ADD_MODES, ExecutionPattern]:
        return {
            ADD_MODES.I_I_UINT8_UINT8: SimpleBinaryExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            ADD_MODES.I_I_UINT16_UINT16: SimpleBinaryExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            ADD_MODES.I_I_UINT32_UINT32: SimpleBinaryExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
            ADD_MODES.I_I_INT8_INT8: SimpleBinaryExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            ADD_MODES.I_I_INT16_INT16: SimpleBinaryExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            ADD_MODES.I_I_INT32_INT32: SimpleBinaryExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            ADD_MODES.I_I_FLOAT16_FLOAT16: SimpleBinaryExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            ADD_MODES.I_I_FLOAT32_FLOAT32: SimpleBinaryExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class AND_MODES(OPERATOR_MODES):
    I_I_BOOL_BOOL = enum.auto()
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()


class AND(Op):
    """
    Perform a _bitwise_ AND operation on two Images,
    [latexmath]
    ++++
    output[y, x, c] = input_0[y, x, c] \land input_1[y, x, c].
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[AND_MODES, ExecutionPattern]:
        return {
            AND_MODES.I_I_BOOL_BOOL: SimpleBinaryExecutionPattern(
                types.DType.BOOL, types.DType.BOOL
            ),
            AND_MODES.I_I_UINT8_UINT8: SimpleBinaryExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            AND_MODES.I_I_UINT16_UINT16: SimpleBinaryExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            AND_MODES.I_I_UINT32_UINT32: SimpleBinaryExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
        }


class DIV_MODES(OPERATOR_MODES):
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class DIV(Op):
    """
    Compute elementwise division of two images,

    [latexmath]
    ++++
    output[y, x, c] = \\frac{input_0[y, x], c}{ input_1[y, x, c]}.
    ++++
    """

    def to_flatbuffer(self):
        import vosa_serialization.DIV
        import vosa_serialization.DIV_MODE

        div_modes = {
            DIV_MODES.I_I_FLOAT16_FLOAT16: vosa_serialization.DIV_MODE.DIV_MODE.I_I_FLOAT16_FLOAT16,
            DIV_MODES.I_I_FLOAT32_FLOAT32: vosa_serialization.DIV_MODE.DIV_MODE.I_I_FLOAT32_FLOAT32,
        }

        div_t = vosa_serialization.DIV.DIVT()
        div_t.mode = div_modes[self.get_operator_mode()]

        return div_t

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[DIV_MODES, ExecutionPattern]:
        return {
            DIV_MODES.I_I_FLOAT16_FLOAT16: SimpleBinaryExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            DIV_MODES.I_I_FLOAT32_FLOAT32: SimpleBinaryExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class MAX_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class MAX(Op):
    """
    Calculate the pixelwise max between two Images,
    [latexmath]
    ++++
    output[y,x,c] = \\mathrm{max}(input_0[y,x,c], input_1[y,x,c]).
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[MAX_MODES, ExecutionPattern]:
        return {
            MAX_MODES.I_I_UINT8_UINT8: SimpleBinaryExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            MAX_MODES.I_I_UINT16_UINT16: SimpleBinaryExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            MAX_MODES.I_I_UINT32_UINT32: SimpleBinaryExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
            MAX_MODES.I_I_INT8_INT8: SimpleBinaryExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            MAX_MODES.I_I_INT16_INT16: SimpleBinaryExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            MAX_MODES.I_I_INT32_INT32: SimpleBinaryExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            MAX_MODES.I_I_FLOAT16_FLOAT16: SimpleBinaryExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            MAX_MODES.I_I_FLOAT32_FLOAT32: SimpleBinaryExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class MIN_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class MIN(Op):
    """
    Calculate the pixelwise min between two Images,
    [latexmath]
    ++++
    output[y,x,c] = \\mathrm{min}(input_0[y,x,c], input_1[y,x,c]).
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[MIN_MODES, ExecutionPattern]:
        return {
            MIN_MODES.I_I_UINT8_UINT8: SimpleBinaryExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            MIN_MODES.I_I_UINT16_UINT16: SimpleBinaryExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            MIN_MODES.I_I_UINT32_UINT32: SimpleBinaryExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
            MIN_MODES.I_I_INT8_INT8: SimpleBinaryExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            MIN_MODES.I_I_INT16_INT16: SimpleBinaryExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            MIN_MODES.I_I_INT32_INT32: SimpleBinaryExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            MIN_MODES.I_I_FLOAT16_FLOAT16: SimpleBinaryExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            MIN_MODES.I_I_FLOAT32_FLOAT32: SimpleBinaryExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class MOD_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class MOD(Op):
    """
    Calculate the element-wise mod between two Images,
    [latexmath]
    ++++
    output[y,x,c] = input_0[y,x,c] \:\%\: input_1[y,x,c].
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[MOD_MODES, ExecutionPattern]:
        return {
            MOD_MODES.I_I_UINT8_UINT8: SimpleBinaryExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            MOD_MODES.I_I_UINT16_UINT16: SimpleBinaryExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            MOD_MODES.I_I_UINT32_UINT32: SimpleBinaryExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
            MOD_MODES.I_I_INT8_INT8: SimpleBinaryExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            MOD_MODES.I_I_INT16_INT16: SimpleBinaryExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            MOD_MODES.I_I_INT32_INT32: SimpleBinaryExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            MOD_MODES.I_I_FLOAT16_FLOAT16: SimpleBinaryExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            MOD_MODES.I_I_FLOAT32_FLOAT32: SimpleBinaryExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class MULT_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class MULT(Op):
    """
    Calculate the elementwise multiplication of two Images,
    [latexmath]
    ++++
    output[y,x,c] = input_0[y,x,c] * input_1[y,x,c].
    ++++
    In any integer based mode where the multiplication may exceed the supported range of the output type, the expected behaviour is that the correct value in arbitrary precision wraps to a value supported by the output type.
    """

    def to_flatbuffer(self):
        import vosa_serialization.MULT
        import vosa_serialization.MULT_MODE

        mult_modes = {
            MULT_MODES.I_I_UINT8_UINT8: vosa_serialization.MULT_MODE.MULT_MODE.I_I_UINT8_UINT8,
            MULT_MODES.I_I_UINT16_UINT16: vosa_serialization.MULT_MODE.MULT_MODE.I_I_UINT16_UINT16,
            MULT_MODES.I_I_UINT32_UINT32: vosa_serialization.MULT_MODE.MULT_MODE.I_I_UINT32_UINT32,
            MULT_MODES.I_I_INT8_INT8: vosa_serialization.MULT_MODE.MULT_MODE.I_I_INT8_INT8,
            MULT_MODES.I_I_INT16_INT16: vosa_serialization.MULT_MODE.MULT_MODE.I_I_INT16_INT16,
            MULT_MODES.I_I_INT32_INT32: vosa_serialization.MULT_MODE.MULT_MODE.I_I_INT32_INT32,
            MULT_MODES.I_I_FLOAT16_FLOAT16: vosa_serialization.MULT_MODE.MULT_MODE.I_I_FLOAT16_FLOAT16,
            MULT_MODES.I_I_FLOAT32_FLOAT32: vosa_serialization.MULT_MODE.MULT_MODE.I_I_FLOAT32_FLOAT32,
        }

        mult_t = vosa_serialization.MULT.MULTT()
        mult_t.mode = mult_modes[self.get_operator_mode()]

        return mult_t

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[MULT_MODES, ExecutionPattern]:
        return {
            MULT_MODES.I_I_UINT8_UINT8: SimpleBinaryExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            MULT_MODES.I_I_UINT16_UINT16: SimpleBinaryExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            MULT_MODES.I_I_UINT32_UINT32: SimpleBinaryExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
            MULT_MODES.I_I_INT8_INT8: SimpleBinaryExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            MULT_MODES.I_I_INT16_INT16: SimpleBinaryExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            MULT_MODES.I_I_INT32_INT32: SimpleBinaryExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            MULT_MODES.I_I_FLOAT16_FLOAT16: SimpleBinaryExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            MULT_MODES.I_I_FLOAT32_FLOAT32: SimpleBinaryExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


# class MULT_SCALAR_MODES(OPERATOR_MODES):
#     S_S_INT16_INT16 = enum.auto()
#     S_S_UINT32_UINT32 = enum.auto()
#
#
# class MULT_SCALAR(Op):
#     """
#     Some text
#     """
#
#     @classmethod
#     def get_pattern_dict(cls) -> typing.Dict[MULT_SCALAR_MODES, ExecutionPattern]:
#         return {
#             MULT_SCALAR_MODES.S_S_INT16_INT16: ExecutionPattern(
#                 input_pattern=InputPattern(types.DataType(types.DType.INT16, types.AType.SCALAR),
#                                            types.DataType(types.DType.INT16, types.AType.SCALAR)),
#                 output_pattern=types.DataType(types.DType.INT16, types.AType.SCALAR)
#             ),
#             MULT_SCALAR_MODES.S_S_UINT32_UINT32: ExecutionPattern(
#                 input_pattern=InputPattern(types.DataType(types.DType.UINT32, types.AType.SCALAR),
#                                            types.DataType(types.DType.UINT32, types.AType.SCALAR)),
#                 output_pattern=types.DataType(types.DType.UINT32, types.AType.SCALAR)
#             )
#         }


class OR_MODES(OPERATOR_MODES):
    I_I_BOOL_BOOL = enum.auto()
    I_I_UINT8_UINT8 = enum.auto()


class OR(Op):
    """
    Perform a _bitwise_ OR operation on two Images,
    [latexmath]
    ++++
    output[y,x,c] = input_0[y,x,c] \lor input_1[y,x,c].
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[OR_MODES, ExecutionPattern]:
        return {
            OR_MODES.I_I_BOOL_BOOL: SimpleBinaryExecutionPattern(
                types.DType.BOOL, types.DType.BOOL
            ),
            OR_MODES.I_I_UINT8_UINT8: SimpleBinaryExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
        }


class SUB_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class SUB(Op):
    """
    Compute elementwise subtraction of two Images,

    [latexmath]
    ++++
    output[y,x,c] = input_0[y,x,c] - input_1[y,x,c].
    ++++
    In any integer based mode where the subtraction may exceed the supported range of the output type, the expected behaviour is that the correct value in arbitrary precision wraps to a value supported by the output type.
    """

    def to_flatbuffer(self):
        import vosa_serialization.SUB
        import vosa_serialization.SUB_MODE

        sub_modes = {
            SUB_MODES.I_I_UINT8_UINT8: vosa_serialization.SUB_MODE.SUB_MODE.I_I_UINT8_UINT8,
            SUB_MODES.I_I_UINT16_UINT16: vosa_serialization.SUB_MODE.SUB_MODE.I_I_UINT16_UINT16,
            SUB_MODES.I_I_UINT32_UINT32: vosa_serialization.SUB_MODE.SUB_MODE.I_I_UINT32_UINT32,
            SUB_MODES.I_I_INT8_INT8: vosa_serialization.SUB_MODE.SUB_MODE.I_I_INT8_INT8,
            SUB_MODES.I_I_INT16_INT16: vosa_serialization.SUB_MODE.SUB_MODE.I_I_INT16_INT16,
            SUB_MODES.I_I_INT32_INT32: vosa_serialization.SUB_MODE.SUB_MODE.I_I_INT32_INT32,
            SUB_MODES.I_I_FLOAT16_FLOAT16: vosa_serialization.SUB_MODE.SUB_MODE.I_I_FLOAT16_FLOAT16,
            SUB_MODES.I_I_FLOAT32_FLOAT32: vosa_serialization.SUB_MODE.SUB_MODE.I_I_FLOAT32_FLOAT32,
        }

        sub_t = vosa_serialization.SUB.SUBT()
        sub_t.mode = sub_modes[self.get_operator_mode()]

        return sub_t

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[SUB_MODES, ExecutionPattern]:
        return {
            SUB_MODES.I_I_UINT8_UINT8: SimpleBinaryExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            SUB_MODES.I_I_UINT16_UINT16: SimpleBinaryExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            SUB_MODES.I_I_UINT32_UINT32: SimpleBinaryExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
            SUB_MODES.I_I_INT8_INT8: SimpleBinaryExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            SUB_MODES.I_I_INT16_INT16: SimpleBinaryExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            SUB_MODES.I_I_INT32_INT32: SimpleBinaryExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            SUB_MODES.I_I_FLOAT16_FLOAT16: SimpleBinaryExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            SUB_MODES.I_I_FLOAT32_FLOAT32: SimpleBinaryExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class XOR_MODES(OPERATOR_MODES):
    I_I_BOOL_BOOL = enum.auto()
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()


class XOR(Op):
    """
    Perform a _bitwise_ XOR operation on two images,

    [latexmath]
    ++++
    output[y,x,c] = input_0[y,x,c] \oplus input_1[y,x,c].
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[XOR_MODES, ExecutionPattern]:
        return {
            XOR_MODES.I_I_BOOL_BOOL: SimpleBinaryExecutionPattern(
                types.DType.BOOL, types.DType.BOOL
            ),
            XOR_MODES.I_I_UINT8_UINT8: SimpleBinaryExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            XOR_MODES.I_I_UINT16_UINT16: SimpleBinaryExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            XOR_MODES.I_I_UINT32_UINT32: SimpleBinaryExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
        }
