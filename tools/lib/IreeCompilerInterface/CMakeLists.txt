add_library(IREECompilerInterface
        STATIC
        iree_compiler_interface.cpp)

target_link_libraries(IREECompilerInterface
        PUBLIC
        IREELLVMIncludeSetup
        iree::compiler::bindings::c::headers
        iree::compiler::API::Impl)

install(TARGETS IREECompilerInterface)