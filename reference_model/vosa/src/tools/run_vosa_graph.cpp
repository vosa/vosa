/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <fstream>
#include <getopt.h>
#include <iostream>
#include <string>
#include <vector>

#include "array_serialization.h"
#include "vosa.h"
#include "vosa_test_artifacts_generated.h"

std::shared_ptr<char> load_flatbuffer(const std::string &flatbuffer_filename) {
  std::ifstream filestream(flatbuffer_filename, std::ios::binary);
  if (!filestream.is_open()) {
    throw std::runtime_error("Error opening " + flatbuffer_filename +
                             " for reading");
  }
  filestream.seekg(0, std::ios::end);
  std::streamoff length = filestream.tellg();
  filestream.seekg(0, std::ios::beg);
  std::shared_ptr<char> data(new char[length]);
  filestream.read(data.get(), length);
  filestream.close();
  return data;
}

int main(int argc, char *argv[]) {
  int c;

  std::vector<std::string> desired_outputs;
  std::vector<std::string> custom_operator_objects;
  std::string stimulus_flatbuffer, graph_flatbuffer, output_flatbuffer;
  while ((c = getopt(argc, argv, "s:g:o:c:d:")) != -1) {
    switch (c) {
    case 'd': {
      desired_outputs.emplace_back(optarg);
      break;
    }
    case 'g': {
      graph_flatbuffer = std::string(optarg);
      break;
    }
    case 's': {
      stimulus_flatbuffer = std::string(optarg);
      break;
    }
    case 'c': {
      custom_operator_objects.emplace_back(optarg);
      break;
    }
    case 'o': {
      output_flatbuffer = std::string(optarg);
      break;
    }
    default: {
      throw std::runtime_error("Unrecognized command line option.");
    }
    }
  }
  std::cout << "Getting output for: " << std::endl;
  for (const auto &output : desired_outputs) {
    std::cout << output << std::endl;
  }

  for (const auto &custom_operator_object : custom_operator_objects) {
    vosa::load_custom_operators(custom_operator_object);
  }

  auto graph_data = load_flatbuffer(graph_flatbuffer);
  auto graph = vosa::Graph((uint8_t *)graph_data.get());

  auto stimulus_data = load_flatbuffer(stimulus_flatbuffer);
  vosa_test_serialization::TestArtifactsT stimulus;
  auto stimulus_buffer =
      flatbuffers::GetRoot<vosa_test_serialization::TestArtifacts>(
          stimulus_data.get());
  stimulus_buffer->UnPackTo(&stimulus);

  std::unordered_map<std::string, vosa::ArrayBase> graph_inputs;
  for (const auto &input : stimulus.inputs) {
    std::string input_name = vosa::test::deserialize_name(*input);
    graph_inputs.emplace(input_name, vosa::test::deserialize(*input));
  }

  auto graph_output = graph.execute(graph_inputs, desired_outputs);

  vosa_test_serialization::TestArtifactsT output;
  for (const auto &graph_input : graph_inputs) {
    vosa::test::serialize_array(output, graph_input.first, graph_input.second,
                                true);
  }
  for (uint32_t output_index = 0; output_index < graph_output.size();
       output_index++) {
    vosa::test::serialize_array(output, desired_outputs[output_index],
                                *graph_output[output_index], false);
  }
  vosa::test::write_artifacts_flatbuffer(output, output_flatbuffer);

  return 0;
}
