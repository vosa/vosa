"""
License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import abc
import collections
import typing
import enum
import inspect
import struct

import vosa.graph as graph
from vosa import types as types


class InputValidationError(RuntimeError):
    pass


class InputPattern:
    def __init__(self, *input_types: types.DataType):
        self._input_types = input_types

    def get_input_types(self):
        return self._input_types

    def __eq__(self, other):
        if not isinstance(other, InputPattern):
            return False
        if len(self._input_types) != len(other.get_input_types()):
            return False
        for input_index in range(len(self._input_types)):
            if (
                self._input_types[input_index].dtype()
                != other.get_input_types()[input_index].dtype()
            ):
                return False
            this_is_input_node = isinstance(self._input_types[input_index], INPUT)
            other_is_input_node = isinstance(
                other.get_input_types()[input_index], INPUT
            )
            if (not this_is_input_node) and (not other_is_input_node):
                if (
                    self._input_types[input_index].ttype()
                    != other.get_input_types()[input_index].ttype()
                ):
                    return False
        return True

    def adoc(self) -> str:
        return " | ".join([input_type.adoc() for input_type in self._input_types])

    def __str__(self):
        return self.adoc()

    def __len__(self):
        return len(self._input_types)

    def __getitem__(self, item):
        return self._input_types[item]


class InputArrayPattern(InputPattern):
    def __init__(self, *input_types: types.DataType):
        assert len(input_types) == 1
        super().__init__(*input_types)

    def __eq__(self, other):
        if not isinstance(other, InputPattern):
            return False
        for other_input in other.get_input_types():
            if self._input_types[0].dtype() != other_input.dtype():
                return False
        return True


class ExecutionPattern:
    def __init__(
        self,
        input_pattern: InputPattern,
        output_pattern: typing.Optional[types.DataType],
        dynamic_attribute_pattern: typing.Optional[
            typing.Dict[str, types.DataType]
        ] = None,
        input_output_pattern: typing.Optional[InputPattern] = None,
    ):
        self._input_pattern = input_pattern
        self._output_pattern = output_pattern
        self._dynamic_attribute_pattern = (
            dynamic_attribute_pattern if dynamic_attribute_pattern else dict()
        )
        self._input_output_pattern = (
            input_output_pattern if input_output_pattern else InputPattern()
        )

    def input_pattern(self):
        return self._input_pattern

    def output_pattern(self):
        return self._output_pattern

    def dynamic_attribute_pattern(self):
        return self._dynamic_attribute_pattern

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {}

    def input_output_pattern(self):
        return self._input_output_pattern

    def adoc(self) -> str:
        return "%s | %s" % (self._input_pattern.adoc(), self._output_pattern.adoc())

    def get_type_string_dict(self):
        types_list: typing.List[types.DataType] = [type for type in self._input_pattern]
        types_list.append(self._output_pattern)
        types_list.extend(self._dynamic_attribute_pattern.values())
        dtype_list = [type.dtype() for type in types_list]
        unique_dtype_list = list(set(dtype_list))
        dtype_dict = {
            dtype: "dtype_%i" % dtype_index
            for dtype_index, dtype in enumerate(unique_dtype_list)
        }
        return dtype_dict


class OPERATOR_MODES(enum.Enum):
    pass


class JoinedExecutionPattern:
    @classmethod
    def make_pattern_dicts(cls, execution_pattern: ExecutionPattern):
        pattern_dtypes = {}
        pattern_atypes = {}
        arguments_rows = []

        execution_pattern_inputs = execution_pattern.input_pattern()
        execution_pattern_input_outputs = execution_pattern.input_output_pattern()

        input_array_pattern = isinstance(execution_pattern_inputs, InputArrayPattern)
        input_index = 0
        all_input_patterns = [execution_pattern_inputs, execution_pattern_input_outputs]
        for pattern_index, pattern in enumerate(all_input_patterns):
            for input in pattern:
                if sum([len(p) for p in all_input_patterns]) == 1:
                    input_str = "input"
                    dtype_name = "input_t"
                else:
                    input_str = "input_%i" % input_index
                    dtype_name = "input_%i_t" % input_index

                atype_name_arg_table = (
                    input.ttype().name + "*"
                    if input_array_pattern
                    else input.ttype().name
                )
                shape_name_arg_table = (
                    "[A]" + input.shape() if input_array_pattern else input.shape()
                )

                pattern_dtypes[dtype_name] = input.dtype()
                pattern_atypes[dtype_name] = input.ttype()

                io_string = "Input" if pattern_index == 0 else "Input/Output"

                input_row = "| %s | %s | %s | %s | %s | %s" % (
                    io_string,
                    input_str,
                    atype_name_arg_table,
                    dtype_name,
                    shape_name_arg_table,
                    input.description(),
                )
                arguments_rows.append(input_row)

                input_index += 1

        num_dynamic_atts = len(execution_pattern.dynamic_attribute_pattern())
        num_static_atts = len(execution_pattern.static_attribute_pattern())
        num_atts = num_dynamic_atts + num_static_atts

        att_index = 0
        for (
            attribute_name,
            attribute,
        ) in execution_pattern.dynamic_attribute_pattern().items():
            if num_atts == 1:
                dtype_name = "att_t"
            else:
                dtype_name = "att_%i_t" % att_index
                att_index += 1
            pattern_dtypes[dtype_name] = attribute.dtype()
            pattern_atypes[dtype_name] = attribute.ttype()

            dynamic_attribute_row = "| Attribute | %s | %s | %s | %s | %s" % (
                attribute_name,
                attribute.ttype().name,
                dtype_name,
                attribute.shape(),
                attribute.description(),
            )
            arguments_rows.append(dynamic_attribute_row)

        for (
            attribute_name,
            attribute,
        ) in execution_pattern.static_attribute_pattern().items():
            if not isinstance(attribute, types.DataType):
                attribute_atype_name = attribute.__name__
                attribute_dtype_name = ""
                attribute_shape = ""
                attribute_description = ""
            else:
                try:
                    attribute_atype_name = attribute.ttype().__name__
                except AttributeError:
                    try:
                        attribute_atype_name = attribute.ttype().name
                    except AttributeError:
                        attribute_atype_name = ""
                try:
                    attribute_dtype_name = attribute.dtype().name
                except AttributeError:
                    attribute_dtype_name = ""
                attribute_shape = attribute.shape()
                attribute_description = attribute.description()
            static_attribute_row = "| Attribute | %s | %s | %s | %s | %s" % (
                attribute_name,
                attribute_atype_name,
                attribute_dtype_name,
                attribute_shape,
                attribute_description,
            )
            # static_attribute_row = "| Attribute | %s | %s | %s | %s | %s" % (
            #     "", "", "", "", "")
            arguments_rows.append(static_attribute_row)

        output_pattern = execution_pattern.output_pattern()
        dtype_name = "output_t"
        try:
            pattern_dtypes[dtype_name] = output_pattern.dtype()
            pattern_atypes[dtype_name] = output_pattern.ttype()
        except AttributeError:
            pass

        try:
            output_row = "| Output | output | %s | %s | %s | %s " % (
                output_pattern.ttype().name,
                dtype_name,
                output_pattern.shape(),
                output_pattern.description(),
            )
            arguments_rows.append(output_row)
        except AttributeError:
            pass

        return pattern_dtypes, pattern_atypes, arguments_rows

    def __init__(self, pattern_dict: typing.Dict[OPERATOR_MODES, ExecutionPattern]):
        self._inputs = {}

        self._all_pattern_dtypes = {}
        self._arguments_rows = []
        first_pattern_atypes = None
        for execution_pattern_name, execution_pattern in pattern_dict.items():
            pattern_dtypes, pattern_atypes, arguments_rows = self.make_pattern_dicts(
                execution_pattern
            )

            if first_pattern_atypes is None:
                first_pattern_atypes = pattern_atypes
                self._arguments_rows = arguments_rows
            else:
                assert (
                    first_pattern_atypes == pattern_atypes
                ), "Non Matching execution pattern."

            self._all_pattern_dtypes[execution_pattern_name] = pattern_dtypes

    def make_modes_table(self):
        modes_table = []
        modes_table.append("*Supported Modes:*")
        modes_table.append("|===")
        first_pattern_dtype = self._all_pattern_dtypes[
            list(self._all_pattern_dtypes.keys())[0]
        ]
        dtype_headers = list(first_pattern_dtype.keys())

        title_row = "| Mode | " + "| ".join(dtype_headers)
        modes_table.append(title_row)

        for pattern_name, pattern_dtypes in self._all_pattern_dtypes.items():
            row_line = "| %s " % (pattern_name.name)
            for header in dtype_headers:
                try:
                    row_line += "| %s" % pattern_dtypes[header].name
                except AttributeError:
                    row_line += "| N/A"
            modes_table.append(row_line)
        modes_table.append("|===")
        return modes_table

    def make_arguments_table(self):
        arguments_table = []
        arguments_table.append("*Arguments:*")
        arguments_table.append("[%autowidth.stretch]")
        arguments_table.append("|===")
        arguments_title_row = (
            "| Argument | Name | AType | DType | Shape | Description \n"
        )
        arguments_table.append(arguments_title_row)
        arguments_table.extend(self._arguments_rows)
        arguments_table.append("|===")
        return arguments_table


class Op(abc.ABC):
    """
    Standard Op Text
    """

    def __init__(self, operands, operator_mode, attributes):
        self._operands = []
        for operand in operands:
            self._operands.append(operand)
        self._return_data = None

        self._operator_mode = operator_mode
        self._pattern_dict = self.get_pattern_dict()
        self._execution_pattern = self._pattern_dict[operator_mode]
        self._attributes = attributes

    @classmethod
    def __count_inputs(cls, pattern_dict):
        num_inputs = None
        for pattern_name, pattern in pattern_dict.items():
            num_pattern_inputs = len(pattern.input_pattern())
            if not num_inputs:
                num_inputs = num_pattern_inputs
            else:
                assert num_inputs == num_pattern_inputs
        return num_inputs

    @classmethod
    def __make__modes_table(cls, pattern_dict):
        joined_execution_pattern = JoinedExecutionPattern(pattern_dict)
        return joined_execution_pattern.make_modes_table()

    @classmethod
    def __make_arguments_table(cls, pattern_dict):
        joined_execution_pattern = JoinedExecutionPattern(pattern_dict)
        return joined_execution_pattern.make_arguments_table()

    @classmethod
    def include_in_specification(cls):
        return True

    @classmethod
    def adoc(cls):
        adoc_output = []
        adoc_output.append("==== %s\n" % cls.__name__)
        adoc_output.append("%s\n" % inspect.getdoc(cls))
        pattern_dict = cls.get_pattern_dict()
        arguments_table = cls.__make_arguments_table(pattern_dict)
        adoc_output.extend(arguments_table)
        modes_table = cls.__make__modes_table(pattern_dict)
        adoc_output.extend(modes_table)
        return adoc_output

    def get_operands(self):
        return self._operands

    @classmethod
    @abc.abstractmethod
    def get_pattern_dict(cls) -> typing.Dict[OPERATOR_MODES, ExecutionPattern]:
        pass

    def get_return_data(self):
        assert self._return_data is not None
        return self._return_data

    def get_operator_mode(self):
        return self._operator_mode

    @classmethod
    def create(
        cls,
        *operands: "Op",
        operator_mode,
        name,
        attributes: typing.Optional[typing.Dict[str, types.DataType]] = None
    ) -> "Op":
        if not attributes:
            attributes = dict()
        op = cls(operands, operator_mode, attributes)
        return_datatype = op.wrapped_fake_run(*operands, attributes=attributes)
        assert isinstance(return_datatype, types.DataType)

        g = graph._graph_stack.get_current()
        g.add_op(op, name)

        return op

    def get_attributes(self):
        return self._attributes

    def to_flatbuffer(self):
        raise NotImplementedError()

    # FIXME -- How do we detect various INT's and FLOAT's ? -- NumPy ?
    @staticmethod
    def _pack_value(value):
        if isinstance(value, int):
            return struct.pack("i", value)
        elif isinstance(value, float):
            return struct.pack("f", value)
        else:
            raise ValueError()

    # NOTE: This is only a wrapper to provide API consistency
    @staticmethod
    def _unpack_value(dtype: str, byte_array):
        return struct.unpack(dtype, byte_array)

    def wrapped_fake_run(self, *operands: "Op", attributes=None) -> types.DataType:
        g = graph._graph_stack.get_current()
        allow_failure = g.allow_failed_validation()

        attribute_success = True
        if attributes is None:
            attributes = dict()
        static_attributes = self._execution_pattern.static_attribute_pattern()
        dynamic_attributes = self._execution_pattern.dynamic_attribute_pattern()
        all_attributes = {**static_attributes, **dynamic_attributes}
        for attribute_name, attribute_val in attributes.items():
            try:
                reference_attribute = all_attributes[attribute_name]
            except KeyError:
                attribute_success = False
                break
            if not attribute_val == reference_attribute:
                attribute_success = False

        input_success = True
        expected_execution_input_pattern = self._execution_pattern.input_pattern()
        received_execution_input_pattern = InputPattern(*operands)
        if expected_execution_input_pattern == received_execution_input_pattern:
            self._return_data = self._execution_pattern.output_pattern()
        else:
            self._return_data = types.DataType(
                types.DType.UNDEFINED, types.AType.UNDEFINED
            )
            input_success = False

        if (input_success and attribute_success) or allow_failure:
            return self._return_data
        else:
            raise InputValidationError()

    def dtype(self):
        return self._return_data.dtype()

    def ttype(self):
        return self._return_data.ttype()

    def make_label(self, name):
        class_name = self.__class__.__name__

        color_dict = {"TOSA": "deeppink"}

        try:
            op_color = color_dict[class_name]
        except KeyError:
            op_color = "darkgoldenrod1"

        op_row = """<tr><td>Type</td><td bgcolor="%s">%s</td></tr>""" % (
            op_color,
            class_name,
        )
        mode_row = (
            """<tr><td>Mode</td><td>"""
            + str(self._operator_mode.name)
            + """ </td></tr>"""
        )
        name_row = """<tr><td>Name</td><td>""" + name + """</td></tr>"""

        ttype_colours = {
            types.AType.IMAGE: "forestgreen",
            types.AType.SCALAR: "dodgerblue",
            types.AType.PLANE: "slateblue",
            types.AType.VECTOR: "plum",
            types.AType.ARRAY: "greenyellow",
        }

        try:
            output_ttype_colour = ttype_colours[self._return_data.ttype()]
        except KeyError:
            output_ttype_colour = "firebrick1"

        output_ttype_row = (
            '''<tr><td>AType</td><td bgcolor="'''
            + output_ttype_colour
            + """">"""
            + str(self._return_data.ttype().name)
            + """</td></tr>"""
        )

        output_dtype_color = (
            "firebrick1"
            if self._return_data.dtype() == types.DType.UNDEFINED
            else "chartreuse"
        )
        try:
            dtype_name = self._return_data.dtype().name
        except AttributeError:
            dtype_name = "N/A"
        output_dtype_row = (
            '''<tr><td>DType</td><td bgcolor="'''
            + output_dtype_color
            + """">"""
            + str(dtype_name)
            + """</td></tr>"""
        )

        label = (
            """<<table BORDER="0" CELLBORDER="1" CELLSPACING="0">"""
            + op_row
            + mode_row
            + name_row
            + output_ttype_row
            + output_dtype_row
            + """</table>>"""
        )
        return label


class INPUT_MODES(OPERATOR_MODES):
    D_UINT8 = enum.auto()
    D_UINT16 = enum.auto()
    D_UINT32 = enum.auto()
    D_INT8 = enum.auto()
    D_INT16 = enum.auto()
    D_INT32 = enum.auto()
    D_FLOAT16 = enum.auto()
    D_FLOAT32 = enum.auto()
    P_UINT8 = enum.auto()
    P_UINT16 = enum.auto()
    I_UINT8 = enum.auto()
    I_BOOL = enum.auto()
    I_INT16 = enum.auto()
    I_FLOAT16 = enum.auto()
    I_FLOAT32 = enum.auto()
    P_FLOAT32 = enum.auto()
    S_FLOAT32 = enum.auto()
    S_FLOAT16 = enum.auto()
    S_INT16 = enum.auto()
    S_INT32 = enum.auto()
    V_UINT32 = enum.auto()
    V_FLOAT16 = enum.auto()
    I_NV12 = enum.auto()
    I_NV21 = enum.auto()
    I_UYVY = enum.auto()
    I_YUYV = enum.auto()
    I_IYUV = enum.auto()
    I_YUV4 = enum.auto()
    I_RGB = enum.auto()
    I_RGBX = enum.auto()
    S_HISTOGRAM = enum.auto()


class INPUT(Op):
    """
    Some text
    """

    def to_flatbuffer(self):
        import vosa_serialization.INPUT

        input_t = vosa_serialization.INPUT.INPUTT()
        input_t.dtype = types.DType.to_flatbuffer(self.get_return_data().dtype())

        return input_t

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[INPUT_MODES, ExecutionPattern]:
        return {
            INPUT_MODES.I_UINT8: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.DType.UINT8, types.AType.IMAGE),
            ),
            INPUT_MODES.I_BOOL: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.DType.BOOL, types.AType.IMAGE),
            ),
            INPUT_MODES.S_FLOAT32: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.DType.FLOAT32, types.AType.SCALAR),
            ),
            INPUT_MODES.S_FLOAT16: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.DType.FLOAT16, types.AType.SCALAR),
            ),
            INPUT_MODES.I_INT16: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.DType.INT16, types.AType.IMAGE),
            ),
            INPUT_MODES.I_FLOAT32: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.DType.FLOAT32, types.AType.IMAGE),
            ),
            INPUT_MODES.P_FLOAT32: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.DType.FLOAT32, types.AType.PLANE),
            ),
            INPUT_MODES.I_FLOAT16: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.DType.FLOAT16, types.AType.IMAGE),
            ),
            INPUT_MODES.S_INT16: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.DType.INT16, types.AType.SCALAR),
            ),
            INPUT_MODES.V_UINT32: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.DType.UINT32, types.AType.VECTOR),
            ),
            INPUT_MODES.V_FLOAT16: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.DType.FLOAT16, types.AType.VECTOR),
            ),
            INPUT_MODES.I_NV12: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.ImageDtype.NV12, types.AType.IMAGE),
            ),
            INPUT_MODES.I_NV21: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.ImageDtype.NV21, types.AType.IMAGE),
            ),
            INPUT_MODES.I_UYVY: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.ImageDtype.UYVY, types.AType.IMAGE),
            ),
            INPUT_MODES.I_YUYV: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.ImageDtype.YUYV, types.AType.IMAGE),
            ),
            INPUT_MODES.I_IYUV: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.ImageDtype.IYUV, types.AType.IMAGE),
            ),
            INPUT_MODES.I_YUV4: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.ImageDtype.YUV4, types.AType.IMAGE),
            ),
            INPUT_MODES.I_RGB: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.ImageDtype.RGB, types.AType.IMAGE),
            ),
            INPUT_MODES.I_RGBX: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.ImageDtype.RGBX, types.AType.IMAGE),
            ),
            INPUT_MODES.S_HISTOGRAM: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(
                    types.DType.UNDEFINED, types.SpecialType.HISTOGRAM_DISTRIBUTION
                ),
            ),
            INPUT_MODES.D_UINT8: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(
                    types.DType.UINT8, types.SpecialType.DATA
                ),
            ),
            INPUT_MODES.D_UINT16: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(
                    types.DType.UINT16, types.SpecialType.DATA
                ),
            ),
            INPUT_MODES.D_UINT32: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(
                    types.DType.UINT32, types.SpecialType.DATA
                ),
            ),
            INPUT_MODES.D_INT8: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.DType.INT8, types.SpecialType.DATA),
            ),
            INPUT_MODES.D_INT16: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(
                    types.DType.INT16, types.SpecialType.DATA
                ),
            ),
            INPUT_MODES.D_INT32: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(
                    types.DType.INT32, types.SpecialType.DATA
                ),
            ),
            INPUT_MODES.D_FLOAT16: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(
                    types.DType.FLOAT16, types.SpecialType.DATA
                ),
            ),
            INPUT_MODES.D_FLOAT32: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(
                    types.DType.FLOAT32, types.SpecialType.DATA
                ),
            ),
            INPUT_MODES.P_UINT16: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.DType.UINT16, types.AType.PLANE),
            ),
            INPUT_MODES.P_UINT8: ExecutionPattern(
                input_pattern=InputPattern(),
                output_pattern=types.DataType(types.DType.UINT8, types.AType.PLANE),
            ),
        }
