/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "reduce_sum_planewise.h"

std::shared_ptr<vosa::Op> vosa::ReduceSumPlanewiseBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_reduce_sum_planewise =
      vosa_operator_union.AsREDUCE_SUM_PLANEWISE();
  assert(serialized_reduce_sum_planewise);
  auto mode = serialized_reduce_sum_planewise->mode;
  switch (mode) {
  case vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::ReduceSumPlanewise<uint8_t, uint8_t>>();
  }
  case vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::ReduceSumPlanewise<uint16_t, uint16_t>>();
  }
  case vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::ReduceSumPlanewise<uint32_t, uint32_t>>();
  }
  case vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::ReduceSumPlanewise<int8_t, int8_t>>();
  }
  case vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::ReduceSumPlanewise<int16_t, int16_t>>();
  }
  case vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::ReduceSumPlanewise<int32_t, int32_t>>();
  }
  case vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::ReduceSumPlanewise<float, float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename inputT, typename outputT>
vosa::EigenImage<outputT> vosa::ReduceSumPlanewise<inputT, outputT>::runme(
    const EigenImage<inputT> &input) const {

  uint32_t height = input.dimension(0);
  uint32_t width = input.dimension(1);
  uint32_t channels = input.dimension(2);
  assert(channels == 1);

  outputT output_accumulator = 0;
  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      inputT raw_input = input(height_index, width_index, 0);
      outputT raw_input_as_outputT = raw_input;
      output_accumulator += raw_input_as_outputT;
    }
  }
  vosa::EigenImage<outputT> output{1, 1, 1};
  output(0, 0, 0) = output_accumulator;
  return output;
}

template <typename inputT, typename outputT>
vosa::ArrayBase::DType
vosa::ReduceSumPlanewise<inputT, outputT>::output_dtype() const {
  return output_dtype_helper<outputT>();
}

template <typename inputT, typename outputT>
std::shared_ptr<vosa::ArrayBase>
vosa::ReduceSumPlanewise<inputT, outputT>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<inputT> &input = inputs[0]->template get_image<inputT>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::REDUCE_SUM_PLANEWISE_MODE
vosa::ReduceSumPlanewise<uint8_t, uint8_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::REDUCE_SUM_PLANEWISE_MODE
vosa::ReduceSumPlanewise<uint16_t, uint16_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::REDUCE_SUM_PLANEWISE_MODE
vosa::ReduceSumPlanewise<uint32_t, uint32_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::REDUCE_SUM_PLANEWISE_MODE
vosa::ReduceSumPlanewise<int8_t, int8_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::REDUCE_SUM_PLANEWISE_MODE
vosa::ReduceSumPlanewise<int16_t, int16_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::REDUCE_SUM_PLANEWISE_MODE
vosa::ReduceSumPlanewise<int32_t, int32_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::REDUCE_SUM_PLANEWISE_MODE
vosa::ReduceSumPlanewise<float, float>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_SUM_PLANEWISE_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename inputT, typename outputT>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::ReduceSumPlanewise<inputT, outputT>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  vosa_serialization::REDUCE_SUM_PLANEWISET reduce_sum_planewise;
  reduce_sum_planewise.mode = get_serialization_mode();

  auto reduce_sum_planewise_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  reduce_sum_planewise_operator->op.Set(reduce_sum_planewise);
  reduce_sum_planewise_operator->name = op_name;
  reduce_sum_planewise_operator->inputs = input_names;
  return reduce_sum_planewise_operator;
}

template class vosa::ReduceSumPlanewise<uint8_t, uint8_t>;

template class vosa::ReduceSumPlanewise<uint16_t, uint16_t>;

template class vosa::ReduceSumPlanewise<uint32_t, uint32_t>;

template class vosa::ReduceSumPlanewise<int8_t, int8_t>;

template class vosa::ReduceSumPlanewise<int16_t, int16_t>;

template class vosa::ReduceSumPlanewise<int32_t, int32_t>;

template class vosa::ReduceSumPlanewise<float, float>;
