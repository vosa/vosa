/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_CUSTOM_H
#define VOSA_CUSTOM_H

#include <dlfcn.h>
#include <unordered_map>

#include "vosa/op.h"

namespace vosa {

typedef std::shared_ptr<vosa::Op> (*op_creation_function_t)();

class MasterRegistryException : public std::runtime_error {
public:
  explicit MasterRegistryException(const std::string &arg)
      : runtime_error(arg) {}
};

class MasterRegistry {
public:
  static void
  register_custom_scalar(const std::string &name,
                         const op_creation_function_t &op_creation_function) {
    MasterRegistry &instance = get_instance();
    instance.op_creation_map[name] = op_creation_function;
  }

  static MasterRegistry &get_instance() {
    static MasterRegistry instance;
    return instance;
  }

  MasterRegistry(const MasterRegistry &) = delete;

  void operator=(const MasterRegistry &) = delete;

  std::unordered_map<std::string, op_creation_function_t> get_ops() const {
    return op_creation_map;
  }

  static op_creation_function_t get_op(const std::string &op_name) {
    MasterRegistry &instance = get_instance();
    op_creation_function_t &op_creation_function =
        instance.op_creation_map[op_name];
    if (op_creation_function == nullptr) {
      std::string message = "Failed to find Op " + op_name + " in registry";
      throw MasterRegistryException(message);
    }
    return op_creation_function;
  }

private:
  MasterRegistry() = default;

  std::unordered_map<std::string, op_creation_function_t> op_creation_map;
};

class CustomScalarRegistrar {
public:
  CustomScalarRegistrar() = default;

  CustomScalarRegistrar(const CustomScalarRegistrar &) = delete;

  void operator=(const CustomScalarRegistrar &) = delete;

  void add_op(const std::string &name,
              const op_creation_function_t &op_creation_function) {
    registry[name] = op_creation_function;
  };

  void register_all() const {
    for (const auto &op : registry) {
      MasterRegistry::register_custom_scalar(op.first, op.second);
    }
  }

private:
  std::unordered_map<std::string, op_creation_function_t> registry;
};

extern "C" CustomScalarRegistrar *get_registrar();

class CustomOperatorLoadException : public std::runtime_error {
public:
  explicit CustomOperatorLoadException(const std::string &arg)
      : runtime_error(arg) {}
};

void load_custom_operators(const std::string &custom_shared_object_file);

} // namespace vosa

#endif // VOSA_CUSTOM_H
