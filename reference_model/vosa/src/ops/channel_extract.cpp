/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "channel_extract.h"

std::shared_ptr<vosa::Op> vosa::ChannelExtractBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_channel_extract = vosa_operator_union.AsCHANNEL_EXTRACT();
  assert(serialized_channel_extract);
  auto mode = serialized_channel_extract->mode;
  auto serialized_attributes = &serialized_channel_extract->attr;
  std::vector<uint32_t> channels = serialized_attributes->get()->channels;
  switch (mode) {
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_BOOL_BOOL: {
    return std::make_shared<vosa::ChannelExtract<bool>>(channels);
  }
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::ChannelExtract<uint8_t>>(channels);
  }
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::ChannelExtract<uint16_t>>(channels);
  }
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::ChannelExtract<uint32_t>>(channels);
  }
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::ChannelExtract<int8_t>>(channels);
  }
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::ChannelExtract<int16_t>>(channels);
  }
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::ChannelExtract<int32_t>>(channels);
  }
  case vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::ChannelExtract<float>>(channels);
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::EigenImage<T>
vosa::ChannelExtract<T>::runme(const vosa::EigenImage<T> &input) const {

  uint32_t input_height = input.dimension(0);
  uint32_t input_width = input.dimension(1);

  // Check that all channel indices are sane
  for (const auto &channel : channels)
    assert(channel <
           input.dimension(2)); // No need to check the lower bound (i.e. 0) as
                                // the indices are unsigned

  vosa::EigenImage<T> output{input_height, input_width,
                             (uint32_t)channels.size()};

  for (uint32_t height_index = 0; height_index < input_height; height_index++) {
    for (uint32_t width_index = 0; width_index < input_width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < channels.size();
           channel_index++)
        output(height_index, width_index, channel_index) =
            input(height_index, width_index, channels[channel_index]);
    }
  }
  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::ChannelExtract<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::ChannelExtract<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::CHANNEL_EXTRACT_MODE
vosa::ChannelExtract<bool>::get_serialization_mode() const {
  return vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_BOOL_BOOL;
}

template <>
vosa_serialization::CHANNEL_EXTRACT_MODE
vosa::ChannelExtract<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::CHANNEL_EXTRACT_MODE
vosa::ChannelExtract<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::CHANNEL_EXTRACT_MODE
vosa::ChannelExtract<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::CHANNEL_EXTRACT_MODE
vosa::ChannelExtract<int8_t>::get_serialization_mode() const {
  return vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::CHANNEL_EXTRACT_MODE
vosa::ChannelExtract<int16_t>::get_serialization_mode() const {
  return vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::CHANNEL_EXTRACT_MODE
vosa::ChannelExtract<int32_t>::get_serialization_mode() const {
  return vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::CHANNEL_EXTRACT_MODE
vosa::ChannelExtract<float>::get_serialization_mode() const {
  return vosa_serialization::CHANNEL_EXTRACT_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::ChannelExtract<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  auto channel_extract_attribute =
      std::make_unique<vosa_serialization::CHANNEL_EXTRACT_ATTRIBUTET>();
  channel_extract_attribute->channels = channels;

  vosa_serialization::CHANNEL_EXTRACTT channel_extract;
  channel_extract.mode = get_serialization_mode();
  channel_extract.attr = std::move(channel_extract_attribute);

  auto channel_extract_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  channel_extract_operator->op.Set(std::move(channel_extract));
  channel_extract_operator->name = op_name;
  channel_extract_operator->inputs = input_names;
  return channel_extract_operator;
}

template class vosa::ChannelExtract<bool>;

template class vosa::ChannelExtract<uint8_t>;

template class vosa::ChannelExtract<uint16_t>;

template class vosa::ChannelExtract<uint32_t>;

template class vosa::ChannelExtract<int8_t>;

template class vosa::ChannelExtract<int16_t>;

template class vosa::ChannelExtract<int32_t>;

template class vosa::ChannelExtract<float>;
