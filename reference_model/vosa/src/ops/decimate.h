/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_CODE_DECIMATE_H
#define VOSA_CODE_DECIMATE_H

#include "vosa/op.h"

namespace vosa {

class DecimateBase : public Op {
public:
  static std::shared_ptr<Op> parse_serialized(
      const vosa_serialization::VosaOperatorUnion &vosa_operator_union);
};

template <typename T>
class Decimate : public DecimateBase {
public:
  explicit Decimate(std::vector<uint32_t> decimate_window,
                    std::vector<uint32_t> offsets)
      : decimate_window(std::move(decimate_window)),
        offsets(std::move(offsets)) {
    assert(this->decimate_window.size() == 2);
    assert(this->offsets.size() == this->decimate_window.size());

    assert(this->offsets[0] < this->decimate_window[0]);
    assert(this->offsets[1] < this->decimate_window[1]);
  }

  [[nodiscard]] EigenImage<T> runme(const EigenImage<T> &) const;
  [[nodiscard]] std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &inputs) const final;
  [[nodiscard]] ArrayBase::DType output_dtype() const final;
  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

private:
  std::vector<uint32_t> decimate_window;
  std::vector<uint32_t> offsets;

  [[nodiscard]] vosa_serialization::DECIMATE_MODE
  get_serialization_mode() const;
};

} // namespace vosa

#endif // VOSA_CODE_DECIMATE_H
