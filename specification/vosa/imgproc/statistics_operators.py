"""
License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""


import enum
import typing

import vosa.types as types
from vosa.op import OPERATOR_MODES, Op, ExecutionPattern, InputPattern


class CREATE_HISTOGRAM_MODES(OPERATOR_MODES):
    S_H_UINT32_S = enum.auto()


class CreateHistogramExecutionPattern(ExecutionPattern):
    def __init__(self):
        super().__init__(
            input_pattern=InputPattern(),
            output_pattern=types.DataType(
                types.DType.UNDEFINED,
                types.SpecialType.HISTOGRAM_DISTRIBUTION,
                description="The empty histogram with complete metadata ready to be populated with values.",
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            CREATE_HISTOGRAM.OFFSET_ATTRIBUTE: types.DataType(
                types.DType.INT32,
                types.AType.SCALAR,
                shape=[],
                description="The lower bound of the lowest bin in the Histogram",
            ),
            CREATE_HISTOGRAM.RANGE_ATTRIBUTE: types.DataType(
                types.DType.UINT32,
                types.AType.SCALAR,
                shape=[],
                description="The range of the histogram (upper bound of the highest bin minus lower bound of the lowest bin).",
            ),
            CREATE_HISTOGRAM.NUM_BINS_ATTRIBUTE: types.DataType(
                types.DType.UINT32,
                types.AType.SCALAR,
                shape=[],
                description="The number of bins to split the range into.",
            ),
        }


class CREATE_HISTOGRAM(Op):
    """
    Creates an empty Histogram ready to be populated with entries from one or more Images.
    """

    OFFSET_ATTRIBUTE = "offset"
    RANGE_ATTRIBUTE = "range"
    NUM_BINS_ATTRIBUTE = "num_bins"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[CREATE_HISTOGRAM_MODES, ExecutionPattern]:
        return {CREATE_HISTOGRAM_MODES.S_H_UINT32_S: CreateHistogramExecutionPattern()}


class EQUALIZE_HISTOGRAM_MODES(OPERATOR_MODES):
    I_I_S_UINT8 = enum.auto()
    I_I_S_INT16 = enum.auto()


class EqualizeHistogramExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    types.DType.UNDEFINED,
                    types.SpecialType.HISTOGRAM_DISTRIBUTION,
                    description="The input Histofram to be equalized",
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.ARRAY,
                shape=["2^bidwidth(output_t)"],
                description="The output lookup table.",
            ),
        )


class EQUALIZE_HISTOGRAM(Op):
    """
    Create an array representing an equalized lookup table from the input Histogram.

    Histogram bins are first mapped to `output_t`. Where a bin spans multiple pixel values, the bin is distributed equally across possible values. Where a bin extends outside the representable range of `output_t` those bin values are ignored.

    After an `output_t` distribution has been built, its cumulative distribution function is calculated and an equalized distribution is built, which is returned as the output.
    """

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[EQUALIZE_HISTOGRAM_MODES, ExecutionPattern]:
        return {
            EQUALIZE_HISTOGRAM_MODES.I_I_S_UINT8: EqualizeHistogramExecutionPattern(
                types.DType.UINT8
            ),
            EQUALIZE_HISTOGRAM_MODES.I_I_S_INT16: EqualizeHistogramExecutionPattern(
                types.DType.INT16
            ),
        }


class UPDATE_HISTOGRAM_MODES(OPERATOR_MODES):
    I_H_UINT8_S = enum.auto()
    I_H_INT16_S = enum.auto()


class UpdateHistogramExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    types.DType.UINT8,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image, statistics of which are to be collected.",
                ),
                types.DataType(
                    types.DType.UNDEFINED,
                    types.SpecialType.HISTOGRAM_DISTRIBUTION,
                    description="Input Histogram, statistics of which will be accumulated into.",
                ),
            ),
            output_pattern=types.DataType(
                types.DType.UNDEFINED,
                types.SpecialType.HISTOGRAM_DISTRIBUTION,
                description="Output Histogram containing all data from the input Histogram as well as any additional statistics from the input Image.",
            ),
        )


class UPDATE_HISTOGRAM(Op):
    """
    Increment statistics in the input Histogram using values from the input Image.

    Each pixel value (treating each channel as an independent pixel) falling within a bin, increments that bin's value by one.

    Pixel values falling outside all bins (below the lower bin or above the upper bin) are disregarded.
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[UPDATE_HISTOGRAM_MODES, ExecutionPattern]:
        return {
            UPDATE_HISTOGRAM_MODES.I_H_UINT8_S: UpdateHistogramExecutionPattern(
                types.DType.UINT8
            ),
            UPDATE_HISTOGRAM_MODES.I_H_INT16_S: UpdateHistogramExecutionPattern(
                types.DType.INT16
            ),
        }
