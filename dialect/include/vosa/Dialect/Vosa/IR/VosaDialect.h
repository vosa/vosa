/* -*- c++ -*-
 *
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_DIALECT_VOSA_VOSADIALECT_H
#define VOSA_DIALECT_VOSA_VOSADIALECT_H

#include "mlir/IR/Dialect.h"

#include "vosa/Dialect/Vosa/IR/VosaOpsDialect.h.inc"

namespace mlir {
namespace vosa {
enum class DataFormat { CHW, HWC };

/// Get data format used by VOSA
///
/// The `vosa-data-format` flag determines if VOSA uses HWC (default), or
/// CHW format for image tensors
DataFormat getDataFormat();

} // namespace vosa
} // namespace mlir

#endif // VOSA_DIALECT_VOSA_VOSADIALECT_H
