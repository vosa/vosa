"""
License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import os
import inspect
import subprocess
import sys
from sys import platform

import vosa
import vosa.introduction
import vosa.custom

ADOC = "adoc"
ADOC_OPS = "ops"
OPS_ADOC_FILE = "ops.adoc"


def make_adoc_for_file(module, path):
    found_ops = {}
    module_string = module.__name__.split(".")[-1]
    next_path = os.path.join(path, module_string)
    module_adoc_lines = ["=== %s" % module_string.title().replace("_", " ")]
    module_docstring = module.__doc__
    if module_docstring is not None:
        module_docstring = module_docstring.split("License")[0]
        module_adoc_lines.append(module_docstring)
    for name, vosa_ops_member in inspect.getmembers(module, inspect.isclass):
        if issubclass(vosa_ops_member, vosa.op.Op) and not inspect.isabstract(
            vosa_ops_member
        ):
            found_ops[name] = vosa_ops_member
            file_name = name + ".adoc"
            os.makedirs(next_path, exist_ok=True)
            op_adoc_path = os.path.join(next_path, file_name)
            relative_op_adoc_path = os.path.join(module_string, file_name)
            member_adoc_lines = vosa_ops_member.adoc()
            with open(op_adoc_path, "w") as f:
                for line in member_adoc_lines:
                    f.write("%s\n" % line)
            if vosa_ops_member.include_in_specification():
                ops_adoc_line = "include::%s[]" % relative_op_adoc_path
                module_adoc_lines.append(ops_adoc_line)
    module_path = next_path + ".adoc"
    with open(module_path, "w") as f:
        for line in module_adoc_lines:
            f.write("%s\n" % line)
    return found_ops


def make_adoc_for_module(module, path):
    found_ops = {}
    module_string = module.__name__.split(".")[-1]
    next_path = os.path.join(path, module_string)
    module_adoc_lines = []
    for name, submodule in inspect.getmembers(module, inspect.ismodule):
        print("Making adoc for module {}".format(submodule))
        found_ops[name] = make_adoc_for_file(submodule, next_path)
        file_name = name + ".adoc"
        relative_op_adoc_path = os.path.join(module_string, file_name)
        ops_adoc_line = "include::%s[]" % relative_op_adoc_path
        module_adoc_lines.append(ops_adoc_line)
    module_path = next_path + ".adoc"
    with open(module_path, "w") as f:
        for line in module_adoc_lines:
            f.write("%s\n" % line)
    return found_ops


def main():
    os.makedirs(ADOC, exist_ok=True)

    try:
        tag = os.environ["CI_COMMIT_TAG"]
    except KeyError:
        tag = "live"

    vosa_lines = [
        ":math:",
        ":imagesoutdir: generated_images",
        ":imagesdir: images",
        ":stem: latexmath",
        "= VOSA {} Specification".format(tag),
        ":numbered:",
        ":toc: left",
        ":toclevels: 4",
        "",
    ]

    vosa_include_lines = []
    introduction_file = vosa.introduction.adoc(ADOC)
    vosa_include_lines.extend(["include::%s[]" % introduction_file])

    operator_groups = [vosa.imgproc]
    vosa_include_lines.extend(["", "== Operators"])
    for operator_group in operator_groups:
        _ = make_adoc_for_module(operator_group, ADOC)
        module_string = operator_group.__name__.split(".")[-1]
        vosa_include_lines.append("include::%s[]" % (module_string + ".adoc"))

    custom_operators_file = vosa.custom.adoc(ADOC)
    vosa_include_lines.extend(["include::%s[]" % custom_operators_file])

    vosa_adoc_path = os.path.join(ADOC, "vosa.adoc")
    vosa_lines += vosa_include_lines

    with open(vosa_adoc_path, "w") as f:
        for line in vosa_lines:
            f.write("%s\n" % line)

    html_cmd = ["asciidoctor", "-b", "html5", vosa_adoc_path]
    print(" ".join(html_cmd))
    if platform == "darwin":
        subprocess.run(" ".join(html_cmd), shell=True, executable="/bin/bash")
    else:
        subprocess.run(" ".join(html_cmd), shell=True, executable="/usr/bin/bash")
    return True


if __name__ == "__main__":
    return_code = main()
    if return_code:
        sys.exit(0)
    else:
        sys.exit(1)
