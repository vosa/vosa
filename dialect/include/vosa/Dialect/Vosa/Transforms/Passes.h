/* -*- c++ -*-
 *
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_DIALECT_TRANSFORMS_PASSES_H
#define VOSA_DIALECT_TRANSFORMS_PASSES_H

#include "mlir/Pass/Pass.h"

namespace mlir {
namespace vosa {

std::unique_ptr<Pass> createVosaInferShapesPass();
std::unique_ptr<Pass> createVosaDebugDumpPass();
std::unique_ptr<Pass>
createVosaPipelineFragmentPass(ArrayRef<std::string> inputs = {},
                               ArrayRef<std::string> outputs = {});
std::unique_ptr<Pass> createVosaOpProfilePass(raw_ostream &os = llvm::errs());
std::unique_ptr<Pass> createVosaOpProfilePass(raw_ostream &os,
                                              llvm::StringRef profile);
std::unique_ptr<Pass> createVosaPadToTensorPadPass(bool constZero = false);
std::unique_ptr<Pass> createVosaEliminateCastPass();
std::unique_ptr<Pass> createVosaSplitChannelPass();

/// Generate the code for registering conversion passes.
#define GEN_PASS_REGISTRATION
#include "vosa/Dialect/Vosa/Transforms/Passes.h.inc"

} // namespace vosa
} // namespace mlir

#endif // VOSA_DIALECT_TRANSFORMS_PASSES_H
