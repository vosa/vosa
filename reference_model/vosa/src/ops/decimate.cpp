/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "decimate.h"

using namespace std;
using namespace vosa;
using namespace vosa_serialization;

// Template initialisation
template class vosa::Decimate<uint8_t>;

template class vosa::Decimate<uint16_t>;

template class vosa::Decimate<uint32_t>;

template class vosa::Decimate<int8_t>;

template class vosa::Decimate<int16_t>;

template class vosa::Decimate<int32_t>;

template class vosa::Decimate<float>;

shared_ptr<Op>
DecimateBase::parse_serialized(const VosaOperatorUnion &vosa_operator_union) {
  auto serialized_decimate = vosa_operator_union.AsDECIMATE();
  assert(serialized_decimate);

  vector<uint32_t> decimate_window = serialized_decimate->attr->window;
  vector<uint32_t> offsets = serialized_decimate->attr->offsets;

  switch (serialized_decimate->mode) {
  case DECIMATE_MODE_I_I_UINT8_UINT8:
    return make_shared<Decimate<uint8_t>>(decimate_window, offsets);
  case DECIMATE_MODE_I_I_UINT16_UINT16:
    return make_shared<Decimate<uint16_t>>(decimate_window, offsets);
  case DECIMATE_MODE_I_I_UINT32_UINT32:
    return make_shared<Decimate<uint32_t>>(decimate_window, offsets);
  case DECIMATE_MODE_I_I_INT8_INT8:
    return make_shared<Decimate<int8_t>>(decimate_window, offsets);
  case DECIMATE_MODE_I_I_INT16_INT16:
    return make_shared<Decimate<int16_t>>(decimate_window, offsets);
  case DECIMATE_MODE_I_I_INT32_INT32:
    return make_shared<Decimate<int32_t>>(decimate_window, offsets);
  case DECIMATE_MODE_I_I_FLOAT32_FLOAT32:
    return make_shared<Decimate<float>>(decimate_window, offsets);
  default:
    throw OperatorModeNotImplementedError();
  }
}

template <typename T>
shared_ptr<ArrayBase>
Decimate<T>::eval(const vector<shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();

  return make_shared<ArrayBase>(this->runme(input));
}

template <typename T>
EigenImage<T> Decimate<T>::runme(const EigenImage<T> &input) const {

  const uint32_t height =
      input.dimension(0) / decimate_window[0] +
      (input.dimension(0) % decimate_window[0] == 0 ? 0 : 1);
  const uint32_t width = input.dimension(1) / decimate_window[1] +
                         (input.dimension(1) % decimate_window[1] == 0 ? 0 : 1);
  const uint32_t channels = input.dimension(2);

  auto output = vosa::EigenImage<T>{height, width, channels};

  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      for (uint32_t c = 0; c < channels; c++)
        output(y, x, c) = input(y * decimate_window[0] + offsets[0],
                                x * decimate_window[1] + offsets[1], c);

  return output;
}

template <typename T>
ArrayBase::DType Decimate<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
unique_ptr<VosaGraphOperatorT>
Decimate<T>::serialize(const string &op_name,
                       const vector<string> &input_names) const {
  auto decimate_attribute = make_unique<DECIMATE_ATTRIBUTET>();
  decimate_attribute->window = decimate_window;
  decimate_attribute->offsets = offsets;

  DECIMATET decimate;
  decimate.mode = get_serialization_mode();
  decimate.attr = std::move(decimate_attribute);

  auto decimate_operator = make_unique<VosaGraphOperatorT>();
  decimate_operator->op.Set(std::move(decimate));
  decimate_operator->name = op_name;
  decimate_operator->inputs = input_names;

  return decimate_operator;
}

template <>
DECIMATE_MODE Decimate<uint8_t>::get_serialization_mode() const {
  return DECIMATE_MODE_I_I_UINT8_UINT8;
}

template <>
DECIMATE_MODE Decimate<uint16_t>::get_serialization_mode() const {
  return DECIMATE_MODE_I_I_UINT16_UINT16;
}

template <>
DECIMATE_MODE Decimate<uint32_t>::get_serialization_mode() const {
  return DECIMATE_MODE_I_I_UINT32_UINT32;
}

template <>
DECIMATE_MODE Decimate<int8_t>::get_serialization_mode() const {
  return DECIMATE_MODE_I_I_INT8_INT8;
}

template <>
DECIMATE_MODE Decimate<int16_t>::get_serialization_mode() const {
  return DECIMATE_MODE_I_I_INT16_INT16;
}

template <>
DECIMATE_MODE Decimate<int32_t>::get_serialization_mode() const {
  return DECIMATE_MODE_I_I_INT32_INT32;
}

template <>
DECIMATE_MODE Decimate<float>::get_serialization_mode() const {
  return DECIMATE_MODE_I_I_FLOAT32_FLOAT32;
}