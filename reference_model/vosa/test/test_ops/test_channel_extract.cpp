/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/channel_extract.h"
#include "test_bench.h"
#include "vosa/vosa.h"

using namespace std;
using namespace vosa;

template <typename T>
static EigenImage<T>
run_channel_extract(EigenImage<T> &image,
                    const std::vector<uint32_t> &channels) {
  auto Op = ChannelExtract<T>(channels);
  return Op.eval({make_shared<ArrayBase>(image)})->template get_image<T>();
}

TEST(test_channel_extract, single_channels) {
  EigenImage<uint32_t> image(4, 3, 5);
  image.setValues({{{4, 2, 1, 4, 8}, {4, 7, 6, 0, 3}, {1, 6, 9, 7, 7}},
                   {{5, 8, 3, 4, 7}, {3, 9, 4, 4, 2}, {4, 8, 2, 4, 1}},
                   {{9, 7, 0, 4, 6}, {2, 3, 1, 6, 6}, {7, 6, 4, 2, 2}},
                   {{2, 5, 9, 3, 2}, {3, 3, 3, 8, 4}, {7, 2, 2, 2, 3}}});
  const std::vector<uint32_t> channel = {2};

  EigenImage<uint32_t> expected(image.dimension(0), image.dimension(1), 1);
  expected.setValues(
      {{{1}, {6}, {9}}, {{3}, {4}, {2}}, {{0}, {1}, {4}}, {{9}, {3}, {2}}});

  EigenImage<uint32_t> actual = run_channel_extract(image, channel);

  bool results =
      test::TestBench<uint32_t, int32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(results);
}

TEST(test_channel_extract, multiple_channels) {
  EigenImage<uint32_t> image(4, 3, 5);
  image.setValues({{{4, 2, 1, 4, 8}, {4, 7, 6, 0, 3}, {1, 6, 9, 7, 7}},
                   {{5, 8, 3, 4, 7}, {3, 9, 4, 4, 2}, {4, 8, 2, 4, 1}},
                   {{9, 7, 0, 4, 6}, {2, 3, 1, 6, 6}, {7, 6, 4, 2, 2}},
                   {{2, 5, 9, 3, 2}, {3, 3, 3, 8, 4}, {7, 2, 2, 2, 3}}});
  const std::vector<uint32_t> channels = {0, 3, 4};

  EigenImage<uint32_t> expected(image.dimension(0), image.dimension(1),
                                (uint32_t)channels.size());
  expected.setValues({{{4, 4, 8}, {4, 0, 3}, {1, 7, 7}},
                      {{5, 4, 7}, {3, 4, 2}, {4, 4, 1}},
                      {{9, 4, 6}, {2, 6, 6}, {7, 2, 2}},
                      {{2, 3, 2}, {3, 8, 4}, {7, 2, 3}}});

  EigenImage<uint32_t> actual = run_channel_extract(image, channels);

  bool results =
      test::TestBench<uint32_t, int32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(results);
}

TEST(test_channel_extract, all_channels) {
  EigenImage<uint32_t> image(4, 3, 5);
  image.setValues({{{4, 2, 1, 4, 8}, {4, 7, 6, 0, 3}, {1, 6, 9, 7, 7}},
                   {{5, 8, 3, 4, 7}, {3, 9, 4, 4, 2}, {4, 8, 2, 4, 1}},
                   {{9, 7, 0, 4, 6}, {2, 3, 1, 6, 6}, {7, 6, 4, 2, 2}},
                   {{2, 5, 9, 3, 2}, {3, 3, 3, 8, 4}, {7, 2, 2, 2, 3}}});
  const std::vector<uint32_t> channels = {0, 1, 2, 3, 4};

  EigenImage<uint32_t> expected = image;

  EigenImage<uint32_t> actual = run_channel_extract(image, channels);

  bool results =
      test::TestBench<uint32_t, int32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(results);
}