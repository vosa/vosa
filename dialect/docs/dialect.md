# The VOSA dialect

## Design

### Types

In VOSA operators, images are represented as 3D tensors.  The default data
format is HWC ordering, with CHW as an option.  Planes are represented as
a single channel image (i.e. 3D HWC tensor with dimension 2 set to 1).  The
elements of these tensors are:

* Boolean, represented as `i1`
* Signed integers - 8 / 16 / 32 bit, represented as `si8`, `si16`, `si32`
* Unsigned integers - 8 / 16 / 32 bit, represented as `ui8`, `ui16`, `ui32`
* Floating point - 16 / 32 bit, represented as `fp16`, `fp32`

For example an 8-bit unsigned 1920x1080 plane is represented as
`tensor<1920x1080x1xui8>`, a 16-bit floating point 3 channel 1000x500 image
as `tensor<1000x500x3xfp16>`.

The exact types supported depend on the operator.  Each operator has a
verifier that checks the inputs and outputs are of the expected dimensions,
size and type - e.g. elementwise binary operators expect both operands to
be of the same size and data type.  Other operators may produce an output of
different size or data type, but this is verified at compile time.  In some
cases, the output shape and type can be inferred from an operators inputs -
this allows operators to be declared with an unknown size
(e.g. `<?x?x?xu8>`) and a shape inference pass will determine the actual
output shape - this is used when constructing a pipeline from a data file.

### Operators

The definition of the operators in the VOSA dialect follows the
specification of the operators in the VOSA specification.

### Lowering

At the VOSA level, the integer types are explicitly signed (e.g. `si32` and
`ui32`), these will be converted to signless types of the same size (`i8`,
`i16`, `i32`) when lowering to TOSA / linalg, using the appropriate
signed/unsigned variants of operators where necessary (e.g. `arith.divsi`
and `arith.divui`).

The vosa compiler supports lowering operators in the VOSA dialect to the
TOSA and linalg dialect.  A VOSA operator is lowered to TOSA where the VOSA
operator can be implemented using primarily TOSA operators, with some other
dialects, e.g. linalg / tensor / arith dialect operators to help manipulate
the inputs/outputs to/from the format required by the TOSA operator.  Where
an operator can't easily be expressed as a TOSA operator, it is lowered to
linalg.  By lowering via TOSA, we can take advantage of any optimizations
applied at the TOSA level, avoid some complex lowering to linalg and also
benefit from future improvements to the TOSA->linalg lowering.

### Relationship to TOSA

VOSA has operators that can be trivially mapped to TOSA operators.  These
exist as separate operators in the VOSA dialect, so that the user of the
VOSA dialect does not need a direct dependency on TOSA and does not have to
produce a mix of TOSA and VOSA operators.  This also allows for stricter
type checking (e.g. TOSA "add" allows arbitrary tensor shapes, VOSA "add"
only applies to a 3D tensor of the supported types).  This also allows the
lowering via TOSA to be changed in the future.

## Implementation

The structure of the code mirrors the main MLIR repository with dialect and
pass declarations in `include` and implementations in `lib` and follows a
similar naming convention for the directories within.

### Dialect

The VOSA dialect is defined in `dialect/include/vosa/Dialect/Vosa` and
implemented in `dialect/lib/Dialect/Vosa`.  The dialect definition is in
`dialect/include/vosa/Dialect/Vosa/IR`, with the following key files:

* `VosaDialect.td`: The top level definition of the dialect
* `VosaTypes.td`: Defines the types used in the VOSA operators
* `VosaOps.td`: Defines the operators in the VOSA dialect

Key files in `dialect/lib/Dialect/Vosa` are:

* `IR/VosaOps.cpp`: Implements the verify functions for the VOSA operators

The VOSA dialect is tested by files in `dialect/test/Vosa`.  Each group of
operators has a `.mlir` file which runs that file through the `vosa-opt` to
in order to round-trip (i.e. parse and print out again) the IR.  This
ensures that verification passes for all valid operators, or checks the
error message for invalid operators.  Each operator is tested with a number
of valid inputs and a number of invalid inputs (e.g. unsupported input
type, incorrect output type, missing attributes).

### Conversion

The available conversion passes are defined in
`dialect/include/vosa/Conversion/Passes.td`.  The conversion passes are declared in
the `dialect/include/vosa/Conversion` directory and implemented in the
`dialect/lib/vosa/Conversion` directory

There is currently one pass defined: `VosaToTosa` which lowers VOSA
operators to the TOSA and linalg dialects (This needs to be broken up in to
separate passes for TOSA and linalg).

The VOSA lowering is tested by the `dialect/test/Vosa/vosa-to-tosa.mlir` file.
This runs the file through `vosa-opt` with the `-vosa-to-tosa` pass
enabled.  Each VOSA operator is instantiated and the output checked for the
expected sequence of TOSA / linalg / other operators.

### Passes

#### Shape inference

Run using `-vosa-infer-shapes` option to `vosa-opt`

When building a pipeline, the description may not fully describe the
dimensions of the input / output nodes.  These are represented as dynamic
dimensions in MLIR, e.g an `uint8` image is `tensor<?x?x?xui8>`.  However,
all operators have predictable output dimensions based on the dimensions of
their inputs.  For example, element-wise operators will preserve the
dimensions of their inputs, padding will add a known amount to the H & W
dimensions, channel-wise reduction will remove channels, etc.  This pass
will walk over functions containing VOSA operators and determine the static
sizes of dynamic dimensions based on each operators inputs.  This is
repeated until all dimensions have been resolved.

#### Debug

Run using `-vosa-debug-dump -vosa-debug-dump-nodes=<nodes>` option to
`vosa-opt`, where `<nodes>` is a comma separated list of names of the nodes
to dump.  Use `-vosa-debug-dump-nodes=*` to dump all nodes of a pipeline.

For each node to be dumped, code is generated that will call the
`dump_tensor_<type>` or `dump_<type>` functions defined in the VOSA runtime
library (in `dialect/runtime`) which will write the data to a file and a
JSON file describing the data format.

#### Eliminate casts

Run using `-vosa-eliminate-cast` option to `vosa-opt`.

This pass removes the `vosa.cast` operator from `vosa.cast` ->
`vosa.conv_2d` chains. This is safe and desirable as Linalg convolutions
(which `vosa.conv_2d` is lowered to) do numeric casting internally. This
leads to better performance.

#### Padding

Run using `-vosa-pad-to-tensor-pad` option to `vosa-opt`.

This pass converts `vosa.pad` operators to `tensor.pad`.  If the
`const-zero` option is enabled, then all pads are converted to constant
pads of a zero value (which some backends have an optimized lower of),
otherwise the original padding mode is preserved.

#### Pipeline fragments

Run using `-vosa-pipeline-fragment` option to `vosa-opt`.

Sometimes it's necessary to examine a sub-section of a larger pipeline,
e.g. to isolate where an error occurs or to benchmark a section of the
pipeline.   The `-vosa-pipeline-fragment` pass can be used to extract a
section of a pipeline.  The `outputs` parameter specifies the names of the
nodes that will be the outputs of the fragment - existing outputs are
removed unless specified in `outputs`.  The `inputs` parameter specifies
additional inputs to the pipeline - these are added to the argument list of
the generated function.  The canoncicalizer pass is run to remove any
values that are no longer required to compute the outputs, and any inputs
that are no longer required are removed from the function arguments.  The
output of the pipeline is a function that only contains the inputs and
operators required to compute the specified outputs.

```
../build/bin/vosa-opt \
  -vosa-pipeline-fragment="outputs=78,92,94,106,150,154,161,160,13 inputs=7,9,14,102" \
  --mlir-print-debuginfo \
  -o fragment.mlir pipeline_vosa.mlir
```

## Tools

### vosa-opt

The `vosa-opt` tool is the low level tool that can parse and process the
VOSA dialect, analagous to MLIR's `mlir-opt` tool.  It parses MLIR assembly
using the VOSA dialect and runs the user specified passes, emitting the
output as MLIR assembly.  The VOSA specific passes are documented below.

The `-vosa-data-format=<chw|hwc>` option selects between CHW and HWC data
formats.  HWC is the default if not specified.

## Development walkthorughs

### Adding an operator

To add an operator to the VOSA dialect:

* Add it to the dialect definition (in `dialect/include/vosa/Dialect/Vosa/IR/` and
  `dialect/lib/Dialect/Vosa/IR`).  Add the operator definition to
  `VosaOps.td`.  Any new type aliases should be added to `VosaTypes.td`.
    * The `arguments` section contains the inputs to the operator - each
      has a type specifier defining the allowed types (e.g. `Vosa_Image` is
      a 3D tensor of unsigned/signed/float values) and a name.  Runtime
      values are MLIR values, compile time constants are MLIR attributes.
    * The `results` section defines the outputs of the operator - again
      with a type and name for each.
    * `assemblyFormat` defines how the operator appears in the MLIR
      assembly (the default shown below is usually OK)
    * `verifier` defines a function that is called to check the operator is
      valid - this allows checks on data types, dimensions, attributes etc
      that aren't expressible in the arguments / results type definitions
      (e.g. some operators require that the output is the next widest
      integer type)
    * The `InferShapedTypeOpInterface` trait indicates that the operator
      can determine the output dimensions from the dimensions of it's
      inputs and the `inferReturnTypeComponents` function should be
      implemented to do this.

    ```
    def Vosa_ArithmeticShiftRightOp : Vosa_Op<"arithmetic_shift_right", [
        DeclareOpInterfaceMethods<InferShapedTypeOpInterface,
                                  ["inferReturnTypeComponents"]>,
        NoSideEffect]> {
        let summary = "Perform an elementwise arithmetic right shift on all pixel values";
        let description = [{
            Perform an elementwise arithmetic right shift on all pixel values

                output[c,y,x] = input[c,y,x] >> shift
        }];

        let arguments = (ins
            Vosa_ImageSignedInt:$input,
            I32Attr:$shift
        );
        let results = (outs
            Vosa_ImageSignedInt:$output
        );

        let assemblyFormat = [{
            $input attr-dict `:` type($input) `->` type($output)
        }];

        let hasVerifier = 1;
    }
    ```

* Add implementations for the verifier and shape inference to
  `dialect/lib/Dialect/Vosa/IR/VosaOps.cpp`.

* Add tests for the operator definition to `dialect/test/Vosa/` (file depends on
  operator category).  This checks that the operator can be parsed and that
  it round-trips (i.e. outputs the same as input) correctly through the
  `vosa-opt` tool.  This should also check error cases that aren't valid
  (e.g. mismatched data types, dimensions, missing attributes etc).

* Add tests for shape inference to `dialect/test/Vosa/shape_inference.mlir` - this
  checks that an operator with static inputs, can infer the sizes of the
  outputs.

* Build and run the tests with:

    ```
    cmake --build ${build_dir} --target check-vosa-dialect
    ```

* Add lowering from VOSA to TOSA or linalg dialects.  Currently all
  lowering is done in `dialect/lib/Conversion/VosaToTosa/VosaToTosa.cpp`.
    * Create a `MyOpConverter` struct which inherits from
      `OpConversionPattern<vosa::MyOp>` and has a `matchAndRewrite()`
      function.
        * The `OpAdaptor adaptor` input is a wrapper around the operator
          input type which converts the input types to the lower level
          dialects (e.g. signed/unsigned intgers to signless).
        * The output type should be explicitly converted:
          `auto outputTy = getTypeConverter()->convertType(op.getType());`
        * Generate the TOSA/linalg/tensor/arith dialect operators required
          to implement the function of the VOSA operator and replace the
          VOSA operator with the output of the lowered operator(s):
          `rewriter.replaceOp(op, tosaOp);`
    * Add tests in `dialect/test/Vosa/vosa-to-tosa.mlir` - these check the expected
      lowered operators are produced for the VOSA operators
    * Add end-to-end tests in `dialect/test/Vosa/end_to_end/` - these lower to
      executable code and execute using the `dialect/test/e2e-runner` script
      allowing the full execution of the VOSA operator.  This is advisable
      for complex operators.

* Add the operator to the flatbuffer builder.
    * Implement the handler for the operator in
      `tools/include/vosa/Builder/mlir_builder.h` and
      `tools/lib/Builder/mlir_builder.cpp` to generate the MLIR operator.
    * Add `process_MyOp()` to `Deserializer` in
      `tools/lib/Builder/mlir_fbs_builder.cpp` and implement
      deserialization from serialized form.
