/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/op.h"

template <>
vosa::ArrayBase::DType vosa::Op::output_dtype_helper<bool>() const {
  return vosa::ArrayBase::BOOL;
}

template <>
vosa::ArrayBase::DType vosa::Op::output_dtype_helper<uint8_t>() const {
  return vosa::ArrayBase::UINT8;
}

template <>
vosa::ArrayBase::DType vosa::Op::output_dtype_helper<uint16_t>() const {
  return vosa::ArrayBase::UINT16;
}

template <>
vosa::ArrayBase::DType vosa::Op::output_dtype_helper<uint32_t>() const {
  return vosa::ArrayBase::UINT32;
}

template <>
vosa::ArrayBase::DType vosa::Op::output_dtype_helper<int8_t>() const {
  return vosa::ArrayBase::INT8;
}

template <>
vosa::ArrayBase::DType vosa::Op::output_dtype_helper<int16_t>() const {
  return vosa::ArrayBase::INT16;
}

template <>
vosa::ArrayBase::DType vosa::Op::output_dtype_helper<int32_t>() const {
  return vosa::ArrayBase::INT32;
}

template <>
vosa::ArrayBase::DType vosa::Op::output_dtype_helper<float>() const {
  return vosa::ArrayBase::FLOAT32;
}

template <>
vosa::ArrayBase::DType vosa::Op::output_dtype_helper<double>() const {
  return vosa::ArrayBase::FLOAT64;
}

template <typename T>
T vosa::Op::unpack_value(const std::vector<uint8_t> &raw_value) {
  auto uint8_ptr = new uint8_t[sizeof(T)];
  std::copy(raw_value.begin(), raw_value.end(), uint8_ptr);
  T output = *((T *)uint8_ptr);
  delete[] uint8_ptr;
  return output;
}

template <typename T>
vosa::EigenVector<T>
vosa::Op::unpack_vector(const std::vector<uint8_t> &raw_value) {
  uint32_t num_elements = raw_value.size() / sizeof(T);
  auto output = vosa::EigenVector<T>(num_elements);
  for (uint32_t element_index = 0; element_index < num_elements;
       element_index++) {
    auto element_start = raw_value.begin() + element_index * sizeof(T);
    std::vector<uint8_t> element_input{element_start,
                                       element_start + sizeof(T)};
    output(element_index) = unpack_value<T>(element_input);
  }
  return output;
}

template <typename T>
vosa::EigenPlane<T>
vosa::Op::unpack_plane(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &plane_dimensions) {
  uint32_t height = plane_dimensions[0];
  uint32_t width = plane_dimensions[1];
  auto output = vosa::EigenPlane<T>(height, width);
  uint32_t element_index = 0;
  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      auto element_start = raw_value.begin() + element_index * sizeof(T);
      std::vector<uint8_t> element_input{element_start,
                                         element_start + sizeof(T)};
      output(height_index, width_index) = unpack_value<T>(element_input);
      element_index++;
    }
  }
  return output;
}

template <typename T>
vosa::EigenImage<T>
vosa::Op::unpack_image(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &image_dimensions) {
  uint32_t channels = image_dimensions[0];
  uint32_t height = image_dimensions[1];
  uint32_t width = image_dimensions[2];
  auto output = vosa::EigenImage<T>(channels, height, width);
  uint32_t element_index = 0;
  for (uint32_t channel_index = 0; channel_index < channels; channel_index++) {
    for (uint32_t height_index = 0; height_index < height; height_index++) {
      for (uint32_t width_index = 0; width_index < width; width_index++) {
        auto element_start = raw_value.begin() + element_index * sizeof(T);
        std::vector<uint8_t> element_input{element_start,
                                           element_start + sizeof(T)};
        output(channel_index, height_index, width_index) =
            unpack_value<T>(element_input);
        element_index++;
      }
    }
  }
  return output;
}

template uint8_t vosa::Op::unpack_value(const std::vector<uint8_t> &raw_value);

template uint16_t vosa::Op::unpack_value(const std::vector<uint8_t> &raw_value);

template uint32_t vosa::Op::unpack_value(const std::vector<uint8_t> &raw_value);

template int8_t vosa::Op::unpack_value(const std::vector<uint8_t> &raw_value);

template int16_t vosa::Op::unpack_value(const std::vector<uint8_t> &raw_value);

template int32_t vosa::Op::unpack_value(const std::vector<uint8_t> &raw_value);

template float vosa::Op::unpack_value(const std::vector<uint8_t> &raw_value);

template vosa::EigenVector<uint8_t>
vosa::Op::unpack_vector(const std::vector<uint8_t> &raw_value);

template vosa::EigenVector<uint16_t>
vosa::Op::unpack_vector(const std::vector<uint8_t> &raw_value);

template vosa::EigenVector<uint32_t>
vosa::Op::unpack_vector(const std::vector<uint8_t> &raw_value);

template vosa::EigenVector<int8_t>
vosa::Op::unpack_vector(const std::vector<uint8_t> &raw_value);

template vosa::EigenVector<int16_t>
vosa::Op::unpack_vector(const std::vector<uint8_t> &raw_value);

template vosa::EigenVector<int32_t>
vosa::Op::unpack_vector(const std::vector<uint8_t> &raw_value);

template vosa::EigenVector<float>
vosa::Op::unpack_vector(const std::vector<uint8_t> &raw_value);

template vosa::EigenPlane<uint8_t>
vosa::Op::unpack_plane(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &plane_dimensions);

template vosa::EigenPlane<uint16_t>
vosa::Op::unpack_plane(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &plane_dimensions);

template vosa::EigenPlane<uint32_t>
vosa::Op::unpack_plane(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &plane_dimensions);

template vosa::EigenPlane<int8_t>
vosa::Op::unpack_plane(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &plane_dimensions);

template vosa::EigenPlane<int16_t>
vosa::Op::unpack_plane(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &plane_dimensions);

template vosa::EigenPlane<int32_t>
vosa::Op::unpack_plane(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &plane_dimensions);

template vosa::EigenPlane<float>
vosa::Op::unpack_plane(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &plane_dimensions);

template vosa::EigenImage<bool>
vosa::Op::unpack_image(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &image_dimensions);

template vosa::EigenImage<uint8_t>
vosa::Op::unpack_image(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &image_dimensions);

template vosa::EigenImage<uint16_t>
vosa::Op::unpack_image(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &image_dimensions);

template vosa::EigenImage<uint32_t>
vosa::Op::unpack_image(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &image_dimensions);

template vosa::EigenImage<int8_t>
vosa::Op::unpack_image(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &image_dimensions);

template vosa::EigenImage<int16_t>
vosa::Op::unpack_image(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &image_dimensions);

template vosa::EigenImage<int32_t>
vosa::Op::unpack_image(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &image_dimensions);

template vosa::EigenImage<float>
vosa::Op::unpack_image(const std::vector<uint8_t> &raw_value,
                       const std::vector<uint32_t> &image_dimensions);

template <typename T>
std::vector<uint8_t> vosa::Op::pack_value(const T &value) {
  std::vector<uint8_t> output;
  auto uint8_t_ptr = new uint8_t[sizeof(T)];
  auto value_as_ptr = (uint8_t *)&value;
  std::copy(value_as_ptr, value_as_ptr + sizeof(T), uint8_t_ptr);
  output.insert(output.end(), uint8_t_ptr, uint8_t_ptr + sizeof(T));
  delete[] uint8_t_ptr;
  return output;
}

template <typename T>
std::vector<uint8_t>
vosa::Op::pack_vector(const vosa::EigenVector<T> &eigen_vector) {
  uint32_t vector_length = eigen_vector.size();
  std::vector<uint8_t> output;
  for (uint32_t vector_index = 0; vector_index < vector_length;
       vector_index++) {
    std::vector<uint8_t> single_value_packed =
        pack_value(eigen_vector(vector_index));
    output.insert(output.end(), single_value_packed.begin(),
                  single_value_packed.end());
  }
  return output;
}

template <typename T>
std::vector<uint8_t>
vosa::Op::pack_plane(const vosa::EigenPlane<T> &eigen_plane) {
  std::vector<uint8_t> output;
  uint32_t height = eigen_plane.dimension(0);
  uint32_t width = eigen_plane.dimension(1);
  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      std::vector<uint8_t> single_value_packed =
          pack_value(eigen_plane(height_index, width_index));
      output.insert(output.end(), single_value_packed.begin(),
                    single_value_packed.end());
    }
  }
  return output;
}

template <typename T>
std::vector<uint8_t>
vosa::Op::pack_image(const vosa::EigenImage<T> &eigen_image) {
  std::vector<uint8_t> output;
  uint32_t channels = eigen_image.dimension(0);
  uint32_t height = eigen_image.dimension(1);
  uint32_t width = eigen_image.dimension(2);
  for (uint32_t channel_index = 0; channel_index < channels; channel_index++) {
    for (uint32_t height_index = 0; height_index < height; height_index++) {
      for (uint32_t width_index = 0; width_index < width; width_index++) {
        std::vector<uint8_t> single_value_packed =
            pack_value(eigen_image(channel_index, height_index, width_index));
        output.insert(output.end(), single_value_packed.begin(),
                      single_value_packed.end());
      }
    }
  }
  return output;
}

template std::vector<uint8_t> vosa::Op::pack_value(const uint8_t &);

template std::vector<uint8_t> vosa::Op::pack_value(const uint16_t &);

template std::vector<uint8_t> vosa::Op::pack_value(const uint32_t &);

template std::vector<uint8_t> vosa::Op::pack_value(const int8_t &);

template std::vector<uint8_t> vosa::Op::pack_value(const int16_t &);

template std::vector<uint8_t> vosa::Op::pack_value(const int32_t &);

template std::vector<uint8_t> vosa::Op::pack_value(const float &);

template std::vector<uint8_t>
vosa::Op::pack_vector(const vosa::EigenVector<uint8_t> &);

template std::vector<uint8_t>
vosa::Op::pack_vector(const vosa::EigenVector<uint16_t> &);

template std::vector<uint8_t>
vosa::Op::pack_vector(const vosa::EigenVector<uint32_t> &);

template std::vector<uint8_t>
vosa::Op::pack_vector(const vosa::EigenVector<int8_t> &);

template std::vector<uint8_t>
vosa::Op::pack_vector(const vosa::EigenVector<int16_t> &);

template std::vector<uint8_t>
vosa::Op::pack_vector(const vosa::EigenVector<int32_t> &);

template std::vector<uint8_t>
vosa::Op::pack_vector(const vosa::EigenVector<float> &);

template std::vector<uint8_t>
vosa::Op::pack_plane(const vosa::EigenPlane<uint8_t> &);

template std::vector<uint8_t>
vosa::Op::pack_plane(const vosa::EigenPlane<uint16_t> &);

template std::vector<uint8_t>
vosa::Op::pack_plane(const vosa::EigenPlane<uint32_t> &);

template std::vector<uint8_t>
vosa::Op::pack_plane(const vosa::EigenPlane<int8_t> &);

template std::vector<uint8_t>
vosa::Op::pack_plane(const vosa::EigenPlane<int16_t> &);

template std::vector<uint8_t>
vosa::Op::pack_plane(const vosa::EigenPlane<int32_t> &);

template std::vector<uint8_t>
vosa::Op::pack_plane(const vosa::EigenPlane<float> &);

template std::vector<uint8_t>
vosa::Op::pack_image(const vosa::EigenImage<bool> &);

template std::vector<uint8_t>
vosa::Op::pack_image(const vosa::EigenImage<uint8_t> &);

template std::vector<uint8_t>
vosa::Op::pack_image(const vosa::EigenImage<uint16_t> &);

template std::vector<uint8_t>
vosa::Op::pack_image(const vosa::EigenImage<uint32_t> &);

template std::vector<uint8_t>
vosa::Op::pack_image(const vosa::EigenImage<int8_t> &);

template std::vector<uint8_t>
vosa::Op::pack_image(const vosa::EigenImage<int16_t> &);

template std::vector<uint8_t>
vosa::Op::pack_image(const vosa::EigenImage<int32_t> &);

template std::vector<uint8_t>
vosa::Op::pack_image(const vosa::EigenImage<float> &);
