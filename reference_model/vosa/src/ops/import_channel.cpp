/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "import_channel.h"

std::shared_ptr<vosa::Op> vosa::ImportChannelBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_import_plane = vosa_operator_union.AsIMPORT_CHANNEL();
  assert(serialized_import_plane);
  auto mode = serialized_import_plane->mode;
  auto serialized_attributes = &serialized_import_plane->attr;
  uint32_t stride = serialized_attributes->get()->stride;
  uint32_t offset = serialized_attributes->get()->offset;
  std::vector<uint32_t> shape = serialized_attributes->get()->shape;
  switch (mode) {
  case vosa_serialization::IMPORT_CHANNEL_MODE_D_I_UINT8_UINT8: {
    return std::make_shared<vosa::ImportChannel<uint8_t>>(stride, offset,
                                                          shape);
  }
  case vosa_serialization::IMPORT_CHANNEL_MODE_D_I_UINT16_UINT16: {
    return std::make_shared<vosa::ImportChannel<uint16_t>>(stride, offset,
                                                           shape);
  }
  case vosa_serialization::IMPORT_CHANNEL_MODE_D_I_UINT32_UINT32: {
    return std::make_shared<vosa::ImportChannel<uint32_t>>(stride, offset,
                                                           shape);
  }
  case vosa_serialization::IMPORT_CHANNEL_MODE_D_I_INT8_INT8: {
    return std::make_shared<vosa::ImportChannel<int8_t>>(stride, offset, shape);
  }
  case vosa_serialization::IMPORT_CHANNEL_MODE_D_I_INT16_INT16: {
    return std::make_shared<vosa::ImportChannel<int16_t>>(stride, offset,
                                                          shape);
  }
  case vosa_serialization::IMPORT_CHANNEL_MODE_D_I_INT32_INT32: {
    return std::make_shared<vosa::ImportChannel<int32_t>>(stride, offset,
                                                          shape);
  }
  case vosa_serialization::IMPORT_CHANNEL_MODE_D_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::ImportChannel<float>>(stride, offset, shape);
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::EigenImage<T> vosa::ImportChannel<T>::runme(const T *input) const {

  uint32_t height = shape[0];
  uint32_t width = shape[1];

  uint32_t value_index = 0;
  auto byte_input = (std::byte *)input;
  vosa::EigenImage<T> output{height, width, 1};
  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      uint32_t byte_offset = offset + value_index * stride;
      std::byte *byte_address = byte_input + byte_offset;
      auto T_address = (T *)byte_address;
      T value = *T_address;
      output(height_index, width_index, 0) = value;
      value_index++;
    }
  }

  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::ImportChannel<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::ImportChannel<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const T *input = inputs[0]->template get_data<T>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::IMPORT_CHANNEL_MODE
vosa::ImportChannel<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::IMPORT_CHANNEL_MODE_D_I_UINT8_UINT8;
}

template <>
vosa_serialization::IMPORT_CHANNEL_MODE
vosa::ImportChannel<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::IMPORT_CHANNEL_MODE_D_I_UINT16_UINT16;
}

template <>
vosa_serialization::IMPORT_CHANNEL_MODE
vosa::ImportChannel<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::IMPORT_CHANNEL_MODE_D_I_UINT32_UINT32;
}

template <>
vosa_serialization::IMPORT_CHANNEL_MODE
vosa::ImportChannel<int8_t>::get_serialization_mode() const {
  return vosa_serialization::IMPORT_CHANNEL_MODE_D_I_INT8_INT8;
}

template <>
vosa_serialization::IMPORT_CHANNEL_MODE
vosa::ImportChannel<int16_t>::get_serialization_mode() const {
  return vosa_serialization::IMPORT_CHANNEL_MODE_D_I_INT16_INT16;
}

template <>
vosa_serialization::IMPORT_CHANNEL_MODE
vosa::ImportChannel<int32_t>::get_serialization_mode() const {
  return vosa_serialization::IMPORT_CHANNEL_MODE_D_I_INT32_INT32;
}

template <>
vosa_serialization::IMPORT_CHANNEL_MODE
vosa::ImportChannel<float>::get_serialization_mode() const {
  return vosa_serialization::IMPORT_CHANNEL_MODE_D_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::ImportChannel<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {
  auto import_plane_attribute =
      std::make_unique<vosa_serialization::IMPORT_CHANNEL_ATTRIBUTET>();
  import_plane_attribute->stride = stride;
  import_plane_attribute->offset = offset;
  import_plane_attribute->shape = shape;

  vosa_serialization::IMPORT_CHANNELT import_plane;
  import_plane.mode = get_serialization_mode();
  import_plane.attr = std::move(import_plane_attribute);

  auto import_plane_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  import_plane_operator->op.Set(std::move(import_plane));
  import_plane_operator->name = op_name;
  import_plane_operator->inputs = input_names;
  return import_plane_operator;
}

template class vosa::ImportChannel<int8_t>;

template class vosa::ImportChannel<int16_t>;

template class vosa::ImportChannel<int32_t>;

template class vosa::ImportChannel<uint8_t>;

template class vosa::ImportChannel<uint16_t>;

template class vosa::ImportChannel<uint32_t>;

template class vosa::ImportChannel<float>;
