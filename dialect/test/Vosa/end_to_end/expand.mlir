// run command without FileCheck to see output (or add "--dump-input always" FileCheck option)
// RUN: vosa-opt -vosa-to-tosa %s | %test_src_root/e2e-runner | FileCheck %s

func.func @main() {
    %input_f32 = arith.constant dense<
      [[[   1.,   10.,  100.],
        [   2.,   20.,  200.],
        [   3.,   30.,  300.],
        [   4.,   40.,  400.]],

       [[   5.,   50.,  500.],
        [   6.,   60.,  600.],
        [   7.,   70.,  700.],
        [   8.,   80.,  800.]],

       [[   9.,   90.,  900.],
        [  10.,  100., 1000.],
        [  11.,  110., 1100.],
        [  12.,  120., 1200.]],

       [[  13.,  130., 1300.],
        [  14.,  140., 1400.],
        [  15.,  150., 1500.],
        [  16.,  160., 1600.]]]> : tensor<4x4x3xf32>
    %input_si32 = vosa.constant dense<
      [[[   1,   10,  100],
        [   2,   20,  200],
        [   3,   30,  300],
        [   4,   40,  400]],

       [[   5,   50,  500],
        [   6,   60,  600],
        [   7,   70,  700],
        [   8,   80,  800]],

       [[   9,   90,  900],
        [  10,  100, 1000],
        [  11,  110, 1100],
        [  12,  120, 1200]],

       [[  13,  130, 1300],
        [  14,  140, 1400],
        [  15,  150, 1500],
        [  16,  160, 1600]]]> : tensor<4x4x3xsi32>

    %dyn_input_f32 = tensor.cast %input_f32 : tensor<4x4x3xf32> to tensor<?x?x?xf32>
    func.call @print_img_f32(%dyn_input_f32) : (tensor<?x?x?xf32>) -> ()
    
    %expand_kernel = vosa.constant dense<[[[0], [1], [2]], [[1], [0], [0]]]> : tensor<2x3x1xui8>

    // CHECK: sizes = [4, 6]
    // CHECK: [0,    2,    5,    0,    4,    7]
    // CHECK: [2,    0,    0,    4,    0,    0],
    // CHECK: [0,    10,    13,    0,    12,    15]
    // CHECK: [10,    0,    0,    12,    0,    0]
    // CHECK: sizes = [4, 6]
    // CHECK:  [0,    20,    50,    0,    40,    70]
    // CHECK: [20,    0,    0,    40,    0,    0]
    // CHECK: [0,    100,    130,    0,    120,    150] 
    // CHECK: [100,    0,    0,    120,    0,    0]
    // CHECK: sizes = [4, 6]
    // CHECK: [0,    200,    500,    0,    400,    700]
    // CHECK: [200,    0,    0,    400,    0,    0]
    // CHECK: [0,    1000,    1300,    0,    1200,    1500]
    // CHECK: [1000,    0,    0,    1200,    0,    0]
    %expanded_f32 = vosa.expand %input_f32, %expand_kernel {gather_kernel = dense<[[0, 1], [2, 0]]> : tensor<2x2xui8>, fill_value = 0.0 : f32} : tensor<4x4x3xf32>, tensor<2x3x1xui8> -> tensor<4x6x3xf32>

    %dyn_expanded_f32 = tensor.cast %expanded_f32 : tensor<4x6x3xf32> to tensor<?x?x?xf32>
    func.call @print_img_f32(%dyn_expanded_f32) : (tensor<?x?x?xf32>) -> ()

    // CHECK: sizes = [4, 6]
    // CHECK: [0,    2,    5,    0,    4,    7]
    // CHECK: [2,    0,    0,    4,    0,    0],
    // CHECK: [0,    10,    13,    0,    12,    15]
    // CHECK: [10,    0,    0,    12,    0,    0]
    // CHECK: sizes = [4, 6]
    // CHECK:  [0,    20,    50,    0,    40,    70]
    // CHECK: [20,    0,    0,    40,    0,    0]
    // CHECK: [0,    100,    130,    0,    120,    150] 
    // CHECK: [100,    0,    0,    120,    0,    0]
    // CHECK: sizes = [4, 6]
    // CHECK: [0,    200,    500,    0,    400,    700]
    // CHECK: [200,    0,    0,    400,    0,    0]
    // CHECK: [0,    1000,    1300,    0,    1200,    1500]
    // CHECK: [1000,    0,    0,    1200,    0,    0]
    %expanded_si32 = vosa.expand %input_si32, %expand_kernel {gather_kernel = dense<[[0, 1], [2, 0]]> : tensor<2x2xui8>, fill_value = 0: si32} : tensor<4x4x3xsi32>, tensor<2x3x1xui8> -> tensor<4x6x3xsi32>
    %expanded_i32 = builtin.unrealized_conversion_cast %expanded_si32 : tensor<4x6x3xsi32> to tensor<4x6x3xi32>
    %dyn_expanded_i32 = tensor.cast %expanded_i32 : tensor<4x6x3xi32> to tensor<?x?x?xi32>
    func.call @print_img_i32(%dyn_expanded_i32) : (tensor<?x?x?xi32>) -> ()

    %c1 = vosa.constant dense<0> : tensor<540x960x1xui32>
    %c2 = vosa.constant dense<1> : tensor<540x960x1xui32>
    %img = vosa.concat %c1, %c2 : tensor<540x960x1xui32>, tensor<540x960x1xui32> -> tensor<540x960x2xui32>
    %cst1 = vosa.constant dense<1> : tensor<1x1x1xui8>
    %exp = vosa.broadcast_planewise %cst1 {size = [2 : i32, 2 : i32]} : tensor<1x1x1xui8> -> tensor<2x2x1xui8>
    %exp_img = vosa.expand %img, %exp {fill_value = 0 : ui32, gather_kernel = dense<1> : tensor<1x1xui8>} : tensor<540x960x2xui32>, tensor<2x2x1xui8> -> tensor<1080x1920x2xui32>
    %exp_img_i32 = builtin.unrealized_conversion_cast %exp_img : tensor<1080x1920x2xui32> to tensor<1080x1920x2xi32>
    %top_corner = tensor.extract_slice %exp_img_i32[0, 0, 0][4, 4, 2][1, 1, 1] : tensor<1080x1920x2xi32> to tensor<4x4x2xi32>
    %dyn_exp_img_i32 = tensor.cast %top_corner : tensor<4x4x2xi32> to tensor<?x?x?xi32>
    func.call @print_img_i32(%dyn_exp_img_i32) : (tensor<?x?x?xi32>) -> ()

    return
}


func.func @print_img_i32(%a : tensor<?x?x?xi32>) {
  // %unranked_all = tensor.cast %a : tensor<?x?x?xi32> to tensor<*xi32>
  // func.call @print_memref_i32(%unranked_all) : (tensor<*xi32>) -> ()
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c0 : tensor<?x?x?xi32>
  %w = tensor.dim %a, %c1 : tensor<?x?x?xi32>
  %nch = tensor.dim %a, %c2 : tensor<?x?x?xi32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[0, 0, %c][%h, %w, 1][1, 1, 1]  : tensor<?x?x?xi32> to tensor<?x?x1xi32>
     %chdata_c = tensor.collapse_shape %chdata [[0], [1, 2]] : tensor<?x?x1xi32> into tensor<?x?xi32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xi32> to tensor<*xi32>
     func.call @printMemrefI32(%unranked_chdata) : (tensor<*xi32>) -> ()
  }
  return
}

func.func @print_img_f32(%a : tensor<?x?x?xf32>) {
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c0 : tensor<?x?x?xf32>
  %w = tensor.dim %a, %c1 : tensor<?x?x?xf32>
  %nch = tensor.dim %a, %c2 : tensor<?x?x?xf32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[0, 0, %c][%h, %w, 1][1, 1, 1]  : tensor<?x?x?xf32> to tensor<?x?x1xf32>
     %chdata_c = tensor.collapse_shape %chdata [[0], [1, 2]] : tensor<?x?x1xf32> into tensor<?x?xf32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xf32> to tensor<*xf32>
     func.call @printMemrefF32(%unranked_chdata) : (tensor<*xf32>) -> ()
  }
  return
}

func.func private @printMemrefI32(%ptr : tensor<*xi32> {bufferization.access = "read"})
func.func private @printMemrefF32(%ptr : tensor<*xf32> {bufferization.access = "read"})
