/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "ops/abs.h"
#include "test_bench.h"
#include "vosa/vosa.h"
#include "gtest/gtest.h"

vosa::ArrayBase make_test_input(vosa::ArrayBase::DType dtype) {
  switch (dtype) {
  case vosa::ArrayBase::INT8: {
    auto test_image = vosa::EigenImage<int8_t>(1, 1, 1);
    test_image.setConstant(-1);
    return vosa::ArrayBase(test_image);
  }
  case vosa::ArrayBase::INT16: {
    auto test_image = vosa::EigenImage<int16_t>(1, 1, 1);
    test_image.setConstant(-1);
    return vosa::ArrayBase(test_image);
  }
  case vosa::ArrayBase::INT32: {
    auto test_image = vosa::EigenImage<int32_t>(1, 1, 1);
    test_image.setConstant(-1);
    return vosa::ArrayBase(test_image);
  }
  case vosa::ArrayBase::FLOAT32: {
    auto test_image = vosa::EigenImage<float>(1, 1, 1);
    test_image.setConstant(-1);
    return vosa::ArrayBase(test_image);
  }
  default: {
    throw std::runtime_error("");
  }
  }
}

bool check_test_output(vosa::ArrayBase::DType dtype,
                       const std::shared_ptr<vosa::ArrayBase> &output_1,
                       const std::shared_ptr<vosa::ArrayBase> &output_2) {
  switch (dtype) {
  case vosa::ArrayBase::DType::INT8: {
    auto original_output_image = output_1->get_image<int8_t>();
    auto deserialized_output_image = output_2->get_image<int8_t>();

    return vosa::test::TestBench<int8_t, int32_t>::compareEigenImages(
        original_output_image, deserialized_output_image);
  }
  case vosa::ArrayBase::DType::INT16: {
    auto original_output_image = output_1->get_image<int16_t>();
    auto deserialized_output_image = output_2->get_image<int16_t>();

    return vosa::test::TestBench<int16_t, int32_t>::compareEigenImages(
        original_output_image, deserialized_output_image);
  }
  case vosa::ArrayBase::DType::INT32: {
    auto original_output_image = output_1->get_image<int32_t>();
    auto deserialized_output_image = output_2->get_image<int32_t>();

    return vosa::test::TestBench<int32_t, int32_t>::compareEigenImages(
        original_output_image, deserialized_output_image);
  }
  case vosa::ArrayBase::DType::FLOAT32: {
    auto original_output_image = output_1->get_image<float>();
    auto deserialized_output_image = output_2->get_image<float>();

    return vosa::test::TestBench<float, int32_t>::compareEigenImages(
        original_output_image, deserialized_output_image);
  }
  default: {
    throw std::runtime_error("");
  }
  }
}

std::shared_ptr<vosa::AbsBase>
make_abs_ops(const vosa::ArrayBase::DType &dtype) {
  switch (dtype) {
  case vosa::ArrayBase::INT8:
    return std::make_shared<vosa::Abs<int8_t>>();
  case vosa::ArrayBase::INT16:
    return std::make_shared<vosa::Abs<int16_t>>();
  case vosa::ArrayBase::INT32:
    return std::make_shared<vosa::Abs<int32_t>>();
  case vosa::ArrayBase::FLOAT32:
    return std::make_shared<vosa::Abs<float>>();
  default:
    throw std::logic_error("");
  }
}

TEST(test_serialization, test_abs) {
  vosa::Graph original_graph;

  std::vector<vosa::ArrayBase::DType> input_types = {
      vosa::ArrayBase::INT8, vosa::ArrayBase::INT16, vosa::ArrayBase::INT32,
      vosa::ArrayBase::FLOAT32};
  std::vector<std::string> abs_names;
  std::unordered_map<std::string, vosa::ArrayBase> test_inputs;

  for (const auto &type : input_types) {
    auto input = std::make_shared<vosa::Input>(type);
    original_graph.add_op(input, std::vector<std::string>{});
    auto abs = make_abs_ops(type);
    original_graph.add_op(abs, {input});
    test_inputs[original_graph.get_name_for_op(input)] = make_test_input(type);
  }

  auto serialized_graph = original_graph.serialize();
  auto deserialized_graph = vosa::Graph(serialized_graph);

  auto original_output = original_graph.execute(test_inputs, abs_names);
  auto deserialized_output = deserialized_graph.execute(test_inputs, abs_names);

  EXPECT_EQ(original_output.size(), deserialized_output.size());
  for (uint32_t output_index = 0; output_index < original_output.size();
       output_index++) {

    auto test_output = check_test_output(input_types[output_index],
                                         original_output[output_index],
                                         deserialized_output[output_index]);
    EXPECT_TRUE(test_output);
  }
}