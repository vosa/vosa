/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_CUSTOM_SCALAR_H
#define VOSA_CUSTOM_SCALAR_H

#include "vosa/op.h"

namespace vosa {

class CustomScalarBase : public vosa::Op {
public:
  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

  static std::shared_ptr<Op>
  parse_serialized(const vosa_serialization::VosaOperatorUnion &);

  void set_op_name(const std::string &op_name) { custom_op_name = op_name; }

protected:
  std::string custom_op_name;
};

template <typename inputT, typename outputT>
class CustomScalar : public CustomScalarBase {
public:
  virtual outputT kernel(std::vector<inputT> &inputs) const = 0;

  [[nodiscard]] std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &inputs) const final {

    std::vector<inputT> input_scalars;
    for (const auto &input : inputs) {
      vosa::EigenScalar<inputT> input_eigen_scalar =
          input->template get_scalar<inputT>();
      inputT input_scalar = input_eigen_scalar();
      input_scalars.push_back(input_scalar);
    }
    outputT output_scalar = kernel(input_scalars);
    vosa::EigenScalar<outputT> output_eigen_scalar;
    output_eigen_scalar.setConstant(output_scalar);
    auto output = std::make_shared<vosa::ArrayBase>(output_eigen_scalar);
    return output;
  }

  [[nodiscard]] vosa::ArrayBase::DType output_dtype() const final {
    return output_dtype_helper<outputT>();
  }
};

} // namespace vosa

#endif // VOSA_CUSTOM_SCALAR_H
