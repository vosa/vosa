/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "resize_bilinear.h"
#include "../data_utils.h"
#include <algorithm>

std::shared_ptr<vosa::Op> vosa::ResizeBilinearBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {

  auto serialized_resize_bilinear = vosa_operator_union.AsRESIZE_BILINEAR();
  assert(serialized_resize_bilinear);
  auto mode = serialized_resize_bilinear->mode;
  auto serialized_attributes = &serialized_resize_bilinear->attr;
  std::vector<uint32_t> resize_size = serialized_attributes->get()->size;
  switch (mode) {
  case vosa_serialization::RESIZE_BILINEAR_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::ResizeBilinear<float>>(resize_size);
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::ResizeBilinear<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

float inp(float x, float size_ratio) { return (x + 0.5f) * size_ratio - 0.5f; }

int32_t ul(float x, float size_ratio) { return floor(inp(x, size_ratio)); }

template <typename T>
vosa::ResizeBilinear<T>::ResizeBilinear(
    const std::vector<uint32_t> &output_size) {
  assert(output_size.size() == 2);
  this->output_size =
      vosa::utils::DataUtils<uint32_t>::vectorFromCPP(output_size);
}

template <typename T>
vosa::EigenImage<T>
vosa::ResizeBilinear<T>::runme(const EigenImage<T> &input) const {

  uint32_t input_height = input.dimension(0);
  uint32_t input_width = input.dimension(1);

  vosa::EigenImage<T> output{output_size[0], output_size[1],
                             input.dimension(2)};

  auto input_height_f = (float)input_height;
  auto input_width_f = (float)input_width;

  auto output_height_f = (float)output_size[0];
  auto output_width_f = (float)output_size[1];

  float height_ratio = input_height_f / output_height_f;
  float width_ratio = input_width_f / output_width_f;

  for (uint32_t height_index = 0; height_index < output_size[0];
       height_index++) {
    for (uint32_t width_index = 0; width_index < output_size[1];
         width_index++) {
      for (uint32_t channel_index = 0; channel_index < input.dimension(2);
           channel_index++) {

        auto x = (float)width_index;
        auto y = (float)height_index;

        float inp_x = inp(x, width_ratio);
        float inp_y = inp(y, height_ratio);

        auto ul_x_f = (float)ul(x, width_ratio);
        auto ul_y_f = (float)ul(y, height_ratio);

        float l_x_weight = (1.0f - inp_x + ul_x_f);
        float r_x_weight = (inp_x - ul_x_f);

        float u_y_weight = (1.0f - inp_y + ul_y_f);
        float b_y_weight = (inp_y - ul_y_f);

        float ul_weight = u_y_weight * l_x_weight;
        float ur_weight = u_y_weight * r_x_weight;
        float bl_weight = b_y_weight * l_x_weight;
        float br_weight = b_y_weight * r_x_weight;

        float upper_left_value, upper_right_value, below_left_value,
            below_right_value;

        int32_t ul_x = ul(x, width_ratio);
        int32_t ul_y = ul(y, height_ratio);

        int32_t ul_x_p1 = ul_x + 1;
        int32_t ul_y_p1 = ul_y + 1;

        int32_t ul_x_safe = std::max(ul_x, 0);
        int32_t ul_y_safe = std::max(ul_y, 0);

        int32_t ul_x_p1_safe = std::min(ul_x_p1, (int32_t)input_width - 1);
        int32_t ul_y_p1_safe = std::min(ul_y_p1, (int32_t)input_height - 1);

        upper_left_value = input(ul_y_safe, ul_x_safe, channel_index);
        upper_right_value = input(ul_y_safe, ul_x_p1_safe, channel_index);
        below_left_value = input(ul_y_p1_safe, ul_x_safe, channel_index);
        below_right_value = input(ul_y_p1_safe, ul_x_p1_safe, channel_index);

        float output_value =
            ul_weight * upper_left_value + ur_weight * upper_right_value +
            bl_weight * below_left_value + br_weight * below_right_value;

        output(height_index, width_index, channel_index) = output_value;
      }
    }
  }

  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::ResizeBilinear<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <>
vosa_serialization::RESIZE_BILINEAR_MODE
vosa::ResizeBilinear<float>::get_serialization_mode() const {
  return vosa_serialization::RESIZE_BILINEAR_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::ResizeBilinear<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {
  auto resize_bilinear_attribute =
      std::make_unique<vosa_serialization::RESIZE_BILINEAR_ATTRIBUTET>();
  resize_bilinear_attribute->size = {output_size(0), output_size(1)};

  vosa_serialization::RESIZE_BILINEART resize_bilinear;
  resize_bilinear.mode = get_serialization_mode();
  resize_bilinear.attr = std::move(resize_bilinear_attribute);

  auto resize_bilinear_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  resize_bilinear_operator->op.Set(std::move(resize_bilinear));
  resize_bilinear_operator->name = op_name;
  resize_bilinear_operator->inputs = input_names;
  return resize_bilinear_operator;
}

template class vosa::ResizeBilinear<float>;