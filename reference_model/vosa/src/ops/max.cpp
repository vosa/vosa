/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "max.h"

using namespace std;
using namespace vosa;
using namespace vosa_serialization;

// Template instantiation
template class vosa::Max<uint8_t>;

template class vosa::Max<uint16_t>;

template class vosa::Max<uint32_t>;

template class vosa::Max<int8_t>;

template class vosa::Max<int16_t>;

template class vosa::Max<int32_t>;

template class vosa::Max<float>;

shared_ptr<Op>
MaxBase::parse_serialized(const VosaOperatorUnion &vosa_operator_union) {
  auto serialized_max = vosa_operator_union.AsMAX_();
  switch (serialized_max->mode) {
  case MAX__MODE_I_I_UINT8_UINT8:
    return make_shared<Max<uint8_t>>();
  case MAX__MODE_I_I_UINT16_UINT16:
    return make_shared<Max<uint16_t>>();
  case MAX__MODE_I_I_UINT32_UINT32:
    return make_shared<Max<uint32_t>>();
  case MAX__MODE_I_I_INT8_INT8:
    return make_shared<Max<int8_t>>();
  case MAX__MODE_I_I_INT16_INT16:
    return make_shared<Max<int16_t>>();
  case MAX__MODE_I_I_INT32_INT32:
    return make_shared<Max<int32_t>>();
  case MAX__MODE_I_I_FLOAT32_FLOAT32:
    return make_shared<Max<float>>();
  default:
    throw OperatorModeNotImplementedError();
  }
}

template <typename T>
shared_ptr<ArrayBase>
Max<T>::eval(const vector<shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input_0 = inputs[0]->template get_image<T>();
  const EigenImage<T> &input_1 = inputs[1]->template get_image<T>();

  return make_shared<ArrayBase>(this->runme(input_0, input_1));
}

template <typename T>
EigenImage<T> Max<T>::runme(const EigenImage<T> &input1,
                            const EigenImage<T> &input2) const {

  const uint32_t height = input1.dimension(0);
  const uint32_t width = input1.dimension(1);
  const uint32_t channels = input1.dimension(2);

  assert(height == input2.dimension(0));
  assert(width == input2.dimension(1));
  assert(channels == input2.dimension(2));

  EigenImage<T> output(height, width, channels);

  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      for (uint32_t c = 0; c < channels; c++)
        output(y, x, c) = std::max(input1(y, x, c), input2(y, x, c));

  return output;
}

template <typename T>
ArrayBase::DType Max<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
unique_ptr<VosaGraphOperatorT>
Max<T>::serialize(const string &op_name,
                  const vector<string> &input_names) const {
  MAX_T max;
  max.mode = get_serialization_mode();

  auto max_operator = make_unique<VosaGraphOperatorT>();
  max_operator->op.Set(max);
  max_operator->name = op_name;
  max_operator->inputs = input_names;

  return max_operator;
}

template <>
MAX__MODE vosa::Max<uint8_t>::get_serialization_mode() const {
  return MAX__MODE_I_I_UINT8_UINT8;
}

template <>
MAX__MODE vosa::Max<uint16_t>::get_serialization_mode() const {
  return MAX__MODE_I_I_UINT16_UINT16;
}

template <>
MAX__MODE vosa::Max<uint32_t>::get_serialization_mode() const {
  return MAX__MODE_I_I_UINT32_UINT32;
}

template <>
MAX__MODE vosa::Max<int8_t>::get_serialization_mode() const {
  return MAX__MODE_I_I_INT8_INT8;
}

template <>
MAX__MODE vosa::Max<int16_t>::get_serialization_mode() const {
  return MAX__MODE_I_I_INT16_INT16;
}

template <>
MAX__MODE vosa::Max<int32_t>::get_serialization_mode() const {
  return MAX__MODE_I_I_INT32_INT32;
}

template <>
MAX__MODE vosa::Max<float>::get_serialization_mode() const {
  return MAX__MODE_I_I_FLOAT32_FLOAT32;
}
