"""
License
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import sys
import argparse
import typing
import logging


class Image:
    def __init__(
        self, ppm_type: str, width: int, height: int, maxval: int, raw_data: bytes
    ):
        self.__ppm_type = ppm_type
        self.__width = width
        self.__height = height
        self.__maxval = maxval
        self.__raw_data = raw_data

    def __eq__(self, other):
        if self.__ppm_type != other.__ppm_type:
            logging.error("Non Matching PPM Type")
            return False
        if self.__width != other.__width:
            logging.error("Non Matching Width")
            return False
        if self.__height != other.__height:
            logging.error("Non Matching Height")
            return False
        if self.__maxval != other.__maxval:
            logging.error("Non Matching MaxVal")
            return False
        if self.__raw_data != other.__raw_data:
            logging.error("Non Matching Data")
            return False
        return True


def read_ppm_or_pgm(image_filename: str) -> typing.Union[Image, None]:
    try:
        with open(image_filename, "rb") as fp:
            file_data = fp.read()
            file_data = file_data.split(b"\n")
            ppm_type = file_data[0].decode("utf-8")
            dimensions_string = file_data[1].decode("utf-8")
            dimensions_strings = dimensions_string.split(" ")
            width = int(dimensions_strings[0])
            height = int(dimensions_strings[1])
            maxval_string = file_data[2].decode("utf-8")
            maxval = int(maxval_string)
            return Image(ppm_type, width, height, maxval, file_data[3])
    except:
        return None


def compare():
    pass


def main():
    argument_one = "first_image"
    argument_two = "second_image"
    parser = argparse.ArgumentParser(
        prog="test_sample_outputs",
        description="Compares two ppm/pgm files for equality",
    )
    parser.add_argument(argument_one)
    parser.add_argument(argument_two)
    args = parser.parse_args()
    args = vars(args)
    first_image = read_ppm_or_pgm(args[argument_one])
    if not first_image:
        return False
    second_image = read_ppm_or_pgm(args[argument_two])
    if not second_image:
        return False
    if first_image != second_image:
        return False
    return True


if __name__ == "__main__":
    if main():
        sys.exit(0)
    else:
        sys.exit(1)
