// RUN: vosa-opt -split-input-file -verify-diagnostics %s | FileCheck %s

// CHECK-LABEL: func.func @conv_2d()
func.func @conv_2d() {
    // integer types are preserved
    %t_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
    // CHECK: %{{.*}} = vosa.conv_2d %{{.*}} {filter = {{.*}}} : tensor<100x200x3xsi8> -> tensor<98x198x3xsi8>
    %res_si8 = vosa.conv_2d %t_si8 { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi8> } : tensor<100x200x3xsi8> -> tensor<98x198x3xsi8>

    %t_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
    // CHECK: %{{.*}} = vosa.conv_2d %{{.*}} {filter = {{.*}}} : tensor<100x200x3xsi16> -> tensor<98x198x3xsi16>
    %res_si16 = vosa.conv_2d %t_si16 { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi16> } : tensor<100x200x3xsi16> -> tensor<98x198x3xsi16>

    %t_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
    // CHECK: %{{.*}} = vosa.conv_2d %{{.*}} {filter = {{.*}}} : tensor<100x200x3xsi32> -> tensor<98x198x3xsi32>
    %res_si32 = vosa.conv_2d %t_si32 { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi32> } : tensor<100x200x3xsi32> -> tensor<98x198x3xsi32>

    // FP 16 is not upgraded
    %t_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
    // CHECK: %{{.*}} = vosa.conv_2d %{{.*}} {filter = {{.*}}} : tensor<100x200x3xf16> -> tensor<98x198x3xf16>
    %res_f16 = vosa.conv_2d %t_f16 { filter = dense<[[0.125, 0.25, 0.125], [0.25, 1.0, 0.25], [0.125, 0.25, 0.125]]> : tensor<3x3xf16> } : tensor<100x200x3xf16> -> tensor<98x198x3xf16>

    %t_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
    // CHECK: %{{.*}} = vosa.conv_2d %{{.*}} {filter = {{.*}}} : tensor<100x200x3xf32> -> tensor<98x198x3xf32>
    %res_f32 = vosa.conv_2d %t_f32 { filter = dense<[[0.125, 0.25, 0.125], [0.25, 1.0, 0.25], [0.125, 0.25, 0.125]]> : tensor<3x3xf32> } : tensor<100x200x3xf32> -> tensor<98x198x3xf32>

    return
}

// CHECK-LABEL: func.func @conv_2d_any(%{{.*}})
func.func @conv_2d_any(%t : tensor<?x?x?xsi8>) {
    // CHECK: %{{.*}} = vosa.conv_2d %{{.*}}  {filter = {{.*}}} : tensor<?x?x?xsi8> -> tensor<?x?x?xsi8>
    %res0 = vosa.conv_2d %t { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi8> } : tensor<?x?x?xsi8> -> tensor<?x?x?xsi8>
    return
}

// -----

func.func @conv_2d_filter_type_mismatch_f16() {
    %t = vosa.constant dense<42.0> : tensor<100x200x3xf16>
    // expected-error @below {{'vosa.conv_2d' op filter and output type don't match}}
    %res0 = vosa.conv_2d %t { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi32> } : tensor<100x200x3xf16> -> tensor<98x198x3xf32>
    return
}

// -----

func.func @conv_2d_output_type_mismatch_si8() {
    %t = vosa.constant dense<42> : tensor<100x200x3xsi8>
    // expected-error @below {{'vosa.conv_2d' op filter and output type don't match}}
    %res0 = vosa.conv_2d %t { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi8> } : tensor<100x200x3xsi8> -> tensor<98x198x3xsi16>
    return
}

// -----

func.func @conv_2d_output_type_mismatch_f16() {
    %t = vosa.constant dense<42.0> : tensor<100x200x3xf16>
    // expected-error @below {{'vosa.conv_2d' op filter and output type don't match}}
    %res0 = vosa.conv_2d %t { filter = dense<[[0.125, 0.25, 0.125], [0.25, 1.0, 0.25], [0.125, 0.25, 0.125]]> : tensor<3x3xf16> } : tensor<100x200x3xf16> -> tensor<98x198x3xf32>
    return
}

// -----

func.func @conv_2d_output_size_mismatch_si8() {
    %t = vosa.constant dense<42> : tensor<100x200x3xsi8>
    // expected-error @below {{'vosa.conv_2d' op invalid output shape [97, 198, 3], expected [98, 198, 3]}}
    %res0 = vosa.conv_2d %t { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi8> } : tensor<100x200x3xsi8> -> tensor<97x198x3xsi8>
    return
}

