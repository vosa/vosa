/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <vector>

#include "custom.h"
#include "ops/custom_scalar.h"
#include "vosa/vosa.h"

template <typename inputT, typename outputT>
class TimesFourCustomScalar : public vosa::CustomScalar<inputT, outputT> {
  outputT kernel(std::vector<inputT> &inputs) const final {
    return 4 * inputs[0];
  }
};

template class TimesFourCustomScalar<float, float>;

std::shared_ptr<vosa::Op> times_four() {
  auto t4 = std::make_shared<TimesFourCustomScalar<float, float>>();
  return std::dynamic_pointer_cast<vosa::Op>(t4);
}

extern "C" vosa::CustomScalarRegistrar *get_registrar() {
  auto custom_registar = new vosa::CustomScalarRegistrar();
  custom_registar->add_op("times_four", &times_four);
  return custom_registar;
}