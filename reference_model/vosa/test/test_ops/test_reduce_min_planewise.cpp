/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/reduce_min_planewise.h"
#include "test_bench.h"
#include "vosa/vosa.h"

using namespace std;
using namespace vosa;

template <typename T>
static T reduce_min_planewise(EigenImage<T> &input) {
  auto Op = ReduceMinPlanewise<T>();

  EigenScalar<T> S_min =
      Op.eval({make_shared<ArrayBase>(input)})->template get_scalar<T>();
  return S_min();
}

TEST(test_reduce_min_planewise, test_reduce_min_planewise) {
  auto input1 = EigenImage<uint8_t>(4, 4, 1);
  input1.setValues({{{50}, {127}, {128}, {255}},
                    {{127}, {127}, {127}, {127}},
                    {{54}, {5}, {30}, {6}},
                    {{255}, {64}, {3}, {4}}});
  EXPECT_TRUE(reduce_min_planewise(input1) == (uint8_t)3);

  auto input2 = EigenImage<uint32_t>(2, 2, 1);
  input2.setValues({{{127}, {254}}, {{255}, {382}}});

  EXPECT_TRUE(reduce_min_planewise(input2) == (uint32_t)127);
}