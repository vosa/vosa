/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <vector>

#include "vosa/vosa.h"

#include "flatbuffer_utils.h"
#include "ppm_utils.h"

int main(int argc, char *argv[]) {

  std::string input_pgm(argv[1]);
  std::string output_pgm(argv[2]);
  std::string ref_model_flatbuffer(argv[3]);

  std::vector<uint8_t> input;
  int input_width, input_height;
  auto input_success = vosa::api::ppm::read_input<uint8_t>(
      input_pgm, input, input_width, input_height);
  if (!input_success)
    return 1;

  auto flatbuffer_data = load_flatbuffer(ref_model_flatbuffer);
  auto graph = vosa::Graph((uint8_t *)flatbuffer_data.get());

  auto default_inputs = graph.get_default_inputs();
  auto default_outputs = graph.get_default_outputs();

  vosa::EigenImage<uint8_t> input_image{input_height, input_width, 1};
  std::memcpy(input_image.data(), input.data(), input_width * input_height);

  std::unordered_map<std::string, vosa::ArrayBase> graph_inputs = {
      {default_inputs[0], vosa::ArrayBase(input_image)}};
  auto outputs = graph.execute(graph_inputs, default_outputs);
  auto output_image = outputs[0]->get_image<uint8_t>();

  std::vector<uint8_t> output(input_height * input_width);
  std::memcpy(output.data(), output_image.data(), input_height * input_width);
  auto output_success =
      vosa::api::ppm::write_output(output_pgm, output, 1920, 1080, 1);
  if (!output_success)
    return 1;

  return 0;
}