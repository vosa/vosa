// RUN: vosa-opt -split-input-file -verify-diagnostics %s | FileCheck %s

module {
    // CHECK-LABEL: func.func @add()
    func.func @add() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // CHECK: %{{.*}} = vosa.add %{{.*}}, %{{.*}} : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>
        %res0 = vosa.add %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>

        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        %t1_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        // CHECK: %{{.*}} = vosa.add %{{.*}}, %{{.*}} : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>
        %res1 = vosa.add %t0_si16, %t1_si16 : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>

        %t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        %t1_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        // CHECK: %{{.*}} = vosa.add %{{.*}}, %{{.*}} : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>
        %res2 = vosa.add %t0_f32, %t1_f32 : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>

        return
    }

    func.func @add_any(%t0_f32: tensor<3x?x?xf32>, %t1_f32: tensor<3x?x?xf32>) {
        // CHECK: %{{.*}} = vosa.add %{{.*}}, %{{.*}} : tensor<3x?x?xf32>, tensor<3x?x?xf32> -> tensor<3x?x?xf32>
        %res = vosa.add %t0_f32, %t1_f32 : tensor<3x?x?xf32>, tensor<3x?x?xf32> -> tensor<3x?x?xf32>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @div()
    func.func @div() {
        %t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        %t1_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        // CHECK: %{{.*}} = vosa.div %{{.*}}, %{{.*}} : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xf16>
        %res1 = vosa.div %t0_f16, %t1_f16 : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xf16>

        %t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        %t1_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        // CHECK: %{{.*}} = vosa.div %{{.*}}, %{{.*}} : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>
        %res2 = vosa.div %t0_f32, %t1_f32 : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @mult()
    func.func @mult() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // CHECK: %{{.*}} = vosa.mult %{{.*}}, %{{.*}} : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>
        %res0 = vosa.mult %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>

        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        %t1_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        // CHECK: %{{.*}} = vosa.mult %{{.*}}, %{{.*}} : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>
        %res1 = vosa.mult %t0_si16, %t1_si16 : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>

        %t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        %t1_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        // CHECK: %{{.*}} = vosa.mult %{{.*}}, %{{.*}} : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>
        %res2 = vosa.mult %t0_f32, %t1_f32 : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @sub()
    func.func @sub() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        // CHECK: %{{.*}} = vosa.sub %{{.*}}, %{{.*}} : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>
        %res_ui8 = vosa.sub %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>

        %t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        %t1_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        // CHECK: %{{.*}} = vosa.sub %{{.*}}, %{{.*}} : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xui16>
        %res_ui16 = vosa.sub %t0_ui16, %t1_ui16 : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xui16>

        %t0_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        %t1_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        // CHECK: %{{.*}} = vosa.sub %{{.*}}, %{{.*}} : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xui32>
        %res_ui32 = vosa.sub %t0_ui32, %t1_ui32 : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xui32>

        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // CHECK: %{{.*}} = vosa.sub %{{.*}}, %{{.*}} : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>
        %res_si8 = vosa.sub %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>

        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        %t1_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        // CHECK: %{{.*}} = vosa.sub %{{.*}}, %{{.*}} : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>
        %res_si16 = vosa.sub %t0_si16, %t1_si16 : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>

        %t0_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
        %t1_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
        // CHECK: %{{.*}} = vosa.sub %{{.*}}, %{{.*}} : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xsi32>
        %res_si32 = vosa.sub %t0_si32, %t1_si32 : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xsi32>

        %t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        %t1_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        // CHECK: %{{.*}} = vosa.sub %{{.*}}, %{{.*}} : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>
        %res_f32 = vosa.sub %t0_f32, %t1_f32 : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>

        return
    }
}

// -----

module {
    //CHECK-LABEL: func.func @and()
    func.func @and() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        // CHECK: %{{.*}} = vosa.and %{{.*}}, %{{.*}} : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>
        %res_ui8 = vosa.and %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>

        %t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        %t1_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        // CHECK: %{{.*}} = vosa.and %{{.*}}, %{{.*}} : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xui16>
        %res_ui16 = vosa.and %t0_ui16, %t1_ui16 : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xui16>

        %t0_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        %t1_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        // CHECK: %{{.*}} = vosa.and %{{.*}}, %{{.*}} : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xui32>
        %res_ui32 = vosa.and %t0_ui32, %t1_ui32 : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xui32>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @abs_diff()
    func.func @abs_diff() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        // CHECK: %{{.*}} = vosa.abs_diff %{{.*}}, %{{.*}} : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>
        %res_ui8 = vosa.abs_diff %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>

        %t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        %t1_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        // CHECK: %{{.*}} = vosa.abs_diff %{{.*}}, %{{.*}} : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xui16>
        %res_ui16 = vosa.abs_diff %t0_ui16, %t1_ui16 : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xui16>

        %t0_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        %t1_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        // CHECK: %{{.*}} = vosa.abs_diff %{{.*}}, %{{.*}} : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xui32>
        %res_ui32 = vosa.abs_diff %t0_ui32, %t1_ui32 : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xui32>

        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // CHECK: %{{.*}} = vosa.abs_diff %{{.*}}, %{{.*}} : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi16>
        %res_si8 = vosa.abs_diff %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi16>

        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        %t1_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        // CHECK: %{{.*}} = vosa.abs_diff %{{.*}}, %{{.*}} : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi32>
        %res_si16 = vosa.abs_diff %t0_si16, %t1_si16 : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi32>

        %t0_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
        %t1_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
        // CHECK: %{{.*}} = vosa.abs_diff %{{.*}}, %{{.*}} : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xsi32>
        %res_si32 = vosa.abs_diff %t0_si32, %t1_si32 : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xsi32>

        %t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        %t1_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        // CHECK: %{{.*}} = vosa.abs_diff %{{.*}}, %{{.*}} : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xf16>
        %res_f16 = vosa.abs_diff %t0_f16, %t1_f16 : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xf16>

        %t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        %t1_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        // CHECK: %{{.*}} = vosa.abs_diff %{{.*}}, %{{.*}} : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>
        %res_f32 = vosa.abs_diff %t0_f32, %t1_f32 : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @where()
    func.func @where() {
        %t_bool = vosa.constant dense<true> : tensor<100x200x3xi1>
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        // CHECK: %{{.*}} = vosa.where %{{.*}}, %{{.*}}, %{{.*}} : tensor<100x200x3xui8>, tensor<100x200x3xui8>, tensor<100x200x3xi1> -> tensor<100x200x3xui8>
        %res_ui8 = vosa.where %t0_ui8, %t1_ui8, %t_bool : tensor<100x200x3xui8>, tensor<100x200x3xui8>, tensor<100x200x3xi1> -> tensor<100x200x3xui8>

        %t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        %t1_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        // CHECK: %{{.*}} = vosa.where %{{.*}}, %{{.*}}, %{{.*}} : tensor<100x200x3xui16>, tensor<100x200x3xui16>, tensor<100x200x3xi1> -> tensor<100x200x3xui16>
        %res_ui16 = vosa.where %t0_ui16, %t1_ui16, %t_bool : tensor<100x200x3xui16>, tensor<100x200x3xui16>, tensor<100x200x3xi1> -> tensor<100x200x3xui16>

        %t0_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        %t1_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        // CHECK: %{{.*}} = vosa.where %{{.*}}, %{{.*}}, %{{.*}} : tensor<100x200x3xui32>, tensor<100x200x3xui32>, tensor<100x200x3xi1> -> tensor<100x200x3xui32>
        %res_ui32 = vosa.where %t0_ui32, %t1_ui32, %t_bool : tensor<100x200x3xui32>, tensor<100x200x3xui32>, tensor<100x200x3xi1> -> tensor<100x200x3xui32>


        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // CHECK: %{{.*}} = vosa.where %{{.*}}, %{{.*}}, %{{.*}} : tensor<100x200x3xsi8>, tensor<100x200x3xsi8>, tensor<100x200x3xi1> -> tensor<100x200x3xsi8>
        %res_si8 = vosa.where %t0_si8, %t1_si8, %t_bool : tensor<100x200x3xsi8>, tensor<100x200x3xsi8>, tensor<100x200x3xi1> -> tensor<100x200x3xsi8>

        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        %t1_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        // CHECK: %{{.*}} = vosa.where %{{.*}}, %{{.*}}, %{{.*}} : tensor<100x200x3xsi16>, tensor<100x200x3xsi16>, tensor<100x200x3xi1> -> tensor<100x200x3xsi16>
        %res_si16 = vosa.where %t0_si16, %t1_si16, %t_bool : tensor<100x200x3xsi16>, tensor<100x200x3xsi16>, tensor<100x200x3xi1> -> tensor<100x200x3xsi16>

        %t0_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
        %t1_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
        // CHECK: %{{.*}} = vosa.where %{{.*}}, %{{.*}}, %{{.*}} : tensor<100x200x3xsi32>, tensor<100x200x3xsi32>, tensor<100x200x3xi1> -> tensor<100x200x3xsi32>
        %res_si32 = vosa.where %t0_si32, %t1_si32, %t_bool : tensor<100x200x3xsi32>, tensor<100x200x3xsi32>, tensor<100x200x3xi1> -> tensor<100x200x3xsi32>

        %t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        %t1_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        // CHECK: %{{.*}} = vosa.where %{{.*}}, %{{.*}}, %{{.*}} : tensor<100x200x3xf16>, tensor<100x200x3xf16>, tensor<100x200x3xi1> -> tensor<100x200x3xf16>
        %res_f16 = vosa.where %t0_f16, %t1_f16, %t_bool : tensor<100x200x3xf16>, tensor<100x200x3xf16>, tensor<100x200x3xi1> -> tensor<100x200x3xf16>

        %t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        %t1_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        // CHECK: %{{.*}} = vosa.where %{{.*}}, %{{.*}}, %{{.*}} : tensor<100x200x3xf32>, tensor<100x200x3xf32>, tensor<100x200x3xi1> -> tensor<100x200x3xf32>
        %res_f32 = vosa.where %t0_f32, %t1_f32, %t_bool : tensor<100x200x3xf32>, tensor<100x200x3xf32>, tensor<100x200x3xi1> -> tensor<100x200x3xf32>

        return
    }
}

// -----

module {
    func.func @or() {
        %t0_bool = vosa.constant dense<true> : tensor<100x200x3xi1>
        %t1_bool = vosa.constant dense<true> : tensor<100x200x3xi1>
        //CHECK: %{{.*}} = vosa.or %{{.*}}, %{{.*}} : tensor<100x200x3xi1>, tensor<100x200x3xi1> -> tensor<100x200x3xi1>
        %res_i1 = vosa.or %t0_bool, %t1_bool : tensor<100x200x3xi1>, tensor<100x200x3xi1> -> tensor<100x200x3xi1>

        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        //CHECK: %{{.*}} = vosa.or %{{.*}}, %{{.*}} : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>
        %res_ui8 = vosa.or %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @equal_op()
    func.func @equal_op() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        //CHECK: %{{.*}} = vosa.equal %{{.*}}, %{{.*}} : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xi1>
        %res_ui8 = vosa.equal %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xi1>

        %t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        %t1_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        //CHECK: %{{.*}} = vosa.equal %{{.*}}, %{{.*}} : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xi1>
        %res_ui16 = vosa.equal %t0_ui16, %t1_ui16 : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xi1>

        %t0_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        %t1_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        //CHECK: %{{.*}} = vosa.equal %{{.*}}, %{{.*}} : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xi1>
        %res_ui32 = vosa.equal %t0_ui32, %t1_ui32 : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xi1>

        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        //CHECK: %{{.*}} = vosa.equal %{{.*}}, %{{.*}} : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xi1>
        %res_si8 = vosa.equal %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xi1>

        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        %t1_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        //CHECK: %{{.*}} = vosa.equal %{{.*}}, %{{.*}} : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xi1>
        %res_si16 = vosa.equal %t0_si16, %t1_si16 : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xi1>

        %t0_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
        %t1_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
        //CHECK: %{{.*}} = vosa.equal %{{.*}}, %{{.*}} : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xi1>
        %res_si32 = vosa.equal %t0_si32, %t1_si32 : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xi1>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @greater()
    func.func @greater() {
	%t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	%t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	//CHECK: %{{.*}} = vosa.greater %{{.*}}, %{{.*}} : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xi1>
	%res_ui8 = vosa.greater %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xi1>


	%t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
	%t1_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
	//CHECK: %{{.*}} = vosa.greater %{{.*}}, %{{.*}} : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xi1>
	%res_ui16 = vosa.greater %t0_ui16, %t1_ui16 : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xi1>

	%t0_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
	%t1_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
	//CHECK: %{{.*}} = vosa.greater %{{.*}}, %{{.*}} : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xi1>
	%res_ui32 = vosa.greater %t0_ui32, %t1_ui32 : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xi1>

	%t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
	%t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
	//CHECK: %{{.*}} = vosa.greater %{{.*}}, %{{.*}} : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xi1>
	%res_si8 = vosa.greater %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xi1>

	%t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
	%t1_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
	//CHECK: %{{.*}} = vosa.greater %{{.*}}, %{{.*}} : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xi1>
	%res_si16 = vosa.greater %t0_si16, %t1_si16 : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xi1>

	%t0_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
	%t1_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
	//CHECK: %{{.*}} = vosa.greater %{{.*}}, %{{.*}} : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xi1>
	%res_si32 = vosa.greater %t0_si32, %t1_si32 : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xi1>

	%t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
	%t1_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
	//CHECK: %{{.*}} = vosa.greater %{{.*}}, %{{.*}} : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xi1>
	%res_f16 = vosa.greater %t0_f16, %t1_f16 : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xi1>

	%t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
	%t1_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
	//CHECK: %{{.*}} = vosa.greater %{{.*}}, %{{.*}} : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xi1>
	%res_f32 = vosa.greater %t0_f32, %t1_f32 : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xi1>
	return
    }
}

// -----

module {
    func.func @greater_type_mismatch() {
	%t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	%t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>

	// expected-error @below {{'vosa.greater' op input types do not match}}
	%res = vosa.greater  %t0_ui8, %t1_si8 : tensor<100x200x3xui8>, tensor<100x200x3xsi8> -> tensor<100x200x3xi1>
	
	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @greater_equal()
    func.func @greater_equal() {
	%t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	%t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	//CHECK: %{{.*}} = vosa.greater_equal %{{.*}}, %{{.*}} : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xi1>
	%res_ui8 = vosa.greater_equal %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xi1>


	%t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
	%t1_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
	//CHECK: %{{.*}} = vosa.greater_equal %{{.*}}, %{{.*}} : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xi1>
	%res_ui16 = vosa.greater_equal %t0_ui16, %t1_ui16 : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xi1>

	%t0_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
	%t1_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
	//CHECK: %{{.*}} = vosa.greater_equal %{{.*}}, %{{.*}} : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xi1>
	%res_ui32 = vosa.greater_equal %t0_ui32, %t1_ui32 : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xi1>

	%t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
	%t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
	//CHECK: %{{.*}} = vosa.greater_equal %{{.*}}, %{{.*}} : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xi1>
	%res_si8 = vosa.greater_equal %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xi1>

	%t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
	%t1_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
	//CHECK: %{{.*}} = vosa.greater_equal %{{.*}}, %{{.*}} : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xi1>
	%res_si16 = vosa.greater_equal %t0_si16, %t1_si16 : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xi1>

	%t0_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
	%t1_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
	//CHECK: %{{.*}} = vosa.greater_equal %{{.*}}, %{{.*}} : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xi1>
	%res_si32 = vosa.greater_equal %t0_si32, %t1_si32 : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xi1>

	%t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
	%t1_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
	//CHECK: %{{.*}} = vosa.greater_equal %{{.*}}, %{{.*}} : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xi1>
	%res_f16 = vosa.greater_equal %t0_f16, %t1_f16 : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xi1>

	%t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
	%t1_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
	//CHECK: %{{.*}} = vosa.greater_equal %{{.*}}, %{{.*}} : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xi1>
	%res_f32 = vosa.greater_equal %t0_f32, %t1_f32 : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xi1>
	return
    }
}

// -----

module {
    func.func @greater_equal_type_mismatch() {
	%t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	%t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>

	// expected-error @below {{'vosa.greater_equal' op input types do not match}}
	%res = vosa.greater_equal  %t0_ui8, %t1_si8 : tensor<100x200x3xui8>, tensor<100x200x3xsi8> -> tensor<100x200x3xi1>
	
	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @mod()
    func.func @mod() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        //CHECK: %{{.*}} = vosa.mod %{{.*}}, %{{.*}} : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>
        %res_ui8 = vosa.mod %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>

        %t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        %t1_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        //CHECK: %{{.*}} = vosa.mod %{{.*}}, %{{.*}} : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xui16>
        %res_ui16 = vosa.mod %t0_ui16, %t1_ui16 : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xui16>

        %t0_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        %t1_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        //CHECK: %{{.*}} = vosa.mod %{{.*}}, %{{.*}} : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xui32>
        %res_ui32 = vosa.mod %t0_ui32, %t1_ui32 : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xui32>

        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        //CHECK: %{{.*}} = vosa.mod %{{.*}}, %{{.*}} : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>
        %res_si8 = vosa.mod %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>

        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        %t1_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        //CHECK: %{{.*}} = vosa.mod %{{.*}}, %{{.*}} : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>
        %res_si16 = vosa.mod %t0_si16, %t1_si16 : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>

        %t0_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
        %t1_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
        //CHECK: %{{.*}} = vosa.mod %{{.*}}, %{{.*}} : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xsi32>
        %res_si32 = vosa.mod %t0_si32, %t1_si32 : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xsi32>

        %t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        %t1_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        //CHECK: %{{.*}} = vosa.mod %{{.*}}, %{{.*}} : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xf16>
        %res_f16 = vosa.mod %t0_f16, %t1_f16 : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xf16>

        %t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        %t1_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        //CHECK: %{{.*}} = vosa.mod %{{.*}}, %{{.*}} : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>
        %res_f32 = vosa.mod %t0_f32, %t1_f32 : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @max()
    func.func @max() {
	%t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	%t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	// CHECK: %{{.*}} = vosa.max %{{.*}}, %{{.*}} : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>
	%res_ui8 = vosa.max %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>

	%t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
	%t1_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
	// CHECK: %{{.*}} = vosa.max %{{.*}}, %{{.*}} : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xui16>
	%res_ui16 = vosa.max %t0_ui16, %t1_ui16 : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xui16>

	%t0_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
	%t1_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
	// CHECK: %{{.*}} = vosa.max %{{.*}}, %{{.*}} : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xui32>
	%res_ui32 = vosa.max %t0_ui32, %t1_ui32 : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xui32>

	%t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
	%t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
	// CHECK: %{{.*}} = vosa.max %{{.*}}, %{{.*}} : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>
	%res_si8 = vosa.max %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>

	%t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
	%t1_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
	// CHECK: %{{.*}} = vosa.max %{{.*}}, %{{.*}} : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>
	%res_si16 = vosa.max %t0_si16, %t1_si16 : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>

	%t0_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
	%t1_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
	// CHECK: %{{.*}} = vosa.max %{{.*}}, %{{.*}} : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xsi32>
	%res_si32 = vosa.max %t0_si32, %t1_si32 : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xsi32>

	%t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
	%t1_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
	// CHECK: %{{.*}} = vosa.max %{{.*}}, %{{.*}} : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xf16>
	%res_f16 = vosa.max %t0_f16, %t1_f16 : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xf16>

	%t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
	%t1_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
	// CHECK: %{{.*}} = vosa.max %{{.*}}, %{{.*}} : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>
	%res_f32 = vosa.max %t0_f32, %t1_f32 : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>

	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @min()
    func.func @min() {
	%t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	%t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	// CHECK: %{{.*}} = vosa.min %{{.*}}, %{{.*}} : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>
	%res_ui8 = vosa.min %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui8>

	%t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
	%t1_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
	// CHECK: %{{.*}} = vosa.min %{{.*}}, %{{.*}} : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xui16>
	%res_ui16 = vosa.min %t0_ui16, %t1_ui16 : tensor<100x200x3xui16>, tensor<100x200x3xui16> -> tensor<100x200x3xui16>

	%t0_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
	%t1_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
	// CHECK: %{{.*}} = vosa.min %{{.*}}, %{{.*}} : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xui32>
	%res_ui32 = vosa.min %t0_ui32, %t1_ui32 : tensor<100x200x3xui32>, tensor<100x200x3xui32> -> tensor<100x200x3xui32>

	%t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
	%t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
	// CHECK: %{{.*}} = vosa.min %{{.*}}, %{{.*}} : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>
	%res_si8 = vosa.min %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>

	%t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
	%t1_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
	// CHECK: %{{.*}} = vosa.min %{{.*}}, %{{.*}} : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>
	%res_si16 = vosa.min %t0_si16, %t1_si16 : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>

	%t0_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
	%t1_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
	// CHECK: %{{.*}} = vosa.min %{{.*}}, %{{.*}} : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xsi32>
	%res_si32 = vosa.min %t0_si32, %t1_si32 : tensor<100x200x3xsi32>, tensor<100x200x3xsi32> -> tensor<100x200x3xsi32>

	%t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
	%t1_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
	// CHECK: %{{.*}} = vosa.min %{{.*}}, %{{.*}} : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xf16>
	%res_f16 = vosa.min %t0_f16, %t1_f16 : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xf16>

	%t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
	%t1_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
	// CHECK: %{{.*}} = vosa.min %{{.*}}, %{{.*}} : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>
	%res_f32 = vosa.min %t0_f32, %t1_f32 : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf32>

	return
    }
}

// -----

module {
    func.func @max_type_mismatch() {
	%t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	%t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>

	// expected-error @below {{'vosa.max' op input types do not match}}
	%res = vosa.max %t0_ui8, %t1_si8 : tensor<100x200x3xui8>, tensor<100x200x3xsi8> -> tensor<100x200x3xui8>

	return
    }
}

// -----

module {
    func.func @max_type_mismatch() {
	%t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	%t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>

	// expected-error @below {{'vosa.max' op output type does not match input}}
	%res_ui16 = vosa.max %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui16>

	return
    }
}

// -----

module {
    func.func @min_type_mismatch() {
	%t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	%t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>

	// expected-error @below {{'vosa.min' op input types do not match}}
	%res = vosa.min %t0_ui8, %t1_si8 : tensor<100x200x3xui8>, tensor<100x200x3xsi8> -> tensor<100x200x3xui8>

	return
    }
}

// -----

module {
    func.func @min_type_mismatch() {
	%t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
	%t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>

	// expected-error @below {{'vosa.min' op output type does not match input}}
	%res_ui16 = vosa.min %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui16>

	return
    }
}

// -----

module {
    func.func @equal_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>

        // expected-error @below {{'vosa.equal' op input types do not match}}
        %res = vosa.equal %t0_ui8, %t1_si8 : tensor<100x200x3xui8>, tensor<100x200x3xsi8> -> tensor<100x200x3xi1>

        return
    }
}

// -----

module {
    func.func @add_type_mismatch() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // expected-error @below {{'vosa.add' op output type does not match input}}
        %res = vosa.add %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi32>
        return
    }
}

// -----

module {
    func.func @add_type_mismatch() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x4xsi8>
        // expected-error @below {{'vosa.add' op input types do not match}}
        %res = vosa.add %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x4xsi8> -> tensor<100x200x3xsi8>
        return
    }
}

// -----

module {
    func.func @add_type_mismatch() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi32>
        // expected-error @below {{'vosa.add' op input types do not match}}
        %res = vosa.add %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi32> -> tensor<100x200x3xsi8>
        return
    }
}

// -----

module {
    func.func @div_int_types() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // expected-error @below {{'vosa.div' op operand #0 must be 3D tensor of 16-bit float or 32-bit float values, but got 'tensor<100x200x3xsi8>'}}
        %res = vosa.div %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi32>
        return
    }
}

// -----

module {
    func.func @div_type_mismatch() {
        %t0_si8 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        %t1_si8 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        // expected-error @below {{'vosa.div' op output type does not match input}}
        %res = vosa.div %t0_si8, %t1_si8 : tensor<100x200x3xf16>, tensor<100x200x3xf16> -> tensor<100x200x3xf32>
        return
    }
}

// -----

module {
    func.func @mult_type_mismatch() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // expected-error @below {{'vosa.mult' op output type does not match input}}
        %res = vosa.mult %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi32>
        return
    }
}

// -----

module {
    func.func @sub_type_mismatch_si8() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %t1_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // expected-error @below {{'vosa.sub' op output type does not match input}}
        %res = vosa.sub %t0_si8, %t1_si8 : tensor<100x200x3xsi8>, tensor<100x200x3xsi8> -> tensor<100x200x3xsi16>
        return
    }
}

// -----

module {
    func.func @sub_type_mismatch_f32() {
        %t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        %t1_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        // expected-error @below {{'vosa.sub' op output type does not match input}}
        %res = vosa.sub %t0_f32, %t1_f32 : tensor<100x200x3xf32>, tensor<100x200x3xf32> -> tensor<100x200x3xf16>
        return
    }
}

// -----

module {
    func.func @and_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        // expected-error @below {{'vosa.and' op output type does not match input}}
        %err_ui16 = vosa.and %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui16>

        return
    }
}

// -----

module {
    func.func @and_type_mismatch() { 
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x4xui8>

        // expected-error @below {{'vosa.and' op input types do not match}}
        %err_ui8 = vosa.and %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x4xui8> -> tensor<100x200x3xui8>
        return
    }
}

// -----

module {
    func.func @and_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>

        // expected-error @below {{'vosa.and' op input types do not match}}
        %err = vosa.and %t0_ui8, %t1_ui16 : tensor<100x200x3xui8>, tensor<100x200x3xui16> -> tensor<100x200x3xui8>
        return
    }
}

// -----

module {
    func.func @abs_diff_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>

        // expected-error @below {{'vosa.abs_diff' op input types do not match}}
        %err = vosa.abs_diff %t0_ui8, %t1_ui16 : tensor<100x200x3xui8>, tensor<100x200x3xui16> -> tensor<100x200x3xui8>
        return
    }
}

// -----

module {
    func.func @abs_diff_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x4xui8>

        // expected-error @below {{'vosa.abs_diff' op input types do not match}}
        %err = vosa.abs_diff %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x4xui8> -> tensor<100x200x3xui8>
        return
    }
}

// -----

module {
    func.func @abs_diff_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>

        // expected-error @below {{'vosa.abs_diff' op output type does not match input}}
        %err = vosa.abs_diff %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xui16>
        return
    }
}

// -----

module {
    func.func @abs_diff_type_mismatch() {
        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        %t1_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>

        // expected-error @below {{'vosa.abs_diff' op invalid output type}}
        %err = vosa.abs_diff %t0_si16, %t0_si16 : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xf16>
        return
    }
}

// -----

module {
    func.func @abs_diff_type_mismatch() {
        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        %t1_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>

        // expected-error @below {{'vosa.abs_diff' op invalid output type}}
        %err = vosa.abs_diff %t0_si16, %t0_si16 : tensor<100x200x3xsi16>, tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>
        return
    }
}

// -----

module {
    func.func @where_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x4xui8>
        %t2_bool = vosa.constant dense<true> : tensor<100x200x3xi1>

        // expected-error @below {{'vosa.where' op input types do not match}}
        %err = vosa.where %t0_ui8, %t1_ui8, %t2_bool : tensor<100x200x3xui8>, tensor<100x200x4xui8>, tensor<100x200x3xi1> -> tensor<100x200x3xui8>

        return
    }
}

// -----

module {
    func.func @where_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui16>
        %t2_bool = vosa.constant dense<true> : tensor<100x200x3xi1>

        // expected-error @below {{'vosa.where' op input types do not match}}
        %err = vosa.where %t0_ui8, %t1_ui8, %t2_bool : tensor<100x200x3xui8>, tensor<100x200x3xui16>, tensor<100x200x3xi1> -> tensor<100x200x3xui8>

        return
    }
}

// -----

module {
    func.func @where_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t2_bool = vosa.constant dense<true> : tensor<100x200x3xi1>

        // expected-error @below {{'vosa.where' op output type does not match input}}
        %err = vosa.where %t0_ui8, %t1_ui8, %t2_bool : tensor<100x200x3xui8>, tensor<100x200x3xui8>, tensor<100x200x3xi1> -> tensor<100x200x4xui8>

        return
    }
}

// -----

module {
    func.func @where_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t2_bool = vosa.constant dense<true> : tensor<100x200x3xi1>

        // expected-error @below {{'vosa.where' op output type does not match input}}
        %err = vosa.where %t0_ui8, %t1_ui8, %t2_bool : tensor<100x200x3xui8>, tensor<100x200x3xui8>, tensor<100x200x3xi1> -> tensor<100x200x3xui16>

        return
    }
}

// -----

module {
    func.func @where_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t2_bool = vosa.constant dense<true> : tensor<100x200x4xi1>

        // expected-error @below {{'vosa.where' op input shapes do not match}}
        %err = vosa.where %t0_ui8, %t1_ui8, %t2_bool : tensor<100x200x3xui8>, tensor<100x200x3xui8>, tensor<100x200x4xi1> -> tensor<100x200x3xui8>

        return
    }
}

// -----

module {
    func.func @or_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_bool = vosa.constant dense<true> : tensor<100x200x3xi1>

        // expected-error @below {{'vosa.or' op input types do not match}}
        %res = vosa.or %t0_ui8, %t1_bool : tensor<100x200x3xui8>, tensor<100x200x3xi1> -> tensor<100x200x3xui8>

        return
    }
}

// -----

module {
    func.func @or_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %t1_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>

        // expected-error @below {{'vosa.or' op output type does not match input}}
        %res = vosa.or %t0_ui8, %t1_ui8 : tensor<100x200x3xui8>, tensor<100x200x3xui8> -> tensor<100x200x3xi1>

        return
    }
}
