"""
License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import enum
import os
import typing

from vosa import types as types
from vosa.op import ExecutionPattern, InputArrayPattern, OPERATOR_MODES, Op

CUSTOM_OPERATORS_LITERAL = """
In order to support the widest possible variety of Imaging and Computer Vision pipelines, VOSA support a Custom Operator structure. 
This enables Imaging and Computer Vision pipelines, implemented in VOSA, to incorporate special purpose operations which may be specific to that pipeline.

In order to facillitate efficient execution of such operations, VOSA defines a structure where the data access pattern of such custom operations is part of their specfication.
"""

SCALAR_CUSTOM_OPERATORS_LITERAL = """
=== Scalar Custom Operators
Since VOSA aims to mostly support Image to Image operations, the specification largely avoids defining Scalar to Scalar operations, which, nonetheless may be needed for parts of processing a wider imaging or computer vision pipeline.

To enable this VOSA defines a custom Scalar operation where generic Scalar to Scalar processing may take place.
"""

LOCAL_CUSTOM_OPERATORS_LITERAL = """
=== Local Custom Operators
Local Custom Operators may be defined for processing where a local neighbourhood of input pixels is sufficient for the computation of an output pixel.
"""

GLOBAL_CUSTOM_OPERATORS_LITERAL = """
=== Global Custom Operators
Global Custom Operators are the most generic form of custom operators and are appropriate for whole Image to Image operations which are difficult or impossible to express as local operations.
"""


def adoc(adoc_path):
    custom_operators_path = "custom"
    custom_operators_folder = os.path.join(adoc_path, custom_operators_path)
    os.makedirs(custom_operators_folder, exist_ok=True)
    lines = ["== Custom Operators"]

    lines.append(CUSTOM_OPERATORS_LITERAL)

    lines.append(SCALAR_CUSTOM_OPERATORS_LITERAL)
    scalar_operator = SCALAR_CUSTOM_OPERATOR.adoc()
    lines.extend(scalar_operator)

    lines.append(LOCAL_CUSTOM_OPERATORS_LITERAL)
    local_operator = LOCAL_IMAGE_CUSTOM_OPERATOR.adoc()
    lines.extend(local_operator)

    local_kernel = LOCAL_IMAGE_CUSTOM_KERNEL.adoc()
    lines.extend(local_kernel)

    lines.append(GLOBAL_CUSTOM_OPERATORS_LITERAL)
    global_operator = GLOBAL_IMAGE_CUSTOM_OPERATOR.adoc()
    lines.extend(global_operator)

    custom_operators_file = "custom.adoc"
    adoc_custom_operators_path = os.path.join(adoc_path, custom_operators_file)
    with open(adoc_custom_operators_path, "w") as f:
        for line in lines:
            f.write("%s\n" % line)
    return custom_operators_file


class ScalarCustomOperatorExecutionPattern(ExecutionPattern):
    def __init__(self):
        super().__init__(
            input_pattern=InputArrayPattern(
                types.DataType(
                    types.DType.ANY,
                    types.AType.SCALAR,
                    shape=[],
                    description="Input Scalars.",
                )
            ),
            output_pattern=types.DataType(
                types.DType.ANY,
                types.AType.SCALAR,
                shape=[],
                description="Output Scalar",
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {SCALAR_CUSTOM_OPERATOR.GENERIC_ATTRIBUTE: GenericAttribute()}


class SCALAR_CUSTOM_OPERATOR_MODES(OPERATOR_MODES):
    S_S_X_X = enum.auto()


class SCALAR_CUSTOM_OPERATOR(Op):
    """
    This operation serves as a generic interface for custom Scalar to Scalar operations in VOSA.
    """

    GENERIC_ATTRIBUTE = "attributes"

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[
        SCALAR_CUSTOM_OPERATOR_MODES, ScalarCustomOperatorExecutionPattern
    ]:
        return {
            SCALAR_CUSTOM_OPERATOR_MODES.S_S_X_X: ScalarCustomOperatorExecutionPattern()
        }


class LocalImageCustomOperatorExecutionPattern(ExecutionPattern):
    def __init__(self):
        super().__init__(
            input_pattern=InputArrayPattern(
                types.DataType(
                    types.DType.ANY,
                    types.AType.IMAGE,
                    shape=["C", "H", "W"],
                    description="Input Images.",
                )
            ),
            output_pattern=types.DataType(
                types.DType.ANY,
                types.AType.IMAGE,
                shape=["C", "H - window_size[0]", "W - window_size[1]"],
                description="Output Image",
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            LOCAL_IMAGE_CUSTOM_OPERATOR.ATTRIBUTE_WINDOW_SIZE: types.DataType(
                types.DType.UINT32,
                types.AType.ARRAY,
                shape=[2],
                description="Window size on which the kernel should operate.",
            ),
            LOCAL_IMAGE_CUSTOM_OPERATOR.ATTRIBUTE_KERNEL: LOCAL_IMAGE_CUSTOM_KERNEL,
        }


class IMAGE_CUSTOM_OPERATOR_MODES(OPERATOR_MODES):
    I_I_X_X = enum.auto()


class LOCAL_IMAGE_CUSTOM_OPERATOR(Op):
    """
    This operation serves as a generic interface for local, custom Image to Image operations in VOSA.

    In this context local means the output value for a given pixel may be calculated using data from a (usually small) window into the input Image(s).

    The operation is parameterized by a `window_size` which indicates the required window into the input Image(s) the kernel, as well as the kernel itself.
    """

    ATTRIBUTE_WINDOW_SIZE = "window"
    ATTRIBUTE_KERNEL = "kernel"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[OPERATOR_MODES, ExecutionPattern]:
        return {
            IMAGE_CUSTOM_OPERATOR_MODES.I_I_X_X: LocalImageCustomOperatorExecutionPattern()
        }


class GenericAttribute(types.DataType):
    def __init__(self):
        super().__init__(
            dtype=types.DType.ANY,
            ttype=types.AType.ANY,
            description="Any attributes needed for the kernel.",
        )


class LocalImageCustomKernelExecutionPattern(ExecutionPattern):
    def __init__(self):
        super().__init__(
            input_pattern=InputArrayPattern(
                types.DataType(
                    types.DType.ANY,
                    types.AType.IMAGE,
                    shape=[],
                    description="Windows into input Images.",
                )
            ),
            output_pattern=types.DataType(
                types.DType.ANY,
                types.AType.IMAGE,
                shape=["C", 1, 1],
                description="Output pixel values to be used in the output of LOCAL_IMAGE_CUSTOM_OPERATOR",
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {LOCAL_IMAGE_CUSTOM_KERNEL.GENERIC_ATTRIBUTE: GenericAttribute()}


class LOCAL_IMAGE_CUSTOM_KERNEL(Op):
    """
    This operation serves as a template for kernels used within `IMAGE_CUSTOM_OPERATOR`.

    The output of this kernel should be a single pixel (with number of channels equal to input).
    """

    GENERIC_ATTRIBUTE = "attributes"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[OPERATOR_MODES, ExecutionPattern]:
        return {
            IMAGE_CUSTOM_OPERATOR_MODES.I_I_X_X: LocalImageCustomKernelExecutionPattern()
        }


class GlobalImageCustomOperatorExecutionPattern(ExecutionPattern):
    def __init__(self):
        super().__init__(
            input_pattern=InputArrayPattern(
                types.DataType(
                    types.DType.ANY,
                    types.AType.IMAGE,
                    shape=["C", "H", "W"],
                    description="Input Image.",
                )
            ),
            output_pattern=types.DataType(
                types.DType.ANY,
                types.AType.IMAGE,
                shape=["C_out", "H_out", "W_out"],
                description="Output Image.",
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {GLOBAL_IMAGE_CUSTOM_OPERATOR.GENERIC_ATTRIBUTE: GenericAttribute()}


class GLOBAL_IMAGE_CUSTOM_OPERATOR(Op):
    """
    This operation serves as a generic interface for global, custom Image to Image operations in VOSA.

    In this context, global means that the output value for any pixel may in principle depend on the the value of any input pixel.
    """

    GENERIC_ATTRIBUTE = "attributes"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[OPERATOR_MODES, ExecutionPattern]:
        return {
            IMAGE_CUSTOM_OPERATOR_MODES.I_I_X_X: GlobalImageCustomOperatorExecutionPattern()
        }
