/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "conv.h"

template <typename T>
vosa::EigenPlane<T>
vosa::ConvBase::unpackValue(const std::vector<uint32_t> &dimension,
                            const std::vector<uint8_t> &raw_value) {
  vosa::EigenPlane<T> output(dimension[0], dimension[1]);
  std::copy(raw_value.begin(), raw_value.end(), (uint8_t *)output.data());
  return output;
}

std::shared_ptr<vosa::Op> vosa::ConvBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_conv = vosa_operator_union.AsCONV_2D();
  assert(serialized_conv);
  auto mode = serialized_conv->mode;
  auto serialized_attributes = &serialized_conv->attr;
  std::vector<uint8_t> filter_value =
      serialized_attributes->get()->filter_value;
  std::vector<uint32_t> filter_dimension =
      serialized_attributes->get()->filter_dimension;

  switch (mode) {
  case vosa_serialization::CONV_2D_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::Conv<uint8_t, uint8_t>>(
        unpackValue<uint8_t>(filter_dimension, filter_value));
  }
  case vosa_serialization::CONV_2D_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::Conv<uint16_t, uint16_t>>(
        unpackValue<uint16_t>(filter_dimension, filter_value));
  }
  case vosa_serialization::CONV_2D_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::Conv<uint32_t, uint32_t>>(
        unpackValue<uint32_t>(filter_dimension, filter_value));
  }
  case vosa_serialization::CONV_2D_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::Conv<int8_t, int8_t>>(
        unpackValue<int8_t>(filter_dimension, filter_value));
  }
  case vosa_serialization::CONV_2D_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::Conv<int16_t, int16_t>>(
        unpackValue<int16_t>(filter_dimension, filter_value));
  }
  case vosa_serialization::CONV_2D_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::Conv<int32_t, int32_t>>(
        unpackValue<int32_t>(filter_dimension, filter_value));
  }
  case vosa_serialization::CONV_2D_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::Conv<float, float>>(
        unpackValue<float>(filter_dimension, filter_value));
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename inputT, typename outputT>
vosa::EigenImage<outputT> vosa::Conv<inputT, outputT>::runme(
    const vosa::EigenImage<inputT> &input) const {

  uint32_t input_height = input.dimension(0);
  uint32_t input_width = input.dimension(1);
  uint32_t num_channels = input.dimension(2);

  uint32_t filter_height = filter.dimension(0);
  uint32_t filter_width = filter.dimension(1);

  uint32_t output_height = input_height - filter_height + 1;
  uint32_t output_width = input_width - filter_width + 1;

  auto output =
      vosa::EigenImage<outputT>{output_height, output_width, num_channels};

  for (uint32_t height_index = 0; height_index < output_height;
       height_index++) {
    for (uint32_t width_index = 0; width_index < output_width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < num_channels;
           channel_index++) {
        outputT output_accumulator = 0;

        for (uint32_t filter_height_index = 0;
             filter_height_index < filter_height; filter_height_index++) {
          for (uint32_t filter_width_index = 0;
               filter_width_index < filter_width; filter_width_index++) {

            auto filter_value =
                (outputT)filter(filter_height_index, filter_width_index);

            uint32_t input_height_index = height_index + filter_height_index;
            uint32_t input_width_index = width_index + filter_width_index;

            auto input_value = (outputT)input(input_height_index,
                                              input_width_index, channel_index);

            output_accumulator += filter_value * input_value;
          }
        }

        output(height_index, width_index, channel_index) = output_accumulator;
      }
    }
  }
  return output;
}

template <typename inputT, typename outputT>
vosa::ArrayBase::DType vosa::Conv<inputT, outputT>::output_dtype() const {
  return output_dtype_helper<outputT>();
}

template <typename inputT, typename outputT>
std::shared_ptr<vosa::ArrayBase> vosa::Conv<inputT, outputT>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {
  const EigenImage<inputT> &input = inputs[0]->template get_image<inputT>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::CONV_2D_MODE
vosa::Conv<uint8_t, uint8_t>::get_serialization_mode() const {
  return vosa_serialization::CONV_2D_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::CONV_2D_MODE
vosa::Conv<uint16_t, uint16_t>::get_serialization_mode() const {
  return vosa_serialization::CONV_2D_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::CONV_2D_MODE
vosa::Conv<uint32_t, uint32_t>::get_serialization_mode() const {
  return vosa_serialization::CONV_2D_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::CONV_2D_MODE
vosa::Conv<int8_t, int8_t>::get_serialization_mode() const {
  return vosa_serialization::CONV_2D_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::CONV_2D_MODE
vosa::Conv<int16_t, int16_t>::get_serialization_mode() const {
  return vosa_serialization::CONV_2D_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::CONV_2D_MODE
vosa::Conv<int32_t, int32_t>::get_serialization_mode() const {
  return vosa_serialization::CONV_2D_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::CONV_2D_MODE
vosa::Conv<float, float>::get_serialization_mode() const {
  return vosa_serialization::CONV_2D_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename inputT, typename outputT>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Conv<inputT, outputT>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  auto conv_attribute =
      std::make_unique<vosa_serialization::CONV_2D_ATTRIBUTET>();
  conv_attribute->filter_dimension = {(uint32_t)filter.dimension(0),
                                      (uint32_t)filter.dimension(1)};
  auto filter_size = filter.size();
  std::vector<uint8_t> value_byte_vector;
  auto byte_filter_ptr = (const uint8_t *)filter.data();
  value_byte_vector.insert(value_byte_vector.end(), byte_filter_ptr,
                           byte_filter_ptr + sizeof(inputT) * filter_size);
  conv_attribute->filter_value = value_byte_vector;

  vosa_serialization::CONV_2DT conv;
  conv.mode = get_serialization_mode();
  conv.attr = std::move(conv_attribute);

  auto conv_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  conv_operator->op.Set(std::move(conv));
  conv_operator->name = op_name;
  conv_operator->inputs = input_names;
  return conv_operator;
}

template class vosa::Conv<uint8_t, uint8_t>;

template class vosa::Conv<uint16_t, uint16_t>;

template class vosa::Conv<uint32_t, uint32_t>;

template class vosa::Conv<int8_t, int8_t>;

template class vosa::Conv<int16_t, int16_t>;

template class vosa::Conv<int32_t, int32_t>;

template class vosa::Conv<float, float>;