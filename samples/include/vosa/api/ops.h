/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_PIPELINES_API_OPS_H_
#define VOSA_PIPELINES_API_OPS_H_

#include <iostream>
#include <memory>
#include <vector>

#include "vosa/api/graph_builder.h"
#include "vosa/api/op.h"

namespace vosa::api {

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
input(vosa::api::GraphBuilder &graph_builder, ArrayBase::DType dtype,
      const std::vector<int64_t> &shape);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
abs(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
abs_diff(vosa::api::GraphBuilder &graph_builder,
         const std::shared_ptr<Op> &input_1,
         const std::shared_ptr<Op> &input_2);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
add(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input_1,
    const std::shared_ptr<Op> &input_2);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
argmin_channelwise(vosa::api::GraphBuilder &graph_builder,
                   const std::shared_ptr<Op> &input);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
arithmetic_shift_right(vosa::api::GraphBuilder &graph_builder,
                       const std::shared_ptr<Op> &input, uint32_t shift);

//[[maybe_unused]] std::shared_ptr<vosa::api::Op>
// atan2(vosa::api::GraphBuilder &graph_builder,
//      const std::shared_ptr<Op> &input_1, const std::shared_ptr<Op> &input_2);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
broadcast_channelwise(vosa::api::GraphBuilder &graph_builder,
                      const std::shared_ptr<Op> &input, uint32_t channels);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
broadcast_single_pixel(vosa::api::GraphBuilder &graph_builder,
                       const std::shared_ptr<Op> &input,
                       const std::vector<uint32_t> &size);

template <typename T>
[[maybe_unused]] std::shared_ptr<vosa::api::Op>
cast(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input,
     vosa::api::CastBase::rounding_mode_t rounding_mode =
         vosa::api::CastBase::rounding_mode_t::VOSA_ROUNDING_MODE_WRAP);

// NOTE: Handy overload
[[maybe_unused]] std::shared_ptr<vosa::api::Op>
channel_extract(vosa::api::GraphBuilder &graph_builder,
                const std::shared_ptr<Op> &input,
                const std::vector<uint32_t> &channels);
[[maybe_unused]] std::shared_ptr<vosa::api::Op>
channel_extract(vosa::api::GraphBuilder &graph_builder,
                const std::shared_ptr<Op> &input, uint32_t channel);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
clamp(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input,
      const std::shared_ptr<Op> &min_value,
      const std::shared_ptr<Op> &max_value);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
concat(vosa::api::GraphBuilder &graph_builder,
       const std::vector<std::shared_ptr<Op>> &input);

template <typename T>
[[maybe_unused]] std::shared_ptr<vosa::api::Op>
const_image(vosa::api::GraphBuilder &graph_builder,
            const std::vector<uint32_t> &dimensions, T value);

template <typename T>
[[maybe_unused]] std::shared_ptr<vosa::api::Op>
const_image(vosa::api::GraphBuilder &graph_builder, const EigenImage<T> &image);

template <typename T>
[[maybe_unused]] std::shared_ptr<vosa::api::Op>
const_plane(vosa::api::GraphBuilder &graph_builder,
            const std::vector<uint32_t> &dimensions, T value);

template <typename T>
[[maybe_unused]] std::shared_ptr<vosa::api::Op>
const_scalar(vosa::api::GraphBuilder &graph_builder, T value);

template <typename T>
[[maybe_unused]] std::shared_ptr<vosa::api::Op>
conv(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input,
     const EigenPlane<T> &filter);

//[[maybe_unused]] std::shared_ptr<vosa::api::Op>
// crop(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op>
// &input,
//     const std::vector<uint32_t> &crop_window);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
custom_scalar(vosa::api::GraphBuilder &graph_builder,
              const std::vector<std::shared_ptr<Op>> &inputs,
              const std::string &op_name);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
div(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input_1,
    const std::shared_ptr<Op> &input_2);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
decimate(vosa::api::GraphBuilder &graph_builder,
         const std::shared_ptr<Op> &input,
         const std::vector<uint32_t> &decimate_window = {1, 1},
         const std::vector<uint32_t> &offsets = {0, 0});

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
equal(vosa::api::GraphBuilder &graph_builder,
      const std::shared_ptr<Op> &input_1, const std::shared_ptr<Op> &input_2);

template <typename T>
[[maybe_unused]] std::shared_ptr<vosa::api::Op>
expand(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input,
       const std::shared_ptr<Op> &expand_kernel, T fill_value,
       const EigenPlane<uint8_t> &gather_kernel);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
gamma_correction(vosa::api::GraphBuilder &graph_builder,
                 const std::shared_ptr<Op> &input, float gamma);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
greater(vosa::api::GraphBuilder &graph_builder,
        const std::shared_ptr<Op> &input_1, const std::shared_ptr<Op> &input_2);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
greater_equal(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<Op> &input_1,
              const std::shared_ptr<Op> &input_2);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
import_channel(vosa::api::GraphBuilder &graph_builder,
               const std::shared_ptr<Op> &input, uint32_t stride,
               uint32_t offset, const std::vector<uint32_t> &shape);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
logical_shift_right(vosa::api::GraphBuilder &graph_builder,
                    const std::shared_ptr<Op> &input, uint32_t shift);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
max(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input_1,
    const std::shared_ptr<Op> &input_2);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
mesh_grid(vosa::api::GraphBuilder &graph_builder,
          const std::vector<uint32_t> &dimensions);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
min(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input_1,
    const std::shared_ptr<Op> &input_2);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
mod(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input_1,
    const std::shared_ptr<Op> &input_2);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
mult(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input_1,
     const std::shared_ptr<Op> &input_2);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
not_(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
or_(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input_1,
    const std::shared_ptr<Op> &input_2);

template <typename T>
[[maybe_unused]] std::shared_ptr<vosa::api::Op>
pad(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input,
    T pad_constant, vosa::api::PadBase::pad_mode_t pad_mode,
    const std::vector<uint32_t> &padding);

template <typename T>
[[maybe_unused]] std::shared_ptr<vosa::api::Op>
piecewise_linear(vosa::api::GraphBuilder &graph_builder,
                 const std::shared_ptr<Op> &input,
                 const vosa::api::EigenVector<T> &nodes,
                 const vosa::api::EigenVector<T> &values);

template <typename T>
[[maybe_unused]] std::shared_ptr<vosa::api::Op> pointwise_matrix_multiply(
    vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input,
    const EigenPlane<T> &matrix, const EigenVector<T> &inner_offsets,
    const EigenVector<T> &outer_offsets);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
power(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input,
      float base);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
print(vosa::api::GraphBuilder &graph_builder,
      const std::shared_ptr<vosa::api::Op> &input, const std::string &message);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
reduce_interp_channelwise(vosa::api::GraphBuilder &graph_builder,
                          const std::shared_ptr<Op> &input_1,
                          const std::shared_ptr<Op> &input_2);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
reduce_max_channelwise(vosa::api::GraphBuilder &graph_builder,
                       const std::shared_ptr<Op> &input);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
reduce_max_planewise(vosa::api::GraphBuilder &graph_builder,
                     const std::shared_ptr<Op> &input);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
reduce_min_planewise(vosa::api::GraphBuilder &graph_builder,
                     const std::shared_ptr<Op> &input);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
reduce_sum_channelwise(vosa::api::GraphBuilder &graph_builder,
                       const std::shared_ptr<Op> &input);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
reduce_sum_planewise(vosa::api::GraphBuilder &graph_builder,
                     const std::shared_ptr<Op> &input);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
resize_bilinear(vosa::api::GraphBuilder &graph_builder,
                const std::shared_ptr<Op> &input,
                const std::vector<uint32_t> &size);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
resize_nearest_neighbour(vosa::api::GraphBuilder &graph_builder,
                         const std::shared_ptr<Op> &input,
                         const std::vector<uint32_t> &size);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
rotate90(vosa::api::GraphBuilder &graph_builder,
         const std::shared_ptr<Op> &input, int rotation);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
round(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input,
      vosa::api::RoundBase::round_method_t round_method);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
sqrt(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
sub(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input_1,
    const std::shared_ptr<Op> &input_2);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
tosa_graph(vosa::api::GraphBuilder &graph_builder,
           const std::vector<std::shared_ptr<Op>> &inputs,
           const std::string &tosa_mlir_path, const std::string &tosa_func_name,
           const std::vector<uint32_t> &output_shape);

[[maybe_unused]] std::shared_ptr<vosa::api::Op>
where(vosa::api::GraphBuilder &graph_builder,
      const std::shared_ptr<Op> &input_1, const std::shared_ptr<Op> &input_2,
      const std::shared_ptr<Op> &where_input);

} // namespace vosa::api

#endif // VOSA_PIPELINES_API_OPS_H_
