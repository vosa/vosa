/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/array_types.h"

template <typename T>
vosa::ArrayBase::DType set_dtype();

template <>
vosa::ArrayBase::DType set_dtype<bool>() {
  return vosa::ArrayBase::BOOL;
}

template <>
vosa::ArrayBase::DType set_dtype<uint8_t>() {
  return vosa::ArrayBase::UINT8;
}

template <>
vosa::ArrayBase::DType set_dtype<uint16_t>() {
  return vosa::ArrayBase::UINT16;
}

template <>
vosa::ArrayBase::DType set_dtype<uint32_t>() {
  return vosa::ArrayBase::UINT32;
}

template <>
vosa::ArrayBase::DType set_dtype<int8_t>() {
  return vosa::ArrayBase::INT8;
}

template <>
vosa::ArrayBase::DType set_dtype<int16_t>() {
  return vosa::ArrayBase::INT16;
}

template <>
vosa::ArrayBase::DType set_dtype<int32_t>() {
  return vosa::ArrayBase::INT32;
}

template <>
vosa::ArrayBase::DType set_dtype<float>() {
  return vosa::ArrayBase::FLOAT32;
}

template <>
vosa::ArrayBase::DType set_dtype<double>() {
  return vosa::ArrayBase::FLOAT64;
}

vosa_serialization::DTYPE
vosa::ArrayBase::serialize_dtype(vosa::ArrayBase::DType dtype) {
  switch (dtype) {
  case ArrayBase::DType::BOOL:
    return vosa_serialization::DTYPE::DTYPE_BOOL;
  case ArrayBase::DType::UINT8:
    return vosa_serialization::DTYPE::DTYPE_UINT8;
  case ArrayBase::DType::UINT16:
    return vosa_serialization::DTYPE::DTYPE_UINT16;
  case ArrayBase::DType::UINT32:
    return vosa_serialization::DTYPE::DTYPE_UINT32;
  case ArrayBase::DType::INT8:
    return vosa_serialization::DTYPE::DTYPE_INT8;
  case ArrayBase::DType::INT16:
    return vosa_serialization::DTYPE::DTYPE_INT16;
  case ArrayBase::DType::INT32:
    return vosa_serialization::DTYPE::DTYPE_INT32;
  case ArrayBase::DType::FLOAT16:
    return vosa_serialization::DTYPE::DTYPE_FLOAT16;
  case ArrayBase::DType::FLOAT32:
    return vosa_serialization::DTYPE::DTYPE_FLOAT32;
  case ArrayBase::DType::FLOAT64:
    return vosa_serialization::DTYPE::DTYPE_FLOAT64;
  default:
    throw std::logic_error("");
  }
}

template <typename T>
vosa::ArrayBase::ArrayBase(const EigenArray<T, 4> &array) {
  const T *data_ptr = array.data();
  size_t data_size = array.size() * sizeof(T);
  raw_data_ptr = std::shared_ptr<uint8_t>(new uint8_t[data_size],
                                          std::default_delete<uint8_t[]>());
  memcpy(raw_data_ptr.get(), data_ptr, data_size);
  const auto &eigen_dimensions = array.dimensions();
  for (const auto &d : eigen_dimensions) {
    dimensions.push_back(d);
  }
  a_type = ARRAY;
  d_type = set_dtype<T>();
}

template <typename T>
vosa::ArrayBase::ArrayBase(const EigenImage<T> &image) {
  const T *data_ptr = image.data();
  size_t data_size = image.size() * sizeof(T);
  raw_data_ptr = std::shared_ptr<uint8_t>(new uint8_t[data_size],
                                          std::default_delete<uint8_t[]>());
  memcpy(raw_data_ptr.get(), data_ptr, data_size);
  const auto &eigen_dimensions = image.dimensions();
  for (const auto &d : eigen_dimensions) {
    dimensions.push_back(d);
  }
  a_type = IMAGE;
  d_type = set_dtype<T>();
}

template <typename T>
vosa::ArrayBase::ArrayBase(const EigenPlane<T> &plane) {
  const T *data_ptr = plane.data();
  size_t data_size = plane.size() * sizeof(T);
  raw_data_ptr = std::shared_ptr<uint8_t>(new uint8_t[data_size],
                                          std::default_delete<uint8_t[]>());
  memcpy(raw_data_ptr.get(), data_ptr, data_size);
  const auto &eigen_dimensions = plane.dimensions();
  for (const auto &d : eigen_dimensions) {
    dimensions.push_back(d);
  }
  a_type = PLANE;
  d_type = set_dtype<T>();
}

template <typename T>
vosa::ArrayBase::ArrayBase(const EigenVector<T> &vector) {
  const T *data_ptr = vector.data();
  size_t data_size = vector.size() * sizeof(T);
  raw_data_ptr = std::shared_ptr<uint8_t>(new uint8_t[data_size],
                                          std::default_delete<uint8_t[]>());
  memcpy(raw_data_ptr.get(), data_ptr, data_size);
  const auto &eigen_dimensions = vector.dimensions();
  for (const auto &d : eigen_dimensions) {
    dimensions.push_back(d);
  }
  a_type = VECTOR;
  d_type = set_dtype<T>();
}

template <typename T>
vosa::ArrayBase::ArrayBase(const EigenScalar<T> &scalar) {
  const T *data_ptr = scalar.data();
  size_t data_size = scalar.size() * sizeof(T);
  raw_data_ptr = std::shared_ptr<uint8_t>(new uint8_t[data_size],
                                          std::default_delete<uint8_t[]>());
  memcpy(raw_data_ptr.get(), data_ptr, data_size);
  const auto &eigen_dimensions = scalar.dimensions();
  for (const auto &d : eigen_dimensions) {
    dimensions.push_back(d);
  }
  a_type = SCALAR;
  d_type = set_dtype<T>();
}

template <typename T>
vosa::ArrayBase::ArrayBase(const T *data, uint32_t array_size) {
  size_t data_size = array_size * sizeof(T);
  raw_data_ptr = std::shared_ptr<uint8_t>(new uint8_t[data_size],
                                          std::default_delete<uint8_t[]>());
  memcpy(raw_data_ptr.get(), data, data_size);
  dimensions.push_back(array_size);
  a_type = DATA;
  d_type = set_dtype<T>();
}

template <typename T>
vosa::EigenArray<T, 4> vosa::ArrayBase::get_array() const {
  if (a_type != ARRAY) {
    throw vosa::ArrayBaseConversionError(a_type, ARRAY);
  }
  auto template_dtype = set_dtype<T>();
  if (template_dtype != d_type) {
    throw vosa::ArrayBaseConversionError(d_type, template_dtype);
  }
  T *data_ptr = (T *)raw_data_ptr.get();
  Eigen::TensorMap<vosa::EigenArray<T, 4>> eigen_map(
      data_ptr, dimensions[0], dimensions[1], dimensions[2], dimensions[3]);
  EigenArray<T, 4> output = eigen_map;
  return output;
}

template <typename T>
vosa::EigenImage<T> vosa::ArrayBase::get_image() const {
  if (a_type != IMAGE) {
    throw vosa::ArrayBaseConversionError(a_type, IMAGE);
  }
  auto template_dtype = set_dtype<T>();
  if (template_dtype != d_type) {
    throw vosa::ArrayBaseConversionError(d_type, template_dtype);
  }
  T *data_ptr = (T *)raw_data_ptr.get();
  Eigen::TensorMap<vosa::EigenImage<T>> eigen_map(data_ptr, dimensions[0],
                                                  dimensions[1], dimensions[2]);
  EigenImage<T> output = eigen_map;
  return output;
}

template <typename T>
vosa::EigenPlane<T> vosa::ArrayBase::get_plane() const {
  if (a_type != PLANE) {
    throw vosa::ArrayBaseConversionError(a_type, PLANE);
  }
  T *data_ptr = (T *)raw_data_ptr.get();
  Eigen::TensorMap<vosa::EigenPlane<T>> eigen_map(data_ptr, dimensions[0],
                                                  dimensions[1]);
  EigenPlane<T> output = eigen_map;
  return output;
}

template <typename T>
vosa::EigenVector<T> vosa::ArrayBase::get_vector() const {
  if (a_type != VECTOR) {
    throw vosa::ArrayBaseConversionError(a_type, VECTOR);
  }
  auto template_dtype = set_dtype<T>();
  if (template_dtype != d_type) {
    throw vosa::ArrayBaseConversionError(d_type, template_dtype);
  }
  T *data_ptr = (T *)raw_data_ptr.get();
  Eigen::TensorMap<vosa::EigenVector<T>> eigen_map(data_ptr, dimensions[0]);
  EigenVector<T> output = eigen_map;
  return output;
}

template <typename T>
vosa::EigenScalar<T> vosa::ArrayBase::get_scalar() const {
  if (a_type != SCALAR) {
    throw vosa::ArrayBaseConversionError(a_type, SCALAR);
  }
  auto template_dtype = set_dtype<T>();
  if (template_dtype != d_type) {
    throw vosa::ArrayBaseConversionError(d_type, template_dtype);
  }
  T *data_ptr = (T *)raw_data_ptr.get();
  Eigen::TensorMap<vosa::EigenScalar<T>> eigen_map(data_ptr);
  EigenScalar<T> output = eigen_map;
  return output;
}

template <typename T>
const T *vosa::ArrayBase::get_data() const {
  if (a_type != DATA) {
    throw vosa::ArrayBaseConversionError(a_type, DATA);
  }
  auto template_dtype = set_dtype<T>();
  if (template_dtype != d_type) {
    throw vosa::ArrayBaseConversionError(d_type, template_dtype);
  }
  T *data_ptr = (T *)raw_data_ptr.get();
  return data_ptr;
}

template vosa::ArrayBase::ArrayBase(const EigenArray<bool, 4> &);
template vosa::ArrayBase::ArrayBase(const EigenImage<bool> &);
template vosa::ArrayBase::ArrayBase(const EigenPlane<bool> &);
template vosa::ArrayBase::ArrayBase(const EigenVector<bool> &);
template vosa::ArrayBase::ArrayBase(const EigenScalar<bool> &);
template vosa::ArrayBase::ArrayBase(const bool *data, uint32_t array_size);

template vosa::EigenArray<bool, 4> vosa::ArrayBase::get_array() const;
template vosa::EigenImage<bool> vosa::ArrayBase::get_image() const;
template vosa::EigenPlane<bool> vosa::ArrayBase::get_plane() const;
template vosa::EigenVector<bool> vosa::ArrayBase::get_vector() const;
template vosa::EigenScalar<bool> vosa::ArrayBase::get_scalar() const;
template const bool *vosa::ArrayBase::get_data() const;

template vosa::ArrayBase::ArrayBase(const EigenArray<uint8_t, 4> &);
template vosa::ArrayBase::ArrayBase(const EigenImage<uint8_t> &);
template vosa::ArrayBase::ArrayBase(const EigenPlane<uint8_t> &);
template vosa::ArrayBase::ArrayBase(const EigenVector<uint8_t> &);
template vosa::ArrayBase::ArrayBase(const EigenScalar<uint8_t> &);
template vosa::ArrayBase::ArrayBase(const uint8_t *data, uint32_t array_size);

template vosa::EigenArray<uint8_t, 4> vosa::ArrayBase::get_array() const;
template vosa::EigenImage<uint8_t> vosa::ArrayBase::get_image() const;
template vosa::EigenPlane<uint8_t> vosa::ArrayBase::get_plane() const;
template vosa::EigenVector<uint8_t> vosa::ArrayBase::get_vector() const;
template vosa::EigenScalar<uint8_t> vosa::ArrayBase::get_scalar() const;
template const uint8_t *vosa::ArrayBase::get_data() const;

template vosa::ArrayBase::ArrayBase(const EigenArray<uint16_t, 4> &);
template vosa::ArrayBase::ArrayBase(const EigenImage<uint16_t> &);
template vosa::ArrayBase::ArrayBase(const EigenPlane<uint16_t> &);
template vosa::ArrayBase::ArrayBase(const EigenVector<uint16_t> &);
template vosa::ArrayBase::ArrayBase(const EigenScalar<uint16_t> &);
template vosa::ArrayBase::ArrayBase(const uint16_t *data, uint32_t array_size);

template vosa::EigenArray<uint16_t, 4> vosa::ArrayBase::get_array() const;
template vosa::EigenImage<uint16_t> vosa::ArrayBase::get_image() const;
template vosa::EigenPlane<uint16_t> vosa::ArrayBase::get_plane() const;
template vosa::EigenVector<uint16_t> vosa::ArrayBase::get_vector() const;
template vosa::EigenScalar<uint16_t> vosa::ArrayBase::get_scalar() const;
template const uint16_t *vosa::ArrayBase::get_data() const;

template vosa::ArrayBase::ArrayBase(const EigenArray<uint32_t, 4> &);
template vosa::ArrayBase::ArrayBase(const EigenImage<uint32_t> &);
template vosa::ArrayBase::ArrayBase(const EigenPlane<uint32_t> &);
template vosa::ArrayBase::ArrayBase(const EigenVector<uint32_t> &);
template vosa::ArrayBase::ArrayBase(const EigenScalar<uint32_t> &);
template vosa::ArrayBase::ArrayBase(const uint32_t *data, uint32_t array_size);

template vosa::EigenArray<uint32_t, 4> vosa::ArrayBase::get_array() const;
template vosa::EigenImage<uint32_t> vosa::ArrayBase::get_image() const;
template vosa::EigenPlane<uint32_t> vosa::ArrayBase::get_plane() const;
template vosa::EigenVector<uint32_t> vosa::ArrayBase::get_vector() const;
template vosa::EigenScalar<uint32_t> vosa::ArrayBase::get_scalar() const;
template const uint32_t *vosa::ArrayBase::get_data() const;

template vosa::ArrayBase::ArrayBase(const EigenArray<int8_t, 4> &);
template vosa::ArrayBase::ArrayBase(const EigenImage<int8_t> &);
template vosa::ArrayBase::ArrayBase(const EigenPlane<int8_t> &);
template vosa::ArrayBase::ArrayBase(const EigenVector<int8_t> &);
template vosa::ArrayBase::ArrayBase(const EigenScalar<int8_t> &);
template vosa::ArrayBase::ArrayBase(const int8_t *data, uint32_t array_size);

template vosa::EigenArray<int8_t, 4> vosa::ArrayBase::get_array() const;
template vosa::EigenImage<int8_t> vosa::ArrayBase::get_image() const;
template vosa::EigenPlane<int8_t> vosa::ArrayBase::get_plane() const;
template vosa::EigenVector<int8_t> vosa::ArrayBase::get_vector() const;
template vosa::EigenScalar<int8_t> vosa::ArrayBase::get_scalar() const;
template const int8_t *vosa::ArrayBase::get_data() const;

template vosa::ArrayBase::ArrayBase(const EigenArray<int16_t, 4> &);
template vosa::ArrayBase::ArrayBase(const EigenImage<int16_t> &);
template vosa::ArrayBase::ArrayBase(const EigenPlane<int16_t> &);
template vosa::ArrayBase::ArrayBase(const EigenVector<int16_t> &);
template vosa::ArrayBase::ArrayBase(const EigenScalar<int16_t> &);
template vosa::ArrayBase::ArrayBase(const int16_t *data, uint32_t array_size);

template vosa::EigenArray<int16_t, 4> vosa::ArrayBase::get_array() const;
template vosa::EigenImage<int16_t> vosa::ArrayBase::get_image() const;
template vosa::EigenPlane<int16_t> vosa::ArrayBase::get_plane() const;
template vosa::EigenVector<int16_t> vosa::ArrayBase::get_vector() const;
template vosa::EigenScalar<int16_t> vosa::ArrayBase::get_scalar() const;
template const int16_t *vosa::ArrayBase::get_data() const;

template vosa::ArrayBase::ArrayBase(const EigenArray<int32_t, 4> &);
template vosa::ArrayBase::ArrayBase(const EigenImage<int32_t> &);
template vosa::ArrayBase::ArrayBase(const EigenPlane<int32_t> &);
template vosa::ArrayBase::ArrayBase(const EigenVector<int32_t> &);
template vosa::ArrayBase::ArrayBase(const EigenScalar<int32_t> &);
template vosa::ArrayBase::ArrayBase(const int32_t *data, uint32_t array_size);

template vosa::EigenArray<int32_t, 4> vosa::ArrayBase::get_array() const;
template vosa::EigenImage<int32_t> vosa::ArrayBase::get_image() const;
template vosa::EigenPlane<int32_t> vosa::ArrayBase::get_plane() const;
template vosa::EigenVector<int32_t> vosa::ArrayBase::get_vector() const;
template vosa::EigenScalar<int32_t> vosa::ArrayBase::get_scalar() const;
template const int32_t *vosa::ArrayBase::get_data() const;

template vosa::ArrayBase::ArrayBase(const EigenArray<float, 4> &);
template vosa::ArrayBase::ArrayBase(const EigenImage<float> &);
template vosa::ArrayBase::ArrayBase(const EigenPlane<float> &);
template vosa::ArrayBase::ArrayBase(const EigenVector<float> &);
template vosa::ArrayBase::ArrayBase(const EigenScalar<float> &);
template vosa::ArrayBase::ArrayBase(const float *data, uint32_t array_size);

template vosa::EigenArray<float, 4> vosa::ArrayBase::get_array() const;
template vosa::EigenImage<float> vosa::ArrayBase::get_image() const;
template vosa::EigenPlane<float> vosa::ArrayBase::get_plane() const;
template vosa::EigenVector<float> vosa::ArrayBase::get_vector() const;
template vosa::EigenScalar<float> vosa::ArrayBase::get_scalar() const;
template const float *vosa::ArrayBase::get_data() const;

template vosa::ArrayBase::ArrayBase(const EigenArray<double, 4> &);
template vosa::ArrayBase::ArrayBase(const EigenImage<double> &);
template vosa::ArrayBase::ArrayBase(const EigenPlane<double> &);
template vosa::ArrayBase::ArrayBase(const EigenVector<double> &);
template vosa::ArrayBase::ArrayBase(const EigenScalar<double> &);
template vosa::ArrayBase::ArrayBase(const double *data, uint32_t array_size);

template vosa::EigenArray<double, 4> vosa::ArrayBase::get_array() const;
template vosa::EigenImage<double> vosa::ArrayBase::get_image() const;
template vosa::EigenPlane<double> vosa::ArrayBase::get_plane() const;
template vosa::EigenVector<double> vosa::ArrayBase::get_vector() const;
template vosa::EigenScalar<double> vosa::ArrayBase::get_scalar() const;
template const double *vosa::ArrayBase::get_data() const;
