"""
License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import enum
import typing

from vosa import types as types
from vosa.op import OPERATOR_MODES, Op, ExecutionPattern, InputPattern


class ArgMaxChannelWiseExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image.",
                )
            ),
            output_pattern=types.DataType(
                types.DType.UINT32,
                types.AType.IMAGE,
                shape=["H", "W", 1],
                description="Output single channel Image containing channel indices for maximum values for each pixel.",
            ),
        )


class ARG_MAX_CHANNELWISE_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT32 = enum.auto()
    I_I_UINT16_UINT32 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_UINT32 = enum.auto()
    I_I_INT16_UINT32 = enum.auto()
    I_I_INT32_UINT32 = enum.auto()
    I_I_FLOAT16_UINT32 = enum.auto()
    I_I_FLOAT32_UINT32 = enum.auto()


class ARG_MAX_CHANNELWISE(Op):
    """
    Find the maximum value af the `input` Image, for each spatial coordinate (along the channel dimension), and return the index of that channel,
    [latexmath]
    ++++
    output[y,x,0] = \\underset{c}{\\mathrm{argmax}}(input[y,x,c]).
    ++++
    """

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[ARG_MAX_CHANNELWISE_MODES, ArgMaxChannelWiseExecutionPattern]:
        return {
            ARG_MAX_CHANNELWISE_MODES.I_I_UINT8_UINT32: ArgMaxChannelWiseExecutionPattern(
                types.DType.UINT8
            ),
            ARG_MAX_CHANNELWISE_MODES.I_I_UINT16_UINT32: ArgMaxChannelWiseExecutionPattern(
                types.DType.UINT16
            ),
            ARG_MAX_CHANNELWISE_MODES.I_I_UINT32_UINT32: ArgMaxChannelWiseExecutionPattern(
                types.DType.UINT32
            ),
            ARG_MAX_CHANNELWISE_MODES.I_I_INT8_UINT32: ArgMaxChannelWiseExecutionPattern(
                types.DType.INT8
            ),
            ARG_MAX_CHANNELWISE_MODES.I_I_INT16_UINT32: ArgMaxChannelWiseExecutionPattern(
                types.DType.INT16
            ),
            ARG_MAX_CHANNELWISE_MODES.I_I_INT32_UINT32: ArgMaxChannelWiseExecutionPattern(
                types.DType.INT32
            ),
            ARG_MAX_CHANNELWISE_MODES.I_I_FLOAT16_UINT32: ArgMaxChannelWiseExecutionPattern(
                types.DType.FLOAT16
            ),
            ARG_MAX_CHANNELWISE_MODES.I_I_FLOAT32_UINT32: ArgMaxChannelWiseExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class ArgMinChannelWiseExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image.",
                )
            ),
            output_pattern=types.DataType(
                types.DType.UINT32,
                types.AType.IMAGE,
                shape=["H", "W", 1],
                description="Output single channel Image containing channel indices for minimum values for each pixel.",
            ),
        )


class ARG_MIN_CHANNELWISE_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT32 = enum.auto()
    I_I_UINT16_UINT32 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_UINT32 = enum.auto()
    I_I_INT16_UINT32 = enum.auto()
    I_I_INT32_UINT32 = enum.auto()
    I_I_FLOAT16_UINT32 = enum.auto()
    I_I_FLOAT32_UINT32 = enum.auto()


class ARG_MIN_CHANNELWISE(Op):
    """
    Find the minimum value af the `input` Image, for each spatial coordinate (along the channel dimension), and return the index of that channel,
    [latexmath]
    ++++
    output[y,x, 0] = \\underset{c}{\\mathrm{argmin}}(input[y,x,c]).
    ++++
    """

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[ARG_MIN_CHANNELWISE_MODES, ArgMinChannelWiseExecutionPattern]:
        return {
            ARG_MIN_CHANNELWISE_MODES.I_I_UINT8_UINT32: ArgMinChannelWiseExecutionPattern(
                types.DType.UINT8
            ),
            ARG_MIN_CHANNELWISE_MODES.I_I_UINT16_UINT32: ArgMinChannelWiseExecutionPattern(
                types.DType.UINT16
            ),
            ARG_MIN_CHANNELWISE_MODES.I_I_UINT32_UINT32: ArgMinChannelWiseExecutionPattern(
                types.DType.UINT32
            ),
            ARG_MIN_CHANNELWISE_MODES.I_I_INT8_UINT32: ArgMinChannelWiseExecutionPattern(
                types.DType.INT8
            ),
            ARG_MIN_CHANNELWISE_MODES.I_I_INT16_UINT32: ArgMinChannelWiseExecutionPattern(
                types.DType.INT16
            ),
            ARG_MIN_CHANNELWISE_MODES.I_I_INT32_UINT32: ArgMinChannelWiseExecutionPattern(
                types.DType.INT32
            ),
            ARG_MIN_CHANNELWISE_MODES.I_I_FLOAT16_UINT32: ArgMinChannelWiseExecutionPattern(
                types.DType.FLOAT16
            ),
            ARG_MIN_CHANNELWISE_MODES.I_I_FLOAT32_UINT32: ArgMinChannelWiseExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class ArgMaxPlaneWiseExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", 1],
                    description="Input Image.",
                )
            ),
            output_pattern=types.DataType(
                types.DType.UINT32,
                types.AType.VECTOR,
                shape=[2],
                description="Output vector containing the `y` and `x` coordinates for the maximum value in the plane",
            ),
        )


class ARG_MAX_PLANEWISE_MODES(OPERATOR_MODES):
    I_V_UINT8_UINT32 = enum.auto()
    I_V_UINT16_UINT32 = enum.auto()
    I_V_UINT32_UINT32 = enum.auto()
    I_V_INT8_UINT32 = enum.auto()
    I_V_INT16_UINT32 = enum.auto()
    I_V_INT32_UINT32 = enum.auto()
    I_V_FLOAT16_UINT32 = enum.auto()
    I_V_FLOAT32_UINT32 = enum.auto()


class ARG_MAX_PLANEWISE(Op):
    """
    Find the maximum value af the `input` single channel Image, and return the, `y,x` index pair of that location as a length 2 Vector,
    [latexmath]
    ++++
    output[:] = \\underset{y,x}{\\mathrm{argmax}}(input[y,x,0]).
    ++++
    """

    def to_flatbuffer(self):
        import vosa_serialization.ARG_MAX_PLANEWISE
        import vosa_serialization.ARG_MAX_PLANEWISE_MODE

        arg_max_planewise_modes = {
            ARG_MAX_PLANEWISE_MODES.I_V_UINT8_UINT32: vosa_serialization.ARG_MAX_PLANEWISE_MODE.ARG_MAX_PLANEWISE_MODE.I_V_UINT8_UINT32,
            ARG_MAX_PLANEWISE_MODES.I_V_UINT16_UINT32: vosa_serialization.ARG_MAX_PLANEWISE_MODE.ARG_MAX_PLANEWISE_MODE.I_V_UINT16_UINT32,
            ARG_MAX_PLANEWISE_MODES.I_V_UINT32_UINT32: vosa_serialization.ARG_MAX_PLANEWISE_MODE.ARG_MAX_PLANEWISE_MODE.I_V_UINT32_UINT32,
            ARG_MAX_PLANEWISE_MODES.I_V_INT8_UINT32: vosa_serialization.ARG_MAX_PLANEWISE_MODE.ARG_MAX_PLANEWISE_MODE.I_V_INT8_UINT32,
            ARG_MAX_PLANEWISE_MODES.I_V_INT16_UINT32: vosa_serialization.ARG_MAX_PLANEWISE_MODE.ARG_MAX_PLANEWISE_MODE.I_V_INT16_UINT32,
            ARG_MAX_PLANEWISE_MODES.I_V_INT32_UINT32: vosa_serialization.ARG_MAX_PLANEWISE_MODE.ARG_MAX_PLANEWISE_MODE.I_V_INT32_UINT32,
            ARG_MAX_PLANEWISE_MODES.I_V_FLOAT16_UINT32: vosa_serialization.ARG_MAX_PLANEWISE_MODE.ARG_MAX_PLANEWISE_MODE.I_V_FLOAT16_UINT32,
            ARG_MAX_PLANEWISE_MODES.I_V_FLOAT32_UINT32: vosa_serialization.ARG_MAX_PLANEWISE_MODE.ARG_MAX_PLANEWISE_MODE.I_V_FLOAT32_UINT32,
        }

        arg_max_planewise_t = vosa_serialization.ARG_MAX_PLANEWISE.ARG_MAX_PLANEWISET()
        arg_max_planewise_t.mode = arg_max_planewise_modes[self.get_operator_mode()]

        return arg_max_planewise_t

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[ARG_MAX_PLANEWISE_MODES, ArgMaxPlaneWiseExecutionPattern]:
        return {
            ARG_MAX_PLANEWISE_MODES.I_V_UINT8_UINT32: ArgMaxPlaneWiseExecutionPattern(
                types.DType.UINT8
            ),
            ARG_MAX_PLANEWISE_MODES.I_V_UINT16_UINT32: ArgMaxPlaneWiseExecutionPattern(
                types.DType.UINT16
            ),
            ARG_MAX_PLANEWISE_MODES.I_V_UINT32_UINT32: ArgMaxPlaneWiseExecutionPattern(
                types.DType.UINT32
            ),
            ARG_MAX_PLANEWISE_MODES.I_V_INT8_UINT32: ArgMaxPlaneWiseExecutionPattern(
                types.DType.INT8
            ),
            ARG_MAX_PLANEWISE_MODES.I_V_INT16_UINT32: ArgMaxPlaneWiseExecutionPattern(
                types.DType.INT16
            ),
            ARG_MAX_PLANEWISE_MODES.I_V_INT32_UINT32: ArgMaxPlaneWiseExecutionPattern(
                types.DType.INT32
            ),
            ARG_MAX_PLANEWISE_MODES.I_V_FLOAT16_UINT32: ArgMaxPlaneWiseExecutionPattern(
                types.DType.FLOAT16
            ),
            ARG_MAX_PLANEWISE_MODES.I_V_FLOAT32_UINT32: ArgMaxPlaneWiseExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class ArgMinPlaneWiseExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", 1],
                    description="Input Image.",
                )
            ),
            output_pattern=types.DataType(
                types.DType.UINT32,
                types.AType.VECTOR,
                shape=[2],
                description="Output vector containing the `y` and `x` coordinates for the minimum value in the plane",
            ),
        )


class ARG_MIN_PLANEWISE_MODES(OPERATOR_MODES):
    I_V_UINT8_UINT32 = enum.auto()
    I_V_UINT16_UINT32 = enum.auto()
    I_V_UINT32_UINT32 = enum.auto()
    I_V_INT8_UINT32 = enum.auto()
    I_V_INT16_UINT32 = enum.auto()
    I_V_INT32_UINT32 = enum.auto()
    I_V_FLOAT16_UINT32 = enum.auto()
    I_V_FLOAT32_UINT32 = enum.auto()


class ARG_MIN_PLANEWISE(Op):
    """
    Find the minimum value af the `input` single channel Image, and return the, `y,x` index pair of that location as a length 2 Vector,
    [latexmath]
    ++++
    output[:] = \\underset{y,x}{\\mathrm{argmin}}(input[y,x,0]).
    ++++
    """

    def to_flatbuffer(self):
        import vosa_serialization.ARG_MIN_PLANEWISE
        import vosa_serialization.ARG_MIN_PLANEWISE_MODE

        arg_min_planewise_modes = {
            ARG_MIN_PLANEWISE_MODES.I_V_UINT8_UINT32: vosa_serialization.ARG_MIN_PLANEWISE_MODE.ARG_MIN_PLANEWISE_MODE.I_V_UINT8_UINT32,
            ARG_MIN_PLANEWISE_MODES.I_V_UINT16_UINT32: vosa_serialization.ARG_MIN_PLANEWISE_MODE.ARG_MIN_PLANEWISE_MODE.I_V_UINT16_UINT32,
            ARG_MIN_PLANEWISE_MODES.I_V_UINT32_UINT32: vosa_serialization.ARG_MIN_PLANEWISE_MODE.ARG_MIN_PLANEWISE_MODE.I_V_UINT32_UINT32,
            ARG_MIN_PLANEWISE_MODES.I_V_INT8_UINT32: vosa_serialization.ARG_MIN_PLANEWISE_MODE.ARG_MIN_PLANEWISE_MODE.I_V_INT8_UINT32,
            ARG_MIN_PLANEWISE_MODES.I_V_INT16_UINT32: vosa_serialization.ARG_MIN_PLANEWISE_MODE.ARG_MIN_PLANEWISE_MODE.I_V_INT16_UINT32,
            ARG_MIN_PLANEWISE_MODES.I_V_INT32_UINT32: vosa_serialization.ARG_MIN_PLANEWISE_MODE.ARG_MIN_PLANEWISE_MODE.I_V_INT32_UINT32,
            ARG_MIN_PLANEWISE_MODES.I_V_FLOAT16_UINT32: vosa_serialization.ARG_MIN_PLANEWISE_MODE.ARG_MIN_PLANEWISE_MODE.I_V_FLOAT16_UINT32,
            ARG_MIN_PLANEWISE_MODES.I_V_FLOAT32_UINT32: vosa_serialization.ARG_MIN_PLANEWISE_MODE.ARG_MIN_PLANEWISE_MODE.I_V_FLOAT32_UINT32,
        }

        arg_min_planewise_t = vosa_serialization.ARG_MIN_PLANEWISE.ARG_MIN_PLANEWISET()
        arg_min_planewise_t.mode = arg_min_planewise_modes[self.get_operator_mode()]

        return arg_min_planewise_t

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[ARG_MIN_PLANEWISE_MODES, ArgMinPlaneWiseExecutionPattern]:
        return {
            ARG_MIN_PLANEWISE_MODES.I_V_UINT8_UINT32: ArgMinPlaneWiseExecutionPattern(
                types.DType.UINT8
            ),
            ARG_MIN_PLANEWISE_MODES.I_V_UINT16_UINT32: ArgMinPlaneWiseExecutionPattern(
                types.DType.UINT16
            ),
            ARG_MIN_PLANEWISE_MODES.I_V_UINT32_UINT32: ArgMinPlaneWiseExecutionPattern(
                types.DType.UINT32
            ),
            ARG_MIN_PLANEWISE_MODES.I_V_INT8_UINT32: ArgMinPlaneWiseExecutionPattern(
                types.DType.INT8
            ),
            ARG_MIN_PLANEWISE_MODES.I_V_INT16_UINT32: ArgMinPlaneWiseExecutionPattern(
                types.DType.INT16
            ),
            ARG_MIN_PLANEWISE_MODES.I_V_INT32_UINT32: ArgMinPlaneWiseExecutionPattern(
                types.DType.INT32
            ),
            ARG_MIN_PLANEWISE_MODES.I_V_FLOAT16_UINT32: ArgMinPlaneWiseExecutionPattern(
                types.DType.FLOAT16
            ),
            ARG_MIN_PLANEWISE_MODES.I_V_FLOAT32_UINT32: ArgMinPlaneWiseExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class ReduceInterpChannelwiseExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Image containing channels from which to interpolate.",
                ),
                types.DataType(
                    types.DType.FLOAT32,
                    types.AType.IMAGE,
                    shape=["H", "W", 1],
                    description="Plane containing channel values to select from.",
                ),
            ),
            output_pattern=types.DataType(
                types.DType.FLOAT32,
                types.AType.IMAGE,
                shape=["H", "W", 1],
                description="Output Plane containing interpolated channels from the input Image.",
            ),
        )


class REDUCE_INTERP_CHANNELWISE_MODES(OPERATOR_MODES):
    I_I_UINT8_FLOAT32 = enum.auto()
    I_I_UINT16_FLOAT32 = enum.auto()
    I_I_UINT32_FLOAT32 = enum.auto()
    I_I_INT8_FLOAT32 = enum.auto()
    I_I_INT16_FLOAT32 = enum.auto()
    I_I_INT32_FLOAT32 = enum.auto()
    I_I_FLOAT16_FLOAT32 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class REDUCE_INTERP_CHANNELWISE(Op):
    """
    Reduce an Image to a single channel Image by a pixelwise linear interpolation between channels in the `input_0` Image.
    The values in the `input_1` Image describe which channels of the `input_0` Image to interpolate between,
    [latexmath]
    ++++
    c_b = \\textrm{floor}(input_1[y,x,0]), \\\\
    output[y,x,0] = (c_b + 1 - input_1[y,x,0]) \\cdot input_0[ y, x, c_b] + (input_1[y,x,0] - c_b) \\cdot input_0[ y, x, c_b + 1].
    ++++
    For example, for a given pixel, a value in `input_1` equal to 2.5 means the value of that pixel in `output` will equal the average of the pixel in the second and third channels of `input_0`.

    NOTE: When accessing `input_0`, `c_b` and `c_b + 1` should be clamped to fall within [0, C-1]
    """

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[REDUCE_INTERP_CHANNELWISE_MODES, ExecutionPattern]:
        return {
            REDUCE_INTERP_CHANNELWISE_MODES.I_I_UINT8_FLOAT32: ReduceInterpChannelwiseExecutionPattern(
                types.DType.UINT8
            ),
            REDUCE_INTERP_CHANNELWISE_MODES.I_I_UINT16_FLOAT32: ReduceInterpChannelwiseExecutionPattern(
                types.DType.UINT16
            ),
            REDUCE_INTERP_CHANNELWISE_MODES.I_I_UINT32_FLOAT32: ReduceInterpChannelwiseExecutionPattern(
                types.DType.UINT32
            ),
            REDUCE_INTERP_CHANNELWISE_MODES.I_I_INT8_FLOAT32: ReduceInterpChannelwiseExecutionPattern(
                types.DType.INT8
            ),
            REDUCE_INTERP_CHANNELWISE_MODES.I_I_INT16_FLOAT32: ReduceInterpChannelwiseExecutionPattern(
                types.DType.INT16
            ),
            REDUCE_INTERP_CHANNELWISE_MODES.I_I_INT32_FLOAT32: ReduceInterpChannelwiseExecutionPattern(
                types.DType.INT32
            ),
            REDUCE_INTERP_CHANNELWISE_MODES.I_I_FLOAT16_FLOAT32: ReduceInterpChannelwiseExecutionPattern(
                types.DType.FLOAT16
            ),
            REDUCE_INTERP_CHANNELWISE_MODES.I_I_FLOAT32_FLOAT32: ReduceInterpChannelwiseExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class PlanewiseReductionExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    description="Single channel input Image.",
                    shape=["H", "W", 1],
                )
            ),
            output_pattern=types.DataType(
                dtype, types.AType.SCALAR, description="Output Scalar.", shape=[]
            ),
        )


class ChannelwiseReductionExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    description="Input Image.",
                    shape=["H", "W", "C"],
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                description="Output single channel Image.",
                shape=["H", "W", 1],
            ),
        )


class REDUCE_MAX_PLANEWISE_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class REDUCE_MAX_PLANEWISE(Op):
    """
    Find the maximum value in a plane and return it as a scalar,
    [latexmath]
    ++++
    output = \\max_{y, x} (input[y,x,0]).
    ++++
    """

    def to_flatbuffer(self):
        import vosa_serialization.REDUCE_MAX_PLANEWISE
        import vosa_serialization.REDUCE_MAX_PLANEWISE_MODE

        reduce_max_planewise_modes = {
            REDUCE_MAX_PLANEWISE_MODES.I_I_UINT8_UINT8: vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_I_UINT8_UINT8,
            REDUCE_MAX_PLANEWISE_MODES.I_I_UINT16_UINT16: vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_I_UINT16_UINT16,
            REDUCE_MAX_PLANEWISE_MODES.I_I_UINT32_UINT32: vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_I_UINT32_UINT32,
            REDUCE_MAX_PLANEWISE_MODES.I_I_INT8_INT8: vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_I_INT8_INT8,
            REDUCE_MAX_PLANEWISE_MODES.I_I_INT16_INT16: vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_I_INT16_INT16,
            REDUCE_MAX_PLANEWISE_MODES.I_I_INT32_INT32: vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_I_INT32_INT32,
            REDUCE_MAX_PLANEWISE_MODES.I_I_FLOAT16_FLOAT16: vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_I_FLOAT16_FLOAT16,
            REDUCE_MAX_PLANEWISE_MODES.I_I_FLOAT32_FLOAT32: vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_I_FLOAT32_FLOAT32,
        }

        reduce_max_planewise_t = (
            vosa_serialization.REDUCE_MAX_PLANEWISE.REDUCE_MAX_PLANEWISET()
        )
        reduce_max_planewise_t.mode = reduce_max_planewise_modes[
            self.get_operator_mode()
        ]

        return reduce_max_planewise_t

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[REDUCE_MAX_PLANEWISE_MODES, ExecutionPattern]:
        return {
            REDUCE_MAX_PLANEWISE_MODES.I_I_UINT8_UINT8: PlanewiseReductionExecutionPattern(
                types.DType.UINT8
            ),
            REDUCE_MAX_PLANEWISE_MODES.I_I_UINT16_UINT16: PlanewiseReductionExecutionPattern(
                types.DType.UINT16
            ),
            REDUCE_MAX_PLANEWISE_MODES.I_I_UINT32_UINT32: PlanewiseReductionExecutionPattern(
                types.DType.UINT32
            ),
            REDUCE_MAX_PLANEWISE_MODES.I_I_INT8_INT8: PlanewiseReductionExecutionPattern(
                types.DType.INT8
            ),
            REDUCE_MAX_PLANEWISE_MODES.I_I_INT16_INT16: PlanewiseReductionExecutionPattern(
                types.DType.INT16
            ),
            REDUCE_MAX_PLANEWISE_MODES.I_I_INT32_INT32: PlanewiseReductionExecutionPattern(
                types.DType.INT32
            ),
            REDUCE_MAX_PLANEWISE_MODES.I_I_FLOAT16_FLOAT16: PlanewiseReductionExecutionPattern(
                types.DType.FLOAT16
            ),
            REDUCE_MAX_PLANEWISE_MODES.I_I_FLOAT32_FLOAT32: PlanewiseReductionExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class REDUCE_MAX_CHANNELWISE_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class REDUCE_MAX_CHANNELWISE(Op):
    """
    Find the maximum value along the channel dimension for each pixel and return the result as a single channel Image,
    [latexmath]
    ++++
    output[ y,x,0] = \\max_{c} (input[y,x,0]).
    ++++
    """

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[REDUCE_MAX_CHANNELWISE_MODES, ExecutionPattern]:
        return {
            REDUCE_MAX_CHANNELWISE_MODES.I_I_UINT8_UINT8: ChannelwiseReductionExecutionPattern(
                types.DType.UINT8
            ),
            REDUCE_MAX_CHANNELWISE_MODES.I_I_UINT16_UINT16: ChannelwiseReductionExecutionPattern(
                types.DType.UINT16
            ),
            REDUCE_MAX_CHANNELWISE_MODES.I_I_UINT32_UINT32: ChannelwiseReductionExecutionPattern(
                types.DType.UINT32
            ),
            REDUCE_MAX_CHANNELWISE_MODES.I_I_INT8_INT8: ChannelwiseReductionExecutionPattern(
                types.DType.INT8
            ),
            REDUCE_MAX_CHANNELWISE_MODES.I_I_INT16_INT16: ChannelwiseReductionExecutionPattern(
                types.DType.INT16
            ),
            REDUCE_MAX_CHANNELWISE_MODES.I_I_INT32_INT32: ChannelwiseReductionExecutionPattern(
                types.DType.INT32
            ),
            REDUCE_MAX_CHANNELWISE_MODES.I_I_FLOAT16_FLOAT16: ChannelwiseReductionExecutionPattern(
                types.DType.FLOAT16
            ),
            REDUCE_MAX_CHANNELWISE_MODES.I_I_FLOAT32_FLOAT32: ChannelwiseReductionExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class REDUCE_MIN_CHANNELWISE_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class REDUCE_MIN_CHANNELWISE(Op):
    """
    Find the minimmum value along the channel dimension for each pixel and return the result as a single channel Image,
    [latexmath]
    ++++
    output[y,x,0] = \\min_c (input[y,x,c]).
    ++++
    """

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[REDUCE_MIN_CHANNELWISE_MODES, ExecutionPattern]:
        return {
            REDUCE_MIN_CHANNELWISE_MODES.I_I_UINT8_UINT8: ChannelwiseReductionExecutionPattern(
                types.DType.UINT8
            ),
            REDUCE_MIN_CHANNELWISE_MODES.I_I_UINT16_UINT16: ChannelwiseReductionExecutionPattern(
                types.DType.UINT16
            ),
            REDUCE_MIN_CHANNELWISE_MODES.I_I_UINT32_UINT32: ChannelwiseReductionExecutionPattern(
                types.DType.UINT32
            ),
            REDUCE_MIN_CHANNELWISE_MODES.I_I_INT8_INT8: ChannelwiseReductionExecutionPattern(
                types.DType.INT8
            ),
            REDUCE_MIN_CHANNELWISE_MODES.I_I_INT16_INT16: ChannelwiseReductionExecutionPattern(
                types.DType.INT16
            ),
            REDUCE_MIN_CHANNELWISE_MODES.I_I_INT32_INT32: ChannelwiseReductionExecutionPattern(
                types.DType.INT32
            ),
            REDUCE_MIN_CHANNELWISE_MODES.I_I_FLOAT16_FLOAT16: ChannelwiseReductionExecutionPattern(
                types.DType.FLOAT16
            ),
            REDUCE_MIN_CHANNELWISE_MODES.I_I_FLOAT32_FLOAT32: ChannelwiseReductionExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class REDUCE_MIN_PLANEWISE_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class REDUCE_MIN_PLANEWISE(Op):
    """
    Find the minimum value in a single channel Image and return it as a scalar,
    [latexmath]
    ++++
    output = \\min_{y, x} (input[y,x,0]).
    ++++
    """

    def to_flatbuffer(self):
        import vosa_serialization.REDUCE_MIN_PLANEWISE
        import vosa_serialization.REDUCE_MIN_PLANEWISE_MODE

        reduce_min_planewise_modes = {
            REDUCE_MIN_PLANEWISE_MODES.I_I_UINT8_UINT8: vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_I_UINT8_UINT8,
            REDUCE_MIN_PLANEWISE_MODES.I_I_UINT16_UINT16: vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_I_UINT16_UINT16,
            REDUCE_MIN_PLANEWISE_MODES.I_I_UINT32_UINT32: vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_I_UINT32_UINT32,
            REDUCE_MIN_PLANEWISE_MODES.I_I_INT8_INT8: vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_I_INT8_INT8,
            REDUCE_MIN_PLANEWISE_MODES.I_I_INT16_INT16: vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_I_INT16_INT16,
            REDUCE_MIN_PLANEWISE_MODES.I_I_INT32_INT32: vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_I_INT32_INT32,
            REDUCE_MIN_PLANEWISE_MODES.I_I_FLOAT16_FLOAT16: vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_I_FLOAT16_FLOAT16,
            REDUCE_MIN_PLANEWISE_MODES.I_I_FLOAT32_FLOAT32: vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_I_FLOAT32_FLOAT32,
        }

        reduce_min_planewise_t = (
            vosa_serialization.REDUCE_MIN_PLANEWISE.REDUCE_MIN_PLANEWISET()
        )
        reduce_min_planewise_t.mode = reduce_min_planewise_modes[
            self.get_operator_mode()
        ]

        return reduce_min_planewise_t

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[REDUCE_MIN_PLANEWISE_MODES, ExecutionPattern]:
        return {
            REDUCE_MIN_PLANEWISE_MODES.I_I_UINT8_UINT8: PlanewiseReductionExecutionPattern(
                types.DType.UINT8
            ),
            REDUCE_MIN_PLANEWISE_MODES.I_I_UINT16_UINT16: PlanewiseReductionExecutionPattern(
                types.DType.UINT16
            ),
            REDUCE_MIN_PLANEWISE_MODES.I_I_UINT32_UINT32: PlanewiseReductionExecutionPattern(
                types.DType.UINT32
            ),
            REDUCE_MIN_PLANEWISE_MODES.I_I_INT8_INT8: PlanewiseReductionExecutionPattern(
                types.DType.INT8
            ),
            REDUCE_MIN_PLANEWISE_MODES.I_I_INT16_INT16: PlanewiseReductionExecutionPattern(
                types.DType.INT16
            ),
            REDUCE_MIN_PLANEWISE_MODES.I_I_INT32_INT32: PlanewiseReductionExecutionPattern(
                types.DType.INT32
            ),
            REDUCE_MIN_PLANEWISE_MODES.I_I_FLOAT16_FLOAT16: PlanewiseReductionExecutionPattern(
                types.DType.FLOAT16
            ),
            REDUCE_MIN_PLANEWISE_MODES.I_I_FLOAT32_FLOAT32: PlanewiseReductionExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class REDUCE_SUM_PLANEWISE_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class PlanewiseSumReductionExecutionPattern(ExecutionPattern):
    def __init__(self, input_dtype, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    input_dtype,
                    types.AType.IMAGE,
                    description="Input single channel Image.",
                    shape=["H", "W", 1],
                )
            ),
            output_pattern=types.DataType(
                output_dtype, types.AType.SCALAR, description="Output Scalar.", shape=[]
            ),
        )


class REDUCE_SUM_PLANEWISE(Op):
    """
    Sum the pixel values in a sinle channel Image to a Scalar,
    [latexmath]
    ++++
    output = \\sum_{y,x} input[y,x,0]
    ++++
    Naive summation is used, thererfore if the summation accumulator increases above its max representable value it will overflow.
    """

    def to_flatbuffer(self):
        import vosa_serialization.REDUCE_SUM_PLANEWISE
        import vosa_serialization.REDUCE_SUM_PLANEWISE_MODE

        reduce_sum_planewise_modes = {
            REDUCE_SUM_PLANEWISE_MODES.I_I_UINT8_UINT8: vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_I_UINT8_UINT8,
            REDUCE_SUM_PLANEWISE_MODES.I_I_UINT16_UINT16: vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_I_UINT16_UINT16,
            REDUCE_SUM_PLANEWISE_MODES.I_I_UINT32_UINT32: vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_I_UINT32_UINT32,
            REDUCE_SUM_PLANEWISE_MODES.I_I_INT8_INT8: vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_I_INT8_INT8,
            REDUCE_SUM_PLANEWISE_MODES.I_I_INT16_INT16: vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_I_INT16_INT16,
            REDUCE_SUM_PLANEWISE_MODES.I_I_INT32_INT32: vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_I_INT32_INT32,
            REDUCE_SUM_PLANEWISE_MODES.I_I_FLOAT16_FLOAT16: vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_I_FLOAT16_FLOAT16,
            REDUCE_SUM_PLANEWISE_MODES.I_I_FLOAT32_FLOAT32: vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_I_FLOAT32_FLOAT32,
        }

        reduce_sum_planewise_t = (
            vosa_serialization.REDUCE_SUM_PLANEWISE.REDUCE_SUM_PLANEWISET()
        )
        reduce_sum_planewise_t.mode = reduce_sum_planewise_modes[
            self.get_operator_mode()
        ]

        return reduce_sum_planewise_t

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[REDUCE_SUM_PLANEWISE_MODES, ExecutionPattern]:
        return {
            REDUCE_SUM_PLANEWISE_MODES.I_I_UINT8_UINT8: PlanewiseSumReductionExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            REDUCE_SUM_PLANEWISE_MODES.I_I_UINT16_UINT16: PlanewiseSumReductionExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            REDUCE_SUM_PLANEWISE_MODES.I_I_UINT32_UINT32: PlanewiseSumReductionExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
            REDUCE_SUM_PLANEWISE_MODES.I_I_INT8_INT8: PlanewiseSumReductionExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            REDUCE_SUM_PLANEWISE_MODES.I_I_INT16_INT16: PlanewiseSumReductionExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            REDUCE_SUM_PLANEWISE_MODES.I_I_INT32_INT32: PlanewiseSumReductionExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            REDUCE_SUM_PLANEWISE_MODES.I_I_FLOAT16_FLOAT16: PlanewiseSumReductionExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            REDUCE_SUM_PLANEWISE_MODES.I_I_FLOAT32_FLOAT32: PlanewiseSumReductionExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class REDUCE_SUM_CHANNELWISE_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class ReduceSumChannelwiseExecutionPattern(ExecutionPattern):
    def __init__(self, input_dtype, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    input_dtype,
                    types.AType.IMAGE,
                    description="Input Image.",
                    shape=["H", "W", "C"],
                )
            ),
            output_pattern=types.DataType(
                output_dtype,
                types.AType.IMAGE,
                description="Output single channel Image.",
                shape=["H", "W", 1],
            ),
        )


class REDUCE_SUM_CHANNELWISE(Op):
    """
    Sum the pixel values in an Image channel-wise to a single channel Image,
    [latexmath]
    ++++
    output[y,x,0] = \\sum_{c} input[y,x,c]
    ++++
    Naïve summation is used, therefore it will overflow if the summation accumulator increases above its max representable value.
    """

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[REDUCE_SUM_CHANNELWISE_MODES, ExecutionPattern]:
        return {
            REDUCE_SUM_CHANNELWISE_MODES.I_I_UINT8_UINT8: ReduceSumChannelwiseExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            REDUCE_SUM_CHANNELWISE_MODES.I_I_UINT16_UINT16: ReduceSumChannelwiseExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            REDUCE_SUM_CHANNELWISE_MODES.I_I_UINT32_UINT32: ReduceSumChannelwiseExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
            REDUCE_SUM_CHANNELWISE_MODES.I_I_INT8_INT8: ReduceSumChannelwiseExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            REDUCE_SUM_CHANNELWISE_MODES.I_I_INT16_INT16: ReduceSumChannelwiseExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            REDUCE_SUM_CHANNELWISE_MODES.I_I_INT32_INT32: ReduceSumChannelwiseExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            REDUCE_SUM_CHANNELWISE_MODES.I_I_FLOAT16_FLOAT16: ReduceSumChannelwiseExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            REDUCE_SUM_CHANNELWISE_MODES.I_I_FLOAT32_FLOAT32: ReduceSumChannelwiseExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }
