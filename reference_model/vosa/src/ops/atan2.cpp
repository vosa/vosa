/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "atan2.h"

std::shared_ptr<vosa::Op> vosa::Atan2Base::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_atan2 = vosa_operator_union.AsATAN2();
  assert(serialized_atan2);
  auto mode = serialized_atan2->mode;
  switch (mode) {
  case vosa_serialization::ATAN2_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::Atan2<float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::Atan2<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {
  const EigenImage<T> &input_0 = inputs[0]->template get_image<T>();
  const EigenImage<T> &input_1 = inputs[1]->template get_image<T>();

  uint32_t input_0_height = input_0.dimension(0);
  uint32_t input_0_width = input_0.dimension(1);
  uint32_t input_0_channels = input_0.dimension(2);

  uint32_t input_1_height = input_1.dimension(0);
  uint32_t input_1_width = input_1.dimension(1);
  uint32_t input_1_channels = input_1.dimension(2);

  assert(input_0_height == input_1_height);
  assert(input_0_width == input_1_width);
  assert(input_0_channels == input_1_channels);

  auto output_image =
      EigenImage<T>(input_0_height, input_0_width, input_0_channels);

  for (uint32_t height_index = 0; height_index < input_0_height;
       height_index++) {
    for (uint32_t width_index = 0; width_index < input_0_width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < input_0_channels;
           channel_index++) {

        output_image(height_index, width_index, channel_index) =
            atan2f(input_0(height_index, width_index, channel_index),
                   input_1(height_index, width_index, channel_index));
      }
    }
  }
  return std::make_shared<vosa::ArrayBase>(output_image);
}

template <>
vosa_serialization::ATAN2_MODE
vosa::Atan2<float>::get_serialization_mode() const {
  return vosa_serialization::ATAN2_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Atan2<T>::serialize(const std::string &op_name,
                          const std::vector<std::string> &input_names) const {

  vosa_serialization::ATAN2T atan2_;
  atan2_.mode = get_serialization_mode();

  auto gamma_correction_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  gamma_correction_operator->op.Set(std::move(atan2_));
  gamma_correction_operator->name = op_name;
  gamma_correction_operator->inputs = input_names;
  return gamma_correction_operator;
}

template <typename T>
vosa::ArrayBase::DType vosa::Atan2<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template class vosa::Atan2<float>;