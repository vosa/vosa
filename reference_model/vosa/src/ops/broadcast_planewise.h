/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_CODE_BROADCAST_SCALAR_H
#define VOSA_CODE_BROADCAST_SCALAR_H

#include "vosa/op.h"

namespace vosa {

class BroadcastPlanewiseBase : public Op {
public:
  static std::shared_ptr<Op>
  parse_serialized(const vosa_serialization::VosaOperatorUnion &);
};

template <typename T>
class BroadcastPlanewise : public BroadcastPlanewiseBase {
public:
  explicit BroadcastPlanewise(std::vector<uint32_t> size)
      : size(std::move(size)) {
    // Enforce that the `size` vector should only have 2 values: [height, width]
    assert(this->size.size() == 2);
  }

  EigenImage<T> runme(const EigenImage<T> &input) const;

  [[nodiscard]] ArrayBase::DType output_dtype() const final;

  [[nodiscard]] std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &) const final;

  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

private:
  [[nodiscard]] vosa_serialization::BROADCAST_PLANEWISE_MODE
  get_serialization_mode() const;

  std::vector<uint32_t> size;
};

} // namespace vosa
#endif // VOSA_CODE_BROADCAST_SCALAR_H
