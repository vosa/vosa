/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/where.h"
#include "test_bench.h"
#include "vosa/vosa.h"

TEST(test_where, test_where) {

  auto input_array = vosa::EigenImage<uint16_t>(1, 4, 4);
  input_array.setValues(
      {{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}}});

  auto where_mask = vosa::EigenImage<bool>(1, 4, 4);

  where_mask.setValues({{{true, false, false, false},
                         {false, true, false, false},
                         {false, false, true, false},
                         {false, false, false, true}}});

  auto zeros_array = vosa::EigenImage<uint16_t>(1, 4, 4);
  zeros_array.setConstant(0);

  auto image_input = std::make_shared<vosa::ArrayBase>(input_array);
  auto zeros_input = std::make_shared<vosa::ArrayBase>(zeros_array);
  auto where_input = std::make_shared<vosa::ArrayBase>(where_mask);
  auto where_op = vosa::Where<uint16_t>();

  auto output = where_op.eval({image_input, zeros_input, where_input});

  auto expand_output = output->get_image<uint16_t>();

  auto expected_output = vosa::EigenImage<uint16_t>(1, 4, 4);
  expected_output.setValues(
      {{{1, 0, 0, 0}, {0, 6, 0, 0}, {0, 0, 11, 0}, {0, 0, 0, 16}}});
  auto test_output =
      vosa::test::TestBench<uint16_t, int32_t>::compareEigenImages(
          expected_output, expand_output);
  EXPECT_TRUE(test_output);
}