/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "clamp.h"

std::shared_ptr<vosa::Op> vosa::ClampBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_clamp = vosa_operator_union.AsCLAMP();
  assert(serialized_clamp);
  auto mode = serialized_clamp->mode;
  switch (mode) {
  case vosa_serialization::CLAMP_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::Clamp<uint8_t>>();
  }
  case vosa_serialization::CLAMP_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::Clamp<uint16_t>>();
  }
  case vosa_serialization::CLAMP_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::Clamp<uint32_t>>();
  }
  case vosa_serialization::CLAMP_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::Clamp<int8_t>>();
  }
  case vosa_serialization::CLAMP_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::Clamp<int16_t>>();
  }
  case vosa_serialization::CLAMP_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::Clamp<int32_t>>();
  }
  case vosa_serialization::CLAMP_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::Clamp<float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::EigenImage<T> vosa::Clamp<T>::runme(const EigenImage<T> &input_0,
                                          const EigenScalar<T> &input_1,
                                          const EigenScalar<T> &input_2) const {

  uint32_t height = input_0.dimension(0);
  uint32_t width = input_0.dimension(1);
  uint32_t num_channels = input_0.dimension(2);

  EigenImage<T> output{height, width, num_channels};

  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < num_channels;
           channel_index++) {
        T input_value = input_0(height_index, width_index, channel_index);
        T output_value = std::max(std::min(input_value, input_2()), input_1());
        output(height_index, width_index, channel_index) = output_value;
      }
    }
  }
  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::Clamp<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::Clamp<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input_0 = inputs[0]->template get_image<T>();
  const EigenScalar<T> &input_1 = inputs[1]->template get_scalar<T>();
  const EigenScalar<T> &input_2 = inputs[2]->template get_scalar<T>();
  auto typed_output = this->runme(input_0, input_1, input_2);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::CLAMP_MODE
vosa::Clamp<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::CLAMP_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::CLAMP_MODE
vosa::Clamp<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::CLAMP_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::CLAMP_MODE
vosa::Clamp<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::CLAMP_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::CLAMP_MODE
vosa::Clamp<int8_t>::get_serialization_mode() const {
  return vosa_serialization::CLAMP_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::CLAMP_MODE
vosa::Clamp<int16_t>::get_serialization_mode() const {
  return vosa_serialization::CLAMP_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::CLAMP_MODE
vosa::Clamp<int32_t>::get_serialization_mode() const {
  return vosa_serialization::CLAMP_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::CLAMP_MODE
vosa::Clamp<float>::get_serialization_mode() const {
  return vosa_serialization::CLAMP_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Clamp<T>::serialize(const std::string &op_name,
                          const std::vector<std::string> &input_names) const {
  vosa_serialization::CLAMPT clamp;
  clamp.mode = get_serialization_mode();

  auto clamp_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  clamp_operator->op.Set(clamp);
  clamp_operator->name = op_name;
  clamp_operator->inputs = input_names;
  return clamp_operator;
}

template class vosa::Clamp<uint8_t>;

template class vosa::Clamp<uint16_t>;

template class vosa::Clamp<uint32_t>;

template class vosa::Clamp<int8_t>;

template class vosa::Clamp<int16_t>;

template class vosa::Clamp<int32_t>;

template class vosa::Clamp<float>;