/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "array_serialization.h"
#include "test_bench.h"
#include "vosa/vosa.h"
#include "gtest/gtest.h"

TEST(test_arraybase_serialization, test_arraybase_serialization) {

  auto test_plane = vosa::EigenPlane<uint16_t>(3, 3);
  test_plane.setValues({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
  std::string test_name = "name";

  auto array_base = vosa::ArrayBase(test_plane);
  auto serialized_array_base = vosa::test::serialize(test_name, array_base);

  auto deserialized_array_base = vosa::test::deserialize(serialized_array_base);

  auto output_plane = deserialized_array_base.get_plane<uint16_t>();

  auto compare_plane =
      vosa::test::TestBench<uint16_t, int32_t>::compareEigenPlanes(
          test_plane, output_plane);
  EXPECT_TRUE(compare_plane);

  auto deserialized_name = vosa::test::deserialize_name(serialized_array_base);
  EXPECT_EQ(test_name, deserialized_name);
}