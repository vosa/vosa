/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <cstdio>
#include <iostream>
#include <map>

#include "test_bench.h"

template <typename T>
vosa::EigenPlane<T>
vosa::test::Packing<T>::extract_plane(const vosa::EigenImage<T> &image,
                                      uint32_t channel) {

  uint32_t height = image.dimension(0);
  uint32_t width = image.dimension(1);

  vosa::EigenPlane<T> output(height, width);

  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      output(height_index, width_index) =
          image(height_index, width_index, channel);
    }
  }

  return output;
}

template <typename T>
vosa::EigenImage<T> vosa::test::Packing<T>::concat_planes(
    const std::vector<vosa::EigenPlane<T>> &planes) {
  const uint32_t height = planes[0].dimension(0),
                 width = planes[0].dimension(1), channels = planes.size();

  for (const auto &plane : planes)
    assert(height == plane.dimension(0) && width == plane.dimension(1));

  EigenImage<T> image(height, width, channels);
  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      for (uint32_t c = 0; c < channels; c++)
        image(y, x, c) = planes[c](y, x);

  return image;
}

template <typename T>
vosa::EigenImage<T>
vosa::test::Packing<T>::pack_plane(const vosa::EigenPlane<T> &plane) {
  uint32_t height = plane.dimension(0);
  uint32_t width = plane.dimension(1);

  vosa::EigenImage<T> output(height, width, 1);

  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      output(height_index, width_index, 0) = plane(height_index, width_index);
    }
  }

  return output;
}

template <typename T>
std::vector<T>
vosa::test::Utils<T>::get_min_max(const vosa::EigenPlane<T> &plane) {

  T min_value = plane(0, 0), max_value = plane(0, 0);

  const uint32_t height = plane.dimension(0), width = plane.dimension(1);
  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++) {
      min_value = std::min(min_value, plane(y, x));
      max_value = std::max(max_value, plane(y, x));
    }

  return {min_value, max_value};
}

template <typename T>
vosa::EigenImage<uint8_t>
vosa::test::Utils<T>::normalise(const vosa::EigenImage<T> &image) {
  const uint32_t channels = image.dimension(2);
  std::vector<vosa::EigenPlane<uint8_t>> planes(channels);
  for (uint32_t c = 0; c < channels; c++) {
    vosa::EigenPlane<T> plane = vosa::test::Packing<T>::extract_plane(image, c);
    planes[c] = vosa::test::Utils<T>::normalise(plane);
  }

  return vosa::test::Packing<uint8_t>::concat_planes(planes);
}

template <typename T>
vosa::EigenPlane<uint8_t>
vosa::test::Utils<T>::normalise(const vosa::EigenPlane<T> &plane) {
  std::vector<T> min_max_values = get_min_max(plane);
  const double min_value = min_max_values[0], max_value = min_max_values[1];
  const uint32_t height = plane.dimension(0), width = plane.dimension(1);
  EigenPlane<uint8_t> normalised(height, width);
  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++) {
      double value =
          ((double)plane(y, x) - min_value) / (max_value - min_value);
      value *= 255.0;
      normalised(y, x) = static_cast<uint8_t>(value);
    }

  return normalised;
}

template <typename inputT, typename compareT>
bool vosa::test::TestBench<inputT, compareT>::compareEigenImages(
    const vosa::EigenImage<inputT> &image_1,
    const vosa::EigenImage<inputT> &image_2, inputT allowed_diff, bool print,
    const std::string &comment) {

  if (!comment.empty())
    std::cout << comment << std::endl;
  uint32_t num_channels_1 = image_1.dimension(2);
  uint32_t num_channels_2 = image_2.dimension(2);

  if (num_channels_1 != num_channels_2)
    return false;

  bool output = true;
  for (uint32_t channel_index = 0; channel_index < num_channels_1;
       channel_index++) {

    auto plane_1 = Packing<inputT>::extract_plane(image_1, channel_index);
    auto plane_2 = Packing<inputT>::extract_plane(image_2, channel_index);
    output &=
        compareEigenPlanes(plane_1, plane_2, allowed_diff, print, comment);
  }
  return output;
}

template <typename inputT, typename compareT>
bool vosa::test::TestBench<inputT, compareT>::compareEigenPlanes(
    const EigenPlane<inputT> &plane_1, const EigenPlane<inputT> &plane_2,
    inputT allowed_diff, bool print, const std::string &comment) {
  if (!comment.empty())
    std::cout << comment << std::endl;

  EigenPlane<compareT> plane_1_32 = plane_1.template cast<compareT>();
  EigenPlane<compareT> plane_2_32 = plane_2.template cast<compareT>();

  uint32_t plane_1_dim_0 = plane_1.dimension(0);
  uint32_t plane_2_dim_0 = plane_2.dimension(0);

  uint32_t plane_1_dim_1 = plane_1.dimension(1);
  uint32_t plane_2_dim_1 = plane_2.dimension(1);

  if (plane_1_dim_0 != plane_2_dim_0 || plane_1_dim_1 != plane_2_dim_1) {
    std::cout << "Non-matching plane dimensions: " << plane_1_dim_0 << "x"
              << plane_1_dim_1 << " != " << plane_2_dim_0 << "x"
              << plane_2_dim_1 << std::endl;
    return false;
  }

  if (print) {

    std::cout << plane_1_32 << std::endl << std::endl;
    std::cout << plane_2_32 << std::endl << std::endl;
  }

  EigenPlane<compareT> compare_plane{plane_1_32.dimension(0),
                                     plane_1_32.dimension(1)};

  std::map<compareT, std::vector<std::pair<uint32_t, uint32_t>>> errors{};
  std::vector<inputT> all_errors;
  for (uint32_t height_index = 0; height_index < plane_1_32.dimension(0);
       height_index++) {
    for (uint32_t width_index = 0; width_index < plane_2_32.dimension(1);
         width_index++) {
      inputT val_1 = plane_1_32(height_index, width_index);
      inputT val_2 = plane_2_32(height_index, width_index);

      // Round to the nearest 5 digits (`diff` will always be positive)
      inputT diff = val_2 > val_1 ? val_2 - val_1 : val_1 - val_2;
      diff = (inputT)(std::round(diff * 1e5) / 1e5);

      compare_plane(height_index, width_index) = (compareT)diff;
      if (diff > 0) // Do not include `diff == 0` in the `errors`
        errors[diff].push_back(std::make_pair(height_index, width_index));
      if (diff > allowed_diff)
        all_errors.push_back(diff);
    }
  }

  if (print) {
    std::cout << compare_plane << std::endl << std::endl;

    for (auto &it : errors) {
      int coord_amount = 0;
      std::cout << it.first << "(" << it.second.size() << "): ";
      for (auto &it2 : it.second) {
        std::cout << '{' << it2.first << ", " << it2.second << "} ";
        coord_amount++;
        if (coord_amount >= 20)
          break;
      }
      std::cout << std::endl;
    }
  }

  if (print && !all_errors.empty())
    std::printf("Max Diff: %.5f\n", (float)(*std::max_element(
                                        all_errors.begin(), all_errors.end())));

  // Were there any errors > `allowed_diff` ?
  return all_errors.empty();
}

template <typename inputT, typename compareT>
bool vosa::test::TestBench<inputT, compareT>::compareEigenVectors(
    const EigenVector<inputT> &vector_1, const EigenVector<inputT> &vector_2,
    inputT allowed_diff, bool print) {

  EigenVector<compareT> vector_1_32 = vector_1.template cast<compareT>();
  EigenVector<compareT> vector_2_32 = vector_2.template cast<compareT>();

  uint32_t plane_1_dim_0 = vector_1.dimension(0);
  uint32_t plane_2_dim_0 = vector_2.dimension(0);

  if (plane_1_dim_0 != plane_2_dim_0) {
    std::cout << "Non-matching vector dimensions" << std::endl;
    return false;
  }

  if (print) {

    std::cout << vector_1_32 << std::endl << std::endl;
    std::cout << vector_2_32 << std::endl << std::endl;
  }

  EigenVector<compareT> compare_vector{vector_1_32.dimension(0)};

  inputT max_diff = 0;
  std::map<compareT, std::vector<uint32_t>> errors{};
  for (uint32_t vector_index = 0; vector_index < vector_2_32.dimension(0);
       vector_index++) {
    inputT val_1 = vector_1_32(vector_index);
    inputT val_2 = vector_2_32(vector_index);

    // Round to the nearest 5 digits (`diff` will always be positive)
    inputT diff = val_2 > val_1 ? val_2 - val_1 : val_1 - val_2;
    diff = (inputT)(std::round(diff * 1e5) / 1e5);

    compare_vector(vector_index) = (compareT)diff;
    max_diff = std::max(max_diff, diff);
    errors[diff].push_back(vector_index);
  }

  if (print) {
    std::cout << compare_vector << std::endl << std::endl;

    for (auto &it : errors) {
      int coord_amount = 0;
      std::cout << it.first << "(" << it.second.size() << "): ";
      for (auto &it2 : it.second) {
        std::cout << '{' << it2 << "} ";
        coord_amount++;
        if (coord_amount >= 20)
          break;
      }
      std::cout << std::endl;
    }
  }

  return max_diff <= allowed_diff;
}

template class vosa::test::TestBench<bool, int32_t>;

template class vosa::test::TestBench<uint8_t, uint8_t>;

template class vosa::test::TestBench<uint16_t, uint16_t>;

template class vosa::test::TestBench<uint16_t, int32_t>;

template class vosa::test::TestBench<int16_t, int16_t>;

template class vosa::test::TestBench<uint8_t, uint32_t>;

template class vosa::test::TestBench<uint8_t, int32_t>;

template class vosa::test::TestBench<uint32_t, int32_t>;

template class vosa::test::TestBench<int8_t, int32_t>;

template class vosa::test::TestBench<int16_t, int32_t>;

template class vosa::test::TestBench<int32_t, int32_t>;

template class vosa::test::TestBench<float, int32_t>;

template class vosa::test::TestBench<float, float>;

template class vosa::test::Packing<uint8_t>;

template class vosa::test::Packing<uint16_t>;

template class vosa::test::Packing<uint32_t>;

template class vosa::test::Packing<int8_t>;

template class vosa::test::Packing<int16_t>;

template class vosa::test::Packing<int32_t>;

template class vosa::test::Packing<float>;

template class vosa::test::Utils<uint8_t>;

template class vosa::test::Utils<uint16_t>;

template class vosa::test::Utils<uint32_t>;

template class vosa::test::Utils<int8_t>;

template class vosa::test::Utils<int16_t>;

template class vosa::test::Utils<int32_t>;

template class vosa::test::Utils<float>;
