// run command without FileCheck to see output (or add "--dump-input always" FileCheck option)
// RUN: vosa-opt -vosa-to-tosa %s | %test_src_root/e2e-runner | FileCheck %s

func.func @main() {
  %input_si32 = vosa.constant dense<
  [
	[[1,   10,  100], [   2,   20,  200], [   3,   30,  300]],

       	[[4,   40,  400], [   5,   50,  500], [6,   60,  600]],

       	[[   7,   70,  700], [   8,   80,  800], [   9,   90,  900]],
	
	[[  -1,  -10, -100], [  -2,  -20, -200], [  -3,  -30, -300]]
	
  ]> : tensor<4x3x3xsi32>

  %input_i32 = builtin.unrealized_conversion_cast %input_si32 : tensor<4x3x3xsi32> to tensor<4x3x3xi32>
  %unranked_input_i32 = tensor.cast %input_i32 : tensor<4x3x3xi32> to tensor<?x?x?xi32>
  func.call @print_img_i32(%unranked_input_i32) : (tensor<?x?x?xi32>) -> ()

  %rotated0_si32 = vosa.rotate_90 %input_si32 { rotate = 1 : si32 } : tensor<4x3x3xsi32> -> tensor<3x4x3xsi32>
  %rotated0_i32 = builtin.unrealized_conversion_cast %rotated0_si32 : tensor<3x4x3xsi32> to tensor<3x4x3xi32>
  %unranked_rotated0_i32 = tensor.cast %rotated0_i32 : tensor<3x4x3xi32> to tensor<?x?x?xi32>
  //CHECK: [-1, 7, 4, 1]
  //CHECK: [-2, 8, 5, 2]
  //CHECK: [-3, 9, 6, 3]
  func.call @print_img_i32(%unranked_rotated0_i32) : (tensor<?x?x?xi32>) -> ()

  %rotated1_si32 = vosa.rotate_90 %input_si32 { rotate = 2 : si32 } : tensor<4x3x3xsi32> -> tensor<4x3x3xsi32>
  %rotated1_i32 = builtin.unrealized_conversion_cast %rotated1_si32 : tensor<4x3x3xsi32> to tensor<4x3x3xi32>
  %unranked_rotated1_i32 = tensor.cast %rotated1_i32 : tensor<4x3x3xi32> to tensor<?x?x?xi32>
  //CHECK: [-3, -2, -1]
  //CHECK: [9, 8, 7]
  //CHECK: [6, 5, 4]
  //CHECK: [3, 2, 1]
  func.call @print_img_i32(%unranked_rotated1_i32) : (tensor<?x?x?xi32>) -> ()

  %rotated2_si32 = vosa.rotate_90 %input_si32 { rotate = 3 : si32 } : tensor<4x3x3xsi32> -> tensor<3x4x3xsi32>
  %rotated2_i32 = builtin.unrealized_conversion_cast %rotated2_si32 : tensor<3x4x3xsi32> to tensor<3x4x3xi32>
  %unranked_rotated2_i32 = tensor.cast %rotated2_i32 : tensor<3x4x3xi32> to tensor<?x?x?xi32>
  //CHECK: [3, 6, 9, -3]
  //CHECK: [2, 5, 8, -2]
  //CHECK: [1, 4, 7, -1]
  func.call @print_img_i32(%unranked_rotated2_i32) : (tensor<?x?x?xi32>) -> ()

  %rotated3_si32 = vosa.rotate_90 %input_si32 { rotate = 4 : si32 } : tensor<4x3x3xsi32> -> tensor<4x3x3xsi32>
  %rotated3_i32 = builtin.unrealized_conversion_cast %rotated3_si32 : tensor<4x3x3xsi32> to tensor<4x3x3xi32>
  %unranked_rotated3_i32 = tensor.cast %rotated3_i32 : tensor<4x3x3xi32> to tensor<?x?x?xi32>
  //CHECK: [1, 2, 3]
  //CHECK: [4, 5, 6]
  //CHECK: [7, 8, 9]
  //CHECK: [-1, -2, -3]
  func.call @print_img_i32(%unranked_rotated3_i32) : (tensor<?x?x?xi32>) -> ()

  %rotated4_si32 = vosa.rotate_90 %input_si32 { rotate = 0 : si32 } : tensor<4x3x3xsi32> -> tensor<4x3x3xsi32>
  %rotated4_i32 = builtin.unrealized_conversion_cast %rotated4_si32 : tensor<4x3x3xsi32> to tensor<4x3x3xi32>
  %unranked_rotated4_i32 = tensor.cast %rotated4_i32 : tensor<4x3x3xi32> to tensor<?x?x?xi32>
  //CHECK: [1, 2, 3]
  //CHECK: [4, 5, 6]
  //CHECK: [7, 8, 9]
  //CHECK: [-1, -2, -3]
  func.call @print_img_i32(%unranked_rotated4_i32) : (tensor<?x?x?xi32>) -> ()

  %rotated5_si32 = vosa.rotate_90 %input_si32 { rotate = -1 : si32 } : tensor<4x3x3xsi32> -> tensor<3x4x3xsi32>
  %rotated5_i32 = builtin.unrealized_conversion_cast %rotated5_si32 : tensor<3x4x3xsi32> to tensor<3x4x3xi32>
  %unranked_rotated5_i32 = tensor.cast %rotated5_i32 : tensor<3x4x3xi32> to tensor<?x?x?xi32>
  //CHECK: [3, 6, 9, -3]
  //CHECK: [2, 5, 8, -2]
  //CHECK: [1, 4, 7, -1]
  func.call @print_img_i32(%unranked_rotated5_i32) : (tensor<?x?x?xi32>) -> ()
  
  %rotated6_si32 = vosa.rotate_90 %input_si32 { rotate = -2 : si32 } : tensor<4x3x3xsi32> -> tensor<4x3x3xsi32>
  %rotated6_i32 = builtin.unrealized_conversion_cast %rotated6_si32 : tensor<4x3x3xsi32> to tensor<4x3x3xi32>
  %unranked_rotated6_i32 = tensor.cast %rotated6_i32 : tensor<4x3x3xi32> to tensor<?x?x?xi32>
  //CHECK: [-3, -2, -1]
  //CHECK: [9, 8, 7]
  //CHECK: [6, 5, 4]
  //CHECK: [3, 2, 1]
  func.call @print_img_i32(%unranked_rotated6_i32) : (tensor<?x?x?xi32>) -> ()

  %rotated7_si32 = vosa.rotate_90 %input_si32 { rotate = -3 : si32 } : tensor<4x3x3xsi32> -> tensor<3x4x3xsi32>
  %rotated7_i32 = builtin.unrealized_conversion_cast %rotated7_si32 : tensor<3x4x3xsi32> to tensor<3x4x3xi32>
  %unranked_rotated7_i32 = tensor.cast %rotated7_i32 : tensor<3x4x3xi32> to tensor<?x?x?xi32>
  //CHECK: [-1, 7, 4, 1]
  //CHECK: [-2, 8, 5, 2]
  //CHECK: [-3, 9, 6, 3]
  func.call @print_img_i32(%unranked_rotated7_i32) : (tensor<?x?x?xi32>) -> ()

  %rotated8_si32 = vosa.rotate_90 %input_si32 { rotate = -4 : si32 } : tensor<4x3x3xsi32> -> tensor<4x3x3xsi32>
  %rotated8_i32 = builtin.unrealized_conversion_cast %rotated8_si32 : tensor<4x3x3xsi32> to tensor<4x3x3xi32>
  %unranked_rotated8_i32 = tensor.cast %rotated8_i32 : tensor<4x3x3xi32> to tensor<?x?x?xi32>
  //CHECK: [1, 2, 3]
  //CHECK: [4, 5, 6]
  //CHECK: [7, 8, 9]
  //CHECK: [-1, -2, -3]
  func.call @print_img_i32(%unranked_rotated4_i32) : (tensor<?x?x?xi32>) -> ()

  return
}

func.func @print_img_i32(%a : tensor<?x?x?xi32>) {
  // %unranked_all = tensor.cast %a : tensor<?x?x?xi32> to tensor<*xi32>
  // func.call @print_memref_i32(%unranked_all) : (tensor<*xi32>) -> ()
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c0 : tensor<?x?x?xi32>
  %w = tensor.dim %a, %c1 : tensor<?x?x?xi32>
  %nch = tensor.dim %a, %c2 : tensor<?x?x?xi32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[0, 0, %c][%h, %w, 1][1, 1, 1]  : tensor<?x?x?xi32> to tensor<?x?x1xi32>
     %chdata_c = tensor.collapse_shape %chdata [[0], [1, 2]] : tensor<?x?x1xi32> into tensor<?x?xi32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xi32> to tensor<*xi32>
     func.call @printMemrefI32(%unranked_chdata) : (tensor<*xi32>) -> ()
  }
  return
}

func.func private @printMemrefI32(%ptr : tensor<*xi32> {bufferization.access = "read"})
