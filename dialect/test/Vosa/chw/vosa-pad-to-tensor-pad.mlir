// RUN: vosa-opt -vosa-data-format=chw -vosa-pad-to-tensor-pad -split-input-file -verify-diagnostics %s | FileCheck %s

// CHECK-LABEL: func.func @pad_constant()
func.func @pad_constant() -> tensor<3x103x212xsi8> {
    %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
    // CHECK: %[[C:.*]] = arith.constant 17 : i8
    // CHECK: %{{.*}} = tensor.pad %{{.*}} low[0, 1, 4] high[0, 2, 8] {
    // CHECK: ^bb0(%arg0: index, %arg1: index, %arg2: index):
    // CHECK:   tensor.yield %[[C]] : i8
    // CHECK: } : tensor<3x100x200xi8> to tensor<3x103x212xi8>
    %res0 = vosa.pad %t_si8 constant { pad_constant = 17 : si8, pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x103x212xsi8>

    return %res0 : tensor<3x103x212xsi8>
}

// -----

// CHECK-LABEL: func.func @pad_replicate()
func.func @pad_replicate() -> tensor<3x103x212xsi8> {
    %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>

    // CHECK: %padded = tensor.pad %1 low[0, 1, 4] high[0, 2, 8] {
    // CHECK: ^bb0(%arg0: index, %arg1: index, %arg2: index):
    // CHECK-DAG:   %c0 = arith.constant 0 : index
    // CHECK-DAG:   %c1 = arith.constant 1 : index
    // CHECK-DAG:   %true = arith.constant true
    // CHECK-DAG:   %false = arith.constant false
    // CHECK-DAG:   %c1_0 = arith.constant 1 : index
    // CHECK-DAG:   %c2 = arith.constant 2 : index
    // CHECK-DAG:   %c99 = arith.constant 99 : index
    // CHECK-DAG:   %c101 = arith.constant 101 : index
    // CHECK-DAG:   %c4 = arith.constant 4 : index
    // CHECK-DAG:   %c8 = arith.constant 8 : index
    // CHECK-DAG:   %c199 = arith.constant 199 : index
    // CHECK-DAG:   %c204 = arith.constant 204 : index
    // CHECK:   %[[IS_Y_LOWER:.*]] = arith.cmpi slt, %arg1, %c1_0 : index
    // CHECK:   %[[IS_Y_UPPER:.*]] = arith.cmpi sge, %arg1, %c101 : index
    // CHECK:   %[[IS_X_LOWER:.*]] = arith.cmpi slt, %arg2, %c4 : index
    // CHECK:   %[[IS_X_UPPER:.*]] = arith.cmpi sge, %arg2, %c204 : index
    // CHECK:   %[[Y_PAD_INDEX:.*]]:2 = scf.if %[[IS_Y_LOWER]] -> (i1, index) {
    // CHECK:     scf.yield %true, %c0 : i1, index
    // CHECK:   } else {
    // CHECK:     %[[Y_PAD_INDEX_A:.*]]:2 = scf.if %[[IS_Y_UPPER]] -> (i1, index) {
    // CHECK:       scf.yield %true, %c99 : i1, index
    // CHECK:     } else {
    // CHECK:       %[[Y_INDEX:.*]] = arith.subi %arg1, %c1_0 : index
    // CHECK:       scf.yield %false, %[[Y_INDEX]] : i1, index
    // CHECK:     }
    // CHECK:     scf.yield %[[Y_PAD_INDEX_A]]#0, %[[Y_PAD_INDEX_A]]#1 : i1, index
    // CHECK:   }
    // CHECK:   %[[X_PAD_INDEX:.*]]:2 = scf.if %[[IS_X_LOWER]] -> (i1, index) {
    // CHECK:     scf.yield %true, %c0 : i1, index
    // CHECK:   } else {
    // CHECK:     %[[X_PAD_INDEX_A:.*]]:2 = scf.if %[[IS_X_UPPER]] -> (i1, index) {
    // CHECK:       scf.yield %true, %c199 : i1, index
    // CHECK:     } else {
    // CHECK:       %[[X_INDEX:.*]] = arith.subi %arg2, %c4 : index
    // CHECK:       scf.yield %false, %[[X_INDEX]] : i1, index
    // CHECK:     }
    // CHECK:     scf.yield %[[X_PAD_INDEX_A]]#0, %[[X_PAD_INDEX_A]]#1 : i1, index
    // CHECK:   }
    // CHECK:   %[[IS_PAD:.*]] = arith.ori %[[Y_PAD_INDEX]]#0, %[[X_PAD_INDEX]]#0 : i1
    // CHECK:   %[[V:.*]] = scf.if %[[IS_PAD]] -> (i8) {
    // CHECK:     %extracted = tensor.extract %1[%arg0, %[[Y_PAD_INDEX]]#1, %[[X_PAD_INDEX]]#1] : tensor<3x100x200xi8>
    // CHECK:     scf.yield %extracted : i8
    // CHECK:   } else {
    // CHECK:     scf.yield %c0_i8 : i8
    // CHECK:   }
    // CHECK:   tensor.yield %[[V]] : i8
    // CHECK: } : tensor<3x100x200xi8> to tensor<3x103x212xi8>
    
    %res0 = vosa.pad %t_si8 replicate { pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x103x212xsi8>

    return %res0 : tensor<3x103x212xsi8>
}

// -----

// CHECK-LABEL: func.func @pad_reflect()
func.func @pad_reflect() -> tensor<3x103x212xsi8> {
    %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>

    // CHECK: %padded = tensor.pad %1 low[0, 1, 4] high[0, 2, 8] {
    // CHECK: ^bb0(%arg0: index, %arg1: index, %arg2: index):
    // CHECK-DAG:   %c0 = arith.constant 0 : index
    // CHECK-DAG:   %c1 = arith.constant 1 : index
    // CHECK-DAG:   %true = arith.constant true
    // CHECK-DAG:   %false = arith.constant false
    // CHECK-DAG:   %c1_0 = arith.constant 1 : index
    // CHECK-DAG:   %c2 = arith.constant 2 : index
    // CHECK-DAG:   %c99 = arith.constant 99 : index
    // CHECK-DAG:   %c101 = arith.constant 101 : index
    // CHECK-DAG:   %c4 = arith.constant 4 : index
    // CHECK-DAG:   %c8 = arith.constant 8 : index
    // CHECK-DAG:   %c199 = arith.constant 199 : index
    // CHECK-DAG:   %c204 = arith.constant 204 : index
    // CHECK:   %[[IS_Y_LOWER:.*]] = arith.cmpi slt, %arg1, %c1_0 : index
    // CHECK:   %[[IS_Y_UPPER:.*]] = arith.cmpi sge, %arg1, %c101 : index
    // CHECK:   %[[IS_X_LOWER:.*]] = arith.cmpi slt, %arg2, %c4 : index
    // CHECK:   %[[IS_X_UPPER:.*]] = arith.cmpi sge, %arg2, %c204 : index
    // CHECK:   %[[Y_PAD_INDEX:.*]]:2 = scf.if %[[IS_Y_LOWER]] -> (i1, index) {
    // CHECK:     %[[Y_INDEX:.*]] = arith.subi %c1_0, %arg1 : index
    // CHECK:     scf.yield %true, %[[Y_INDEX]] : i1, index
    // CHECK:   } else {
    // CHECK:     %[[Y_PAD_INDEX_A:.*]]:2 = scf.if %[[IS_Y_UPPER]] -> (i1, index) {
    // CHECK:       %[[PAD_INDEX:.*]] = arith.subi %arg1, %c101 : index
    // CHECK:       %[[IMG_UPPER:.*]] = arith.subi %c99, %c1 : index
    // CHECK:       %[[IMG_INDEX:.*]] = arith.subi %[[IMG_UPPER]], %[[PAD_INDEX]] : index
    // CHECK:       scf.yield %true, %[[IMG_INDEX]] : i1, index
    // CHECK:     } else {
    // CHECK:       %[[IMG_INDEX:.*]] = arith.subi %arg1, %c1_0 : index
    // CHECK:       scf.yield %false, %[[IMG_INDEX]] : i1, index
    // CHECK:     }
    // CHECK:     scf.yield %[[Y_PAD_INDEX_A]]#0, %[[Y_PAD_INDEX_A]]#1 : i1, index
    // CHECK:   }
    // CHECK:   %[[X_PAD_INDEX:.*]]:2 = scf.if %[[IS_X_LOWER]] -> (i1, index) {
    // CHECK:     %[[X_INDEX:.*]] = arith.subi %c4, %arg2 : index
    // CHECK:     scf.yield %true, %[[X_INDEX]] : i1, index
    // CHECK:   } else {
    // CHECK:     %[[X_PAD_INDEX_A:.*]]:2 = scf.if %[[IS_X_UPPER]] -> (i1, index) {
    // CHECK:       %[[PAD_INDEX:.*]] = arith.subi %arg2, %c204 : index
    // CHECK:       %[[IMG_UPPER:.*]] = arith.subi %c199, %c1 : index
    // CHECK:       %[[IMG_INDEX:.*]] = arith.subi %[[IMG_UPPER]], %[[PAD_INDEX]] : index
    // CHECK:       scf.yield %true, %[[IMG_INDEX]] : i1, index
    // CHECK:     } else {
    // CHECK:       %[[IMG_INDEX:.*]] = arith.subi %arg2, %c4 : index
    // CHECK:       scf.yield %false, %[[IMG_INDEX]] : i1, index
    // CHECK:     }
    // CHECK:     scf.yield %[[X_PAD_INDEX_A]]#0, %[[X_PAD_INDEX_A]]#1 : i1, index
    // CHECK:   }
    // CHECK:   %[[IS_PAD:.*]] = arith.ori %[[Y_PAD_INDEX]]#0, %[[X_PAD_INDEX]]#0 : i1
    // CHECK:   %[[V:.*]] = scf.if %[[IS_PAD]] -> (i8) {
    // CHECK:     %extracted = tensor.extract %1[%arg0, %[[Y_PAD_INDEX]]#1, %[[X_PAD_INDEX]]#1] : tensor<3x100x200xi8>
    // CHECK:     scf.yield %extracted : i8
    // CHECK:   } else {
    // CHECK:     scf.yield %c0_i8 : i8
    // CHECK:   }
    // CHECK:   tensor.yield %[[V]] : i8
    // CHECK: } : tensor<3x100x200xi8> to tensor<3x103x212xi8>
    
    %res0 = vosa.pad %t_si8 reflect { pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x103x212xsi8>

    return %res0 : tensor<3x103x212xsi8>
}
