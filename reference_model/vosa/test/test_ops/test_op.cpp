/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "test_bench.h"
#include "vosa/vosa.h"

template <typename T>
bool run_value_test() {
  T input = 10;
  auto packed_value = vosa::Op::pack_value(input);
  auto unpacked_value = vosa::Op::unpack_value<T>(packed_value);
  return input == unpacked_value;
}

template <typename T>
bool run_vector_test() {
  auto vector_input = vosa::EigenVector<T>(5);
  vector_input.setValues({0, 1, 2, 3, 4});

  auto packed_value = vosa::Op::pack_vector(vector_input);
  auto unpacked_value = vosa::Op::unpack_vector<T>(packed_value);

  bool success = vosa::test::TestBench<T, int32_t>::compareEigenVectors(
      vector_input, unpacked_value);
  return success;
}

template <typename T>
bool run_plane_test() {
  auto plane_input = vosa::EigenPlane<T>(3, 3);
  plane_input.setValues({{0, 1, 2}, {3, 4, 5}, {6, 7, 8}});

  auto packed_value = vosa::Op::pack_plane(plane_input);
  auto unpacked_value = vosa::Op::unpack_plane<T>(packed_value, {3, 3});

  bool success = vosa::test::TestBench<T, int32_t>::compareEigenPlanes(
      plane_input, unpacked_value);
  return success;
}

template <typename T>
bool run_image_test() {
  auto image_input = vosa::EigenImage<T>(2, 3, 3);
  image_input.setValues({{{0, 1, 2}, {3, 4, 5}, {6, 7, 8}},
                         {{9, 10, 11}, {12, 13, 14}, {15, 16, 17}}});

  auto packed_value = vosa::Op::pack_image(image_input);
  auto unpacked_value = vosa::Op::unpack_image<T>(packed_value, {2, 3, 3});

  bool success = vosa::test::TestBench<T, int32_t>::compareEigenImages(
      image_input, unpacked_value);
  return success;
}

TEST(test_op, test_value_pack_unpack) {

  bool success_u8 = run_value_test<uint8_t>();
  EXPECT_TRUE(success_u8);

  bool success_u16 = run_value_test<uint16_t>();
  EXPECT_TRUE(success_u16);

  bool success_u32 = run_value_test<uint32_t>();
  EXPECT_TRUE(success_u32);

  bool success_i8 = run_value_test<int8_t>();
  EXPECT_TRUE(success_i8);

  bool success_i16 = run_value_test<int16_t>();
  EXPECT_TRUE(success_i16);

  bool success_i32 = run_value_test<int32_t>();
  EXPECT_TRUE(success_i32);

  bool success_f = run_value_test<float>();
  EXPECT_TRUE(success_f);
}

TEST(test_op, test_vector_pack_unpack) {

  bool success_u8 = run_vector_test<uint8_t>();
  EXPECT_TRUE(success_u8);

  bool success_u16 = run_vector_test<uint16_t>();
  EXPECT_TRUE(success_u16);

  bool success_u32 = run_vector_test<uint32_t>();
  EXPECT_TRUE(success_u32);

  bool success_i8 = run_vector_test<int8_t>();
  EXPECT_TRUE(success_i8);

  bool success_i16 = run_vector_test<int16_t>();
  EXPECT_TRUE(success_i16);

  bool success_i32 = run_vector_test<int32_t>();
  EXPECT_TRUE(success_i32);

  bool success_f = run_vector_test<float>();
  EXPECT_TRUE(success_f);
}

TEST(test_op, test_plane_pack_unpack) {

  bool success_u8 = run_plane_test<uint8_t>();
  EXPECT_TRUE(success_u8);

  bool success_u16 = run_plane_test<uint16_t>();
  EXPECT_TRUE(success_u16);

  bool success_u32 = run_plane_test<uint32_t>();
  EXPECT_TRUE(success_u32);

  bool success_i8 = run_plane_test<int8_t>();
  EXPECT_TRUE(success_i8);

  bool success_i16 = run_plane_test<int16_t>();
  EXPECT_TRUE(success_i16);

  bool success_i32 = run_plane_test<int32_t>();
  EXPECT_TRUE(success_i32);

  bool success_f = run_plane_test<float>();
  EXPECT_TRUE(success_f);
}

TEST(test_op, test_image_pack_unpack) {

  bool success_u8 = run_image_test<uint8_t>();
  EXPECT_TRUE(success_u8);

  bool success_u16 = run_image_test<uint16_t>();
  EXPECT_TRUE(success_u16);

  bool success_u32 = run_image_test<uint32_t>();
  EXPECT_TRUE(success_u32);

  bool success_i8 = run_image_test<int8_t>();
  EXPECT_TRUE(success_i8);

  bool success_i16 = run_image_test<int16_t>();
  EXPECT_TRUE(success_i16);

  bool success_i32 = run_image_test<int32_t>();
  EXPECT_TRUE(success_i32);

  bool success_f = run_image_test<float>();
  EXPECT_TRUE(success_f);
}