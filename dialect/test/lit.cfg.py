# -*- Python -*-

import os
import platform
import re
import subprocess
import tempfile

import lit.formats
import lit.util

from lit.llvm import llvm_config
from lit.llvm.subst import ToolSubst
from lit.llvm.subst import FindTool

# Configuration file for the 'lit' test runner.

# name: The name of this test suite.
config.name = "VOSA"

config.test_format = lit.formats.ShTest(not llvm_config.use_lit_shell)

# suffixes: A list of file extensions to treat as test files.
config.suffixes = [".mlir", ".sh"]

config.substitutions.append(("%PATH%", config.environment["PATH"]))
config.substitutions.append(("%shlibext", config.llvm_shlib_ext))
config.substitutions.append(("%llvm_lib_dir", config.llvm_lib_dir))

llvm_config.with_system_environment(["HOME", "INCLUDE", "LIB", "TMP", "TEMP"])

llvm_config.use_default_substitutions()

# excludes: A list of directories to exclude from the testsuite. The 'Inputs'
# subdirectories contain auxiliary inputs for various tests in their parent
# directories.
config.excludes = ["Inputs", "Examples", "CMakeLists.txt", "README.txt", "LICENSE.txt"]

# test_source_root: The root path where tests are located.
config.test_source_root = os.path.dirname(__file__)

# test_exec_root: The root path where tests should be run.
config.test_exec_root = os.path.join(config.vosa_obj_root, "test")
config.vosa_tools_dir = os.path.join(config.vosa_obj_root, "bin")

# Tweak the PATH to include the LLVM/MLIR tools dir.
llvm_config.with_environment("PATH", config.llvm_tools_dir, append_path=True)

# Tweak the PATH to include the VOSA tools dir.
llvm_config.with_environment("PATH", config.vosa_tools_dir, append_path=True)

llvm_config.with_environment("LLVM_LIB_DIR", config.llvm_lib_dir)

tool_dirs = [config.vosa_tools_dir]
tools = [
    "vosa-opt",
]
llvm_config.add_tool_substitutions(tools, tool_dirs)

config.substitutions.append(("%test_src_root", config.test_source_root))
config.substitutions.append(("%vosa_obj_root", config.vosa_obj_root))
