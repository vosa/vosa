// run command without FileCheck to see output (or add "--dump-input always" FileCheck option)
// RUN: vosa-opt -vosa-to-tosa %s | %test_src_root/e2e-runner | FileCheck %s

func.func @main() {
    %input_f32 = vosa.constant dense<
      [[[1., 2., 3.],
        [1., 2., 3.],
        [1., 2., 3.]],

       [[1., 2., 3.],
        [1., 2., 3.],
        [1., 2., 3.]],

       [[1., 2., 3.],
        [1., 2., 3.],
        [1., 2., 3.]]]> : tensor<3x3x3xf32>
    %unranked_input_f32 = tensor.cast %input_f32 : tensor<3x3x3xf32> to tensor<*xf32>
    // func.call @printMemrefF32(%unranked_input_f32) : (tensor<*xf32>) -> ()

    //CHECK: sizes = [3, 3]
    //CHECK-COUNT-3: [0.5, 0.5, 0.5]
    //CHECK-COUNT-3: [0.95, 0.95, 0.95]
    //CHECK-COUNT-3: [1.4, 1.4, 1.4]
    %converted_f32 = vosa.pointwise_matrix_multiply %input_f32 {M=dense<[[0.5, 0.0, 0.0], [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]]>: tensor<3x3xf32>, K_1=dense<[0.2, 0.3, 0.4]> : tensor<3xf32>, K_2=dense<[0.1, 0.1, 0.1]> : tensor<3xf32>} : tensor<3x3x3xf32> -> tensor<3x3x3xf32>
    %dyn_converted_f32 = tensor.cast %converted_f32 : tensor<3x3x3xf32> to tensor<?x?x?xf32>
    func.call @print_img_f32(%dyn_converted_f32) : (tensor<?x?x?xf32>) -> ()

    %input_si32 = vosa.constant dense<[[[1, 1, 1],[1, 1, 1],[1, 1, 1]], [[2, 2, 2],[2, 2, 2],[2, 2, 2]], [[3, 3, 3],[3, 3, 3],[3, 3, 3]]]> : tensor<3x3x3xsi32>
    %converted_si = vosa.pointwise_matrix_multiply %input_si32 {K_1 = dense<[0, 0, 512]> : tensor<3xsi32>, K_2 = dense<0> : tensor<3xsi32>, M = dense<[[262144, 0, 386554], [262144, -43131, -149772], [262144, 493195, 0]]> : tensor<3x3xsi32>} : tensor<3x3x3xsi32> -> tensor<3x3x3xsi32>

    return
}

func.func @print_img_f32(%a : tensor<?x?x?xf32>) {
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c0 : tensor<?x?x?xf32>
  %w = tensor.dim %a, %c1 : tensor<?x?x?xf32>
  %nch = tensor.dim %a, %c2 : tensor<?x?x?xf32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[0, 0, %c][%h, %w, 1][1, 1, 1]  : tensor<?x?x?xf32> to tensor<?x?x1xf32>
     %chdata_c = tensor.collapse_shape %chdata [[0], [1, 2]] : tensor<?x?x1xf32> into tensor<?x?xf32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xf32> to tensor<*xf32>
     func.call @printMemrefF32(%unranked_chdata) : (tensor<*xf32>) -> ()
  }
  return
}

func.func private @printMemrefI32(%ptr : tensor<*xf32> {bufferization.access = "read"})
func.func private @printMemrefF32(%ptr : tensor<*xf32> {bufferization.access = "read"})
