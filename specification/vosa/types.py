"""
License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   """

import typing
import enum
import aenum


def aenum_to_table(an_aenum: aenum.Enum):
    lines = []
    lines.append('[caption=""]')
    lines.append(".%s: %s" % (an_aenum.__name__, an_aenum.__doc__.strip("\n")))
    lines.append("|===")
    for val in an_aenum:
        if val.name == "UNDEFINED" or val.name == "ANY":
            continue
        lines.append("| %s a| %s " % (val.name, val.__doc__))
    lines.append("|===")
    return lines


class DType(aenum.Enum):
    """
    An itemization of basic data types,
    """

    _init_ = "value __doc__"
    BOOL = enum.auto(), "Boolean value."
    UINT8 = enum.auto(), "Unsigned 8 bit integer value."
    UINT16 = enum.auto(), "Unsigned 16 bit integer value."
    UINT32 = enum.auto(), "Unsigned 32 bit integer value."
    INT8 = enum.auto(), "Signed twos-complement 8 bit integer value."
    INT16 = enum.auto(), "Signed twos-complement 16 bit integer value."
    INT32 = enum.auto(), "Signed twos-complement 32 bit integer value."
    FLOAT16 = (
        enum.auto(),
        "Floating point 16 bit value (IEEE 754 half-precision binary floating-point format).",
    )
    FLOAT32 = (
        enum.auto(),
        "Floating point 32 bit value (IEEE 754 single-precision binary floating-point format).",
    )
    UNDEFINED = enum.auto(), ""
    ANY = enum.auto()

    @classmethod
    def to_flatbuffer(cls, dtype):
        from vosa_serialization import DTYPE

        dtypes = {
            cls.BOOL: DTYPE.DTYPE.BOOL,
            cls.UINT8: DTYPE.DTYPE.UINT8,
            cls.UINT16: DTYPE.DTYPE.UINT16,
            cls.UINT32: DTYPE.DTYPE.UINT32,
            cls.INT8: DTYPE.DTYPE.INT8,
            cls.INT16: DTYPE.DTYPE.INT16,
            cls.INT32: DTYPE.DTYPE.INT32,
            cls.FLOAT16: DTYPE.DTYPE.FLOAT16,
            cls.FLOAT32: DTYPE.DTYPE.FLOAT32,
        }

        return dtypes[dtype]


class ImageDtype(aenum.Enum):
    """
    An itemization of specific bit packed image formats.
    """

    _init_ = "value __doc__"
    NV12 = enum.auto(), "a"
    NV21 = enum.auto(), "a"
    UYVY = enum.auto(), "a"
    YUYV = enum.auto(), "a"
    IYUV = enum.auto(), "a"
    YUV4 = enum.auto(), "a"
    RGB = enum.auto(), "a"
    RGBX = enum.auto(), "a"
    GRY = enum.auto(), "a"


class BorderType(aenum.Enum):
    """
    An enumeration of possible border padding modes in operations where a kernel of some type may extend beyond the defined image boundaries
    """

    _init_ = "value __doc__"
    CONSTANT = (enum.auto(),)
    REPLICATE = (enum.auto(),)
    UNDEFINED = (enum.auto(),)


ARRAY_ATYPE_DOC = """
A general purpose array of arbitrary shape and size.

When used directly (rather than the particular types IMAGE or PLANE), the ARRAY type may be used to encapsulate general purpose data, which is necessary to the operation of Imaging and CV Pipelines but has no direct visual meaning, for example,

* Coordinate Pairs,
* Matrices of parameters,
* Lookup tables.
"""


IMAGE_ATYPE_DOC = """
A generalized image. An ARRAY with dimension 3, having a logical (not necessarily physical) data ordering [Channels, Height, Width]."

The IMAGE type may be used to encapsulate,

* Pixel intensities in, for example, the formats,
** RGB,
** YUV.
* The results of computations on pixel intensities, for example,
** Responses to linear filters (e.g. Sobel), 
** Responses to complex or non linear filtering methods (e.g. edge detectors)
** Motion vectors.
* Groupings of channels from different but related sources, for example,
** Stereo pairs.

One should expect that rendering any channel of an Image should result in a visually meaningful output.
"""

PLANE_ATYPE_DOC = """
A single image plane. An ARRAY with dimension 2, having a logical (not necessarily physical) data ordering [Height, Width].

The PLANE type is used as a special case of an IMAGE. It is used to encapsulate the same types of data but has no channel dimension.

Its use is most common in operations where the size of the channel dimension of an input, attribute or output _must_ be one.
"""


VECTOR_TYPE_DOC = """
A vector of values. An ARRAY with dimension 1.

Used for the same cases as the ARRAY, as a shorthand to make the one-dimensional nature explicit. 
"""

SCALAR_TYPE_DOC = """
A single value. An ARRAY with dimension 0.

Used for the same cases as the ARRAY, as a shorthand to make the zero-dimensional nature explicit.
"""


class AType(aenum.Enum):
    """
    An itemization of fundamental raw data struct types. Categorized by constraints on their shape and/or size.
    """

    _init_ = "value __doc__"
    ARRAY = enum.auto(), ARRAY_ATYPE_DOC
    IMAGE = enum.auto(), IMAGE_ATYPE_DOC
    PLANE = enum.auto(), PLANE_ATYPE_DOC
    VECTOR = enum.auto(), VECTOR_TYPE_DOC
    SCALAR = enum.auto(), SCALAR_TYPE_DOC
    UNDEFINED = enum.auto()
    ANY = enum.auto()


class SpecialType(aenum.Enum):
    """
    An itemization of special data types.
    """

    _init_ = "value __doc__"
    HISTOGRAM_DISTRIBUTION = (
        enum.auto(),
        "A Vector of shape [NUM_BINS]. Associated metadata include OFFSET, the start point of the distribution and RANGE, the total length of the distribution."
        "Hence a HISTOGRAM_DISTRIBUTION with parameters NUM_BINS=5, OFFSET=2, RANGE=10 consists of 5 bins covering the intervals [2,4), [4,6), [6,8), [8, 10), [10,12).",
    )
    DATA = enum.auto(), "A raw block of memory"


class AttributeEnum(aenum.Enum):
    pass


class DataType:
    def __init__(
        self,
        dtype: typing.Union[DType, ImageDtype, BorderType, None],
        ttype: typing.Union[AType, SpecialType, typing.Type[AttributeEnum], None],
        description: typing.Optional[str] = None,
        shape: typing.Optional[typing.List[typing.Union[int, str]]] = None,
        value=None,
    ):
        self._dtype = dtype
        self._ttype = ttype
        self._description = description
        self._shape = shape
        self._value = value

    def dtype(self):
        return self._dtype

    def ttype(self):
        return self._ttype

    def adoc(self) -> str:
        try:
            dtype_name = self._dtype.name
            return " %s[%s]" % (self._ttype.name, dtype_name)
        except AttributeError:
            return self._ttype.name

    def description(self):
        return self._description

    def shape(self):
        if self._shape is None:
            return ""
        shape_str = str(self._shape).replace("'", "")
        return shape_str

    def value(self):
        return self._value

    def __eq__(self, other):
        return (self._ttype == other.ttype()) and (self._dtype == other.dtype())
