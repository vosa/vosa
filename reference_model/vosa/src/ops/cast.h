/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_CODE_CAST_H
#define VOSA_CODE_CAST_H

#include <exception>

#include "vosa/op.h"

namespace vosa {

class CastBase : public Op {
public:
  enum rounding_mode_t { VOSA_ROUNDING_MODE_SATURATE, VOSA_ROUNDING_MODE_WRAP };
  static std::shared_ptr<Op>
  parse_serialized(const vosa_serialization::VosaOperatorUnion &);
};

template <typename inputT, typename outputT>
class Cast : public CastBase {
public:
  explicit Cast(
      rounding_mode_t rounding_mode = vosa::CastBase::VOSA_ROUNDING_MODE_WRAP)
      : rounding_mode(rounding_mode){};

  EigenImage<outputT> runme(const EigenImage<inputT> &input) const;

  [[nodiscard]] ArrayBase::DType output_dtype() const final;

  [[nodiscard]] std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &) const final;

  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

private:
  rounding_mode_t rounding_mode;
};

class CastException : public std::exception {
public:
  virtual char const *what() const noexcept { return "Exception in cast"; }
};

} // namespace vosa

#endif // VOSA_CODE_CAST_H
