/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "const_image.h"

template <typename T>
T vosa::ConstImageBase::unpackValue(const std::vector<uint8_t> &raw_value) {
  auto uint8_ptr = new uint8_t[sizeof(T)];
  std::copy(raw_value.begin(), raw_value.end(), uint8_ptr);
  T output = *((T *)uint8_ptr);
  delete[] uint8_ptr;
  return output;
}

std::shared_ptr<vosa::Op> vosa::ConstImageBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_const_image = vosa_operator_union.AsCONST_IMAGE();
  assert(serialized_const_image);
  auto mode = serialized_const_image->mode;
  auto serialized_attributes = &serialized_const_image->attr;
  std::vector<uint8_t> raw_value = serialized_attributes->get()->value;
  std::vector<uint32_t> dimensions = serialized_attributes->get()->dimension;
  switch (mode) {
  case vosa_serialization::CONST_IMAGE_MODE_I_FLOAT32: {
    return std::make_shared<vosa::ConstImage<float>>(
        dimensions, unpackValue<float>(raw_value));
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::ArrayBase::DType vosa::ConstImage<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::ConstImage<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &) const {
  vosa::EigenImage<T> image{dimensions[0], dimensions[1], dimensions[2]};
  image.setConstant(value);
  return std::make_shared<vosa::ArrayBase>(image);
}

template <>
vosa_serialization::CONST_IMAGE_MODE
vosa::ConstImage<float>::get_serialization_mode() const {
  return vosa_serialization::CONST_IMAGE_MODE_I_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::ConstImage<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  auto const_image_attribute =
      std::make_unique<vosa_serialization::CONST_IMAGE_ATTRIBUTET>();
  std::vector<uint8_t> value_byte_vector;
  value_byte_vector.insert(value_byte_vector.end(), (uint8_t *)&value,
                           ((uint8_t *)&value) + sizeof(T));
  const_image_attribute->value = value_byte_vector;
  const_image_attribute->dimension = dimensions;

  vosa_serialization::CONST_IMAGET const_image;
  const_image.mode = get_serialization_mode();
  const_image.attr = std::move(const_image_attribute);

  auto const_scalar_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  const_scalar_operator->op.Set(std::move(const_image));
  const_scalar_operator->name = op_name;
  const_scalar_operator->inputs = input_names;
  return const_scalar_operator;
}

template class vosa::ConstImage<float>;