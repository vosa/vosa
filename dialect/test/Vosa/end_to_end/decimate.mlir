// run command without FileCheck to see output (or add "--dump-input always" FileCheck option)
// RUN: vosa-opt -vosa-to-tosa %s | %test_src_root/e2e-runner | FileCheck %s

func.func @main() {
    // generate using numpy:
    //   np.transpose(np.reshape(np.arange(0,300),(3, 10,10)), [1, 2, 0])
    %input = vosa.constant dense<[
       [[  0, 100, 200], [  1, 101, 201], [  2, 102, 202], [  3, 103, 203], [  4, 104, 204],
        [  5, 105, 205], [  6, 106, 206], [  7, 107, 207], [  8, 108, 208], [  9, 109, 209]],

       [[ 10, 110, 210], [ 11, 111, 211], [ 12, 112, 212], [ 13, 113, 213], [ 14, 114, 214],
        [ 15, 115, 215], [ 16, 116, 216], [ 17, 117, 217], [ 18, 118, 218], [ 19, 119, 219]],

       [[ 20, 120, 220], [ 21, 121, 221], [ 22, 122, 222], [ 23, 123, 223], [ 24, 124, 224],
        [ 25, 125, 225], [ 26, 126, 226], [ 27, 127, 227], [ 28, 128, 228], [ 29, 129, 229]],

       [[ 30, 130, 230], [ 31, 131, 231], [ 32, 132, 232], [ 33, 133, 233], [ 34, 134, 234],
        [ 35, 135, 235], [ 36, 136, 236], [ 37, 137, 237], [ 38, 138, 238], [ 39, 139, 239]],

       [[ 40, 140, 240], [ 41, 141, 241], [ 42, 142, 242], [ 43, 143, 243], [ 44, 144, 244],
        [ 45, 145, 245], [ 46, 146, 246], [ 47, 147, 247], [ 48, 148, 248], [ 49, 149, 249]],

       [[ 50, 150, 250], [ 51, 151, 251], [ 52, 152, 252], [ 53, 153, 253], [ 54, 154, 254],
        [ 55, 155, 255], [ 56, 156, 256], [ 57, 157, 257], [ 58, 158, 258], [ 59, 159, 259]],

       [[ 60, 160, 260], [ 61, 161, 261], [ 62, 162, 262], [ 63, 163, 263], [ 64, 164, 264],
        [ 65, 165, 265], [ 66, 166, 266], [ 67, 167, 267], [ 68, 168, 268], [ 69, 169, 269]],

       [[ 70, 170, 270], [ 71, 171, 271], [ 72, 172, 272], [ 73, 173, 273], [ 74, 174, 274],
        [ 75, 175, 275], [ 76, 176, 276], [ 77, 177, 277], [ 78, 178, 278], [ 79, 179, 279]],

       [[ 80, 180, 280], [ 81, 181, 281], [ 82, 182, 282], [ 83, 183, 283], [ 84, 184, 284],
        [ 85, 185, 285], [ 86, 186, 286], [ 87, 187, 287], [ 88, 188, 288], [ 89, 189, 289]],

       [[ 90, 190, 290], [ 91, 191, 291], [ 92, 192, 292], [ 93, 193, 293], [ 94, 194, 294],
        [ 95, 195, 295], [ 96, 196, 296], [ 97, 197, 297], [ 98, 198, 298], [ 99, 199, 299]]
        ]> : tensor<10x10x3xsi32>

    // CHECK: sizes = [2, 5]
    // CHECK: [0, 2, 4, 6, 8]
    // CHECK: [50, 52, 54, 56, 58]
    // CHECK: sizes = [2, 5]
    // CHECK: [100, 102, 104, 106, 108]
    // CHECK: [150, 152, 154, 156, 158]
    // CHECK: sizes = [2, 5]
    // CHECK: [200, 202, 204, 206, 208]
    // CHECK: [250, 252, 254, 256, 258]
    %res = vosa.decimate %input  { N = [5 : i32, 2 : i32], offsets = [0 : i32, 0 : i32] } : tensor<10x10x3xsi32> -> tensor<2x5x3xsi32>
    %i32_res = builtin.unrealized_conversion_cast %res : tensor<2x5x3xsi32> to tensor<2x5x3xi32>
    %dyn_res = tensor.cast %i32_res : tensor<2x5x3xi32> to tensor<?x?x?xi32>
    func.call @print_img_i32(%dyn_res) : (tensor<?x?x?xi32>) -> ()

    // CHECK: sizes = [2, 5]
    // CHECK: [21, 23, 25, 27, 29]
    // CHECK: [71, 73, 75, 77, 79]
    // CHECK: sizes = [2, 5]
    // CHECK: [121, 123, 125, 127, 129]
    // CHECK: [171, 173, 175, 177, 179]
    // CHECK: sizes = [2, 5]
    // CHECK: [221, 223, 225, 227, 229]
    // CHECK: [271, 273, 275, 277, 279]
    %res1 = vosa.decimate %input  { N = [5 : i32, 2 : i32], offsets = [2 : i32, 1 : i32] } : tensor<10x10x3xsi32> -> tensor<2x5x3xsi32>
    %i32_res1 = builtin.unrealized_conversion_cast %res1 : tensor<2x5x3xsi32> to tensor<2x5x3xi32>
    %dyn_res1 = tensor.cast %i32_res1 : tensor<2x5x3xi32> to tensor<?x?x?xi32>
    func.call @print_img_i32(%dyn_res1) : (tensor<?x?x?xi32>) -> ()

    return
}

func.func @print_img_i32(%a : tensor<?x?x?xi32>) {
  // %unranked_all = tensor.cast %a : tensor<?x?x?xi32> to tensor<*xi32>
  // func.call @print_memref_i32(%unranked_all) : (tensor<*xi32>) -> ()
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c0 : tensor<?x?x?xi32>
  %w = tensor.dim %a, %c1 : tensor<?x?x?xi32>
  %nch = tensor.dim %a, %c2 : tensor<?x?x?xi32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[0, 0, %c][%h, %w, 1][1, 1, 1]  : tensor<?x?x?xi32> to tensor<?x?x1xi32>
     %chdata_c = tensor.collapse_shape %chdata [[0], [1, 2]] : tensor<?x?x1xi32> into tensor<?x?xi32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xi32> to tensor<*xi32>
     func.call @printMemrefI32(%unranked_chdata) : (tensor<*xi32>) -> ()
  }
  return
}

func.func private @printMemrefI32(%ptr : tensor<*xi32> {bufferization.access = "read"})
