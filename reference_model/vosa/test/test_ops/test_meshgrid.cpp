/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/mesh_grid.h"
#include "test_bench.h"
#include "vosa/vosa.h"

TEST(test_meshgrid, test_meshgrid) {

  auto mesh_grid = vosa::MeshGrid<uint32_t>({3, 3});

  auto output = mesh_grid.eval({});
  auto typed_output = output->get_image<uint32_t>();

  auto expected_output = vosa::EigenImage<uint32_t>(3, 3, 2);
  ;
  expected_output.setValues({{{0, 0}, {0, 1}, {0, 2}},
                             {{1, 0}, {1, 1}, {1, 2}},
                             {{2, 0}, {2, 1}, {2, 2}}});
  auto test_output =
      vosa::test::TestBench<uint32_t, int32_t>::compareEigenImages(
          typed_output, expected_output);
  EXPECT_TRUE(test_output);
}