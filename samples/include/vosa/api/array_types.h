
/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_PIPELINES_API_ARRAY_TYPES_H_
#define VOSA_PIPELINES_API_ARRAY_TYPES_H_

#include <unsupported/Eigen/CXX11/Tensor>

namespace vosa::api {

template <typename T>
using EigenImage = Eigen::Tensor<T, 3, Eigen::RowMajor | Eigen::DontAlign>;

template <typename T>
using EigenPlane = Eigen::Tensor<T, 2, Eigen::RowMajor | Eigen::DontAlign>;

template <typename T>
using EigenVector = Eigen::Tensor<T, 1, Eigen::RowMajor | Eigen::DontAlign>;

template <typename T>
using EigenScalar = Eigen::Tensor<T, 0, Eigen::RowMajor | Eigen::DontAlign>;

template <typename T, size_t dim>
using EigenArray = Eigen::Tensor<T, dim, Eigen::RowMajor | Eigen::DontAlign>;

class ArrayBase {
public:
  enum class DType {
    BOOL,
    UINT8,
    UINT16,
    UINT32,
    INT8,
    INT16,
    INT32,
    FLOAT16,
    FLOAT32,
    FLOAT64
  };
};

} // namespace vosa::api

#endif // VOSA_PIPELINES_API_ARRAY_TYPES_H_
