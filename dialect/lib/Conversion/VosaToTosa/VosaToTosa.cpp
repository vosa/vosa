/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/Conversion/VosaToTosa/VosaToTosa.h"
#include "mlir/Dialect/Arith/IR/Arith.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/Linalg/IR/Linalg.h"
#include "mlir/Dialect/Math/IR/Math.h"
#include "mlir/Dialect/SCF/IR/SCF.h"
#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/Dialect/Tosa/IR/TosaOps.h"
#include "mlir/Dialect/Tosa/Utils/QuantUtils.h"
#include "mlir/IR/AffineExpr.h"
#include "mlir/IR/PatternMatch.h"
#include "mlir/IR/SymbolTable.h"
#include "mlir/Transforms/DialectConversion.h"
#include "vosa/Dialect/Vosa/IR/VosaDialect.h"
#include "vosa/Dialect/Vosa/IR/VosaOps.h"
#include "vosa/Dialect/Vosa/Utils/Conversion.h"
#include "vosa/Dialect/Vosa/Utils/DataFormat.h"

#include "../PassDetail.h"

#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"

#define DEBUG_TYPE "vosa-to-tosa"

using namespace mlir;
using namespace mlir::vosa;

namespace {

inline Value makeFilledTensor(Location loc, ArrayRef<int64_t> shape, Type ty,
                              Value val, OpBuilder &builder) {
  auto tensorInit = builder.create<tensor::EmptyOp>(loc, shape, ty).getResult();
  return builder.create<linalg::FillOp>(loc, val, tensorInit).result();
}

inline Value makeFilledTensor(Location loc, ArrayRef<int64_t> shape, Value val,
                              OpBuilder &builder) {
  return makeFilledTensor(loc, shape, val.getType(), val, builder);
}

template <typename T, typename P>
static mlir::arith::SelectOp clampHelper(Location loc, Value arg, Value min,
                                         Value max, P pred,
                                         OpBuilder &rewriter) {
  auto smallerThanMin = rewriter.create<T>(loc, pred, arg, min);
  auto minOrArg =
      rewriter.create<mlir::arith::SelectOp>(loc, smallerThanMin, min, arg);
  auto largerThanMax = rewriter.create<T>(loc, pred, max, arg);
  return rewriter.create<mlir::arith::SelectOp>(loc, largerThanMax, max,
                                                minOrArg);
}

//===----------------------------------------------------------------------===//
// VosaToTosa RewritePatterns
//===----------------------------------------------------------------------===//

struct ConstantOpConverter : public OpConversionPattern<vosa::ConstantOp> {
  using OpConversionPattern<vosa::ConstantOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::ConstantOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const override {
    // replace with arith ConstantOp
    auto outputTy = getTypeConverter()->convertType(op.getType());
    auto value = adaptor.getValue();

    // convert large splat (all the same value) to runtime filled tensor
    // (otherwise large constant planes / image result in large constant arrays
    // in LLVM IR)
    if (auto shapedTy = outputTy.dyn_cast<ShapedType>()) {
      if (auto denseVal =
              adaptor.getValue().dyn_cast<DenseIntOrFPElementsAttr>()) {
        if (denseVal.isSplat() &&
            (!shapedTy.hasStaticShape() || shapedTy.getNumElements() >= 1000)) {
          TypedAttr splatVal;
          if (denseVal.isa<DenseIntElementsAttr>()) {
            // convert to signless value(s)
            splatVal = rewriter.getIntegerAttr(
                shapedTy.getElementType(),
                denseVal.getSplatValue<IntegerAttr>().getValue());
          } else {
            splatVal = denseVal.getSplatValue<FloatAttr>();
          }
          auto splatConst =
              rewriter.create<arith::ConstantOp>(op.getLoc(), splatVal);
          auto filledVal = makeFilledTensor(op.getLoc(), shapedTy.getShape(),
                                            splatConst, rewriter);
          rewriter.replaceOp(op, filledVal);
          return success();
        }
      }

      // convert to signless value(s)
      if (auto intTy = shapedTy.getElementType().dyn_cast<IntegerType>()) {
        auto elementsAttr = adaptor.getValue().cast<DenseIntElementsAttr>();
        value =
            elementsAttr.mapValues(shapedTy.getElementType(),
                                   [&](const APInt &value) { return value; });
      }
    } else if (outputTy.isa<IntegerType>()) {
      value = rewriter.getIntegerAttr(outputTy,
                                      value.cast<IntegerAttr>().getValue());
    }

    auto constVal =
        rewriter.create<arith::ConstantOp>(op.getLoc(), outputTy, value)
            .getResult();
    rewriter.replaceOp(op, constVal);
    return success();
  }
};

//===----------------------------------------------------------------------===//
// Comparison operations.
//===----------------------------------------------------------------------===//

struct EqualOpConverter : public OpConversionPattern<vosa::EqualOp> {
  using OpConversionPattern<vosa::EqualOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::EqualOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputType = getTypeConverter()->convertType(op.getType());
    auto equalOp = rewriter.create<tosa::EqualOp>(
        op.getLoc(), outputType, adaptor.getInput0(), adaptor.getInput1());
    rewriter.replaceOp(op, equalOp.getOutput());
    return success();
  }
};

struct GreaterOpConverter : public OpConversionPattern<vosa::GreaterOp> {
  using OpConversionPattern<vosa::GreaterOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::GreaterOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputType = getTypeConverter()->convertType(op.getType());
    auto greaterOp = rewriter.create<tosa::GreaterOp>(
        op.getLoc(), outputType, adaptor.getInput0(), adaptor.getInput1());
    rewriter.replaceOp(op, greaterOp.getOutput());
    return success();
  }
};

struct GreaterEqualOpConverter
    : public OpConversionPattern<vosa::GreaterEqualOp> {
  using OpConversionPattern<vosa::GreaterEqualOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::GreaterEqualOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputType = getTypeConverter()->convertType(op.getType());
    auto greaterEqualOp = rewriter.create<tosa::GreaterEqualOp>(
        op.getLoc(), outputType, adaptor.getInput0(), adaptor.getInput1());
    rewriter.replaceOp(op, greaterEqualOp.getOutput());
    return success();
  }
};

//===----------------------------------------------------------------------===//
// Data Manipulation operations.
//===----------------------------------------------------------------------===//

struct BroadcastChannelwiseOpConverter
    : public OpConversionPattern<vosa::BroadcastChannelwiseOp> {
  using OpConversionPattern<vosa::BroadcastChannelwiseOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::BroadcastChannelwiseOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputTy =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();

    if (op.getSize() == 1) {
      Value v = adaptor.getInput();
      if (outputTy.getShape() != v.getType().cast<ShapedType>().getShape()) {
        v = rewriter
                .create<tosa::ReshapeOp>(
                    op.getLoc(), outputTy, v,
                    rewriter.getDenseI64ArrayAttr(outputTy.getShape()))
                .getOutput();
      }
      rewriter.replaceOp(op, v);
    } else if (auto fillOp =
                   adaptor.getOperands()[0].getDefiningOp<linalg::FillOp>()) {
      // if broadcast of an already filled value (e.g. BroadcastScalarOp),
      // re-fill to the new shape
      auto filled = makeFilledTensor(op.getLoc(), outputTy.getShape(),
                                     fillOp.value(), rewriter);
      rewriter.replaceOp(op, filled);
    } else {
      // use TOSA tile
      SmallVector<int64_t> multiples =
          hwcToDataFormatShape(SmallVector<int64_t>{1, 1, op.getSize()});
      auto tosaTileOp =
          rewriter
              .create<tosa::TileOp>(op.getLoc(), outputTy, adaptor.getInput(),
                                    rewriter.getDenseI64ArrayAttr(multiples))
              .getOutput();
      rewriter.replaceOp(op, tosaTileOp);
    }
    return success();
  }
};

struct BroadcastPlanewiseOpConverter
    : public OpConversionPattern<vosa::BroadcastPlanewiseOp> {
  using OpConversionPattern<vosa::BroadcastPlanewiseOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::BroadcastPlanewiseOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {

    auto tensor_input = adaptor.getInput();
    auto tensor_input_element_type =
        tensor_input.getType().cast<mlir::ShapedType>().getElementType();
    auto zero_dim_tensor_type =
        mlir::RankedTensorType::get({}, tensor_input_element_type);
    auto reassociation_list = rewriter.getArrayAttr({});
    auto zero_dim_tensor_input = rewriter.create<tensor::CollapseShapeOp>(
        op.getLoc(), zero_dim_tensor_type, tensor_input, reassociation_list);
    auto scalar_input = rewriter.create<tensor::ExtractOp>(
        op.getLoc(), zero_dim_tensor_input, ValueRange{});
    SmallVector<int64_t> size;
    for (Attribute val : op.getSize().getValue()) {
      size.push_back(val.cast<IntegerAttr>().getValue().getSExtValue());
    }
    size.push_back(1);
    auto output = makeFilledTensor(op.getLoc(), hwcToDataFormatShape(size),
                                   scalar_input, rewriter);
    rewriter.replaceOp(op, output);
    return success();
  }
};

static Value makeCastElementOp(Operation *op, Value val, Type resultTy,
                               PatternRewriter &rewriter) {
  Location loc = op->getLoc();
  auto valTy = val.getType(); // converted type
  auto srcTy = op->getOperand(0).getType().cast<ShapedType>().getElementType();
  auto dstTy = op->getResult(0).getType().cast<ShapedType>().getElementType();
  bool isSameWidth =
      srcTy.getIntOrFloatBitWidth() == dstTy.getIntOrFloatBitWidth();
  bool isNarrowing =
      srcTy.getIntOrFloatBitWidth() > dstTy.getIntOrFloatBitWidth();
  vosa::RoundingMode rounding_mode =
      op->getAttr("rounding_mode").cast<vosa::RoundingModeAttr>().getValue();
  bool isSaturating = false;

  switch (rounding_mode) {
  case RoundingMode::wrap:
    break;
  case RoundingMode::saturate:
    isSaturating = true;
    break;
  case RoundingMode::reinterpret:
    (void)rewriter.notifyMatchFailure(op, "unsupported cast mode");
    return nullptr;
  }

  if (srcTy == dstTy)
    return val;

  // sint to
  if (srcTy.isSignedInteger()) {
    // sint
    if (dstTy.isSignedInteger()) {
      if (isSameWidth)
        return val;
      else if (isNarrowing) {
        if (isSaturating) {
          // clamp to range of dest type
          auto dstMin = rewriter.create<arith::ConstantOp>(
              loc,
              rewriter.getIntegerAttr(
                  valTy, APInt::getSignedMinValue(dstTy.getIntOrFloatBitWidth())
                             .sext(valTy.getIntOrFloatBitWidth())));
          auto ltMin = rewriter.create<arith::CmpIOp>(
              loc, arith::CmpIPredicate::slt, val, dstMin);
          val = rewriter.create<mlir::arith::SelectOp>(loc, ltMin, dstMin, val);
          auto dstMax = rewriter.create<arith::ConstantOp>(
              loc,
              rewriter.getIntegerAttr(
                  valTy, APInt::getSignedMaxValue(dstTy.getIntOrFloatBitWidth())
                             .sext(valTy.getIntOrFloatBitWidth())));
          auto ltMax = rewriter.create<arith::CmpIOp>(
              loc, arith::CmpIPredicate::slt, val, dstMax);
          val = rewriter.create<mlir::arith::SelectOp>(loc, ltMax, val, dstMax);
        }
        return rewriter.create<arith::TruncIOp>(loc, resultTy, val);
      } else
        return rewriter.create<arith::ExtSIOp>(loc, resultTy, val);
    }

    // uint
    else if (dstTy.isUnsignedInteger()) {
      if (isSaturating) {
        // clamp to >= 0 if saturating mode
        auto zero = rewriter.create<arith::ConstantOp>(
            loc, rewriter.getIntegerAttr(
                     valTy, APInt(dstTy.getIntOrFloatBitWidth(), 0)
                                .zextOrTrunc(valTy.getIntOrFloatBitWidth())));
        auto ltZero = rewriter.create<arith::CmpIOp>(
            loc, arith::CmpIPredicate::slt, val, zero);
        val = rewriter.create<mlir::arith::SelectOp>(loc, ltZero, zero, val);
        if (isNarrowing) {
          // clamp to dst max on saturating narrow
          auto dstMax = rewriter.create<arith::ConstantOp>(
              loc, rewriter.getIntegerAttr(
                       valTy, APInt::getMaxValue(dstTy.getIntOrFloatBitWidth())
                                  .zext(valTy.getIntOrFloatBitWidth())));
          auto ltMax = rewriter.create<arith::CmpIOp>(
              loc, arith::CmpIPredicate::slt, val, dstMax);
          val = rewriter.create<mlir::arith::SelectOp>(loc, ltMax, val, dstMax);
        }
      }
      if (isSameWidth)
        return rewriter.create<arith::BitcastOp>(loc, resultTy, val);
      else if (isNarrowing)
        return rewriter.create<arith::TruncIOp>(loc, resultTy, val);
      else
        return rewriter.create<arith::ExtSIOp>(loc, resultTy, val);
    }

    // bool
    else if (dstTy.isSignlessInteger() && dstTy.getIntOrFloatBitWidth() == 1) {
      auto zero = rewriter.create<arith::ConstantOp>(
          loc, valTy,
          rewriter.getIntegerAttr(valTy,
                                  APInt(valTy.getIntOrFloatBitWidth(), 0)));
      return rewriter.create<arith::CmpIOp>(loc, arith::CmpIPredicate::ne, val,
                                            zero);
    }

    // float
    else if (dstTy.isa<FloatType>()) {
      return rewriter.create<arith::SIToFPOp>(loc, resultTy, val);
    }
  }

  // uint to
  else if (srcTy.isUnsignedInteger() || srcTy.isSignlessInteger()) {
    // sint
    if (dstTy.isSignedInteger()) {
      if (isSaturating && (isSameWidth || isNarrowing)) {
        // clamp to < SMAX if saturating mode
        auto dstMax = rewriter.create<arith::ConstantOp>(
            loc,
            rewriter.getIntegerAttr(
                valTy, APInt::getSignedMaxValue(dstTy.getIntOrFloatBitWidth())
                           .zext(valTy.getIntOrFloatBitWidth())));
        auto ltMax = rewriter.create<arith::CmpIOp>(
            loc, arith::CmpIPredicate::ult, val, dstMax);
        val = rewriter.create<mlir::arith::SelectOp>(loc, ltMax, val, dstMax);
      }
      if (isSameWidth)
        return rewriter.create<arith::BitcastOp>(loc, resultTy, val);
      else if (isNarrowing)
        return rewriter.create<arith::TruncIOp>(loc, resultTy, val);
      else {
        return rewriter.create<arith::ExtUIOp>(loc, resultTy, val);
      }
    }

    // uint
    else if (dstTy.isUnsignedInteger()) {
      if (isSameWidth)
        return val;
      else if (isNarrowing) {
        if (isSaturating) {
          // clamp to range of dest type
          auto dstMax = rewriter.create<arith::ConstantOp>(
              loc, rewriter.getIntegerAttr(
                       valTy, APInt::getMaxValue(dstTy.getIntOrFloatBitWidth())
                                  .zext(valTy.getIntOrFloatBitWidth())));
          auto ltMax = rewriter.create<arith::CmpIOp>(
              loc, arith::CmpIPredicate::ult, val, dstMax);
          val = rewriter.create<mlir::arith::SelectOp>(loc, ltMax, val, dstMax);
        }
        return rewriter.create<arith::TruncIOp>(loc, resultTy, val);
      } else
        return rewriter.create<arith::ExtUIOp>(loc, resultTy, val);
    }

    // bool
    else if (dstTy.isSignlessInteger() && dstTy.getIntOrFloatBitWidth() == 1) {
      auto zero = rewriter.create<arith::ConstantOp>(
          loc, valTy,
          rewriter.getIntegerAttr(valTy,
                                  APInt(valTy.getIntOrFloatBitWidth(), 0)));
      return rewriter.create<arith::CmpIOp>(loc, arith::CmpIPredicate::ne, val,
                                            zero);
    }

    // float
    else if (dstTy.isa<FloatType>()) {
      return rewriter.create<arith::UIToFPOp>(loc, resultTy, val);
    }
  }

  // float to sint/uint
  else if (srcTy.isa<FloatType>() && dstTy.isa<IntegerType>()) {
    bool isSigned = dstTy.isSignedInteger();
    if (isSaturating) {

      // clamp to range of dest type
      APFloat dstMinF(valTy.cast<FloatType>().getFloatSemantics());
      APFloat dstMaxF(valTy.cast<FloatType>().getFloatSemantics());
      APInt dstMinI;
      APInt dstMaxI;
      if (isSigned) {
        dstMinI = APInt::getSignedMinValue(dstTy.getIntOrFloatBitWidth());
        dstMaxI = APInt::getSignedMaxValue(dstTy.getIntOrFloatBitWidth());
      } else {
        dstMinI = APInt::getMinValue(dstTy.getIntOrFloatBitWidth());
        dstMaxI = APInt::getMaxValue(dstTy.getIntOrFloatBitWidth());
      }
      dstMinF.convertFromAPInt(dstMinI, isSigned, APFloat::rmTowardZero);
      dstMaxF.convertFromAPInt(dstMaxI, isSigned, APFloat::rmTowardZero);

      auto dstMin = rewriter.create<arith::ConstantOp>(
          loc, rewriter.getFloatAttr(valTy, dstMinF));
      auto dstMax = rewriter.create<arith::ConstantOp>(
          loc, rewriter.getFloatAttr(valTy, dstMaxF));
      auto ltMin = rewriter.create<arith::CmpFOp>(
          loc, arith::CmpFPredicate::OLT, val, dstMin);
      val = rewriter.create<mlir::arith::SelectOp>(loc, ltMin, dstMin, val);
      auto ltMax = rewriter.create<arith::CmpFOp>(
          loc, arith::CmpFPredicate::OLT, val, dstMax);
      val = rewriter.create<mlir::arith::SelectOp>(loc, ltMax, val, dstMax);
    }

    if (isSigned)
      return rewriter.create<arith::FPToSIOp>(loc, resultTy, val);
    else
      return rewriter.create<arith::FPToUIOp>(loc, resultTy, val);
  }

  // float to float
  else if (srcTy.isa<FloatType>() && dstTy.isa<FloatType>()) {
    if (isSameWidth)
      return val;
    else if (!isNarrowing)
      return rewriter.create<arith::ExtFOp>(loc, resultTy, val);
    else {
      if (isSaturating) {
        bool lossy;
        // clamp to range of dest type
        APFloat apMin = APFloat::getLargest(
            dstTy.cast<FloatType>().getFloatSemantics(), true);
        apMin.convert(valTy.cast<FloatType>().getFloatSemantics(),
                      APFloat::rmTowardZero, &lossy);
        auto dstMin = rewriter.create<arith::ConstantOp>(
            loc, rewriter.getFloatAttr(valTy, apMin));
        APFloat apMax = APFloat::getLargest(
            dstTy.cast<FloatType>().getFloatSemantics(), false);
        apMax.convert(valTy.cast<FloatType>().getFloatSemantics(),
                      APFloat::rmTowardZero, &lossy);
        auto dstMax = rewriter.create<arith::ConstantOp>(
            loc, rewriter.getFloatAttr(valTy, apMax));
        auto ltMin = rewriter.create<arith::CmpFOp>(
            loc, arith::CmpFPredicate::OLT, val, dstMin);
        val = rewriter.create<mlir::arith::SelectOp>(loc, ltMin, dstMin, val);
        auto ltMax = rewriter.create<arith::CmpFOp>(
            loc, arith::CmpFPredicate::OLT, val, dstMax);
        val = rewriter.create<mlir::arith::SelectOp>(loc, ltMax, val, dstMax);
      }
      return rewriter.create<arith::TruncFOp>(loc, resultTy, val);
    }
  }

  (void)rewriter.notifyMatchFailure(op, "unhandled types for cast: ");
  return nullptr;
}

struct CastOpConverter : public OpConversionPattern<vosa::CastOp> {
  using OpConversionPattern<vosa::CastOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::CastOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    // use linalg generic - can't use TOSA cast as it doesn't support casting to
    // unsigned types and only has saturating semantics

    auto inputTy = adaptor.getInput().getType().cast<ShapedType>();
    auto outputTy =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    auto initOutTensor =
        rewriter
            .create<tensor::EmptyOp>(op.getLoc(), outputTy.getShape(),
                                     outputTy.getElementType())
            .getResult();

    SmallVector<AffineMap, 2> indexingMaps;

    // input indexing map
    indexingMaps.push_back(rewriter.getMultiDimIdentityMap(inputTy.getRank()));

    // output indexing map
    indexingMaps.push_back(rewriter.getMultiDimIdentityMap(inputTy.getRank()));

    bool didEncounterError = false;
    auto linalgOp = rewriter.create<linalg::GenericOp>(
        op.getLoc(), outputTy, adaptor.getInput(), initOutTensor, indexingMaps,
        SmallVector<utils::IteratorType>(inputTy.getRank(),
                                         utils::IteratorType::parallel),
        [&](OpBuilder &nestedBuilder, Location nestedLoc,
            ValueRange blockArgs) {
          Value opResult = makeCastElementOp(
              op, blockArgs.front(), outputTy.getElementType(), rewriter);
          if (!opResult) {
            didEncounterError = true;
            return;
          }
          nestedBuilder.create<linalg::YieldOp>(op.getLoc(), opResult);
        });

    if (didEncounterError)
      return failure();

    rewriter.replaceOp(op, linalgOp->getResults());
    return success();
  }
};

struct ChannelExtractOpConverter
    : public OpConversionPattern<vosa::ChannelExtractOp> {
  using OpConversionPattern<vosa::ChannelExtractOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::ChannelExtractOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto inputType = adaptor.getInput().getType().cast<ShapedType>();
    auto outputTy =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();

    SmallVector<int64_t> channels;
    for (Attribute val : adaptor.getChannels().getValue()) {
      channels.push_back(val.cast<IntegerAttr>().getValue().getSExtValue());
    }

    auto makeSlice = [&](int64_t offset, int64_t count) {
      SmallVector<int64_t> start;
      start.resize(2, 0);
      start.insert(start.begin() + getChannelDimension(), offset);
      SmallVector<int64_t> size;
      size.push_back(getHeight(inputType));
      size.push_back(getWidth(inputType));
      size.insert(size.begin() + getChannelDimension(), count);

      auto sliceTy = RankedTensorType::get(size, outputTy.getElementType());
      return rewriter
          .create<tosa::SliceOp>(op.getLoc(), sliceTy, adaptor.getInput(),
                                 rewriter.getDenseI64ArrayAttr(start),
                                 rewriter.getDenseI64ArrayAttr(size))
          .getOutput();
    };

    // collect contiguous chunks of channels
    auto iChannel = channels.begin();
    int64_t curChunkStart = *iChannel++;
    int64_t curChunkSize = 1;
    SmallVector<Value> slices;

    for (; iChannel != channels.end(); ++iChannel) {
      if (*iChannel == curChunkStart + curChunkSize) {
        curChunkSize++;
      } else {
        // emit slice op
        slices.push_back(makeSlice(curChunkStart, curChunkSize));
        // start new chunk
        curChunkStart = *iChannel;
        curChunkSize = 1;
      }
    }
    // emit trailing slice op
    slices.push_back(makeSlice(curChunkStart, curChunkSize));

    Value result;
    if (slices.size() == 1) {
      // use slice directly
      result = slices[0];
    } else {
      // concat to single output
      result = rewriter
                   .create<tosa::ConcatOp>(
                       op.getLoc(), outputTy, slices,
                       rewriter.getI32IntegerAttr(getChannelDimension()))
                   .getOutput();
    }

    rewriter.replaceOp(op, result);

    return success();
  }
};

struct ConcatOpConverter : public OpConversionPattern<vosa::ConcatOp> {
  using OpConversionPattern<vosa::ConcatOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::ConcatOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputTy = getTypeConverter()->convertType(op.getType());

    if (adaptor.getInput().size() == 1) {
      // nop
      rewriter.replaceOp(op, op.getOperation()->getOperands());
    } else {
      // use TOSA concat
      auto tosaOp = rewriter
                        .create<tosa::ConcatOp>(
                            op.getLoc(), outputTy, adaptor.getInput(),
                            rewriter.getI32IntegerAttr(getChannelDimension()))
                        .getOutput();
      rewriter.replaceOp(op, tosaOp);
    }
    return success();
  }
};

struct ExportChannelOpConverter
    : public OpConversionPattern<vosa::ExportChannelOp> {
  using OpConversionPattern<vosa::ExportChannelOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::ExportChannelOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    // replace with TOSA reshape
    auto inputTy = adaptor.getInput().getType().cast<ShapedType>();
    auto outputTy =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    auto elBytes = inputTy.getElementType().getIntOrFloatBitWidth() / 8;

    SmallVector<int64_t> flatShape{getHeight(inputTy) * getWidth(inputTy)};
    auto flatTy = RankedTensorType::get(flatShape, inputTy.getElementType());
    auto exportData =
        rewriter
            .create<tosa::ReshapeOp>(op.getLoc(), flatTy, adaptor.getInput(),
                                     rewriter.getDenseI64ArrayAttr(flatShape))
            .getOutput();

    if (op.getOffset() != 0 || op.getStride() != 1) {
      SmallVector<int64_t> offsets{op.getOffset() / elBytes};
      SmallVector<int64_t> sizes{flatTy.getDimSize(0)};
      SmallVector<int64_t> strides{op.getStride() / elBytes};
      exportData = rewriter
                       .create<tensor::InsertSliceOp>(
                           op.getLoc(), outputTy, exportData, adaptor.getDest(),
                           ValueRange{}, ValueRange{}, ValueRange{}, offsets,
                           sizes, strides)
                       .getResult();
    }
    rewriter.replaceOp(op, exportData);
    return success();
  }
};

struct ImportChannelOpConverter
    : public OpConversionPattern<vosa::ImportChannelOp> {
  using OpConversionPattern<vosa::ImportChannelOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::ImportChannelOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {

    auto outputTy =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    auto elBytes = outputTy.getElementType().getIntOrFloatBitWidth() / 8;

    // extract required amount of data from input tensor (possibly unknown size)
    SmallVector<int64_t> flatShape{getHeight(outputTy) * getWidth(outputTy)};
    auto flatTy = RankedTensorType::get(flatShape, outputTy.getElementType());
    SmallVector<int64_t> offsets{op.getOffset() / elBytes};
    SmallVector<int64_t> strides{op.getStride() / elBytes};
    auto importData =
        rewriter
            .create<tensor::ExtractSliceOp>(
                op.getLoc(), flatTy, adaptor.getInput(), ValueRange{},
                ValueRange{}, ValueRange{}, offsets, flatShape, strides)
            .getResult();

    auto reshapeOp = rewriter.create<tosa::ReshapeOp>(
        op.getLoc(), outputTy, importData,
        rewriter.getDenseI64ArrayAttr(outputTy.getShape()));
    rewriter.replaceOp(op, reshapeOp.getOutput());
    return success();
  }
};

Value iotaOp(Location loc, OpBuilder &builder, Type elementType, int64_t axis,
             int64_t size) {
  llvm::SmallVector<int64_t, 3> shape{1, 1, 1};
  shape[axis] = size;
  auto iotaTy = RankedTensorType::get(shape, elementType);

  auto initOutTensor =
      builder.create<tensor::EmptyOp>(loc, iotaTy.getShape(), elementType)
          .getResult();

  SmallVector<AffineMap, 2> indexingMaps;
  // output indexing map
  indexingMaps.push_back(builder.getMultiDimIdentityMap(iotaTy.getRank()));

  auto linalgOp = builder.create<linalg::GenericOp>(
      loc, iotaTy, ValueRange{}, initOutTensor, indexingMaps,
      SmallVector<utils::IteratorType>(iotaTy.getRank(),
                                       utils::IteratorType::parallel),
      [&](OpBuilder &nestedBuilder, Location nestedLoc, ValueRange blockArgs) {
        Value index = nestedBuilder.create<linalg::IndexOp>(nestedLoc, axis);
        Value iVal = nestedBuilder.create<arith::IndexCastOp>(
            nestedLoc, elementType, index);
        nestedBuilder.create<linalg::YieldOp>(nestedLoc, iVal);
      });
  return linalgOp.getResult(0);
}

struct MeshGridOpConverter : public OpConversionPattern<vosa::MeshGridOp> {
  using OpConversionPattern<vosa::MeshGridOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::MeshGridOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputTy =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    SmallVector<int64_t> shape;
    for (Attribute val : adaptor.getShape().getValue()) {
      shape.push_back(val.cast<IntegerAttr>().getValue().getSExtValue());
    }
    auto planeTy =
        RankedTensorType::get(hwcToDataFormatShape({shape[0], shape[1], 1}),
                              outputTy.getElementType());

    // create Y plane
    auto yVals = iotaOp(op.getLoc(), rewriter, outputTy.getElementType(),
                        getHeightDimension(), shape[0]);
    auto yPlane =
        rewriter
            .create<tosa::TileOp>(op.getLoc(), planeTy, yVals,
                                  rewriter.getDenseI64ArrayAttr(
                                      hwcToDataFormatShape({1, shape[1], 1})))
            .getOutput();

    // create X plane
    auto xVals = iotaOp(op.getLoc(), rewriter, outputTy.getElementType(),
                        getWidthDimension(), shape[1]);
    auto xPlane =
        rewriter
            .create<tosa::TileOp>(op.getLoc(), planeTy, xVals,
                                  rewriter.getDenseI64ArrayAttr(
                                      hwcToDataFormatShape({shape[0], 1, 1})))
            .getOutput();

    // concat to form 2D image
    auto tosaOp = rewriter
                      .create<tosa::ConcatOp>(
                          op.getLoc(), outputTy, ValueRange{yPlane, xPlane},
                          rewriter.getI32IntegerAttr(getChannelDimension()))
                      .getOutput();
    rewriter.replaceOp(op, tosaOp);

    return success();
  }
};

struct PiecewiseLinearOpConverter
    : public OpConversionPattern<vosa::PiecewiseLinearOp> {
  using OpConversionPattern<vosa::PiecewiseLinearOp>::OpConversionPattern;

  Value extractValueFromArray(OpBuilder &rewriter, Location loc, Value array,
                              Value position) const {
    auto index = rewriter.create<arith::IndexCastOp>(
        loc, rewriter.getIndexType(), position);
    return rewriter.create<tensor::ExtractOp>(loc, array, ValueRange{index})
        .getResult();
  }

  bool canOptimizeLUT(ElementsAttr nodes, Type eltTy,
                      SmallVector<APInt> &segmentNodes,
                      SmallVector<APInt> &segmentWidths,
                      int &scaleFactor) const {
    int index = 0;
    APInt currentSegmentSpacing = APInt(eltTy.getIntOrFloatBitWidth(), 0);
    APInt nodesInCurrentSegment = APInt(eltTy.getIntOrFloatBitWidth(), 0);
    APInt nextExpectedWidth = APInt(eltTy.getIntOrFloatBitWidth(), 0);
    auto values = nodes.getValues<APInt>();

    scaleFactor = -1;

    assert(values.size() != 0);

    auto saveSegment = [&](APInt totalWidth) {
      if (currentSegmentSpacing != 0) {
        if (scaleFactor == -1) {
          if (!llvm::isPowerOf2_64(totalWidth.getZExtValue()))
            return false;

          scaleFactor = log2(totalWidth.getZExtValue()) - 4;
        }

        if (nextExpectedWidth != totalWidth && index > 0)
          return false;

        segmentWidths.push_back(totalWidth);
        segmentNodes.push_back(nodesInCurrentSegment);
        index++;
      }

      nextExpectedWidth = pow(2, index + 3 + scaleFactor);

      return true;
    };

    APInt lastNode = values[0];
    for (auto it = values.begin() + 1; it != values.end(); ++it) {
      APInt segmentSpacing = *it - lastNode;
      lastNode = *it;

      APInt totalWidth = nodesInCurrentSegment * currentSegmentSpacing;

      if (segmentSpacing == currentSegmentSpacing &&
          nextExpectedWidth != totalWidth) {
        nodesInCurrentSegment += 1;
        continue;
      }

      if (!saveSegment(totalWidth))
        return false;

      currentSegmentSpacing = segmentSpacing;
      nodesInCurrentSegment = 1;
    }

    return saveSegment(nodesInCurrentSegment * currentSegmentSpacing);
  }

  std::tuple<Value, Value> generateOptimizedLut(
      OpBuilder &rewriter, Location loc, Type eltTy, Value inputValue,
      int maxNode, SmallVector<APInt> segmentNodes,
      SmallVector<APInt> segmentWidths, int scaleFactor) const {
    // width_scaled(s) = max(1, segment_width(s) << scale_factor);
    // Expr last_seg_index = lut1d::N_SEGMENTS - 1;
    //
    // RDom r(0, last_seg_index);
    // left_node(s) = 0;
    // left_node(r + 1) = left_node(r) + n_nodes_in_segment(r);
    // node_gap(s) = width_scaled(s) / n_nodes_in_segment(s);
    //
    // left_val(s) = select(s == 0, 0, 1 << (s + 3 + scale_factor));
    // Expr msb = 32 - clz(cast<int32_t>(x >> scale_factor)) - 1;
    // Expr target_seg = cast<int>(clamp(msb - 3, 0, last_seg_index));
    // Expr node_in_seg = (x - left_val(target_seg)) / node_gap(target_seg);
    // Expr m_index = left_node(target_seg) + node_in_seg;

    SmallVector<APInt> leftNodes;
    APInt leftNode = APInt(eltTy.getIntOrFloatBitWidth(), 0);

    leftNodes.push_back(leftNode);
    for (auto segmentNode : segmentNodes) {
      leftNode += segmentNode;
      leftNodes.push_back(leftNode);
    }

    auto swAttrType =
        RankedTensorType::get({(int64_t)segmentWidths.size()}, eltTy);
    auto segmentWidthsCst = rewriter.create<vosa::ConstantOp>(
        loc, swAttrType, DenseIntElementsAttr::get(swAttrType, segmentWidths));

    auto snAttrType =
        RankedTensorType::get({(int64_t)segmentNodes.size()}, eltTy);
    auto segmentNodesCst = rewriter.create<vosa::ConstantOp>(
        loc, snAttrType, DenseIntElementsAttr::get(snAttrType, segmentNodes));

    auto lnAttrType = RankedTensorType::get({(int64_t)leftNodes.size()}, eltTy);
    auto leftNodesCst = rewriter.create<vosa::ConstantOp>(
        loc, lnAttrType, DenseIntElementsAttr::get(lnAttrType, leftNodes));

    Value zeroCst = rewriter.create<arith::ConstantIntOp>(
        loc, 0, eltTy.getIntOrFloatBitWidth());
    Value oneCst = rewriter.create<arith::ConstantIntOp>(
        loc, 1, eltTy.getIntOrFloatBitWidth());
    Value threeCst = rewriter.create<arith::ConstantIntOp>(
        loc, 3, eltTy.getIntOrFloatBitWidth());
    Value scaleFactorCst = rewriter.create<arith::ConstantIntOp>(
        loc, scaleFactor, eltTy.getIntOrFloatBitWidth());

    Value scaledInput =
        rewriter.create<arith::ShRSIOp>(loc, inputValue, scaleFactorCst);
    Value msb = rewriter.create<arith::SubIOp>(
        loc,
        rewriter.create<arith::SubIOp>(
            loc,
            rewriter.create<arith::ConstantIntOp>(
                loc, eltTy.getIntOrFloatBitWidth(),
                eltTy.getIntOrFloatBitWidth()),
            rewriter.create<math::CountLeadingZerosOp>(loc, eltTy,
                                                       scaledInput)),
        oneCst);

    Value targetSeg = clampHelper<arith::CmpIOp>(
        loc, rewriter.create<arith::SubIOp>(loc, msb, threeCst), zeroCst,
        rewriter.create<arith::ConstantIntOp>(loc, segmentWidths.size() - 1,
                                              eltTy.getIntOrFloatBitWidth()),
        arith::CmpIPredicate::slt, rewriter);

    Value leftVal = rewriter.create<arith::SelectOp>(
        loc, eltTy,
        rewriter.create<arith::CmpIOp>(loc, arith::CmpIPredicate::eq, targetSeg,
                                       zeroCst),
        zeroCst,
        rewriter.create<arith::ShLIOp>(
            loc, oneCst,
            rewriter.create<arith::AddIOp>(
                loc, targetSeg,
                rewriter.create<arith::AddIOp>(loc, scaleFactorCst,
                                               threeCst))));

    Value targetSegWidth =
        extractValueFromArray(rewriter, loc, segmentWidthsCst, targetSeg);
    Value targetSegNodes =
        extractValueFromArray(rewriter, loc, segmentNodesCst, targetSeg);

    Value nodeGap = rewriter.create<arith::DivUIOp>(
        loc, rewriter.create<arith::MaxUIOp>(loc, oneCst, targetSegWidth),
        targetSegNodes);

    Value targetNodeInSeg = rewriter.create<arith::DivUIOp>(
        loc, rewriter.create<arith::SubIOp>(loc, inputValue, leftVal), nodeGap);

    Value targetSegLeft =
        extractValueFromArray(rewriter, loc, leftNodesCst, targetSeg);

    Value lowerIndexValue = clampHelper<arith::CmpIOp>(
        loc,
        rewriter.create<arith::AddIOp>(loc, targetSegLeft, targetNodeInSeg),
        rewriter.create<arith::ConstantIntOp>(loc, 0,
                                              eltTy.getIntOrFloatBitWidth()),
        rewriter.create<arith::ConstantIntOp>(loc, maxNode,
                                              eltTy.getIntOrFloatBitWidth()),
        arith::CmpIPredicate::slt, rewriter);

    // TODO: Clamp this?
    Value upperIndexValue =
        rewriter.create<arith::AddIOp>(loc, lowerIndexValue, oneCst);

    return std::make_tuple(lowerIndexValue, upperIndexValue);
  }

  std::tuple<Value, Value> generateUnoptimizedLut(OpBuilder &rewriter,
                                                  Location loc, Type eltTy,
                                                  Value inputValue, int maxNode,
                                                  Value nodes) const {
    // If nodes[0] < input[y,x,c] <= nodes[N-1]
    // For n = 1 to N-1
    //  If input[y,x,c] <= nodes[n]
    //      delta = values[n] - values[n-1]
    //      offset = input[y,x,c] - nodes[n-1]
    //      norm = nodes[n] - nodes[n-1]
    //      if intOp return values[n-1] + int_norm((delta *
    //      offset), norm) else return values[n-1] +
    //      ((delta*offset)/norm)

    Value one = rewriter.create<arith::ConstantIntOp>(
        loc, 1, eltTy.getIntOrFloatBitWidth());

    // Find input's position in nodes array
    auto whileNodeNotFound = rewriter.create<scf::WhileOp>(
        loc, TypeRange{one.getType()}, ValueRange{one});

    // scf::WhileOp's before block
    Block *before = rewriter.createBlock(&whileNodeNotFound.getBefore(), {},
                                         TypeRange{one.getType()},
                                         SmallVector<Location>{loc});
    rewriter.setInsertionPointToEnd(before);
    auto whileCounterVar = before->getArguments().front();
    auto currentNode =
        extractValueFromArray(rewriter, loc, nodes, whileCounterVar);
    auto counterVarLimitCmp = rewriter.create<arith::CmpIOp>(
        loc, arith::CmpIPredicate::sle, whileCounterVar,
        rewriter.create<arith::ConstantIntOp>(loc, maxNode,
                                              eltTy.getIntOrFloatBitWidth()));

    Value counterVarNodeCmp;
    if (eltTy.isa<IntegerType>())
      counterVarNodeCmp =
          rewriter
              .create<arith::CmpIOp>(loc, arith::CmpIPredicate::sgt, inputValue,
                                     currentNode)
              .getResult();
    else
      counterVarNodeCmp =
          rewriter
              .create<arith::CmpFOp>(loc, arith::CmpFPredicate::OGT, inputValue,
                                     currentNode)
              .getResult();

    auto counterVarCmp = rewriter.create<arith::AndIOp>(loc, counterVarLimitCmp,
                                                        counterVarNodeCmp);
    rewriter.create<scf::ConditionOp>(loc, counterVarCmp,
                                      ValueRange{whileCounterVar});

    // scf::WhileOp's after block
    Block *after = rewriter.createBlock(&whileNodeNotFound.getAfter(), {},
                                        TypeRange{one.getType()},
                                        SmallVector<Location>{loc});
    rewriter.setInsertionPointToStart(after);
    auto counterInc =
        rewriter.create<arith::AddIOp>(loc, after->getArguments().front(), one);
    rewriter.create<scf::YieldOp>(loc, counterInc.getResult());

    rewriter.setInsertionPointAfter(whileNodeNotFound);

    Value upperIndexValue = whileNodeNotFound.getResult(0);
    Value lowerIndexValue =
        rewriter.create<arith::SubIOp>(loc, upperIndexValue, one);

    return std::make_tuple(lowerIndexValue, upperIndexValue);
  }

  LogicalResult
  matchAndRewrite(vosa::PiecewiseLinearOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto opLoc = op.getLoc();
    auto inputType = adaptor.getInput().getType().cast<ShapedType>();
    auto outputType =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    auto outputElementType = outputType.getElementType();
    auto outputTensor = rewriter
                            .create<tensor::EmptyOp>(
                                opLoc, outputType.getShape(), outputElementType)
                            .getResult();

    bool isIntOp = outputType.getElementType().isa<IntegerType>();

    SmallVector<APInt> segmentWidths;
    SmallVector<APInt> segmentNodes;
    int scaleFactor = -1;
    // TODO: Can we make this work with floats?
    bool optimizeLUT =
        isIntOp && canOptimizeLUT(adaptor.getNodes(), outputElementType,
                                  segmentNodes, segmentWidths, scaleFactor);

    // Turn nodes and values attributes into accessible tensors
    auto nvAttrType =
        RankedTensorType::get({adaptor.getNodes().size()}, outputElementType);
    auto nodes = rewriter.create<vosa::ConstantOp>(opLoc, nvAttrType,
                                                   adaptor.getNodes());
    auto values = rewriter.create<vosa::ConstantOp>(opLoc, nvAttrType,
                                                    adaptor.getValues());

    auto zeroAttr =
        cast<TypedAttr>(rewriter.getIntegerAttr(rewriter.getIndexType(), 0));
    auto oneAttr = (isIntOp) ? rewriter.getIntegerAttr(outputElementType, 1)
                             : rewriter.getI32IntegerAttr(1);
    int nodesSize = adaptor.getNodes().size() - 1;
    auto sizeAttr = (isIntOp)
                        ? rewriter.getIntegerAttr(outputElementType, nodesSize)
                        : rewriter.getI32IntegerAttr(nodesSize);

    Value zero =
        rewriter.create<arith::ConstantOp>(opLoc, zeroAttr).getResult();
    Value one = rewriter.create<arith::ConstantOp>(opLoc, oneAttr).getResult();
    Value size =
        rewriter.create<arith::ConstantOp>(opLoc, sizeAttr).getResult();

    SmallVector<AffineMap, 2> indexingMaps;
    indexingMaps.push_back(
        rewriter.getMultiDimIdentityMap(inputType.getRank()));
    indexingMaps.push_back(
        rewriter.getMultiDimIdentityMap(outputType.getRank()));

    auto genericOp = rewriter.create<linalg::GenericOp>(
        opLoc, outputType, adaptor.getInput(), outputTensor, indexingMaps,
        SmallVector<utils::IteratorType>(inputType.getRank(),
                                         utils::IteratorType::parallel),
        [&](OpBuilder &nestedBuilder, Location nestedLoc,
            ValueRange blockArgs) {
          // For each pixel in the input tensor
          auto inputValue = blockArgs.front();
          auto zerothNode =
              nestedBuilder.create<tensor::ExtractOp>(nestedLoc, nodes, zero)
                  .getResult();

          Value zeroNodeCmp;
          if (isIntOp)
            zeroNodeCmp =
                nestedBuilder
                    .create<arith::CmpIOp>(nestedLoc, arith::CmpIPredicate::sle,
                                           inputValue, zerothNode)
                    .getResult();
          else
            zeroNodeCmp =
                nestedBuilder
                    .create<arith::CmpFOp>(nestedLoc, arith::CmpFPredicate::OLE,
                                           inputValue, zerothNode)
                    .getResult();

          auto ifLEZeroNode = nestedBuilder.create<mlir::scf::IfOp>(
              nestedLoc, zeroNodeCmp,
              [&](OpBuilder &ifNestedBuilder, Location ifLoc) {
                // If input[y,x,c] <= nodes[0]
                auto zerothValue =
                    ifNestedBuilder
                        .create<tensor::ExtractOp>(ifLoc, values, zero)
                        .getResult();
                ifNestedBuilder.create<scf::YieldOp>(ifLoc, zerothValue);
              },
              [&](OpBuilder &ifNestedBuilder, Location ifLoc) {
                auto finalNode =
                    extractValueFromArray(ifNestedBuilder, ifLoc, nodes, size);

                Value finalNodeCmp;
                if (isIntOp)
                  finalNodeCmp = ifNestedBuilder.create<arith::CmpIOp>(
                      ifLoc, arith::CmpIPredicate::sgt, inputValue, finalNode);
                else
                  finalNodeCmp = ifNestedBuilder.create<arith::CmpFOp>(
                      ifLoc, arith::CmpFPredicate::OGT, inputValue, finalNode);

                scf::IfOp ifGTFinalNode = ifNestedBuilder.create<scf::IfOp>(
                    ifLoc, finalNodeCmp,
                    [&](OpBuilder &finalIfNestedBuilder, Location finalIfLoc) {
                      // If input[y,x,c] > nodes[N-1]
                      auto finalValue = extractValueFromArray(
                          finalIfNestedBuilder, finalIfLoc, values, size);
                      finalIfNestedBuilder.create<scf::YieldOp>(finalIfLoc,
                                                                finalValue);
                    },
                    [&](OpBuilder &finalIfNestedBuilder, Location finalIfLoc) {
                      Value lowerIndexValue, upperIndexValue;
                      if (optimizeLUT)
                        std::tie(lowerIndexValue, upperIndexValue) =
                            generateOptimizedLut(
                                finalIfNestedBuilder, finalIfLoc,
                                outputElementType, inputValue, nodesSize,
                                segmentNodes, segmentWidths, scaleFactor);
                      else
                        std::tie(lowerIndexValue, upperIndexValue) =
                            generateUnoptimizedLut(
                                finalIfNestedBuilder, finalIfLoc,
                                outputElementType, inputValue, nodesSize,
                                nodes);

                      // Extract nodes and values entries at n and n-1
                      auto lowerNode = extractValueFromArray(
                          finalIfNestedBuilder, finalIfLoc, nodes,
                          lowerIndexValue);
                      auto upperNode = extractValueFromArray(
                          finalIfNestedBuilder, finalIfLoc, nodes,
                          upperIndexValue);
                      auto lowerValue = extractValueFromArray(
                          finalIfNestedBuilder, finalIfLoc, values,
                          lowerIndexValue);
                      auto upperValue = extractValueFromArray(
                          finalIfNestedBuilder, finalIfLoc, values,
                          upperIndexValue);

                      Value newPixelValue;

                      if (isIntOp) {
                        // Calculated delta, offset, norm
                        auto delta = finalIfNestedBuilder.create<arith::SubIOp>(
                            finalIfLoc, upperValue, lowerValue);
                        auto offset =
                            finalIfNestedBuilder.create<arith::SubIOp>(
                                finalIfLoc, inputValue, lowerNode);
                        auto norm = finalIfNestedBuilder.create<arith::SubIOp>(
                            finalIfLoc, upperNode, lowerNode);

                        // Compute int_norm
                        auto int_normFirstArg =
                            finalIfNestedBuilder.create<arith::MulIOp>(
                                finalIfLoc, delta, offset);
                        auto int_normQuotientNumerator =
                            finalIfNestedBuilder.create<arith::ShLIOp>(
                                finalIfLoc, int_normFirstArg, one);
                        auto int_normQuotient =
                            finalIfNestedBuilder.create<arith::DivSIOp>(
                                finalIfLoc, int_normQuotientNumerator, norm);
                        auto int_normParentheses =
                            finalIfNestedBuilder.create<arith::AddIOp>(
                                finalIfLoc, int_normQuotient, one);
                        auto int_norm =
                            finalIfNestedBuilder.create<arith::ShRSIOp>(
                                finalIfLoc, int_normParentheses, one);

                        newPixelValue =
                            finalIfNestedBuilder
                                .create<arith::AddIOp>(finalIfLoc, lowerValue,
                                                       int_norm)
                                .getResult();
                      } else {
                        // Calculated delta, offset, norm
                        auto delta = finalIfNestedBuilder.create<arith::SubFOp>(
                            finalIfLoc, upperValue, lowerValue);
                        auto offset =
                            finalIfNestedBuilder.create<arith::SubFOp>(
                                finalIfLoc, inputValue, lowerNode);
                        auto norm = finalIfNestedBuilder.create<arith::SubFOp>(
                            finalIfLoc, upperNode, lowerNode);

                        // Compute (delta*offset)/norm
                        auto numerator =
                            finalIfNestedBuilder.create<arith::MulFOp>(
                                finalIfLoc, delta, offset);
                        auto quotient =
                            finalIfNestedBuilder.create<arith::DivFOp>(
                                finalIfLoc, numerator, norm);

                        newPixelValue =
                            finalIfNestedBuilder
                                .create<arith::AddFOp>(finalIfLoc, lowerValue,
                                                       quotient)
                                .getResult();
                      }

                      finalIfNestedBuilder.create<scf::YieldOp>(finalIfLoc,
                                                                newPixelValue);
                    });

                ifNestedBuilder.create<scf::YieldOp>(
                    ifLoc, ifGTFinalNode.getResults());
              });

          nestedBuilder.create<linalg::YieldOp>(nestedLoc,
                                                ifLEZeroNode.getResults());
        });

    rewriter.replaceOp(op, genericOp->getResult(0));
    return success();
  }
};

struct TosaGraphOpConverter : public OpConversionPattern<vosa::TosaGraphOp> {
  using OpConversionPattern<vosa::TosaGraphOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::TosaGraphOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputType = op.getType();
    auto loc = op.getLoc();

    auto dataFormatTransposeTy =
        mlir::RankedTensorType::get({3}, rewriter.getI64Type());

    auto funcAttr = op->getAttrOfType<FlatSymbolRefAttr>("function");
    func::FuncOp func =
        SymbolTable::lookupNearestSymbolFrom<func::FuncOp>(op, funcAttr);
    if (!func) {
      return op.emitError() << "'" << funcAttr.getValue()
                            << "' does not reference a valid function";
    }

    // reshape input value (dynamic sized 3D tensor at this stage) to function
    // arg type (likely a fixed sized 4/5D tensor)
    llvm::SmallVector<mlir::Value> castedArgs;
    for (const auto &i : llvm::zip(op.getInputs(), func.getArgumentTypes())) {
      auto inputVal = std::get<0>(i);
      auto funcArg = std::get<1>(i);
      if (auto inputShapedTy =
              inputVal.getType().dyn_cast<mlir::ShapedType>()) {
        // TOSA pipelines are HWC format, so need to transpose in/out with CHW
        // pipelines
        if (getDataFormat() == DataFormat::CHW &&
            inputShapedTy.getRank() == 3) {
          auto hwcTy = mlir::RankedTensorType::get(
              {inputShapedTy.getDimSize(1), inputShapedTy.getDimSize(2),
               inputShapedTy.getDimSize(0)},
              inputShapedTy.getElementType());
          auto chwToHwc = rewriter.create<mlir::arith::ConstantOp>(
              loc, dataFormatTransposeTy,
              mlir::DenseIntElementsAttr::get(
                  dataFormatTransposeTy, llvm::SmallVector<int64_t>{1, 2, 0}));
          inputVal = rewriter.create<mlir::tosa::TransposeOp>(
              loc, hwcTy, inputVal, chwToHwc);
        }
      }
      if (auto funcArgShapedTy = funcArg.dyn_cast<mlir::ShapedType>()) {
        inputVal =
            rewriter
                .create<mlir::tosa::ReshapeOp>(
                    loc, funcArgShapedTy, inputVal,
                    rewriter.getDenseI64ArrayAttr(funcArgShapedTy.getShape()))
                .getResult();
      }
      castedArgs.push_back(inputVal);
    }

    // call the TOSA function
    auto callOp = rewriter.create<mlir::func::CallOp>(loc, func, castedArgs);
    auto res = callOp.getResult(0);

    // reshape the output into a 3D tensor
    if (auto outShapedTy = outputType.dyn_cast<mlir::ShapedType>()) {
      auto hwcOutTy = outShapedTy;
      if (getDataFormat() == DataFormat::CHW && outShapedTy.getRank() == 3) {
        hwcOutTy = mlir::RankedTensorType::get({outShapedTy.getDimSize(1),
                                                outShapedTy.getDimSize(2),
                                                outShapedTy.getDimSize(0)},
                                               outShapedTy.getElementType());
      }
      if (res.getType() != hwcOutTy) {
        res = rewriter
                  .create<mlir::tosa::ReshapeOp>(
                      loc, hwcOutTy, res,
                      rewriter.getDenseI64ArrayAttr(hwcOutTy.getShape()))
                  .getResult();
      }
      // transpose back to CHW format if needed
      if (getDataFormat() == DataFormat::CHW && outShapedTy.getRank() == 3) {
        auto hwcToChw = rewriter.create<mlir::arith::ConstantOp>(
            loc, dataFormatTransposeTy,
            mlir::DenseIntElementsAttr::get(
                dataFormatTransposeTy, llvm::SmallVector<int64_t>{2, 0, 1}));
        res = rewriter.create<mlir::tosa::TransposeOp>(loc, outShapedTy, res,
                                                       hwcToChw);
      }
    }

    rewriter.replaceOp(op, res);
    return success();
  }
};

struct WhereOpConverter : public OpConversionPattern<vosa::WhereOp> {
  using OpConversionPattern<vosa::WhereOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::WhereOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputType = getTypeConverter()->convertType(op.getType());
    auto tosaSelectOp =
        rewriter
            .create<tosa::SelectOp>(op.getLoc(), outputType, op.getInput2(),
                                    adaptor.getInput0(), adaptor.getInput1())
            .getOutput();
    rewriter.replaceOp(op, tosaSelectOp);
    return success();
  }
};

//===----------------------------------------------------------------------===//
// Elementwise binary operations.
//===----------------------------------------------------------------------===//

struct AbsDiffOpConverter : public OpConversionPattern<vosa::AbsDiffOp> {
  using OpConversionPattern<vosa::AbsDiffOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::AbsDiffOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputType = getTypeConverter()->convertType(op.getType());
    auto tosaSubOp = rewriter.create<tosa::SubOp>(
        op.getLoc(), outputType, adaptor.getInput0(), adaptor.getInput1());
    auto tosaAbsOp =
        rewriter
            .create<tosa::AbsOp>(op.getLoc(), outputType, tosaSubOp.getOutput())
            .getOutput();
    rewriter.replaceOp(op, tosaAbsOp);
    return success();
  }
};

struct AddOpConverter : public OpConversionPattern<vosa::AddOp> {
  using OpConversionPattern<vosa::AddOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::AddOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    // replace with TOSA AddOp
    auto outputTy = getTypeConverter()->convertType(op.getType());
    auto tosaOp =
        rewriter
            .create<tosa::AddOp>(op.getLoc(), outputTy, adaptor.getInput0(),
                                 adaptor.getInput1())
            .getOutput();
    rewriter.replaceOp(op, tosaOp);
    return success();
  }
};

struct AndOpConverter : public OpConversionPattern<vosa::AndOp> {
  using OpConversionPattern<vosa::AndOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::AndOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    // replace with TOSA AndOp
    auto outputType = getTypeConverter()->convertType(op.getType());
    auto tosaOp = rewriter
                      .create<tosa::BitwiseAndOp>(op.getLoc(), outputType,
                                                  adaptor.getInput0(),
                                                  adaptor.getInput1())
                      .getOutput();
    rewriter.replaceOp(op, tosaOp);
    return success();
  }
};

struct DivOpConverter : public OpConversionPattern<vosa::DivOp> {
  using OpConversionPattern<vosa::DivOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::DivOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {

    auto outputType =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    auto adaptedElementType = outputType.cast<ShapedType>().getElementType();

    auto inputType = op.getInput0().getType().cast<ShapedType>();

    auto outputTensor =
        rewriter
            .create<tensor::EmptyOp>(op.getLoc(), outputType.getShape(),
                                     adaptedElementType)
            .getResult();

    SmallVector<utils::IteratorType> iterators;
    iterators.push_back(utils::IteratorType::parallel);
    iterators.push_back(utils::IteratorType::parallel);
    iterators.push_back(utils::IteratorType::parallel);

    SmallVector<AffineMap, 3> indexingMaps;
    indexingMaps.push_back(
        rewriter.getMultiDimIdentityMap(inputType.getRank()));
    indexingMaps.push_back(
        rewriter.getMultiDimIdentityMap(inputType.getRank()));
    indexingMaps.push_back(
        rewriter.getMultiDimIdentityMap(inputType.getRank()));

    auto genericOp = rewriter.create<linalg::GenericOp>(
        op.getLoc(), outputType,
        ValueRange{adaptor.getInput0(), adaptor.getInput1()}, outputTensor,
        indexingMaps, iterators,
        [&](OpBuilder &nestedBuilder, Location nestedLoc,
            ValueRange blockArgs) {
          Value in0 = blockArgs[0];
          Value in1 = blockArgs[1];
          Value divOp = nestedBuilder
                            .create<arith::DivFOp>(nestedLoc,
                                                   adaptedElementType, in0, in1)
                            .getResult();
          nestedBuilder.create<linalg::YieldOp>(nestedLoc, divOp);
        });
    rewriter.replaceOp(op, genericOp.getResult(0));
    return success();
  }
};

struct MaxOpConverter : public OpConversionPattern<vosa::MaxOp> {
  using OpConversionPattern<vosa::MaxOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::MaxOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputType = getTypeConverter()->convertType(op.getType());
    auto maxOp = rewriter.create<tosa::MaximumOp>(
        op.getLoc(), outputType, adaptor.getInput0(), adaptor.getInput1());
    rewriter.replaceOp(op, maxOp.getOutput());
    return success();
  }
};

struct MinOpConverter : public OpConversionPattern<vosa::MinOp> {
  using OpConversionPattern<vosa::MinOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::MinOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputType = getTypeConverter()->convertType(op.getType());
    auto minOp = rewriter.create<tosa::MinimumOp>(
        op.getLoc(), outputType, adaptor.getInput0(), adaptor.getInput1());
    rewriter.replaceOp(op, minOp.getOutput());
    return success();
  }
};

struct ModOpConverter : public OpConversionPattern<vosa::ModOp> {
  using OpConversionPattern<vosa::ModOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::ModOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {

    auto outputType =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    auto adaptedElementType = outputType.cast<ShapedType>().getElementType();

    auto inputType = op.getInput0().getType().cast<ShapedType>();
    auto elementType = inputType.getElementType();
    bool isSignedIntOp = elementType.isSignedInteger();
    bool isUnsignedIntOp = elementType.isUnsignedInteger();

    auto outputTensor =
        rewriter
            .create<tensor::EmptyOp>(op.getLoc(), outputType.getShape(),
                                     adaptedElementType)
            .getResult();

    SmallVector<utils::IteratorType> iterators;
    iterators.push_back(utils::IteratorType::parallel);
    iterators.push_back(utils::IteratorType::parallel);
    iterators.push_back(utils::IteratorType::parallel);

    SmallVector<AffineMap, 3> indexingMaps;
    indexingMaps.push_back(
        rewriter.getMultiDimIdentityMap(inputType.getRank()));
    indexingMaps.push_back(
        rewriter.getMultiDimIdentityMap(inputType.getRank()));
    indexingMaps.push_back(
        rewriter.getMultiDimIdentityMap(inputType.getRank()));

    auto genericOp = rewriter.create<linalg::GenericOp>(
        op.getLoc(), outputType,
        ValueRange{adaptor.getInput0(), adaptor.getInput1()}, outputTensor,
        indexingMaps, iterators,
        [&](OpBuilder &nestedBuilder, Location nestedLoc,
            ValueRange blockArgs) {
          Value modOp;
          Value in0 = blockArgs.front();
          Value in1 = *(++blockArgs.begin());

          if (isUnsignedIntOp)
            modOp = nestedBuilder
                        .create<arith::RemUIOp>(nestedLoc, adaptedElementType,
                                                in0, in1)
                        .getResult();
          else if (isSignedIntOp)
            modOp = nestedBuilder
                        .create<arith::RemSIOp>(nestedLoc, adaptedElementType,
                                                in0, in1)
                        .getResult();
          else
            modOp = nestedBuilder
                        .create<arith::RemFOp>(nestedLoc, adaptedElementType,
                                               in0, in1)
                        .getResult();
          nestedBuilder.create<linalg::YieldOp>(nestedLoc, modOp);
        });
    rewriter.replaceOp(op, genericOp.getResult(0));
    return success();
  }
};

struct MultOpConverter : public OpConversionPattern<vosa::MultOp> {
  using OpConversionPattern<vosa::MultOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::MultOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    // replace with TOSA MulOp
    auto outputTy = getTypeConverter()->convertType(op.getType());
    auto tosaOp =
        rewriter
            .create<tosa::MulOp>(op.getLoc(), outputTy, adaptor.getInput0(),
                                 adaptor.getInput1(), 0)
            .getOutput();
    rewriter.replaceOp(op, tosaOp);
    return success();
  }
};

struct OrOpConverter : public OpConversionPattern<vosa::OrOp> {
  using OpConversionPattern<vosa::OrOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::OrOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputType = getTypeConverter()->convertType(op.getType());
    auto orOp = rewriter.create<tosa::BitwiseOrOp>(
        op.getLoc(), outputType, adaptor.getInput0(), adaptor.getInput1());
    rewriter.replaceOp(op, orOp.getOutput());
    return success();
  }
};

struct SubOpConverter : public OpConversionPattern<vosa::SubOp> {
  using OpConversionPattern<vosa::SubOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::SubOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    // replace with TOSA SubOp
    auto outputTy = getTypeConverter()->convertType(op.getType());
    auto tosaOp =
        rewriter
            .create<tosa::SubOp>(op.getLoc(), outputTy, adaptor.getInput0(),
                                 adaptor.getInput1())
            .getOutput();
    rewriter.replaceOp(op, tosaOp);
    return success();
  }
};

//===----------------------------------------------------------------------===//
// Elementwise unary operations.
//===----------------------------------------------------------------------===//

struct AbsOpConverter : public OpConversionPattern<vosa::AbsOp> {
  using OpConversionPattern<vosa::AbsOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::AbsOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    // replace with TOSA AbsOp
    auto outputTy = getTypeConverter()->convertType(op.getType());
    auto tosaOp =
        rewriter.create<tosa::AbsOp>(op.getLoc(), outputTy, adaptor.getInput())
            .getOutput();
    rewriter.replaceOp(op, tosaOp);
    return success();
  }
};

struct ArithmeticShiftRightOpConverter
    : public OpConversionPattern<vosa::ArithmeticShiftRightOp> {
  using OpConversionPattern<vosa::ArithmeticShiftRightOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::ArithmeticShiftRightOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    // replace with TOSA ArithmeticRightShiftOp
    auto inputTy = adaptor.getInput().getType().cast<ShapedType>();
    auto outputTy = getTypeConverter()->convertType(op.getType());

    // use TOSA implicit broadcast for shift tensor
    auto shiftVal = rewriter.create<arith::ConstantOp>(
        op.getLoc(),
        rewriter.getIntegerAttr(inputTy.getElementType(), op.getShift()));
    auto shiftTensor = makeFilledTensor(
        op.getLoc(), SmallVector<int64_t>(inputTy.getRank(), 1), shiftVal,
        rewriter);

    auto tosaOp =
        rewriter
            .create<tosa::ArithmeticRightShiftOp>(
                op.getLoc(), outputTy, adaptor.getInput(), shiftTensor, false)
            .getOutput();
    rewriter.replaceOp(op, tosaOp);
    return success();
  }
};

static Value makeClampElementOp(Operation *op, Value val, Value min, Value max,
                                PatternRewriter &rewriter) {
  Location loc = op->getLoc();
  auto elementTy =
      op->getOperand(0).getType().cast<ShapedType>().getElementType();

  if (elementTy.isa<FloatType>()) {
    return clampHelper<arith::CmpFOp>(loc, val, min, max,
                                      arith::CmpFPredicate::OLT, rewriter);
  }

  if (elementTy.isa<IntegerType>()) {
    auto pred = elementTy.isSignedInteger() ? arith::CmpIPredicate::slt
                                            : arith::CmpIPredicate::ult;
    return clampHelper<arith::CmpIOp>(loc, val, min, max, pred, rewriter);
  }

  (void)rewriter.notifyMatchFailure(op, "unhandled element type for clamp");
  return nullptr;
}

struct ClampOpConverter : public OpConversionPattern<vosa::ClampOp> {
  using OpConversionPattern<vosa::ClampOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::ClampOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    // use linalg generic - can't use TOSA clamp as it has fixed attributes for
    // min/max

    auto inputTy = adaptor.getInput0().getType().cast<ShapedType>();
    auto outputTy =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    auto initOutTensor =
        rewriter
            .create<tensor::EmptyOp>(op.getLoc(), outputTy.getShape(),
                                     outputTy.getElementType())
            .getResult();

    SmallVector<AffineMap, 2> indexingMaps;

    // input indexing map
    indexingMaps.push_back(rewriter.getMultiDimIdentityMap(inputTy.getRank()));

    // output indexing map
    indexingMaps.push_back(rewriter.getMultiDimIdentityMap(outputTy.getRank()));

    bool didEncounterError = false;
    auto linalgOp = rewriter.create<linalg::GenericOp>(
        op.getLoc(), outputTy, adaptor.getInput0(), initOutTensor, indexingMaps,
        SmallVector<utils::IteratorType>(inputTy.getRank(),
                                         utils::IteratorType::parallel),
        [&](OpBuilder &nestedBuilder, Location nestedLoc,
            ValueRange blockArgs) {
          Value opResult =
              makeClampElementOp(op, blockArgs.front(), adaptor.getInput1(),
                                 adaptor.getInput2(), rewriter);
          if (!opResult) {
            didEncounterError = true;
            return;
          }
          nestedBuilder.create<linalg::YieldOp>(op.getLoc(), opResult);
        });

    if (didEncounterError)
      return failure();

    rewriter.replaceOp(op, linalgOp->getResults());
    return success();
  }
};

struct GammaCorrectionOpConverter
    : public OpConversionPattern<vosa::GammaCorrectionOp> {
  using OpConversionPattern<vosa::GammaCorrectionOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::GammaCorrectionOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputType = getTypeConverter()->convertType(op.getType());
    auto inputType = adaptor.getInput().getType().template cast<ShapedType>();
    auto elementType = inputType.getElementType();

    auto gammaConst = rewriter.create<arith::ConstantOp>(
        op.getLoc(), elementType, cast<TypedAttr>(adaptor.getGamma()));

    auto gammaTensor = makeFilledTensor(op.getLoc(), inputType.getShape(),
                                        gammaConst, rewriter);
    auto powOp = rewriter.create<tosa::PowOp>(op.getLoc(), outputType,
                                              adaptor.getInput(), gammaTensor);
    rewriter.replaceOp(op, powOp.getZ());
    return success();
  }
};

struct LogicalShiftRightOpConverter
    : public OpConversionPattern<vosa::LogicalShiftRightOp> {
  using OpConversionPattern<vosa::LogicalShiftRightOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::LogicalShiftRightOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    // replace with TOSA LogicalRightShiftOp
    auto inputTy = adaptor.getInput().getType().cast<ShapedType>();
    auto outputTy = getTypeConverter()->convertType(op.getType());

    // use TOSA implicit broadcast for shift tensor
    auto shiftVal = rewriter.create<arith::ConstantOp>(
        op.getLoc(),
        rewriter.getIntegerAttr(inputTy.getElementType(), op.getShift()));
    auto shiftTensor = makeFilledTensor(
        op.getLoc(), SmallVector<int64_t>(inputTy.getRank(), 1), shiftVal,
        rewriter);

    auto tosaOp =
        rewriter
            .create<tosa::LogicalRightShiftOp>(op.getLoc(), outputTy,
                                               adaptor.getInput(), shiftTensor)
            .getOutput();
    rewriter.replaceOp(op, tosaOp);
    return success();
  }
};

struct NotOpConverter : public OpConversionPattern<vosa::NotOp> {
  using OpConversionPattern<vosa::NotOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::NotOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {

    auto outputType = getTypeConverter()->convertType(op.getType());
    auto notOp = rewriter.create<tosa::BitwiseNotOp>(op.getLoc(), outputType,
                                                     adaptor.getInput());
    rewriter.replaceOp(op, notOp.getOutput());
    return success();
  }
};

struct PowerOpConverter : public OpConversionPattern<vosa::PowerOp> {
  using OpConversionPattern<vosa::PowerOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::PowerOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputType = getTypeConverter()->convertType(op.getType());
    auto inputType = adaptor.getInput().getType().template cast<ShapedType>();
    auto elementType = inputType.getElementType();
    auto baseConst = rewriter.create<arith::ConstantOp>(
        op.getLoc(), elementType, cast<TypedAttr>(adaptor.getBase()));
    auto baseTensor = makeFilledTensor(op.getLoc(), inputType.getShape(),
                                       baseConst, rewriter);
    auto powOp = rewriter.create<tosa::PowOp>(op.getLoc(), outputType,
                                              baseTensor, adaptor.getInput());
    rewriter.replaceOp(op, powOp.getZ());
    return success();
  }
};

struct RoundOpConverter : public OpConversionPattern<vosa::RoundOp> {
  using OpConversionPattern<vosa::RoundOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::RoundOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto inputType = op.getInput().getType().cast<ShapedType>();
    auto outputType =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    auto fpType = inputType.cast<ShapedType>().getElementType();
    auto elType = outputType.cast<ShapedType>().getElementType();

    auto outputTensor =
        rewriter
            .create<tensor::EmptyOp>(op.getLoc(), outputType.getShape(), elType)
            .getResult();

    SmallVector<utils::IteratorType> iterators;
    iterators.push_back(utils::IteratorType::parallel);
    iterators.push_back(utils::IteratorType::parallel);
    iterators.push_back(utils::IteratorType::parallel);

    SmallVector<AffineMap> indexingMaps;
    indexingMaps.push_back(
        rewriter.getMultiDimIdentityMap(inputType.getRank()));
    indexingMaps.push_back(
        rewriter.getMultiDimIdentityMap(inputType.getRank()));

    bool didEncounterError = false;
    auto genericOp = rewriter.create<linalg::GenericOp>(
        op.getLoc(), outputType, adaptor.getInput(), outputTensor, indexingMaps,
        iterators, [&](OpBuilder &builder, Location loc, ValueRange blockArgs) {
          Value in = blockArgs.front();
          Value resFp;
          switch (op.getRoundMethod()) {
          case RoundMethod::floor:
            resFp = builder.create<math::FloorOp>(loc, fpType, in).getResult();
            break;
          case RoundMethod::ceil:
            resFp = builder.create<math::CeilOp>(loc, fpType, in).getResult();
            break;
          case RoundMethod::half_away_from_zero:
            resFp = builder.create<math::RoundOp>(loc, fpType, in).getResult();
            break;
          case RoundMethod::half_to_even:
            resFp =
                builder.create<math::RoundEvenOp>(loc, fpType, in).getResult();
            break;
          case RoundMethod::towards_zero:
          case RoundMethod::away_from_zero:
          case RoundMethod::half_up:
          case RoundMethod::half_down:
          case RoundMethod::half_towards_zero:
          case RoundMethod::half_to_odd:
            (void)rewriter.notifyMatchFailure(op, "Unsupported rounding mode");
            didEncounterError = true;
            return;
          }
          Value res =
              builder.create<arith::FPToSIOp>(loc, builder.getI32Type(), resFp);
          builder.create<linalg::YieldOp>(loc, res);
        });
    if (didEncounterError)
      return failure();
    rewriter.replaceOp(op, genericOp.getResult(0));
    return success();
  }
};

struct SqrtOpConverter : public OpConversionPattern<vosa::SqrtOp> {
  using OpConversionPattern<vosa::SqrtOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::SqrtOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {

    auto outputType =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    auto adaptedElementType = outputType.cast<ShapedType>().getElementType();

    auto inputType = op.getInput().getType().cast<ShapedType>();

    auto outputTensor =
        rewriter
            .create<tensor::EmptyOp>(op.getLoc(), outputType.getShape(),
                                     adaptedElementType)
            .getResult();

    SmallVector<utils::IteratorType> iterators;
    iterators.push_back(utils::IteratorType::parallel);
    iterators.push_back(utils::IteratorType::parallel);
    iterators.push_back(utils::IteratorType::parallel);

    SmallVector<AffineMap> indexingMaps;
    indexingMaps.push_back(
        rewriter.getMultiDimIdentityMap(inputType.getRank()));
    indexingMaps.push_back(
        rewriter.getMultiDimIdentityMap(inputType.getRank()));

    auto genericOp = rewriter.create<linalg::GenericOp>(
        op.getLoc(), outputType, adaptor.getInput(), outputTensor, indexingMaps,
        iterators,
        [&](OpBuilder &nestedBuilder, Location nestedLoc,
            ValueRange blockArgs) {
          Value in0 = blockArgs.front();
          Value sqrtOp =
              nestedBuilder
                  .create<math::SqrtOp>(nestedLoc, adaptedElementType, in0)
                  .getResult();
          nestedBuilder.create<linalg::YieldOp>(nestedLoc, sqrtOp);
        });
    rewriter.replaceOp(op, genericOp.getResult(0));
    return success();
  }
};

//===----------------------------------------------------------------------===//
// Image Filtering operations.
//===----------------------------------------------------------------------===//

struct Conv2DOpConverter : public OpConversionPattern<vosa::Conv2DOp> {
  using OpConversionPattern<vosa::Conv2DOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::Conv2DOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {

    // replace with Linalg DepthwiseConv2DNhwcHwcOp
    auto inputTy = adaptor.getInput().getType().cast<ShapedType>();
    int64_t numCh = getChannels(inputTy);
    auto filterVal = adaptor.getFilter();
    auto filterTy =
        getTypeConverter()->convertType(filterVal.getType()).cast<ShapedType>();
    auto outputTy =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    auto outputElTy = outputTy.getElementType();

    // input in NHWC/NCHW
    //  N is batch number - we only do one at a time, so it's one
    //  can reshape to 1xHxWxC or 1xCxHxW
    SmallVector<int64_t> nShape(inputTy.getShape());
    nShape.insert(nShape.begin(), 1);
    auto nInTy = RankedTensorType::get(nShape, inputTy.getElementType());
    auto inputN = rewriter.create<tosa::ReshapeOp>(
        op.getLoc(), nInTy, adaptor.getInput(),
        rewriter.getDenseI64ArrayAttr(nInTy.getShape()));

    if (auto intTy = filterTy.getElementType().dyn_cast<IntegerType>()) {
      // convert to signless value(s)
      auto elementsAttr = filterVal.cast<DenseIntElementsAttr>();
      filterVal = elementsAttr.mapValues(
          intTy, [&](const APInt &value) { return value; });
    }

    // Duplicate filter across all channels
    auto filterVals = filterVal.getValues<Attribute>();
    std::vector<Attribute> weightVal;
    SmallVector<int64_t> filterShape;
    if (getDataFormat() == DataFormat::CHW) {
      for (int c = 0; c < numCh; ++c) {
        for (int h = 0; h < filterTy.getDimSize(0); ++h) {
          for (int w = 0; w < filterTy.getDimSize(1); ++w) {
            weightVal.push_back(filterVals[h * filterTy.getDimSize(1) + w]);
          }
        }
      }
      filterShape = {numCh, filterTy.getDimSize(0), filterTy.getDimSize(1)};
    } else {
      for (int h = 0; h < filterTy.getDimSize(0); ++h) {
        for (int w = 0; w < filterTy.getDimSize(1); ++w) {
          for (int c = 0; c < numCh; ++c) {
            weightVal.push_back(filterVals[h * filterTy.getDimSize(1) + w]);
          }
        }
      }
      filterShape = {filterTy.getDimSize(0), filterTy.getDimSize(1), numCh};
    }
    auto weightTy =
        RankedTensorType::get(filterShape, filterTy.getElementType());
    auto weight = rewriter.create<arith::ConstantOp>(
        op.getLoc(), weightTy, DenseElementsAttr::get(weightTy, weightVal));

    // do the convolution

    // create zero filled output tensor 1xCxHxW
    TypedAttr zeroInit;
    if (outputElTy.isa<IntegerType>()) {
      zeroInit = rewriter.getIntegerAttr(
          outputElTy, APInt(outputElTy.getIntOrFloatBitWidth(), 0));
    } else {
      zeroInit = rewriter.getFloatAttr(
          outputElTy,
          APFloat::getZero(outputElTy.cast<FloatType>().getFloatSemantics(),
                           false));
    }
    SmallVector<int64_t> nOutShape(outputTy.getShape());
    nOutShape.insert(nOutShape.begin(), 1);
    auto nOutTy = RankedTensorType::get(nOutShape, outputElTy);
    auto output = makeFilledTensor(
        op.getLoc(), nOutTy.getShape(),
        rewriter.create<arith::ConstantOp>(op.getLoc(), zeroInit), rewriter);

    SmallVector<int64_t> stride{1, 1};
    SmallVector<int64_t> dilation{1, 1};
    mlir::Value convOut;
    if (getDataFormat() == DataFormat::CHW) {
      convOut = rewriter
                    .create<linalg::DepthwiseConv2DNchwChwOp>(
                        op.getLoc(), nOutTy, ValueRange{inputN, weight},
                        ValueRange{output},
                        DenseIntElementsAttr::get(
                            RankedTensorType::get({2}, rewriter.getI64Type()),
                            stride),
                        DenseIntElementsAttr::get(
                            RankedTensorType::get({2}, rewriter.getI64Type()),
                            dilation))
                    .getResult(0);
    } else {
      convOut = rewriter
                    .create<linalg::DepthwiseConv2DNhwcHwcOp>(
                        op.getLoc(), nOutTy, ValueRange{inputN, weight},
                        ValueRange{output},
                        DenseIntElementsAttr::get(
                            RankedTensorType::get({2}, rewriter.getI64Type()),
                            stride),
                        DenseIntElementsAttr::get(
                            RankedTensorType::get({2}, rewriter.getI64Type()),
                            dilation))
                    .getResult(0);
    }

    // reshape / transpose to expected output type
    convOut = rewriter.create<tosa::ReshapeOp>(
        op.getLoc(), outputTy, convOut,
        rewriter.getDenseI64ArrayAttr(outputTy.getShape()));

    rewriter.replaceOp(op, convOut);

    return success();
  }
};

//===----------------------------------------------------------------------===//
// Image Transformation operations.
//===----------------------------------------------------------------------===//

struct DecimateOpConverter : public OpConversionPattern<vosa::DecimateOp> {
  using OpConversionPattern<vosa::DecimateOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::DecimateOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputTy =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();

    SmallVector<OpFoldResult> strides;
    for (Attribute val : op.getN().getValue()) {
      int64_t s = val.cast<IntegerAttr>().getValue().getSExtValue();
      strides.push_back(rewriter.getIndexAttr(s));
    }
    strides.insert(strides.begin() + getChannelDimension(),
                   rewriter.getIndexAttr(1));

    SmallVector<OpFoldResult> offsets;
    for (Attribute val : op.getOffsets().getValue()) {
      int64_t o = val.cast<IntegerAttr>().getValue().getSExtValue();
      offsets.push_back(rewriter.getIndexAttr(o));
    }
    offsets.insert(offsets.begin() + getChannelDimension(),
                   rewriter.getIndexAttr(0));

    SmallVector<OpFoldResult> dims;
    dims.push_back(rewriter.getIndexAttr(outputTy.getDimSize(0)));
    dims.push_back(rewriter.getIndexAttr(outputTy.getDimSize(1)));
    dims.push_back(rewriter.getIndexAttr(outputTy.getDimSize(2)));

    auto extractOp = rewriter.create<tensor::ExtractSliceOp>(
        op.getLoc(), adaptor.getInput(), offsets, dims, strides);

    rewriter.replaceOp(op, extractOp->getResult(0));
    return success();
  }
};

struct ExpandOpConverter : public OpConversionPattern<vosa::ExpandOp> {
  using OpConversionPattern<vosa::ExpandOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::ExpandOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto loc = op.getLoc();
    auto outputType =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    auto elementType = outputType.cast<ShapedType>().getElementType();
    auto expandKernelType =
        adaptor.getExpandKernel().getType().cast<ShapedType>();
    auto expandKernelH = getHeight(expandKernelType);
    auto expandKernelW = getWidth(expandKernelType);
    auto gatherKernelShape =
        adaptor.getGatherKernel().getType().cast<ShapedType>().getShape();
    auto gatherKernelH = gatherKernelShape[0];
    auto gatherKernelW = gatherKernelShape[1];
    auto gatherKernelLength = gatherKernelH * gatherKernelW;
    auto gatherValues = adaptor.getGatherKernel().getValues<APInt>();

    // Constants
    auto zero = rewriter
                    .create<arith::ConstantOp>(loc, rewriter.getIndexType(),
                                               rewriter.getIndexAttr(0))
                    .getResult();
    auto one = rewriter
                   .create<arith::ConstantOp>(loc, rewriter.getIndexType(),
                                              rewriter.getIndexAttr(1))
                   .getResult();
    auto minusOne = rewriter
                        .create<arith::ConstantOp>(loc, rewriter.getIndexType(),
                                                   rewriter.getIndexAttr(-1))
                        .getResult();

    // Attribute constants
    auto cstGatherKernelW =
        rewriter
            .create<arith::ConstantOp>(loc, rewriter.getIndexType(),
                                       rewriter.getIndexAttr(gatherKernelW))
            .getResult();
    auto cstGatherKernelH =
        rewriter
            .create<arith::ConstantOp>(loc, rewriter.getIndexType(),
                                       rewriter.getIndexAttr(gatherKernelH))
            .getResult();
    auto cstExpandKernelW =
        rewriter
            .create<arith::ConstantOp>(loc, rewriter.getIndexType(),
                                       rewriter.getIndexAttr(expandKernelW))
            .getResult();
    auto cstExpandKernelH =
        rewriter
            .create<arith::ConstantOp>(loc, rewriter.getIndexType(),
                                       rewriter.getIndexAttr(expandKernelH))
            .getResult();

    auto fillValue =
        rewriter
            .create<vosa::ConstantOp>(loc, elementType,
                                      cast<TypedAttr>(adaptor.getFillValue()))
            .getResult();

    // CREATE KERNEL MAP
    // maps IDs from expand kernel to indexes in gather kernel
    // kernel_map[gather_kernel[counter]] = [y, x]
    // store as i32 as IREE doesn't handle tensors of indexes
    std::vector<int32_t> kernelMapVals(gatherKernelLength * 2 + 2, -1);
    for (int32_t i = 0; i < gatherKernelLength; ++i) {
      int32_t j = gatherValues[i].getSExtValue();
      kernelMapVals[j * 2] = i / gatherKernelW;
      kernelMapVals[j * 2 + 1] = i % gatherKernelW;
    }
    // entry 0 contains a marker to indicate use the fill value
    kernelMapVals[0] = -1;
    kernelMapVals[1] = -1;
    auto kernelMapTy = RankedTensorType::get({gatherKernelLength + 1, 2},
                                             rewriter.getI32Type());
    auto kernelMap = rewriter.create<arith::ConstantOp>(
        loc, kernelMapTy,
        mlir::DenseIntElementsAttr::get(kernelMapTy, kernelMapVals));

    // DO EXPAND
    // linalg.generic on expand kernel -> output
    //   tensor.extract is used to get values from input
    auto initOutputTensor =
        rewriter
            .create<tensor::EmptyOp>(loc, outputType.getShape(),
                                     outputType.getElementType())
            .getResult();

    // expand kernel is modulo size
    SmallVector<AffineExpr, 3> expKernelExprs;
    expKernelExprs.push_back(
        mlir::getAffineDimExpr(getHeightDimension(), rewriter.getContext()) %
        getHeight(expandKernelType));
    expKernelExprs.push_back(
        mlir::getAffineDimExpr(getWidthDimension(), rewriter.getContext()) %
        getWidth(expandKernelType));
    expKernelExprs.insert(
        expKernelExprs.begin() + getChannelDimension(),
        mlir::getAffineConstantExpr(0, rewriter.getContext()));

    SmallVector<AffineMap, 3> maps = {
        AffineMap::get(3, 0, expKernelExprs, rewriter.getContext()),
        rewriter.getMultiDimIdentityMap(3),
    };

    SmallVector<utils::IteratorType> iterators;
    iterators.push_back(utils::IteratorType::parallel);
    iterators.push_back(utils::IteratorType::parallel);
    iterators.push_back(utils::IteratorType::parallel);

    auto linalgOp = rewriter.create<linalg::GenericOp>(
        loc, outputType, adaptor.getExpandKernel(), initOutputTensor, maps,
        iterators,
        [&](OpBuilder &builder, Location nestedLoc, ValueRange args) {
          Value yPos =
              builder.create<linalg::IndexOp>(nestedLoc, getHeightDimension());
          Value xPos =
              builder.create<linalg::IndexOp>(nestedLoc, getWidthDimension());
          Value c =
              builder.create<linalg::IndexOp>(nestedLoc, getChannelDimension());

          // use current value in expand kernel as index to look up input
          // co-ords
          Value expKernelVal = args[0];
          Value expandIndex = builder.create<arith::IndexCastOp>(
              nestedLoc, builder.getIndexType(), expKernelVal);
          Value kernelMapValY = builder.create<tensor::ExtractOp>(
              nestedLoc, kernelMap, ValueRange{expandIndex, zero});
          Value kernelMapIndexY = builder.create<arith::IndexCastOp>(
              nestedLoc, builder.getIndexType(), kernelMapValY);
          Value kernelMapValX = builder.create<tensor::ExtractOp>(
              nestedLoc, kernelMap, ValueRange{expandIndex, one});
          Value kernelMapIndexX = builder.create<arith::IndexCastOp>(
              nestedLoc, builder.getIndexType(), kernelMapValX);

          // -1 indicates no mapping in either gather or expand kernel, so
          // yield fill value
          auto fillValueCmp = builder.create<arith::CmpIOp>(
              nestedLoc, arith::CmpIPredicate::eq, kernelMapIndexY, minusOne);
          auto outputIf = builder.create<scf::IfOp>(
              nestedLoc, fillValueCmp,
              [&](OpBuilder &insertIfBuilder, Location insertIfLoc) {
                insertIfBuilder.create<scf::YieldOp>(insertIfLoc, fillValue);
              },
              [&](OpBuilder &insertIfBuilder, Location insertIfLoc) {
                // calculate the co-ordinate in the input image
                // yTile = outY / expandW
                // xTile = outX / expandX
                // inY = yTile * gatherW + kernelMapY
                // inX = xTile * gatherH + kernelMapX
                Value yTile = insertIfBuilder
                                  .create<arith::DivUIOp>(insertIfLoc, yPos,
                                                          cstExpandKernelH)
                                  .getResult();
                Value xTile = insertIfBuilder
                                  .create<arith::DivUIOp>(insertIfLoc, xPos,
                                                          cstExpandKernelW)
                                  .getResult();
                Value inY = insertIfBuilder.create<arith::AddIOp>(
                    insertIfLoc,
                    insertIfBuilder.create<arith::MulIOp>(insertIfLoc, yTile,
                                                          cstGatherKernelH),
                    kernelMapIndexY);
                Value inX = insertIfBuilder.create<arith::AddIOp>(
                    insertIfLoc,
                    insertIfBuilder.create<arith::MulIOp>(insertIfLoc, xTile,
                                                          cstGatherKernelW),
                    kernelMapIndexX);
                // yield value extracted from input[inY][inX]
                SmallVector<Value> offsets{inY, inX};
                offsets.insert(offsets.begin() + getChannelDimension(), c);
                Value v = insertIfBuilder.create<tensor::ExtractOp>(
                    insertIfLoc, adaptor.getInput(), offsets);
                insertIfBuilder.create<scf::YieldOp>(insertIfLoc, v);
              });

          builder.create<linalg::YieldOp>(nestedLoc, outputIf.getResults());
        });

    rewriter.replaceOp(op, linalgOp.getResult(0));
    return success();
  }
};

Value concatenateHelper(OpBuilder &builder, Location loc,
                        llvm::ArrayRef<Value> values, int axis) {
  assert(values.size() >= 1 && "Need at least one value to concetenate");

  if (values.size() == 1) {
    // only one value - no need to concatenate op
    return values[0];
  } else {
    auto v0Ty = values[0].getType().cast<ShapedType>();
    llvm::SmallVector<int64_t, 3> outDims(v0Ty.getShape().begin(),
                                          v0Ty.getShape().end());
    for (auto val : values.drop_front(1)) {
      outDims[axis] += val.getType().cast<ShapedType>().getShape()[axis];
    }
    auto outTy = RankedTensorType::get(outDims, v0Ty.getElementType());
    return builder
        .create<tosa::ConcatOp>(loc, outTy, values,
                                builder.getI32IntegerAttr(axis))
        .getOutput();
  }
}

Value generatePadConstant(OpBuilder &builder, Location loc,
                          llvm::ArrayRef<int64_t> shape, Value padValue) {
  return makeFilledTensor(loc, hwcToDataFormatShape(shape), padValue, builder);
}

Value generatePadExtract(OpBuilder &builder, Location loc,
                         llvm::ArrayRef<int64_t> shape, Value input,
                         function_ref<Value(Value, Value, Value)> heightIdxFn,
                         function_ref<Value(Value, Value, Value)> widthIdxFn) {
  // use linalg.generic to generate padding tensor
  auto inputTy = input.getType().cast<ShapedType>();
  auto padTy = RankedTensorType::get(hwcToDataFormatShape(shape),
                                     inputTy.getElementType());
  auto initOutTensor = builder
                           .create<tensor::EmptyOp>(loc, padTy.getShape(),
                                                    padTy.getElementType())
                           .getResult();
  auto padOp = builder.create<linalg::GenericOp>(
      loc, padTy, ValueRange{}, initOutTensor,
      SmallVector<AffineMap, 1>{builder.getMultiDimIdentityMap(3)},
      SmallVector<utils::IteratorType>(3, utils::IteratorType::parallel),
      [&](OpBuilder &nestedBuilder, Location nestedLoc, ValueRange blockArgs) {
        Value idxHeight =
            builder.create<linalg::IndexOp>(nestedLoc, getHeightDimension())
                .getResult();
        Value idxWidth =
            builder.create<linalg::IndexOp>(nestedLoc, getWidthDimension())
                .getResult();
        Value padHeight =
            builder
                .create<arith::ConstantOp>(loc, builder.getIndexType(),
                                           builder.getIndexAttr(shape[0]))
                .getResult();
        Value padWidth =
            builder
                .create<arith::ConstantOp>(loc, builder.getIndexType(),
                                           builder.getIndexAttr(shape[1]))
                .getResult();
        Value maxHeight = builder
                              .create<arith::ConstantOp>(
                                  loc, builder.getIndexType(),
                                  builder.getIndexAttr(getHeight(inputTy) - 1))
                              .getResult();
        Value maxWidth = builder
                             .create<arith::ConstantOp>(
                                 loc, builder.getIndexType(),
                                 builder.getIndexAttr(getWidth(inputTy) - 1))
                             .getResult();

        Value heightIdx = heightIdxFn(idxHeight, padHeight, maxHeight);
        Value widthIdx = widthIdxFn(idxWidth, padWidth, maxWidth);

        SmallVector<Value, 3> extractIndices{heightIdx, widthIdx};
        extractIndices.insert(
            extractIndices.begin() + getChannelDimension(),
            builder.create<linalg::IndexOp>(nestedLoc, getChannelDimension()));
        auto val =
            nestedBuilder
                .create<tensor::ExtractOp>(nestedLoc, input, extractIndices)
                .getResult();
        nestedBuilder.create<linalg::YieldOp>(nestedLoc, val);
      });
  return padOp.getResult(0);
}

struct PadOpConverter : public OpConversionPattern<vosa::PadOp> {
  using OpConversionPattern<vosa::PadOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::PadOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto loc = op.getLoc();
    auto inputTy = adaptor.getInput().getType().cast<ShapedType>();
    auto pad_mode = op.getMode();
    auto height = getHeight(inputTy);
    auto width = getWidth(inputTy);
    auto channels = getChannels(inputTy);

    // convert optional padding constant attr to constant value
    Value padValue;
    if (op.getPadConstant()) {
      Type constTy;
      auto v = cast<TypedAttr>(*op.getPadConstant());
      if (auto intAttr = v.dyn_cast<IntegerAttr>()) {
        constTy = getTypeConverter()->convertType(intAttr.getType());
        v = rewriter.getIntegerAttr(constTy, intAttr.getValue());
      } else if (auto floatAttr = v.dyn_cast<FloatAttr>())
        constTy = getTypeConverter()->convertType(floatAttr.getType());
      else
        op.emitError("Unsupported pad constant type");
      padValue =
          rewriter.create<arith::ConstantOp>(loc, constTy, v).getResult();
    };

    // verifier has ensured this is 4 integer elements:
    auto getPadElem = [&](int i) {
      return op.getPadSize()
          .getValue()[i]
          .cast<IntegerAttr>()
          .getValue()
          .getSExtValue();
    };
    int64_t y_before = getPadElem(0);
    int64_t y_after = getPadElem(1);
    int64_t x_before = getPadElem(2);
    int64_t x_after = getPadElem(3);

    if (pad_mode != PadMode::constant && pad_mode != PadMode::replicate &&
        pad_mode != PadMode::reflect) {
      return op.emitError("unhandled padding mode");
    }

    // early return if nothing to pad
    if (y_before == 0 && y_after == 0 && x_before == 0 && x_after == 0) {
      rewriter.replaceOp(op, adaptor.getInput());
      return success();
    }

    // use tensor.generate to generate required padding blocks and then
    // tosa.concat to assemble into output (concat columns into rows, then
    // concat rows to output)

    auto padIdxCurr = [&](Value idx, Value padSz, Value inputMax) {
      return idx;
    };
    auto padReplLower = [&](Value idx, Value padSz, Value inputMax) {
      return rewriter
          .create<arith::ConstantOp>(loc, rewriter.getIndexType(),
                                     rewriter.getIndexAttr(0))
          .getResult();
    };
    auto padReplUpper = [&](Value idx, Value padSz, Value inputMax) {
      return inputMax;
    };
    auto padReflectLower = [&](Value idx, Value padSz, Value inputMax) {
      return rewriter.create<arith::SubIOp>(loc, padSz, idx).getResult();
    };
    auto padReflectUpper = [&](Value idx, Value padSz, Value inputMax) {
      auto cstOne = rewriter.create<arith::ConstantOp>(
          loc, rewriter.getIndexType(), rewriter.getIndexAttr(1));
      auto maxMinusOne = rewriter.create<arith::SubIOp>(loc, inputMax, cstOne);
      return rewriter.create<arith::SubIOp>(loc, maxMinusOne, idx).getResult();
    };

    function_ref<Value(Value, Value, Value)> lowerFn = padReplLower;
    function_ref<Value(Value, Value, Value)> upperFn = padReplUpper;
    if (pad_mode == PadMode::reflect) {
      lowerFn = padReflectLower;
      upperFn = padReflectUpper;
    }

    // generate rows
    SmallVector<Value, 3> rows;
    //   padding above
    if (y_before > 0) {
      if (pad_mode == PadMode::constant) {
        // optimize for constant pad - can generate all above in one value
        rows.push_back(generatePadConstant(
            rewriter, loc, {y_before, x_before + width + x_after, channels},
            padValue));
      } else {
        SmallVector<Value, 3> cols;
        if (x_before > 0) {
          cols.push_back(
              generatePadExtract(rewriter, loc, {y_before, x_before, channels},
                                 adaptor.getInput(), lowerFn, lowerFn));
        }
        cols.push_back(
            generatePadExtract(rewriter, loc, {y_before, width, channels},
                               adaptor.getInput(), lowerFn, padIdxCurr));
        if (y_before > 0 && x_after > 0) {
          cols.push_back(
              generatePadExtract(rewriter, loc, {y_before, x_after, channels},
                                 adaptor.getInput(), lowerFn, upperFn));
        }
        rows.push_back(
            concatenateHelper(rewriter, loc, cols, getWidthDimension()));
      }
    }

    //   padding left and right
    {
      SmallVector<Value, 3> cols;
      if (x_before > 0) {
        if (pad_mode == PadMode::constant) {
          cols.push_back(generatePadConstant(
              rewriter, loc, {height, x_before, channels}, padValue));
        } else {
          cols.push_back(
              generatePadExtract(rewriter, loc, {height, x_before, channels},
                                 adaptor.getInput(), padIdxCurr, lowerFn));
        }
      }
      cols.push_back(adaptor.getInput());
      if (x_after > 0) {
        if (pad_mode == PadMode::constant) {
          cols.push_back(generatePadConstant(
              rewriter, loc, {height, x_after, channels}, padValue));
        } else {
          cols.push_back(
              generatePadExtract(rewriter, loc, {height, x_after, channels},
                                 adaptor.getInput(), padIdxCurr, upperFn));
        }
      }
      rows.push_back(
          concatenateHelper(rewriter, loc, cols, getWidthDimension()));
    }

    //   padding below
    if (y_after > 0) {
      if (pad_mode == PadMode::constant) {
        // optimize for constant pad - can generate in one go
        rows.push_back(generatePadConstant(
            rewriter, loc, {y_after, x_before + width + x_after, channels},
            padValue));
      } else {
        SmallVector<Value, 3> cols;
        if (x_before > 0) {
          cols.push_back(
              generatePadExtract(rewriter, loc, {y_after, x_before, channels},
                                 adaptor.getInput(), upperFn, lowerFn));
        }
        cols.push_back(
            generatePadExtract(rewriter, loc, {y_after, width, channels},
                               adaptor.getInput(), upperFn, padIdxCurr));
        if (x_after > 0) {
          cols.push_back(
              generatePadExtract(rewriter, loc, {y_after, x_after, channels},
                                 adaptor.getInput(), upperFn, upperFn));
        }
        rows.push_back(
            concatenateHelper(rewriter, loc, cols, getWidthDimension()));
      }
    }

    // concatenate rows
    auto padded = concatenateHelper(rewriter, loc, rows, getHeightDimension());
    rewriter.replaceOp(op, padded);
    return success();
  }
};

struct PointwiseMatrixMultiplyOpConverter
    : public OpConversionPattern<vosa::PointwiseMatrixMultiplyOp> {
  using OpConversionPattern<
      vosa::PointwiseMatrixMultiplyOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::PointwiseMatrixMultiplyOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    // Declare types used in lowering
    auto inputType = adaptor.getInput().getType().cast<ShapedType>();
    auto outputType =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();

    int HxW = getHeight(inputType) * getWidth(inputType);
    int inCh = getChannels(inputType);
    int outCh = getChannels(outputType);
    auto elementType = inputType.getElementType();
    auto MAttrType = RankedTensorType::get({outCh, inCh}, elementType);
    auto K1AttrType = RankedTensorType::get({inCh}, elementType);
    auto K2AttrType = RankedTensorType::get({outCh}, elementType);
    auto MType = RankedTensorType::get({1, outCh, inCh}, elementType);
    auto K1Type = RankedTensorType::get({inCh, 1}, elementType);
    auto K2Type = RankedTensorType::get({outCh, 1}, elementType);
    auto cnInType = RankedTensorType::get({1, inCh, HxW}, elementType);
    auto cnInReshapeType = RankedTensorType::get({1, inCh, 1}, elementType);
    auto ncInType = RankedTensorType::get({1, HxW, inCh}, elementType);
    auto cnOutType = RankedTensorType::get({1, outCh, HxW}, elementType);
    auto cnOutReshapeType = RankedTensorType::get({1, outCh, 1}, elementType);
    auto ncOutType = RankedTensorType::get({1, HxW, outCh}, elementType);

    // transpose { 1, HxW, C } <==> { 1, C, HxW }
    auto permType = RankedTensorType::get({3}, rewriter.getI64Type());
    auto transposeNC = rewriter.create<arith::ConstantOp>(
        op.getLoc(), permType,
        DenseIntElementsAttr::get(permType, SmallVector<int64_t>{0, 2, 1}));

    // Op does output = M * (input - K1) + K2
    // Lowering takes input tensor (h,w,c):
    //   reshapes input to (c, h*w)
    //   tiles K1 and K2 to (c, h*w)
    //   input - K1
    //   matmul M * (input - K1)
    //   M * (input - K1) + K2
    //   reshapes result back to (h,w,c)

    // Declare and reshape constants
    auto Ma = rewriter.create<vosa::ConstantOp>(op.getLoc(), MAttrType,
                                                adaptor.getM());
    auto M = rewriter.create<tosa::ReshapeOp>(
        op.getLoc(), MType, Ma,
        rewriter.getDenseI64ArrayAttr(MType.getShape()));
    auto K1a = rewriter.create<vosa::ConstantOp>(op.getLoc(), K1AttrType,
                                                 adaptor.getK_1());
    auto K1 = rewriter.create<tosa::ReshapeOp>(
        op.getLoc(), K1Type, K1a,
        rewriter.getDenseI64ArrayAttr(K1Type.getShape()));
    auto K2a = rewriter.create<vosa::ConstantOp>(op.getLoc(), K2AttrType,
                                                 adaptor.getK_2());
    auto K2 = rewriter.create<tosa::ReshapeOp>(
        op.getLoc(), K2Type, K2a,
        rewriter.getDenseI64ArrayAttr(K2Type.getShape()));

    // Get tensors in correct shape for M*(input - K1) + K2

    auto K1Reshape = rewriter.create<tosa::ReshapeOp>(
        op.getLoc(), cnInReshapeType, K1,
        rewriter.getDenseI64ArrayAttr(cnInReshapeType.getShape()));

    auto K2Reshape = rewriter.create<tosa::ReshapeOp>(
        op.getLoc(), cnOutReshapeType, K2,
        rewriter.getDenseI64ArrayAttr(cnOutReshapeType.getShape()));

    auto K1Matrix = rewriter.create<tosa::TileOp>(
        op.getLoc(), cnInType, K1Reshape,
        rewriter.getDenseI64ArrayAttr({1, 1, HxW}));
    auto K2Matrix = rewriter.create<tosa::TileOp>(
        op.getLoc(), cnOutType, K2Reshape,
        rewriter.getDenseI64ArrayAttr({1, 1, HxW}));

    Value a;
    if (getDataFormat() == DataFormat::HWC) {
      auto reshape = rewriter.create<tosa::ReshapeOp>(
          op.getLoc(), ncInType, adaptor.getInput(),
          rewriter.getDenseI64ArrayAttr(ncInType.getShape()));
      a = rewriter.create<tosa::TransposeOp>(op.getLoc(), cnInType, reshape,
                                             transposeNC);
    } else {
      a = rewriter.create<tosa::ReshapeOp>(
          op.getLoc(), cnInType, adaptor.getInput(),
          rewriter.getDenseI64ArrayAttr(cnInType.getShape()));
    }

    // Do M*(input - K1) + K2
    auto innerSub =
        rewriter.create<tosa::SubOp>(op.getLoc(), cnInType, a, K1Matrix);
    auto multiply =
        rewriter.create<tosa::MatMulOp>(op.getLoc(), cnOutType, M, innerSub);
    Value res =
        rewriter.create<tosa::AddOp>(op.getLoc(), cnOutType, multiply, K2Matrix)
            .getResult();

    // Reshape result back to the input tensor type
    if (getDataFormat() == DataFormat::HWC) {
      res = rewriter.create<tosa::TransposeOp>(op.getLoc(), ncOutType, res,
                                               transposeNC);
    }
    res = rewriter
              .create<tosa::ReshapeOp>(
                  op.getLoc(), outputType, res,
                  rewriter.getDenseI64ArrayAttr(outputType.getShape()))
              .getOutput();

    rewriter.replaceOp(op, res);

    return success();
  }
};

struct ResizeBilinearOpConverter
    : public OpConversionPattern<vosa::ResizeBilinearOp> {
  using OpConversionPattern<vosa::ResizeBilinearOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::ResizeBilinearOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    // replace with TOSA ResizeOp - this only operates on HWC format data. so
    // transpose if in CHW mode

    auto inputTy = adaptor.getInput().getType().cast<ShapedType>();
    auto outputTy =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    Value input = adaptor.getInput();

    auto chw_to_hwcTy = RankedTensorType::get({3}, rewriter.getI64Type());
    auto chw_to_hwc = rewriter.create<arith::ConstantOp>(
        op.getLoc(), chw_to_hwcTy,
        DenseIntElementsAttr::get(chw_to_hwcTy, SmallVector<int64_t>{1, 2, 0}));
    auto hwcInTy = RankedTensorType::get(
        {getHeight(inputTy), getWidth(inputTy), getChannels(inputTy)},
        inputTy.getElementType());
    auto nhwcOutTy = RankedTensorType::get(
        {1, getHeight(outputTy), getWidth(outputTy), getChannels(outputTy)},
        outputTy.getElementType());
    auto hwcOutTy = RankedTensorType::get(
        {getHeight(outputTy), getWidth(outputTy), getChannels(outputTy)},
        outputTy.getElementType());

    if (getDataFormat() == DataFormat::CHW) {
      // transpose input to HWC format
      input = rewriter
                  .create<tosa::TransposeOp>(op.getLoc(), hwcInTy, input,
                                             chw_to_hwc)
                  .getOutput();
    }

    // reshape to add N dimension
    SmallVector<int64_t> nInShape(hwcInTy.getShape());
    nInShape.insert(nInShape.begin(), 1);
    auto nInTy = RankedTensorType::get(nInShape, inputTy.getElementType());
    auto inputN = rewriter.create<tosa::ReshapeOp>(
        op.getLoc(), nInTy, input,
        rewriter.getDenseI64ArrayAttr(nInTy.getShape()));

    // scale is num, denom for H & W
    SmallVector<int64_t> scale{getHeight(outputTy), getHeight(inputTy),
                               getWidth(outputTy), getWidth(inputTy)};
    // these are equivalent to half pixel sampling
    SmallVector<int64_t> offset{(getHeight(inputTy) - getHeight(outputTy)) / 2,
                                (getWidth(inputTy) - getWidth(outputTy)) / 2};
    SmallVector<int64_t> border{0, 0};

    mlir::Value resized =
        rewriter
            .create<tosa::ResizeOp>(op.getLoc(), nhwcOutTy, inputN,
                                    rewriter.getDenseI64ArrayAttr(scale),
                                    rewriter.getDenseI64ArrayAttr(offset),
                                    rewriter.getDenseI64ArrayAttr(border),
                                    rewriter.getStringAttr("BILINEAR"))
            .getOutput();

    // reshape to expected output type
    Value output = rewriter
                       .create<tosa::ReshapeOp>(
                           op.getLoc(), hwcOutTy, resized,
                           rewriter.getDenseI64ArrayAttr(hwcOutTy.getShape()))
                       .getOutput();

    if (getDataFormat() == DataFormat::CHW) {
      // transpose back to CHW format
      auto hwc_to_chw = rewriter.create<arith::ConstantOp>(
          op.getLoc(), chw_to_hwcTy,
          DenseIntElementsAttr::get(chw_to_hwcTy,
                                    SmallVector<int64_t>{2, 0, 1}));
      output = rewriter
                   .create<tosa::TransposeOp>(op.getLoc(), outputTy, output,
                                              hwc_to_chw)
                   .getOutput();
    }

    rewriter.replaceOp(op, output);

    return success();
  }
};

struct ResizeNearestNeighbourOpConverter
    : public OpConversionPattern<vosa::ResizeNearestNeighbourOp> {
  using OpConversionPattern<
      vosa::ResizeNearestNeighbourOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::ResizeNearestNeighbourOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    // replace with TOSA ResizeOp - this only operates on HWC format data. so
    // transpose if in CHW mode
    auto inputTy = adaptor.getInput().getType().cast<ShapedType>();
    auto outputTy =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    Value input = adaptor.getInput();

    auto chw_to_hwcTy = RankedTensorType::get({3}, rewriter.getI64Type());
    auto chw_to_hwc = rewriter.create<arith::ConstantOp>(
        op.getLoc(), chw_to_hwcTy,
        DenseIntElementsAttr::get(chw_to_hwcTy, SmallVector<int64_t>{1, 2, 0}));
    auto hwcInTy = RankedTensorType::get(
        {getHeight(inputTy), getWidth(inputTy), getChannels(inputTy)},
        inputTy.getElementType());
    auto nhwcOutTy = RankedTensorType::get(
        {1, getHeight(outputTy), getWidth(outputTy), getChannels(outputTy)},
        outputTy.getElementType());
    auto hwcOutTy = RankedTensorType::get(
        {getHeight(outputTy), getWidth(outputTy), getChannels(outputTy)},
        outputTy.getElementType());

    if (getDataFormat() == DataFormat::CHW) {
      // transpose input to HWC format
      input = rewriter
                  .create<tosa::TransposeOp>(op.getLoc(), hwcInTy, input,
                                             chw_to_hwc)
                  .getOutput();
    }

    // reshape to add N dimension
    SmallVector<int64_t> nInShape(hwcInTy.getShape());
    nInShape.insert(nInShape.begin(), 1);
    auto nInTy = RankedTensorType::get(nInShape, inputTy.getElementType());
    auto inputN = rewriter.create<tosa::ReshapeOp>(
        op.getLoc(), nInTy, input,
        rewriter.getDenseI64ArrayAttr(nInTy.getShape()));

    // scale is num, denom for H & W
    SmallVector<int64_t> scale{getHeight(outputTy), getHeight(inputTy),
                               getWidth(outputTy), getWidth(inputTy)};
    // these are equivalent to half pixel sampling
    SmallVector<int64_t> offset{(getHeight(inputTy) - getHeight(outputTy)) / 2,
                                (getWidth(inputTy) - getWidth(outputTy)) / 2};
    SmallVector<int64_t> border{0, 0};

    mlir::Value resized =
        rewriter
            .create<tosa::ResizeOp>(op.getLoc(), nhwcOutTy, inputN,
                                    rewriter.getDenseI64ArrayAttr(scale),
                                    rewriter.getDenseI64ArrayAttr(offset),
                                    rewriter.getDenseI64ArrayAttr(border),
                                    rewriter.getStringAttr("NEAREST_NEIGHBOR"))
            .getOutput();

    // reshape to expected output type
    Value output = rewriter
                       .create<tosa::ReshapeOp>(
                           op.getLoc(), hwcOutTy, resized,
                           rewriter.getDenseI64ArrayAttr(hwcOutTy.getShape()))
                       .getOutput();

    if (getDataFormat() == DataFormat::CHW) {
      // transpose back to CHW format
      auto hwc_to_chw = rewriter.create<arith::ConstantOp>(
          op.getLoc(), chw_to_hwcTy,
          DenseIntElementsAttr::get(chw_to_hwcTy,
                                    SmallVector<int64_t>{2, 0, 1}));
      output = rewriter
                   .create<tosa::TransposeOp>(op.getLoc(), outputTy, output,
                                              hwc_to_chw)
                   .getOutput();
    }

    rewriter.replaceOp(op, output);

    return success();
  }
};

struct Rotate90OpConverter : public OpConversionPattern<vosa::Rotate90Op> {
  using OpConversionPattern<vosa::Rotate90Op>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::Rotate90Op op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto outputType =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    int rotate = adaptor.getRotate();
    // Mod rotate to be between -3 and 3
    rotate = rotate % 4;
    if (rotate == 0)
      rewriter.replaceOp(op, adaptor.getInput());
    else {
      auto permType = RankedTensorType::get({3}, rewriter.getI64Type());
      SmallVector<int64_t> transposePerm{getWidthDimension(),
                                         getHeightDimension()};
      transposePerm.insert(transposePerm.begin() + getChannelDimension(),
                           getChannelDimension());
      auto transposePermCst = rewriter.create<arith::ConstantOp>(
          op.getLoc(), permType,
          DenseIntElementsAttr::get(permType, transposePerm));

      // If the image should be rotated 90 cw
      if (rotate == -3 || rotate == 1) {
        auto transposeOp = rewriter.create<tosa::TransposeOp>(
            op.getLoc(), outputType, adaptor.getInput(), transposePermCst);
        auto reverseOp = rewriter.create<tosa::ReverseOp>(
            op.getLoc(), outputType, transposeOp.getOutput(),
            rewriter.getI32IntegerAttr(getWidthDimension()));
        rewriter.replaceOp(op, reverseOp.getOutput());
      }
      // If the image should be rotated 90 acw
      else if (rotate == -1 || rotate == 3) {
        auto transposeOp = rewriter.create<tosa::TransposeOp>(
            op.getLoc(), outputType, adaptor.getInput(), transposePermCst);
        auto reverseOp = rewriter.create<tosa::ReverseOp>(
            op.getLoc(), outputType, transposeOp.getOutput(),
            rewriter.getI32IntegerAttr(getHeightDimension()));
        rewriter.replaceOp(op, reverseOp.getOutput());
      } else if (rotate == -2 || rotate == 2) {
        auto reverseOp = rewriter.create<tosa::ReverseOp>(
            op.getLoc(), outputType, adaptor.getInput(),
            rewriter.getI32IntegerAttr(getHeightDimension()));
        reverseOp = rewriter.create<tosa::ReverseOp>(
            op.getLoc(), outputType, reverseOp,
            rewriter.getI32IntegerAttr(getWidthDimension()));
        rewriter.replaceOp(op, reverseOp.getOutput());
      }
    }
    return success();
  }
};

//===----------------------------------------------------------------------===//
// Reduction operations.
//===----------------------------------------------------------------------===//

static FailureOr<Attribute> getMinValue(Builder &builder, Type outputTy,
                                        Type convertedTy) {
  if (outputTy.isa<IntegerType>()) {
    auto val = outputTy.isSignedInteger()
                   ? APInt::getSignedMinValue(outputTy.getIntOrFloatBitWidth())
                   : APInt(outputTy.getIntOrFloatBitWidth(), 0);
    return builder.getIntegerAttr(convertedTy, val);
  } else if (outputTy.isa<FloatType>()) {
    return builder.getFloatAttr(
        outputTy, APFloat::getLargest(
                      outputTy.cast<FloatType>().getFloatSemantics(), true));
  } else {
    return failure();
  }
}

static FailureOr<Attribute> getMaxValue(Builder &builder, Type outputTy,
                                        Type convertedTy) {
  if (outputTy.isa<IntegerType>()) {
    auto val = outputTy.isSignedInteger()
                   ? APInt::getSignedMaxValue(outputTy.getIntOrFloatBitWidth())
                   : APInt::getMaxValue(outputTy.getIntOrFloatBitWidth());
    return builder.getIntegerAttr(convertedTy, val);
  } else if (outputTy.isa<FloatType>()) {
    return builder.getFloatAttr(
        outputTy, APFloat::getLargest(
                      outputTy.cast<FloatType>().getFloatSemantics(), false));
  } else {
    return failure();
  }
}

static FailureOr<Attribute> getZeroValue(Builder &builder, Type ty) {
  if (ty.isa<IntegerType>()) {
    return builder.getIntegerAttr(ty, APInt(ty.getIntOrFloatBitWidth(), 0));
  } else if (ty.isa<FloatType>()) {
    return builder.getFloatAttr(
        ty, APFloat::getZero(ty.cast<FloatType>().getFloatSemantics(), false));
  } else {
    return failure();
  }
}

static FailureOr<Value> argMaxPredicate(OpBuilder &builder, Location loc,
                                        Type inputTy, Value cur, Value last) {
  if (inputTy.isa<IntegerType>()) {
    auto pred = inputTy.isSignedInteger() ? arith::CmpIPredicate::sgt
                                          : arith::CmpIPredicate::ugt;
    return builder.create<arith::CmpIOp>(loc, pred, cur, last).getResult();
  } else if (inputTy.isa<FloatType>()) {
    return builder
        .create<arith::CmpFOp>(loc, arith::CmpFPredicate::OGT, cur, last)
        .getResult();
  } else {
    return failure();
  }
}

static FailureOr<Value> argMinPredicate(OpBuilder &builder, Location loc,
                                        Type inputTy, Value cur, Value last) {
  if (inputTy.isa<IntegerType>()) {
    auto pred = inputTy.isSignedInteger() ? arith::CmpIPredicate::slt
                                          : arith::CmpIPredicate::ult;
    return builder.create<arith::CmpIOp>(loc, pred, cur, last).getResult();
  } else if (inputTy.isa<FloatType>()) {
    return builder
        .create<arith::CmpFOp>(loc, arith::CmpFPredicate::OLT, cur, last)
        .getResult();
  } else {
    return failure();
  }
}

static LogicalResult
convertChannelwiseArgOp(Operation *op, Location loc, Value input,
                        const TypeConverter *converter,
                        ConversionPatternRewriter &rewriter) {
  auto origInputElTy =
      op->getOperand(0).getType().cast<ShapedType>().getElementType();
  auto inputTy = input.getType().cast<ShapedType>();
  auto inputElTy = inputTy.getElementType();
  auto convertedTy =
      converter->convertType(op->getResult(0).getType()).cast<ShapedType>();
  auto convertedElTy = convertedTy.getElementType();

  SmallVector<AffineExpr, 3> srcExprs;
  for (int i = 0; i < 3; ++i)
    srcExprs.push_back(mlir::getAffineDimExpr(i, rewriter.getContext()));
  SmallVector<AffineExpr, 3> dstExprs;
  dstExprs.push_back(
      mlir::getAffineDimExpr(getHeightDimension(), rewriter.getContext()));
  dstExprs.push_back(
      mlir::getAffineDimExpr(getWidthDimension(), rewriter.getContext()));
  // channel dimension is always 0
  dstExprs.insert(dstExprs.begin() + getChannelDimension(),
                  mlir::getAffineConstantExpr(0, rewriter.getContext()));
  auto indexingMaps =
      AffineMap::inferFromExprList({srcExprs, dstExprs, dstExprs});

  SmallVector<utils::IteratorType> iterators;
  iterators.push_back(utils::IteratorType::parallel);
  iterators.push_back(utils::IteratorType::parallel);
  iterators.insert(iterators.begin() + getChannelDimension(),
                   utils::IteratorType::reduction);

  // get initial value for current min/max
  FailureOr<Attribute> currInit;
  if (isa<ArgMaxChannelwiseOp>(op)) {
    currInit = getMinValue(rewriter, origInputElTy, inputElTy);
  } else if (isa<ArgMinChannelwiseOp>(op)) {
    currInit = getMaxValue(rewriter, origInputElTy, inputElTy);
  } else {
    return op->emitError("unsupported channelwise arg op");
  }
  if (failed(currInit)) {
    return op->emitError("unsupported data type for arg op");
  }
  auto currentTy = RankedTensorType::get(convertedTy.getShape(), inputElTy);
  Value current = makeFilledTensor(
      loc, currentTy.getShape(),
      rewriter.create<arith::ConstantOp>(loc, cast<TypedAttr>(*currInit)),
      rewriter);

  // create index output tensor
  Value outIndex = makeFilledTensor(
      loc, convertedTy.getShape(),
      rewriter.create<arith::ConstantOp>(
          loc,
          rewriter.getIntegerAttr(
              convertedElTy, APInt(convertedElTy.getIntOrFloatBitWidth(), 0))),
      rewriter);

  bool didEncounterError = false;
  auto linalgOp = rewriter.create<linalg::GenericOp>(
      loc, TypeRange{convertedTy, currentTy}, input,
      ValueRange{outIndex, current}, indexingMaps, iterators,
      [&](OpBuilder &nestedBuilder, Location nestedLoc, ValueRange args) {
        auto curVal = args[0];
        auto lastIdx = args[1];
        auto lastVal = args[2];

        auto curIdx = nestedBuilder.create<arith::IndexCastOp>(
            nestedLoc, lastIdx.getType(),
            nestedBuilder.create<linalg::IndexOp>(loc, getChannelDimension()));

        FailureOr<Value> predicate;
        if (isa<ArgMaxChannelwiseOp>(op)) {
          predicate = argMaxPredicate(nestedBuilder, nestedLoc, origInputElTy,
                                      curVal, lastVal);
        } else if (isa<ArgMinChannelwiseOp>(op)) {
          predicate = argMinPredicate(nestedBuilder, nestedLoc, origInputElTy,
                                      curVal, lastVal);
        }

        if (failed(predicate)) {
          op->emitError("unsupported channelwise reduction");
          didEncounterError = true;
          return;
        }

        Value i = nestedBuilder
                      .create<mlir::arith::SelectOp>(nestedLoc, *predicate,
                                                     curIdx, lastIdx)
                      .getResult();
        Value v = nestedBuilder
                      .create<mlir::arith::SelectOp>(nestedLoc, *predicate,
                                                     curVal, lastVal)
                      .getResult();

        nestedBuilder.create<linalg::YieldOp>(nestedLoc, ValueRange{i, v});
      });

  if (didEncounterError)
    return failure();

  rewriter.replaceOp(op, linalgOp->getResult(0));
  return success();
}

template <typename T>
struct ArgChannelwiseOpConverter : public OpConversionPattern<T> {
  using OpConversionPattern<T>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(T op, typename T::Adaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    const TypeConverter *converter = ConversionPattern::getTypeConverter();
    return convertChannelwiseArgOp(op, op.getLoc(), adaptor.getInput(),
                                   converter, rewriter);
  }
};

static LogicalResult
convertPlanewiseArgOp(Operation *op, Location loc, Value input,
                      const TypeConverter *converter,
                      ConversionPatternRewriter &rewriter) {
  auto origInputElTy =
      op->getOperand(0).getType().cast<ShapedType>().getElementType();
  auto inputTy = input.getType().cast<ShapedType>();
  auto inputElTy = inputTy.getElementType();

  auto outputTy = op->getResult(0).getType();
  auto convertedTy = converter->convertType(outputTy).cast<ShapedType>();
  auto convertedElTy = convertedTy.getElementType();

  SmallVector<AffineExpr, 3> srcExprs;
  for (int i = 0; i < 3; ++i)
    srcExprs.push_back(mlir::getAffineDimExpr(i, rewriter.getContext()));
  SmallVector<AffineExpr, 3> dstExprs;
  auto indexingMaps =
      AffineMap::inferFromExprList({srcExprs, dstExprs, dstExprs, dstExprs});

  SmallVector<utils::IteratorType> iterators{3, utils::IteratorType::reduction};

  // get initial value for current min/max
  FailureOr<Attribute> currInit;
  if (isa<ArgMaxPlanewiseOp>(op)) {
    currInit = getMinValue(rewriter, origInputElTy, inputElTy);
  } else if (isa<ArgMinPlanewiseOp>(op)) {
    currInit = getMaxValue(rewriter, origInputElTy, inputElTy);
  } else {
    return op->emitError("unsupported planewise arg op");
  }
  if (failed(currInit)) {
    return op->emitError("unsupported data type for arg op");
  }
  Value current = makeFilledTensor(
      loc, {},
      rewriter.create<arith::ConstantOp>(loc, cast<TypedAttr>(*currInit)),
      rewriter);

  // create Y,X index output tensors
  Value outYIndex = makeFilledTensor(
      loc, {},
      rewriter.create<arith::ConstantOp>(
          loc,
          rewriter.getIntegerAttr(
              convertedElTy, APInt(convertedElTy.getIntOrFloatBitWidth(), 0))),
      rewriter);
  Value outXIndex = makeFilledTensor(
      loc, {},
      rewriter.create<arith::ConstantOp>(
          loc,
          rewriter.getIntegerAttr(
              convertedElTy, APInt(convertedElTy.getIntOrFloatBitWidth(), 0))),
      rewriter);

  bool didEncounterError = false;
  auto linalgOp = rewriter.create<linalg::GenericOp>(
      loc,
      TypeRange{outYIndex.getType(), outXIndex.getType(), current.getType()},
      input, ValueRange{outYIndex, outXIndex, current}, indexingMaps, iterators,
      [&](OpBuilder &nestedBuilder, Location nestedLoc, ValueRange args) {
        auto curVal = args[0];
        auto lastYIdx = args[1];
        auto lastXIdx = args[2];
        auto lastVal = args[3];

        auto curYIdx = nestedBuilder
                           .create<arith::IndexCastOp>(
                               nestedLoc, lastYIdx.getType(),
                               nestedBuilder.create<linalg::IndexOp>(
                                   loc, getHeightDimension()))
                           .getResult();
        auto curXIdx = nestedBuilder
                           .create<arith::IndexCastOp>(
                               nestedLoc, lastXIdx.getType(),
                               nestedBuilder.create<linalg::IndexOp>(
                                   loc, getWidthDimension()))
                           .getResult();

        FailureOr<Value> predicate;
        if (isa<ArgMaxPlanewiseOp>(op)) {
          predicate = argMaxPredicate(nestedBuilder, nestedLoc, origInputElTy,
                                      curVal, lastVal);
        } else if (isa<ArgMinPlanewiseOp>(op)) {
          predicate = argMinPredicate(nestedBuilder, nestedLoc, origInputElTy,
                                      curVal, lastVal);
        }

        if (failed(predicate)) {
          op->emitError("unsupported planewise reduction");
          didEncounterError = true;
          return;
        }

        Value y = nestedBuilder
                      .create<mlir::arith::SelectOp>(nestedLoc, *predicate,
                                                     curYIdx, lastYIdx)
                      .getResult();
        Value x = nestedBuilder
                      .create<mlir::arith::SelectOp>(nestedLoc, *predicate,
                                                     curXIdx, lastXIdx)
                      .getResult();
        Value v = nestedBuilder
                      .create<mlir::arith::SelectOp>(nestedLoc, *predicate,
                                                     curVal, lastVal)
                      .getResult();

        nestedBuilder.create<linalg::YieldOp>(nestedLoc, ValueRange{y, x, v});
      });

  if (didEncounterError)
    return failure();

  auto yVal =
      rewriter
          .create<tensor::ExtractOp>(loc, linalgOp->getResult(0), ValueRange{})
          .getResult();
  auto xVal =
      rewriter
          .create<tensor::ExtractOp>(loc, linalgOp->getResult(1), ValueRange{})
          .getResult();
  auto result = rewriter.create<tensor::FromElementsOp>(loc, convertedTy,
                                                        ValueRange{yVal, xVal});

  rewriter.replaceOp(op, result.getResult());
  return success();
}

template <typename T>
struct ArgPlanewiseOpConverter : public OpConversionPattern<T> {
  using OpConversionPattern<T>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(T op, typename T::Adaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    const TypeConverter *converter = ConversionPattern::getTypeConverter();
    return convertPlanewiseArgOp(op, op.getLoc(), adaptor.getInput(), converter,
                                 rewriter);
  }
};

struct ReduceInterpChannelwiseOpConverter
    : public OpConversionPattern<vosa::ReduceInterpChannelwiseOp> {
  using OpConversionPattern<
      vosa::ReduceInterpChannelwiseOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::ReduceInterpChannelwiseOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    Location loc = op.getLoc();

    auto origInputTy = op.getInput0().getType().cast<ShapedType>();
    auto origInputElTy = origInputTy.getElementType();
    auto outputTy =
        getTypeConverter()->convertType(op.getType()).cast<ShapedType>();
    auto initOutTensor = rewriter
                             .create<tensor::EmptyOp>(loc, outputTy.getShape(),
                                                      outputTy.getElementType())
                             .getResult();

    unsigned numChannels = getChannels(origInputTy);
    // Define the number of loops - we will only iterate over x and y
    // coordinates
    SmallVector<utils::IteratorType> iterators;
    iterators.push_back(utils::IteratorType::parallel);
    iterators.push_back(utils::IteratorType::parallel);
    iterators.push_back(utils::IteratorType::parallel);

    // Define the affine maps
    SmallVector<SmallVector<AffineExpr>, 5> maps;
    for (unsigned channel = 0; channel < numChannels + 1; channel++) {
      // affine_map<(d0, d1, d2) -> (d0, d1, d2)>
      maps.push_back({
          mlir::getAffineDimExpr(0, rewriter.getContext()),
          mlir::getAffineDimExpr(1, rewriter.getContext()),
          mlir::getAffineDimExpr(2, rewriter.getContext()),
      });
    }

    SmallVector<AffineExpr> dstExprs;
    for (int i = 0; i < 3; ++i)
      // affine_map<(d0, d1, d2) -> (d0, d1, d2)>
      dstExprs.push_back(mlir::getAffineDimExpr(i, rewriter.getContext()));
    maps.push_back(dstExprs);

    SmallVector<ArrayRef<AffineExpr>, 5> mapsAsRefs;
    for (unsigned channel = 0; channel < numChannels + 2; channel++) {
      mapsAsRefs.push_back(ArrayRef<AffineExpr>(maps[channel]));
    }

    // Create slices for the input image - one slice per channel. First, define
    // all the required args (types, offsets, strides, sizes).
    auto inputTy = adaptor.getInput0().getType().cast<ShapedType>();

    SmallVector<mlir::Value> channelSlices;
    for (unsigned i = 0; i < numChannels; i++) {
      SmallVector<int64_t> start;
      SmallVector<int64_t> size;
      start.resize(2, 0);
      start.insert(start.begin() + getChannelDimension(), i);
      size.push_back(getHeight(inputTy));
      size.push_back(getWidth(inputTy));
      // size.push_back(getChannels(inputTy));
      size.insert(size.begin() + getChannelDimension(), 1);

      auto sliceTy = RankedTensorType::get(size, inputTy.getElementType());
      channelSlices.push_back(rewriter.create<tosa::SliceOp>(
          op.getLoc(), sliceTy, adaptor.getInput0(),
          rewriter.getDenseI64ArrayAttr(start),
          rewriter.getDenseI64ArrayAttr(size)));
    }
    // Create a slice for the input/output tensor
    SmallVector<int64_t> start;
    SmallVector<int64_t> size;
    start.resize(2, 0);
    start.insert(start.begin() + getChannelDimension(), 0);
    size.push_back(getHeight(outputTy));
    size.push_back(getWidth(outputTy));
    size.insert(size.begin() + getChannelDimension(), 1);

    auto flatTy = RankedTensorType::get(size, outputTy.getElementType());
    auto slice_out = rewriter.create<tosa::SliceOp>(
        op.getLoc(), flatTy, adaptor.getInput1(), start, size);
    channelSlices.push_back(slice_out);

    initOutTensor = rewriter
                        .create<tensor::EmptyOp>(loc, flatTy.getShape(),
                                                 outputTy.getElementType())
                        .getResult();

    // Linalg generic op over 1-channel slices of input0 and input1 values.
    bool didEncounterError = false;
    auto linalgOp = rewriter.create<linalg::GenericOp>(
        loc, flatTy, channelSlices, initOutTensor,
        AffineMap::inferFromExprList(mapsAsRefs), iterators,
        [&](OpBuilder &nestedBuilder, Location nestedLoc,
            ValueRange blockArgs) {
          Value ci32_zero = rewriter.create<arith::ConstantOp>(
              loc, rewriter.getI32IntegerAttr(0));
          Value ci32_one = rewriter.create<arith::ConstantOp>(
              loc, rewriter.getI32IntegerAttr(1));
          Value cf32_one = rewriter.create<arith::ConstantOp>(
              loc, rewriter.getF32FloatAttr(1.0));

          // Channel value (i.e. the value to decide which channel values to
          // interpolate) is the first input after the input image (input1(y,
          // x))
          Value v = blockArgs[numChannels];

          // Top channel index - used for clamping
          Value top_channel = rewriter.create<arith::ConstantOp>(
              loc, rewriter.getI32IntegerAttr(numChannels - 1));

          // c=floor(input1[y,x])
          Value cf =
              rewriter.create<math::FloorOp>(loc, rewriter.getF32Type(), v);

          // Lower channel index (cf as an int)
          Value lower_ci =
              rewriter.create<arith::FPToSIOp>(loc, rewriter.getI32Type(), cf);
          // Upper channel index ((cf + 1) as an int)
          Value upper_ci = rewriter.create<arith::AddIOp>(
              loc, rewriter.getI32Type(), lower_ci, ci32_one);

          // (lower_ci < 0)
          auto lower_ci_lt_zero = rewriter.create<arith::CmpIOp>(
              loc, arith::CmpIPredicate::slt, lower_ci, ci32_zero);
          // (lower_ci >= top_channel)
          auto lower_ci_gte_tc = rewriter.create<arith::CmpIOp>(
              loc, arith::CmpIPredicate::slt, top_channel, lower_ci);
          // Clamp lower_ci
          lower_ci = rewriter.create<arith::SelectOp>(loc, lower_ci_lt_zero,
                                                      ci32_zero, lower_ci);
          lower_ci = rewriter.create<arith::SelectOp>(loc, lower_ci_gte_tc,
                                                      top_channel, lower_ci);

          // (upper_ci < 0)
          auto upper_ci_lt_zero = rewriter.create<arith::CmpIOp>(
              loc, arith::CmpIPredicate::slt, upper_ci, ci32_zero);
          // (upper_ci >= top_channel)
          auto upper_ci_gte_tc = rewriter.create<arith::CmpIOp>(
              loc, arith::CmpIPredicate::slt, top_channel, upper_ci);
          // Clamp upper_ci
          upper_ci = rewriter.create<arith::SelectOp>(loc, upper_ci_lt_zero,
                                                      ci32_zero, upper_ci);
          upper_ci = rewriter.create<arith::SelectOp>(loc, upper_ci_gte_tc,
                                                      top_channel, upper_ci);

          // lower_weight = c + 1 - v
          Value lower_weight = rewriter.create<arith::SubFOp>(
              loc, rewriter.getF32Type(),
              rewriter.create<arith::AddFOp>(loc, rewriter.getF32Type(), cf,
                                             cf32_one),
              v);
          // upper_weight = v - c
          Value upper_weight =
              rewriter.create<arith::SubFOp>(loc, rewriter.getF32Type(), v, cf);

          // Set the init value for input0[x, y, c] to input0[x, y, 0]
          Value lower_val = blockArgs[0];
          // Set the init value for input0[x, y, c + 1] to input0[x, y, 0]
          Value upper_val = blockArgs[0];

          // The output is alwasy f32, so just cast to f32.
          if (origInputElTy.isSignedInteger()) {
            lower_val = rewriter.create<arith::SIToFPOp>(
                loc, rewriter.getF32Type(), lower_val);
            upper_val = rewriter.create<arith::SIToFPOp>(
                loc, rewriter.getF32Type(), upper_val);
          } else if (origInputElTy.isUnsignedInteger()) {
            lower_val = rewriter.create<arith::UIToFPOp>(
                loc, rewriter.getF32Type(), lower_val);
            upper_val = rewriter.create<arith::UIToFPOp>(
                loc, rewriter.getF32Type(), upper_val);
          }

          // Iterate over the remaining channels and, if needed, update the
          // values for lower_val and for upper_val
          for (unsigned channel = 1; channel < numChannels; channel++) {
            // Channel value. The output is alwasy f32, so just cast to f32.
            Value chVal = blockArgs[channel];
            if (origInputElTy.isSignedInteger())
              chVal = rewriter.create<arith::SIToFPOp>(
                  loc, rewriter.getF32Type(), chVal);
            else if (origInputElTy.isUnsignedInteger())
              chVal = rewriter.create<arith::UIToFPOp>(
                  loc, rewriter.getF32Type(), chVal);

            // "This channel" idx
            Value ci32_channel = rewriter.create<arith::ConstantOp>(
                loc, rewriter.getI32IntegerAttr(channel));

            //  cond = (cb == channel)
            Value lower_val_eq_ch = rewriter.create<arith::CmpIOp>(
                loc, arith::CmpIPredicate::eq, lower_ci, ci32_channel);
            // lower_val = (cond) ? chVal : lower_val;
            lower_val = rewriter.create<arith::SelectOp>(loc, lower_val_eq_ch,
                                                         chVal, lower_val);

            //  cond = ((cb + 1) == channel)
            Value upper_val_eq_ch = rewriter.create<arith::CmpIOp>(
                loc, arith::CmpIPredicate::eq, upper_ci, ci32_channel);
            // upper_val = (cond) ? chVal : upper_val;
            upper_val = rewriter.create<arith::SelectOp>(loc, upper_val_eq_ch,
                                                         chVal, upper_val);
          }

          //  opResult = lower_val * lower_weight + upper_val * upper_weight
          Value lower_s =
              rewriter.create<arith::MulFOp>(loc, lower_val, lower_weight);
          Value upper_s =
              rewriter.create<arith::MulFOp>(loc, upper_val, upper_weight);
          Value opResult =
              rewriter.create<arith::AddFOp>(loc, lower_s, upper_s);

          nestedBuilder.create<linalg::YieldOp>(loc, opResult);
        });

    auto reshapedResTy =
        RankedTensorType::get(outputTy.getShape(), outputTy.getElementType());
    auto res = rewriter.create<tosa::ReshapeOp>(
        op.getLoc(), reshapedResTy, linalgOp->getResults().front(),
        rewriter.getDenseI64ArrayAttr(reshapedResTy.getShape()));

    if (didEncounterError)
      return failure();

    rewriter.replaceOp(op, res->getResults());
    return success();
  }
};

static FailureOr<Value> reduceMax(OpBuilder &builder, Location loc,
                                  Type outputTy, ValueRange args) {
  Value predicate;
  if (outputTy.isa<IntegerType>()) {
    auto pred = outputTy.isSignedInteger() ? arith::CmpIPredicate::sgt
                                           : arith::CmpIPredicate::ugt;
    predicate = builder.create<arith::CmpIOp>(loc, pred, args[0], args[1]);
  } else if (outputTy.isa<FloatType>()) {
    predicate = builder.create<arith::CmpFOp>(loc, arith::CmpFPredicate::OGT,
                                              args[0], args[1]);
  } else {
    return failure();
  }

  return builder.create<mlir::arith::SelectOp>(loc, predicate, args[0], args[1])
      .getResult();
}

static FailureOr<Value> reduceMin(OpBuilder &builder, Location loc,
                                  Type outputTy, ValueRange args) {
  Value predicate;
  if (outputTy.isa<IntegerType>()) {
    auto pred = outputTy.isSignedInteger() ? arith::CmpIPredicate::slt
                                           : arith::CmpIPredicate::ult;
    predicate = builder.create<arith::CmpIOp>(loc, pred, args[0], args[1]);
  } else if (outputTy.isa<FloatType>()) {
    predicate = builder.create<arith::CmpFOp>(loc, arith::CmpFPredicate::OLT,
                                              args[0], args[1]);
  } else {
    return failure();
  }

  return builder.create<mlir::arith::SelectOp>(loc, predicate, args[0], args[1])
      .getResult();
}

static FailureOr<Value> reduceSum(OpBuilder &builder, Location loc,
                                  Type outputTy, ValueRange args) {
  if (outputTy.isa<IntegerType>()) {
    return builder.create<mlir::arith::AddIOp>(loc, args[0], args[1])
        .getResult();
  } else if (outputTy.isa<FloatType>()) {
    return builder.create<mlir::arith::AddFOp>(loc, args[0], args[1])
        .getResult();
  } else {
    return failure();
  }
}

static LogicalResult
convertChannelwiseReductionOp(Operation *op, Location loc, Value input,
                              const TypeConverter *converter,
                              ConversionPatternRewriter &rewriter) {
  auto outputElTy =
      op->getResult(0).getType().cast<ShapedType>().getElementType();
  auto convertedTy =
      converter->convertType(op->getResult(0).getType()).cast<ShapedType>();
  auto convertedElTy = convertedTy.getElementType();

  SmallVector<AffineExpr, 3> srcExprs;
  for (int i = 0; i < 3; ++i)
    srcExprs.push_back(mlir::getAffineDimExpr(i, rewriter.getContext()));
  SmallVector<AffineExpr, 2> dstExprs;
  dstExprs.push_back(
      mlir::getAffineDimExpr(getHeightDimension(), rewriter.getContext()));
  dstExprs.push_back(
      mlir::getAffineDimExpr(getWidthDimension(), rewriter.getContext()));
  // channel dimension is always 0
  dstExprs.insert(dstExprs.begin() + getChannelDimension(),
                  mlir::getAffineConstantExpr(0, rewriter.getContext()));
  auto indexingMaps = AffineMap::inferFromExprList({srcExprs, dstExprs});

  SmallVector<utils::IteratorType> iterators;
  iterators.push_back(utils::IteratorType::parallel);
  iterators.push_back(utils::IteratorType::parallel);
  iterators.insert(iterators.begin() + getChannelDimension(),
                   utils::IteratorType::reduction);

  // get reduction initial value
  FailureOr<Attribute> rInit;
  if (isa<ReduceMaxChannelwiseOp>(op)) {
    rInit = getMinValue(rewriter, outputElTy, convertedElTy);
  } else if (isa<ReduceMinChannelwiseOp>(op)) {
    rInit = getMaxValue(rewriter, outputElTy, convertedElTy);
  } else if (isa<ReduceSumChannelwiseOp>(op)) {
    rInit = getZeroValue(rewriter, convertedElTy);
  } else {
    return op->emitError("unsupported channelwise reduction");
  }
  if (failed(rInit)) {
    return op->emitError("unsupported data type for reduction");
  }

  // create output: 0 dimension tensor with initial value
  Value rTensor = makeFilledTensor(
      loc, convertedTy.getShape(),
      rewriter.create<arith::ConstantOp>(loc, cast<TypedAttr>(*rInit)),
      rewriter);

  bool didEncounterError = false;
  auto linalgOp = rewriter.create<linalg::GenericOp>(
      loc, convertedTy, input, rTensor, indexingMaps, iterators,
      [&](OpBuilder &nestedBuilder, Location nestedLoc, ValueRange args) {
        FailureOr<Value> opResult;
        if (isa<ReduceMaxChannelwiseOp>(op)) {
          opResult = reduceMax(nestedBuilder, nestedLoc, outputElTy, args);
        } else if (isa<ReduceMinChannelwiseOp>(op)) {
          opResult = reduceMin(nestedBuilder, nestedLoc, outputElTy, args);
        } else if (isa<ReduceSumChannelwiseOp>(op)) {
          opResult = reduceSum(nestedBuilder, nestedLoc, outputElTy, args);
        }

        if (failed(opResult)) {
          op->emitError("unsupported channelwise reduction");
          didEncounterError = true;
          return;
        }

        nestedBuilder.create<linalg::YieldOp>(nestedLoc, *opResult);
      });

  if (didEncounterError)
    return failure();

  rewriter.replaceOp(op, linalgOp->getResults());
  return success();
}

template <typename T>
struct ReduceChannelwiseOpConverter : public OpConversionPattern<T> {
  using OpConversionPattern<T>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(T op, typename T::Adaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    const TypeConverter *converter = ConversionPattern::getTypeConverter();
    return convertChannelwiseReductionOp(op, op.getLoc(), adaptor.getInput(),
                                         converter, rewriter);
  }
};

static LogicalResult
convertPlanewiseReductionOp(Operation *op, Location loc, Value input,
                            const TypeConverter *converter,
                            ConversionPatternRewriter &rewriter) {
  // use linalg generic for 2D reduction

  auto outputElTy =
      op->getResult(0).getType().cast<ShapedType>().getElementType();
  auto convertedTy =
      converter->convertType(op->getResult(0).getType()).cast<ShapedType>();
  auto convertedElTy = convertedTy.getElementType();

  SmallVector<AffineExpr, 3> srcExprs;
  for (int i = 0; i < 3; ++i)
    srcExprs.push_back(mlir::getAffineDimExpr(i, rewriter.getContext()));
  SmallVector<AffineExpr, 3> dstExprs;
  auto indexingMaps = AffineMap::inferFromExprList({srcExprs, dstExprs});

  SmallVector<utils::IteratorType> iterators{3, utils::IteratorType::reduction};

  // get reduction initial value
  FailureOr<Attribute> rInit;
  if (isa<ReduceMaxPlanewiseOp>(op)) {
    rInit = getMinValue(rewriter, outputElTy, convertedElTy);
  } else if (isa<ReduceMinPlanewiseOp>(op)) {
    rInit = getMaxValue(rewriter, outputElTy, convertedElTy);
  } else if (isa<ReduceSumPlanewiseOp>(op)) {
    rInit = getZeroValue(rewriter, convertedElTy);
  } else {
    return op->emitError("unsupported planewise reduction");
  }
  if (failed(rInit)) {
    return op->emitError("unsupported data type for reduction");
  }

  // create output: 0 dimension tensor with initial value
  Value rTensor = makeFilledTensor(
      loc, {}, rewriter.create<arith::ConstantOp>(loc, cast<TypedAttr>(*rInit)),
      rewriter);

  // generic op over input values
  bool didEncounterError = false;
  auto linalgOp = rewriter.create<linalg::GenericOp>(
      loc, rTensor.getType(), input, rTensor, indexingMaps, iterators,
      [&](OpBuilder &nestedBuilder, Location nestedLoc, ValueRange args) {
        FailureOr<Value> opResult;
        if (isa<ReduceMaxPlanewiseOp>(op)) {
          opResult = reduceMax(nestedBuilder, nestedLoc, outputElTy, args);
        } else if (isa<ReduceMinPlanewiseOp>(op)) {
          opResult = reduceMin(nestedBuilder, nestedLoc, outputElTy, args);
        } else if (isa<ReduceSumPlanewiseOp>(op)) {
          opResult = reduceSum(nestedBuilder, nestedLoc, outputElTy, args);
        }

        if (failed(opResult)) {
          op->emitError("unsupported planewise reduction");
          didEncounterError = true;
          return;
        }

        nestedBuilder.create<linalg::YieldOp>(nestedLoc, *opResult);
      });

  if (didEncounterError)
    return failure();

  auto output_type = mlir::RankedTensorType::get({1, 1, 1}, convertedElTy);
  auto reassociation_list = rewriter.getArrayAttr({});
  auto expand_shape = rewriter.create<tensor::ExpandShapeOp>(
      loc, output_type, linalgOp->getResult(0), reassociation_list);
  rewriter.replaceOp(op, expand_shape->getResult(0));
  return success();
}

template <typename T>
struct ReducePlanewiseOpConverter : public OpConversionPattern<T> {
  using OpConversionPattern<T>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(T op, typename T::Adaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    const TypeConverter *converter = ConversionPattern::getTypeConverter();
    return convertPlanewiseReductionOp(op, op.getLoc(), adaptor.getInput(),
                                       converter, rewriter);
  }
};

//===----------------------------------------------------------------------===//
// Custom operators.
//===----------------------------------------------------------------------===//

struct CustomScalarOpConverter
    : public OpConversionPattern<vosa::CustomScalarOp> {
  using OpConversionPattern<vosa::CustomScalarOp>::OpConversionPattern;

  LogicalResult
  matchAndRewrite(vosa::CustomScalarOp op, OpAdaptor adaptor,
                  ConversionPatternRewriter &rewriter) const final {
    auto loc = op.getLoc();

    auto funcAttr = op->getAttrOfType<FlatSymbolRefAttr>("function");
    func::FuncOp func =
        SymbolTable::lookupNearestSymbolFrom<func::FuncOp>(op, funcAttr);
    if (!func) {
      return op.emitError() << "'" << funcAttr.getValue()
                            << "' does not reference a valid function";
    }

    // call the function
    auto callOp =
        rewriter.create<mlir::func::CallOp>(loc, func, op.getInputs());
    auto res = callOp.getResult(0);

    rewriter.replaceOp(op, res);
    return success();
  }
};

void populateVosaToTosaConversionPatterns(TypeConverter &type_converter,
                                          RewritePatternSet *patterns) {
  // clang-format off
  patterns->add<
    ConstantOpConverter,
    EqualOpConverter,
    GreaterOpConverter,
    GreaterEqualOpConverter,
    BroadcastChannelwiseOpConverter,
    BroadcastPlanewiseOpConverter,
    CastOpConverter,
    ChannelExtractOpConverter,
    ConcatOpConverter,
    ExportChannelOpConverter,
    ImportChannelOpConverter,
    MeshGridOpConverter,
    PiecewiseLinearOpConverter,
    TosaGraphOpConverter,
    WhereOpConverter,
    AbsDiffOpConverter,
    AddOpConverter,
    AndOpConverter,
    DivOpConverter,
    MaxOpConverter,
    MinOpConverter,
    ModOpConverter,
    MultOpConverter,
    OrOpConverter,
    SubOpConverter,
    AbsOpConverter,
    ArithmeticShiftRightOpConverter,
    ClampOpConverter,
    GammaCorrectionOpConverter,
    LogicalShiftRightOpConverter,
    NotOpConverter,
    PowerOpConverter,
    RoundOpConverter,
    SqrtOpConverter,
    Conv2DOpConverter,
    DecimateOpConverter,
    ExpandOpConverter,
    PadOpConverter,
    PointwiseMatrixMultiplyOpConverter,
    ResizeBilinearOpConverter,
    ResizeNearestNeighbourOpConverter,
    Rotate90OpConverter,
    ArgChannelwiseOpConverter<ArgMaxChannelwiseOp>,
    ArgPlanewiseOpConverter<ArgMaxPlanewiseOp>,
    ArgChannelwiseOpConverter<ArgMinChannelwiseOp>,
    ArgPlanewiseOpConverter<ArgMinPlanewiseOp>,
    ReduceInterpChannelwiseOpConverter,
    ReduceChannelwiseOpConverter<ReduceMaxChannelwiseOp>,
    ReducePlanewiseOpConverter<ReduceMaxPlanewiseOp>,
    ReduceChannelwiseOpConverter<ReduceMinChannelwiseOp>,
    ReducePlanewiseOpConverter<ReduceMinPlanewiseOp>,
    ReduceChannelwiseOpConverter<ReduceSumChannelwiseOp>,
    ReducePlanewiseOpConverter<ReduceSumPlanewiseOp>,
    CustomScalarOpConverter
    >(type_converter, patterns->getContext());
  // clang-format on
}

struct VosaToTosa : public VosaToTosaBase<VosaToTosa> {
public:
  void runOnOperation() override {
    RewritePatternSet patterns(&getContext());
    ConversionTarget target(getContext());
    VosaTypeConverter typeConverter;

    target.addLegalDialect<mlir::tosa::TosaDialect>();
    target.addLegalDialect<mlir::linalg::LinalgDialect>();
    target.addLegalDialect<mlir::tensor::TensorDialect>();
    target.addLegalDialect<mlir::math::MathDialect>();
    target.addLegalDialect<mlir::scf::SCFDialect>();
    target.addLegalDialect<mlir::arith::ArithDialect>();
    target.addLegalDialect<mlir::func::FuncDialect>();

    target.addIllegalDialect<vosa::VosaDialect>();

    populateVosaToTosaConversionPatterns(typeConverter, &patterns);

    mlir::func::FuncOp func = getOperation();
    if (failed(applyPartialConversion(func, target, std::move(patterns))))
      signalPassFailure();
  }
};
} // namespace

std::unique_ptr<Pass> vosa::createVosaToTosa() {
  return std::make_unique<VosaToTosa>();
}
