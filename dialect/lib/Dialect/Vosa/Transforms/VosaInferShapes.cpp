/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/Dialect/Vosa/IR/VosaDialect.h"
#include "vosa/Dialect/Vosa/Transforms/Passes.h"

#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/OpImplementation.h"
#include "mlir/IR/TypeUtilities.h"
#include "mlir/Interfaces/InferTypeOpInterface.h"
#include "mlir/Pass/Pass.h"

#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#define DEBUG_TYPE "shape-inference"

#include "PassDetail.h"

using namespace mlir;
using namespace mlir::vosa;

namespace {
/// The InferShapePass is a FunctionPass that performs intra-procedural
/// shape inference.
///
///    Algorithm:
///
///   1) Build a worklist containing all the VOSA operations that return a
///      dynamically shaped tensor: these are the operations that need shape
///      inference.
///   2) Iterate on the worklist:
///     a) find an operation to process: the next ready operation in the
///        worklist has all of its arguments with static shapes or scalars,
///     b) if no operation is found, break out of the loop,
///     c) remove the operation from the worklist,
///     d) infer the shape of its output from the argument types.
///   3) If the worklist is empty, the algorithm succeeded.
///
class InferShapePass : public mlir::vosa::VosaInferShapesBase<InferShapePass> {
public:
  void runOnOperation() override {
    DenseMap<Value, ShapedTypeComponents> shapesStorage;
    auto setShapes = [&](Value val, Type t) {
      if (auto st = t.dyn_cast<ShapedType>())
        shapesStorage[val] = st;
      else
        shapesStorage[val] = t;
    };
    auto operandShape = [&](Value val) -> ShapeAdaptor {
      // Query the WIP mapping rather than the type if set.
      auto it = shapesStorage.find(val);
      if (it == shapesStorage.end())
        return nullptr;
      return it->second;
    };
    // whether all of an ops operands are static shaped or scalar
    auto allOperandsInferred = [&](Operation *op) {
      ValueShapeRange range(op->getOperands(), operandShape);
      return llvm::all_of(op->getOperands(), [range](Value v) {
        if (v.getType().isa<ShapedType>())
          return range.getShape(v).hasStaticShape();
        else
          return true;
      });
    };

    auto f = getOperation();

    // Populate the worklist with the operations that need shape inference:
    // these are operations that return a dynamic shape.
    llvm::SmallPtrSet<mlir::Operation *, 16> opWorklist;
    f.walk([&](mlir::Operation *op) {
      if ((op->getDialect()->getNamespace() ==
               VosaDialect::getDialectNamespace() ||
           isa<mlir::func::ReturnOp>(op)) &&
          returnsDynamicShape(op))
        opWorklist.insert(op);
    });

    LLVM_DEBUG(llvm::dbgs()
               << "Inferring shapes for: " << opWorklist.size() << " ops\n");

    // Iterate on the operations in the worklist until all operations have been
    // inferred or no change happened (fix point).
    while (!opWorklist.empty()) {
      // Find the next operation ready for inference, that is an operation
      // with all operands already resolved (non-generic).
      auto nextop = llvm::find_if(opWorklist, allOperandsInferred);
      if (nextop == opWorklist.end()) {
        LLVM_DEBUG(llvm::dbgs() << "All done\n");
        break;
      }

      Operation *op = *nextop;
      opWorklist.erase(op);

      // Ask the operation to infer its output shapes.
      LLVM_DEBUG(llvm::dbgs() << "Inferring shape for: " << *op << "\n");
      if (auto shapeOp = dyn_cast<InferShapedTypeOpInterface>(op)) {
        ValueShapeRange range(op->getOperands(), operandShape);
        SmallVector<ShapedTypeComponents> returnedShapes;
        if (shapeOp
                .inferReturnTypeComponents(op->getContext(), op->getLoc(),
                                           range, op->getAttrDictionary(),
                                           op->getPropertiesStorage(),
                                           op->getRegions(), returnedShapes)
                .succeeded()) {
          for (auto it : llvm::zip(op->getResults(), returnedShapes)) {
            Value result = std::get<0>(it);
            if (auto shaped = result.getType().dyn_cast<ShapedType>()) {
              ShapedTypeComponents predictedShape = std::get<1>(it);
              auto newType = RankedTensorType::get(predictedShape.getDims(),
                                                   shaped.getElementType());
              setShapes(result, newType);
            }
          }
        }
      } else {
        op->emitError("unable to infer shape of operation without shape "
                      "inference interface ")
            << op->getName();
        return signalPassFailure();
      }
    }

    // If the operation worklist isn't empty, this indicates a failure.
    if (!opWorklist.empty()) {
      f.emitError("Shape inference failed, ")
          << opWorklist.size() << " operations couldn't be inferred\n";
      signalPassFailure();
    }

    // Actually update types with updated shape knowledge.
    for (auto it : shapesStorage) {
      auto result = it.second;
      if (result.hasRank()) {
        Type t = it.first.getType().cast<ShapedType>().clone(result.getDims());
        it.first.setType(t);
      }
    }

    // Update function result type with inferred shapes from return op
    llvm::SmallVector<mlir::Type, 4> inferredReturnTypes;
    f.walk([&](mlir::func::ReturnOp op) {
      if (inferredReturnTypes.empty()) {
        for (OpOperand &returnVal : op->getOpOperands()) {
          inferredReturnTypes.push_back(returnVal.get().getType());
        }
      } else {
        // already seen a ReturnOp - check it has same type
        for (auto i : llvm::zip(inferredReturnTypes, op->getOpOperands())) {
          if (std::get<0>(i) != std::get<1>(i).get().getType()) {
            f.emitError("Non-matching ReturnOp types");
            signalPassFailure();
          }
        }
      }
    });
    auto functionType = f.getFunctionType();
    auto newFunctionType = FunctionType::get(
        f.getContext(), functionType.getInputs(), inferredReturnTypes);
    f.setType(newFunctionType);
  }

  /// A utility method that returns if the given operation has a dynamically
  /// shaped result or dynamic dimensions.
  static bool returnsDynamicShape(Operation *op) {
    return llvm::any_of(op->getResultTypes(), [](Type resultType) {
      auto shaped = resultType.dyn_cast<ShapedType>();
      if (!shaped)
        return false;
      return !shaped.hasStaticShape();
    });
  }
};
} // namespace

std::unique_ptr<mlir::Pass> mlir::vosa::createVosaInferShapesPass() {
  return std::make_unique<InferShapePass>();
}
