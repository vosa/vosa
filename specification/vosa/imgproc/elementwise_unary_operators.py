"""Operations of unary type on Images.

License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import enum
import typing

from vosa import types as types
from vosa.op import OPERATOR_MODES, Op, ExecutionPattern, InputPattern


class SimpleUnaryExecutionPattern(ExecutionPattern):
    def __init__(self, input_dtype, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    input_dtype,
                    types.AType.IMAGE,
                    description="Input Image",
                    shape=["H", "W", "C"],
                )
            ),
            output_pattern=types.DataType(
                output_dtype,
                types.AType.IMAGE,
                description="Output Image",
                shape=["H", "W", "C"],
            ),
        )


class ABS_MODE(OPERATOR_MODES):
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class ABS(Op):
    """
    Return the absolute value of the Image,
    [latexmath]
    ++++
    output[y, x, c] = \\left| input[y,x,c] \\right|.
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[ABS_MODE, ExecutionPattern]:
        return {
            ABS_MODE.I_I_INT8_INT8: SimpleUnaryExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            ABS_MODE.I_I_INT16_INT16: SimpleUnaryExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            ABS_MODE.I_I_INT32_INT32: SimpleUnaryExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            ABS_MODE.I_I_FLOAT16_FLOAT16: SimpleUnaryExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            ABS_MODE.I_I_FLOAT32_FLOAT32: SimpleUnaryExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class ShiftExecutionPattern(SimpleUnaryExecutionPattern):
    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            ARITHMETIC_SHIFT_RIGHT.SHIFT_ATTRIBUTE: types.DataType(
                types.DType.UINT32,
                types.AType.SCALAR,
                shape=[],
                description="Number of bits to shift.",
            )
        }


class ARITHMETIC_SHIFT_RIGHT_MODES(OPERATOR_MODES):
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()


class ARITHMETIC_SHIFT_RIGHT(Op):
    """
    Perform an elementwise arithmetic right shift on all pixel values,
    [latexmath]
    ++++
    output[y,x,c] = input[y,x,c] \\gg shift.
    ++++
    """

    SHIFT_ATTRIBUTE = "shift"

    def to_flatbuffer(self):
        import vosa_serialization.ARITHMETIC_SHIFT_RIGHT
        import vosa_serialization.ARITHMETIC_SHIFT_RIGHT_MODE
        import vosa_serialization.ARITHMETIC_SHIFT_RIGHT_ATTRIBUTE

        arithmetic_shift_right_modes = {
            ARITHMETIC_SHIFT_RIGHT_MODES.I_I_INT8_INT8: vosa_serialization.ARITHMETIC_SHIFT_RIGHT_MODE.ARITHMETIC_SHIFT_RIGHT_MODE.I_I_INT8_INT8,
            ARITHMETIC_SHIFT_RIGHT_MODES.I_I_INT16_INT16: vosa_serialization.ARITHMETIC_SHIFT_RIGHT_MODE.ARITHMETIC_SHIFT_RIGHT_MODE.I_I_INT16_INT16,
            ARITHMETIC_SHIFT_RIGHT_MODES.I_I_INT32_INT32: vosa_serialization.ARITHMETIC_SHIFT_RIGHT_MODE.ARITHMETIC_SHIFT_RIGHT_MODE.I_I_INT32_INT32,
        }

        arithmetic_shift_right_t = (
            vosa_serialization.ARITHMETIC_SHIFT_RIGHT.ARITHMETIC_SHIFT_RIGHTT()
        )
        arithmetic_shift_right_t.mode = arithmetic_shift_right_modes[
            self.get_operator_mode()
        ]
        arithmetic_shift_right_t.attr = (
            vosa_serialization.ARITHMETIC_SHIFT_RIGHT_ATTRIBUTE.ARITHMETIC_SHIFT_RIGHT_ATTRIBUTET()
        )
        arithmetic_shift_right_t.attr.shift = self._attributes[
            self.SHIFT_ATTRIBUTE
        ].value()

        return arithmetic_shift_right_t

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[ARITHMETIC_SHIFT_RIGHT_MODES, ExecutionPattern]:
        return {
            ARITHMETIC_SHIFT_RIGHT_MODES.I_I_INT8_INT8: ShiftExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            ARITHMETIC_SHIFT_RIGHT_MODES.I_I_INT16_INT16: ShiftExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            ARITHMETIC_SHIFT_RIGHT_MODES.I_I_INT32_INT32: ShiftExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
        }


class ClampExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    description="Input Image.",
                    shape=["H", "W", "C"],
                ),
                types.DataType(
                    dtype, types.AType.SCALAR, description="Min Value", shape=[]
                ),
                types.DataType(
                    dtype, types.AType.SCALAR, description="Max Value", shape=[]
                ),
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                description="Output Image.",
                shape=["H", "W", "C"],
            ),
        )


class CLAMP_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class CLAMP(Op):
    """
    Clamp the values of an Image between a minimum and a maximum value,

    [latexmath]
    ++++
    output[y, x, c] = \\max(\\min( input_0[y, x, c], input_2), input_1).
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[CLAMP_MODES, ExecutionPattern]:
        return {
            CLAMP_MODES.I_I_UINT8_UINT8: ClampExecutionPattern(types.DType.UINT8),
            CLAMP_MODES.I_I_UINT16_UINT16: ClampExecutionPattern(types.DType.UINT16),
            CLAMP_MODES.I_I_UINT32_UINT32: ClampExecutionPattern(types.DType.UINT32),
            CLAMP_MODES.I_I_INT8_INT8: ClampExecutionPattern(types.DType.INT8),
            CLAMP_MODES.I_I_INT16_INT16: ClampExecutionPattern(types.DType.INT16),
            CLAMP_MODES.I_I_INT32_INT32: ClampExecutionPattern(types.DType.INT32),
            CLAMP_MODES.I_I_FLOAT16_FLOAT16: ClampExecutionPattern(types.DType.FLOAT16),
            CLAMP_MODES.I_I_FLOAT32_FLOAT32: ClampExecutionPattern(types.DType.FLOAT32),
        }


class GammaCorrectionExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image.",
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                shape=["H", "W", "C"],
                description="Output Image.",
            ),
            dynamic_attribute_pattern={
                GAMMA_CORRECTION.GAMMA_ATTRIBUTE: types.DataType(
                    dtype,
                    types.AType.SCALAR,
                    shape=[],
                    description="Gamma correction parameter.",
                )
            },
        )


class GAMMA_CORRECTION_MODES(OPERATOR_MODES):
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class GAMMA_CORRECTION(Op):
    """
    Change the brightness of an Image by gamma correction,

    [latexmath]
    ++++
    output[y,x,c] = \\left(input[y,x,c]\\right)^{gamma}.
    ++++
    Any operations of this type to be performed on Integer format Images should use the <<TABLE>> operator.
    """

    GAMMA_ATTRIBUTE = "gamma"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[GAMMA_CORRECTION_MODES, ExecutionPattern]:
        return {
            GAMMA_CORRECTION_MODES.I_I_FLOAT16_FLOAT16: GammaCorrectionExecutionPattern(
                types.DType.FLOAT16
            ),
            GAMMA_CORRECTION_MODES.I_I_FLOAT32_FLOAT32: GammaCorrectionExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class LOGICAL_SHIFT_RIGHT_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()


class LOGICAL_SHIFT_RIGHT(Op):
    """
    Perform an elementwise logical right shift on all pixel values,
    [latexmath]
    ++++
    output[y,x,c] = input[y,x,c] \\gg shift.
    ++++
    """

    SHIFT_ATTRIBUTE = "shift"

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[LOGICAL_SHIFT_RIGHT_MODES, ExecutionPattern]:
        return {
            LOGICAL_SHIFT_RIGHT_MODES.I_I_UINT8_UINT8: ShiftExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            LOGICAL_SHIFT_RIGHT_MODES.I_I_UINT16_UINT16: ShiftExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            LOGICAL_SHIFT_RIGHT_MODES.I_I_UINT32_UINT32: ShiftExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
        }


class NOT_MODES(OPERATOR_MODES):
    I_I_BOOL_BOOL = enum.auto()
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()


class NOT(Op):
    """
    Perform a _bitwise_ NOT operation on an Image,
    [latexmath]
    ++++
    output[y,x,c] = ! input[y,x,c]
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[NOT_MODES, ExecutionPattern]:
        return {
            NOT_MODES.I_I_BOOL_BOOL: SimpleUnaryExecutionPattern(
                types.DType.BOOL, types.DType.BOOL
            ),
            NOT_MODES.I_I_UINT8_UINT8: SimpleUnaryExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            NOT_MODES.I_I_UINT16_UINT16: SimpleUnaryExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            NOT_MODES.I_I_UINT32_UINT32: SimpleUnaryExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
        }


class PowerExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image",
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                shape=["H", "W", "C"],
                description="Output Image",
            ),
            dynamic_attribute_pattern={
                POWER.BASE_ATTRIBUTE: types.DataType(
                    dtype, types.AType.SCALAR, shape=[], description="Base parameter"
                )
            },
        )


class POWER_MODES(OPERATOR_MODES):
    # REVISIT(miholt01, M1, Re-introduce the I_I_INT32_INT32 option)
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class POWER(Op):
    """
    Calculate the power of a scalar to an Image,
    [latexmath]
    ++++
    output[y,x,c] = α^{input[y,x,c]}.
    ++++
    In any integer-based mode where the result may exceed the supported range of the output type, the expected behaviour is that the correct value in arbitrary precision wraps to a value supported by the output type.
    """

    BASE_ATTRIBUTE = "base"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[POWER_MODES, ExecutionPattern]:
        return {
            POWER_MODES.I_I_FLOAT16_FLOAT16: PowerExecutionPattern(types.DType.FLOAT16),
            POWER_MODES.I_I_FLOAT32_FLOAT32: PowerExecutionPattern(types.DType.FLOAT32),
        }


class RoundExecutionPattern(SimpleUnaryExecutionPattern):
    def __init__(self, input_dtype):
        super().__init__(input_dtype=input_dtype, output_dtype=types.DType.INT32)

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            ROUND.ROUND_ATTRIBUTE: types.DataType(
                None, ROUND.ROUND_METHOD, description="Selects the rounding method"
            )
        }


class ROUND_MODES(OPERATOR_MODES):
    I_I_FLOAT16_INT32 = enum.auto()
    I_I_FLOAT32_INT32 = enum.auto()


class ROUND(Op):
    ROUND_ATTRIBUTE = "round_method"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[ROUND_MODES, ExecutionPattern]:
        return {
            ROUND_MODES.I_I_FLOAT16_INT32: RoundExecutionPattern(types.DType.FLOAT16),
            ROUND_MODES.I_I_FLOAT32_INT32: RoundExecutionPattern(types.DType.FLOAT32),
        }

    def to_flatbuffer(self):
        raise NotImplementedError()

    class ROUND_METHOD(types.AttributeEnum):
        """
        Enumeration of the ROUND methods
        """

        _init_ = "value __doc__"

        # Directed rounding
        FLOOR = enum.auto(), "Round towards negative infinity/`floor()` function"
        CEIL = enum.auto(), "Round towards positive infinity/`ceil()` function"
        TOWARDS_ZERO = (
            enum.auto(),
            "Round towards zero (or away from infinity)/`trunc()` function",
        )
        AWAY_FROM_ZERO = enum.auto(), "Round away from zero (or towards infinity)"

        # Tie-breaking rounding
        HALF_UP = enum.auto(), "Round half towards positive infinity"
        HALF_DOWN = enum.auto(), "Round half towards negative infinity"
        HALF_TOWARDS_ZERO = enum.auto(), "Round half towards zero"
        HALF_AWAY_FROM_ZERO = (
            enum.auto(),
            "Round half away from zero/`round()` function",
        )
        HALF_TO_EVEN = enum.auto(), "Round half to even"
        HALF_TO_ODD = enum.auto(), "Round half to odd"


ROUND.__doc__ = """
Round a floating-point number to an integer, based on the `round_method` attribute.

%s

""" % "\n".join(
    types.aenum_to_table(ROUND.ROUND_METHOD)
)


class SHIFT_LEFT_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()


class SHIFT_LEFT(Op):
    """
    Perform an elementwise left shift on all pixel values,
    [latexmath]
    ++++
    output[y,x,c] = input[y,x,c] \\ll shift.
    ++++
    """

    SHIFT_ATTRIBUTE = "shift"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[SHIFT_LEFT_MODES, ExecutionPattern]:
        return {
            SHIFT_LEFT_MODES.I_I_UINT8_UINT8: ShiftExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            SHIFT_LEFT_MODES.I_I_UINT16_UINT16: ShiftExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            SHIFT_LEFT_MODES.I_I_UINT32_UINT32: ShiftExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            SHIFT_LEFT_MODES.I_I_INT8_INT8: ShiftExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            SHIFT_LEFT_MODES.I_I_INT16_INT16: ShiftExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            SHIFT_LEFT_MODES.I_I_INT32_INT32: ShiftExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
        }


class SQRT_MODES(OPERATOR_MODES):
    I_I_FLOAT32_FLOAT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()


class SQRT(Op):
    """
    Perform an sqrt operation on an Image,
    [latexmath]
    ++++
    output[y,x,c] = \sqrt{input[y,x,c]}
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[SQRT_MODES, ExecutionPattern]:
        return {
            SQRT_MODES.I_I_FLOAT32_FLOAT32: SimpleUnaryExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
            SQRT_MODES.I_I_FLOAT16_FLOAT16: SimpleUnaryExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
        }
