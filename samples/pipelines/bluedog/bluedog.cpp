
/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "bluedog.h"
#include <complex>

namespace vosa::samples::bluedog {

template <typename T, bool is_float = false>
static std::shared_ptr<vosa::api::Op>
saturate_cast(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<vosa::api::Op> &input) {
  auto image = input;
  if constexpr (is_float)
    image = vosa::api::round(graph_builder, input,
                             vosa::api::RoundBase::HALF_AWAY_FROM_ZERO);

  return vosa::api::cast<T>(
      graph_builder, image,
      vosa::api::CastBase::rounding_mode_t::VOSA_ROUNDING_MODE_SATURATE);
}

std::shared_ptr<vosa::api::Op>
boxFilter(vosa::api::GraphBuilder &graph_builder,
          const std::shared_ptr<vosa::api::Op> &image,
          const uint32_t kernel_size,
          vosa::api::PadBase::pad_mode_t pad_mode =
              vosa::api::PadBase::pad_mode_t::VOSA_PAD_MODE_REFLECT) {
  const uint32_t padding = kernel_size / 2;

  vosa::api::EigenPlane<float> kernel_w(1, kernel_size),
      kernel_h(kernel_size, 1);
  kernel_w.setConstant(1.0f / (float)kernel_size);
  kernel_h.setConstant(1.0f / (float)kernel_size);

  auto casted = vosa::api::cast<float>(graph_builder, image);

  // Horizontal Convolution
  auto padded_w = vosa::api::pad(graph_builder, casted, 0, pad_mode,
                                 {0, 0, padding, padding});
  auto conv_w = vosa::api::conv(graph_builder, padded_w, kernel_w);

  // Vertical Convolution
  auto padded_h = vosa::api::pad(graph_builder, conv_w, 0, pad_mode,
                                 {padding, padding, 0, 0});
  auto conv_h = vosa::api::conv(graph_builder, padded_h, kernel_h);

  return saturate_cast<uint8_t, true>(graph_builder, conv_h);
}

std::vector<std::shared_ptr<vosa::api::Op>>
split(vosa::api::GraphBuilder &graph_builder,
      const std::shared_ptr<vosa::api::Op> &image,
      const uint32_t num_channels) {
  std::vector<std::shared_ptr<vosa::api::Op>> channels(num_channels);
  for (uint32_t c = 0; c < channels.size(); c++)
    channels[c] = vosa::api::channel_extract(graph_builder, image, c);

  return channels;
}

std::shared_ptr<vosa::api::Op> demosaicing_BayerBG2BGR(
    vosa::api::GraphBuilder &graph_builder,
    const std::shared_ptr<vosa::api::Op> &bayer,
    const std::vector<std::shared_ptr<vosa::api::Op>> &expansion_kernels) {

  // Create expansion & gather kernels
  vosa::api::EigenPlane<uint8_t> gather_kernel(1, 1);
  gather_kernel.setConstant(1);

  // Extract channels: DECIMATE over a 2x2 window, then EXPAND back to full size
  // (and fill with 0's)
  const std::vector<uint32_t> decimate_window = {2, 2};
  auto R = vosa::api::expand(
      graph_builder,
      vosa::api::decimate(graph_builder, bayer, decimate_window, {0, 0}),
      expansion_kernels[0], (uint8_t)0, gather_kernel);
  auto G1 = vosa::api::expand(
      graph_builder,
      vosa::api::decimate(graph_builder, bayer, decimate_window, {0, 1}),
      expansion_kernels[1], (uint8_t)0, gather_kernel);
  auto G2 = vosa::api::expand(
      graph_builder, decimate(graph_builder, bayer, decimate_window, {1, 0}),
      expansion_kernels[2], (uint8_t)0, gather_kernel);
  auto B = vosa::api::expand(
      graph_builder,
      vosa::api::decimate(graph_builder, bayer, decimate_window, {1, 1}),
      expansion_kernels[3], (uint8_t)0, gather_kernel);

  // Gather all green pixels on the same plane
  auto G = vosa::api::add(graph_builder, G1, G2);

  // Convolution kernels
  vosa::api::EigenPlane<uint16_t> G_kernel(3, 3), RB_kernel(3, 3);
  G_kernel.setValues({{0, 1, 0}, {1, 4, 1}, {0, 1, 0}});
  RB_kernel.setValues({{1, 2, 1}, {2, 4, 2}, {1, 2, 1}});
  assert(G_kernel.dimension(0) == G_kernel.dimension(1) &&
         RB_kernel.dimension(0) == RB_kernel.dimension(1) &&
         G_kernel.dimension(0) == RB_kernel.dimension(0));
  const uint32_t padding = G_kernel.dimension(0) / 2;

  // Convolve each channel with the corresponding kernel
  auto R_channel = vosa::api::conv(
      graph_builder, vosa::api::cast<uint16_t>(graph_builder, R), RB_kernel);
  auto G_channel = vosa::api::conv(
      graph_builder, vosa::api::cast<uint16_t>(graph_builder, G), G_kernel);
  auto B_channel = vosa::api::conv(
      graph_builder, vosa::api::cast<uint16_t>(graph_builder, B), RB_kernel);
  auto image_bgr_concat =
      vosa::api::concat(graph_builder, {B_channel, G_channel, R_channel});

  const std::vector<uint32_t> post_conv_shape = {
      CONSTANTS::input_image_shape_hwc[0] - padding * 2,
      CONSTANTS::input_image_shape_hwc[1] - padding * 2, 3};
  auto image_bgr =
      vosa::api::add(graph_builder, image_bgr_concat,
                     vosa::api::const_image(graph_builder, post_conv_shape,
                                            (uint16_t)2)); // acts like a 0.5f

  // Normalise values and cast
  auto image_bgr_normalised = saturate_cast<uint8_t>(
      graph_builder,
      vosa::api::logical_shift_right(graph_builder, image_bgr, 2));

  // Copy over first and last rows and columns
  return vosa::api::pad(graph_builder, image_bgr_normalised, 0,
                        vosa::api::PadBase::VOSA_PAD_MODE_REPLICATE,
                        {padding, padding, padding, padding});
}

std::shared_ptr<vosa::api::Op>
run_inference(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<vosa::api::Op> &image,
              const std::string &tosa_mlir_path) {
  return vosa::api::tosa_graph(graph_builder, {image}, tosa_mlir_path, "main",
                               CONSTANTS::tosa_output_shape_hwc);
}

std::shared_ptr<vosa::api::Op>
cvtColor_BGR2GRAY(vosa::api::GraphBuilder &graph_builder,
                  const std::shared_ptr<vosa::api::Op> &image) {

  auto image_casted = vosa::api::cast<uint32_t>(graph_builder, image);
  vosa::api::EigenPlane<uint32_t> bgr_to_grayscale_matrix(1, 3);
  vosa::api::EigenVector<uint32_t> inner_offsets(3), outer_offsets(1);
  bgr_to_grayscale_matrix.setValues({{3735, 19235, 9798}}); // B G R order
  inner_offsets.setZero();
  outer_offsets.setConstant(16384);

  auto grayscale_unnormalised = vosa::api::pointwise_matrix_multiply(
      graph_builder, image_casted, bgr_to_grayscale_matrix, inner_offsets,
      outer_offsets);
  auto grayscale_normalised =
      vosa::api::logical_shift_right(graph_builder, grayscale_unnormalised, 15);
  return saturate_cast<uint8_t>(graph_builder, grayscale_normalised);
}

std::shared_ptr<vosa::api::Op>
compare_gt(vosa::api::GraphBuilder &graph_builder,
           const std::shared_ptr<vosa::api::Op> &image, float threshold,
           const std::vector<uint32_t> &shape) {

  auto mask = vosa::api::greater(
      graph_builder, image,
      vosa::api::const_image(graph_builder, shape, threshold));
  auto mask_uint8 = vosa::api::cast<uint8_t>(
      graph_builder, mask); // values will be either 0 or 1
  return vosa::api::mult(
      graph_builder, mask_uint8,
      vosa::api::const_image(graph_builder, shape,
                             (uint8_t)255)); // values will be either 0 or 255
}

static std::shared_ptr<vosa::api::Op>
compare_ge(vosa::api::GraphBuilder &graph_builder,
           const std::shared_ptr<vosa::api::Op> &image, float threshold,
           const std::vector<uint32_t> &shape) {

  auto mask = vosa::api::greater_equal(
      graph_builder, image,
      vosa::api::const_image(graph_builder, shape, threshold));
  auto mask_uint8 = vosa::api::cast<uint8_t>(
      graph_builder, mask); // values will be either 0 or 1
  return vosa::api::mult(
      graph_builder, mask_uint8,
      vosa::api::const_image(graph_builder, shape,
                             (uint8_t)255)); // values will be either 0 or 255
}

static std::shared_ptr<vosa::api::Op>
safe_add(vosa::api::GraphBuilder &graph_builder,
         const std::shared_ptr<vosa::api::Op> &image_1,
         const std::shared_ptr<vosa::api::Op> &image_2) {
  auto image_1_casted = vosa::api::cast<uint16_t>(graph_builder, image_1);
  auto image_2_casted = vosa::api::cast<uint16_t>(graph_builder, image_2);

  auto added = vosa::api::add(graph_builder, image_1_casted, image_2_casted);
  return saturate_cast<uint8_t>(graph_builder, added);
}

static std::shared_ptr<vosa::api::Op>
safe_mult(vosa::api::GraphBuilder &graph_builder,
          const std::shared_ptr<vosa::api::Op> &image_1,
          const std::shared_ptr<vosa::api::Op> &image_2) {
  auto image_1_casted = vosa::api::cast<uint16_t>(graph_builder, image_1);
  auto image_2_casted = vosa::api::cast<uint16_t>(graph_builder, image_2);

  auto multiplied =
      vosa::api::mult(graph_builder, image_1_casted, image_2_casted);
  return saturate_cast<uint8_t>(graph_builder, multiplied);
}

std::shared_ptr<vosa::api::Op>
postprocessing(vosa::api::GraphBuilder &graph_builder,
               const std::shared_ptr<vosa::api::Op> &image_bgr,
               const std::shared_ptr<vosa::api::Op> &logits) {
  // BGR --> Grayscale
  auto grayscale_image = cvtColor_BGR2GRAY(graph_builder, image_bgr);

  // Grayscale --> BGR
  auto bgr_grayscale = vosa::api::broadcast_channelwise(
      graph_builder, grayscale_image, CONSTANTS::output_image_shape_hwc[2]);

  // Extract the blue channel
  auto bgr_grayscale_channels =
      split(graph_builder, bgr_grayscale, CONSTANTS::output_image_shape_hwc[2]);
  auto blue_channel = bgr_grayscale_channels[2];

  // Convert NN logits to probabilities
  auto exp = vosa::api::power(graph_builder, logits, (float)std::exp(1.0f));

  // Note: This is actually REDUCE_SUM_CHANNELWISE
  std::vector<std::shared_ptr<vosa::api::Op>> exp_channels =
      split(graph_builder, exp, CONSTANTS::tosa_output_shape_hwc[2]);
  for (uint32_t c = 1; c < exp_channels.size(); c++)
    exp_channels[0] =
        vosa::api::add(graph_builder, exp_channels[0], exp_channels[c]);

  auto scale_factors = vosa::api::broadcast_channelwise(
      graph_builder, exp_channels[0], CONSTANTS::tosa_output_shape_hwc[2]);
  auto probabilities = vosa::api::div(graph_builder, exp, scale_factors);

  // Extract probabilities for desired class
  auto class_of_interest = vosa::api::channel_extract(
      graph_builder, probabilities, CONSTANTS::class_index);

  // Apply lower-bound threshold over 0.5f
  const std::vector<uint32_t> downscaled_shape_hw1 = {
      CONSTANTS::tosa_output_shape_hwc[0], CONSTANTS::tosa_output_shape_hwc[0],
      1};
  auto mask =
      compare_gt(graph_builder, class_of_interest, 0.5f, downscaled_shape_hw1);

  // Apply mask to the blue channel
  const std::vector<uint32_t> image_shape_hw1 = {
      CONSTANTS::input_image_shape_hwc[0], CONSTANTS::input_image_shape_hwc[1],
      1};
  const std::vector<uint32_t> image_shape_hw = {
      CONSTANTS::input_image_shape_hwc[0], CONSTANTS::input_image_shape_hwc[1]};
  auto mask_resized_float = vosa::api::resize_bilinear(
      graph_builder, vosa::api::cast<float>(graph_builder, mask),
      image_shape_hw);
  auto mask_resized =
      compare_ge(graph_builder, mask_resized_float, 128.0f, image_shape_hw1);
  auto mask_shifted = safe_add(
      graph_builder, mask_resized,
      vosa::api::const_image(graph_builder, image_shape_hw1, (uint8_t)1));
  auto blue_channel_modified =
      safe_mult(graph_builder, blue_channel, mask_shifted);

  // Recreate BGR image
  bgr_grayscale_channels[2] = blue_channel_modified;
  return vosa::api::concat(graph_builder, bgr_grayscale_channels);
}

std::vector<std::shared_ptr<vosa::api::Op>>
preprocessing(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<vosa::api::Op> &image) {

  vosa::api::EigenImage<uint8_t> top_left(2, 2, 1), top_right(2, 2, 1),
      bottom_left(2, 2, 1), bottom_right(2, 2, 1);
  top_left.setZero();
  top_left(0, 0, 0) = 1;
  top_right.setZero();
  top_right(0, 1, 0) = 1;
  bottom_left.setZero();
  bottom_left(1, 0, 0) = 1;
  bottom_right.setZero();
  bottom_right(1, 1, 0) = 1;

  std::vector<std::shared_ptr<vosa::api::Op>> expansion_kernels_ops = {
      vosa::api::const_image(graph_builder, top_left),
      vosa::api::const_image(graph_builder, top_right),
      vosa::api::const_image(graph_builder, bottom_left),
      vosa::api::const_image(graph_builder, bottom_right)};

  // De-Bayer
  auto image_bgr =
      demosaicing_BayerBG2BGR(graph_builder, image, expansion_kernels_ops);

  // Blue and resize
  auto blurred = boxFilter(graph_builder, image_bgr, 5);
  auto resized = vosa::api::resize_bilinear(
      graph_builder, vosa::api::cast<float>(graph_builder, blurred),
      {vosa::samples::bluedog::CONSTANTS::tosa_input_shape_hwc[0],
       vosa::samples::bluedog::CONSTANTS::tosa_input_shape_hwc[1]});

  // Scale to [-1, 1]
  auto rescaled = vosa::api::div(
      graph_builder, resized,
      vosa::api::const_image(graph_builder, CONSTANTS::tosa_input_shape_hwc,
                             127.5f));
  auto shifted =
      vosa::api::sub(graph_builder, rescaled,
                     vosa::api::const_image(
                         graph_builder, CONSTANTS::tosa_input_shape_hwc, 1.0f));

  // cvtColor: BGR --> RGB
  auto channels =
      split(graph_builder, shifted, CONSTANTS::tosa_input_shape_hwc[2]);
  std::reverse(channels.begin(), channels.end()); // reorder B, G, R -> R, G, B
  auto preprocessed_image = vosa::api::concat(graph_builder, channels);

  return {image_bgr, preprocessed_image};
}

std::vector<std::shared_ptr<vosa::api::Op>>
make_test_graph(vosa::api::GraphBuilder &graph_builder) {

  auto image = vosa::api::input(
      graph_builder, vosa::api::ArrayBase::DType::UINT8,
      {CONSTANTS::input_image_shape_hwc[0], CONSTANTS::input_image_shape_hwc[1],
       CONSTANTS::input_image_shape_hwc[2]});
  std::vector<std::shared_ptr<vosa::api::Op>> preprocessing_results =
      preprocessing(graph_builder, image);
  std::shared_ptr<vosa::api::Op> logits =
      run_inference(graph_builder, preprocessing_results[1],
                    "lite-model_deeplabv3_1_metadata_2.mlir");
  std::shared_ptr<vosa::api::Op> image_with_mask =
      postprocessing(graph_builder, preprocessing_results[0], logits);
  //    return preprocessing_results;
  return {image_with_mask};
}

} // namespace vosa::samples::bluedog
