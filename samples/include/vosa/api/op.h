
/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_PIPELINES_API_OP_H_
#define VOSA_PIPELINES_API_OP_H_

#include "vosa/api/array_types.h"

namespace vosa::api {

class Op {
private:
  vosa::api::ArrayBase::DType dtype;

public:
  explicit Op(ArrayBase::DType dtype) : dtype(dtype) {}
  [[nodiscard]] ArrayBase::DType get_dtype() const { return dtype; }
};

class CastBase {
public:
  enum class rounding_mode_t {
    VOSA_ROUNDING_MODE_SATURATE,
    VOSA_ROUNDING_MODE_WRAP
  };
};

class PadBase {
public:
  enum pad_mode_t {
    VOSA_PAD_MODE_CONSTANT,
    VOSA_PAD_MODE_REPLICATE,
    VOSA_PAD_MODE_REFLECT,
    VOSA_PAD_MODE_MIRROR
  };
};

class RoundBase {
public:
  enum round_method_t {
    FLOOR,
    CEIL,
    TOWARDS_ZERO,
    AWAY_FROM_ZERO,
    HALF_UP,
    HALF_DOWN,
    HALF_TOWARDS_ZERO,
    HALF_AWAY_FROM_ZERO, // default C++ round() function
    HALF_TO_EVEN,
    HALF_TO_ODD
  };
};

} // namespace vosa::api

#endif // VOSA_PIPELINES_API_OP_H_
