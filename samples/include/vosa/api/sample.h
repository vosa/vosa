/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_SAMPLE_H
#define VOSA_SAMPLE_H

#include <string>

#include "vosa/Compiler/vosa_compiler.h"
#include "vosa/api/graph_builder.h"
#include "llvm/Support/CommandLine.h"

static llvm::cl::opt<std::string>
    PipelineName(llvm::cl::Positional, llvm::cl::desc("<Pipeline Name>"),
                 llvm::cl::Required,
                 llvm::cl::cat(vosa::Compiler::get_compiler_category()));

static llvm::cl::opt<std::string>
    VMFBFilename(llvm::cl::Positional, llvm::cl::desc("<Output VMFB Filename>"),
                 llvm::cl::Required,
                 llvm::cl::cat(vosa::Compiler::get_compiler_category()));

namespace vosa::api::sample {

template <typename T>
class SampleBuilder {

public:
  virtual ~SampleBuilder() = default;
  virtual int run(const std::string &pipeline_name,
                  const std::string &vmfb_file) = 0;
};

} // namespace vosa::api::sample

#define VOSA_SAMPLE(SampleBuilderClass)                                        \
  int main(int argc, char **argv) {                                            \
    llvm::cl::HideUnrelatedOptions(vosa::Compiler::get_compiler_category());   \
    llvm::cl::ParseCommandLineOptions(argc, argv);                             \
    auto builder = SampleBuilderClass();                                       \
    return builder.run(PipelineName, VMFBFilename);                            \
  }

#endif // VOSA_SAMPLE_H
