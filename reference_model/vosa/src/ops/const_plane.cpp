/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "const_plane.h"

std::shared_ptr<vosa::Op> vosa::ConstPlaneBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_const_plane = vosa_operator_union.AsCONST_PLANE();
  assert(serialized_const_plane);
  auto mode = serialized_const_plane->mode;
  auto serialized_attributes = &serialized_const_plane->attr;
  std::vector<uint8_t> raw_value = serialized_attributes->get()->value;
  std::vector<uint32_t> dimensions = serialized_attributes->get()->dimension;
  switch (mode) {
  case vosa_serialization::CONST_PLANE_MODE_P_UINT8: {
    return std::make_shared<vosa::ConstPlane<uint8_t>>(
        dimensions, unpack_value<uint8_t>(raw_value));
  }
  case vosa_serialization::CONST_PLANE_MODE_P_UINT16: {
    return std::make_shared<vosa::ConstPlane<uint16_t>>(
        dimensions, unpack_value<uint16_t>(raw_value));
  }
  case vosa_serialization::CONST_PLANE_MODE_P_UINT32: {
    return std::make_shared<vosa::ConstPlane<uint32_t>>(
        dimensions, unpack_value<uint32_t>(raw_value));
  }
  case vosa_serialization::CONST_PLANE_MODE_P_INT8: {
    return std::make_shared<vosa::ConstPlane<int8_t>>(
        dimensions, unpack_value<int8_t>(raw_value));
  }
  case vosa_serialization::CONST_PLANE_MODE_P_INT16: {
    return std::make_shared<vosa::ConstPlane<int16_t>>(
        dimensions, unpack_value<int16_t>(raw_value));
  }
  case vosa_serialization::CONST_PLANE_MODE_P_INT32: {
    return std::make_shared<vosa::ConstPlane<int32_t>>(
        dimensions, unpack_value<int32_t>(raw_value));
  }
  case vosa_serialization::CONST_PLANE_MODE_P_FLOAT32: {
    return std::make_shared<vosa::ConstPlane<float>>(
        dimensions, unpack_value<float>(raw_value));
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::ArrayBase::DType vosa::ConstPlane<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::ConstPlane<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &) const {
  vosa::EigenPlane<T> plane{dimensions[0], dimensions[1]};
  plane.setConstant(value);
  return std::make_shared<vosa::ArrayBase>(plane);
}

template <>
vosa_serialization::CONST_PLANE_MODE
vosa::ConstPlane<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::CONST_PLANE_MODE_P_UINT8;
}
template <>
vosa_serialization::CONST_PLANE_MODE
vosa::ConstPlane<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::CONST_PLANE_MODE_P_UINT16;
}

template <>
vosa_serialization::CONST_PLANE_MODE
vosa::ConstPlane<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::CONST_PLANE_MODE_P_UINT32;
}

template <>
vosa_serialization::CONST_PLANE_MODE
vosa::ConstPlane<int8_t>::get_serialization_mode() const {
  return vosa_serialization::CONST_PLANE_MODE_P_INT8;
}

template <>
vosa_serialization::CONST_PLANE_MODE
vosa::ConstPlane<int16_t>::get_serialization_mode() const {
  return vosa_serialization::CONST_PLANE_MODE_P_INT16;
}

template <>
vosa_serialization::CONST_PLANE_MODE
vosa::ConstPlane<int32_t>::get_serialization_mode() const {
  return vosa_serialization::CONST_PLANE_MODE_P_INT32;
}

template <>
vosa_serialization::CONST_PLANE_MODE
vosa::ConstPlane<float>::get_serialization_mode() const {
  return vosa_serialization::CONST_PLANE_MODE_P_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::ConstPlane<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  auto const_plane_attribute =
      std::make_unique<vosa_serialization::CONST_PLANE_ATTRIBUTET>();
  std::vector<uint8_t> value_byte_vector;
  value_byte_vector.insert(value_byte_vector.end(), (uint8_t *)&value,
                           ((uint8_t *)&value) + sizeof(T));
  const_plane_attribute->value = value_byte_vector;
  const_plane_attribute->dimension = dimensions;

  vosa_serialization::CONST_PLANET const_plane;
  const_plane.mode = get_serialization_mode();
  const_plane.attr = std::move(const_plane_attribute);

  auto const_plane_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  const_plane_operator->op.Set(std::move(const_plane));
  const_plane_operator->name = op_name;
  const_plane_operator->inputs = input_names;
  return const_plane_operator;
}

template class vosa::ConstPlane<uint8_t>;

template class vosa::ConstPlane<uint16_t>;

template class vosa::ConstPlane<uint32_t>;

template class vosa::ConstPlane<int8_t>;

template class vosa::ConstPlane<int16_t>;

template class vosa::ConstPlane<int32_t>;

template class vosa::ConstPlane<float>;