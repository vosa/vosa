/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "reduce_sum_channelwise.h"

using namespace std;
using namespace vosa;
using namespace vosa_serialization;

shared_ptr<Op> ReduceSumChannelwiseBase::parse_serialized(
    const VosaOperatorUnion &vosa_operator_union) {
  auto serialized_reduce_sum_channelwise =
      vosa_operator_union.AsREDUCE_SUM_CHANNELWISE();
  assert(serialized_reduce_sum_channelwise);

  switch (serialized_reduce_sum_channelwise->mode) {
  case REDUCE_SUM_CHANNELWISE_MODE_I_I_UINT8_UINT8:
    return make_shared<ReduceSumChannelwise<uint8_t, uint8_t>>();
  case REDUCE_SUM_CHANNELWISE_MODE_I_I_UINT16_UINT16:
    return make_shared<ReduceSumChannelwise<uint16_t, uint16_t>>();
  case REDUCE_SUM_CHANNELWISE_MODE_I_I_UINT32_UINT32:
    return make_shared<ReduceSumChannelwise<uint32_t, uint32_t>>();
  case REDUCE_SUM_CHANNELWISE_MODE_I_I_INT8_INT8:
    return make_shared<ReduceSumChannelwise<int8_t, int8_t>>();
  case REDUCE_SUM_CHANNELWISE_MODE_I_I_INT16_INT16:
    return make_shared<ReduceSumChannelwise<int16_t, int16_t>>();
  case REDUCE_SUM_CHANNELWISE_MODE_I_I_INT32_INT32:
    return make_shared<ReduceSumChannelwise<int32_t, int32_t>>();
  case REDUCE_SUM_CHANNELWISE_MODE_I_I_FLOAT32_FLOAT32:
    return make_shared<ReduceSumChannelwise<float, float>>();
  default:
    throw OperatorModeNotImplementedError();
  }
}

template <typename I, typename O>
shared_ptr<ArrayBase> ReduceSumChannelwise<I, O>::eval(
    const vector<shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<I> &input = inputs[0]->template get_image<I>();

  return make_shared<ArrayBase>(this->runme(input));
}

template <typename I, typename O>
ArrayBase::DType ReduceSumChannelwise<I, O>::output_dtype() const {
  return output_dtype_helper<O>();
}

template <typename I, typename O>
EigenImage<O>
ReduceSumChannelwise<I, O>::runme(const EigenImage<I> &input) const {

  int height = input.dimension(0);
  int width = input.dimension(1);
  int channels = input.dimension(2);

  EigenImage<O> output(height, width, 1);

  for (int height_index = 0; height_index < height; height_index++)
    for (int width_index = 0; width_index < width; width_index++)
      output(height_index, width_index, 0) = 0;

  for (int height_index = 0; height_index < height; height_index++)
    for (int width_index = 0; width_index < width; width_index++)
      for (int c = 0; c < channels; c++)
        output(height_index, width_index, 0) +=
            input(height_index, width_index, c);

  return output;
}

template <typename I, typename O>
unique_ptr<VosaGraphOperatorT>
ReduceSumChannelwise<I, O>::serialize(const string &op_name,
                                      const vector<string> &input_names) const {
  REDUCE_SUM_CHANNELWISET reduce_sum_channelwise;
  reduce_sum_channelwise.mode = get_serialization_mode();

  auto reduce_sum_channelwise_operator = make_unique<VosaGraphOperatorT>();

  reduce_sum_channelwise_operator->op.Set(reduce_sum_channelwise);
  reduce_sum_channelwise_operator->name = op_name;
  reduce_sum_channelwise_operator->inputs = input_names;

  return reduce_sum_channelwise_operator;
}

template <>
REDUCE_SUM_CHANNELWISE_MODE
ReduceSumChannelwise<uint8_t, uint8_t>::get_serialization_mode() const {
  return REDUCE_SUM_CHANNELWISE_MODE_I_I_UINT8_UINT8;
}

template <>
REDUCE_SUM_CHANNELWISE_MODE
ReduceSumChannelwise<uint16_t, uint16_t>::get_serialization_mode() const {
  return REDUCE_SUM_CHANNELWISE_MODE_I_I_UINT16_UINT16;
}

template <>
REDUCE_SUM_CHANNELWISE_MODE
ReduceSumChannelwise<uint32_t, uint32_t>::get_serialization_mode() const {
  return REDUCE_SUM_CHANNELWISE_MODE_I_I_UINT32_UINT32;
}

template <>
REDUCE_SUM_CHANNELWISE_MODE
ReduceSumChannelwise<int8_t, int8_t>::get_serialization_mode() const {
  return REDUCE_SUM_CHANNELWISE_MODE_I_I_INT8_INT8;
}

template <>
REDUCE_SUM_CHANNELWISE_MODE
ReduceSumChannelwise<int16_t, int16_t>::get_serialization_mode() const {
  return REDUCE_SUM_CHANNELWISE_MODE_I_I_INT16_INT16;
}

template <>
REDUCE_SUM_CHANNELWISE_MODE
ReduceSumChannelwise<int32_t, int32_t>::get_serialization_mode() const {
  return REDUCE_SUM_CHANNELWISE_MODE_I_I_INT32_INT32;
}

template <>
REDUCE_SUM_CHANNELWISE_MODE
ReduceSumChannelwise<float, float>::get_serialization_mode() const {
  return REDUCE_SUM_CHANNELWISE_MODE_I_I_FLOAT32_FLOAT32;
}

// Template instantiation
template class vosa::ReduceSumChannelwise<uint8_t, uint8_t>;

template class vosa::ReduceSumChannelwise<uint16_t, uint16_t>;

template class vosa::ReduceSumChannelwise<uint32_t, uint32_t>;

template class vosa::ReduceSumChannelwise<int8_t, int8_t>;

template class vosa::ReduceSumChannelwise<int16_t, int16_t>;

template class vosa::ReduceSumChannelwise<int32_t, int32_t>;

template class vosa::ReduceSumChannelwise<float, float>;
