/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "div.h"

std::shared_ptr<vosa::Op> vosa::DivBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_div = vosa_operator_union.AsDIV();
  assert(serialized_div);
  auto mode = serialized_div->mode;
  switch (mode) {
  case vosa_serialization::DIV_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::Div<float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::EigenImage<T>
vosa::Div<T>::runme(const vosa::EigenImage<T> &input_1,
                    const vosa::EigenImage<T> &input_2) const {

  uint32_t height_1 = input_1.dimension(0);
  uint32_t width_1 = input_1.dimension(1);
  uint32_t num_channels_1 = input_1.dimension(2);

  uint32_t height_2 = input_2.dimension(0);
  uint32_t width_2 = input_2.dimension(1);
  uint32_t num_channels_2 = input_2.dimension(2);

  assert(width_1 == width_2);
  assert(height_1 == height_2);
  assert(num_channels_1 == num_channels_2);

  vosa::EigenImage<T> output{height_1, width_1, num_channels_1};

  for (uint32_t height_index = 0; height_index < height_1; height_index++) {
    for (uint32_t width_index = 0; width_index < width_1; width_index++) {
      for (uint32_t channel_index = 0; channel_index < num_channels_1;
           channel_index++) {
        T input_1_value = input_1(height_index, width_index, channel_index);
        T input_2_value = input_2(height_index, width_index, channel_index);
        T output_value = input_1_value / input_2_value;
        output(height_index, width_index, channel_index) = output_value;
      }
    }
  }
  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::Div<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::Div<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input_1 = inputs[0]->template get_image<T>();
  const EigenImage<T> &input_2 = inputs[1]->template get_image<T>();
  auto typed_output = this->runme(input_1, input_2);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::DIV_MODE vosa::Div<float>::get_serialization_mode() const {
  return vosa_serialization::DIV_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Div<T>::serialize(const std::string &op_name,
                        const std::vector<std::string> &input_names) const {

  vosa_serialization::DIVT div;
  div.mode = get_serialization_mode();

  auto div_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  div_operator->op.Set(div);
  div_operator->name = op_name;
  div_operator->inputs = input_names;
  return div_operator;
}

template class vosa::Div<float>;