/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_CONST_PLANE_H
#define VOSA_CONST_PLANE_H

#include "vosa/op.h"

namespace vosa {

class ConstPlaneBase : public Op {
public:
  static std::shared_ptr<Op>
  parse_serialized(const vosa_serialization::VosaOperatorUnion &);
};

template <typename T>
class ConstPlane : public ConstPlaneBase {
public:
  ConstPlane(std::vector<uint32_t> dimensions, T value)
      : dimensions(std::move(dimensions)), value(value){};

  [[nodiscard]] ArrayBase::DType output_dtype() const final;

  [[nodiscard]] std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &) const final;

  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

private:
  std::vector<uint32_t> dimensions;
  T value;

  [[nodiscard]] vosa_serialization::CONST_PLANE_MODE
  get_serialization_mode() const;
};

} // namespace vosa

#endif // VOSA_CONST_PLANE_H
