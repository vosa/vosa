// RUN: vosa-opt -split-input-file -verify-diagnostics %s | FileCheck %s

module {
    // CHECK-LABEL: func.func @abs()
    func.func @abs() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // CHECK: %{{.*}} = vosa.abs %{{.*}} : tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>
        %res0 = vosa.abs %t0_si8 : tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>

        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        // CHECK: %{{.*}} = vosa.abs %{{.*}} : tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>
        %res1 = vosa.abs %t0_si16 : tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>

        %t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        // CHECK: %{{.*}} = vosa.abs %{{.*}} : tensor<100x200x3xf32> -> tensor<100x200x3xf32>
        %res2 = vosa.abs %t0_f32 : tensor<100x200x3xf32> -> tensor<100x200x3xf32>

        return
    }
}

// -----

module {
    func.func @abs_type_mismatch() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // expected-error @below {{'vosa.abs' op output type does not match input}}
        %res = vosa.abs %t0_si8 : tensor<100x200x3xsi8> -> tensor<100x200x3xsi32>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @arithmetic_shift_right()
    func.func @arithmetic_shift_right() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // CHECK: %{{.*}} = vosa.arithmetic_shift_right %{{.*}} : tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>
        %res0 = vosa.arithmetic_shift_right %t0_si8 { shift = 2 : i32 } : tensor<100x200x3xsi8> -> tensor<100x200x3xsi8>

        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        // CHECK: %{{.*}} = vosa.arithmetic_shift_right %{{.*}} : tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>
        %res1 = vosa.arithmetic_shift_right %t0_si16 { shift = 2 : i32 } : tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>

        %t0_si32 = vosa.constant dense<42> : tensor<100x200x3xsi32>
        // CHECK: %{{.*}} = vosa.arithmetic_shift_right %{{.*}} : tensor<100x200x3xsi32> -> tensor<100x200x3xsi32>
        %res2 = vosa.arithmetic_shift_right %t0_si32 { shift = 2 : i32 } : tensor<100x200x3xsi32> -> tensor<100x200x3xsi32>

        return
    }
}

// -----

module {
    func.func @arithmetic_shift_right_invalid_float() {
        %t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        // expected-error @below {{'vosa.arithmetic_shift_right' op operand #0 must be 3D tensor of 8-bit signed integer or 16-bit signed integer or 32-bit signed integer values, but got 'tensor<100x200x3xf16>'}}
        %res = vosa.arithmetic_shift_right %t0_f16 { shift = 2 : i32 } : tensor<100x200x3xf16> -> tensor<100x200x3xf16>
        return
    }
}

// -----

module {
    func.func @arithmetic_shift_right_invalid_ui() {
        %t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        // expected-error @below {{'vosa.arithmetic_shift_right' op operand #0 must be 3D tensor of 8-bit signed integer or 16-bit signed integer or 32-bit signed integer values, but got 'tensor<100x200x3xui16>'}}
        %res = vosa.arithmetic_shift_right %t0_ui16 { shift = 2 : i32 } : tensor<100x200x3xui16> -> tensor<100x200x3xui16>
        return
    }
}

// -----

module {
    func.func @arithmetic_shift_right_type_mismatch() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        // expected-error @below {{'vosa.arithmetic_shift_right' op output type does not match input}}
        %res = vosa.arithmetic_shift_right %t0_si8 { shift = 2 : i32 } : tensor<100x200x3xsi8> -> tensor<100x200x3xsi32>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @logical_shift_right()
    func.func @logical_shift_right() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        // CHECK: %{{.*}} = vosa.logical_shift_right %{{.*}} : tensor<100x200x3xui8> -> tensor<100x200x3xui8>
        %res0 = vosa.logical_shift_right %t0_ui8 { shift = 2 : i32 } : tensor<100x200x3xui8> -> tensor<100x200x3xui8>

        %t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        // CHECK: %{{.*}} = vosa.logical_shift_right %{{.*}} : tensor<100x200x3xui16> -> tensor<100x200x3xui16>
        %res1 = vosa.logical_shift_right %t0_ui16 { shift = 2 : i32 } : tensor<100x200x3xui16> -> tensor<100x200x3xui16>

        %t0_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        // CHECK: %{{.*}} = vosa.logical_shift_right %{{.*}} : tensor<100x200x3xui32> -> tensor<100x200x3xui32>
        %res2 = vosa.logical_shift_right %t0_ui32 { shift = 2 : i32 } : tensor<100x200x3xui32> -> tensor<100x200x3xui32>

        return
    }
}

// -----

module {
    func.func @logical_shift_right_invalid_float() {
        %t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        // expected-error @below {{'vosa.logical_shift_right' op operand #0 must be 3D tensor of 8-bit unsigned integer or 16-bit unsigned integer or 32-bit unsigned integer values, but got 'tensor<100x200x3xf16>'}}
        %res = vosa.logical_shift_right %t0_f16 { shift = 2 : i32 } : tensor<100x200x3xf16> -> tensor<100x200x3xf16>
        return
    }
}

// -----

module {
    func.func @logical_shift_right_invalid_si() {
        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        // expected-error @below {{'vosa.logical_shift_right' op operand #0 must be 3D tensor of 8-bit unsigned integer or 16-bit unsigned integer or 32-bit unsigned integer values, but got 'tensor<100x200x3xsi16>'}}
        %res = vosa.logical_shift_right %t0_si16 { shift = 2 : i32 } : tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>
        return
    }
}

// -----

module {
    func.func @logical_shift_right_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        // expected-error @below {{'vosa.logical_shift_right' op output type does not match input}}
        %res = vosa.logical_shift_right %t0_ui8 { shift = 2 : i32 } : tensor<100x200x3xui8> -> tensor<100x200x3xui32>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @logical_shift_right()
    func.func @logical_shift_right() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        // CHECK: %{{.*}} = vosa.logical_shift_right %{{.*}} : tensor<100x200x3xui8> -> tensor<100x200x3xui8>
        %res0 = vosa.logical_shift_right %t0_ui8 { shift = 2 : i32 } : tensor<100x200x3xui8> -> tensor<100x200x3xui8>

        %t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        // CHECK: %{{.*}} = vosa.logical_shift_right %{{.*}} : tensor<100x200x3xui16> -> tensor<100x200x3xui16>
        %res1 = vosa.logical_shift_right %t0_ui16 { shift = 2 : i32 } : tensor<100x200x3xui16> -> tensor<100x200x3xui16>

        %t0_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        // CHECK: %{{.*}} = vosa.logical_shift_right %{{.*}} : tensor<100x200x3xui32> -> tensor<100x200x3xui32>
        %res2 = vosa.logical_shift_right %t0_ui32 { shift = 2 : i32 } : tensor<100x200x3xui32> -> tensor<100x200x3xui32>

        return
    }
}

// -----

module {
    func.func @logical_shift_right_invalid_float() {
        %t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
        // expected-error @below {{'vosa.logical_shift_right' op operand #0 must be 3D tensor of 8-bit unsigned integer or 16-bit unsigned integer or 32-bit unsigned integer values, but got 'tensor<100x200x3xf16>'}}
        %res = vosa.logical_shift_right %t0_f16 { shift = 2 : i32 } : tensor<100x200x3xf16> -> tensor<100x200x3xf16>
        return
    }
}

// -----

module {
    func.func @logical_shift_right_invalid_si() {
        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        // expected-error @below {{'vosa.logical_shift_right' op operand #0 must be 3D tensor of 8-bit unsigned integer or 16-bit unsigned integer or 32-bit unsigned integer values, but got 'tensor<100x200x3xsi16>'}}
        %res = vosa.logical_shift_right %t0_si16 { shift = 2 : i32 } : tensor<100x200x3xsi16> -> tensor<100x200x3xsi16>
        return
    }
}

// -----

module {
    func.func @logical_shift_right_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        // expected-error @below {{'vosa.logical_shift_right' op output type does not match input}}
        %res = vosa.logical_shift_right %t0_ui8 { shift = 2 : i32 } : tensor<100x200x3xui8> -> tensor<100x200x3xui32>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @clamp()
    func.func @clamp() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %min_si8 = vosa.constant 10 : si8
        %max_si8 = vosa.constant 10 : si8
        // CHECK: %{{.*}} = vosa.clamp %{{.*}}, %{{.*}}, %{{.*}} : tensor<100x200x3xsi8>, si8, si8 -> tensor<100x200x3xsi8>
        %res0 = vosa.clamp %t0_si8, %min_si8, %max_si8 : tensor<100x200x3xsi8>, si8, si8 -> tensor<100x200x3xsi8>

        %t0_si16 = vosa.constant dense<42> : tensor<100x200x3xsi16>
        %min_si16 = vosa.constant 10 : si16
        %max_si16 = vosa.constant 10 : si16
        // CHECK: %{{.*}} = vosa.clamp %{{.*}}, %{{.*}}, %{{.*}} : tensor<100x200x3xsi16>, si16, si16 -> tensor<100x200x3xsi16>
        %res1 = vosa.clamp %t0_si16, %min_si16, %max_si16 : tensor<100x200x3xsi16>, si16, si16 -> tensor<100x200x3xsi16>

        %t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        %min_f32 = vosa.constant 10.0 : f32
        %max_f32 = vosa.constant 10.0 : f32
        // CHECK: %{{.*}} = vosa.clamp %{{.*}}, %{{.*}}, %{{.*}} : tensor<100x200x3xf32>, f32, f32 -> tensor<100x200x3xf32>
        %res2 = vosa.clamp %t0_f32, %min_f32, %max_f32 : tensor<100x200x3xf32>, f32, f32 -> tensor<100x200x3xf32>

        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        %min_ui8 = vosa.constant 10 : ui8
        %max_ui8 = vosa.constant 10 : ui8
        // CHECK: %{{.*}} = vosa.clamp %{{.*}}, %{{.*}}, %{{.*}} : tensor<100x200x3xui8>, ui8, ui8 -> tensor<100x200x3xui8>
        %res_ui8 = vosa.clamp %t0_ui8, %min_ui8, %max_ui8 : tensor<100x200x3xui8>, ui8, ui8 -> tensor<100x200x3xui8>

        %t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        %min_ui16 = vosa.constant 10 : ui16
        %max_ui16 = vosa.constant 10 : ui16
        // CHECK: %{{.*}} = vosa.clamp %{{.*}}, %{{.*}}, %{{.*}} : tensor<100x200x3xui16>, ui16, ui16 -> tensor<100x200x3xui16>
        %res_ui16 = vosa.clamp %t0_ui16, %min_ui16, %max_ui16 : tensor<100x200x3xui16>, ui16, ui16 -> tensor<100x200x3xui16>

        return
    }
}

// -----

module {
    func.func @clamp_type_mismatch() {
        %t0_si8 = vosa.constant dense<42> : tensor<100x200x3xsi8>
        %min_si8 = vosa.constant 10 : si8
        %max_si8 = vosa.constant 10 : si8
        // expected-error @below {{'vosa.clamp' op output type does not match input}}
        %res = vosa.clamp %t0_si8, %min_si8, %max_si8 : tensor<100x200x3xsi8>, si8, si8 -> tensor<100x200x3xsi16>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @not()
    func.func @not() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>
        // CHECK: %{{.*}} = vosa.not %{{.*}} : tensor<100x200x3xui8>
        %res_ui8 = vosa.not %t0_ui8 : tensor<100x200x3xui8> -> tensor<100x200x3xui8>

        %t0_ui16 = vosa.constant dense<42> : tensor<100x200x3xui16>
        // CHECK: %{{.*}} = vosa.not %{{.*}} : tensor<100x200x3xui16>
        %res_ui16 = vosa.not %t0_ui16 : tensor<100x200x3xui16> -> tensor<100x200x3xui16>

        %t0_ui32 = vosa.constant dense<42> : tensor<100x200x3xui32>
        // CHECK: %{{.*}} = vosa.not %{{.*}} : tensor<100x200x3xui32>
        %res_ui32 = vosa.not %t0_ui32 : tensor<100x200x3xui32> -> tensor<100x200x3xui32>

        %t0_i1 = vosa.constant dense<true> : tensor<100x200x3xi1>
        // CHECK: %{{.*}} = vosa.not %{{.*}} : tensor<100x200x3xi1>
        %res_i1 = vosa.not %t0_i1 : tensor<100x200x3xi1> -> tensor<100x200x3xi1>

        return
    }
}

// -----
module {
    func.func @not_type_mismatch() {
        %t0_ui8 = vosa.constant dense<42> : tensor<100x200x3xui8>

        // expected-error @below {{'vosa.not' op output type does not match input}}
        %res = vosa.not %t0_ui8 : tensor<100x200x3xui8> -> tensor<100x200x3xi1>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @round()
    func.func @round() {
	%t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        // CHECK: %{{.*}} = vosa.round %{{.*}} floor : tensor<100x200x3xf32> -> tensor<100x200x3xsi32>
        %res1 = vosa.round %t0_f32 floor : tensor<100x200x3xf32> -> tensor<100x200x3xsi32>

        // CHECK: %{{.*}} = vosa.round %{{.*}} half_down : tensor<100x200x3xf32> -> tensor<100x200x3xsi32>
        %res2 = vosa.round %t0_f32 half_down : tensor<100x200x3xf32> -> tensor<100x200x3xsi32>

        // CHECK: %{{.*}} = vosa.round %{{.*}} half_away_from_zero : tensor<100x200x3xf32> -> tensor<100x200x3xsi32>
        %res3 = vosa.round %t0_f32 half_away_from_zero : tensor<100x200x3xf32> -> tensor<100x200x3xsi32>

        return
    }
}

// -----
module {
    func.func @round_size_mismatch() {
	%t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
        // expected-error @below {{'vosa.round' op output shape does not match input}}
        %res1 = vosa.round %t0_f32 floor : tensor<100x200x3xf32> -> tensor<100x201x3xsi32>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @sqrt()
    func.func @sqrt() {
	%t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
	// CHECK: %{{.*}} = vosa.sqrt %{{.*}} : tensor<100x200x3xf16>
	%res_f16 = vosa.sqrt %t0_f16 : tensor<100x200x3xf16> -> tensor<100x200x3xf16>

	%t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
	// CHECK: %{{.*}} = vosa.sqrt %{{.*}} : tensor<100x200x3xf32>
	%res_f32 = vosa.sqrt %t0_f32 : tensor<100x200x3xf32> -> tensor<100x200x3xf32>

	return
    }
}

// -----

module {
    func.func @sqrt_type_mismatch() {
	%t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>

	// expected-error @below {{'vosa.sqrt' op output type does not match input}}
	%res_f32 = vosa.sqrt %t0_f16 : tensor<100x200x3xf16> -> tensor<100x200x3xf32>

	return 
    }
}

// -----

module {
    // CHECK-LABEL: func.func @gamma_correction()
    func.func @gamma_correction() {
	%t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
	
	// CHECK: %{{.*}} = vosa.gamma_correction %{{.*}} {{.*}} : tensor<100x200x3xf16> -> tensor<100x200x3xf16>
	%res_f16 = vosa.gamma_correction %t0_f16 { gamma = 2.2 : f16 } : tensor<100x200x3xf16> -> tensor<100x200x3xf16>

	%t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
	
	// CHECK: %{{.*}} = vosa.gamma_correction %{{.*}} {{.*}} : tensor<100x200x3xf32> -> tensor<100x200x3xf32>
	%res_f32 = vosa.gamma_correction %t0_f32 { gamma = 2.2 : f32 } : tensor<100x200x3xf32> -> tensor<100x200x3xf32>

	return
    }
}

// -----

module {
    func.func @gamma_correction_type_mismatch() {
	%t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>

	// expected-error @below {{'vosa.gamma_correction' op output type does not match input}}
	%res_f32 = vosa.gamma_correction %t0_f16 { gamma = 2.2 : f16} : tensor<100x200x3xf16> -> tensor<100x200x3xf32>

	return
    }
}

// -----

module {
    func.func @gamma_correction_type_mismatch() {
	%t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
	
	// expected-error @below {{'vosa.gamma_correction' op gamma attribute and input element types do not match}}
	%res_f16 = vosa.gamma_correction %t0_f16 { gamma = 2.2 : f32 } : tensor<100x200x3xf16> -> tensor<100x200x3xf16>

	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @power()
    func.func @power() {
	%t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
	
	// CHECK: %{{.*}} = vosa.power %{{.*}} {{.*}} : tensor<100x200x3xf16> -> tensor<100x200x3xf16>
	%res_f16 = vosa.power %t0_f16 { base = 2.0 : f16 } : tensor<100x200x3xf16> -> tensor<100x200x3xf16>

	%t0_f32 = vosa.constant dense<42.0> : tensor<100x200x3xf32>
	
	// CHECK: %{{.*}} = vosa.power %{{.*}} {{.*}} : tensor<100x200x3xf32> -> tensor<100x200x3xf32>
	%res_f32 = vosa.power %t0_f32 { base = 2.0 : f32 } : tensor<100x200x3xf32> -> tensor<100x200x3xf32>

	return
    }
}

// -----

module {
    func.func @power_type_mismatch() {
	%t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>

	// expected-error @below {{'vosa.power' op output type does not match input}}
	%res_f32 = vosa.power %t0_f16 { base = 2.0 : f16} : tensor<100x200x3xf16> -> tensor<100x200x3xf32>

	return
    }
}

// -----

module {
    func.func @power_type_mismatch() {
	%t0_f16 = vosa.constant dense<42.0> : tensor<100x200x3xf16>
	
	// expected-error @below {{'vosa.power' op base attribute and input element types do not match}}
	%res_f16 = vosa.power %t0_f16 { base = 2.0 : f32 } : tensor<100x200x3xf16> -> tensor<100x200x3xf16>

	return
    }
}
