/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/vosa.h"
#include <unsupported/Eigen/CXX11/Tensor>

#ifndef VOSA_CODE_TESTBENCH_H
#define VOSA_CODE_TESTBENCH_H

namespace vosa::test {

template <typename T>
class Packing {
public:
  static vosa::EigenPlane<T> extract_plane(const vosa::EigenImage<T> &image,
                                           uint32_t channel);
  static vosa::EigenImage<T>
  concat_planes(const std::vector<vosa::EigenPlane<T>> &planes);
  static vosa::EigenImage<T> pack_plane(const vosa::EigenPlane<T> &plane);
};

template <typename T>
class Utils {
public:
  /*
   * Equivalent to REDUCE_MIN_PLANEWISE and REDUCE_MAX_PLANEWISE
   *
   * Returns: {min_value, max_value}
   */
  static std::vector<T> get_min_max(const vosa::EigenPlane<T> &plane);

  static vosa::EigenImage<uint8_t> normalise(const vosa::EigenImage<T> &image);
  static vosa::EigenPlane<uint8_t> normalise(const vosa::EigenPlane<T> &plane);
};

template <typename inputT, typename compareT>
class TestBench {
public:
  static bool compareEigenImages(const EigenImage<inputT> &image_1,
                                 const EigenImage<inputT> &image_2,
                                 inputT allowed_diff = 0, bool print = false,
                                 const std::string &comment = "");
  static bool compareEigenPlanes(const EigenPlane<inputT> &plane_1,
                                 const EigenPlane<inputT> &plane_2,
                                 inputT allowed_diff = 0, bool print = false,
                                 const std::string &comment = "");
  static bool compareEigenVectors(const EigenVector<inputT> &vector_1,
                                  const EigenVector<inputT> &vector_2,
                                  inputT allowed_diff = 0, bool print = false);
};

} // namespace vosa::test

#endif // VOSA_CODE_TESTBENCH_H
