"""Operations that transform Images in various ways (e.g. resize, warp, crop, etc.).

In all cases pixel locations are defined to be at the centre of the pixels. I.E the most upper left pixel in the image has location latexmath:[(0.5, 0.5)].

Given an output pixel location, the location of that pixel in the input would be
[latexmath]
++++
\\textrm{inp}_x : x \mapsto (x+0.5) \\frac{\\textrm{width}(input)}{\\textrm{width}(output)} - 0.5
++++
and analagously for the height. Therefore, the index for the nearest upper left pixel may be given by:
[latexmath]
++++
\\textrm{ul}_x : x \mapsto \\textrm{floor}\\left( \\textrm{inp}_x(x)\\right)
++++
and again analagously for the height. Outputs of this function are integers and may be used for both indexing and arithmetic.

License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import typing
import enum

from vosa import types as types
from vosa.op import OPERATOR_MODES, Op, ExecutionPattern, InputPattern


class CropExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    description="Input Image.",
                    shape=["H", "W", "C"],
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                description="Output Image.",
                shape=["window[2]", "window[3]", "C"],
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            CROP.CROP_WINDOW: types.DataType(
                types.DType.UINT32,
                types.AType.VECTOR,
                shape=[4],
                description="Crop window",
            )
        }


class CROP_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class CROP(Op):
    """
    Crop a sub Image from another Image.

    The required crop is parameterized by a length 4 array consisting of the elements [upper_left_y, upper_left_x, height, width].
    """

    CROP_WINDOW = "window"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[CROP_MODES, ExecutionPattern]:
        return {
            CROP_MODES.I_I_UINT8_UINT8: CropExecutionPattern(types.DType.UINT8),
            CROP_MODES.I_I_UINT16_UINT16: CropExecutionPattern(types.DType.UINT16),
            CROP_MODES.I_I_UINT32_UINT32: CropExecutionPattern(types.DType.UINT32),
            CROP_MODES.I_I_INT8_INT8: CropExecutionPattern(types.DType.INT8),
            CROP_MODES.I_I_INT16_INT16: CropExecutionPattern(types.DType.INT16),
            CROP_MODES.I_I_INT32_INT32: CropExecutionPattern(types.DType.INT32),
            CROP_MODES.I_I_FLOAT16_FLOAT16: CropExecutionPattern(types.DType.FLOAT16),
            CROP_MODES.I_I_FLOAT32_FLOAT32: CropExecutionPattern(types.DType.FLOAT32),
        }


class FlipExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image.",
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                shape=["H", "W", "C"],
                description="Flipped Input Image.",
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            FLIP.FLIP_ATTRIBUTE: types.DataType(
                types.DType.BOOL,
                types.AType.SCALAR,
                shape=[],
                description="Toggle vertical flip (true sets vertical, false sets horizontal).",
            )
        }


class FLIP_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class FLIP(Op):
    """
    Flip an Image. In the case `vert=true`, flip along the Image's vertical axis,
    [latexmath]
    ++++
    output[y,x,c] = input[y,W-x,c].
    ++++
    In the case `vert=false`, flip along the Image's horizontal axis,
    [latexmath]
    ++++
    output[y,x,c] = input[H-y,x,c].
    ++++
    """

    FLIP_ATTRIBUTE = "vert"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[FLIP_MODES, FlipExecutionPattern]:
        return {
            FLIP_MODES.I_I_UINT8_UINT8: FlipExecutionPattern(types.DType.UINT8),
            FLIP_MODES.I_I_UINT16_UINT16: FlipExecutionPattern(types.DType.UINT16),
            FLIP_MODES.I_I_UINT32_UINT32: FlipExecutionPattern(types.DType.UINT32),
            FLIP_MODES.I_I_INT8_INT8: FlipExecutionPattern(types.DType.INT8),
            FLIP_MODES.I_I_INT16_INT16: FlipExecutionPattern(types.DType.INT16),
            FLIP_MODES.I_I_INT32_INT32: FlipExecutionPattern(types.DType.INT32),
            FLIP_MODES.I_I_FLOAT16_FLOAT16: FlipExecutionPattern(types.DType.FLOAT16),
            FLIP_MODES.I_I_FLOAT32_FLOAT32: FlipExecutionPattern(types.DType.FLOAT32),
        }


class PointwiseMatrixMultiplyExecutionPattern(ExecutionPattern):
    def __init__(self, input_dtype, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    input_dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C_1"],
                    description="Input Image.",
                )
            ),
            output_pattern=types.DataType(
                output_dtype,
                types.AType.IMAGE,
                shape=["H", "W", "C_2"],
                description="Output Image.",
            ),
            dynamic_attribute_pattern={
                POINTWISE_MATRIX_MULTIPLY.MATRIX_ATTRIBUTE: types.DataType(
                    input_dtype,
                    types.AType.PLANE,
                    shape=["C_2", "C_1"],
                    description="Matrix",
                ),
                POINTWISE_MATRIX_MULTIPLY.INNER_OFFSETS_ATTRIBUTE: types.DataType(
                    input_dtype,
                    types.AType.VECTOR,
                    shape=["C_1"],
                    description="Inner Offsets",
                ),
                POINTWISE_MATRIX_MULTIPLY.OUTER_OFFSETS_ATTRIBUTE: types.DataType(
                    input_dtype,
                    types.AType.VECTOR,
                    shape=["C_2"],
                    description="Outer Offsets",
                ),
            },
        )


class POINTWISE_MATRIX_MULTIPLY_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class POINTWISE_MATRIX_MULTIPLY(Op):
    """
    Convert an Image by means of the GEMV like operations,
    [latexmath]
    ++++
    output[y, x, c_2] = M (input[y, x, :] - K_1[c_1]) + K_2[c_2] .
    ++++
    In the case of integer operations all arithmetic is performed naively (arithmetic may under or overflow for certain combinations of inputs and attributes).
    """

    MATRIX_ATTRIBUTE = "M"
    INNER_OFFSETS_ATTRIBUTE = "K_1"
    OUTER_OFFSETS_ATTRIBUTE = "K_2"

    def to_flatbuffer(self):
        import vosa_serialization.POINTWISE_MATRIX_MULTIPLY
        import vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE
        import vosa_serialization.POINTWISE_MATRIX_MULTIPLY_ATTRIBUTE

        broadcast_scalar_modes = {
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_UINT8_UINT8: vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_UINT8_UINT8,
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_UINT16_UINT16: vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_UINT16_UINT16,
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_UINT32_UINT32: vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_UINT32_UINT32,
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_INT8_INT8: vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_INT8_INT8,
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_INT16_INT16: vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_INT16_INT16,
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_INT32_INT32: vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_INT32_INT32,
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_FLOAT16_FLOAT16: vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_FLOAT16_FLOAT16,
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_FLOAT32_FLOAT32: vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_FLOAT32_FLOAT32,
        }

        # Get the attributes
        M = self._attributes[self.MATRIX_ATTRIBUTE].value()
        K_1 = self._attributes[self.INNER_OFFSETS_ATTRIBUTE].value()
        K_2 = self._attributes[self.OUTER_OFFSETS_ATTRIBUTE].value()

        broadcast_scalar_t = (
            vosa_serialization.POINTWISE_MATRIX_MULTIPLY.POINTWISE_MATRIX_MULTIPLYT()
        )
        broadcast_scalar_t.mode = broadcast_scalar_modes[self.get_operator_mode()]
        broadcast_scalar_t.attr = (
            vosa_serialization.POINTWISE_MATRIX_MULTIPLY_ATTRIBUTE.POINTWISE_MATRIX_MULTIPLY_ATTRIBUTET()
        )
        broadcast_scalar_t.attr.m = [self._pack_value(m) for m in M]
        broadcast_scalar_t.attr.mDimensions = [len(M), len(M[0])]
        broadcast_scalar_t.attr.k1 = [self._pack_value(k_1) for k_1 in K_1]
        broadcast_scalar_t.attr.k2 = [self._pack_value(k_2) for k_2 in K_2]

        return broadcast_scalar_t

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[POINTWISE_MATRIX_MULTIPLY_MODES, ExecutionPattern]:
        return {
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_UINT8_UINT8: PointwiseMatrixMultiplyExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_UINT16_UINT16: PointwiseMatrixMultiplyExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_UINT32_UINT32: PointwiseMatrixMultiplyExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_INT8_INT8: PointwiseMatrixMultiplyExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_INT16_INT16: PointwiseMatrixMultiplyExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_INT32_INT32: PointwiseMatrixMultiplyExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_FLOAT16_FLOAT16: PointwiseMatrixMultiplyExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            POINTWISE_MATRIX_MULTIPLY_MODES.I_I_FLOAT32_FLOAT32: PointwiseMatrixMultiplyExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class RemapExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        if dtype == types.DType.FLOAT16:
            map_dtype = dtype
        else:
            map_dtype = types.DType.FLOAT32
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    description="Input Image.",
                    shape=["C", "H", "W"],
                ),
                types.DataType(
                    map_dtype,
                    types.AType.ARRAY,
                    description="Remapped input pixel coordinates.",
                    shape=["A", "B", 2],
                ),
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                description="Output Image.",
                shape=["C", "A", "B"],
            ),
        )


class REMAP_BILINEAR_MODES(OPERATOR_MODES):
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class REMAP_BILINEAR(Op):
    """
    The output is the weighted sum of the fource pixels whose remapped coordinates (`input_`) are nearest, weighted using bilinear interpolation,

    [latexmath]
    ++++
    y^*, x^* = input_1[y,x,:],
    ++++
    [latexmath]
    ++++
    \\begin{array}{cc}
    output[ y,x,c]=
    & (1- x^* + \\textrm{floor}(x^*)) (1-y^* + \\textrm{floor}(y^*))  input[\\textrm{floor}(y^*), \\textrm{floor}(x^*), c ] \\\\
    &+ (x^* - \\textrm{floor}(x^*)) (1-y^* + \\textrm{floor}(y^*))  input[\\textrm{floor}(y^*), \\textrm{floor}(x^*) + 1, c] \\\\
    &+ (x^* - \\textrm{floor}(x^*)) (y^* - \\textrm{floor}(y^*))  input[\\textrm{floor}(y^*) + 1, \\textrm{floor}(x^*) + 1, c] \\\\
    &+ (1-x^* + \\textrm{floor}(x^*)) (y^* - \\textrm{floor}(y^*))  input[\\textrm{floor}(y^*) + 1, \\textrm{floor}(x^*), c].
    \\end{array}
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[REMAP_BILINEAR_MODES, ExecutionPattern]:
        return {
            REMAP_BILINEAR_MODES.I_I_FLOAT16_FLOAT16: RemapExecutionPattern(
                types.DType.FLOAT16
            ),
            REMAP_BILINEAR_MODES.I_I_FLOAT32_FLOAT32: RemapExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class REMAP_NEAREAST_NEIGHBOUR_MODES(OPERATOR_MODES):
    I_I_BOOL_BOOL = enum.auto()
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class REMAP_NEAREST_NEIGHBOUR(Op):
    """
    The output is equal to the pixel whose remapped coordinates (`input_1`) are nearest, where distance is measured in euclidean space,

    [latexmath]
    ++++
    y^*, x^* = input_1[y,x,:],
    ++++
    [latexmath]
    ++++
    n_y, n_x= \\underset{l,m}{\\textrm{argmin}} \\left\{ \\left\\Vert \\textrm{floor}(y^*) + l - y^*, \\textrm{floor}(x)  + m - x^*\\right\\Vert_2\\right\} \\mid \; l,m \\in (0, 1),
    ++++
    [latexmath]
    ++++
    output[y,x,c] = input_0[n_y, n_x, c].
    ++++
    """

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[REMAP_NEAREAST_NEIGHBOUR_MODES, ExecutionPattern]:
        return {
            REMAP_NEAREAST_NEIGHBOUR_MODES.I_I_BOOL_BOOL: RemapExecutionPattern(
                types.DType.BOOL
            ),
            REMAP_NEAREAST_NEIGHBOUR_MODES.I_I_UINT8_UINT8: RemapExecutionPattern(
                types.DType.UINT8
            ),
            REMAP_NEAREAST_NEIGHBOUR_MODES.I_I_UINT16_UINT16: RemapExecutionPattern(
                types.DType.UINT16
            ),
            REMAP_NEAREAST_NEIGHBOUR_MODES.I_I_UINT32_UINT32: RemapExecutionPattern(
                types.DType.UINT32
            ),
            REMAP_NEAREAST_NEIGHBOUR_MODES.I_I_INT8_INT8: RemapExecutionPattern(
                types.DType.INT8
            ),
            REMAP_NEAREAST_NEIGHBOUR_MODES.I_I_INT16_INT16: RemapExecutionPattern(
                types.DType.INT16
            ),
            REMAP_NEAREAST_NEIGHBOUR_MODES.I_I_INT32_INT32: RemapExecutionPattern(
                types.DType.INT32
            ),
            REMAP_NEAREAST_NEIGHBOUR_MODES.I_I_FLOAT16_FLOAT16: RemapExecutionPattern(
                types.DType.FLOAT16
            ),
            REMAP_NEAREAST_NEIGHBOUR_MODES.I_I_FLOAT32_FLOAT32: RemapExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class ResizeExecutionPattern(ExecutionPattern):
    def __init__(self, input_dtype, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    input_dtype,
                    types.AType.IMAGE,
                    description="Input Image.",
                    shape=["H", "W", "C"],
                )
            ),
            output_pattern=types.DataType(
                output_dtype,
                types.AType.IMAGE,
                description="Output Image.",
                shape=["size[0]", "size[1]", "C"],
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            Resize.SIZE_ATTRIBUTE: types.DataType(
                types.DType.UINT32,
                types.AType.VECTOR,
                shape=[2],
                description="Required output Image size.",
            )
        }


class RESIZE_MODES(OPERATOR_MODES):
    I_I_BOOL_BOOL = enum.auto()
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class Resize(Op):
    SIZE_ATTRIBUTE = "size"


class RESIZE_AREA(Resize):
    """
    Output pixel values are calculated from a weighted average of input pixels which cover the virtual location of the output pixel in the input Image.

    That is the borders of each pixel in the output image (a square) is projected back to a rectangle in the input Image.

    The weighted average is constructed by summing pixel values in the input Image, weighted by their intersection with the output pixel's rectangle and dividing by the sum of weights.
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[RESIZE_MODES, ExecutionPattern]:
        return {
            RESIZE_MODES.I_I_FLOAT16_FLOAT16: ResizeExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            RESIZE_MODES.I_I_FLOAT32_FLOAT32: ResizeExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class RESIZE_BILINEAR(Resize):
    """
    The output is the weighted sum of the four nearest pixels, weighted using bilinear interpolation.

    [latexmath]
    ++++
    \\begin{array}{cc}
    output[y,x,c]=
    & (1-\\textrm{inp}_x(x) + \\textrm{ul}_x(x)) (1-\\textrm{inp}_y(y) + \\textrm{ul}_y(y))  input[\\textrm{ul}_y(y), \\textrm{ul}_x(x), c] \\\\
    &+ (\\textrm{inp}_x(x) - \\textrm{ul}_x(x)) (1-\\textrm{inp}_y(y) + \\textrm{ul}_y(y))  input[\\textrm{ul}_y(y), \\textrm{ul}_x(x) + 1, c] \\\\
    &+ (\\textrm{inp}_x(x) - \\textrm{ul}_x(x)) (\\textrm{inp}_y(y) - \\textrm{ul}_y(y))  input[\\textrm{ul}_y(y) + 1, \\textrm{ul}_x(x) + 1, c] \\\\
    &+ (1-\\textrm{inp}_x(x) + \\textrm{ul}_x(x)) (\\textrm{inp}_y(y) - \\textrm{ul}_y(y))  input[\\textrm{ul}_y(y) + 1, \\textrm{ul}_x(x), c] \\\\
    \\end{array}
    ++++

    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[RESIZE_MODES, ExecutionPattern]:
        return {
            RESIZE_MODES.I_I_FLOAT16_FLOAT16: ResizeExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            RESIZE_MODES.I_I_FLOAT32_FLOAT32: ResizeExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class RESIZE_BICUBIC_MODES(OPERATOR_MODES):
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class RESIZE_BICUBIC(Op):
    """
    Resize an Image using separable bicubic interpolation.

    For interpolation in the _x_ direction.  the value at location _x_ in the interval latexmath:[\\left[\\textrm{ul}_x(x),\\textrm{ul}_x(x)+1 \\right)], having local coordinate latexmath:[x_* = \\textrm{inp}_x(x) - \\textrm{ul}_x(x) \in [0, 1)]  is given by
    [latexmath]
    ++++
    \\begin{array}{cl}
    interpolant_x[y,x,c] =
    &\\left(-\\frac{1}{2}x_*^3  + x_*^2 - \\frac{1}{2}x_*\\right)input[y, \\textrm{ul}_x(x)-1, c] \\\\
    & + \left(\\frac{3}{2}x_*^3  - \\frac{5}{2}x_*^2 + 1\\right)input[y, \\textrm{ul}_x(x), c] \\\\
    & + \\left(-\\frac{3}{2}x_*^3 + 2x_*^2 + \\frac{1}{2}x_* \\right)input[y, \\textrm{ul}_x(x)+1, c] \\\\
    & + \\left(\\frac{1}{2}x_*^3 - \\frac{1}{2}x_*^2\\right)input[y, \\textrm{ul}_x(x)+2, c]
    \\end{array}
    ++++
    The interpolation in the vertical dimension proceeds analagously.
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[RESIZE_BICUBIC_MODES, ExecutionPattern]:
        return {
            RESIZE_BICUBIC_MODES.I_I_FLOAT16_FLOAT16: ResizeExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            RESIZE_BICUBIC_MODES.I_I_FLOAT32_FLOAT32: ResizeExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class RESIZE_LANCZOS_MODES(OPERATOR_MODES):
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class RESIZE_LANCZOS(Op):
    """
    Resize an Image using separable 4 tap Lanczos interpolation.

    For interpolation in the _x_ direction.  the value at location _x_ in the interval latexmath:[\\left[\\textrm{ul}_x(x),\\textrm{ul}_x(x)+1 \\right)], having local coordinate latexmath:[x_* = x - \\textrm{ul}_x(x) \in [0, 1)]  is given by
    [latexmath]
    ++++
    \\begin{array}{cl}
    coefficients_x[y,x,c,:]
    = &  \\left[\\left( \\frac{\\pi  (x_*-3)]}{4} \\right)^{-2} \\cdot \\left( \\sin{\\frac{\\pi x_*}{4}}  \\right), \\right.\\\\
    & \\left( \\frac{\\pi (x_*-2)}{4} \\right)^{-2} \\cdot \\left( -\\frac{1}{\\sqrt{2}}\\sin{\\frac{\\pi x_*}{4}} -\\frac{1}{\\sqrt{2}}\\cos{\\frac{\\pi x_*}{4}} \\right), \\\\
    &  \\left( \\frac{\\pi (x_*-1)}{4} \\right)^{-2} \\cdot \\left( \\cos{\\frac{\\pi x_*}{4}} \\right), \\\\
    & \\left( \\frac{\\pi x_*}{4} \\right)^{-2} \\cdot\\left( \\frac{1}{\\sqrt{2}} \\sin{\\frac{\\pi x_*}{4}} -\\frac{1}{\\sqrt{2}} \\cos{\\frac{\\pi x_*}{4}} \\right), \\\\
    & \\left( \\frac{\\pi (x_*+1)}{4} \\right)^{-2} \\cdot\\left( -\\sin{\\frac{\\pi x_*}{4}}  \\right)_x(x) + 1], \\\\
    & \\left( \\frac{\\pi (x_*+2)}{4} \\right)^{-2} \\cdot\\left( \\frac{1}{\\sqrt{2}}\\sin{\\frac{\\pi x_*}{4}} + \\frac{1}{\\sqrt{2}}\\cos{\\frac{\\pi x_*}{4}} \\right), \\\\
    & \\left( \\frac{\\pi (x_*+3)}{4} \\right)^{-2} \\cdot\\left( - \\cos{\\frac{\\pi x_*}{4}} \\right), \\\\
    & \\left. \\left( \\frac{\\pi (x_*+4))}{4} \\right)^{-2} \\cdot\\left( -\\frac{1}{\\sqrt{2}}\\sin{\\frac{\\pi x_*}{4}} + \\frac{1}{\\sqrt{2}}\\cos{\\frac{\\pi x_*}{4}} \\right) \\right]
    \\end{array}
    ++++
    [latexmath]
    ++++
    interpolant[y,x, c] = \\frac{1}{\\sum_{i=0}^7 coefficients[y,x,c,i]}\\sum_{i=0}^7 coefficients[y,x,c,i] \\cdot input[y, \\textrm{ul}_x(x) +i-3, c]
    ++++
    The interpolation in the vertical dimension proceeds analagously.
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[RESIZE_LANCZOS_MODES, ExecutionPattern]:
        return {
            RESIZE_LANCZOS_MODES.I_I_FLOAT16_FLOAT16: ResizeExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            RESIZE_LANCZOS_MODES.I_I_FLOAT32_FLOAT32: ResizeExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class RESIZE_NEAREST_NEIGHBOUR(Resize):
    """
    The output is equal to the pixel in the input which is nearest (it must be one of the four nearest pixels), where distance is measured in euclidean space.

    [latexmath]
    ++++
    n_y, n_x= \\underset{l,m}{\\textrm{argmin}} \\left\{ \\left\\Vert \\textrm{ul}_y(y) + l - \\textrm{inp}_y(y), \\textrm{ul}_x(x)  + m - \\textrm{inp}_x(x)\\right\\Vert_2\\right\} \\mid \; l,m \\in (0, 1)
    ++++

    [latexmath]
    ++++
    output[y,x] = input[\\textrm{ul}_y(y) + n_y, \\textrm{ul}_x(x) + n_x, c]
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[RESIZE_MODES, ExecutionPattern]:
        return {
            RESIZE_MODES.I_I_BOOL_BOOL: ResizeExecutionPattern(
                types.DType.BOOL, types.DType.BOOL
            ),
            RESIZE_MODES.I_I_INT8_INT8: ResizeExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            RESIZE_MODES.I_I_INT16_INT16: ResizeExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            RESIZE_MODES.I_I_INT32_INT32: ResizeExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            RESIZE_MODES.I_I_UINT8_UINT8: ResizeExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            RESIZE_MODES.I_I_UINT16_UINT16: ResizeExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            RESIZE_MODES.I_I_UINT32_UINT32: ResizeExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
            RESIZE_MODES.I_I_FLOAT16_FLOAT16: ResizeExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            RESIZE_MODES.I_I_FLOAT32_FLOAT32: ResizeExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class Rotate90ExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image",
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                shape=["H*", "W*", "C"],
                description="Rotated Input Image",
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            ROTATE_90.ROTATE_ATTRIBUTE: types.DataType(
                types.DType.INT32,
                types.AType.SCALAR,
                shape=[],
                description="Number of rotations to perform, positive for clockwise, negative for anticlockwise.",
            )
        }


class ROTATE_90_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class ROTATE_90(Op):
    """
    Rotate an Image by a multiple of 90 degrees.

    For positive values of `rotate` the rotations are clockwise, for negative values anticlockwise.

    The output shape is given by,
    [latexmath]
    ++++
    H^*, W^* = \\left\{\\begin{array}{cc} H, W & rotate \pmod 2  == 0 \\\\ W, H & \\textrm{otherwise} \\end{array} \\right. .
    ++++
    """

    ROTATE_ATTRIBUTE = "rotate"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[ROTATE_90_MODES, Rotate90ExecutionPattern]:
        return {
            ROTATE_90_MODES.I_I_UINT8_UINT8: Rotate90ExecutionPattern(
                types.DType.UINT8
            ),
            ROTATE_90_MODES.I_I_UINT16_UINT16: Rotate90ExecutionPattern(
                types.DType.UINT16
            ),
            ROTATE_90_MODES.I_I_UINT32_UINT32: Rotate90ExecutionPattern(
                types.DType.UINT32
            ),
            ROTATE_90_MODES.I_I_INT8_INT8: Rotate90ExecutionPattern(types.DType.INT8),
            ROTATE_90_MODES.I_I_INT16_INT16: Rotate90ExecutionPattern(
                types.DType.INT16
            ),
            ROTATE_90_MODES.I_I_INT32_INT32: Rotate90ExecutionPattern(
                types.DType.INT32
            ),
            ROTATE_90_MODES.I_I_FLOAT16_FLOAT16: Rotate90ExecutionPattern(
                types.DType.FLOAT16
            ),
            ROTATE_90_MODES.I_I_FLOAT32_FLOAT32: Rotate90ExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class WarpAffineExecutionPattern(ExecutionPattern):
    def __init__(self, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    types.DType.UINT32,
                    types.AType.VECTOR,
                    description="Plane Dimensions.",
                    shape=[2],
                )
            ),
            output_pattern=types.DataType(
                output_dtype,
                types.AType.ARRAY,
                description="Output Pixel Coordinates.",
                shape=["input_0[0]", "input_0[1]", "2"],
            ),
            dynamic_attribute_pattern={
                WARP_AFFINE.AFFINE_TRANSFORMATION: types.DataType(
                    output_dtype,
                    types.AType.ARRAY,
                    shape=[2, 3],
                    description="Affine Transformation Matrix",
                )
            },
        )


class WARP_AFFINE_MODES(OPERATOR_MODES):
    I_A_UINT32_FLOAT16 = enum.auto()
    I_A_UINT32_FLOAT32 = enum.auto()


class WARP_AFFINE(Op):
    """
    Creates a 3 dimensional array defining the mapping of pixel coordinates for a given size Plane under an affine transformation.

    The first two dimensions of the `output` are given by the `input`. The third dimensions is size 2, to encode `y,x` coordinate pairs for the mapped location of that pixel.

    The affine transform is defined by,
    [latexmath]
    ++++
    \\left[ \\begin{array}{c} x_a \\\\ y_a  \\end{array}\\right] = M \\left[ \\begin{array}{c} x \\\\ y \\\\ 1 \\end{array} \\right], \\\\
    output[y,x,:] = [y_a, x_a].
    ++++
    """

    AFFINE_TRANSFORMATION = "M"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[WARP_AFFINE_MODES, ExecutionPattern]:
        return {
            WARP_AFFINE_MODES.I_A_UINT32_FLOAT16: WarpAffineExecutionPattern(
                types.DType.FLOAT16
            ),
            WARP_AFFINE_MODES.I_A_UINT32_FLOAT32: WarpAffineExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class WarpPerspectiveExecutionPattern(ExecutionPattern):
    def __init__(self, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    types.DType.UINT32,
                    types.AType.VECTOR,
                    description="Plane Dimensions.",
                    shape=[2],
                )
            ),
            output_pattern=types.DataType(
                output_dtype,
                types.AType.ARRAY,
                description="Output Pixel Coordinates.",
                shape=["input_0[0]", "input_0[1]", "2"],
            ),
            dynamic_attribute_pattern={
                WARP_PERSPECTIVE.PERSPECTIVE_TRANSFORMATION: types.DataType(
                    output_dtype,
                    types.AType.ARRAY,
                    shape=[3, 3],
                    description="Perspective Transformation Matrix",
                )
            },
        )


class WARP_PERSPECTIVE_MODES(OPERATOR_MODES):
    I_A_UINT32_FLOAT16 = enum.auto()
    I_A_UINT32_FLOAT32 = enum.auto()


class WARP_PERSPECTIVE(Op):
    """
    Creates a 3 dimensional array defining the mapping of pixel coordinates for a given size Plane under a perspective transformation.

    The first two dimensions of the `output` are given by the `input`. The third dimensions is size 2, to encode `y,x` coordinate pairs for the mapped location of that pixel.

    The perspective transform is given by
    [latexmath]
    ++++
    \\left[ \\begin{array}{c} x_p^* \\\\ y_p^* \\\\ z_p^* \\end{array}\\right] = M \\left[ \\begin{array}{c} x \\\\ y \\\\ 1 \\end{array} \\right], \\\\
    x_p, y_p =  \\frac{x_p^*}{z_p^*}, \\frac{y_p^*}{z_p^*},\\\\
    output[x,y,:] = [x_p, y_p].
    ++++
    """

    PERSPECTIVE_TRANSFORMATION = "M"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[WARP_PERSPECTIVE_MODES, ExecutionPattern]:
        return {
            WARP_PERSPECTIVE_MODES.I_A_UINT32_FLOAT16: WarpPerspectiveExecutionPattern(
                types.DType.FLOAT16
            ),
            WARP_PERSPECTIVE_MODES.I_A_UINT32_FLOAT32: WarpPerspectiveExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class WarpFlowExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    description="Input pixel offsets.",
                    shape=["H", "W", 2],
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.ARRAY,
                description="Output Pixel Coordinates.",
                shape=["H", "W", 2],
            ),
        )


class WARP_FLOW_MODES(OPERATOR_MODES):
    I_A_FLOAT16_FLOAT16 = enum.auto()
    I_A_FLOAT32_FLOAT32 = enum.auto()


class WARP_FLOW(Op):
    """
    Creates a 3 dimensional array defining a mapping of pixel coordinates, where the input Image describes `y, x` offsets from the assumed pixel coordinates defined by their indices,
    [latexmath]
    ++++
    output[y, x, :] = \\left[\\begin{array}{c} input[0,y,x] + y \\\\ input[1,y,x] + x \\end{array}\\right].
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[WARP_FLOW_MODES, ExecutionPattern]:
        return {
            WARP_FLOW_MODES.I_A_FLOAT16_FLOAT16: WarpFlowExecutionPattern(
                types.DType.FLOAT16
            ),
            WARP_FLOW_MODES.I_A_FLOAT32_FLOAT32: WarpFlowExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class PadExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    description="Input Image.",
                    shape=["H", "W", "C"],
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                description="Output Image.",
                shape=[
                    "H + pad_size[0] + pad_size[1]",
                    "W + pad_size[2] + pad_size[3]",
                    "C",
                ],
            ),
            dynamic_attribute_pattern={
                PAD.PAD_CONSTANT_ATTRIBUTE: types.DataType(
                    dtype,
                    types.AType.SCALAR,
                    shape=[],
                    description="Constant value to pad with when using `PAD_MODE::CONSTANT, otherwise ignored.",
                )
            },
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            PAD.PAD_MODE_ATTRIBUTE: types.DataType(
                None, PAD.PAD_MODE, description="Selects the type of padding to employ."
            ),
            PAD.PAD_SIZE_ATTRIBUTE: types.DataType(
                types.DType.UINT32,
                types.AType.VECTOR,
                shape=[4],
                description="Amount of padding to apply in the structure [y_start, y_end, x_start, x_end.",
            ),
        }


class PAD_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()
    I_I_BOOL_BOOL = enum.auto


class PAD(Op):
    class PAD_MODE(types.AttributeEnum):
        "Enumeration for the selection of padding mode."
        _init_ = "value __doc__"
        CONSTANT = (
            enum.auto(),
            "The Image is padded by the constant value given in `pad_constant`.",
        )
        REFLECT = (
            enum.auto(),
            "The image is padded by mirror reflection using the most outer border elements as axis of symmtery. eg. dcb\|abcdefg\|fed",
        )
        REPLICATE = (
            enum.auto(),
            "The Image is padded using the value from the nearest pixel within the input Image.",
        )
        MIRROR = (
            enum.auto(),
            "The Image is padded using the value from the pixel reflected from the nearest boundary in the input Image.",
        )

    PAD_MODE_ATTRIBUTE = "mode"
    PAD_CONSTANT_ATTRIBUTE = "pad_constant"
    PAD_SIZE_ATTRIBUTE = "pad_size"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[PAD_MODES, ExecutionPattern]:
        return {
            PAD_MODES.I_I_BOOL_BOOL: PadExecutionPattern(types.DType.BOOL),
            PAD_MODES.I_I_UINT8_UINT8: PadExecutionPattern(types.DType.UINT8),
            PAD_MODES.I_I_UINT16_UINT16: PadExecutionPattern(types.DType.UINT16),
            PAD_MODES.I_I_UINT32_UINT32: PadExecutionPattern(types.DType.UINT32),
            PAD_MODES.I_I_INT8_INT8: PadExecutionPattern(types.DType.INT8),
            PAD_MODES.I_I_INT16_INT16: PadExecutionPattern(types.DType.INT16),
            PAD_MODES.I_I_INT32_INT32: PadExecutionPattern(types.DType.INT32),
            PAD_MODES.I_I_FLOAT16_FLOAT16: PadExecutionPattern(types.DType.FLOAT16),
            PAD_MODES.I_I_FLOAT32_FLOAT32: PadExecutionPattern(types.DType.FLOAT32),
        }


PAD.__doc__ = """
Pad an Image in the vertical and horizontal directions.

Padding is applied to the four possible edges independently with options to populate the new pixels.

%s

""" % "\n".join(
    types.aenum_to_table(PAD.PAD_MODE)
)


class DecimateExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image.",
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                shape=[
                    "(H + window[0] - 1) / window[0]",
                    "(W + window[1] - 1) / window[1]",
                    "C",
                ],
                description="Decimated input Image",
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            DECIMATE.DECIMATE_WINDOW: types.DataType(
                types.DType.UINT32,
                types.AType.VECTOR,
                shape=[2],
                description="Numbers of pixels to decimate by",
            ),
            DECIMATE.DECIMATE_OFFSETS: types.DataType(
                types.DType.UINT32,
                types.AType.VECTOR,
                shape=[2],
                description="Start coordinates for sampling a window[0] x window[1] tile",
            ),
        }


class DECIMATE_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class DECIMATE(Op):
    """
    Subsample the input Image and return an Image consisting of the pixels on a subgrid within the input, which is spaced by `window[0]` in the vertical and `window[1]` in the horizontal direction. The `offsets` specify which pixel to subsample in the `window[0] x window[1]` tile.

    The following must also hold: `offsets[0] < window[0]` and `offsets[1] < window[1]`.

    """

    DECIMATE_WINDOW = "window"
    DECIMATE_OFFSETS = "offsets"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[DECIMATE_MODES, ExecutionPattern]:
        return {
            DECIMATE_MODES.I_I_UINT8_UINT8: DecimateExecutionPattern(types.DType.UINT8),
            DECIMATE_MODES.I_I_UINT16_UINT16: DecimateExecutionPattern(
                types.DType.UINT16
            ),
            DECIMATE_MODES.I_I_UINT32_UINT32: DecimateExecutionPattern(
                types.DType.UINT32
            ),
            DECIMATE_MODES.I_I_INT8_INT8: DecimateExecutionPattern(types.DType.INT8),
            DECIMATE_MODES.I_I_INT16_INT16: DecimateExecutionPattern(types.DType.INT16),
            DECIMATE_MODES.I_I_INT32_INT32: DecimateExecutionPattern(types.DType.INT32),
            DECIMATE_MODES.I_I_FLOAT16_FLOAT16: DecimateExecutionPattern(
                types.DType.FLOAT16
            ),
            DECIMATE_MODES.I_I_FLOAT32_FLOAT32: DecimateExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class ExpandExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image.",
                ),
                types.DataType(
                    types.DType.UINT8,
                    types.AType.IMAGE,
                    shape=["EH", "EW", 1],
                    description="Expand Kernel.",
                ),
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                shape=["H*EH/GH", "W*EW/GW", "C"],
                description="Expanded input Image",
            ),
            dynamic_attribute_pattern={
                EXPAND.CONSTANT_ATTRIBUTE: types.DataType(
                    dtype,
                    types.AType.SCALAR,
                    shape=[],
                    description="Value to fill with.",
                )
            },
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            EXPAND.GATHER_ATTRIBUTE: types.DataType(
                types.DType.UINT8,
                types.AType.PLANE,
                shape=["GH", "GW"],
                description="Gather Kernel.",
            )
        }


class EXPAND_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class EXPAND(Op):
    """
    Create a new upsampled Image, where the pattern of the upsampled values is defined by `input_1` (hereto known as the `expand_kernel`), and the attribute `gather_kernel`.

    The `gather_kernel` describes a tile in the input Image from which pixel values should be extracted.

    The `expand_kernel` describes a tile in the output Image, where the pixel values should be inserted.

    The values in each kernel define the mapping from input pixel values to output pixel values, that is the value or a pixel, in the output Image, with value `k` in the expand_kernel is given by the value of the pixel in the input Image with value `k` in the `gather_kernel`.
    The special value `0` in either kernel indicates no mapping to or from that location.

    All other pixels in the output Image are set to the `fill_value`.

    For example an input Image,
    [latexmath]
    ++++
    input = \\left[\\begin{array}{cc} 1 & 2 & 3 & 4\\\\ 5 & 6 & 7 & 8 \\\\ 9 & 10 & 11 & 12 \\\\\ 13 & 14 & 15 & 16 \\end{array}\\right],
    ++++
    and an `expand_kernel`,
    [latexmath]
    ++++
    expand\_kernel = \\left[\\begin{array}{cc} 0 & 1 & 2 \\\\ 1 & 0 & 0 \\end{array}\\right],
    ++++
    with a `gather_kernel`,
    [latexmath]
    ++++
    gather\_kernel = \\left[\\begin{array}{cc} 0 & 1 \\\\ 2 & 0 \\end{array}\\right],
    ++++
    and `fill_value=0`, results in an output Image,
    [latexmath]
    ++++
    output = \\left[\\begin{array}{cccc} 0 & 2 & 5 & 0 & 4 & 7 \\\\ 2 & 0 & 0 & 4 & 0 & 0 \\\\ 0 & 10 & 13 & 0 & 12 & 15 \\\\ 10 & 0 & 0 & 12 & 0 & 0 \\end{array}\\right].
    ++++
    """

    GATHER_ATTRIBUTE = "gather_kernel"
    CONSTANT_ATTRIBUTE = "fill_value"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[EXPAND_MODES, ExecutionPattern]:
        return {
            EXPAND_MODES.I_I_UINT8_UINT8: ExpandExecutionPattern(types.DType.UINT8),
            EXPAND_MODES.I_I_UINT16_UINT16: ExpandExecutionPattern(types.DType.UINT16),
            EXPAND_MODES.I_I_UINT32_UINT32: ExpandExecutionPattern(types.DType.UINT32),
            EXPAND_MODES.I_I_INT8_INT8: ExpandExecutionPattern(types.DType.INT8),
            EXPAND_MODES.I_I_INT16_INT16: ExpandExecutionPattern(types.DType.INT16),
            EXPAND_MODES.I_I_INT32_INT32: ExpandExecutionPattern(types.DType.INT32),
            EXPAND_MODES.I_I_FLOAT16_FLOAT16: ExpandExecutionPattern(
                types.DType.FLOAT16
            ),
            EXPAND_MODES.I_I_FLOAT32_FLOAT32: ExpandExecutionPattern(
                types.DType.FLOAT32
            ),
        }
