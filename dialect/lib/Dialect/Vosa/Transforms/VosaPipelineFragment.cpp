/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/Dialect/Vosa/IR/VosaDialect.h"
#include "vosa/Dialect/Vosa/Transforms/Passes.h"

#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/LLVMIR/LLVMDialect.h"
#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/IR/Builders.h"
#include "mlir/Pass/PassManager.h"
#include "mlir/Transforms/DialectConversion.h"
#include "mlir/Transforms/Passes.h"

using namespace mlir;
using namespace mlir::vosa;
#define DEBUG_TYPE "vosa-pipeline-fragment"

#include "PassDetail.h"

namespace {
class PipelineFragmentPass
    : public mlir::vosa::VosaPipelineFragmentBase<PipelineFragmentPass> {
public:
  explicit PipelineFragmentPass() = default;

  explicit PipelineFragmentPass(ArrayRef<std::string> inputs,
                                ArrayRef<std::string> outputs) {
    this->inputs = inputs;
    this->outputs = outputs;
  }

  void runOnOperation() override {
    auto func = getOperation();

    // Handles pipelines with more than one function (e.g. bokeh)
    if (func.getName().equals(llvm::StringRef("pipeline"))) {
      OpBuilder builder(func);

      // Output nodes
      // Clear function results
      unsigned initNumResults = func.getNumResults();
      for (unsigned i = 0; i < initNumResults; ++i)
        func.eraseResult(0);

      // Find new function results
      llvm::SmallVector<mlir::Type> outputTypes;
      llvm::SmallVector<mlir::Value> outputValues;
      func.walk([&](mlir::Operation *op) {
        if (auto nameLoc = op->getLoc().dyn_cast<NameLoc>()) {
          auto opName = nameLoc.getName().getValue();
          if (std::find(outputs.begin(), outputs.end(), opName) !=
              outputs.end()) {
            outputTypes.push_back(op->getResult(0).getType());
            outputValues.push_back(op->getResult(0));
          }
        }
      });
      // Set new function results
      for (size_t i = 0; i < outputTypes.size(); ++i)
        func.insertResult(i, outputTypes[i], {});

      // Create return statement with new function results and remove old one
      Region &region = func->getRegions()[0];
      auto &ret = region.front().back();
      builder.setInsertionPoint(&ret);
      builder.create<mlir::func::ReturnOp>(ret.getLoc(), outputValues);
      ret.erase();

      // Input nodes
      // Create new arg for each given input node, then replaces all uses of the
      // input node's result with the new arg
      unsigned newArgCount = func.getNumArguments();
      func.walk([&](mlir::Operation *op) {
        if (auto nameLoc = op->getLoc().dyn_cast<NameLoc>()) {
          auto opName = nameLoc.getName().getValue();
          if (std::find(inputs.begin(), inputs.end(), opName) != inputs.end()) {
            func.insertArgument(newArgCount, op->getResult(0).getType(), {},
                                func.getLoc());
            Value newArg = func.getArgument(newArgCount);
            ValueRange newArgs(newArg);
            op->replaceAllUsesWith(newArgs);
            ++newArgCount;
          }
        }
      });

      // Remove now useless code from pipeline
      OpPassManager canonicalizePipeline(
          mlir::func::FuncOp::getOperationName());
      canonicalizePipeline.addPass(createCanonicalizerPass());
      (void)runPipeline(canonicalizePipeline, func);

      removeUnusedArgs(func); // Canon pass doesn't remove unused args

      // Canon pass will not remove function calls even if their results are
      // useless so this checks for unused func calls, removes them and runs the
      // canon pass again
      bool anyLingeringFuncCalls = false;
      func.walk([&](mlir::Operation *op) {
        if (op->getName().getStringRef().equals(llvm::StringRef("func.call")) &&
            op->use_empty()) {
          op->erase();
          anyLingeringFuncCalls = true;
        }
      });
      if (anyLingeringFuncCalls) {
        (void)runPipeline(canonicalizePipeline, func);
        removeUnusedArgs(func);
      }
    }
  }

private:
  void removeUnusedArgs(mlir::func::FuncOp &func) {
    for (size_t i = 0; i < func.getNumArguments();) {
      if (func.getArgument(i).use_empty())
        func.eraseArgument(i);
      else
        ++i;
    }
  }
};
} // namespace

std::unique_ptr<mlir::Pass>
mlir::vosa::createVosaPipelineFragmentPass(ArrayRef<std::string> inputs,
                                           ArrayRef<std::string> outputs) {
  return std::make_unique<PipelineFragmentPass>(inputs, outputs);
}
