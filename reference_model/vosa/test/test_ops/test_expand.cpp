/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/expand.h"
#include "test_bench.h"
#include "vosa/vosa.h"

TEST(test_expand, test_expand) {

  uint16_t fill_value = 0;

  auto input_array = vosa::EigenImage<uint16_t>(4, 4, 1);
  input_array.setValues({{{1}, {2}, {3}, {4}},
                         {{5}, {6}, {7}, {8}},
                         {{9}, {10}, {11}, {12}},
                         {{13}, {14}, {15}, {16}}});

  auto expand_kernel_array = vosa::EigenImage<uint8_t>(2, 3, 1);
  expand_kernel_array.setValues({{{0}, {1}, {2}}, {{1}, {0}, {0}}});

  auto gather_kernel = vosa::EigenPlane<uint8_t>(2, 2);
  gather_kernel.setValues({{0, 1}, {2, 0}});
  auto input = std::make_shared<vosa::ArrayBase>(input_array);
  auto expand_kernel = std::make_shared<vosa::ArrayBase>(expand_kernel_array);

  auto expand_op = vosa::Expand<uint16_t>(fill_value, gather_kernel);
  auto expand_output = expand_op.eval({input, expand_kernel});

  auto expected_output = vosa::EigenImage<uint16_t>(4, 6, 1);
  expected_output.setValues({{{0}, {2}, {5}, {0}, {4}, {7}},
                             {{2}, {0}, {0}, {4}, {0}, {0}},
                             {{0}, {10}, {13}, {0}, {12}, {15}},
                             {{10}, {0}, {0}, {12}, {0}, {0}}});
  auto test_output =
      vosa::test::TestBench<uint16_t, int32_t>::compareEigenImages(
          expected_output, expand_output->get_image<uint16_t>());
  EXPECT_TRUE(test_output);
}

TEST(test_expand, test_expand_2_channel) {

  uint16_t fill_value = 100;

  auto input_array = vosa::EigenImage<uint16_t>(2, 2, 2);
  input_array.setValues({{{1, 2}, {3, 4}}, {{9, 10}, {11, 12}}});

  auto expand_kernel_array = vosa::EigenImage<uint8_t>(2, 2, 1);
  expand_kernel_array.setValues({{{0}, {1}}, {{1}, {0}}});

  auto gather_kernel = vosa::EigenPlane<uint8_t>(1, 1);
  gather_kernel.setValues({{1}});

  auto expand_op = vosa::Expand<uint16_t>(fill_value, gather_kernel);

  auto input = std::make_shared<vosa::ArrayBase>(input_array);
  auto expand_kernel = std::make_shared<vosa::ArrayBase>(expand_kernel_array);
  auto expand_output = expand_op.eval({input, expand_kernel});

  auto expected_output = vosa::EigenImage<uint16_t>(4, 4, 2);
  expected_output.setValues(
      {{{fill_value, fill_value}, {1, 2}, {fill_value, fill_value}, {3, 4}},
       {{1, 2}, {fill_value, fill_value}, {3, 4}, {fill_value, fill_value}},
       {{fill_value, fill_value}, {9, 10}, {fill_value, fill_value}, {11, 12}},
       {{9, 10},
        {fill_value, fill_value},
        {11, 12},
        {fill_value, fill_value}}});
  auto test_output =
      vosa::test::TestBench<uint16_t, int32_t>::compareEigenImages(
          expected_output, expand_output->get_image<uint16_t>());
  EXPECT_TRUE(test_output);
}