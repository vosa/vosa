/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_REFERENCE_MODEL_VOSA_TEST_TEST_PIPELINES_TEST_PIPELINE_H_
#define VOSA_REFERENCE_MODEL_VOSA_TEST_TEST_PIPELINES_TEST_PIPELINE_H_

#include "array_serialization.h"
#include "vosa/vosa.h"

namespace vosa::test {

class TestPipeline {
public:
  virtual ~TestPipeline() = default;

  virtual void make_graph(const std::string &stop_stage) = 0;
  std::vector<std::shared_ptr<ArrayBase>> execute();
  std::vector<ArrayBase>
  execute_and_save_serialized(const std::string &base_path,
                              bool save_json = false);

protected:
  Graph graph;
  std::unordered_map<std::string, ArrayBase> inputs_map;
  std::vector<std::string> output_names;

private:
  void serialize_and_deserialize_inputs(
      vosa_test_serialization::TestArtifactsT &test_artifacts);
  void serialize_and_deserialize_outputs(
      vosa_test_serialization::TestArtifactsT &test_artifacts,
      std::vector<ArrayBase> &graph_results);
};

} // namespace vosa::test

#endif // VOSA_REFERENCE_MODEL_VOSA_TEST_TEST_PIPELINES_TEST_PIPELINE_H_
