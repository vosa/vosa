// run command without FileCheck to see output (or add "--dump-input always" FileCheck option)
// RUN: vosa-opt -vosa-to-tosa %s | %test_src_root/e2e-runner | FileCheck %s

func.func @main() {
    %input_f32 = arith.constant dense<
      [[[  1.], [  2.], [  3.], [  4.]],
       [[  5.], [  6.], [  7.], [  8.]],
       [[  9.], [ 10.], [ 11.], [ 12.]],
       [[ 13.], [ 14.], [ 15.], [ 16.]]]> : tensor<4x4x1xf32>

    // CHECK: sizes = [8, 8]
    // CHECK: [1, 1.25, 1.75, 2.25, 2.75, 3.25, 3.75, 4]
    // CHECK: [2, 2.25, 2.75, 3.25, 3.75, 4.25, 4.75, 5]
    // CHECK: [4, 4.25, 4.75, 5.25, 5.75, 6.25, 6.75, 7]
    // CHECK: [6, 6.25, 6.75, 7.25, 7.75, 8.25, 8.75, 9]
    // CHECK: [8, 8.25, 8.75, 9.25, 9.75, 10.25, 10.75, 11]
    // CHECK: [10, 10.25, 10.75, 11.25, 11.75, 12.25, 12.75, 13]
    // CHECK: [12, 12.25, 12.75, 13.25, 13.75, 14.25, 14.75, 15]
    // CHECK: [13, 13.25, 13.75, 14.25, 14.75, 15.25, 15.75, 16]
    %resize_2x = vosa.resize_bilinear %input_f32 { size = [ 8 : i32, 8 : i32 ] } : tensor<4x4x1xf32> -> tensor<8x8x1xf32>

    %dyn_resize_2x = tensor.cast %resize_2x : tensor<8x8x1xf32> to tensor<?x?x?xf32>
    func.call @print_img_f32(%dyn_resize_2x) : (tensor<?x?x?xf32>) -> ()

    return
}

func.func @print_img_f32(%a : tensor<?x?x?xf32>) {
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c0 : tensor<?x?x?xf32>
  %w = tensor.dim %a, %c1 : tensor<?x?x?xf32>
  %nch = tensor.dim %a, %c2 : tensor<?x?x?xf32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[0, 0, %c][%h, %w, 1][1, 1, 1]  : tensor<?x?x?xf32> to tensor<?x?x1xf32>
     %chdata_c = tensor.collapse_shape %chdata [[0], [1, 2]] : tensor<?x?x1xf32> into tensor<?x?xf32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xf32> to tensor<*xf32>
     func.call @printMemrefF32(%unranked_chdata) : (tensor<*xf32>) -> ()
  }
  return
}

func.func private @printMemrefF32(%ptr : tensor<*xf32> {bufferization.access = "read"})
