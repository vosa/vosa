/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <flatbuffers/idl.h>
#include <flatbuffers/util.h>
#include <fstream>
#include <iostream>

#include "array_serialization.h"

void vosa::test::serialize_array(
    vosa_test_serialization::TestArtifactsT &test_artifacts,
    const std::string &name, const vosa::ArrayBase &array, bool input) {
  auto serialized_array = std::make_unique<vosa_test_serialization::ArrayT>(
      vosa::test::serialize(name, array));
  if (input) {
    test_artifacts.inputs.push_back(std::move(serialized_array));
  } else {
    test_artifacts.outputs.push_back(std::move(serialized_array));
  }
}

bool vosa::test::write_artifacts_flatbuffer(
    const vosa_test_serialization::TestArtifactsT &test_artifacts,
    const std::string &filename) {

  flatbuffers::FlatBufferBuilder fbb;
  fbb.Finish(
      vosa_test_serialization::TestArtifacts::Pack(fbb, &test_artifacts));

  std::ofstream o;
  o.open(filename.c_str(), std::ofstream::binary | std::ofstream::out);
  if (!o.good()) {
    std::cerr << "Failed to open: " << filename << std::endl;
    return false;
  }
  o.write(reinterpret_cast<const char *>(fbb.GetBufferPointer()),
          fbb.GetSize());
  o.close();

  return true;
}

bool vosa::test::write_artifacts_json(
    const vosa_test_serialization::TestArtifactsT &test_artifacts,
    const std::string &filename) {

  flatbuffers::FlatBufferBuilder fbb;
  fbb.Finish(
      vosa_test_serialization::TestArtifacts::Pack(fbb, &test_artifacts));

  std::string jsongen;
  flatbuffers::IDLOptions options;
  options.strict_json = true;
  flatbuffers::Parser parser(options);
  std::string schema_file;
  flatbuffers::LoadFile(
      "reference_model/serialization/schema/vosa_test_artifacts.fbs", false,
      &schema_file);
  parser.Parse(schema_file.c_str());

  uint8_t *buffer = fbb.GetBufferPointer();
  flatbuffers::GenerateText(parser, buffer, &jsongen);

  std::ofstream o(filename);
  o << jsongen;
  o.close();

  return true;
}

size_t vosa::test::get_dtype_size(vosa::ArrayBase::DType dtype) {
  switch (dtype) {
  case vosa::ArrayBase::BOOL: {
    return sizeof(bool);
  }
  case vosa::ArrayBase::UINT8: {
    return sizeof(uint8_t);
  }
  case vosa::ArrayBase::UINT16: {
    return sizeof(uint16_t);
  }
  case vosa::ArrayBase::UINT32: {
    return sizeof(uint32_t);
  }
  case vosa::ArrayBase::INT8: {
    return sizeof(int8_t);
  }
  case vosa::ArrayBase::INT16: {
    return sizeof(int16_t);
  }
  case vosa::ArrayBase::INT32: {
    return sizeof(int32_t);
  }
  case vosa::ArrayBase::FLOAT32: {
    return sizeof(float);
  }
  default: {
    throw std::runtime_error("");
  }
  }
}

vosa_test_serialization::ArrayT
vosa::test::serialize(const std::string &name, const vosa::ArrayBase &array) {
  vosa_test_serialization::ArrayT serialized_array;
  serialized_array.name = name;
  auto array_dtype = array.get_dtype();

  switch (array_dtype) {
  case vosa::ArrayBase::BOOL: {
    serialized_array.dtype = vosa_test_serialization::DTYPE_BOOL;
    break;
  };
  case vosa::ArrayBase::UINT8: {
    serialized_array.dtype = vosa_test_serialization::DTYPE_UINT8;
    break;
  }
  case vosa::ArrayBase::UINT16: {
    serialized_array.dtype = vosa_test_serialization::DTYPE_UINT16;
    break;
  }
  case vosa::ArrayBase::UINT32: {
    serialized_array.dtype = vosa_test_serialization::DTYPE_UINT32;
    break;
  }
  case vosa::ArrayBase::INT8: {
    serialized_array.dtype = vosa_test_serialization::DTYPE_INT8;
    break;
  }
  case vosa::ArrayBase::INT16: {
    serialized_array.dtype = vosa_test_serialization::DTYPE_INT16;
    break;
  }
  case vosa::ArrayBase::INT32: {
    serialized_array.dtype = vosa_test_serialization::DTYPE_INT32;
    break;
  }
  case vosa::ArrayBase::FLOAT32: {
    serialized_array.dtype = vosa_test_serialization::DTYPE_FLOAT32;
    break;
  }
  default: {
    throw std::runtime_error("");
  }
  }
  auto array_atype = array.get_atype();
  switch (array_atype) {
  case vosa::ArrayBase::DATA: {
    serialized_array.atype = vosa_test_serialization::ATYPE_DATA;
    break;
  }
  case vosa::ArrayBase::SCALAR: {
    serialized_array.atype = vosa_test_serialization::ATYPE_SCALAR;
    break;
  }
  case vosa::ArrayBase::VECTOR: {
    serialized_array.atype = vosa_test_serialization::ATYPE_VECTOR;
    break;
  }
  case vosa::ArrayBase::PLANE: {
    serialized_array.atype = vosa_test_serialization::ATYPE_PLANE;
    break;
  }
  case vosa::ArrayBase::IMAGE: {
    serialized_array.atype = vosa_test_serialization::ATYPE_IMAGE;
    break;
  }
  case vosa::ArrayBase::ARRAY: {
    serialized_array.atype = vosa_test_serialization::ATYPE_ARRAY;
    break;
  }
  }
  serialized_array.shape = array.get_dimensions();
  uint32_t size = 1;
  for (const auto &dim : array.get_dimensions()) {
    size *= dim;
  }
  size *= get_dtype_size(array.get_dtype());
  std::vector<uint8_t> data;
  data.insert(data.end(), array.get_raw_pointer().get(),
              array.get_raw_pointer().get() + size);
  serialized_array.data = data;

  return serialized_array;
}

vosa::ArrayBase vosa::test::deserialize(
    const vosa_test_serialization::ArrayT &serialized_array) {

  vosa::ArrayBase::AType a_type;
  switch (serialized_array.atype) {
  case vosa_test_serialization::ATYPE_DATA: {
    a_type = vosa::ArrayBase::DATA;
    break;
  }
  case vosa_test_serialization::ATYPE_SCALAR: {
    a_type = vosa::ArrayBase::SCALAR;
    break;
  }
  case vosa_test_serialization::ATYPE_VECTOR: {
    a_type = vosa::ArrayBase::VECTOR;
    break;
  }
  case vosa_test_serialization::ATYPE_PLANE: {
    a_type = vosa::ArrayBase::PLANE;
    break;
  }
  case vosa_test_serialization::ATYPE_IMAGE: {
    a_type = vosa::ArrayBase::IMAGE;
    break;
  }
  case vosa_test_serialization::ATYPE_ARRAY: {
    a_type = vosa::ArrayBase::ARRAY;
    break;
  }
  default: {
    throw std::runtime_error("");
  }
  }
  vosa::ArrayBase::DType d_type;
  switch (serialized_array.dtype) {
  case vosa_test_serialization::DTYPE_BOOL: {
    d_type = vosa::ArrayBase::BOOL;
    break;
  }
  case vosa_test_serialization::DTYPE_UINT8: {
    d_type = vosa::ArrayBase::UINT8;
    break;
  }
  case vosa_test_serialization::DTYPE_UINT16: {
    d_type = vosa::ArrayBase::UINT16;
    break;
  }
  case vosa_test_serialization::DTYPE_UINT32: {
    d_type = vosa::ArrayBase::UINT32;
    break;
  }
  case vosa_test_serialization::DTYPE_INT8: {
    d_type = vosa::ArrayBase::INT8;
    break;
  }
  case vosa_test_serialization::DTYPE_INT16: {
    d_type = vosa::ArrayBase::INT16;
    break;
  }
  case vosa_test_serialization::DTYPE_INT32: {
    d_type = vosa::ArrayBase::INT32;
    break;
  }
  case vosa_test_serialization::DTYPE_FLOAT32: {
    d_type = vosa::ArrayBase::FLOAT32;
    break;
  }
  default: {
    throw std::runtime_error("");
  }
  }

  return {a_type, d_type, serialized_array.shape, serialized_array.data};
}

std::string vosa::test::deserialize_name(
    const vosa_test_serialization::ArrayT &serialized_array) {
  return serialized_array.name;
}