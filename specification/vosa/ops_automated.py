"""
License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

from typing import List
import enum
import struct

import vosa.imgproc.comparison_operators as comp
import vosa.imgproc.data_manipulation_operators
import vosa.imgproc.elementwise_binary_operators
import vosa.imgproc.image_transformation_operators
import vosa.imgproc.reduction_operators
from vosa import types as types
import vosa.op as _ops

import vosa.imgproc.image_filtering_operators as filtering
import vosa.imgproc.elementwise_binary_operators as elementwise_binary
import vosa.imgproc.image_transformation_operators as image_transformation
import vosa.imgproc.reduction_operators as reduction
import vosa.imgproc.data_manipulation_operators as data
import vosa.imgproc.elementwise_unary_operators as elementwise_unary
import vosa.imgproc.statistics_operators as statistics


from vosa.op import Op, INPUT_MODES, INPUT


def abs_diff(*operands, output_dtype: vosa.types.DType, name=None):
    abs_diff_modes = {
        vosa.types.DType.UINT8: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_UINT8_UINT8,
        vosa.types.DType.UINT16: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_UINT16_UINT16,
        vosa.types.DType.UINT32: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_UINT32_UINT32,
        vosa.types.DType.INT8: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_INT8_UINT8,
        vosa.types.DType.INT16: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_INT16_UINT16,
        vosa.types.DType.INT32: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_INT32_UINT32,
        vosa.types.DType.FLOAT16: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_FLOAT16_FLOAT16,
        vosa.types.DType.FLOAT32: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_FLOAT32_FLOAT32,
    }

    if output_dtype not in abs_diff_modes:
        raise ValueError()

    return vosa.ops.abs_diff(
        *operands, operator_mode=abs_diff_modes[output_dtype], name=name
    )


def add(*operands, output_dtype: vosa.types.DType, name=None):
    add_modes = {
        vosa.types.DType.UINT8: vosa.ops.elementwise_binary.ADD_MODES.I_I_UINT8_UINT8,
        vosa.types.DType.UINT16: vosa.ops.elementwise_binary.ADD_MODES.I_I_UINT16_UINT16,
        vosa.types.DType.UINT32: vosa.ops.elementwise_binary.ADD_MODES.I_I_UINT32_UINT32,
        vosa.types.DType.INT8: vosa.ops.elementwise_binary.ADD_MODES.I_I_INT8_INT8,
        vosa.types.DType.INT16: vosa.ops.elementwise_binary.ADD_MODES.I_I_INT16_INT16,
        vosa.types.DType.INT32: vosa.ops.elementwise_binary.ADD_MODES.I_I_INT32_INT32,
        vosa.types.DType.FLOAT16: vosa.ops.elementwise_binary.ADD_MODES.I_I_FLOAT16_FLOAT16,
        vosa.types.DType.FLOAT32: vosa.ops.elementwise_binary.ADD_MODES.I_I_FLOAT32_FLOAT32,
    }

    if output_dtype not in add_modes:
        raise ValueError()

    return vosa.ops.add(*operands, operator_mode=add_modes[output_dtype], name=name)


def argmax_planewise(*operands, output_dtype: vosa.types.DType, name=None):
    argmax_planewise_modes = {
        vosa.types.DType.UINT8: vosa.ops.reduction.ARG_MAX_PLANEWISE_MODES.I_V_UINT8_UINT32,
        vosa.types.DType.UINT16: vosa.ops.reduction.ARG_MAX_PLANEWISE_MODES.I_V_UINT16_UINT32,
        vosa.types.DType.UINT32: vosa.ops.reduction.ARG_MAX_PLANEWISE_MODES.I_V_UINT32_UINT32,
        vosa.types.DType.INT8: vosa.ops.reduction.ARG_MAX_PLANEWISE_MODES.I_V_INT8_UINT32,
        vosa.types.DType.INT16: vosa.ops.reduction.ARG_MAX_PLANEWISE_MODES.I_V_INT16_UINT32,
        vosa.types.DType.INT32: vosa.ops.reduction.ARG_MAX_PLANEWISE_MODES.I_V_INT32_UINT32,
        vosa.types.DType.FLOAT16: vosa.ops.reduction.ARG_MAX_PLANEWISE_MODES.I_V_FLOAT16_UINT32,
        vosa.types.DType.FLOAT32: vosa.ops.reduction.ARG_MAX_PLANEWISE_MODES.I_V_FLOAT32_UINT32,
    }

    if output_dtype not in argmax_planewise_modes:
        raise ValueError()

    return vosa.ops.argmax_planewise(
        *operands, operator_mode=argmax_planewise_modes[output_dtype], name=name
    )


def argmin_planewise(*operands, output_dtype: vosa.types.DType, name=None):
    argmin_planewise_modes = {
        vosa.types.DType.UINT8: vosa.ops.reduction.ARG_MIN_PLANEWISE_MODES.I_V_UINT8_UINT32,
        vosa.types.DType.UINT16: vosa.ops.reduction.ARG_MIN_PLANEWISE_MODES.I_V_UINT16_UINT32,
        vosa.types.DType.UINT32: vosa.ops.reduction.ARG_MIN_PLANEWISE_MODES.I_V_UINT32_UINT32,
        vosa.types.DType.INT8: vosa.ops.reduction.ARG_MIN_PLANEWISE_MODES.I_V_INT8_UINT32,
        vosa.types.DType.INT16: vosa.ops.reduction.ARG_MIN_PLANEWISE_MODES.I_V_INT16_UINT32,
        vosa.types.DType.INT32: vosa.ops.reduction.ARG_MIN_PLANEWISE_MODES.I_V_INT32_UINT32,
        vosa.types.DType.FLOAT16: vosa.ops.reduction.ARG_MIN_PLANEWISE_MODES.I_V_FLOAT16_UINT32,
        vosa.types.DType.FLOAT32: vosa.ops.reduction.ARG_MIN_PLANEWISE_MODES.I_V_FLOAT32_UINT32,
    }

    if output_dtype not in argmin_planewise_modes:
        raise ValueError()

    return vosa.ops.argmin_planewise(
        *operands, operator_mode=argmin_planewise_modes[output_dtype], name=name
    )


def arithmetic_shift_right(*operands, output_dtype: vosa.types.DType, shift, name=None):
    arithmetic_shift_right_modes = {
        vosa.types.DType.INT8: vosa.ops.elementwise_unary.ARITHMETIC_SHIFT_RIGHT_MODES.I_I_INT8_INT8,
        vosa.types.DType.INT16: vosa.ops.elementwise_unary.ARITHMETIC_SHIFT_RIGHT_MODES.I_I_INT16_INT16,
        vosa.types.DType.INT32: vosa.ops.elementwise_unary.ARITHMETIC_SHIFT_RIGHT_MODES.I_I_INT32_INT32,
    }

    if output_dtype not in arithmetic_shift_right_modes:
        raise ValueError()

    attributes = {
        vosa.ops.elementwise_unary.ARITHMETIC_SHIFT_RIGHT.SHIFT_ATTRIBUTE: shift
    }

    return vosa.ops.arithmetic_shift_right(
        *operands,
        operator_mode=arithmetic_shift_right_modes[output_dtype],
        attributes=attributes,
        name=name
    )


def broadcast_scalar(*operands, output_dtype: vosa.types.DType, dimensions, name=None):
    broadcast_scalar_modes = {
        vosa.types.DType.UINT8: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_UINT8_UINT8,
        vosa.types.DType.UINT16: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_UINT16_UINT16,
        vosa.types.DType.UINT32: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_UINT32_UINT32,
        vosa.types.DType.INT8: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_INT8_INT8,
        vosa.types.DType.INT16: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_INT16_INT16,
        vosa.types.DType.INT32: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_INT32_INT32,
        vosa.types.DType.FLOAT16: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_FLOAT16_FLOAT16,
        vosa.types.DType.FLOAT32: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_FLOAT32_FLOAT32,
    }

    if output_dtype not in broadcast_scalar_modes:
        raise ValueError()

    attributes = {vosa.ops.data.BROADCAST_PLANEWISE.SIZE_ATTRIBUTE: dimensions}

    return vosa.ops.broadcast_scalar(
        *operands,
        operator_mode=broadcast_scalar_modes[output_dtype],
        name=name,
        attributes=attributes
    )


def cast_image(
    *operands,
    output_dtype: vosa.types.DType,
    rounding_mode=vosa.ops.data.CAST.ROUNDING_MODE.WRAP,
    name=None
):
    if rounding_mode not in vosa.ops.data.CAST.ROUNDING_MODE:
        raise ValueError()

    attributes = {vosa.ops.data.CAST.ROUNDING_MODE: rounding_mode}

    return vosa.ops.cast_image(
        *operands, output_dtype=output_dtype, name=name, attributes=attributes
    )


def const_image(
    *operands, output_dtype: vosa.types.DType, value, dimensions: List, name=None
):
    const_image_modes = {
        vosa.types.DType.UINT8: vosa.ops.data.CONST_IMAGE_MODES.I_UINT8,
        vosa.types.DType.UINT16: vosa.ops.data.CONST_IMAGE_MODES.I_UINT16,
        vosa.types.DType.UINT32: vosa.ops.data.CONST_IMAGE_MODES.I_UINT32,
        vosa.types.DType.INT8: vosa.ops.data.CONST_IMAGE_MODES.I_INT8,
        vosa.types.DType.INT16: vosa.ops.data.CONST_IMAGE_MODES.I_INT16,
        vosa.types.DType.INT32: vosa.ops.data.CONST_IMAGE_MODES.I_INT32,
        vosa.types.DType.FLOAT16: vosa.ops.data.CONST_IMAGE_MODES.I_FLOAT16,
        vosa.types.DType.FLOAT32: vosa.ops.data.CONST_IMAGE_MODES.I_FLOAT32,
    }

    if output_dtype not in const_image_modes:
        raise ValueError()

    attributes = {
        vosa.ops.data.CONST_IMAGE.VALUE: value,
        vosa.ops.data.CONST_IMAGE.DIMENSION: dimensions,
    }

    return vosa.ops.const_image(
        *operands,
        operator_mode=const_image_modes[output_dtype],
        name=name,
        attributes=attributes
    )


def const_scalar(*operands, output_dtype: vosa.types.DType, value, name=None):
    const_scalar_modes = {
        vosa.types.DType.UINT8: vosa.ops.data.CONST_SCALAR_MODES.S_UINT8,
        vosa.types.DType.UINT16: vosa.ops.data.CONST_SCALAR_MODES.S_UINT16,
        vosa.types.DType.UINT32: vosa.ops.data.CONST_SCALAR_MODES.S_UINT32,
        vosa.types.DType.INT8: vosa.ops.data.CONST_SCALAR_MODES.S_INT8,
        vosa.types.DType.INT16: vosa.ops.data.CONST_SCALAR_MODES.S_INT16,
        vosa.types.DType.INT32: vosa.ops.data.CONST_SCALAR_MODES.S_INT32,
        vosa.types.DType.FLOAT16: vosa.ops.data.CONST_SCALAR_MODES.S_FLOAT16,
        vosa.types.DType.FLOAT32: vosa.ops.data.CONST_SCALAR_MODES.S_FLOAT32,
    }

    if output_dtype not in const_scalar_modes:
        raise ValueError()

    attributes = {vosa.ops.data.CONST_SCALAR.VALUE: value}

    return vosa.ops.const_scalar(
        *operands,
        operator_mode=const_scalar_modes[output_dtype],
        name=name,
        attributes=attributes
    )


def div(*operands, output_dtype: vosa.types.DType, name=None):
    div_modes = {
        vosa.types.DType.FLOAT16: vosa.ops.elementwise_binary.DIV_MODES.I_I_FLOAT16_FLOAT16,
        vosa.types.DType.FLOAT32: vosa.ops.elementwise_binary.DIV_MODES.I_I_FLOAT32_FLOAT32,
    }

    if output_dtype not in div_modes:
        raise ValueError()

    return vosa.ops.div(*operands, operator_mode=div_modes[output_dtype], name=name)


def reduce_max_planewise(*operands, output_dtype: vosa.types.DType, name=None):
    reduce_max_planewise_modes = {
        vosa.types.DType.UINT8: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_UINT8_UINT8,
        vosa.types.DType.UINT16: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_UINT16_UINT16,
        vosa.types.DType.UINT32: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_UINT32_UINT32,
        vosa.types.DType.INT8: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_INT8_INT8,
        vosa.types.DType.INT16: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_INT16_INT16,
        vosa.types.DType.INT32: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_INT32_INT32,
        vosa.types.DType.FLOAT16: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_FLOAT16_FLOAT16,
        vosa.types.DType.FLOAT32: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_FLOAT32_FLOAT32,
    }

    if output_dtype not in reduce_max_planewise_modes:
        raise ValueError()

    return vosa.ops.reduce_max_planewise(
        *operands, operator_mode=reduce_max_planewise_modes[output_dtype], name=name
    )


def reduce_min_planewise(*operands, output_dtype: vosa.types.DType, name=None):
    reduce_min_planewise_modes = {
        vosa.types.DType.UINT8: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_UINT8_UINT8,
        vosa.types.DType.UINT16: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_UINT16_UINT16,
        vosa.types.DType.UINT32: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_UINT32_UINT32,
        vosa.types.DType.INT8: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_INT8_INT8,
        vosa.types.DType.INT16: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_INT16_INT16,
        vosa.types.DType.INT32: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_INT32_INT32,
        vosa.types.DType.FLOAT16: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_FLOAT16_FLOAT16,
        vosa.types.DType.FLOAT32: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_FLOAT32_FLOAT32,
    }

    if output_dtype not in reduce_min_planewise_modes:
        raise ValueError()

    return vosa.ops.reduce_min_planewise(
        *operands, operator_mode=reduce_min_planewise_modes[output_dtype], name=name
    )


def mult(*operands, output_dtype: vosa.types.DType, name=None):
    mult_modes = {
        vosa.types.DType.UINT8: vosa.ops.elementwise_binary.MULT_MODES.I_I_UINT8_UINT8,
        vosa.types.DType.UINT16: vosa.ops.elementwise_binary.MULT_MODES.I_I_UINT16_UINT16,
        vosa.types.DType.UINT32: vosa.ops.elementwise_binary.MULT_MODES.I_I_UINT32_UINT32,
        vosa.types.DType.INT8: vosa.ops.elementwise_binary.MULT_MODES.I_I_INT8_INT8,
        vosa.types.DType.INT16: vosa.ops.elementwise_binary.MULT_MODES.I_I_INT16_INT16,
        vosa.types.DType.INT32: vosa.ops.elementwise_binary.MULT_MODES.I_I_INT32_INT32,
        vosa.types.DType.FLOAT16: vosa.ops.elementwise_binary.MULT_MODES.I_I_FLOAT16_FLOAT16,
        vosa.types.DType.FLOAT32: vosa.ops.elementwise_binary.MULT_MODES.I_I_FLOAT32_FLOAT32,
    }

    if output_dtype not in mult_modes:
        raise ValueError()

    return vosa.ops.mult(*operands, operator_mode=mult_modes[output_dtype], name=name)


def reduce_sum_planewise(*operands, output_dtype: vosa.types.DType, name=None):
    reduce_sum_planewise_modes = {
        vosa.types.DType.UINT8: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_UINT8_UINT8,
        vosa.types.DType.UINT16: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_UINT16_UINT16,
        vosa.types.DType.UINT32: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_UINT32_UINT32,
        vosa.types.DType.INT8: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_INT8_INT8,
        vosa.types.DType.INT16: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_INT16_INT16,
        vosa.types.DType.INT32: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_INT32_INT32,
        vosa.types.DType.FLOAT16: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_FLOAT16_FLOAT16,
        vosa.types.DType.FLOAT32: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_FLOAT32_FLOAT32,
    }

    if output_dtype not in reduce_sum_planewise_modes:
        raise ValueError()

    return vosa.ops.reduce_sum_planewise(
        *operands, operator_mode=reduce_sum_planewise_modes[output_dtype], name=name
    )


def subtract(*operands, output_dtype: vosa.types.DType, name=None):
    sub_modes = {
        vosa.types.DType.UINT8: vosa.ops.elementwise_binary.SUB_MODES.I_I_UINT8_UINT8,
        vosa.types.DType.UINT16: vosa.ops.elementwise_binary.SUB_MODES.I_I_UINT16_UINT16,
        vosa.types.DType.UINT32: vosa.ops.elementwise_binary.SUB_MODES.I_I_UINT32_UINT32,
        vosa.types.DType.INT8: vosa.ops.elementwise_binary.SUB_MODES.I_I_INT8_INT8,
        vosa.types.DType.INT16: vosa.ops.elementwise_binary.SUB_MODES.I_I_INT16_INT16,
        vosa.types.DType.INT32: vosa.ops.elementwise_binary.SUB_MODES.I_I_INT32_INT32,
        vosa.types.DType.FLOAT16: vosa.ops.elementwise_binary.SUB_MODES.I_I_FLOAT16_FLOAT16,
        vosa.types.DType.FLOAT32: vosa.ops.elementwise_binary.SUB_MODES.I_I_FLOAT32_FLOAT32,
    }

    if output_dtype not in sub_modes:
        raise ValueError()

    return vosa.ops.sub(*operands, operator_mode=sub_modes[output_dtype], name=name)
