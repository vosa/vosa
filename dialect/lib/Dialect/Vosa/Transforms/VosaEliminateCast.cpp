/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/Dialect/Vosa/IR/VosaDialect.h"
#include "vosa/Dialect/Vosa/IR/VosaOps.h"
#include "vosa/Dialect/Vosa/Transforms/Passes.h"

#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/Linalg/IR/Linalg.h"
#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/Pass/Pass.h"
#include "mlir/Transforms/DialectConversion.h"
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"

#include "llvm/Support/Debug.h"

#define DEBUG_TYPE "vosa-eliminate-cast"

#include "PassDetail.h"

using namespace mlir;
using namespace mlir::vosa;

namespace {

struct ConvOpConverter : public OpRewritePattern<vosa::Conv2DOp> {
public:
  using OpRewritePattern::OpRewritePattern;

  ConvOpConverter(MLIRContext *context) : OpRewritePattern(context) {}

  LogicalResult matchAndRewrite(vosa::Conv2DOp op,
                                PatternRewriter &rewriter) const override {
    auto convInput = op->getOperands()[0];

    if (!llvm::isa_and_nonnull<vosa::CastOp>(convInput.getDefiningOp()))
      return failure();

    rewriter.replaceOpWithNewOp<vosa::Conv2DOp>(
        op, op.getResult().getType(),
        convInput.getDefiningOp()->getOperands()[0], op.getFilterAttr());
    return success();
  }
};

class VosaEliminateCastPass
    : public mlir::vosa::VosaEliminateCastBase<VosaEliminateCastPass> {
public:
  explicit VosaEliminateCastPass() = default;

  void runOnOperation() override {

    RewritePatternSet patterns(&getContext());
    patterns.add<ConvOpConverter>(patterns.getContext());

    ConversionTarget target(getContext());

    if (failed(
            applyPatternsAndFoldGreedily(getOperation(), std::move(patterns))))
      signalPassFailure();
  }
};
} // namespace

/// Create "VOSA Eliminate Cast" pass
std::unique_ptr<mlir::Pass> mlir::vosa::createVosaEliminateCastPass() {
  LLVM_DEBUG(llvm::dbgs() << "createVosaEliminateCast\n";);
  return std::make_unique<VosaEliminateCastPass>();
}
