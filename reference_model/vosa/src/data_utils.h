/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_CODE_DATA_UTILS_H
#define VOSA_CODE_DATA_UTILS_H

#include "vosa/array_types.h"

namespace vosa::utils {

template <typename T>
class DataUtils {
public:
  static vosa::EigenScalar<T> scalarFromCPP(T value);
  static vosa::EigenVector<T> vectorFromCPP(const std::vector<T> &value);
  static vosa::EigenPlane<T>
  planeFromCPP(const std::vector<std::vector<T>> &value);
  static vosa::EigenImage<T>
  imageFromCPP(T value, const std::vector<uint32_t> &spacial_dims,
               const uint32_t &channels);
  static vosa::EigenPlane<T>
  transpose_plane(const vosa::EigenPlane<T> &input_plane);
};

} // namespace vosa::utils

#endif // VOSA_CODE_DATA_UTILS_H
