/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "custom.h"

void vosa::load_custom_operators(const std::string &custom_shared_object_file) {
  void *custom_shared_object_handle =
      dlopen(custom_shared_object_file.c_str(), RTLD_LAZY);
  if (custom_shared_object_handle == nullptr) {
    std::string message = custom_shared_object_file + " not found";
    throw vosa::CustomOperatorLoadException(message);
  }

  typedef vosa::CustomScalarRegistrar *(*registar_creator_function_t)();
  auto registrar_creater_function = (registar_creator_function_t)dlsym(
      custom_shared_object_handle, "get_registrar");
  if (registrar_creater_function == nullptr) {
    std::string message =
        custom_shared_object_file + " does not contain get_registrar";
    throw vosa::CustomOperatorLoadException(message);
  }

  auto registrar = registrar_creater_function();
  registrar->register_all();
  delete registrar;
  //    dlclose(custom_shared_object_handle);
}
