/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/cast.h"
#include "test_bench.h"
#include "vosa/vosa.h"

using namespace std;
using namespace vosa;

template <typename I, typename O>
static EigenImage<O> test_cast(EigenImage<I> &input_1,
                               CastBase::rounding_mode_t rounding_mode =
                                   CastBase::VOSA_ROUNDING_MODE_WRAP) {
  auto Op = Cast<I, O>(rounding_mode);

  return Op.eval({make_shared<ArrayBase>(input_1)})->template get_image<O>();
}

TEST(test_min, test_int32_to_bool) {
  EigenImage<int32_t> input_0(1, 3, 3);
  EigenImage<int32_t> input_1(1, 3, 3);
  input_0.setValues({{{1, 2, 0}, {3, 5, 3}, {7, 8, 9}}});
  input_1.setValues({{{1, 0, 0}, {1, 1, 0}, {0, 0, 1}}});

  EigenImage<bool> expected_0(1, 3, 3);
  EigenImage<bool> expected_1(1, 3, 3);
  expected_0.setValues(
      {{{true, true, false}, {true, true, true}, {true, true, true}}});
  expected_1.setValues(
      {{{true, false, false}, {true, true, false}, {false, false, true}}});

  EigenImage<bool> actual_0 =
      test_cast<int32_t, bool>(input_0, CastBase::VOSA_ROUNDING_MODE_SATURATE);
  bool result =
      test::TestBench<bool, int32_t>::compareEigenImages(actual_0, expected_0);
  EXPECT_TRUE(result);

  EigenImage<bool> actual_1 =
      test_cast<int32_t, bool>(input_1, CastBase::VOSA_ROUNDING_MODE_SATURATE);
  result =
      test::TestBench<bool, int32_t>::compareEigenImages(actual_1, expected_1);
  EXPECT_TRUE(result);
}

TEST(test_min, test_bool_to_int32) {
  EigenImage<bool> input(1, 3, 3);
  input.setValues(
      {{{true, true, false}, {true, false, true}, {true, true, true}}});

  EigenImage<int32_t> expected(1, 3, 3);
  expected.setValues({{{1, 1, 0}, {1, 0, 1}, {1, 1, 1}}});

  EigenImage<int32_t> actual = test_cast<bool, int32_t>(input);
  bool result =
      test::TestBench<int32_t, int32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}
