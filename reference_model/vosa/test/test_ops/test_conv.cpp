/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/conv.h"
#include "test_bench.h"
#include "vosa/vosa.h"

TEST(test_conv, test_conv) {

  vosa::EigenImage<uint32_t> input{3, 6, 1};
  input.setValues({{{1}, {2}, {3}, {4}, {5}, {6}},
                   {{7}, {8}, {9}, {10}, {11}, {12}},
                   {{13}, {14}, {15}, {16}, {17}, {18}}});

  vosa::EigenPlane<uint32_t> filter{3, 3};
  filter.setValues({{1, 1, 1}, {1, 1, 1}, {1, 1, 1}});

  auto output_as_image = vosa::Conv<uint32_t, uint32_t>(filter).runme(input);

  auto output_as_plane =
      vosa::test::Packing<uint32_t>::extract_plane(output_as_image, 0);

  vosa::EigenPlane<uint32_t> expected_output{1, 4};
  expected_output.setValues({{72, 72 + 9, 72 + 18, 72 + 27}});

  bool compare = vosa::test::TestBench<uint32_t, int32_t>::compareEigenPlanes(
      output_as_plane, expected_output);
  EXPECT_EQ(compare, true);
}