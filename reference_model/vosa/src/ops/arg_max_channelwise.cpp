/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "arg_max_channelwise.h"

std::shared_ptr<vosa::Op> vosa::ArgMaxChannelwiseBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_arg_max_channelwise =
      vosa_operator_union.AsARG_MAX_CHANNELWISE();
  assert(serialized_arg_max_channelwise);
  auto mode = serialized_arg_max_channelwise->mode;
  switch (mode) {
  case vosa_serialization::ARG_MAX_CHANNELWISE_MODE_I_I_UINT8_UINT32: {
    return std::make_shared<vosa::ArgMaxChannelwise<uint8_t>>();
  }
  case vosa_serialization::ARG_MAX_CHANNELWISE_MODE_I_I_UINT16_UINT32: {
    return std::make_shared<vosa::ArgMaxChannelwise<uint16_t>>();
  }
  case vosa_serialization::ARG_MAX_CHANNELWISE_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::ArgMaxChannelwise<uint32_t>>();
  }
  case vosa_serialization::ARG_MAX_CHANNELWISE_MODE_I_I_INT8_UINT32: {
    return std::make_shared<vosa::ArgMaxChannelwise<int8_t>>();
  }
  case vosa_serialization::ARG_MAX_CHANNELWISE_MODE_I_I_INT16_UINT32: {
    return std::make_shared<vosa::ArgMaxChannelwise<int16_t>>();
  }
  case vosa_serialization::ARG_MAX_CHANNELWISE_MODE_I_I_INT32_UINT32: {
    return std::make_shared<vosa::ArgMaxChannelwise<int16_t>>();
  }
  case vosa_serialization::ARG_MAX_CHANNELWISE_MODE_I_I_FLOAT32_UINT32: {
    return std::make_shared<vosa::ArgMaxChannelwise<float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <>
vosa_serialization::ARG_MAX_CHANNELWISE_MODE
vosa::ArgMaxChannelwise<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::ARG_MAX_CHANNELWISE_MODE_I_I_UINT8_UINT32;
}

template <>
vosa_serialization::ARG_MAX_CHANNELWISE_MODE
vosa::ArgMaxChannelwise<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::ARG_MAX_CHANNELWISE_MODE_I_I_UINT16_UINT32;
}

template <>
vosa_serialization::ARG_MAX_CHANNELWISE_MODE
vosa::ArgMaxChannelwise<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::ARG_MAX_CHANNELWISE_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::ARG_MAX_CHANNELWISE_MODE
vosa::ArgMaxChannelwise<int8_t>::get_serialization_mode() const {
  return vosa_serialization::ARG_MAX_CHANNELWISE_MODE_I_I_INT8_UINT32;
}

template <>
vosa_serialization::ARG_MAX_CHANNELWISE_MODE
vosa::ArgMaxChannelwise<int16_t>::get_serialization_mode() const {
  return vosa_serialization::ARG_MAX_CHANNELWISE_MODE_I_I_INT16_UINT32;
}

template <>
vosa_serialization::ARG_MAX_CHANNELWISE_MODE
vosa::ArgMaxChannelwise<int32_t>::get_serialization_mode() const {
  return vosa_serialization::ARG_MAX_CHANNELWISE_MODE_I_I_INT32_UINT32;
}

template <>
vosa_serialization::ARG_MAX_CHANNELWISE_MODE
vosa::ArgMaxChannelwise<float>::get_serialization_mode() const {
  return vosa_serialization::ARG_MAX_CHANNELWISE_MODE_I_I_FLOAT32_UINT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::ArgMaxChannelwise<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {
  vosa_serialization::ARG_MAX_CHANNELWISET arg_max_channelwise;
  arg_max_channelwise.mode = get_serialization_mode();

  auto arg_max_channelwise_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  arg_max_channelwise_operator->op.Set(arg_max_channelwise);
  arg_max_channelwise_operator->name = op_name;
  arg_max_channelwise_operator->inputs = input_names;
  return arg_max_channelwise_operator;
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::ArgMaxChannelwise<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();

  uint32_t height = input.dimension(0);
  uint32_t width = input.dimension(1);
  uint32_t channels = input.dimension(2);

  auto typed_output = EigenImage<uint32_t>(height, width, 1);
  auto current_values = EigenPlane<T>(height, width);

  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < channels;
           channel_index++) {
        T input_value = input(height_index, width_index, channel_index);
        if (channel_index == 0) {
          typed_output(height_index, width_index, 0) = 0;
          current_values(height_index, width_index) = input_value;
        } else {
          if (input_value > current_values(height_index, width_index)) {
            typed_output(height_index, width_index, 0) = channel_index;
            current_values(height_index, width_index) = input_value;
          }
        }
      }
    }
  }
  return std::make_shared<vosa::ArrayBase>(typed_output);
}

template <typename T>
vosa::ArrayBase::DType vosa::ArgMaxChannelwise<T>::output_dtype() const {
  return vosa::ArrayBase::UINT32;
}

template class vosa::ArgMaxChannelwise<uint8_t>;

template class vosa::ArgMaxChannelwise<uint16_t>;

template class vosa::ArgMaxChannelwise<uint32_t>;

template class vosa::ArgMaxChannelwise<int8_t>;

template class vosa::ArgMaxChannelwise<int16_t>;

template class vosa::ArgMaxChannelwise<int32_t>;

template class vosa::ArgMaxChannelwise<float>;