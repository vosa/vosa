/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/Builder/mlir_builder.h"

#include "vosa/Dialect/Vosa/IR/VosaDialect.h"
#include "vosa/Dialect/Vosa/IR/VosaOps.h"
#include "vosa/Dialect/Vosa/Utils/DataFormat.h"

#include "mlir/Dialect/Arith/IR/Arith.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/Dialect/Tosa/IR/TosaOps.h"
#include "mlir/IR/Builders.h"

#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"

#define DEBUG_TYPE "vosa-fbs-builder"

#include <iostream>

using namespace mlir::vosa;

namespace vosa {

namespace detail {

RoundingMode map_rounding_mode(rounding_mode_t mode) {
  switch (mode) {
  case rounding_mode_t::VOSA_ROUNDING_MODE_SATURATE:
    return RoundingMode::saturate;
  case rounding_mode_t::VOSA_ROUNDING_MODE_WRAP:
    return RoundingMode::wrap;
  default:
    return RoundingMode::wrap;
  }
}

PadMode map_pad_mode(pad_mode_t mode) {
  switch (mode) {
  case pad_mode_t::VOSA_PAD_MODE_CONSTANT:
    return PadMode::constant;
  case pad_mode_t::VOSA_PAD_MODE_REPLICATE:
    return PadMode::replicate;
  case pad_mode_t::VOSA_PAD_MODE_REFLECT:
    return PadMode::reflect;
  case pad_mode_t::VOSA_PAD_MODE_MIRROR:
    return PadMode::mirror;
  default:
    return PadMode::mirror;
  }
}

RoundMethod map_round_method(round_method_t round_method) {
  switch (round_method) {
  case round_method_t::VOSA_ROUND_METHOD_FLOOR:
    return RoundMethod::floor;
  case round_method_t::VOSA_ROUND_METHOD_CEIL:
    return RoundMethod::ceil;
  case round_method_t::VOSA_ROUND_METHOD_TOWARDS_ZERO:
    return RoundMethod::towards_zero;
  case round_method_t::VOSA_ROUND_METHOD_AWAY_FROM_ZERO:
    return RoundMethod::away_from_zero;
  case round_method_t::VOSA_ROUND_METHOD_HALF_UP:
    return RoundMethod::half_up;
  case round_method_t::VOSA_ROUND_METHOD_HALF_DOWN:
    return RoundMethod::half_down;
  case round_method_t::VOSA_ROUND_METHOD_HALF_TOWARDS_ZERO:
    return RoundMethod::half_towards_zero;
  case round_method_t::VOSA_ROUND_METHOD_HALF_AWAY_FROM_ZERO:
    return RoundMethod::half_away_from_zero;
  case round_method_t::VOSA_ROUND_METHOD_HALF_TO_EVEN:
    return RoundMethod::half_to_even;
  case round_method_t::VOSA_ROUND_METHOD_HALF_TO_ODD:
    return RoundMethod::half_to_odd;
  default:
    return RoundMethod::floor;
  }
}

static constexpr int64_t kDynamic = mlir::ShapedType::kDynamic;

} // namespace detail

using namespace detail;

template <>
mlir::Attribute MLIRBuilder::scalar_attr<uint8_t>(uint8_t value) {
  return builder.getIntegerAttr(mlir_scalar_type(dtype_t::VOSA_UINT8), value);
}

template <>
mlir::Attribute MLIRBuilder::scalar_attr<uint16_t>(uint16_t value) {
  return builder.getIntegerAttr(mlir_scalar_type(dtype_t::VOSA_UINT16), value);
}

template <>
mlir::Attribute MLIRBuilder::scalar_attr<uint32_t>(uint32_t value) {
  return builder.getIntegerAttr(mlir_scalar_type(dtype_t::VOSA_UINT32), value);
}

template <>
mlir::Attribute MLIRBuilder::scalar_attr<int8_t>(int8_t value) {
  return builder.getIntegerAttr(mlir_scalar_type(dtype_t::VOSA_INT8), value);
}

template <>
mlir::Attribute MLIRBuilder::scalar_attr<int16_t>(int16_t value) {
  return builder.getIntegerAttr(mlir_scalar_type(dtype_t::VOSA_INT16), value);
}

template <>
mlir::Attribute MLIRBuilder::scalar_attr<int32_t>(int32_t value) {
  return builder.getIntegerAttr(mlir_scalar_type(dtype_t::VOSA_INT32), value);
}

template <>
mlir::Attribute MLIRBuilder::scalar_attr<float>(float value) {
  return builder.getF32FloatAttr(value);
}

MLIRBuilder::MLIRBuilder(mlir::MLIRContext &context, const std::string &inFile,
                         const std::string &pipelineName,
                         mlir::OwningOpRef<mlir::ModuleOp> &customOps)
    : inputFile(inFile), builder(&context), customOpsModule(customOps),
      opCount(0) {
  module = mlir::ModuleOp::create(fileLoc(0));

  auto loc = fileLoc(0);
  // create a function with no args/results
  // args will be added by Input()
  // return values will be set in finalize
  llvm::SmallVector<mlir::Type, 4> argTypes;
  llvm::SmallVector<mlir::Type, 4> resTypes;
  auto func_type = builder.getFunctionType(argTypes, resTypes);
  context.getOrLoadDialect<mlir::func::FuncDialect>();
  function = mlir::func::FuncOp::create(loc, pipelineName, func_type);
  function->setAttr("llvm.emit_c_interface", builder.getUnitAttr());
  auto &entryBlock = *function.addEntryBlock();
  builder.setInsertionPointToStart(&entryBlock);

  module.push_back(function);
}

mlir::ModuleOp MLIRBuilder::finalize(const std::vector<std::string> &outputs) {
  // collect inputs and results and update function type
  llvm::SmallVector<mlir::Type, 4> argTypes;
  for (const auto &arg : function.getArguments())
    argTypes.push_back(arg.getType());

  llvm::SmallVector<mlir::Type, 4> resTypes;
  llvm::SmallVector<mlir::Value> results;
  for (const auto &o : outputs) {
    auto iV = values.find(o);
    if (iV == values.end()) {
      module.emitError("Unknown output: " + o);
      return nullptr;
    }
    auto v = iV->second;
    results.push_back(v);
    resTypes.push_back(v.getType());
  }

  // add return operator
  builder.create<mlir::func::ReturnOp>(fileLoc(0), results);

  auto func_type = builder.getFunctionType(argTypes, resTypes);
  function.setType(func_type);

  return module;
}

bool MLIRBuilder::Input(const std::string &name, dtype_t ty) {
  unsigned argIdx = function.getNumArguments();
  function.insertArgument(argIdx, mlir_scalar_type(ty), {}, function.getLoc());
  mlir::Value arg = function.getArgument(argIdx);
  values[name] = arg;
  return true;
}

bool MLIRBuilder::Input(const std::string &name, dtype_t ty,
                        llvm::ArrayRef<int64_t> shape) {
  unsigned argIdx = function.getNumArguments();
  auto shapedTy = mlir::RankedTensorType::get(shape, mlir_scalar_type(ty));
  function.insertArgument(argIdx, shapedTy, {}, function.getLoc());
  mlir::Value arg = function.getArgument(argIdx);
  values[name] = arg;
  return true;
}

bool MLIRBuilder::Equal(const std::string &name,
                        const std::vector<std::string> &inputs) {
  addOp<EqualOp>(name, mlir_image_type(dtype_t::VOSA_BOOL), inputs);
  return true;
}

bool MLIRBuilder::Greater(const std::string &name,
                          const std::vector<std::string> &inputs) {
  addOp<GreaterOp>(name, mlir_image_type(dtype_t::VOSA_BOOL), inputs);
  return true;
}

bool MLIRBuilder::GreaterEqual(const std::string &name,
                               const std::vector<std::string> &inputs) {
  addOp<GreaterEqualOp>(name, mlir_image_type(dtype_t::VOSA_BOOL), inputs);
  return true;
}

bool MLIRBuilder::BroadcastChannelwise(const std::string &name, dtype_t outTy,
                                       const std::vector<std::string> &inputs,
                                       uint32_t size) {
  addOp<BroadcastChannelwiseOp>(name, mlir_image_type(outTy), inputs,
                                llvm::SmallVector<mlir::NamedAttribute>{
                                    namedAttr("size", i32_attr(size))});
  return true;
}

bool MLIRBuilder::BroadcastPlanewise(const std::string &name, dtype_t outTy,
                                     const std::vector<std::string> &inputs,
                                     std::vector<uint32_t> size) {
  addOp<BroadcastPlanewiseOp>(name, mlir_plane_type(outTy), inputs,
                              llvm::SmallVector<mlir::NamedAttribute>{
                                  namedAttr("size", i32_array_attr(size))});
  return true;
}

bool MLIRBuilder::Cast(const std::string &name, dtype_t outTy,
                       const std::vector<std::string> &inputs,
                       rounding_mode_t rounding_mode) {
  addOp<CastOp>(name, mlir_image_type(outTy), inputs,
                llvm::SmallVector<mlir::NamedAttribute>{namedAttr(
                    "rounding_mode",
                    RoundingModeAttr::get(builder.getContext(),
                                          map_rounding_mode(rounding_mode)))});
  return true;
}

bool MLIRBuilder::ChannelExtract(const std::string &name, dtype_t outTy,
                                 const std::vector<std::string> &inputs,
                                 const std::vector<uint32_t> &channels) {
  addOp<ChannelExtractOp>(name, mlir_plane_type(outTy), inputs,
                          llvm::SmallVector<mlir::NamedAttribute>{
                              namedAttr("channels", i32_array_attr(channels))});
  return true;
}

bool MLIRBuilder::Concat(const std::string &name, dtype_t outTy,
                         const std::vector<std::string> &inputs) {
  addOp<ConcatOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::ConstImageImpl(const std::string &name, dtype_t outTy,
                                 const std::vector<uint32_t> &dimensions,
                                 mlir::Attribute value) {
  auto imgTy = mlir_image_type(outTy, dimensions);
  addOp<ConstantOp>(name, imgTy, std::vector<std::string>{},
                    llvm::SmallVector<mlir::NamedAttribute>{namedAttr(
                        "value", mlir::DenseElementsAttr::get(imgTy, value))});
  return true;
}

bool MLIRBuilder::ConstImageImpl(const std::string &name, dtype_t outTy,
                                 const std::vector<uint32_t> &dimensions,
                                 llvm::ArrayRef<mlir::Attribute> value) {
  auto imgTy = mlir_image_type(outTy, dimensions);
  addOp<ConstantOp>(name, imgTy, std::vector<std::string>{},
                    llvm::SmallVector<mlir::NamedAttribute>{namedAttr(
                        "value", mlir::DenseElementsAttr::get(imgTy, value))});
  return true;
}

bool MLIRBuilder::ConstScalarImpl(const std::string &name, dtype_t outTy,
                                  mlir::Attribute value) {
  addOp<ConstantOp>(
      name, mlir_scalar_type(outTy), std::vector<std::string>{},
      llvm::SmallVector<mlir::NamedAttribute>{namedAttr("value", value)});
  return true;
}

bool MLIRBuilder::ImportChannel(const std::string &name, dtype_t outTy,
                                const std::vector<std::string> &inputs,
                                uint32_t stride, uint32_t offset,
                                const std::vector<uint32_t> &shape) {
  addOp<ImportChannelOp>(name, mlir_plane_type(outTy, shape), inputs,
                         llvm::SmallVector<mlir::NamedAttribute>{
                             namedAttr("stride", i32_attr(stride)),
                             namedAttr("offset", i32_attr(offset)),
                             namedAttr("shape", i32_array_attr(shape))});
  return true;
}

bool MLIRBuilder::MeshGrid(const std::string &name, dtype_t outTy,
                           const std::vector<std::string> &inputs,
                           const std::vector<uint32_t> &shape) {
  addOp<MeshGridOp>(name, mlir_image_type(outTy, 2, shape),
                    std::vector<std::string>{},
                    llvm::SmallVector<mlir::NamedAttribute>{
                        namedAttr("shape", i32_array_attr(shape))});
  return true;
}

bool MLIRBuilder::PiecewiseLinearImpl(const std::string &name, dtype_t outTy,
                                      const std::vector<std::string> &inputs,
                                      llvm::ArrayRef<mlir::Attribute> nodes,
                                      llvm::ArrayRef<mlir::Attribute> vals) {
  addOp<PiecewiseLinearOp>(
      name, mlir_image_type(outTy), inputs,
      llvm::SmallVector<mlir::NamedAttribute>{
          namedAttr("nodes",
                    dense_elements_attr(
                        outTy, {static_cast<int64_t>(nodes.size())}, nodes)),
          namedAttr("values",
                    dense_elements_attr(
                        outTy, {static_cast<int64_t>(vals.size())}, vals))});
  return true;
}

bool MLIRBuilder::TosaGraph(const std::string &name,
                            const std::vector<std::string> &inputs,
                            const std::string &op_name,
                            const std::vector<uint32_t> &output_shape) {
  auto func = customOpsModule->lookupSymbol<mlir::func::FuncOp>(op_name);
  if (!func) {
    ReportError("Custom operator " + op_name + " not defined");
    return false;
  }

  mlir::OpBuilder b(module.getBodyRegion());
  func = mlir::cast<mlir::func::FuncOp>(b.clone(*func.getOperation()));
  func.setPrivate();

  auto resTy = func.getResultTypes()[0];
  if (auto shapedResTy = resTy.dyn_cast<mlir::ShapedType>())
    resTy = shapedResTy.getElementType();
  llvm::SmallVector<int64_t> llvm_output_shape;
  for (const uint32_t shape : output_shape)
    llvm_output_shape.push_back(shape);
  auto vosa_res_type = mlir::RankedTensorType::get(llvm_output_shape, resTy);
  auto funcAttr = mlir::SymbolRefAttr::get(builder.getContext(), op_name);
  addOp<TosaGraphOp>(
      name, vosa_res_type, inputs,
      llvm::SmallVector<mlir::NamedAttribute>{namedAttr("function", funcAttr)});

  return true;
}

bool MLIRBuilder::TosaGraph(const std::string &name, dtype_t outTy,
                            const std::vector<std::string> &inputs,
                            const std::vector<uint8_t> &buffer) {
  // lookup function in module and insert call
  std::string graphFunc("tosa_graph");
  auto func = customOpsModule->lookupSymbol<mlir::func::FuncOp>(graphFunc);
  if (!func) {
    ReportError("Tosa graph " + graphFunc + " not defined");
    return false;
  }

  // TODO: check types, has one return value

  mlir::OpBuilder b(module.getBodyRegion());
  func = mlir::cast<mlir::func::FuncOp>(b.clone(*func.getOperation()));
  func.setPrivate();

  auto resTy = func.getResultTypes()[0];
  if (auto shapedResTy = resTy.dyn_cast<mlir::ShapedType>()) {
    auto resShape = shapedResTy.getShape();
    llvm::SmallVector<int64_t> outShape;
    // drop any leading 1 sized dimensions
    int numToDrop = resShape.size() - 3;
    auto d = resShape.begin();
    while (numToDrop-- > 0 && *d == 1)
      d++;
    // preserve the next two dimensions as H, W
    for (int i = 0; i < 2 && d != resShape.end(); ++i)
      outShape.emplace_back(*d++);
    // collapse any remaining dimensions into C
    int64_t c = 1;
    while (d != resShape.end()) {
      c *= *d++;
    }
    outShape.emplace_back(c);
    if (getDataFormat() == DataFormat::CHW && outShape.size() == 3) {
      // transpose to CHW format
      outShape = hwcToDataFormatShape(outShape);
    }
    resTy = mlir::RankedTensorType::get(outShape, shapedResTy.getElementType());
  }

  auto funcAttr = mlir::SymbolRefAttr::get(builder.getContext(), graphFunc);
  addOp<TosaGraphOp>(
      name, resTy, inputs,
      llvm::SmallVector<mlir::NamedAttribute>{namedAttr("function", funcAttr)});

  return true;
}

bool MLIRBuilder::Where(const std::string &name, dtype_t outTy,
                        const std::vector<std::string> &inputs) {
  addOp<WhereOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::AbsDiff(const std::string &name, dtype_t outTy,
                          const std::vector<std::string> &inputs) {
  addOp<AbsDiffOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::Add(const std::string &name, dtype_t outTy,
                      const std::vector<std::string> &inputs) {
  addOp<AddOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::Div(const std::string &name, dtype_t outTy,
                      const std::vector<std::string> &inputs) {
  addOp<DivOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::Max(const std::string &name, dtype_t outTy,
                      const std::vector<std::string> &inputs) {
  addOp<MaxOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::Min(const std::string &name, dtype_t outTy,
                      const std::vector<std::string> &inputs) {
  addOp<MinOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::Mod(const std::string &name, dtype_t outTy,
                      const std::vector<std::string> &inputs) {
  addOp<ModOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::Mult(const std::string &name, dtype_t outTy,
                       const std::vector<std::string> &inputs) {
  addOp<MultOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::Or(const std::string &name, dtype_t outTy,
                     const std::vector<std::string> &inputs) {
  addOp<OrOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::Sub(const std::string &name, dtype_t outTy,
                      const std::vector<std::string> &inputs) {
  addOp<SubOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::Abs(const std::string &name, dtype_t outTy,
                      const std::vector<std::string> &inputs) {
  addOp<AbsOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::ArithmeticShiftRight(const std::string &name, dtype_t outTy,
                                       const std::vector<std::string> &inputs,
                                       uint32_t shift) {
  addOp<ArithmeticShiftRightOp>(name, mlir_image_type(outTy), inputs,
                                llvm::SmallVector<mlir::NamedAttribute>{
                                    namedAttr("shift", i32_attr(shift))});
  return true;
}

bool MLIRBuilder::Clamp(const std::string &name, dtype_t outTy,
                        const std::vector<std::string> &inputs) {
  addOp<ClampOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::GammaCorrection(const std::string &name, dtype_t outTy,
                                  const std::vector<std::string> &inputs,
                                  float gamma) {
  addOp<GammaCorrectionOp>(name, mlir_image_type(outTy), inputs,
                           llvm::SmallVector<mlir::NamedAttribute>{
                               namedAttr("gamma", scalar_attr(gamma))});
  return true;
}

bool MLIRBuilder::LogicalShiftRight(const std::string &name, dtype_t outTy,
                                    const std::vector<std::string> &inputs,
                                    uint32_t shift) {
  addOp<LogicalShiftRightOp>(name, mlir_image_type(outTy), inputs,
                             llvm::SmallVector<mlir::NamedAttribute>{
                                 namedAttr("shift", i32_attr(shift))});
  return true;
}

bool MLIRBuilder::Not(const std::string &name, dtype_t outTy,
                      const std::vector<std::string> &inputs) {
  addOp<NotOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::Power(const std::string &name, dtype_t outTy,
                        const std::vector<std::string> &inputs, float base) {
  addOp<PowerOp>(name, mlir_image_type(outTy), inputs,
                 llvm::SmallVector<mlir::NamedAttribute>{
                     namedAttr("base", scalar_attr(base))});
  return true;
}

bool MLIRBuilder::Round(const std::string &name, dtype_t outTy,
                        const std::vector<std::string> &inputs,
                        round_method_t round_method) {
  llvm::SmallVector<mlir::NamedAttribute> attrs{namedAttr(
      "round_method", RoundMethodAttr::get(builder.getContext(),
                                           map_round_method(round_method)))};
  addOp<RoundOp>(name, mlir_image_type(outTy), inputs, attrs);
  return true;
}

bool MLIRBuilder::Sqrt(const std::string &name, dtype_t outTy,
                       const std::vector<std::string> &inputs) {
  addOp<SqrtOp>(name, mlir_image_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::Conv2DImpl(const std::string &name, dtype_t inTy,
                             dtype_t outTy,
                             const std::vector<std::string> &inputs,
                             const std::vector<uint32_t> &filter_dimension,
                             llvm::ArrayRef<mlir::Attribute> filter_value) {
  addOp<Conv2DOp>(
      name, mlir_image_type(outTy), inputs,
      llvm::SmallVector<mlir::NamedAttribute>{namedAttr(
          "filter",
          dense_elements_attr(inTy, {filter_dimension[0], filter_dimension[1]},
                              filter_value))});
  return true;
}

bool MLIRBuilder::Decimate(const std::string &name, dtype_t outTy,
                           const std::vector<std::string> &inputs,
                           const std::vector<uint32_t> &N,
                           const std::vector<uint32_t> &offsets) {
  addOp<DecimateOp>(name, mlir_image_type(outTy), inputs,
                    llvm::SmallVector<mlir::NamedAttribute>{
                        namedAttr("N", i32_array_attr(N)),
                        namedAttr("offsets", i32_array_attr(offsets))});
  return true;
}

bool MLIRBuilder::ExpandImpl(
    const std::string &name, dtype_t outTy,
    const std::vector<std::string> &inputs, mlir::Attribute fill_value,
    const std::vector<uint32_t> &gather_kernel_dimension,
    llvm::ArrayRef<uint8_t> gather_kernel) {
  addOp<ExpandOp>(
      name, mlir_image_type(outTy), inputs,
      llvm::SmallVector<mlir::NamedAttribute>{
          namedAttr("gather_kernel",
                    dense_elements_attr(dtype_t::VOSA_UINT8,
                                        {gather_kernel_dimension[0],
                                         gather_kernel_dimension[1]},
                                        attr_vector<uint8_t>(gather_kernel))),
          namedAttr("fill_value", fill_value)});
  return true;
}

bool MLIRBuilder::PadImpl(const std::string &name, dtype_t outTy,
                          const std::vector<std::string> &inputs,
                          mlir::Attribute pad_constant, pad_mode_t pad_mode,
                          const std::vector<uint32_t> &pad_size) {
  llvm::SmallVector<mlir::NamedAttribute> attrs{
      builder.getNamedAttr("mode", PadModeAttr::get(builder.getContext(),
                                                    map_pad_mode(pad_mode))),
      builder.getNamedAttr("pad_size", i32_array_attr(pad_size))};
  if (pad_mode == pad_mode_t::VOSA_PAD_MODE_CONSTANT) {
    attrs.push_back(builder.getNamedAttr("pad_constant", pad_constant));
  }
  addOp<PadOp>(name, mlir_image_type(outTy), inputs, attrs);
  return true;
}

bool MLIRBuilder::PointwiseMatrixMultiplyImpl(
    const std::string &name, dtype_t outTy,
    const std::vector<std::string> &inputs, llvm::ArrayRef<mlir::Attribute> M,
    llvm::ArrayRef<mlir::Attribute> K_1, llvm::ArrayRef<mlir::Attribute> K_2) {
  int inCh = K_1.size();
  int outCh = K_2.size();
  addOp<PointwiseMatrixMultiplyOp>(
      name, mlir_image_type(outTy), inputs,
      llvm::SmallVector<mlir::NamedAttribute>{
          namedAttr("M", dense_elements_attr(outTy, {outCh, inCh}, M)),
          namedAttr("K_1", dense_elements_attr(outTy, {inCh}, K_1)),
          namedAttr("K_2", dense_elements_attr(outTy, {outCh}, K_2))});
  return true;
}

bool MLIRBuilder::ResizeBilinear(const std::string &name, dtype_t outTy,
                                 const std::vector<std::string> &inputs,
                                 const std::vector<uint32_t> &size) {
  addOp<ResizeBilinearOp>(name, mlir_image_type(outTy), inputs,
                          llvm::SmallVector<mlir::NamedAttribute>{
                              namedAttr("size", i32_array_attr(size))});
  return true;
}

bool MLIRBuilder::ResizeNearestNeighbour(const std::string &name, dtype_t outTy,
                                         const std::vector<std::string> &inputs,
                                         const std::vector<uint32_t> &size) {
  addOp<ResizeNearestNeighbourOp>(name, mlir_image_type(outTy), inputs,
                                  llvm::SmallVector<mlir::NamedAttribute>{
                                      namedAttr("size", i32_array_attr(size))});
  return true;
}

bool MLIRBuilder::Rotate90(const std::string &name, dtype_t outTy,
                           const std::vector<std::string> &inputs,
                           int32_t rotate) {
  addOp<Rotate90Op>(name, mlir_image_type(outTy), inputs,
                    llvm::SmallVector<mlir::NamedAttribute>{
                        namedAttr("rotate", scalar_attr(rotate))});
  return true;
}

bool MLIRBuilder::ArgMaxChannelwise(const std::string &name,
                                    const std::vector<std::string> &inputs) {
  addOp<ArgMaxChannelwiseOp>(name, mlir_plane_type(dtype_t::VOSA_UINT32),
                             inputs);
  return true;
}

bool MLIRBuilder::ArgMaxPlanewise(const std::string &name,
                                  const std::vector<std::string> &inputs) {
  addOp<ArgMaxPlanewiseOp>(name, mlir_scalar_type(dtype_t::VOSA_UINT32),
                           inputs);
  return true;
}

bool MLIRBuilder::ArgMinChannelwise(const std::string &name,
                                    const std::vector<std::string> &inputs) {
  addOp<ArgMinChannelwiseOp>(name, mlir_plane_type(dtype_t::VOSA_UINT32),
                             inputs);
  return true;
}

bool MLIRBuilder::ArgMinPlanewise(const std::string &name,
                                  const std::vector<std::string> &inputs) {
  addOp<ArgMinPlanewiseOp>(name, mlir_scalar_type(dtype_t::VOSA_UINT32),
                           inputs);
  return true;
}

bool MLIRBuilder::ReduceInterpChannelwise(
    const std::string &name, dtype_t outTy,
    const std::vector<std::string> &inputs) {
  addOp<ReduceInterpChannelwiseOp>(name, mlir_plane_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::ReduceMaxChannelwise(const std::string &name, dtype_t outTy,
                                       const std::vector<std::string> &inputs) {
  addOp<ReduceMaxChannelwiseOp>(name, mlir_plane_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::ReduceMaxPlanewise(const std::string &name, dtype_t outTy,
                                     const std::vector<std::string> &inputs) {
  addOp<ReduceMaxPlanewiseOp>(name, mlir_single_pixel_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::ReduceMinChannelwise(const std::string &name, dtype_t outTy,
                                       const std::vector<std::string> &inputs) {
  addOp<ReduceMinChannelwiseOp>(name, mlir_plane_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::ReduceMinPlanewise(const std::string &name, dtype_t outTy,
                                     const std::vector<std::string> &inputs) {
  addOp<ReduceMinPlanewiseOp>(name, mlir_single_pixel_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::ReduceSumChannelwise(const std::string &name, dtype_t outTy,
                                       const std::vector<std::string> &inputs) {
  addOp<ReduceSumChannelwiseOp>(name, mlir_plane_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::ReduceSumPlanewise(const std::string &name, dtype_t outTy,
                                     const std::vector<std::string> &inputs) {
  addOp<ReduceSumPlanewiseOp>(name, mlir_single_pixel_type(outTy), inputs);
  return true;
}

bool MLIRBuilder::CustomScalar(const std::string &name,
                               const std::vector<std::string> &inputs,
                               const std::string &custom_op_name) {
  auto func = customOpsModule->lookupSymbol<mlir::func::FuncOp>(custom_op_name);
  if (!func) {
    ReportError("Custom operator " + custom_op_name + " not defined");
    return false;
  }

  mlir::OpBuilder b(module.getBodyRegion());
  func = mlir::cast<mlir::func::FuncOp>(b.clone(*func.getOperation()));
  func.setPrivate();

  auto resTy = func.getResultTypes()[0];
  auto funcAttr =
      mlir::SymbolRefAttr::get(builder.getContext(), custom_op_name);
  addOp<CustomScalarOp>(
      name, resTy, inputs,
      llvm::SmallVector<mlir::NamedAttribute>{namedAttr("function", funcAttr)});

  return true;
}

dtype_t MLIRBuilder::builder_type(mlir::Type mlir_type) {
  if (mlir_type.isUnsignedInteger(8)) {
    return dtype_t::VOSA_UINT8;
  } else if (mlir_type.isUnsignedInteger(16)) {
    return dtype_t::VOSA_UINT16;
  } else if (mlir_type.isUnsignedInteger(32)) {
    return dtype_t::VOSA_UINT32;
  } else if (mlir_type.isSignedInteger(8)) {
    return dtype_t::VOSA_INT8;
  } else if (mlir_type.isSignedInteger(16)) {
    return dtype_t::VOSA_INT16;
  } else if (mlir_type.isSignedInteger(32)) {
    return dtype_t::VOSA_INT32;
  } else if (mlir_type.isF32()) {
    return dtype_t::VOSA_FLOAT32;
  }
  ReportError("Unrecognized mlir type");
  return dtype_t::VOSA_NONE;
}

dtype_t MLIRBuilder::CustomScalarOutputType(const std::string &custom_op_name) {
  auto func = customOpsModule->lookupSymbol<mlir::func::FuncOp>(custom_op_name);
  mlir::Type result_type = func.getResultTypes()[0];
  if (mlir::isa<mlir::ShapedType>(result_type)) {
    mlir::ShapedType shaped_result_type =
        mlir::dyn_cast<mlir::ShapedType>(result_type);
    return builder_type(shaped_result_type.getElementType());
  }
  return builder_type(result_type);
}

bool MLIRBuilder::ReportError(const std::string &error) {
  llvm::errs() << error << "\n";
  return false;
}

mlir::Location MLIRBuilder::l() { return builder.getUnknownLoc(); }

mlir::Location MLIRBuilder::fileLoc(int line) {
  return mlir::FileLineColLoc::get(builder.getStringAttr(inputFile), line, 0);
}

mlir::Value MLIRBuilder::getInput(const std::string &name) {
  auto iInput = values.find(name);
  if (iInput == values.end()) {
    ReportError("Invalid input for operator: " + name);
    return nullptr;
  }
  return iInput->second;
}

llvm::SmallVector<mlir::Value>
MLIRBuilder::getInputs(llvm::ArrayRef<std::string> names) {
  return llvm::to_vector<4>(llvm::map_range(
      names, [this](const std::string &name) { return getInput(name); }));
}

mlir::Type MLIRBuilder::mlir_scalar_type(dtype_t ty) {
  switch (ty) {
  case dtype_t::VOSA_NONE:
    return builder.getNoneType();
  case dtype_t::VOSA_BOOL:
    return builder.getIntegerType(1);
  case dtype_t::VOSA_UINT8:
    return builder.getIntegerType(8, false);
  case dtype_t::VOSA_UINT16:
    return builder.getIntegerType(16, false);
  case dtype_t::VOSA_UINT32:
    return builder.getIntegerType(32, false);
  case dtype_t::VOSA_INT8:
    return builder.getIntegerType(8, true);
  case dtype_t::VOSA_INT16:
    return builder.getIntegerType(16, true);
  case dtype_t::VOSA_INT32:
    return builder.getIntegerType(32, true);
  case dtype_t::VOSA_FLOAT32:
    return builder.getF32Type();
  }
}

mlir::ShapedType MLIRBuilder::mlir_single_pixel_type(dtype_t ty) {
  return mlir::RankedTensorType::get(hwcToDataFormatShape({1, 1, 1}),
                                     mlir_scalar_type(ty));
}

mlir::ShapedType MLIRBuilder::mlir_plane_type(dtype_t ty) {
  return mlir::RankedTensorType::get(
      hwcToDataFormatShape({kDynamic, kDynamic, 1}), mlir_scalar_type(ty));
}

mlir::ShapedType MLIRBuilder::mlir_plane_type(dtype_t ty,
                                              const std::vector<uint32_t> &sz) {
  assert(sz.size() == 2 && "Invalid plane dimensions");
  return mlir::RankedTensorType::get(
      mlir::vosa::hwcToDataFormatShape({sz[0], sz[1], 1}),
      mlir_scalar_type(ty));
}

mlir::ShapedType MLIRBuilder::mlir_image_type(dtype_t ty,
                                              llvm::ArrayRef<uint32_t> sz) {
  assert(sz.size() == 3 && "Invalid image dimensions");
  return mlir::RankedTensorType::get(
      hwcToDataFormatShape({sz[0], sz[1], sz[2]}), mlir_scalar_type(ty));
}

mlir::ShapedType MLIRBuilder::mlir_image_type(dtype_t ty, int32_t ch,
                                              llvm::ArrayRef<uint32_t> sz) {
  assert(sz.size() == 2 && "Invalid image dimensions");
  return mlir::RankedTensorType::get(hwcToDataFormatShape({sz[0], sz[1], ch}),
                                     mlir_scalar_type(ty));
}

mlir::ShapedType MLIRBuilder::mlir_image_type(dtype_t ty, int32_t ch,
                                              llvm::ArrayRef<int32_t> sz) {
  assert(sz.size() == 2 && "Invalid image dimensions");
  return mlir::RankedTensorType::get(hwcToDataFormatShape({sz[0], sz[1], ch}),
                                     mlir_scalar_type(ty));
}

mlir::ShapedType MLIRBuilder::mlir_image_type(dtype_t ty) {
  return mlir::RankedTensorType::get({kDynamic, kDynamic, kDynamic},
                                     mlir_scalar_type(ty));
}

mlir::NamedAttribute MLIRBuilder::namedAttr(llvm::StringRef name,
                                            mlir::Attribute value) {
  return builder.getNamedAttr(name, value);
}

mlir::Attribute MLIRBuilder::i32_attr(uint32_t value) {
  return builder.getIntegerAttr(builder.getIntegerType(32), value);
}

mlir::Attribute
MLIRBuilder::dense_elements_attr(dtype_t elTy,
                                 const llvm::ArrayRef<int64_t> dimensions,
                                 llvm::ArrayRef<mlir::Attribute> val) {
  auto ty = mlir::RankedTensorType::get(dimensions, mlir_scalar_type(elTy));
  return mlir::DenseElementsAttr::get(ty, val);
}

mlir::ArrayAttr MLIRBuilder::i32_array_attr(llvm::ArrayRef<uint32_t> value) {
  return builder.getI32ArrayAttr(llvm::to_vector<4>(
      llvm::map_range(value, [](uint32_t v) { return (int32_t)v; })));
}

} // namespace vosa
