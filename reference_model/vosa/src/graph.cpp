/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <fstream>
#include <iostream>

#include <flatbuffers/idl.h>
#include <flatbuffers/minireflect.h>

#include "ops.h"
#include "vosa/graph.h"

vosa::ArrayBase::DType vosa::Input::output_dtype() const { return dtype; }

std::shared_ptr<vosa::Op> vosa::Input::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_input = vosa_operator_union.AsINPUT();
  assert(serialized_input);
  auto dtype = serialized_input->dtype;
  switch (dtype) {
  case vosa_serialization::DTYPE_UINT8: {
    return std::make_shared<vosa::Input>(vosa::ArrayBase::UINT8);
  }
  case vosa_serialization::DTYPE_UINT16: {
    return std::make_shared<vosa::Input>(vosa::ArrayBase::UINT16);
  }
  case vosa_serialization::DTYPE_UINT32: {
    return std::make_shared<vosa::Input>(vosa::ArrayBase::UINT32);
  }
  case vosa_serialization::DTYPE_INT8: {
    return std::make_shared<vosa::Input>(vosa::ArrayBase::INT8);
  }
  case vosa_serialization::DTYPE_INT16: {
    return std::make_shared<vosa::Input>(vosa::ArrayBase::INT16);
  }
  case vosa_serialization::DTYPE_INT32: {
    return std::make_shared<vosa::Input>(vosa::ArrayBase::INT32);
  }
  case vosa_serialization::DTYPE_FLOAT32: {
    return std::make_shared<vosa::Input>(vosa::ArrayBase::FLOAT32);
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

std::shared_ptr<vosa::ArrayBase>
vosa::Input::eval(const std::vector<std::shared_ptr<ArrayBase>> &) const {
  assert(data != nullptr);
  return data;
}

void vosa::Input::set(const ArrayBase &input_data) {
  assert(dtype == input_data.get_dtype());
  data = std::make_shared<vosa::ArrayBase>(input_data);
}

std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Input::serialize(const std::string &op_name,
                       const std::vector<std::string> &input_names) const {
  vosa_serialization::INPUTT input;
  input.dtype = vosa::ArrayBase::serialize_dtype(dtype);

  auto input_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  input_operator->op.Set(input);
  input_operator->name = op_name;
  input_operator->inputs = input_names;
  return input_operator;
}

vosa::Graph::Graph(uint8_t *flatbuffer_data) {
  vosa_serialization::VosaGraphT serialized_graph;
  auto graph =
      flatbuffers::GetRoot<vosa_serialization::VosaGraph>(flatbuffer_data);
  graph->UnPackTo(&serialized_graph);
  this->init(serialized_graph);
}

void vosa::Graph::add_default_output(const std::string &default_output) {
  default_outputs.push_back(default_output);
}

void vosa::Graph::add_default_input(const std::string &default_input) {
  default_inputs.push_back(default_input);
}

std::vector<std::string> vosa::Graph::get_default_outputs() const {
  return default_outputs;
}

std::vector<std::string> vosa::Graph::get_default_inputs() const {
  return default_inputs;
}

void vosa::Graph::init(const vosa_serialization::VosaGraphT &serialized_graph) {

  default_inputs = serialized_graph.default_inputs;
  default_outputs = serialized_graph.default_outputs;
  for (const auto &serialized_graph_op : serialized_graph.operators) {

    const std::string &name = serialized_graph_op->name;
    if (ops.find(name) != ops.end()) {
      throw vosa::GraphCreationError(
          "Duplicate named node in serialized graph.");
    }

    inputs[name] = serialized_graph_op->inputs;

    const auto &serialized_op = serialized_graph_op->op;
    std::shared_ptr<vosa::Op> op_ptr;
    switch (serialized_op.type) {
    case vosa_serialization::VosaOperator_RESIZE_BILINEAR: {
      op_ptr = vosa::ResizeBilinearBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_RESIZE_NEAREST_NEIGHBOUR: {
      op_ptr =
          vosa::ResizeNearestNeighbourBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_CROP: {
      op_ptr = vosa::CropBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_CONST_SCALAR: {
      op_ptr = vosa::ConstScalarBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_CONV_2D: {
      op_ptr = vosa::ConvBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_CONST_IMAGE: {
      op_ptr = vosa::ConstImageBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_CONCAT: {
      op_ptr = vosa::ConcatBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_REDUCE_INTERP_CHANNELWISE: {
      op_ptr =
          vosa::ReduceInterpChannelwiseBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_ROTATE_90: {
      op_ptr = vosa::Rotate90Base::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_ROUND: {
      op_ptr = vosa::RoundBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_TOSA_GRAPH: {
      op_ptr = vosa::TOSA_Graph_Base::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_MULT: {
      op_ptr = vosa::MultBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_ADD: {
      op_ptr = vosa::AddBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_SUB: {
      op_ptr = vosa::SubBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_PAD: {
      op_ptr = vosa::PadBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_CLAMP: {
      op_ptr = vosa::ClampBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_CAST: {
      op_ptr = vosa::CastBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_DIV: {
      op_ptr = vosa::DivBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_IMPORT_CHANNEL: {
      op_ptr = vosa::ImportChannelBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_BROADCAST_CHANNELWISE: {
      op_ptr = vosa::BroadcastChannelwiseBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_BROADCAST_PLANEWISE: {
      op_ptr = vosa::BroadcastPlanewiseBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_CHANNEL_EXTRACT: {
      op_ptr = vosa::ChannelExtractBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_LOGICAL_SHIFT_RIGHT: {
      op_ptr = vosa::LogicalShiftRightBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_INPUT: {
      op_ptr = vosa::Input::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_REDUCE_MAX_PLANEWISE: {
      op_ptr = vosa::ReduceMaxPlanewiseBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_REDUCE_MIN_PLANEWISE: {
      op_ptr = vosa::ReduceMinPlanewiseBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_REDUCE_SUM_CHANNELWISE: {
      op_ptr = vosa::ReduceSumChannelwiseBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_REDUCE_SUM_PLANEWISE: {
      op_ptr = vosa::ReduceSumPlanewiseBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_CUSTOM_SCALAR: {
      op_ptr = vosa::CustomScalarBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_ABS: {
      op_ptr = vosa::AbsBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_ABS_DIFF: {
      op_ptr = vosa::AbsDiffBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_PIECEWISE_LINEAR: {
      op_ptr = vosa::PiecewiseLinearBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_REDUCE_MAX_CHANNELWISE: {
      op_ptr = vosa::ReduceMaxChannelwiseBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_POINTWISE_MATRIX_MULTIPLY: {
      op_ptr =
          vosa::PointwiseMatrixMultiplyBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_DECIMATE: {
      op_ptr = vosa::DecimateBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_WHERE: {
      op_ptr = vosa::WhereBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_ARITHMETIC_SHIFT_RIGHT: {
      op_ptr = vosa::ArithmeticShiftRightBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_CONST_PLANE: {
      op_ptr = vosa::ConstPlaneBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_EXPAND: {
      op_ptr = vosa::ExpandBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_MAX_: {
      op_ptr = vosa::MaxBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_MIN_: {
      op_ptr = vosa::MinBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_SQRT: {
      op_ptr = vosa::SqrtBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_ARG_MIN_CHANNELWISE: {
      op_ptr = vosa::ArgMinChannelwiseBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_ARG_MAX_CHANNELWISE: {
      op_ptr = vosa::ArgMaxChannelwiseBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_ARG_MIN_PLANEWISE: {
      op_ptr = vosa::ArgMinPlanewiseBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_ARG_MAX_PLANEWISE: {
      op_ptr = vosa::ArgMaxPlanewiseBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_EQUAL: {
      op_ptr = vosa::EqualBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_NOT: {
      op_ptr = vosa::NotBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_MESH_GRID: {
      op_ptr = vosa::MeshGridBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_MOD: {
      op_ptr = vosa::ModBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_OR: {
      op_ptr = vosa::OrBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_ATAN2: {
      op_ptr = vosa::Atan2Base::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_POWER: {
      op_ptr = vosa::PowerBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_GAMMA_CORRECTION: {
      op_ptr = vosa::GammaCorrectionBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_GREATER: {
      op_ptr = vosa::GreaterBase::parse_serialized(serialized_op);
      break;
    }
    case vosa_serialization::VosaOperator_GREATER_EQUAL: {
      op_ptr = vosa::GreaterEqualBase::parse_serialized(serialized_op);
      break;
    }
    default:
      throw vosa::GraphCreationError("Unrecognized operator type.");
    }
    ops[name] = op_ptr;
  }
}

std::shared_ptr<vosa::Op>
vosa::Graph::add_op(const std::shared_ptr<vosa::Op> &op,
                    const std::vector<std::shared_ptr<vosa::Op>> &op_inputs) {
  std::vector<std::string> op_input_names;
  for (const auto &op_input : op_inputs) {
    bool found = false;
    for (const auto &op_kv : ops) {
      if (op_input == op_kv.second) {
        found = true;
        op_input_names.push_back(op_kv.first);
        break;
      }
    }
    if (!found) {
      throw vosa::GraphCreationError();
    }
  }
  std::string name = std::to_string(op_count);
  op_count++;
  inputs[name] = op_input_names;
  ops[name] = op;
  return op;
}

std::shared_ptr<vosa::Op>
vosa::Graph::add_op(const std::shared_ptr<vosa::Op> &op,
                    const std::vector<std::string> &op_input_names) {
  std::string name = std::to_string(op_count);
  op_count++;
  inputs[name] = op_input_names;
  ops[name] = op;
  return op;
}

std::vector<std::shared_ptr<vosa::ArrayBase>> vosa::Graph::complete_execution(
    std::unordered_map<std::string, std::shared_ptr<ArrayBase>> &completed,
    const std::vector<std::string> &output_names) {
  bool did_something = true;
  while (did_something) {
    did_something = false;
    for (const auto &op_kv : ops) {
      auto op_name = op_kv.first;

      if (completed.find(op_name) != completed.end()) {
        continue;
      }

      auto op_input_names = inputs[op_name];
      bool inputs_valid = true;
      for (const auto &input_name : op_input_names) {
        bool this_input_valid = completed.find(input_name) != completed.end();
        inputs_valid &= this_input_valid;
      }
      if (inputs_valid) {
        did_something = true;

        std::vector<std::shared_ptr<ArrayBase>> op_inputs;
        for (const auto &input_name : op_input_names) {
          op_inputs.push_back(completed[input_name]);
        }
        auto op = op_kv.second;
        auto op_output = op->eval(op_inputs);
        completed[op_name] = op_output;
      }
    }
  }

  std::vector<std::shared_ptr<vosa::ArrayBase>> output{};
  for (const auto &output_name : output_names) {
    if (completed.find(output_name) == completed.end()) {
      std::string exception_message =
          "Did not find output " + output_name + " in completed operations";
      throw vosa::GraphCreationError(exception_message);
    } else {
      output.push_back(completed[output_name]);
    }
  }
  return output;
}

std::vector<std::shared_ptr<vosa::ArrayBase>> vosa::Graph::execute(
    const std::unordered_map<std::shared_ptr<vosa::Input>, ArrayBase>
        &input_arrays,
    const std::vector<std::shared_ptr<vosa::Op>> &outputs) {

  std::unordered_map<std::string, std::shared_ptr<ArrayBase>> completed;
  std::vector<std::string> output_names = {};

  for (const auto &output_op : outputs) {
    bool found_op = false;
    for (const auto &op_kv : ops) {
      const auto &op_ptr = op_kv.second;
      if (op_ptr == output_op) {
        found_op = true;
        std::string op_name = op_kv.first;
        output_names.push_back(op_name);
        break;
      }
    }
    if (!found_op) {
      std::string exception_message =
          "Failed to find output operator in the graph.";
      throw vosa::GraphCreationError(exception_message);
    }
  }

  for (const auto &input_kv : input_arrays) {
    const auto &input_op_ptr = input_kv.first;
    const auto &input_op_value = input_kv.second;
    for (const auto &op_kv : ops) {
      const auto &op_ptr = op_kv.second;
      if (op_ptr == input_op_ptr) {
        input_op_ptr->set(input_op_value);
      }
    }
  }

  return complete_execution(completed, output_names);
}

std::vector<std::shared_ptr<vosa::ArrayBase>> vosa::Graph::execute(
    const std::unordered_map<std::string, ArrayBase> &input_arrays,
    const std::vector<std::string> &output_names) {

  for (const auto &output_name : output_names) {
    if (ops.find(output_name) == ops.end()) {
      std::string exception_message =
          "Did not find output " + output_name + " in the graph";
      throw vosa::GraphCreationError(exception_message);
    }
  }

  std::unordered_map<std::string, std::shared_ptr<ArrayBase>> completed;
  for (const auto &input_array_kv : input_arrays) {
    auto input_name = input_array_kv.first;
    const auto &input_array = input_array_kv.second;
    auto input_array_ptr = std::make_shared<ArrayBase>(input_array);
    completed[input_name] = input_array_ptr;
  }

  return complete_execution(completed, output_names);
}

const std::unordered_map<std::string, std::shared_ptr<vosa::Op>> &
vosa::Graph::get_ops() const {
  return ops;
}

std::string
vosa::Graph::get_name_for_op(const std::shared_ptr<vosa::Op> &op) const {
  for (const auto &kv : ops) {
    if (kv.second == op) {
      return kv.first;
    }
  }
  throw std::runtime_error("Op not found");
}

vosa_serialization::VosaGraphT vosa::Graph::serialize() const {
  vosa_serialization::VosaGraphT serialized_vosa_graph;
  auto operators =
      std::vector<std::unique_ptr<vosa_serialization::VosaGraphOperatorT>>();
  for (const auto &op_kv : ops) {
    std::string op_name = op_kv.first;
    const std::vector<std::string> &input_names = inputs.at(op_name);
    auto op = op_kv.second;
    auto op_ptr = op->serialize(op_name, input_names);
    assert(op_ptr->op.type != vosa_serialization::VosaOperator_NONE);
    operators.emplace_back(std::move(op_ptr));
  }
  serialized_vosa_graph.operators = std::move(operators);
  for (const auto &default_output : default_outputs)
    serialized_vosa_graph.default_outputs.push_back(default_output);
  for (const auto &default_input : default_inputs)
    serialized_vosa_graph.default_inputs.push_back(default_input);
  return serialized_vosa_graph;
}

vosa::Graph
vosa::Graph::load_from_flatbuffer(const std::string &flatbuffer_path) {
  // Read the flatbuffer
  std::ifstream i(flatbuffer_path, std::ifstream::binary | std::ifstream::in);
  if (!i.good())
    throw std::runtime_error("File failed to open" + flatbuffer_path + "\n");
  std::vector<uint8_t> buffer(std::istreambuf_iterator<char>(i), {});
  i.close();

  // Deserialise
  vosa_serialization::VosaGraphT serialised_graph;
  vosa_serialization::GetVosaGraph(buffer.data())->UnPackTo(&serialised_graph);

  vosa::Graph graph = vosa::Graph(serialised_graph);
  return graph;
}

void vosa::Graph::write_flatbuffer(const std::string &filename) const {
  auto serialization = this->serialize();
  flatbuffers::FlatBufferBuilder fbb;
  fbb.Finish(vosa_serialization::VosaGraph::Pack(fbb, &serialization));

  std::ofstream o;
  o.open(filename.c_str(), std::ofstream::binary | std::ofstream::out);
  if (!o.good()) {
    std::cerr << "Failed to open: " << filename << std::endl;
    return;
  }
  o.write(reinterpret_cast<const char *>(fbb.GetBufferPointer()),
          fbb.GetSize());
  o.close();
}

void vosa::Graph::write_json(const std::string &filename) const {
  auto serialization = this->serialize();
  flatbuffers::FlatBufferBuilder fbb;
  fbb.Finish(vosa_serialization::VosaGraph::Pack(fbb, &serialization));

  std::string jsongen = vosa::Graph::flatbuffer_to_json(fbb.GetBufferPointer());

  std::ofstream o(filename);
  o << jsongen;
  o.close();
}

std::string vosa::Graph::flatbuffer_to_json(uint8_t *buffer) {
  // Configure JSON
  std::string jsongen;
  flatbuffers::IDLOptions options;
  options.strict_json = true;
  flatbuffers::Parser parser(options);

  // Load schema
  std::string schema_file;
  flatbuffers::LoadFile("reference_model/serialization/schema/vosa.fbs", false,
                        &schema_file);
  parser.Parse(schema_file.c_str());

  // Convert to JSON
  flatbuffers::GenerateText(parser, buffer, &jsongen);
  return jsongen;
}

std::string vosa::Graph::dump() const {
  auto serialization = this->serialize();
  flatbuffers::FlatBufferBuilder fbb;
  fbb.Finish(vosa_serialization::VosaGraph::Pack(fbb, &serialization));
  return flatbuffers::FlatBufferToString(
      fbb.GetBufferPointer(), vosa_serialization::VosaGraphTypeTable());
}
