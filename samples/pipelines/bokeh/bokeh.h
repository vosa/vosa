
/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_PIPELINES_PIPELINES_BOKEH_BOKEH_H_
#define VOSA_PIPELINES_PIPELINES_BOKEH_BOKEH_H_

#include "vosa/api/graph_builder.h"
#include "vosa/api/ops.h"

namespace vosa::samples::bokeh {

struct CONSTANTS {
  constexpr static float REGIONS = 4.f;
  constexpr static float BOUNDS = 256.f / REGIONS;
  constexpr static float IFBF = 4.0f;
};

constexpr std::string_view depth_map_average_op_name = "depth_map_average";
constexpr std::string_view scale_factor_op_name = "scale_factor";
constexpr std::string_view two_five_five_op_name = "two_five_five";

std::shared_ptr<vosa::api::Op> vosaGraphDepthNormalization(
    vosa::api::GraphBuilder &graph_builder,
    const std::shared_ptr<vosa::api::Op> &depth_map_as_plane,
    const std::shared_ptr<vosa::api::Op> &binary_mask_as_plane);

std::shared_ptr<vosa::api::Op> vosaGraphGaussianBlurY(
    vosa::api::GraphBuilder &graph_builder,
    const std::shared_ptr<vosa::api::Op> &depth_map_normalized,
    const std::shared_ptr<vosa::api::Op> &Y_channel);

std::shared_ptr<vosa::api::Op> vosaGraphGaussianBlurUV(
    vosa::api::GraphBuilder &graph_builder,
    const std::shared_ptr<vosa::api::Op> &depth_map_normalized,
    const std::shared_ptr<vosa::api::Op> &UV);

std::vector<std::shared_ptr<vosa::api::Op>> make_test_graph(
    vosa::api::GraphBuilder &, std::shared_ptr<vosa::api::Op> &depth_map,
    std::shared_ptr<vosa::api::Op> &binary_mask,
    std::shared_ptr<vosa::api::Op> &uv, std::shared_ptr<vosa::api::Op> &y);
} // namespace vosa::samples::bokeh

#endif // VOSA_PIPELINES_PIPELINES_BOKEH_BOKEH_H_
