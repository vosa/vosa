/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <unordered_map>

#include "vosa/Dialect/Vosa/IR/VosaDialect.h"
#include "vosa/Dialect/Vosa/IR/VosaOps.h"

#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/Linalg/IR/Linalg.h"
#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/IR/AsmState.h"
#include "mlir/Pass/Pass.h"
#include "mlir/Transforms/DialectConversion.h"
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"

#include "llvm/Support/Debug.h"

#include "ops.h"
#include "vosa/vosa.h"

#include "VosaSerialization.h"

#define DEBUG_TYPE "vosa-serialization"

namespace std {

template <>
struct hash<mlir::Value> {
  std::size_t operator()(const mlir::Value &val) const {
    return static_cast<std::size_t>(mlir::hash_value(val));
  }
};

template <>
struct equal_to<mlir::Value> {
  bool operator()(const mlir::Value &lhs, const mlir::Value &rhs) const {
    return (lhs == rhs);
  }
};

} // namespace std

namespace {

vosa::ArrayBase::DType convert_dtype(const mlir::Type &mlir_type) {
  if (mlir_type.isSignedInteger(8)) {
    return vosa::ArrayBase::DType::INT8;
  } else if (mlir_type.isSignedInteger(16)) {
    return vosa::ArrayBase::DType::INT16;
  } else if (mlir_type.isSignedInteger(32)) {
    return vosa::ArrayBase::DType::INT32;
  } else if (mlir_type.isUnsignedInteger(8)) {
    return vosa::ArrayBase::DType::UINT8;
  } else if (mlir_type.isUnsignedInteger(16)) {
    return vosa::ArrayBase::DType::UINT16;
  } else if (mlir_type.isUnsignedInteger(32)) {
    return vosa::ArrayBase::DType::UINT32;
  } else if (mlir_type.isF32()) {
    return vosa::ArrayBase::DType::FLOAT32;
  } else {
    llvm::errs() << "Attempted to convert an mlir type whose equivalent has "
                    "not been defined "
                 << mlir_type << "\n";
    return vosa::ArrayBase::DType::UNDEFINED;
  }
}

class VosaSerializer {
private:
  vosa::Graph graph;
  std::unordered_map<mlir::Value, std::string> name_map;

  bool add_input(mlir::BlockArgument &arg);

  template <typename T>
  bool add_operation(mlir::Operation &op);

  bool add_return(mlir::Operation &op);

public:
  mlir::LogicalResult translate_to_flatbuffer(mlir::func::FuncOp &func,
                                              const std::string &filename);
};

bool VosaSerializer::add_input(mlir::BlockArgument &arg) {
  auto argType = arg.getType();
  auto argShapedType = llvm::dyn_cast<mlir::ShapedType>(argType);
  if (!argShapedType) {
    llvm::errs() << "Invalid block arg, must be shaped type\n";
    return false;
  }
  auto argElementType = argShapedType.getElementType();
  auto argShape = argShapedType.getShape();

  auto dtype = convert_dtype(argElementType);
  if (dtype == vosa::ArrayBase::DType::UNDEFINED)
    return false;
  auto input = std::make_shared<vosa::Input>(dtype);
  std::vector<std::string> empty_operands;
  graph.add_op(input, empty_operands);
  graph.add_default_input(graph.get_name_for_op(input));
  auto name = graph.get_name_for_op(input);
  name_map[arg] = name;
  return true;
}

template <typename T>
vosa::EigenPlane<T>
convert_elements_attr_to_plane(std::vector<int64_t> plane_shape,
                               mlir::ElementsAttr &elements_attr) {
  auto raw_elements = elements_attr.getValues<T>();
  vosa::EigenPlane<T> plane{plane_shape[0], plane_shape[1]};
  for (uint32_t index_0 = 0; index_0 < plane_shape[0]; index_0++) {
    for (uint32_t index_1 = 0; index_1 < plane_shape[1]; index_1++) {
      plane(index_0, index_1) =
          raw_elements[index_0 * plane_shape[1] + index_1];
    }
  }
  return plane;
}

template <typename T>
T convert_elements_attr_to_value(mlir::ElementsAttr &elements_attr) {
  auto raw_elements = elements_attr.getValues<T>();
  return raw_elements[0];
}

template <typename T>
T convert_scalar_attr_to_value(mlir::Attribute &attr) {
  auto integer_attr = attr.cast<mlir::IntegerAttr>();
  return integer_attr.getValue().getSExtValue();
}

template <>
float convert_scalar_attr_to_value(mlir::Attribute &attr) {
  auto f32_attr = attr.cast<mlir::FloatAttr>();
  return f32_attr.getValue().convertToFloat();
}

template <typename T>
std::vector<T> convert_array_attr_to_vec(mlir::ArrayAttr &attr) {
  std::vector<T> vec;
  for (mlir::Attribute val : attr.getValue()) {
    vec.push_back(val.cast<mlir::IntegerAttr>().getValue().getSExtValue());
  }
  return vec;
}

int64_t convert_attr_to_int64(mlir::Attribute &attr) {
  return attr.cast<mlir::IntegerAttr>().getValue().getSExtValue();
}

mlir::Type getResultElementType(mlir::Operation &op, int idx = 0) {
  auto op_result = op.getOpResult(idx);
  auto op_result_type = op_result.getType();
  auto shaped_result_type = llvm::dyn_cast<mlir::ShapedType>(op_result_type);
  return shaped_result_type.getElementType();
}

mlir::Type getOperandElementType(mlir::Operation &op, int idx = 0) {
  auto &op_operand = op.getOpOperand(idx);
  auto op_result_type = op_operand.get().getType();
  auto shaped_result_type = llvm::dyn_cast<mlir::ShapedType>(op_result_type);
  return shaped_result_type.getElementType();
}

template <typename T>
std::shared_ptr<vosa::Op> make_reference_model_op(mlir::Operation &op) {
  llvm::errs() << "Serialization of Op " << op << "not implemented yet\n.";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::AbsOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);
  if (result_element_type.isSignedInteger(8)) {
    return std::make_shared<vosa::Abs<int8_t>>();
  } else if (result_element_type.isSignedInteger(16)) {
    return std::make_shared<vosa::Abs<int16_t>>();
  } else if (result_element_type.isSignedInteger(32)) {
    return std::make_shared<vosa::Abs<int32_t>>();
  } else if (result_element_type.isF32()) {
    return std::make_shared<vosa::Abs<float>>();
  }
  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::AddOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);
  if (result_element_type.isUnsignedInteger(8)) {
    return std::make_shared<vosa::Add<uint8_t, uint8_t>>();
  } else if (result_element_type.isUnsignedInteger(16)) {
    return std::make_shared<vosa::Add<uint16_t, uint16_t>>();
  } else if (result_element_type.isUnsignedInteger(32)) {
    return std::make_shared<vosa::Add<uint32_t, uint32_t>>();
  } else if (result_element_type.isSignedInteger(8)) {
    return std::make_shared<vosa::Add<int8_t, int8_t>>();
  } else if (result_element_type.isSignedInteger(16)) {
    return std::make_shared<vosa::Add<int16_t, int16_t>>();
  } else if (result_element_type.isSignedInteger(32)) {
    return std::make_shared<vosa::Add<int32_t, int32_t>>();
  } else if (result_element_type.isF32()) {
    return std::make_shared<vosa::Add<float, float>>();
  }
  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::BroadcastChannelwiseOp>(
    mlir::Operation &op) {
  auto size_attr = op.getAttr("size").cast<mlir::ArrayAttr>();
  auto vosa_size = convert_attr_to_int64(size_attr);
  auto result_element_type = getResultElementType(op);
  if (result_element_type.isUnsignedInteger(8)) {
    return std::make_shared<vosa::BroadcastChannelwise<uint8_t>>(vosa_size);
  } else if (result_element_type.isUnsignedInteger(16)) {
    return std::make_shared<vosa::BroadcastChannelwise<uint16_t>>(vosa_size);
  } else if (result_element_type.isUnsignedInteger(32)) {
    return std::make_shared<vosa::BroadcastChannelwise<uint32_t>>(vosa_size);
  } else if (result_element_type.isSignedInteger(8)) {
    return std::make_shared<vosa::BroadcastChannelwise<int8_t>>(vosa_size);
  } else if (result_element_type.isSignedInteger(16)) {
    return std::make_shared<vosa::BroadcastChannelwise<int16_t>>(vosa_size);
  } else if (result_element_type.isSignedInteger(32)) {
    return std::make_shared<vosa::BroadcastChannelwise<int32_t>>(vosa_size);
  } else if (result_element_type.isF32()) {
    return std::make_shared<vosa::BroadcastChannelwise<float>>(vosa_size);
  }
  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::BroadcastPlanewiseOp>(mlir::Operation &op) {
  auto size_attr = op.getAttr("size").cast<mlir::ArrayAttr>();
  auto vosa_size = convert_array_attr_to_vec<uint32_t>(size_attr);
  auto result_element_type = getResultElementType(op);
  if (result_element_type.isUnsignedInteger(8)) {
    return std::make_shared<vosa::BroadcastPlanewise<uint8_t>>(vosa_size);
  } else if (result_element_type.isUnsignedInteger(16)) {
    return std::make_shared<vosa::BroadcastPlanewise<uint16_t>>(vosa_size);
  } else if (result_element_type.isUnsignedInteger(32)) {
    return std::make_shared<vosa::BroadcastPlanewise<uint32_t>>(vosa_size);
  } else if (result_element_type.isSignedInteger(8)) {
    return std::make_shared<vosa::BroadcastPlanewise<int8_t>>(vosa_size);
  } else if (result_element_type.isSignedInteger(16)) {
    return std::make_shared<vosa::BroadcastPlanewise<int16_t>>(vosa_size);
  } else if (result_element_type.isSignedInteger(32)) {
    return std::make_shared<vosa::BroadcastPlanewise<int32_t>>(vosa_size);
  } else if (result_element_type.isF32()) {
    return std::make_shared<vosa::BroadcastPlanewise<float>>(vosa_size);
  }
  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <typename inputT, typename outputT>
std::shared_ptr<vosa::Op>
makeCast(vosa::CastBase::rounding_mode_t rounding_mode) {
  return std::make_shared<vosa::Cast<inputT, outputT>>(rounding_mode);
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::CastOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);
  auto result_vosa_type = convert_dtype(result_element_type);
  auto input_element_type = getOperandElementType(op);
  auto input_vosa_type = convert_dtype(input_element_type);

  typedef std::shared_ptr<vosa::Op> (*factory_ptr)(
      vosa::CastBase::rounding_mode_t);

  std::unordered_map<vosa::ArrayBase::DType, factory_ptr> bool_table = {
      {vosa::ArrayBase::DType::BOOL, makeCast<bool, bool>},
      {vosa::ArrayBase::DType::UINT8, makeCast<bool, uint8_t>},
      {vosa::ArrayBase::DType::UINT16, makeCast<bool, uint16_t>},
      {vosa::ArrayBase::DType::UINT32, makeCast<bool, uint32_t>},
      {vosa::ArrayBase::DType::INT8, makeCast<bool, int8_t>},
      {vosa::ArrayBase::DType::INT16, makeCast<bool, int16_t>},
      {vosa::ArrayBase::DType::INT32, makeCast<bool, int32_t>},
      {vosa::ArrayBase::DType::FLOAT32, makeCast<bool, float>}};

  std::unordered_map<vosa::ArrayBase::DType, factory_ptr> uint8_table = {
      {vosa::ArrayBase::DType::BOOL, makeCast<uint8_t, bool>},
      {vosa::ArrayBase::DType::UINT8, makeCast<uint8_t, uint8_t>},
      {vosa::ArrayBase::DType::UINT16, makeCast<uint8_t, uint16_t>},
      {vosa::ArrayBase::DType::UINT32, makeCast<uint8_t, uint32_t>},
      {vosa::ArrayBase::DType::INT8, makeCast<uint8_t, int8_t>},
      {vosa::ArrayBase::DType::INT16, makeCast<uint8_t, int16_t>},
      {vosa::ArrayBase::DType::INT32, makeCast<uint8_t, int32_t>},
      {vosa::ArrayBase::DType::FLOAT32, makeCast<uint8_t, float>}};

  std::unordered_map<vosa::ArrayBase::DType, factory_ptr> uint16_table = {
      {vosa::ArrayBase::DType::BOOL, makeCast<uint16_t, bool>},
      {vosa::ArrayBase::DType::UINT8, makeCast<uint16_t, uint8_t>},
      {vosa::ArrayBase::DType::UINT16, makeCast<uint16_t, uint16_t>},
      {vosa::ArrayBase::DType::UINT32, makeCast<uint16_t, uint32_t>},
      {vosa::ArrayBase::DType::INT8, makeCast<uint16_t, int8_t>},
      {vosa::ArrayBase::DType::INT16, makeCast<uint16_t, int16_t>},
      {vosa::ArrayBase::DType::INT32, makeCast<uint16_t, int32_t>},
      {vosa::ArrayBase::DType::FLOAT32, makeCast<uint16_t, float>}};

  std::unordered_map<vosa::ArrayBase::DType, factory_ptr> uint32_table = {
      {vosa::ArrayBase::DType::BOOL, makeCast<uint32_t, bool>},
      {vosa::ArrayBase::DType::UINT8, makeCast<uint32_t, uint8_t>},
      {vosa::ArrayBase::DType::UINT16, makeCast<uint32_t, uint16_t>},
      {vosa::ArrayBase::DType::UINT32, makeCast<uint32_t, uint32_t>},
      {vosa::ArrayBase::DType::INT8, makeCast<uint32_t, int8_t>},
      {vosa::ArrayBase::DType::INT16, makeCast<uint32_t, int16_t>},
      {vosa::ArrayBase::DType::INT32, makeCast<uint32_t, int32_t>},
      {vosa::ArrayBase::DType::FLOAT32, makeCast<uint32_t, float>}};

  std::unordered_map<vosa::ArrayBase::DType, factory_ptr> int8_table = {
      {vosa::ArrayBase::DType::BOOL, makeCast<int8_t, bool>},
      {vosa::ArrayBase::DType::UINT8, makeCast<int8_t, uint8_t>},
      {vosa::ArrayBase::DType::UINT16, makeCast<int8_t, uint16_t>},
      {vosa::ArrayBase::DType::UINT32, makeCast<int8_t, uint32_t>},
      {vosa::ArrayBase::DType::INT8, makeCast<int8_t, int8_t>},
      {vosa::ArrayBase::DType::INT16, makeCast<int8_t, int16_t>},
      {vosa::ArrayBase::DType::INT32, makeCast<int8_t, int32_t>},
      {vosa::ArrayBase::DType::FLOAT32, makeCast<int8_t, float>}};

  std::unordered_map<vosa::ArrayBase::DType, factory_ptr> int16_table = {
      {vosa::ArrayBase::DType::BOOL, makeCast<int16_t, bool>},
      {vosa::ArrayBase::DType::UINT8, makeCast<int16_t, uint8_t>},
      {vosa::ArrayBase::DType::UINT16, makeCast<int16_t, uint16_t>},
      {vosa::ArrayBase::DType::UINT32, makeCast<int16_t, uint32_t>},
      {vosa::ArrayBase::DType::INT8, makeCast<int16_t, int8_t>},
      {vosa::ArrayBase::DType::INT16, makeCast<int16_t, int16_t>},
      {vosa::ArrayBase::DType::INT32, makeCast<int16_t, int32_t>},
      {vosa::ArrayBase::DType::FLOAT32, makeCast<int16_t, float>}};

  std::unordered_map<vosa::ArrayBase::DType, factory_ptr> int32_table = {
      {vosa::ArrayBase::DType::BOOL, makeCast<int32_t, bool>},
      {vosa::ArrayBase::DType::UINT8, makeCast<int32_t, uint8_t>},
      {vosa::ArrayBase::DType::UINT16, makeCast<int32_t, uint16_t>},
      {vosa::ArrayBase::DType::UINT32, makeCast<int32_t, uint32_t>},
      {vosa::ArrayBase::DType::INT8, makeCast<int32_t, int8_t>},
      {vosa::ArrayBase::DType::INT16, makeCast<int32_t, int16_t>},
      {vosa::ArrayBase::DType::INT32, makeCast<int32_t, int32_t>},
      {vosa::ArrayBase::DType::FLOAT32, makeCast<int32_t, float>}};

  std::unordered_map<vosa::ArrayBase::DType, factory_ptr> float_table = {
      {vosa::ArrayBase::DType::BOOL, makeCast<float, bool>},
      {vosa::ArrayBase::DType::UINT8, makeCast<float, uint8_t>},
      {vosa::ArrayBase::DType::UINT16, makeCast<float, uint16_t>},
      {vosa::ArrayBase::DType::UINT32, makeCast<float, uint32_t>},
      {vosa::ArrayBase::DType::INT8, makeCast<float, int8_t>},
      {vosa::ArrayBase::DType::INT16, makeCast<float, int16_t>},
      {vosa::ArrayBase::DType::INT32, makeCast<float, int32_t>},
      {vosa::ArrayBase::DType::FLOAT32, makeCast<float, float>}};

  std::unordered_map<vosa::ArrayBase::DType,
                     std::unordered_map<vosa::ArrayBase::DType, factory_ptr>>
      lookup;
  lookup[vosa::ArrayBase::DType::BOOL] = bool_table;
  lookup[vosa::ArrayBase::DType::UINT8] = uint8_table;
  lookup[vosa::ArrayBase::DType::UINT16] = uint16_table;
  lookup[vosa::ArrayBase::DType::UINT32] = uint32_table;
  lookup[vosa::ArrayBase::DType::INT8] = int8_table;
  lookup[vosa::ArrayBase::DType::INT16] = int16_table;
  lookup[vosa::ArrayBase::DType::INT32] = int32_table;
  lookup[vosa::ArrayBase::DType::FLOAT32] = float_table;

  auto rounding_mode_attr = op.getAttr("rounding_mode");
  auto rounding_mode =
      rounding_mode_attr.cast<mlir::vosa::RoundingModeAttr>().getValue();
  vosa::CastBase::rounding_mode_t vosa_rounding_mode;
  switch (rounding_mode) {
  case mlir::vosa::RoundingMode::wrap:
    vosa_rounding_mode = vosa::CastBase::VOSA_ROUNDING_MODE_WRAP;
    break;
  case mlir::vosa::RoundingMode::saturate:
    vosa_rounding_mode = vosa::CastBase::VOSA_ROUNDING_MODE_SATURATE;
    break;
  case mlir::vosa::RoundingMode::reinterpret:
    llvm::errs() << "Reinterpret case not supported for reference model "
                    "serialization.\n";
    return {nullptr};
  }

  return lookup.at(input_vosa_type).at(result_vosa_type)(vosa_rounding_mode);

  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::ChannelExtractOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);
  auto channels_attr = op.getAttr("channels");
  auto channels_array_attr = channels_attr.cast<mlir::ArrayAttr>();
  auto vosa_channels_attr =
      convert_array_attr_to_vec<uint32_t>(channels_array_attr);
  if (result_element_type.isUnsignedInteger(8)) {
    return std::make_shared<vosa::ChannelExtract<uint8_t>>(vosa_channels_attr);
  } else if (result_element_type.isUnsignedInteger(16)) {
    return std::make_shared<vosa::ChannelExtract<uint16_t>>(vosa_channels_attr);
  } else if (result_element_type.isUnsignedInteger(32)) {
    return std::make_shared<vosa::ChannelExtract<uint32_t>>(vosa_channels_attr);
  } else if (result_element_type.isSignedInteger(8)) {
    return std::make_shared<vosa::ChannelExtract<int8_t>>(vosa_channels_attr);
  } else if (result_element_type.isSignedInteger(16)) {
    return std::make_shared<vosa::ChannelExtract<int16_t>>(vosa_channels_attr);
  } else if (result_element_type.isSignedInteger(32)) {
    return std::make_shared<vosa::ChannelExtract<int32_t>>(vosa_channels_attr);
  } else if (result_element_type.isF32()) {
    return std::make_shared<vosa::ChannelExtract<float>>(vosa_channels_attr);
  }
  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::ClampOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);
  if (result_element_type.isUnsignedInteger(8)) {
    return std::make_shared<vosa::Clamp<uint8_t>>();
  } else if (result_element_type.isUnsignedInteger(16)) {
    return std::make_shared<vosa::Clamp<uint16_t>>();
  } else if (result_element_type.isUnsignedInteger(32)) {
    return std::make_shared<vosa::Clamp<uint32_t>>();
  } else if (result_element_type.isSignedInteger(8)) {
    return std::make_shared<vosa::Clamp<int8_t>>();
  } else if (result_element_type.isSignedInteger(16)) {
    return std::make_shared<vosa::Clamp<int16_t>>();
  } else if (result_element_type.isSignedInteger(32)) {
    return std::make_shared<vosa::Clamp<int32_t>>();
  } else if (result_element_type.isF32()) {
    return std::make_shared<vosa::Clamp<float>>();
  }
  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::ConcatOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);
  if (result_element_type.isUnsignedInteger(8)) {
    return std::make_shared<vosa::Concat<uint8_t>>();
  } else if (result_element_type.isUnsignedInteger(16)) {
    return std::make_shared<vosa::Concat<uint16_t>>();
  } else if (result_element_type.isUnsignedInteger(32)) {
    return std::make_shared<vosa::Concat<uint32_t>>();
  } else if (result_element_type.isSignedInteger(8)) {
    return std::make_shared<vosa::Concat<int8_t>>();
  } else if (result_element_type.isSignedInteger(16)) {
    return std::make_shared<vosa::Concat<int16_t>>();
  } else if (result_element_type.isSignedInteger(32)) {
    return std::make_shared<vosa::Concat<int32_t>>();
  } else if (result_element_type.isF32()) {
    return std::make_shared<vosa::Concat<float>>();
  }
  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

std::shared_ptr<vosa::ConstImageBase> make_const_image(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);
  std::vector<uint32_t> vosa_shape;
  auto result_shape =
      op.getResult(0).getType().cast<mlir::ShapedType>().getShape().vec();
  for (auto v : result_shape)
    vosa_shape.push_back(v);
  auto value_attr = op.getAttr("value");
  auto elements_attr = value_attr.cast<mlir::ElementsAttr>();
  if (result_element_type.isF32()) {
    auto value = convert_elements_attr_to_value<float>(elements_attr);
    return std::make_shared<vosa::ConstImage<float>>(vosa_shape, value);
  }
  llvm::errs() << "Cannot serialize " << op << " to const image\n";
  return {nullptr};
}

std::shared_ptr<vosa::ConstScalarBase> make_const_scalar(mlir::Operation &op) {
  auto result_type = op.getResult(0).getType();
  auto value_attr = op.getAttr("value");
  if (result_type.isUnsignedInteger(8)) {
    auto value = convert_scalar_attr_to_value<uint8_t>(value_attr);
    return std::make_shared<vosa::ConstScalar<uint8_t>>(value);
  } else if (result_type.isUnsignedInteger(16)) {
    auto value = convert_scalar_attr_to_value<uint16_t>(value_attr);
    return std::make_shared<vosa::ConstScalar<uint16_t>>(value);
  } else if (result_type.isUnsignedInteger(32)) {
    auto value = convert_scalar_attr_to_value<uint32_t>(value_attr);
    return std::make_shared<vosa::ConstScalar<uint32_t>>(value);
  } else if (result_type.isSignedInteger(8)) {
    auto value = convert_scalar_attr_to_value<int8_t>(value_attr);
    return std::make_shared<vosa::ConstScalar<int8_t>>(value);
  } else if (result_type.isSignedInteger(16)) {
    auto value = convert_scalar_attr_to_value<int16_t>(value_attr);
    return std::make_shared<vosa::ConstScalar<int16_t>>(value);
  } else if (result_type.isSignedInteger(32)) {
    auto value = convert_scalar_attr_to_value<int32_t>(value_attr);
    return std::make_shared<vosa::ConstScalar<int32_t>>(value);
  }
  if (result_type.isF32()) {
    auto value = convert_scalar_attr_to_value<float>(value_attr);
    return std::make_shared<vosa::ConstScalar<float>>(value);
  }
  llvm::errs() << "Cannot serialize " << op << " to const scalar\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::ConstantOp>(mlir::Operation &op) {
  auto result_type = op.getResult(0).getType();
  if (llvm::isa<mlir::ShapedType>(result_type)) { // Const Image
    return make_const_image(op);
  } else { // Const Scalar
    return make_const_scalar(op);
  }
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::Conv2DOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);
  auto op_filter = op.getAttr("filter");
  auto op_filter_typed = llvm::cast<mlir::TypedAttr>(op_filter);
  auto op_filter_shaped_type =
      op_filter_typed.getType().cast<mlir::ShapedType>();
  auto filter_shape = op_filter_shaped_type.getShape().vec();
  auto elements_attr = op_filter.cast<mlir::ElementsAttr>();

  if (result_element_type.isUnsignedInteger(8)) {
    vosa::EigenPlane<uint8_t> vosa_filter =
        convert_elements_attr_to_plane<uint8_t>(filter_shape, elements_attr);
    return std::make_shared<vosa::Conv<uint8_t, uint8_t>>(vosa_filter);
  } else if (result_element_type.isUnsignedInteger(16)) {
    vosa::EigenPlane<uint16_t> vosa_filter =
        convert_elements_attr_to_plane<uint16_t>(filter_shape, elements_attr);
    return std::make_shared<vosa::Conv<uint16_t, uint16_t>>(vosa_filter);
  } else if (result_element_type.isUnsignedInteger(32)) {
    vosa::EigenPlane<uint32_t> vosa_filter =
        convert_elements_attr_to_plane<uint32_t>(filter_shape, elements_attr);
    return std::make_shared<vosa::Conv<uint32_t, uint32_t>>(vosa_filter);
  } else if (result_element_type.isSignedInteger(8)) {
    vosa::EigenPlane<int8_t> vosa_filter =
        convert_elements_attr_to_plane<int8_t>(filter_shape, elements_attr);
    return std::make_shared<vosa::Conv<int8_t, int8_t>>(vosa_filter);
  } else if (result_element_type.isSignedInteger(16)) {
    vosa::EigenPlane<int16_t> vosa_filter =
        convert_elements_attr_to_plane<int16_t>(filter_shape, elements_attr);
    return std::make_shared<vosa::Conv<int16_t, int16_t>>(vosa_filter);
  } else if (result_element_type.isSignedInteger(32)) {
    vosa::EigenPlane<int32_t> vosa_filter =
        convert_elements_attr_to_plane<int32_t>(filter_shape, elements_attr);
    return std::make_shared<vosa::Conv<int32_t, int32_t>>(vosa_filter);
  }

  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::DivOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);
  if (result_element_type.isF32()) {
    return std::make_shared<vosa::Div<float>>();
  }
  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::ImportChannelOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);

  auto op_stride_attr = op.getAttr("stride");
  auto vosa_stride = convert_attr_to_int64(op_stride_attr);

  auto op_offset_attr = op.getAttr("offset");
  auto vosa_offset = convert_attr_to_int64(op_offset_attr);

  auto op_shape_attr = op.getAttr("shape").cast<mlir::ArrayAttr>();
  auto vosa_shape = convert_array_attr_to_vec<uint32_t>(op_shape_attr);

  if (result_element_type.isUnsignedInteger(8)) {
    return std::make_shared<vosa::ImportChannel<uint8_t>>(
        vosa_stride, vosa_offset, vosa_shape);
  } else if (result_element_type.isUnsignedInteger(16)) {
    return std::make_shared<vosa::ImportChannel<uint16_t>>(
        vosa_stride, vosa_offset, vosa_shape);
  } else if (result_element_type.isUnsignedInteger(32)) {
    return std::make_shared<vosa::ImportChannel<uint32_t>>(
        vosa_stride, vosa_offset, vosa_shape);
  } else if (result_element_type.isSignedInteger(8)) {
    return std::make_shared<vosa::ImportChannel<int8_t>>(
        vosa_stride, vosa_offset, vosa_shape);
  } else if (result_element_type.isSignedInteger(16)) {
    return std::make_shared<vosa::ImportChannel<int16_t>>(
        vosa_stride, vosa_offset, vosa_shape);
  } else if (result_element_type.isSignedInteger(32)) {
    return std::make_shared<vosa::ImportChannel<int32_t>>(
        vosa_stride, vosa_offset, vosa_shape);
  } else if (result_element_type.isF32()) {
    return std::make_shared<vosa::ImportChannel<float>>(
        vosa_stride, vosa_offset, vosa_shape);
  }
  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::LogicalShiftRightOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);

  auto op_shift_attr = op.getAttr("shift");

  auto vosa_shift = convert_attr_to_int64(op_shift_attr);

  if (result_element_type.isUnsignedInteger(8)) {
    return std::make_shared<vosa::LogicalShiftRight<uint8_t>>(vosa_shift);
  } else if (result_element_type.isUnsignedInteger(16)) {
    return std::make_shared<vosa::LogicalShiftRight<uint16_t>>(vosa_shift);
  } else if (result_element_type.isUnsignedInteger(32)) {
    return std::make_shared<vosa::LogicalShiftRight<uint32_t>>(vosa_shift);
  }
  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::MultOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);
  if (result_element_type.isUnsignedInteger(8)) {
    return std::make_shared<vosa::Mult<uint8_t, uint8_t>>();
  } else if (result_element_type.isUnsignedInteger(16)) {
    return std::make_shared<vosa::Mult<uint16_t, uint16_t>>();
  } else if (result_element_type.isUnsignedInteger(32)) {
    return std::make_shared<vosa::Mult<uint32_t, uint32_t>>();
  } else if (result_element_type.isSignedInteger(8)) {
    return std::make_shared<vosa::Mult<int8_t, int8_t>>();
  } else if (result_element_type.isSignedInteger(16)) {
    return std::make_shared<vosa::Mult<int16_t, int16_t>>();
  } else if (result_element_type.isSignedInteger(32)) {
    return std::make_shared<vosa::Mult<int32_t, int32_t>>();
  } else if (result_element_type.isF32()) {
    return std::make_shared<vosa::Mult<float, float>>();
  }
  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::PadOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);

  auto pad_op = mlir::dyn_cast<mlir::vosa::PadOp>(op);
  auto pad_constant_attr = pad_op.getPadConstant();

  int64_t vosa_pad_constant = 0;
  if (pad_constant_attr)
    vosa_pad_constant = convert_attr_to_int64(pad_constant_attr.value());

  auto pad_mode_attr = op.getAttr("mode").cast<mlir::vosa::PadModeAttr>();
  vosa::PadBase::pad_mode_t vosa_pad_mode;
  switch (pad_mode_attr.getValue()) {
  case mlir::vosa::PadMode::constant:
    vosa_pad_mode = vosa::PadBase::pad_mode_t::VOSA_PAD_MODE_CONSTANT;
    break;
  case mlir::vosa::PadMode::mirror:
    vosa_pad_mode = vosa::PadBase::pad_mode_t::VOSA_PAD_MODE_MIRROR;
    break;
  case mlir::vosa::PadMode::reflect:
    vosa_pad_mode = vosa::PadBase::pad_mode_t::VOSA_PAD_MODE_REFLECT;
    break;
  case mlir::vosa::PadMode::replicate:
    vosa_pad_mode = vosa::PadBase::pad_mode_t::VOSA_PAD_MODE_REPLICATE;
    break;
  }

  auto pad_size_attr = op.getAttr("pad_size").cast<mlir::ArrayAttr>();
  auto vosa_pad_size = convert_array_attr_to_vec<uint32_t>(pad_size_attr);

  if (result_element_type.isUnsignedInteger(8)) {
    return std::make_shared<vosa::Pad<uint8_t>>(vosa_pad_constant,
                                                vosa_pad_mode, vosa_pad_size);
  } else if (result_element_type.isUnsignedInteger(16)) {
    return std::make_shared<vosa::Pad<uint16_t>>(vosa_pad_constant,
                                                 vosa_pad_mode, vosa_pad_size);
  } else if (result_element_type.isUnsignedInteger(32)) {
    return std::make_shared<vosa::Pad<uint32_t>>(vosa_pad_constant,
                                                 vosa_pad_mode, vosa_pad_size);
  } else if (result_element_type.isSignedInteger(8)) {
    return std::make_shared<vosa::Pad<int8_t>>(vosa_pad_constant, vosa_pad_mode,
                                               vosa_pad_size);
  } else if (result_element_type.isSignedInteger(16)) {
    return std::make_shared<vosa::Pad<int16_t>>(vosa_pad_constant,
                                                vosa_pad_mode, vosa_pad_size);
  } else if (result_element_type.isSignedInteger(32)) {
    return std::make_shared<vosa::Pad<int32_t>>(vosa_pad_constant,
                                                vosa_pad_mode, vosa_pad_size);
  }

  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::ReduceInterpChannelwiseOp>(
    mlir::Operation &op) {
  auto operand_element_type = getOperandElementType(op);
  if (operand_element_type.isUnsignedInteger(8)) {
    return std::make_shared<vosa::ReduceInterpChannelwise<uint8_t>>();
  } else if (operand_element_type.isUnsignedInteger(16)) {
    return std::make_shared<vosa::ReduceInterpChannelwise<uint16_t>>();
  } else if (operand_element_type.isUnsignedInteger(32)) {
    return std::make_shared<vosa::ReduceInterpChannelwise<uint32_t>>();
  } else if (operand_element_type.isSignedInteger(8)) {
    return std::make_shared<vosa::ReduceInterpChannelwise<int8_t>>();
  } else if (operand_element_type.isSignedInteger(16)) {
    return std::make_shared<vosa::ReduceInterpChannelwise<int16_t>>();
  } else if (operand_element_type.isSignedInteger(32)) {
    return std::make_shared<vosa::ReduceInterpChannelwise<int32_t>>();
  } else if (operand_element_type.isF32()) {
    return std::make_shared<vosa::ReduceInterpChannelwise<float>>();
  }
  llvm::errs() << "Cannot serialize " << op << " with result type "
               << operand_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::ReduceMaxPlanewiseOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);

  if (result_element_type.isUnsignedInteger(8)) {
    return std::make_shared<vosa::ReduceMaxPlanewise<uint8_t>>();
  } else if (result_element_type.isUnsignedInteger(16)) {
    return std::make_shared<vosa::ReduceMaxPlanewise<uint16_t>>();
  } else if (result_element_type.isUnsignedInteger(32)) {
    return std::make_shared<vosa::ReduceMaxPlanewise<uint32_t>>();
  } else if (result_element_type.isSignedInteger(8)) {
    return std::make_shared<vosa::ReduceMaxPlanewise<int8_t>>();
  } else if (result_element_type.isSignedInteger(16)) {
    return std::make_shared<vosa::ReduceMaxPlanewise<int16_t>>();
  } else if (result_element_type.isSignedInteger(32)) {
    return std::make_shared<vosa::ReduceMaxPlanewise<int32_t>>();
  } else if (result_element_type.isF32()) {
    return std::make_shared<vosa::ReduceMaxPlanewise<float>>();
  }

  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::ReduceSumPlanewiseOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);

  if (result_element_type.isUnsignedInteger(8)) {
    return std::make_shared<vosa::ReduceSumPlanewise<uint8_t, uint8_t>>();
  } else if (result_element_type.isUnsignedInteger(16)) {
    return std::make_shared<vosa::ReduceSumPlanewise<uint16_t, uint16_t>>();
  } else if (result_element_type.isUnsignedInteger(32)) {
    return std::make_shared<vosa::ReduceSumPlanewise<uint32_t, uint32_t>>();
  } else if (result_element_type.isSignedInteger(8)) {
    return std::make_shared<vosa::ReduceSumPlanewise<int8_t, int8_t>>();
  } else if (result_element_type.isSignedInteger(16)) {
    return std::make_shared<vosa::ReduceSumPlanewise<int16_t, int16_t>>();
  } else if (result_element_type.isSignedInteger(32)) {
    return std::make_shared<vosa::ReduceSumPlanewise<int32_t, int32_t>>();
  } else if (result_element_type.isF32()) {
    return std::make_shared<vosa::ReduceSumPlanewise<float, float>>();
  }

  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::ResizeBilinearOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);

  auto size_attr = op.getAttr("size").cast<mlir::ArrayAttr>();
  auto vosa_size = convert_array_attr_to_vec<uint32_t>(size_attr);

  if (result_element_type.isF32()) {
    return std::make_shared<vosa::ResizeBilinear<float>>(vosa_size);
  }

  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <>
std::shared_ptr<vosa::Op>
make_reference_model_op<mlir::vosa::SubOp>(mlir::Operation &op) {
  auto result_element_type = getResultElementType(op);
  if (result_element_type.isUnsignedInteger(8)) {
    return std::make_shared<vosa::Sub<uint8_t, uint8_t>>();
  } else if (result_element_type.isUnsignedInteger(16)) {
    return std::make_shared<vosa::Sub<uint16_t, uint16_t>>();
  } else if (result_element_type.isUnsignedInteger(32)) {
    return std::make_shared<vosa::Sub<uint32_t, uint32_t>>();
  } else if (result_element_type.isSignedInteger(8)) {
    return std::make_shared<vosa::Sub<int8_t, int8_t>>();
  } else if (result_element_type.isSignedInteger(16)) {
    return std::make_shared<vosa::Sub<int16_t, int16_t>>();
  } else if (result_element_type.isSignedInteger(32)) {
    return std::make_shared<vosa::Sub<int32_t, int32_t>>();
  } else if (result_element_type.isF32()) {
    return std::make_shared<vosa::Sub<float, float>>();
  }
  llvm::errs() << "Cannot serialize " << op << " with result type "
               << result_element_type << "\n";
  return {nullptr};
}

template <typename T>
bool VosaSerializer::add_operation(mlir::Operation &op) {
  auto reference_model_op = make_reference_model_op<T>(op);
  if (!reference_model_op) {
    llvm::errs() << "Creation of reference model op matching " << op
                 << " failed\n";
    return false;
  }
  std::vector<std::string> operand_names;
  for (auto &operand : op.getOpOperands()) {
    operand_names.push_back(name_map[operand.get()]);
  }
  graph.add_op(reference_model_op, operand_names);
  auto op_name = graph.get_name_for_op(reference_model_op);
  auto op_result = op.getOpResult(0);
  name_map[op_result] = op_name;
  return true;
}

bool VosaSerializer::add_return(mlir::Operation &op) {
  for (auto &operand : op.getOpOperands()) {
    graph.add_default_output(name_map[operand.get()]);
  }
  return true;
}

mlir::LogicalResult
VosaSerializer::translate_to_flatbuffer(mlir::func::FuncOp &func,
                                        const std::string &filename) {
  mlir::Region *main_region = func.getCallableRegion();

  if (!main_region) {
    llvm::errs() << "Invalid MLIR: doesn't have valid \"main\" region\n";
    return mlir::failure();
  }

  auto &blocks = main_region->getBlocks();
  if (blocks.size() != 1) {
    llvm::errs()
        << "Invalid MLIR: doesn't have exactly one block in \"main\" region\n";
    return mlir::failure();
  }
  auto &block = blocks.front();
  for (auto &arg : block.getArguments()) {
    auto add_input_success = add_input(arg);
    if (!add_input_success) {
      llvm::errs() << "Failed to add argument " << arg << "\n";
    }
  }
  for (auto &op : block.getOperations()) {
    bool op_add_success;
    if (llvm::isa<mlir::vosa::AbsOp>(op)) {
      op_add_success = add_operation<mlir::vosa::AbsOp>(op);
    } else if (llvm::isa<mlir::vosa::AddOp>(op)) {
      op_add_success = add_operation<mlir::vosa::AddOp>(op);
    } else if (llvm::isa<mlir::vosa::BroadcastChannelwiseOp>(op)) {
      op_add_success = add_operation<mlir::vosa::BroadcastChannelwiseOp>(op);
    } else if (llvm::isa<mlir::vosa::BroadcastPlanewiseOp>(op)) {
      op_add_success = add_operation<mlir::vosa::BroadcastPlanewiseOp>(op);
    } else if (llvm::isa<mlir::vosa::CastOp>(op)) {
      op_add_success = add_operation<mlir::vosa::CastOp>(op);
    } else if (llvm::isa<mlir::vosa::ChannelExtractOp>(op)) {
      op_add_success = add_operation<mlir::vosa::ChannelExtractOp>(op);
    } else if (llvm::isa<mlir::vosa::ClampOp>(op)) {
      op_add_success = add_operation<mlir::vosa::ClampOp>(op);
    } else if (llvm::isa<mlir::vosa::ConcatOp>(op)) {
      op_add_success = add_operation<mlir::vosa::ConcatOp>(op);
    } else if (llvm::isa<mlir::vosa::ConstantOp>(op)) {
      op_add_success = add_operation<mlir::vosa::ConstantOp>(op);
    } else if (llvm::isa<mlir::vosa::Conv2DOp>(op)) {
      op_add_success = add_operation<mlir::vosa::Conv2DOp>(op);
    } else if (llvm::isa<mlir::vosa::DivOp>(op)) {
      op_add_success = add_operation<mlir::vosa::DivOp>(op);
    } else if (llvm::isa<mlir::vosa::ImportChannelOp>(op)) {
      op_add_success = add_operation<mlir::vosa::ImportChannelOp>(op);
    } else if (llvm::isa<mlir::vosa::LogicalShiftRightOp>(op)) {
      op_add_success = add_operation<mlir::vosa::LogicalShiftRightOp>(op);
    } else if (llvm::isa<mlir::vosa::MultOp>(op)) {
      op_add_success = add_operation<mlir::vosa::MultOp>(op);
    } else if (llvm::isa<mlir::vosa::PadOp>(op)) {
      op_add_success = add_operation<mlir::vosa::PadOp>(op);
    } else if (llvm::isa<mlir::vosa::ReduceInterpChannelwiseOp>(op)) {
      op_add_success = add_operation<mlir::vosa::ReduceInterpChannelwiseOp>(op);
    } else if (llvm::isa<mlir::vosa::ReduceMaxPlanewiseOp>(op)) {
      op_add_success = add_operation<mlir::vosa::ReduceMaxPlanewiseOp>(op);
    } else if (llvm::isa<mlir::vosa::ReduceSumPlanewiseOp>(op)) {
      op_add_success = add_operation<mlir::vosa::ReduceSumPlanewiseOp>(op);
    } else if (llvm::isa<mlir::vosa::ResizeBilinearOp>(op)) {
      op_add_success = add_operation<mlir::vosa::ResizeBilinearOp>(op);
    } else if (llvm::isa<mlir::vosa::SubOp>(op)) {
      op_add_success = add_operation<mlir::vosa::SubOp>(op);
    } else if (llvm::isa<mlir::func::ReturnOp>(op)) {
      op_add_success = add_return(op);
    } else {
      llvm::errs() << "Vosa Serialization doesnt know how to process " << op
                   << "\n";
      return mlir::failure();
    }
    if (!op_add_success) {
      llvm::errs() << "Failed to add operation " << op << "\n";
      return mlir::failure();
    }
  }

  graph.write_flatbuffer(filename);
  return mlir::success();
}

class VosaSerializationPass
    : public mlir::vosa::impl::VosaSerializationBase<VosaSerializationPass> {

public:
  explicit VosaSerializationPass(
      const mlir::vosa::VosaSerializationOptions &options)
      : mlir::vosa::impl::VosaSerializationBase<VosaSerializationPass>(
            options) {}

  void runOnOperation() override {
    auto function = getOperation();
    VosaSerializer serializer;

    if (serializer.translate_to_flatbuffer(function, this->filename).failed()) {
      llvm::errs() << "Failed to generate VOSA flatbuffer...\n";
      return signalPassFailure();
    }
  }
};

} // namespace

/// Create "VOSA Serialization" pass
std::unique_ptr<mlir::Pass>
mlir::vosa::createVosaSerializationPass(const std::string &filename) {
  LLVM_DEBUG(llvm::dbgs() << "createVosaSerialization\n";);
  VosaSerializationOptions options;
  options.filename = filename;
  return std::make_unique<VosaSerializationPass>(options);
}
