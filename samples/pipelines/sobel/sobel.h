/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_SOBEL_H
#define VOSA_SOBEL_H

#include "vosa/api/graph_builder.h"
#include "vosa/api/ops.h"

namespace vosa::samples::sobel {

std::shared_ptr<vosa::api::Op>
make_sobel_graph(vosa::api::GraphBuilder &graph_builder);

}

#endif // VOSA_SOBEL_H
