/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/round.h"
#include "test_bench.h"
#include "vosa/vosa.h"

using namespace vosa;

static EigenImage<float> get_input() {
  EigenImage<float> input(1, 1, 18);
  input.setValues({{{-24.7, -24.5, -24.2, -23.7, -23.5, -23.2, -0.7, -0.5, -0.2,
                     0.2, 0.5, 0.7, 23.2, 23.5, 23.7, 24.2, 24.5, 24.7}}});
  return input;
}

static EigenImage<int32_t>
round(const EigenImage<float> &input,
      const vosa::RoundBase::round_method_t &round_method) {
  auto Op = Round<float>(round_method);

  auto input_wrapped = std::make_shared<ArrayBase>(input);
  auto output_wrapped = Op.eval({input_wrapped});

  return output_wrapped->get_image<int32_t>();
}

TEST(test_round, test_round_floor) {
  EigenImage<float> input = get_input();
  EigenImage<int32_t> expected(input.dimension(0), input.dimension(1),
                               input.dimension(2));
  expected.setValues({{{-25, -25, -25, -24, -24, -24, -1, -1, -1, 0, 0, 0, 23,
                        23, 23, 24, 24, 24}}});

  EigenImage<int32_t> actual =
      round(input, vosa::RoundBase::round_method_t::FLOOR);

  bool result =
      test::TestBench<int32_t, int32_t>::compareEigenImages(expected, actual);
  EXPECT_TRUE(result);
}

TEST(test_round, test_round_ceil) {
  EigenImage<float> input = get_input();
  EigenImage<int32_t> expected(input.dimension(0), input.dimension(1),
                               input.dimension(2));
  expected.setValues({{{-24, -24, -24, -23, -23, -23, 0, 0, 0, 1, 1, 1, 24, 24,
                        24, 25, 25, 25}}});

  EigenImage<int32_t> actual =
      round(input, vosa::RoundBase::round_method_t::CEIL);

  bool result =
      test::TestBench<int32_t, int32_t>::compareEigenImages(expected, actual);
  EXPECT_TRUE(result);
}

TEST(test_round, test_round_towards_zero) {
  EigenImage<float> input = get_input();
  EigenImage<int32_t> expected(input.dimension(0), input.dimension(1),
                               input.dimension(2));
  expected.setValues({{{-24, -24, -24, -23, -23, -23, 0, 0, 0, 0, 0, 0, 23, 23,
                        23, 24, 24, 24}}});

  EigenImage<int32_t> actual =
      round(input, vosa::RoundBase::round_method_t::TOWARDS_ZERO);

  bool result =
      test::TestBench<int32_t, int32_t>::compareEigenImages(expected, actual);
  EXPECT_TRUE(result);
}

TEST(test_round, test_round_away_from_zero) {
  EigenImage<float> input = get_input();
  EigenImage<int32_t> expected(input.dimension(0), input.dimension(1),
                               input.dimension(2));
  expected.setValues({{{-25, -25, -25, -24, -24, -24, -1, -1, -1, 1, 1, 1, 24,
                        24, 24, 25, 25, 25}}});

  EigenImage<int32_t> actual =
      round(input, vosa::RoundBase::round_method_t::AWAY_FROM_ZERO);

  bool result =
      test::TestBench<int32_t, int32_t>::compareEigenImages(expected, actual);
  EXPECT_TRUE(result);
}

TEST(test_round, test_round_half_up) {
  EigenImage<float> input = get_input();
  EigenImage<int32_t> expected(input.dimension(0), input.dimension(1),
                               input.dimension(2));
  expected.setValues({{{-25, -24, -24, -24, -23, -23, -1, 0, 0, 0, 1, 1, 23, 24,
                        24, 24, 25, 25}}});

  EigenImage<int32_t> actual =
      round(input, vosa::RoundBase::round_method_t::HALF_UP);

  bool result =
      test::TestBench<int32_t, int32_t>::compareEigenImages(expected, actual);
  EXPECT_TRUE(result);
}

TEST(test_round, test_round_half_down) {
  EigenImage<float> input = get_input();
  EigenImage<int32_t> expected(input.dimension(0), input.dimension(1),
                               input.dimension(2));
  expected.setValues({{{-25, -25, -24, -24, -24, -23, -1, -1, 0, 0, 0, 1, 23,
                        23, 24, 24, 24, 25}}});

  EigenImage<int32_t> actual =
      round(input, vosa::RoundBase::round_method_t::HALF_DOWN);

  bool result =
      test::TestBench<int32_t, int32_t>::compareEigenImages(expected, actual);
  EXPECT_TRUE(result);
}

TEST(test_round, test_round_half_towards_zero) {
  EigenImage<float> input = get_input();
  EigenImage<int32_t> expected(input.dimension(0), input.dimension(1),
                               input.dimension(2));
  expected.setValues({{{-25, -24, -24, -24, -23, -23, -1, 0, 0, 0, 0, 1, 23, 23,
                        24, 24, 24, 25}}});

  EigenImage<int32_t> actual =
      round(input, vosa::RoundBase::round_method_t::HALF_TOWARDS_ZERO);

  bool result =
      test::TestBench<int32_t, int32_t>::compareEigenImages(expected, actual);
  EXPECT_TRUE(result);
}

TEST(test_round, test_round_half_away_from_zero) {
  EigenImage<float> input = get_input();
  EigenImage<int32_t> expected(input.dimension(0), input.dimension(1),
                               input.dimension(2));
  expected.setValues({{{-25, -25, -24, -24, -24, -23, -1, -1, 0, 0, 1, 1, 23,
                        24, 24, 24, 25, 25}}});

  EigenImage<int32_t> actual =
      round(input, vosa::RoundBase::round_method_t::HALF_AWAY_FROM_ZERO);

  bool result =
      test::TestBench<int32_t, int32_t>::compareEigenImages(expected, actual);
  EXPECT_TRUE(result);
}

TEST(test_round, test_round_half_to_even) {
  EigenImage<float> input = get_input();
  EigenImage<int32_t> expected(input.dimension(0), input.dimension(1),
                               input.dimension(2));
  expected.setValues({{{-25, -24, -24, -24, -24, -23, -1, 0, 0, 0, 0, 1, 23, 24,
                        24, 24, 24, 25}}});

  EigenImage<int32_t> actual =
      round(input, vosa::RoundBase::round_method_t::HALF_TO_EVEN);

  bool result =
      test::TestBench<int32_t, int32_t>::compareEigenImages(expected, actual);
  EXPECT_TRUE(result);
}

TEST(test_round, test_round_half_to_odd) {
  EigenImage<float> input = get_input();
  EigenImage<int32_t> expected(input.dimension(0), input.dimension(1),
                               input.dimension(2));
  expected.setValues({{{-25, -25, -24, -24, -23, -23, -1, -1, 0, 0, 1, 1, 23,
                        23, 24, 24, 25, 25}}});

  EigenImage<int32_t> actual =
      round(input, vosa::RoundBase::round_method_t::HALF_TO_ODD);

  bool result =
      test::TestBench<int32_t, int32_t>::compareEigenImages(expected, actual);
  EXPECT_TRUE(result);
}
