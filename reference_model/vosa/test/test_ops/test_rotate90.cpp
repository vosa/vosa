/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/rotate90.h"
#include "test_bench.h"
#include "vosa/vosa.h"

using namespace std;
using namespace vosa;

template <typename T>
static EigenImage<T> rotate90(EigenImage<T> &input_array, const int rotation) {
  auto Op = Rotate90<T>(rotation);

  auto input = make_shared<ArrayBase>(input_array);
  auto output = Op.eval({input});

  return output->template get_image<T>();
}

TEST(test_rotate90, clockwise_simple) {
  auto input = EigenImage<uint8_t>(2, 2, 1);
  input.setValues({{{1}, {0}}, {{0}, {0}}});

  auto expected = EigenImage<uint8_t>(2, 2, 1);
  expected.setValues({{{0}, {1}}, {{0}, {0}}});

  auto actual = rotate90(input, +1);

  bool result =
      test::TestBench<uint8_t, uint32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}

TEST(test_rotate90, clockwise) {
  auto input = EigenImage<uint8_t>(3, 5, 1);
  input.setValues({{{1}, {2}, {3}, {4}, {5}},
                   {{6}, {7}, {8}, {9}, {10}},
                   {{11}, {12}, {13}, {14}, {15}}});

  auto expected = EigenImage<uint8_t>(5, 3, 1);
  expected.setValues({{{11}, {6}, {1}},
                      {{12}, {7}, {2}},
                      {{13}, {8}, {3}},
                      {{14}, {9}, {4}},
                      {{15}, {10}, {5}}});

  constexpr int rotation = +5; // will be +1
  auto actual = rotate90(input, rotation);

  bool result =
      test::TestBench<uint8_t, uint32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}

TEST(test_rotate90, counter_clockwise_simple) {
  auto input = EigenImage<uint8_t>(2, 2, 1);
  input.setValues({{{1}, {0}}, {{0}, {0}}});

  auto expected = EigenImage<uint8_t>(2, 2, 1);
  expected.setValues({{{0}, {0}}, {{1}, {0}}});

  auto actual = rotate90(input, -1);

  bool result =
      test::TestBench<uint8_t, uint32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}

TEST(test_rotate90, counter_clockwise) {
  auto input = EigenImage<uint8_t>(3, 5, 1);
  input.setValues({{{1}, {2}, {3}, {4}, {5}},
                   {{6}, {7}, {8}, {9}, {10}},
                   {{11}, {12}, {13}, {14}, {15}}});

  auto expected = EigenImage<uint8_t>(5, 3, 1);
  expected.setValues({{{5}, {10}, {15}},
                      {{4}, {9}, {14}},
                      {{3}, {8}, {13}},
                      {{2}, {7}, {12}},
                      {{1}, {6}, {11}}});

  constexpr int rotation = -9; // will become 3 (i.e. -1)
  auto actual = rotate90(input, rotation);

  bool result =
      test::TestBench<uint8_t, uint32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}

TEST(test_rotate90, rotate180_simple) {
  auto input = EigenImage<uint8_t>(2, 2, 1);
  input.setValues({{{1}, {0}}, {{0}, {0}}});

  auto expected = EigenImage<uint8_t>(2, 2, 1);
  expected.setValues({{{0}, {0}}, {{0}, {1}}});

  auto actual = rotate90(input, +2);

  bool result =
      test::TestBench<uint8_t, uint32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}

TEST(test_rotate90, rotate180) {
  auto input = EigenImage<uint8_t>(3, 5, 1);
  input.setValues({{{1}, {2}, {3}, {4}, {5}},
                   {{6}, {7}, {8}, {9}, {10}},
                   {{11}, {12}, {13}, {14}, {15}}});

  auto expected = EigenImage<uint8_t>(3, 5, 1);
  expected.setValues({{{15}, {14}, {13}, {12}, {11}},
                      {{10}, {9}, {8}, {7}, {6}},
                      {{5}, {4}, {3}, {2}, {1}}});

  constexpr int rotation = -10; // will be 2
  auto actual = rotate90(input, rotation);

  bool result =
      test::TestBench<uint8_t, uint32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}

TEST(test_rotate90, composition) {
  constexpr uint32_t width = 13, height = 19;
  auto input = EigenImage<uint8_t>(1, height, width);
  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      input(0, y, x) = x + y * height;

  EigenImage<uint8_t> rotate_1 = rotate90(input, +1);
  EigenImage<uint8_t> rotate_2 = rotate90(input, +2);
  EigenImage<uint8_t> rotate_3 = rotate90(input, -1);

  EigenImage<uint8_t> rotate_1_1 = rotate90(rotate_1, +1);
  bool result = test::TestBench<uint8_t, uint32_t>::compareEigenImages(
      rotate_1_1, rotate_2);

  EigenImage<uint8_t> rotate_2_1 = rotate90(rotate_2, +1);
  result &= test::TestBench<uint8_t, uint32_t>::compareEigenImages(rotate_2_1,
                                                                   rotate_3);

  EigenImage<uint8_t> rotate_2_minus_1 = rotate90(rotate_2, -1);
  result &= test::TestBench<uint8_t, uint32_t>::compareEigenImages(
      rotate_2_minus_1, rotate_1);

  EigenImage<uint8_t> rotate_2_2 = rotate90(rotate_2, +2);
  result &=
      test::TestBench<uint8_t, uint32_t>::compareEigenImages(rotate_2_2, input);

  EigenImage<uint8_t> rotate_1_minus_1 = rotate90(rotate_1, -1);
  result &= test::TestBench<uint8_t, uint32_t>::compareEigenImages(
      rotate_1_minus_1, input);

  EigenImage<uint8_t> rotate_3_minus_1 = rotate90(rotate_3, -1);
  result &= test::TestBench<uint8_t, uint32_t>::compareEigenImages(
      rotate_3_minus_1, rotate_2);

  EXPECT_TRUE(result);
}