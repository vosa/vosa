/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "bokeh_custom.h"

#include "mlir/Dialect/Arith/IR/Arith.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/IR/Verifier.h"

namespace vosa::samples::bokeh {

bool populate_custom_ops(mlir::MLIRContext &context,
                         mlir::OwningOpRef<mlir::ModuleOp> &module) {
  mlir::OpBuilder builder(&context);
  context.getOrLoadDialect<mlir::func::FuncDialect>();

  llvm::SmallVector<mlir::Type> two_five_five_args = {};
  llvm::SmallVector<mlir::Type> two_five_five_returns = {builder.getF32Type()};
  auto two_five_five_func_type =
      builder.getFunctionType(two_five_five_args, two_five_five_returns);
  mlir::func::FuncOp two_five_five_function = mlir::func::FuncOp::create(
      builder.getUnknownLoc(), "two_five_five", two_five_five_func_type);
  auto &two_five_five_entry_block = *two_five_five_function.addEntryBlock();
  builder.setInsertionPointToStart(&two_five_five_entry_block);
  mlir::TypedAttr two_five_five_value =
      builder.getFloatAttr(builder.getF32Type(), 255.);
  auto two_five_five_constant_op = builder.create<mlir::arith::ConstantOp>(
      builder.getUnknownLoc(), two_five_five_value);
  llvm::SmallVector<mlir::Value> two_five_five_results = {
      two_five_five_constant_op};
  builder.create<mlir::func::ReturnOp>(builder.getUnknownLoc(),
                                       two_five_five_results);
  module->push_back(two_five_five_function);

  llvm::SmallVector<mlir::Type> scale_factor_args = {builder.getF32Type()};
  llvm::SmallVector<mlir::Type> scale_factor_returns = {builder.getF32Type()};
  auto scale_factor_func_type =
      builder.getFunctionType(scale_factor_args, scale_factor_returns);
  mlir::func::FuncOp scale_factor_function = mlir::func::FuncOp::create(
      builder.getUnknownLoc(), "scale_factor", scale_factor_func_type);
  auto &scale_factor_entry_block = *scale_factor_function.addEntryBlock();
  builder.setInsertionPointToStart(&scale_factor_entry_block);
  mlir::TypedAttr scale_factor_value =
      builder.getFloatAttr(builder.getF32Type(), 255.);
  auto scale_factor_arg = scale_factor_function.getArgument(0);
  auto scale_factor_constant_op = builder.create<mlir::arith::ConstantOp>(
      builder.getUnknownLoc(), scale_factor_value);
  auto scale_factor_div_op = builder.create<mlir::arith::DivFOp>(
      builder.getUnknownLoc(), scale_factor_constant_op, scale_factor_arg);
  llvm::SmallVector<mlir::Value> scale_factor_results = {scale_factor_div_op};
  builder.create<mlir::func::ReturnOp>(builder.getUnknownLoc(),
                                       scale_factor_results);
  module->push_back(scale_factor_function);

  llvm::SmallVector<mlir::Type> depth_map_average_args = {
      builder.getIntegerType(32, true), builder.getIntegerType(32, true)};
  llvm::SmallVector<mlir::Type> depth_map_average_returns = {
      builder.getF32Type()};
  auto depth_map_average_func_type = builder.getFunctionType(
      depth_map_average_args, depth_map_average_returns);
  mlir::func::FuncOp depth_map_average_function =
      mlir::func::FuncOp::create(builder.getUnknownLoc(), "depth_map_average",
                                 depth_map_average_func_type);
  auto &depth_map_average_entry_block =
      *depth_map_average_function.addEntryBlock();
  builder.setInsertionPointToStart(&depth_map_average_entry_block);
  mlir::Value depth_map_average_arg_0 =
      depth_map_average_function.getArgument(0);
  mlir::Value depth_map_average_arg_1 =
      depth_map_average_function.getArgument(1);
  auto converted_arg_0 =
      builder
          .create<mlir::UnrealizedConversionCastOp>(
              builder.getUnknownLoc(),
              builder.getIntegerType(
                  builder.getIntegerType(32, true).getIntOrFloatBitWidth()),
              depth_map_average_arg_0)
          .getResult(0);
  auto converted_arg_1 =
      builder
          .create<mlir::UnrealizedConversionCastOp>(
              builder.getUnknownLoc(),
              builder.getIntegerType(
                  builder.getIntegerType(32, true).getIntOrFloatBitWidth()),
              depth_map_average_arg_1)
          .getResult(0);
  auto float_arg_0 = builder.create<mlir::arith::SIToFPOp>(
      builder.getUnknownLoc(), builder.getF32Type(), converted_arg_0);
  auto float_arg_1 = builder.create<mlir::arith::SIToFPOp>(
      builder.getUnknownLoc(), builder.getF32Type(), converted_arg_1);
  auto depth_map_averag_div_op = builder.create<mlir::arith::DivFOp>(
      builder.getUnknownLoc(), float_arg_0, float_arg_1);
  llvm::SmallVector<mlir::Value> depth_map_average_results = {
      depth_map_averag_div_op};
  builder.create<mlir::func::ReturnOp>(builder.getUnknownLoc(),
                                       depth_map_average_results);
  module->push_back(depth_map_average_function);

  auto verified = mlir::verify(module.get());
  return verified.failed();
}

} // namespace vosa::samples::bokeh