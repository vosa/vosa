/* -*- c++ -*-
 *
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_SERIALIZATION_PASS_H
#define VOSA_SERIALIZATION_PASS_H

#include "mlir/Pass/Pass.h"

namespace mlir {
namespace vosa {

std::unique_ptr<Pass>
createVosaSerializationPass(const std::string &filename = "");

class VosaDialect;
/// Generate the code for registering conversion passes.
#define GEN_PASS_REGISTRATION
#define GEN_PASS_DECL_VOSASERIALIZATION
#define GEN_PASS_DEF_VOSASERIALIZATION
#include "VosaSerialization.h.inc"

} // namespace vosa
} // namespace mlir

#endif // VOSA_SERIALIZATION_PASS_H
