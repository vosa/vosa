"""
License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import logging
import os
import sys
import inspect
import importlib
import typing

import vosa
import vosa_serialization.VosaOperator as vosa_operator


def get_vosa_file_operators(module):
    op_names_and_modes = dict()
    for name, vosa_ops_member in inspect.getmembers(module, inspect.isclass):
        if issubclass(vosa_ops_member, vosa.op.Op) and not inspect.isabstract(
            vosa_ops_member
        ):
            if vosa_ops_member.include_in_specification():
                modes = [
                    mode.name
                    for mode in list(vosa_ops_member.get_pattern_dict().keys())
                ]
                op_names_and_modes[name] = modes
    return op_names_and_modes


def get_vosa_module_operators(module):
    found_op_names_and_modes = dict()
    for name, submodule in inspect.getmembers(module, inspect.ismodule):
        found_op_names_and_modes.update(get_vosa_file_operators(submodule))
    return found_op_names_and_modes


def make_schema_dict(schema_operators):
    schema_operators = list(schema_operators)
    schema_dict = dict()
    for operator in schema_operators:
        operator_mode = operator + "_MODE"
        operator_module_name = "vosa_serialization." + operator_mode
        try:
            operator_mode_module = importlib.import_module(
                operator_module_name, operator_mode
            )
        except ModuleNotFoundError:
            schema_dict[operator] = []
            continue
        module_members = {
            member[0]: member[1] for member in inspect.getmembers(operator_mode_module)
        }
        operator_mode_class = module_members[operator_mode]
        operator_mode_class_members = inspect.getmembers(operator_mode_class)
        public_operator_mode_class_members = [
            member[0]
            for member in operator_mode_class_members
            if not member[0].startswith("__")
        ]
        schema_dict[operator] = public_operator_mode_class_members
    return schema_dict


def validate(
    spec_operators: typing.Dict[str, typing.List[str]],
    schema_operators: typing.Dict[str, typing.List[str]],
):
    special_ignores = ["CAST", "TOSA_GRAPH"]

    success = True
    for operator_name, spec_operator_modes in spec_operators.items():
        possible_operator_names = [operator_name, operator_name + "_"]
        found_operator = False
        for possible_operator_name in possible_operator_names:
            try:
                schema_operator_modes = schema_operators[possible_operator_name]
                found_operator = True
                break
            except KeyError:
                continue
        if not found_operator:
            logging.error("Operator " + operator_name + " in spec but not in schema")
            success = False
            continue
        if operator_name in special_ignores:
            continue
        for spec_operator_mode in spec_operator_modes:
            if spec_operator_mode not in schema_operator_modes:
                logging.error(
                    "Operator mode "
                    + spec_operator_mode
                    + " in spec for operator "
                    + operator_name
                    + " but not in schema "
                )
                success = False

    return success


def main():
    vosa_operator_object = vosa_operator.VosaOperator
    potential_schema_operators = set(vars(vosa_operator_object).keys())
    private_members = {
        member for member in potential_schema_operators if member.startswith("__")
    }
    schema_operators = potential_schema_operators - {"NONE"} - private_members
    schema_operators = make_schema_dict(schema_operators)

    spec_operators = dict()
    operator_groups = [vosa.imgproc]
    for operator_group in operator_groups:
        spec_operators.update(get_vosa_module_operators(operator_group))

    return validate(spec_operators, schema_operators)


if __name__ == "__main__":
    success_code = main()
    if success_code:
        sys.exit(0)
    else:
        sys.exit(1)
