// run command without FileCheck to see output (or add "--dump-input always" FileCheck option)
// RUN: vosa-opt -vosa-pad-to-tensor-pad -vosa-to-tosa %s | %test_src_root/e2e-runner | FileCheck %s
// RUN: vosa-opt -vosa-pad-to-tensor-pad=const-zero -vosa-to-tosa %s | %test_src_root/e2e-runner | FileCheck --check-prefix=CHECK_ZERO %s

func.func @main() {
  %input = arith.constant dense<[[[11], [12], [13], [14], [15]], [[21], [22], [23], [24], [25]], [[31], [32], [33], [34], [35]], [[41], [42], [43], [44], [45]]]> : tensor<4x5x1xi32>
  %input_si = vosa.constant dense<[[[11], [12], [13], [14], [15]], [[21], [22], [23], [24], [25]], [[31], [32], [33], [34], [35]], [[41], [42], [43], [44], [45]]]> : tensor<4x5x1xsi32>
  %input2_si = vosa.constant dense<[[[111], [112], [113], [114], [115]], [[121], [122], [123], [124], [125]], [[131], [132], [133], [134], [135]], [[141], [142], [143], [144], [145]]]> : tensor<4x5x1xsi32>
  
  // constant padding

  // Pad in Y direction
  // CHECK: sizes = [9, 5]
  // CHECK-COUNT-2: [17,    17,    17,    17,    17]
  // CHECK-NEXT: [11,    12,    13,    14,    15]
  // CHECK: [41,    42,    43,    44,    45]
  // CHECK-COUNT-3: [17,    17,    17,    17,    17]
  // CHECK_ZERO-COUNT-2: [0,    0,    0,    0,    0]
  // CHECK_ZERO-NEXT: [11,    12,    13,    14,    15]
  // CHECK_ZERO: [41,    42,    43,    44,    45]
  // CHECK_ZERO-COUNT-3: [0,    0,    0,    0,    0]
  %pad_const1 = vosa.pad %input_si constant { pad_constant = 17 : si32, pad_size = [ 2 : i32, 3 : i32, 0 : i32, 0 : i32 ] } : tensor<4x5x1xsi32> -> tensor<9x5x1xsi32>
  %pad_const1_a = builtin.unrealized_conversion_cast %pad_const1 : tensor<9x5x1xsi32> to tensor<9x5x1xi32>
  %pad_const1_b = tensor.cast %pad_const1_a : tensor<9x5x1xi32> to tensor<?x?x?xi32>
  func.call @print_img_i32(%pad_const1_b) : (tensor<?x?x?xi32>) -> ()

  // Pad in X direction
  // CHECK: sizes = [4, 10]
  // CHECK-NEXT: [17,    17,    11,    12,    13,    14,    15,    17,    17,    17]
  // CHECK: [17,    17,    41,    42,    43,    44,    45,    17,    17,    17]
  // CHECK_ZERO: sizes = [4, 10]
  // CHECK_ZERO-NEXT: [0,    0,    11,    12,    13,    14,    15,    0,    0,    0]
  // CHECK_ZERO: [0,    0,    41,    42,    43,    44,    45,    0,    0,    0]
  %pad_const2 = vosa.pad %input_si constant { pad_constant = 17 : si32, pad_size = [ 0 : i32, 0 : i32, 2 : i32, 3 : i32 ] } : tensor<4x5x1xsi32> -> tensor<4x10x1xsi32>
  %pad_const2_a = builtin.unrealized_conversion_cast %pad_const2 : tensor<4x10x1xsi32> to tensor<4x10x1xi32>
  %pad_const2_b = tensor.cast %pad_const2_a : tensor<4x10x1xi32> to tensor<?x?x?xi32>
  func.call @print_img_i32(%pad_const2_b) : (tensor<?x?x?xi32>) -> ()

  // Pad in both directions
  // CHECK: sizes = [9, 10]
  // CHECK-COUNT-2: [17,    17,    17,    17,    17,    17,    17,    17,    17,    17]
  // CHECK-NEXT: [17,    17,    11,    12,    13,    14,    15,    17,    17,    17]
  // CHECK: [17,    17,    41,    42,    43,    44,    45,    17,    17,    17]
  // CHECK-COUNT-3: [17,    17,    17,    17,    17,    17,    17,    17,    17,    17]
  // CHECK_ZERO-COUNT-2: [0,    0,    0,    0,    0,    0,    0,    0,    0,    0]
  // CHECK_ZERO-NEXT: [0,    0,    11,    12,    13,    14,    15,    0,    0,    0]
  // CHECK_ZERO: [0,    0,    41,    42,    43,    44,    45,    0,    0,    0]
  // CHECK_ZERO-COUNT-3: [0,    0,    0,    0,    0,    0,    0,    0,    0,    0]
  %pad_const3 = vosa.pad %input_si constant { pad_constant = 17 : si32, pad_size = [ 2 : i32, 3 : i32, 2 : i32, 3 : i32 ] } : tensor<4x5x1xsi32> -> tensor<9x10x1xsi32>
  %pad_const3_a = builtin.unrealized_conversion_cast %pad_const3 : tensor<9x10x1xsi32> to tensor<9x10x1xi32>
  %pad_const3_b = tensor.cast %pad_const3_a : tensor<9x10x1xi32> to tensor<?x?x?xi32>
  func.call @print_img_i32(%pad_const3_b) : (tensor<?x?x?xi32>) -> ()

  // replicate padding

  // Pad in Y direction
  // CHECK: sizes = [9, 5]
  // CHECK-COUNT-3: [111,    112,    113,    114,    115]
  // CHECK-COUNT-4: [141,    142,    143,    144,    145]
  // CHECK_ZERO-COUNT-2: [0,    0,    0,    0,    0]
  // CHECK_ZERO-NEXT: [111,    112,    113,    114,    115]
  // CHECK_ZERO: [141,    142,    143,    144,    145]
  // CHECK_ZERO-COUNT-3: [0,    0,    0,    0,    0]
  %pad_repl1 = vosa.pad %input2_si replicate { pad_replant = 17 : si32, pad_size = [ 2 : i32, 3 : i32, 0 : i32, 0 : i32 ] } : tensor<4x5x1xsi32> -> tensor<9x5x1xsi32>
  %pad_repl1_a = builtin.unrealized_conversion_cast %pad_repl1 : tensor<9x5x1xsi32> to tensor<9x5x1xi32>
  %pad_repl1_b = tensor.cast %pad_repl1_a : tensor<9x5x1xi32> to tensor<?x?x?xi32>
  func.call @print_img_i32(%pad_repl1_b) : (tensor<?x?x?xi32>) -> ()

  // Pad in X direction
  // CHECK: sizes = [4, 10]
  // CHECK-NEXT: [111,    111,    111,    112,    113,    114,    115,    115,    115,    115]
  // CHECK: [141,    141,    141,    142,    143,    144,    145,    145,    145,    145]
  // CHECK_ZERO: sizes = [4, 10]
  // CHECK_ZERO-NEXT: [0,    0,    111,    112,    113,    114,    115,    0,    0,    0]
  // CHECK_ZERO: [0,    0,    141,    142,    143,    144,    145,    0,    0,    0]
  %pad_repl2 = vosa.pad %input2_si replicate { pad_replant = 17 : si32, pad_size = [ 0 : i32, 0 : i32, 2 : i32, 3 : i32 ] } : tensor<4x5x1xsi32> -> tensor<4x10x1xsi32>
  %pad_repl2_a = builtin.unrealized_conversion_cast %pad_repl2 : tensor<4x10x1xsi32> to tensor<4x10x1xi32>
  %pad_repl2_b = tensor.cast %pad_repl2_a : tensor<4x10x1xi32> to tensor<?x?x?xi32>
  func.call @print_img_i32(%pad_repl2_b) : (tensor<?x?x?xi32>) -> ()

  // Pad in both directions
  // CHECK: sizes = [9, 10]
  // CHECK-COUNT-3: [111,    111,    111,    112,    113,    114,    115,    115,    115,    115]
  // CHECK-COUNT-4: [141,    141,    141,    142,    143,    144,    145,    145,    145,    145]
  // CHECK_ZERO: sizes = [9, 10]
  // CHECK_ZERO-COUNT-2: [0,    0,    0,    0,    0,    0,    0,    0,    0,    0]
  // CHECK_ZERO-NEXT: [0,    0,    111,    112,    113,    114,    115,    0,    0,    0]
  // CHECK_ZERO: [0,    0,    141,    142,    143,    144,    145,    0,    0,    0]
  // CHECK_ZERO-COUNT-3: [0,    0,    0,    0,    0,    0,    0,    0,    0,    0]
  %pad_repl3 = vosa.pad %input2_si replicate { pad_replant = 17 : si32, pad_size = [ 2 : i32, 3 : i32, 2 : i32, 3 : i32 ] } : tensor<4x5x1xsi32> -> tensor<9x10x1xsi32>
  %pad_repl3_a = builtin.unrealized_conversion_cast %pad_repl3 : tensor<9x10x1xsi32> to tensor<9x10x1xi32>
  %pad_repl3_b = tensor.cast %pad_repl3_a : tensor<9x10x1xi32> to tensor<?x?x?xi32>
  func.call @print_img_i32(%pad_repl3_b) : (tensor<?x?x?xi32>) -> ()

  // reflect padding

  // Pad in Y direction
  // CHECK: sizes = [9, 5]
  // CHECK: [31,    32,    33,    34,    35]
  // CHECK: [21,    22,    23,    24,    25]
  //---
  // CHECK: [11,    12,    13,    14,    15]
  // CHECK: [21,    22,    23,    24,    25]
  // CHECK: [31,    32,    33,    34,    35]
  // CHECK: [41,    42,    43,    44,    45]
  //---
  // CHECK: [31,    32,    33,    34,    35]
  // CHECK: [21,    22,    23,    24,    25]
  // CHECK: [11,    12,    13,    14,    15]
  // CHECK_ZERO-COUNT-2: [0,    0,    0,    0,    0]
  // CHECK_ZERO-NEXT: [11,    12,    13,    14,    15]
  // CHECK_ZERO: [41,    42,    43,    44,    45]
  // CHECK_ZERO-COUNT-3: [0,    0,    0,    0,    0]
  %pad_refl1 = vosa.pad %input_si reflect { pad_reflant = 17 : si32, pad_size = [ 2 : i32, 3 : i32, 0 : i32, 0 : i32 ] } : tensor<4x5x1xsi32> -> tensor<9x5x1xsi32>
  %pad_refl1_a = builtin.unrealized_conversion_cast %pad_refl1 : tensor<9x5x1xsi32> to tensor<9x5x1xi32>
  %pad_refl1_b = tensor.cast %pad_refl1_a : tensor<9x5x1xi32> to tensor<?x?x?xi32>
  func.call @print_img_i32(%pad_refl1_b) : (tensor<?x?x?xi32>) -> ()

  // Pad in X direction
  // CHECK: sizes = [4, 10]
  // CHECK: [13,    12,    11,    12,    13,    14,    15,    14,    13,    12]
  // CHECK: [43,    42,    41,    42,    43,    44,    45,    44,    43,    42]
  // CHECK_ZERO: sizes = [4, 10]
  // CHECK_ZERO-NEXT: [0,    0,    11,    12,    13,    14,    15,    0,    0,    0]
  // CHECK_ZERO: [0,    0,    41,    42,    43,    44,    45,    0,    0,    0]
  %pad_refl2 = vosa.pad %input_si reflect { pad_reflant = 17 : si32, pad_size = [ 0 : i32, 0 : i32, 2 : i32, 3 : i32 ] } : tensor<4x5x1xsi32> -> tensor<4x10x1xsi32>
  %pad_refl2_a = builtin.unrealized_conversion_cast %pad_refl2 : tensor<4x10x1xsi32> to tensor<4x10x1xi32>
  %pad_refl2_b = tensor.cast %pad_refl2_a : tensor<4x10x1xi32> to tensor<?x?x?xi32>
  func.call @print_img_i32(%pad_refl2_b) : (tensor<?x?x?xi32>) -> ()

  // Pad in both directions
  // CHECK: sizes = [9, 10]
  // CHECK: [33,    32,    31,    32,    33,    34,    35,    34,    33,    32]
  // CHECK: [23,    22,    21,    22,    23,    24,    25,    24,    23,    22]
  //---
  // CHECK: [13,    12,    11,    12,    13,    14,    15,    14,    13,    12]
  // CHECK: [23,    22,    21,    22,    23,    24,    25,    24,    23,    22]
  // CHECK: [33,    32,    31,    32,    33,    34,    35,    34,    33,    32]
  // CHECK: [43,    42,    41,    42,    43,    44,    45,    44,    43,    42]
  //---
  // CHECK: [33,    32,    31,    32,    33,    34,    35,    34,    33,    32]
  // CHECK: [23,    22,    21,    22,    23,    24,    25,    24,    23,    22]
  // CHECK: [13,    12,    11,    12,    13,    14,    15,    14,    13,    12]
  // CHECK_ZERO: sizes = [9, 10]
  // CHECK_ZERO-COUNT-2: [0,    0,    0,    0,    0,    0,    0,    0,    0,    0]
  // CHECK_ZERO-NEXT: [0,    0,    11,    12,    13,    14,    15,    0,    0,    0]
  // CHECK_ZERO: [0,    0,    41,    42,    43,    44,    45,    0,    0,    0]
  // CHECK_ZERO-COUNT-3: [0,    0,    0,    0,    0,    0,    0,    0,    0,    0]
  %pad_refl3 = vosa.pad %input_si reflect { pad_reflant = 17 : si32, pad_size = [ 2 : i32, 3 : i32, 2 : i32, 3 : i32 ] } : tensor<4x5x1xsi32> -> tensor<9x10x1xsi32>
  %pad_refl3_a = builtin.unrealized_conversion_cast %pad_refl3 : tensor<9x10x1xsi32> to tensor<9x10x1xi32>
  %pad_refl3_b = tensor.cast %pad_refl3_a : tensor<9x10x1xi32> to tensor<?x?x?xi32>
  func.call @print_img_i32(%pad_refl3_b) : (tensor<?x?x?xi32>) -> ()

  return
}

func.func @print_img_i32(%a : tensor<?x?x?xi32>) {
  // %unranked_all = tensor.cast %a : tensor<?x?x?xi32> to tensor<*xi32>
  // func.call @print_memref_i32(%unranked_all) : (tensor<*xi32>) -> ()
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c0 : tensor<?x?x?xi32>
  %w = tensor.dim %a, %c1 : tensor<?x?x?xi32>
  %nch = tensor.dim %a, %c2 : tensor<?x?x?xi32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[0, 0, %c][%h, %w, 1][1, 1, 1]  : tensor<?x?x?xi32> to tensor<?x?x1xi32>
     %chdata_c = tensor.collapse_shape %chdata [[0], [1, 2]] : tensor<?x?x1xi32> into tensor<?x?xi32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xi32> to tensor<*xi32>
     func.call @printMemrefI32(%unranked_chdata) : (tensor<*xi32>) -> ()
  }
  return
}

func.func private @printMemrefI32(%ptr : tensor<*xi32> {bufferization.access = "read"})
