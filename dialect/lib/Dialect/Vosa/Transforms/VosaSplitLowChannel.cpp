/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/Dialect/Vosa/IR/VosaDialect.h"
#include "vosa/Dialect/Vosa/IR/VosaOps.h"
#include "vosa/Dialect/Vosa/Transforms/Passes.h"
#include "vosa/Dialect/Vosa/Utils/DataFormat.h"

#include "mlir/Dialect/Arith/IR/Arith.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/Linalg/IR/Linalg.h"
#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/Dialect/Tosa/IR/TosaOps.h"

#include "mlir/IR/PatternMatch.h"
#include "mlir/Pass/Pass.h"
#include "mlir/Transforms/DialectConversion.h"
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"

#include "llvm/Support/Debug.h"

#define DEBUG_TYPE "vosa-split-channel"

#include "PassDetail.h"
using namespace mlir;
using namespace mlir::vosa;
namespace {

struct SplitChannelCast : public OpRewritePattern<vosa::CastOp> {
  using OpRewritePattern::OpRewritePattern;

  LogicalResult matchAndRewrite(vosa::CastOp Op,
                                PatternRewriter &rewriter) const override {

    auto input = Op.getInput();
    vosa::LogicalShiftRightOp lsrop =
        input.getDefiningOp<vosa::LogicalShiftRightOp>();
    if (!lsrop)
      return failure();

    // LSR Channels
    auto inputTy = Op.getInput().getType().cast<ShapedType>();
    auto channels = getChannels(inputTy);
    // Cast channels
    auto outputTy = Op.getOutput().getType().cast<ShapedType>();
    auto cast_channels = getChannels(outputTy);

    // if the channels don't match
    if (cast_channels != channels)
      return failure();

    if (cast_channels > 1 && cast_channels <= 16) {
      auto shiftVal = lsrop.getShift();

      // extract two tensors
      // Create slices for the input image - one slice per channel.
      auto flatTy = RankedTensorType::get(
          {getHeight(inputTy), getWidth(inputTy), 1}, inputTy.getElementType());

      SmallVector<OpFoldResult, 2> offsets, dims, strides;
      for (int i = 0; i < 3; ++i) {
        offsets.push_back(rewriter.getIndexAttr(0));
        strides.push_back(rewriter.getIndexAttr(1));
      }

      // The channel dimension is simply `1` as we are extracting one channel.
      dims.push_back(rewriter.getIndexAttr(getHeight(inputTy)));
      dims.push_back(rewriter.getIndexAttr(getWidth(inputTy)));
      dims.insert(dims.begin() + getChannelDimension(),
                  rewriter.getIndexAttr(1));

      SmallVector<mlir::Value> channelSlices;
      for (unsigned i = 0; i < cast_channels; i++) {
        offsets[getChannelDimension()] = rewriter.getIndexAttr(i);
        channelSlices.push_back(rewriter.create<tensor::ExtractSliceOp>(
            Op.getLoc(), flatTy, lsrop.getInput(), offsets, dims, strides));
      }

      SmallVector<mlir::Value> lsrOps;
      for (unsigned i = 0; i < cast_channels; i++) {
        lsrOps.push_back(
            rewriter
                .create<vosa::LogicalShiftRightOp>(Op.getLoc(), flatTy,
                                                   channelSlices[i], shiftVal)
                .getOutput());
      }

      vosa::RoundingMode rounding_mode = Op->getAttr("rounding_mode")
                                             .cast<vosa::RoundingModeAttr>()
                                             .getValue();
      auto castflatTy =
          RankedTensorType::get({getHeight(outputTy), getWidth(outputTy), 1},
                                outputTy.getElementType());
      SmallVector<mlir::Value> castOps;
      for (unsigned i = 0; i < cast_channels; i++) {
        castOps.push_back(rewriter
                              .create<vosa::CastOp>(Op.getLoc(), castflatTy,
                                                    lsrOps[i], rounding_mode)
                              .getResult());
      }

      auto concatTy = RankedTensorType::get(
          {getHeight(outputTy), getWidth(outputTy), getChannels(outputTy)},
          outputTy.getElementType());

      auto vosaConcatOp = rewriter
                              .create<vosa::ConcatOp>(Op.getLoc(), concatTy,
                                                      ValueRange{castOps})
                              .getOutput();

      rewriter.replaceOp(Op, vosaConcatOp);
    } else {
      return failure();
    }
    return success();
  }
};

class VosaSplitChannelPass
    : public mlir::vosa::VosaSplitChannelBase<VosaSplitChannelPass> {
public:
  explicit VosaSplitChannelPass() = default;

  void runOnOperation() override {

    RewritePatternSet patterns(&getContext());
    patterns.add<SplitChannelCast>(patterns.getContext());

    ConversionTarget target(getContext());

    if (failed(
            applyPatternsAndFoldGreedily(getOperation(), std::move(patterns))))
      signalPassFailure();
  }
};
} // namespace

std::unique_ptr<mlir::Pass> mlir::vosa::createVosaSplitChannelPass() {
  LLVM_DEBUG(llvm::dbgs() << "createVosaSplitChannel\n";);
  return std::make_unique<VosaSplitChannelPass>();
}
