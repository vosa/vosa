/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "round.h"

namespace vosa {

std::shared_ptr<Op> RoundBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_round = vosa_operator_union.AsROUND();
  assert(serialized_round);

  RoundBase::round_method_t round_method;
  switch (serialized_round->attr->round_method) {
  case vosa_serialization::ROUND_METHOD_FLOOR: {
    round_method = RoundBase::round_method_t::FLOOR;
    break;
  }
  case vosa_serialization::ROUND_METHOD_CEIL: {
    round_method = RoundBase::round_method_t::CEIL;
    break;
  }
  case vosa_serialization::ROUND_METHOD_TOWARDS_ZERO: {
    round_method = RoundBase::round_method_t::TOWARDS_ZERO;
    break;
  }
  case vosa_serialization::ROUND_METHOD_AWAY_FROM_ZERO: {
    round_method = RoundBase::round_method_t::AWAY_FROM_ZERO;
    break;
  }
  case vosa_serialization::ROUND_METHOD_HALF_UP: {
    round_method = RoundBase::round_method_t::HALF_UP;
    break;
  }
  case vosa_serialization::ROUND_METHOD_HALF_DOWN: {
    round_method = RoundBase::round_method_t::HALF_DOWN;
    break;
  }
  case vosa_serialization::ROUND_METHOD_HALF_TOWARDS_ZERO: {
    round_method = RoundBase::round_method_t::HALF_TOWARDS_ZERO;
    break;
  }
  case vosa_serialization::ROUND_METHOD_HALF_AWAY_FROM_ZERO: {
    round_method = RoundBase::round_method_t::HALF_AWAY_FROM_ZERO;
    break;
  }
  case vosa_serialization::ROUND_METHOD_HALF_TO_EVEN: {
    round_method = RoundBase::round_method_t::HALF_TO_EVEN;
    break;
  }
  case vosa_serialization::ROUND_METHOD_HALF_TO_ODD: {
    round_method = RoundBase::round_method_t::HALF_TO_ODD;
    break;
  }
  default:
    throw vosa::OperatorSerializeNotImplementedError();
  }

  switch (serialized_round->mode) {
  case vosa_serialization::ROUND_MODE_I_I_FLOAT32_INT32:
    return std::make_shared<Round<float>>(round_method);
  default:
    throw vosa::OperatorModeNotImplementedError();
  }
}

template <typename T>
std::shared_ptr<ArrayBase>
Round<T>::eval(const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();
  auto typed_output = this->runme(input);

  auto output = std::make_shared<ArrayBase>(typed_output);
  return output;
}

template <typename T>
EigenImage<int32_t> Round<T>::runme(const EigenImage<T> &input) const {

  const uint32_t height = input.dimension(0);
  const uint32_t width = input.dimension(1);
  const uint32_t channels = input.dimension(2);

  EigenImage<int32_t> output(height, width, channels);
  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      for (uint32_t c = 0; c < channels; c++) {
        float input_value = input(y, x, c);
        switch (round_method) {
        case FLOOR: {
          output(y, x, c) = (int)std::floor(input_value);
          break;
        }
        case CEIL: {
          output(y, x, c) = (int)std::ceil(input_value);
          break;
        }
        case TOWARDS_ZERO: {
          output(y, x, c) = (int)std::trunc(input_value);
          break;
        }
        case AWAY_FROM_ZERO: {
          output(y, x, c) = input_value >= 0 ? (int)std::ceil(input_value)
                                             : (int)std::floor(input_value);
          break;
        }
        case HALF_UP: {
          output(y, x, c) = (int)std::floor(input_value + 0.5f);
          break;
        }
        case HALF_DOWN: {
          output(y, x, c) = (int)std::ceil(input_value - 0.5f);
          break;
        }
        case HALF_TOWARDS_ZERO: {
          output(y, x, c) = (input_value >= 0 ? 1 : -1) *
                            (int)std::ceil(std::abs(input_value) - 0.5f);
          break;
        }
        case HALF_AWAY_FROM_ZERO: {
          output(y, x, c) = (int)std::round(input_value);
          break;
        }
        case HALF_TO_EVEN: {
          int integer_part = std::floor(std::abs(input_value));

          if (abs(input_value) - (float)integer_part != 0.5f)
            output(y, x, c) = (int)std::round(
                input_value); // Round half away from zero in non-corner cases
          else
            output(y, x, c) =
                (input_value >= 0 ? 1 : -1) *
                (integer_part % 2 == 0 ? integer_part : integer_part + 1);

          break;
        }
        case HALF_TO_ODD: {
          int integer_part = std::floor(std::abs(input_value));

          if (abs(input_value) - (float)integer_part != 0.5f)
            output(y, x, c) = (int)std::round(
                input_value); // Round half away from zero in non-corner cases
          else
            output(y, x, c) =
                (input_value >= 0 ? 1 : -1) *
                (integer_part % 2 == 1 ? integer_part : integer_part + 1);

          break;
        }
        default:
          throw std::logic_error("");
        }
      }

  return output;
}

template <typename T>
ArrayBase::DType Round<T>::output_dtype() const {
  return vosa::ArrayBase::DType::INT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Round<T>::serialize(const std::string &op_name,
                          const std::vector<std::string> &input_names) const {
  auto round_attribute =
      std::make_unique<vosa_serialization::ROUND_ATTRIBUTET>();
  switch (round_method) {
  case FLOOR: {
    round_attribute->round_method = vosa_serialization::ROUND_METHOD_FLOOR;
    break;
  }
  case CEIL: {
    round_attribute->round_method = vosa_serialization::ROUND_METHOD_CEIL;
    break;
  }
  case TOWARDS_ZERO: {
    round_attribute->round_method =
        vosa_serialization::ROUND_METHOD_TOWARDS_ZERO;
    break;
  }
  case AWAY_FROM_ZERO: {
    round_attribute->round_method =
        vosa_serialization::ROUND_METHOD_AWAY_FROM_ZERO;
    break;
  }
  case HALF_UP: {
    round_attribute->round_method = vosa_serialization::ROUND_METHOD_HALF_UP;
    break;
  }
  case HALF_DOWN: {
    round_attribute->round_method = vosa_serialization::ROUND_METHOD_HALF_DOWN;
    break;
  }
  case HALF_TOWARDS_ZERO: {
    round_attribute->round_method =
        vosa_serialization::ROUND_METHOD_HALF_TOWARDS_ZERO;
    break;
  }
  case HALF_AWAY_FROM_ZERO: {
    round_attribute->round_method =
        vosa_serialization::ROUND_METHOD_HALF_AWAY_FROM_ZERO;
    break;
  }
  case HALF_TO_EVEN: {
    round_attribute->round_method =
        vosa_serialization::ROUND_METHOD_HALF_TO_EVEN;
    break;
  }
  case HALF_TO_ODD: {
    round_attribute->round_method =
        vosa_serialization::ROUND_METHOD_HALF_TO_ODD;
    break;
  }
  default:
    throw std::logic_error("");
  }

  vosa_serialization::ROUNDT round;
  round.mode = get_serialization_mode();
  round.attr = std::move(round_attribute);

  auto round_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  round_operator->op.Set(std::move(round));
  round_operator->name = op_name;
  round_operator->inputs = input_names;

  return round_operator;
}

template <>
vosa_serialization::ROUND_MODE Round<float>::get_serialization_mode() const {
  return vosa_serialization::ROUND_MODE_I_I_FLOAT32_INT32;
}

template class vosa::Round<float>;

} // namespace vosa
