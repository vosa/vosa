/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <llvm/Support/raw_ostream.h>
#include <string>

#include "vosa/IreeCompilerInterface/iree_compiler_interface.h"

#include "iree/compiler/embedding_api.h"

void shutdownIree(iree_compiler_invocation_t *invocation,
                  iree_compiler_session_t *session) {
  ireeCompilerInvocationDestroy(invocation);
  ireeCompilerSessionDestroy(session);
  ireeCompilerGlobalShutdown();
}

bool handleError(iree_compiler_invocation_t *invocation,
                 iree_compiler_session_t *session, iree_compiler_error_t *error,
                 const std::string &prepend) {
  if (error) {
    const char *msg = ireeCompilerErrorGetMessage(error);
    llvm::errs() << "" << msg << "\n";
    shutdownIree(invocation, session);
    return true;
  }
  return false;
}

bool vosa::Compiler::compile_iree(const std::string &input_ir,
                                  const std::string &pipeline_name,
                                  const std::string &output_filename) {

  iree_compiler_session_t *session = nullptr;
  iree_compiler_invocation_t *invocation = nullptr;
  iree_compiler_source_t *source = nullptr;

  ireeCompilerGlobalInitialize();

  session = ireeCompilerSessionCreate();

  invocation = ireeCompilerInvocationCreate(session);
  ireeCompilerInvocationEnableConsoleDiagnostics(invocation);
  ireeCompilerInvocationSetVerifyIR(invocation, true);

  char tosa_arg[] = "--iree-input-type=tosa";
  char llvm_cpu_arg[] = "-iree-hal-target-backends=llvm-cpu";
  char mlir_print_after_all_arg[] = "-mlir-print-ir-after-all";
  int iree_argc = 3;
  char *iree_argv[] = {tosa_arg, llvm_cpu_arg, mlir_print_after_all_arg};
  ireeCompilerSessionSetFlags(session, iree_argc, iree_argv);

  char *input_ir_cstr = new char[input_ir.length() + 1];
  std::strcpy(input_ir_cstr, input_ir.c_str());

  char *pipeline_name_cstr = new char[pipeline_name.length() + 1];
  std::strcpy(pipeline_name_cstr, pipeline_name.c_str());

  char *output_filename_cstr = new char[output_filename.length() + 1];
  std::strcpy(output_filename_cstr, output_filename.c_str());

  iree_compiler_error_t *source_wrap_error =
      ireeCompilerSourceWrapBuffer(session, pipeline_name_cstr, input_ir_cstr,
                                   input_ir.length() + 1, true, &source);
  if (handleError(invocation, session, source_wrap_error,
                  "Error wrapping source buffer: "))
    return true;

  bool parse_source = ireeCompilerInvocationParseSource(invocation, source);
  if (!parse_source) {
    llvm::errs() << "Fail to parse source\n";
    return true;
  }

  ireeCompilerInvocationPipeline(invocation, IREE_COMPILER_PIPELINE_STD);

  iree_compiler_output_t *output = nullptr;
  iree_compiler_error_t *output_open_error =
      ireeCompilerOutputOpenFile(output_filename_cstr, &output);
  if (handleError(invocation, session, output_open_error,
                  "Error opening output: "))
    return true;

  iree_compiler_error_t *compile_error =
      ireeCompilerInvocationOutputVMBytecode(invocation, output);
  if (handleError(invocation, session, compile_error, "Error compiling: "))
    return true;

  shutdownIree(invocation, session);
  return false;
}
