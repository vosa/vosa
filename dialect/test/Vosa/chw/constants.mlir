// RUN: vosa-opt -vosa-data-format=chw -split-input-file -verify-diagnostics %s | FileCheck %s

module {
    func.func @arith_consts() {
        %cst_0_i8 = arith.constant 0 : i8
        %cst_m1_i8 = arith.constant -1 : i8
        %cst_m128_i8 = arith.constant -128 : i8
        %cst_127_i8 = arith.constant 127 : i8
    
        // mapped to signed range (i.e. -128..-1)
        %cst_128_i8 = arith.constant 128 : i8
        %cst_255_i8 = arith.constant 255 : i8
        return
    }
}

// -----

module {
    func.func @scalar_constants() {
        // CHECK: %{{.*}} = vosa.constant 0 : si8
        %cst_si8 = vosa.constant 0 : si8
        // CHECK: %{{.*}} = vosa.constant 0 : si16
        %cst_si16 = vosa.constant 0 : si16
        // CHECK: %{{.*}} = vosa.constant 0 : si32
        %cst_si32 = vosa.constant 0 : si32
        
        // CHECK: %{{.*}} = vosa.constant 0 : ui8
        %cst_ui8 = vosa.constant 0 : ui8
        // CHECK: %{{.*}} = vosa.constant 0 : ui16
        %cst_ui16 = vosa.constant 0 : ui16
        // CHECK: %{{.*}} = vosa.constant 0 : ui32
        %cst_ui32 = vosa.constant 0 : ui32
        
        // CHECK: %{{.*}} = vosa.constant 0.0{{.*}} : f16
        %cst_f16 = vosa.constant 0.0 : f16
        // CHECK: %{{.*}} = vosa.constant 0.0{{.*}} : f32
        %cst_f32 = vosa.constant 0.0 : f32
    
        return
    }
}

// -----

module {
    func.func @tensor_constants() {
        // CHECK: %{{.*}} = vosa.constant dense<0> : tensor<100xsi8>
        %cst_100_si8 = vosa.constant dense<0> : tensor<100xsi8>
        // CHECK: %{{.*}} = vosa.constant dense<0> : tensor<100xsi16>
        %cst_100_si16 = vosa.constant dense<0> : tensor<100xsi16>
        // CHECK: %{{.*}} = vosa.constant dense<0> : tensor<100xsi32>
        %cst_100_si32 = vosa.constant dense<0> : tensor<100xsi32>
    
        // CHECK: %{{.*}} = vosa.constant dense<0> : tensor<200x100x1xsi8>
        %cst_200x100_si8 = vosa.constant dense<0> : tensor<200x100x1xsi8>
        // CHECK: %{{.*}} = vosa.constant dense<0> : tensor<200x100x1xsi16>
        %cst_200x100_si16 = vosa.constant dense<0> : tensor<200x100x1xsi16>
        // CHECK: %{{.*}} = vosa.constant dense<0> : tensor<200x100x1xsi32>
        %cst_200x100_si32 = vosa.constant dense<0> : tensor<200x100x1xsi32>
    
        // CHECK: %{{.*}} = vosa.constant dense<0> : tensor<200x100x3xsi8>
        %cst_3x200x100_si8 = vosa.constant dense<0> : tensor<200x100x3xsi8>
        // CHECK: %{{.*}} = vosa.constant dense<0> : tensor<200x100x3xsi16>
        %cst_3x200x100_si16 = vosa.constant dense<0> : tensor<200x100x3xsi16>
        // CHECK: %{{.*}} = vosa.constant dense<0> : tensor<200x100x3xsi32>
        %cst_3x200x100_si32 = vosa.constant dense<0> : tensor<200x100x3xsi32>
    
        // CHECK: %{{.*}} = vosa.constant dense<{{.*}}> : tensor<2xsi8>
        %cst_2_si8 = vosa.constant dense<[1, 2]> : tensor<2xsi8>
        // CHECK: %{{.*}} = vosa.constant dense<{{.*}}> : tensor<2x2xsi8>
        %cst_2x2_si8 = vosa.constant dense<[[1, 2], [3, 4]]> : tensor<2x2xsi8>
        // CHECK: %{{.*}} = vosa.constant dense<{{.*}}> : tensor<1x2x2xsi8>
        %cst_1x2x2_si8 = vosa.constant dense<[[[1, 2], [3, 4]]]> : tensor<1x2x2xsi8>
    
        return
    }
}

// -----

module {
    func.func @invalid_scalar_constants_si8() {
        // expected-error @below {{integer constant out of range for attribute}}
        %cst_si8 = vosa.constant 128 : si8
        return
    }
}

// TODO: invalid siN : out of range, FP
// TODO: invalid uiN : -ve, out of range, FP
// TODO: invalid FP: out of range, int
