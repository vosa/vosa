// RUN: vosa-opt -vosa-data-format=chw -split-input-file -verify-diagnostics %s | FileCheck %s

// CHECK-LABEL: func.func @arg_max_channelwise()
module {
    func.func @arg_max_channelwise() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.arg_max_channelwise %{{.*}} : tensor<3x100x200xsi8> -> tensor<1x100x200xui32>
        %res0 = vosa.arg_max_channelwise %t_si8 : tensor<3x100x200xsi8> -> tensor<1x100x200xui32>
    
        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.arg_max_channelwise %{{.*}} : tensor<3x100x200xsi32> -> tensor<1x100x200xui32>
        %res1 = vosa.arg_max_channelwise %t_si32 : tensor<3x100x200xsi32> -> tensor<1x100x200xui32>
    
        %t_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // CHECK: %{{.*}} = vosa.arg_max_channelwise %{{.*}} : tensor<3x100x200xf16> -> tensor<1x100x200xui32>
        %res2 = vosa.arg_max_channelwise %t_f16 : tensor<3x100x200xf16> -> tensor<1x100x200xui32>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.arg_max_channelwise %{{.*}} : tensor<3x100x200xf32> -> tensor<1x100x200xui32>
        %res3 = vosa.arg_max_channelwise %t_f32 : tensor<3x100x200xf32> -> tensor<1x100x200xui32>
    
        return
    }
}

// -----

module {
    func.func @arg_max_channelwise_bad_out_sz() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.arg_max_channelwise' op input size != output size}}
        %res0 = vosa.arg_max_channelwise %t_si8 : tensor<3x100x200xsi8> -> tensor<1x101x200xui32>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @arg_max_planewise()
module {
    func.func @arg_max_planewise() {
    
        %t_si8 = vosa.constant dense<42> : tensor<1x100x200xsi8>
        // CHECK: %{{.*}} = vosa.arg_max_planewise %{{.*}} : tensor<1x100x200xsi8> -> tensor<2xui32>
        %res0 = vosa.arg_max_planewise %t_si8 : tensor<1x100x200xsi8> -> tensor<2xui32>
    
        %t_si32 = vosa.constant dense<42> : tensor<1x100x200xsi32>
        // CHECK: %{{.*}} = vosa.arg_max_planewise %{{.*}} : tensor<1x100x200xsi32> -> tensor<2xui32>
        %res1 = vosa.arg_max_planewise %t_si32 : tensor<1x100x200xsi32> -> tensor<2xui32>
    
        %t_f16 = vosa.constant dense<42.0> : tensor<1x100x200xf16>
        // CHECK: %{{.*}} = vosa.arg_max_planewise %{{.*}} : tensor<1x100x200xf16> -> tensor<2xui32>
        %res2 = vosa.arg_max_planewise %t_f16 : tensor<1x100x200xf16> -> tensor<2xui32>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<1x100x200xf32>
        // CHECK: %{{.*}} = vosa.arg_max_planewise %{{.*}} : tensor<1x100x200xf32> -> tensor<2xui32>
        %res3 = vosa.arg_max_planewise %t_f32 : tensor<1x100x200xf32> -> tensor<2xui32>
    
        return
    }
}

// -----

module {
    func.func @arg_max_planewise_bad_out_ty() {
    
        %t_si8 = vosa.constant dense<42> : tensor<1x100x200xsi8>
        // expected-error @below {{'vosa.arg_max_planewise' op result #0 must be two element 1D tensor of 32-bit unsigned integer values}}
        %res0 = vosa.arg_max_planewise %t_si8 : tensor<1x100x200xsi8> -> tensor<3xui32>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @arg_min_channelwise()
module {
    func.func @arg_min_channelwise() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.arg_min_channelwise %{{.*}} : tensor<3x100x200xsi8> -> tensor<1x100x200xui32>
        %res0 = vosa.arg_min_channelwise %t_si8 : tensor<3x100x200xsi8> -> tensor<1x100x200xui32>
    
        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.arg_min_channelwise %{{.*}} : tensor<3x100x200xsi32> -> tensor<1x100x200xui32>
        %res1 = vosa.arg_min_channelwise %t_si32 : tensor<3x100x200xsi32> -> tensor<1x100x200xui32>
    
        %t_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // CHECK: %{{.*}} = vosa.arg_min_channelwise %{{.*}} : tensor<3x100x200xf16> -> tensor<1x100x200xui32>
        %res2 = vosa.arg_min_channelwise %t_f16 : tensor<3x100x200xf16> -> tensor<1x100x200xui32>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.arg_min_channelwise %{{.*}} : tensor<3x100x200xf32> -> tensor<1x100x200xui32>
        %res3 = vosa.arg_min_channelwise %t_f32 : tensor<3x100x200xf32> -> tensor<1x100x200xui32>
    
        return
    }
}

// -----

module {
    func.func @arg_min_channelwise_bad_out_sz() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.arg_min_channelwise' op input size != output size}}
        %res0 = vosa.arg_min_channelwise %t_si8 : tensor<3x100x200xsi8> -> tensor<1x101x200xui32>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @arg_min_planewise()
module {
    func.func @arg_min_planewise() {
    
        %t_si8 = vosa.constant dense<42> : tensor<1x100x200xsi8>
        // CHECK: %{{.*}} = vosa.arg_min_planewise %{{.*}} : tensor<1x100x200xsi8> -> tensor<2xui32>
        %res0 = vosa.arg_min_planewise %t_si8 : tensor<1x100x200xsi8> -> tensor<2xui32>
    
        %t_si32 = vosa.constant dense<42> : tensor<1x100x200xsi32>
        // CHECK: %{{.*}} = vosa.arg_min_planewise %{{.*}} : tensor<1x100x200xsi32> -> tensor<2xui32>
        %res1 = vosa.arg_min_planewise %t_si32 : tensor<1x100x200xsi32> -> tensor<2xui32>
    
        %t_f16 = vosa.constant dense<42.0> : tensor<1x100x200xf16>
        // CHECK: %{{.*}} = vosa.arg_min_planewise %{{.*}} : tensor<1x100x200xf16> -> tensor<2xui32>
        %res2 = vosa.arg_min_planewise %t_f16 : tensor<1x100x200xf16> -> tensor<2xui32>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<1x100x200xf32>
        // CHECK: %{{.*}} = vosa.arg_min_planewise %{{.*}} : tensor<1x100x200xf32> -> tensor<2xui32>
        %res3 = vosa.arg_min_planewise %t_f32 : tensor<1x100x200xf32> -> tensor<2xui32>
    
        return
    }
}

// -----

module {
    func.func @arg_min_planewise_bad_out_ty() {
    
        %t_si8 = vosa.constant dense<42> : tensor<1x100x200xsi8>
        // expected-error @below {{'vosa.arg_min_planewise' op result #0 must be two element 1D tensor of 32-bit unsigned integer values}}
        %res0 = vosa.arg_min_planewise %t_si8 : tensor<1x100x200xsi8> -> tensor<3xui32>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @reduce_interp_channelwise()
module {
    func.func @reduce_interp_channelwise() {
        %s = vosa.constant dense<2.0> : tensor<1x100x200xf32>
    
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.reduce_interp_channelwise %{{.*}}, %{{.*}} : tensor<3x100x200xsi8>, tensor<1x100x200xf32> -> tensor<1x100x200xf32>
        %res0 = vosa.reduce_interp_channelwise %t_si8, %s : tensor<3x100x200xsi8>, tensor<1x100x200xf32> -> tensor<1x100x200xf32>
    
        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.reduce_interp_channelwise %{{.*}}, %{{.*}} : tensor<3x100x200xsi32>, tensor<1x100x200xf32> -> tensor<1x100x200xf32>
        %res1 = vosa.reduce_interp_channelwise %t_si32, %s : tensor<3x100x200xsi32>, tensor<1x100x200xf32> -> tensor<1x100x200xf32>
    
        %t_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // CHECK: %{{.*}} = vosa.reduce_interp_channelwise %{{.*}}, %{{.*}} : tensor<3x100x200xf16>, tensor<1x100x200xf32> -> tensor<1x100x200xf32>
        %res2 = vosa.reduce_interp_channelwise %t_f16, %s : tensor<3x100x200xf16>, tensor<1x100x200xf32> -> tensor<1x100x200xf32>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.reduce_interp_channelwise %{{.*}}, %{{.*}} : tensor<3x100x200xf32>, tensor<1x100x200xf32> -> tensor<1x100x200xf32>
        %res3 = vosa.reduce_interp_channelwise %t_f32, %s : tensor<3x100x200xf32>, tensor<1x100x200xf32> -> tensor<1x100x200xf32>
    
        return
    }
}

// -----

module {
    func.func @reduce_interp_channelwise_bad_sel1() {
        %s = vosa.constant dense<2> : tensor<1x100x200xsi8>
    
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.reduce_interp_channelwise' op operand #1 must be single channel 3D tensor of 32-bit float values, but got 'tensor<1x100x200xsi8>'}}
        %res0 = vosa.reduce_interp_channelwise %t_si8, %s : tensor<3x100x200xsi8>, tensor<1x100x200xsi8> -> tensor<1x100x200xf32>
    
        return
    }
}

// -----

module {
    func.func @reduce_interp_channelwise_bad_sel2() {
        %s = vosa.constant dense<2.0> : tensor<1x100x200xf16>
    
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.reduce_interp_channelwise' op operand #1 must be single channel 3D tensor of 32-bit float values, but got 'tensor<1x100x200xf16>'}}
        %res0 = vosa.reduce_interp_channelwise %t_si8, %s : tensor<3x100x200xsi8>, tensor<1x100x200xf16> -> tensor<1x100x200xf32>
    
        return
    }
}

// -----

module {
    func.func @reduce_interp_channelwise_bad_sel3() {
        %s = vosa.constant dense<2.0> : tensor<2x100x200xf32>
    
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.reduce_interp_channelwise' op operand #1 must be single channel 3D tensor of 32-bit float values, but got 'tensor<2x100x200xf32>'}}
        %res0 = vosa.reduce_interp_channelwise %t_si8, %s : tensor<3x100x200xsi8>, tensor<2x100x200xf32> -> tensor<1x100x200xf32>
    
        return
    }
}

// -----

module {
    func.func @reduce_interp_channelwise_bad_sel_sz() {
        %s = vosa.constant dense<2.0> : tensor<1x101x200xf32>
    
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.reduce_interp_channelwise' op input0 size != input1 size}}
        %res0 = vosa.reduce_interp_channelwise %t_si8, %s : tensor<3x100x200xsi8>, tensor<1x101x200xf32> -> tensor<1x100x200xf32>
    
        return
    }
}

// -----

module {
    func.func @reduce_interp_channelwise_bad_out_sz() {
        %s = vosa.constant dense<2.0> : tensor<1x100x200xf32>
    
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.reduce_interp_channelwise' op input0 size != output size}}
        %res0 = vosa.reduce_interp_channelwise %t_si8, %s : tensor<3x100x200xsi8>, tensor<1x100x200xf32> -> tensor<1x101x200xf32>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @reduce_max_channelwise()
module {
    func.func @reduce_max_channelwise() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.reduce_max_channelwise %{{.*}} : tensor<3x100x200xsi8> -> tensor<1x100x200xsi8>
        %res0 = vosa.reduce_max_channelwise %t_si8 : tensor<3x100x200xsi8> -> tensor<1x100x200xsi8>
    
        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.reduce_max_channelwise %{{.*}} : tensor<3x100x200xsi32> -> tensor<1x100x200xsi32>
        %res1 = vosa.reduce_max_channelwise %t_si32 : tensor<3x100x200xsi32> -> tensor<1x100x200xsi32>
    
        %t_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // CHECK: %{{.*}} = vosa.reduce_max_channelwise %{{.*}} : tensor<3x100x200xf16> -> tensor<1x100x200xf16>
        %res2 = vosa.reduce_max_channelwise %t_f16 : tensor<3x100x200xf16> -> tensor<1x100x200xf16>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.reduce_max_channelwise %{{.*}} : tensor<3x100x200xf32> -> tensor<1x100x200xf32>
        %res3 = vosa.reduce_max_channelwise %t_f32 : tensor<3x100x200xf32> -> tensor<1x100x200xf32>
    
        return
    }
}

// -----

module {
    func.func @reduce_max_channelwise_bad_out_sz() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.reduce_max_channelwise' op input size != output size}}
        %res0 = vosa.reduce_max_channelwise %t_si8 : tensor<3x100x200xsi8> -> tensor<1x101x200xsi8>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @reduce_max_planewise()
module {
    func.func @reduce_max_planewise() {
    
        %t_si8 = vosa.constant dense<42> : tensor<1x100x200xsi8>
        // CHECK: %{{.*}} = vosa.reduce_max_planewise %{{.*}} : tensor<1x100x200xsi8> -> tensor<1x1x1xsi8>
        %res0 = vosa.reduce_max_planewise %t_si8 : tensor<1x100x200xsi8> -> tensor<1x1x1xsi8>
    
        %t_si32 = vosa.constant dense<42> : tensor<1x100x200xsi32>
        // CHECK: %{{.*}} = vosa.reduce_max_planewise %{{.*}} : tensor<1x100x200xsi32> -> tensor<1x1x1xsi32>
        %res1 = vosa.reduce_max_planewise %t_si32 : tensor<1x100x200xsi32> -> tensor<1x1x1xsi32>
    
        %t_f16 = vosa.constant dense<42.0> : tensor<1x100x200xf16>
        // CHECK: %{{.*}} = vosa.reduce_max_planewise %{{.*}} : tensor<1x100x200xf16> -> tensor<1x1x1xf16>
        %res2 = vosa.reduce_max_planewise %t_f16 : tensor<1x100x200xf16> -> tensor<1x1x1xf16>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<1x100x200xf32>
        // CHECK: %{{.*}} = vosa.reduce_max_planewise %{{.*}} : tensor<1x100x200xf32> -> tensor<1x1x1xf32>
        %res3 = vosa.reduce_max_planewise %t_f32 : tensor<1x100x200xf32> -> tensor<1x1x1xf32>
    
        return
    }
}

// -----

module {
    func.func @reduce_max_planewise_bad_out_ty() {
    
        %t_si8 = vosa.constant dense<42> : tensor<1x100x200xsi8>
        // expected-error @below {{'vosa.reduce_max_planewise' op output type does not match input element type}}
        %res0 = vosa.reduce_max_planewise %t_si8 : tensor<1x100x200xsi8> -> tensor<1x1x1xsi32>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @reduce_min_channelwise()
module {
    func.func @reduce_min_channelwise() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.reduce_min_channelwise %{{.*}} : tensor<3x100x200xsi8> -> tensor<1x100x200xsi8>
        %res0 = vosa.reduce_min_channelwise %t_si8 : tensor<3x100x200xsi8> -> tensor<1x100x200xsi8>
    
        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.reduce_min_channelwise %{{.*}} : tensor<3x100x200xsi32> -> tensor<1x100x200xsi32>
        %res1 = vosa.reduce_min_channelwise %t_si32 : tensor<3x100x200xsi32> -> tensor<1x100x200xsi32>
    
        %t_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // CHECK: %{{.*}} = vosa.reduce_min_channelwise %{{.*}} : tensor<3x100x200xf16> -> tensor<1x100x200xf16>
        %res2 = vosa.reduce_min_channelwise %t_f16 : tensor<3x100x200xf16> -> tensor<1x100x200xf16>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.reduce_min_channelwise %{{.*}} : tensor<3x100x200xf32> -> tensor<1x100x200xf32>
        %res3 = vosa.reduce_min_channelwise %t_f32 : tensor<3x100x200xf32> -> tensor<1x100x200xf32>
    
        return
    }
}

// -----

module {
    func.func @reduce_min_channelwise_bad_out_sz() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.reduce_min_channelwise' op input size != output size}}
        %res0 = vosa.reduce_min_channelwise %t_si8 : tensor<3x100x200xsi8> -> tensor<1x101x200xsi8>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @reduce_min_planewise()
module {
    func.func @reduce_min_planewise() {
    
        %t_si8 = vosa.constant dense<42> : tensor<1x100x200xsi8>
        // CHECK: %{{.*}} = vosa.reduce_min_planewise %{{.*}} : tensor<1x100x200xsi8> -> tensor<1x1x1xsi8>
        %res0 = vosa.reduce_min_planewise %t_si8 : tensor<1x100x200xsi8> -> tensor<1x1x1xsi8>
    
        %t_si32 = vosa.constant dense<42> : tensor<1x100x200xsi32>
        // CHECK: %{{.*}} = vosa.reduce_min_planewise %{{.*}} : tensor<1x100x200xsi32> -> tensor<1x1x1xsi32>
        %res1 = vosa.reduce_min_planewise %t_si32 : tensor<1x100x200xsi32> -> tensor<1x1x1xsi32>
    
        %t_f16 = vosa.constant dense<42.0> : tensor<1x100x200xf16>
        // CHECK: %{{.*}} = vosa.reduce_min_planewise %{{.*}} : tensor<1x100x200xf16> -> tensor<1x1x1xf16>
        %res2 = vosa.reduce_min_planewise %t_f16 : tensor<1x100x200xf16> -> tensor<1x1x1xf16>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<1x100x200xf32>
        // CHECK: %{{.*}} = vosa.reduce_min_planewise %{{.*}} : tensor<1x100x200xf32> -> tensor<1x1x1xf32>
        %res3 = vosa.reduce_min_planewise %t_f32 : tensor<1x100x200xf32> -> tensor<1x1x1xf32>
    
        return
    }
}

// -----

module {
    func.func @reduce_min_planewise_bad_out_ty() {
    
        %t_si8 = vosa.constant dense<42> : tensor<1x100x200xsi8>
        // expected-error @below {{'vosa.reduce_min_planewise' op output type does not match input element type}}
        %res0 = vosa.reduce_min_planewise %t_si8 : tensor<1x100x200xsi8> -> tensor<1x1x1xsi32>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @reduce_sum_channelwise()
module {
    func.func @reduce_sum_channelwise() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.reduce_sum_channelwise %{{.*}} : tensor<3x100x200xsi8> -> tensor<1x100x200xsi8>
        %res0 = vosa.reduce_sum_channelwise %t_si8 : tensor<3x100x200xsi8> -> tensor<1x100x200xsi8>
    
        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.reduce_sum_channelwise %{{.*}} : tensor<3x100x200xsi32> -> tensor<1x100x200xsi32>
        %res1 = vosa.reduce_sum_channelwise %t_si32 : tensor<3x100x200xsi32> -> tensor<1x100x200xsi32>
    
        %t_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // CHECK: %{{.*}} = vosa.reduce_sum_channelwise %{{.*}} : tensor<3x100x200xf16> -> tensor<1x100x200xf16>
        %res2 = vosa.reduce_sum_channelwise %t_f16 : tensor<3x100x200xf16> -> tensor<1x100x200xf16>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.reduce_sum_channelwise %{{.*}} : tensor<3x100x200xf32> -> tensor<1x100x200xf32>
        %res3 = vosa.reduce_sum_channelwise %t_f32 : tensor<3x100x200xf32> -> tensor<1x100x200xf32>
    
        return
    }
}

// -----

module {
    func.func @reduce_sum_channelwise_bad_out_sz() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.reduce_sum_channelwise' op input size != output size}}
        %res0 = vosa.reduce_sum_channelwise %t_si8 : tensor<3x100x200xsi8> -> tensor<1x101x200xsi8>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @reduce_sum_planewise()
module {
    func.func @reduce_sum_planewise() {
    
        %t_si8 = vosa.constant dense<42> : tensor<1x100x200xsi8>
        // CHECK: %{{.*}} = vosa.reduce_sum_planewise %{{.*}} : tensor<1x100x200xsi8> -> tensor<1x1x1xsi8>
        %res0 = vosa.reduce_sum_planewise %t_si8 : tensor<1x100x200xsi8> -> tensor<1x1x1xsi8>
    
        %t_si32 = vosa.constant dense<42> : tensor<1x100x200xsi32>
        // CHECK: %{{.*}} = vosa.reduce_sum_planewise %{{.*}} : tensor<1x100x200xsi32> -> tensor<1x1x1xsi32>
        %res1 = vosa.reduce_sum_planewise %t_si32 : tensor<1x100x200xsi32> -> tensor<1x1x1xsi32>
    
        %t_f16 = vosa.constant dense<42.0> : tensor<1x100x200xf16>
        // CHECK: %{{.*}} = vosa.reduce_sum_planewise %{{.*}} : tensor<1x100x200xf16> -> tensor<1x1x1xf16>
        %res2 = vosa.reduce_sum_planewise %t_f16 : tensor<1x100x200xf16> -> tensor<1x1x1xf16>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<1x100x200xf32>
        // CHECK: %{{.*}} = vosa.reduce_sum_planewise %{{.*}} : tensor<1x100x200xf32> -> tensor<1x1x1xf32>
        %res3 = vosa.reduce_sum_planewise %t_f32 : tensor<1x100x200xf32> -> tensor<1x1x1xf32>
    
        return
    }
}

// -----

module {
    func.func @reduce_sum_planewise_bad_out_ty() {
    
        %t_si8 = vosa.constant dense<42> : tensor<1x100x200xsi8>
        // expected-error @below {{'vosa.reduce_sum_planewise' op output type does not match input element type}}
        %res0 = vosa.reduce_sum_planewise %t_si8 : tensor<1x100x200xsi8> -> tensor<1x1x1xsi32>
    
        return
    }
}
