/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/vosa.h"

#include "bokeh_io.h"
#include "flatbuffer_utils.h"

int main(int argc, char *argv[]) {

  std::string binary_mask_input_file(argv[1]);
  std::string depth_map_input_file(argv[2]);
  std::string u_channel_input_file(argv[3]);
  std::string v_channel_input_file(argv[4]);
  std::string y_channel_input_file(argv[5]);

  std::string depth_map_output_file(argv[6]);
  std::string u_channel_output_file(argv[7]);
  std::string v_channel_output_file(argv[8]);
  std::string y_channel_output_file(argv[9]);

  std::vector<uint8_t> binary_mask_input;
  std::vector<uint8_t> depth_map_input;
  std::vector<uint8_t> uv_channels_input;
  std::vector<uint8_t> y_channel_input;
  auto input_read_success = read_inputs(
      binary_mask_input_file, depth_map_input_file, u_channel_input_file,
      v_channel_input_file, y_channel_input_file, binary_mask_input,
      depth_map_input, uv_channels_input, y_channel_input);
  if (!input_read_success)
    return 1;

  std::string ref_model_flatbuffer(argv[10]);

  auto flatbuffer_data = load_flatbuffer(ref_model_flatbuffer);
  auto graph = vosa::Graph((uint8_t *)flatbuffer_data.get());

  auto default_inputs = graph.get_default_inputs();
  auto default_outputs = graph.get_default_outputs();
  for (const auto &o : default_outputs)
    std::cout << o << std::endl;

  int input_height_half = 540;
  int input_width_half = 960;

  int input_height = 1080;
  int input_width = 1920;

  auto depth_map_input_data = vosa::ArrayBase(
      depth_map_input.data(), input_height_half * input_width_half);
  auto binary_mask_input_data = vosa::ArrayBase(
      binary_mask_input.data(), input_height_half * input_width_half);
  auto uv_channels_input_data = vosa::ArrayBase(
      uv_channels_input.data(), input_height_half * input_width_half * 2);
  auto y_channel_input_data =
      vosa::ArrayBase(y_channel_input.data(), input_height * input_width);

  std::unordered_map<std::string, vosa::ArrayBase> graph_inputs = {
      {default_inputs[0], depth_map_input_data},
      {default_inputs[1], binary_mask_input_data},
      {default_inputs[2], uv_channels_input_data},
      {default_inputs[3], y_channel_input_data}};
  auto outputs = graph.execute(graph_inputs, default_outputs);
  auto output_depth_map = outputs[0]->get_image<uint8_t>();
  auto output_uv_channels = outputs[1]->get_image<uint8_t>();
  auto output_y_channel = outputs[2]->get_image<uint8_t>();
  std::vector<uint8_t> depth_map_output(depth_map_input.size());
  std::vector<uint8_t> uv_channels_output(uv_channels_input.size());
  std::vector<uint8_t> y_channel_output(y_channel_input.size());

  std::memcpy(depth_map_output.data(), output_depth_map.data(),
              depth_map_output.size());
  std::memcpy(uv_channels_output.data(), output_uv_channels.data(),
              uv_channels_output.size());
  std::memcpy(y_channel_output.data(), output_y_channel.data(),
              y_channel_output.size());

  auto write_success =
      write_outputs(depth_map_output_file, u_channel_output_file,
                    v_channel_output_file, y_channel_output_file,
                    depth_map_output, uv_channels_output, y_channel_output);
  if (!write_success)
    return 1;
  return 0;
}