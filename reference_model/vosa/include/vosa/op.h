/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_CODE_OP_H
#define VOSA_CODE_OP_H

#include "array_types.h"
#include "vosa_generated.h"

namespace vosa {

class OperatorModeNotImplementedError : public std::logic_error {
public:
  explicit OperatorModeNotImplementedError()
      : std::logic_error("Operator Mode Not Implemented") {}
};

class OperatorEvalNotImplementedError : public std::logic_error {
public:
  explicit OperatorEvalNotImplementedError()
      : std::logic_error("Operator Eval Not Implemented") {}
};

class OperatorSerializeNotImplementedError : public std::logic_error {
public:
  explicit OperatorSerializeNotImplementedError()
      : std::logic_error("Operator Serialize Not Implemented") {}
};

class OperatorOutputDtypeNotImplementedError : public std::logic_error {
public:
  explicit OperatorOutputDtypeNotImplementedError(const std::string &name)
      : std::logic_error("output_dtype not implemented for operator " + name) {}
};

class Op {
public:
  virtual ~Op() = default;

  [[nodiscard]] virtual std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &) const = 0;

  [[nodiscard]] virtual vosa::ArrayBase::DType output_dtype() const = 0;

  [[nodiscard]] virtual std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const = 0;

  template <typename T>
  static T unpack_value(const std::vector<uint8_t> &raw_value);

  template <typename T>
  static vosa::EigenVector<T>
  unpack_vector(const std::vector<uint8_t> &raw_value);

  template <typename T>
  static std::vector<uint8_t> pack_value(const T &value);

  template <typename T>
  static std::vector<uint8_t>
  pack_vector(const vosa::EigenVector<T> &eigen_vector);

  template <typename T>
  static vosa::EigenPlane<T>
  unpack_plane(const std::vector<uint8_t> &raw_value,
               const std::vector<uint32_t> &plane_dimensions);

  template <typename T>
  static std::vector<uint8_t>
  pack_plane(const vosa::EigenPlane<T> &eigen_plane);

  template <typename T>
  static vosa::EigenImage<T>
  unpack_image(const std::vector<uint8_t> &raw_value,
               const std::vector<uint32_t> &image_dimensions);

  template <typename T>
  static std::vector<uint8_t>
  pack_image(const vosa::EigenImage<T> &eigen_image);

protected:
  template <typename T>
  [[nodiscard]] vosa::ArrayBase::DType output_dtype_helper() const;
};

} // namespace vosa

#endif // VOSA_CODE_OP_H
