/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "power.h"

std::shared_ptr<vosa::Op> vosa::PowerBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_power = vosa_operator_union.AsPOWER();
  assert(serialized_power);
  auto mode = serialized_power->mode;
  auto serialized_attributes = &serialized_power->attr;
  std::vector<uint8_t> raw_base_value = serialized_attributes->get()->base;

  switch (mode) {
  case vosa_serialization::POWER_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::Power<float>>(
        unpack_value<float>(raw_base_value));
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::Power<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();

  uint32_t input_height = input.dimension(0);
  uint32_t input_width = input.dimension(1);
  uint32_t input_channels = input.dimension(2);

  auto output_image = EigenImage<T>(input_height, input_width, input_channels);

  for (uint32_t height_index = 0; height_index < input_height; height_index++) {
    for (uint32_t width_index = 0; width_index < input_width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < input_channels;
           channel_index++) {

        output_image(height_index, width_index, channel_index) =
            powf(base, input(height_index, width_index, channel_index));
      }
    }
  }
  return std::make_shared<vosa::ArrayBase>(output_image);
}

template <>
vosa_serialization::POWER_MODE
vosa::Power<float>::get_serialization_mode() const {
  return vosa_serialization::POWER_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Power<T>::serialize(const std::string &op_name,
                          const std::vector<std::string> &input_names) const {

  auto power_attribute =
      std::make_unique<vosa_serialization::POWER_ATTRIBUTET>();
  power_attribute->base = pack_value(base);

  vosa_serialization::POWERT power;
  power.mode = get_serialization_mode();
  power.attr = std::move(power_attribute);

  auto gamma_correction_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  gamma_correction_operator->op.Set(std::move(power));
  gamma_correction_operator->name = op_name;
  gamma_correction_operator->inputs = input_names;
  return gamma_correction_operator;
}

template <typename T>
vosa::ArrayBase::DType vosa::Power<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template class vosa::Power<float>;