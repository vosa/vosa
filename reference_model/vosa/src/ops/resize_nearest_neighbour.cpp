/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "resize_nearest_neighbour.h"

using namespace std;
using namespace vosa;
using namespace vosa_serialization;

// Utils
inline float inp(uint32_t x, float size_ratio) {
  return ((float)x + 0.5f) * size_ratio - 0.5f;
}

inline int32_t ul(uint32_t x, float size_ratio) {
  return floor(inp(x, size_ratio));
}

// Template instantiation
template class vosa::ResizeNearestNeighbour<bool>;
template class vosa::ResizeNearestNeighbour<uint8_t>;
template class vosa::ResizeNearestNeighbour<uint16_t>;
template class vosa::ResizeNearestNeighbour<uint32_t>;
template class vosa::ResizeNearestNeighbour<int8_t>;
template class vosa::ResizeNearestNeighbour<int16_t>;
template class vosa::ResizeNearestNeighbour<int32_t>;
template class vosa::ResizeNearestNeighbour<float>;

shared_ptr<Op> ResizeNearestNeighbourBase::parse_serialized(
    const VosaOperatorUnion &vosa_operator_union) {
  auto serialized_resize_nearest_neighbour =
      vosa_operator_union.AsRESIZE_NEAREST_NEIGHBOUR();
  assert(serialized_resize_nearest_neighbour);

  auto serialized_attributes = &serialized_resize_nearest_neighbour->attr;
  const vector<uint32_t> resize_size = serialized_attributes->get()->size;

  switch (serialized_resize_nearest_neighbour->mode) {
  case RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_BOOL_BOOL:
    return make_shared<ResizeNearestNeighbour<bool>>(resize_size);
  case RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_UINT8_UINT8:
    return make_shared<ResizeNearestNeighbour<uint8_t>>(resize_size);
  case RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_UINT16_UINT16:
    return make_shared<ResizeNearestNeighbour<uint16_t>>(resize_size);
  case RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_UINT32_UINT32:
    return make_shared<ResizeNearestNeighbour<uint32_t>>(resize_size);
  case RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_INT8_INT8:
    return make_shared<ResizeNearestNeighbour<int8_t>>(resize_size);
  case RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_INT16_INT16:
    return make_shared<ResizeNearestNeighbour<int16_t>>(resize_size);
  case RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_INT32_INT32:
    return make_shared<ResizeNearestNeighbour<int32_t>>(resize_size);
  case RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_FLOAT32_FLOAT32:
    return make_shared<ResizeNearestNeighbour<float>>(resize_size);
  default:
    throw OperatorModeNotImplementedError();
  }
}

template <typename T>
shared_ptr<ArrayBase> ResizeNearestNeighbour<T>::eval(
    const vector<shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();

  return make_shared<ArrayBase>(this->runme(input));
}

template <typename T>
EigenImage<T>
ResizeNearestNeighbour<T>::runme(const EigenImage<T> &input) const {
  const uint32_t input_height = input.dimension(0);
  const uint32_t input_width = input.dimension(1);
  const uint32_t channels = input.dimension(2);

  const uint32_t height = output_size[0];
  const uint32_t width = output_size[1];

  const float height_ratio = (float)input_height / (float)height;
  const float width_ratio = (float)input_width / (float)width;

  constexpr uint32_t offsets_size = 4;
  const uint32_t offsets[offsets_size][2] = {{0, 0}, {0, 1}, {1, 0}, {1, 1}};

  EigenImage<T> output(height, width, channels);
  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      for (uint32_t c = 0; c < channels; c++) {

        const int32_t ul_y = ul(y, height_ratio);
        const int32_t ul_x = ul(x, width_ratio);

        const float frac_y = (float)ul_y - inp(y, height_ratio);
        const float frac_x = (float)ul_x - inp(x, width_ratio);

        const float l2_distances[offsets_size] = {
            // (ny, nx)
            hypot(frac_y + 0, frac_x + 0), // (0, 0)
            hypot(frac_y + 0, frac_x + 1), // (0, 1)
            hypot(frac_y + 1, frac_x + 0), // (1, 0)
            hypot(frac_y + 1, frac_x + 1), // (1, 1)
        };

        uint32_t argmin_index = 0;
        for (uint32_t i = 1; i < offsets_size; i++)
          if (l2_distances[i] < l2_distances[argmin_index])
            argmin_index = i;

        const uint32_t ny = offsets[argmin_index][0],
                       nx = offsets[argmin_index][1];
        const uint32_t input_y =
            max((uint32_t)0, min(ul_y + ny, input_height - 1));
        const uint32_t input_x =
            max((uint32_t)0, min(ul_x + nx, input_width - 1));

        output(y, x, c) = input(input_y, input_x, c);
      }

  return output;
}

template <typename T>
ArrayBase::DType ResizeNearestNeighbour<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
unique_ptr<VosaGraphOperatorT>
ResizeNearestNeighbour<T>::serialize(const string &op_name,
                                     const vector<string> &input_names) const {
  auto resize_nearest_neighbour_attribute =
      make_unique<RESIZE_NEAREST_NEIGHBOUR_ATTRIBUTET>();
  resize_nearest_neighbour_attribute->size = output_size;

  RESIZE_NEAREST_NEIGHBOURT resize_nearest_neighbour;
  resize_nearest_neighbour.mode = get_serialization_mode();
  resize_nearest_neighbour.attr = std::move(resize_nearest_neighbour_attribute);

  auto resize_nearest_neighbour_operator = make_unique<VosaGraphOperatorT>();
  resize_nearest_neighbour_operator->op.Set(
      std::move(resize_nearest_neighbour));
  resize_nearest_neighbour_operator->name = op_name;
  resize_nearest_neighbour_operator->inputs = input_names;

  return resize_nearest_neighbour_operator;
}

template <>
RESIZE_NEAREST_NEIGHBOUR_MODE
ResizeNearestNeighbour<bool>::get_serialization_mode() const {
  return RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_BOOL_BOOL;
}

template <>
RESIZE_NEAREST_NEIGHBOUR_MODE
ResizeNearestNeighbour<uint8_t>::get_serialization_mode() const {
  return RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_UINT8_UINT8;
}

template <>
RESIZE_NEAREST_NEIGHBOUR_MODE
ResizeNearestNeighbour<uint16_t>::get_serialization_mode() const {
  return RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_UINT16_UINT16;
}

template <>
RESIZE_NEAREST_NEIGHBOUR_MODE
ResizeNearestNeighbour<uint32_t>::get_serialization_mode() const {
  return RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_UINT32_UINT32;
}

template <>
RESIZE_NEAREST_NEIGHBOUR_MODE
ResizeNearestNeighbour<int8_t>::get_serialization_mode() const {
  return RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_INT8_INT8;
}

template <>
RESIZE_NEAREST_NEIGHBOUR_MODE
ResizeNearestNeighbour<int16_t>::get_serialization_mode() const {
  return RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_INT16_INT16;
}

template <>
RESIZE_NEAREST_NEIGHBOUR_MODE
ResizeNearestNeighbour<int32_t>::get_serialization_mode() const {
  return RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_INT32_INT32;
}

template <>
RESIZE_NEAREST_NEIGHBOUR_MODE
ResizeNearestNeighbour<float>::get_serialization_mode() const {
  return RESIZE_NEAREST_NEIGHBOUR_MODE_I_I_FLOAT32_FLOAT32;
}
