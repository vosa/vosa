"""
License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""


import enum
import typing

from vosa import types as types
from vosa.imgproc.elementwise_binary_operators import SimpleBinaryExecutionPattern
from vosa.op import OPERATOR_MODES, Op, ExecutionPattern


class EQUAL_MODES(OPERATOR_MODES):
    I_I_UINT8_BOOL = enum.auto()
    I_I_UINT16_BOOL = enum.auto()
    I_I_UINT32_BOOL = enum.auto()
    I_I_INT8_BOOL = enum.auto()
    I_I_INT16_BOOL = enum.auto()
    I_I_INT32_BOOL = enum.auto()


class EQUAL(Op):
    """
    Perform the equal-to than operation, pixelwise, on two images,
    [latexmath]
    ++++
    output[y,x,c] = input_0[y,x,c] == input_1[y,x,c].
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[EQUAL_MODES, ExecutionPattern]:
        return {
            EQUAL_MODES.I_I_UINT8_BOOL: SimpleBinaryExecutionPattern(
                types.DType.UINT8, types.DType.BOOL
            ),
            EQUAL_MODES.I_I_UINT16_BOOL: SimpleBinaryExecutionPattern(
                types.DType.UINT16, types.DType.BOOL
            ),
            EQUAL_MODES.I_I_UINT32_BOOL: SimpleBinaryExecutionPattern(
                types.DType.UINT32, types.DType.BOOL
            ),
            EQUAL_MODES.I_I_INT8_BOOL: SimpleBinaryExecutionPattern(
                types.DType.INT8, types.DType.BOOL
            ),
            EQUAL_MODES.I_I_INT16_BOOL: SimpleBinaryExecutionPattern(
                types.DType.INT16, types.DType.BOOL
            ),
            EQUAL_MODES.I_I_INT32_BOOL: SimpleBinaryExecutionPattern(
                types.DType.INT32, types.DType.BOOL
            ),
        }


class GREATER_MODES(OPERATOR_MODES):
    I_I_UINT8_BOOL = enum.auto()
    I_I_UINT16_BOOL = enum.auto()
    I_I_UINT32_BOOL = enum.auto()
    I_I_INT8_BOOL = enum.auto()
    I_I_INT16_BOOL = enum.auto()
    I_I_INT32_BOOL = enum.auto()
    I_I_FLOAT16_BOOL = enum.auto()
    I_I_FLOAT32_BOOL = enum.auto()


class GREATER(Op):
    """
    Perform the greater than operation, pixelwise, on two images,
    [latexmath]
    ++++
    output[y,x,c] = input_0[y,x,c] > input_1[y,x,c].
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[GREATER_MODES, ExecutionPattern]:
        return {
            GREATER_MODES.I_I_UINT8_BOOL: SimpleBinaryExecutionPattern(
                types.DType.UINT8, types.DType.BOOL
            ),
            GREATER_MODES.I_I_UINT16_BOOL: SimpleBinaryExecutionPattern(
                types.DType.UINT16, types.DType.BOOL
            ),
            GREATER_MODES.I_I_UINT32_BOOL: SimpleBinaryExecutionPattern(
                types.DType.UINT32, types.DType.BOOL
            ),
            GREATER_MODES.I_I_INT8_BOOL: SimpleBinaryExecutionPattern(
                types.DType.INT8, types.DType.BOOL
            ),
            GREATER_MODES.I_I_INT16_BOOL: SimpleBinaryExecutionPattern(
                types.DType.INT16, types.DType.BOOL
            ),
            GREATER_MODES.I_I_INT32_BOOL: SimpleBinaryExecutionPattern(
                types.DType.INT32, types.DType.BOOL
            ),
            GREATER_MODES.I_I_FLOAT16_BOOL: SimpleBinaryExecutionPattern(
                types.DType.FLOAT16, types.DType.BOOL
            ),
            GREATER_MODES.I_I_FLOAT32_BOOL: SimpleBinaryExecutionPattern(
                types.DType.FLOAT32, types.DType.BOOL
            ),
        }


class GREATER_EQUAL_MODES(OPERATOR_MODES):
    I_I_UINT8_BOOL = enum.auto()
    I_I_UINT16_BOOL = enum.auto()
    I_I_UINT32_BOOL = enum.auto()
    I_I_INT8_BOOL = enum.auto()
    I_I_INT16_BOOL = enum.auto()
    I_I_INT32_BOOL = enum.auto()
    I_I_FLOAT16_BOOL = enum.auto()
    I_I_FLOAT32_BOOL = enum.auto()


class GREATER_EQUAL(Op):
    """
    Perform the greater than or equal to operation, pixelwise, on two images,
    [latexmath]
    ++++
    output[y,x,c] = input_0[y,x,c] \\geq input_1[y,x,c].
    ++++
    """

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[GREATER_EQUAL_MODES, ExecutionPattern]:
        return {
            GREATER_EQUAL_MODES.I_I_UINT8_BOOL: SimpleBinaryExecutionPattern(
                types.DType.UINT8, types.DType.BOOL
            ),
            GREATER_EQUAL_MODES.I_I_UINT16_BOOL: SimpleBinaryExecutionPattern(
                types.DType.UINT16, types.DType.BOOL
            ),
            GREATER_EQUAL_MODES.I_I_UINT32_BOOL: SimpleBinaryExecutionPattern(
                types.DType.UINT32, types.DType.BOOL
            ),
            GREATER_EQUAL_MODES.I_I_INT8_BOOL: SimpleBinaryExecutionPattern(
                types.DType.INT8, types.DType.BOOL
            ),
            GREATER_EQUAL_MODES.I_I_INT16_BOOL: SimpleBinaryExecutionPattern(
                types.DType.INT16, types.DType.BOOL
            ),
            GREATER_EQUAL_MODES.I_I_INT32_BOOL: SimpleBinaryExecutionPattern(
                types.DType.INT32, types.DType.BOOL
            ),
            GREATER_EQUAL_MODES.I_I_FLOAT16_BOOL: SimpleBinaryExecutionPattern(
                types.DType.FLOAT16, types.DType.BOOL
            ),
            GREATER_EQUAL_MODES.I_I_FLOAT32_BOOL: SimpleBinaryExecutionPattern(
                types.DType.FLOAT32, types.DType.BOOL
            ),
        }
