/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "rotate90.h"

using namespace std;
using namespace vosa;
using namespace vosa_serialization;

// Template initialisation
template class vosa::Rotate90<uint8_t>;

template class vosa::Rotate90<uint16_t>;

template class vosa::Rotate90<uint32_t>;

template class vosa::Rotate90<int8_t>;

template class vosa::Rotate90<int16_t>;

template class vosa::Rotate90<int32_t>;

template class vosa::Rotate90<float>;

shared_ptr<Op>
Rotate90Base::parse_serialized(const VosaOperatorUnion &vosa_operator_union) {
  auto serialized_rotate90 = vosa_operator_union.AsROTATE_90();
  assert(serialized_rotate90);

  uint32_t channel = serialized_rotate90->attr->rotate;

  switch (serialized_rotate90->mode) {
  case ROTATE_90_MODE_I_I_UINT8_UINT8:
    return make_shared<Rotate90<uint8_t>>(channel);
  case ROTATE_90_MODE_I_I_UINT16_UINT16:
    return make_shared<Rotate90<uint16_t>>(channel);
  case ROTATE_90_MODE_I_I_UINT32_UINT32:
    return make_shared<Rotate90<uint32_t>>(channel);
  case ROTATE_90_MODE_I_I_INT8_INT8:
    return make_shared<Rotate90<int8_t>>(channel);
  case ROTATE_90_MODE_I_I_INT16_INT16:
    return make_shared<Rotate90<int16_t>>(channel);
  case ROTATE_90_MODE_I_I_INT32_INT32:
    return make_shared<Rotate90<int32_t>>(channel);
  case ROTATE_90_MODE_I_I_FLOAT32_FLOAT32:
    return make_shared<Rotate90<float>>(channel);
  default:
    throw OperatorModeNotImplementedError();
  }
}

template <typename T>
shared_ptr<ArrayBase>
Rotate90<T>::eval(const vector<shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();

  return make_shared<ArrayBase>(this->runme(input));
}

template <typename T>
EigenImage<T>
Rotate90<T>::rotate90_clockwise(const EigenImage<T> &input) const {

  const uint32_t height = input.dimension(0);
  const uint32_t width = input.dimension(1);
  const uint32_t channels = input.dimension(2);

  EigenImage<T> output(width, height, channels);
  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      for (uint32_t c = 0; c < channels; c++)
        output(x, height - 1 - y, c) = input(y, x, c);

  return output;
}

template <typename T>
EigenImage<T>
Rotate90<T>::rotate90_counterclockwise(const EigenImage<T> &input) const {
  const uint32_t height = input.dimension(0);
  const uint32_t width = input.dimension(1);
  const uint32_t channels = input.dimension(2);

  EigenImage<T> output(width, height, channels);

  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      for (uint32_t c = 0; c < channels; c++)
        output(width - 1 - x, y, c) = input(y, x, c);

  return output;
}

template <typename T>
EigenImage<T> Rotate90<T>::rotate180(const EigenImage<T> &input) const {

  const uint32_t height = input.dimension(0);
  const uint32_t width = input.dimension(1);
  const uint32_t channels = input.dimension(2);

  EigenImage<T> output(height, width, channels);
  for (uint32_t c = 0; c < channels; c++)
    for (uint32_t y = 0; y < height; y++)
      for (uint32_t x = 0; x < width; x++)
        output(height - 1 - y, width - 1 - x, c) = input(y, x, c);

  return output;
}

template <typename T>
EigenImage<T> Rotate90<T>::runme(const EigenImage<T> &input) const {
  switch (rotate) {
  case 0:
    return input;
  case 1:
    return rotate90_clockwise(input);
  case 2:
    return rotate180(input);
  case 3:
    return rotate90_counterclockwise(input);
  default:
    throw OperatorEvalNotImplementedError();
  }
}

template <typename T>
ArrayBase::DType Rotate90<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
unique_ptr<VosaGraphOperatorT>
Rotate90<T>::serialize(const string &op_name,
                       const vector<string> &input_names) const {
  auto rotate90_attribute = make_unique<ROTATE_90_ATTRIBUTET>();
  rotate90_attribute->rotate = rotate;

  ROTATE_90T rotate90;
  rotate90.mode = get_serialization_mode();
  rotate90.attr = std::move(rotate90_attribute);

  auto rotate90_operator = make_unique<VosaGraphOperatorT>();
  rotate90_operator->op.Set(std::move(rotate90));
  rotate90_operator->name = op_name;
  rotate90_operator->inputs = input_names;

  return rotate90_operator;
}

template <>
ROTATE_90_MODE Rotate90<uint8_t>::get_serialization_mode() const {
  return ROTATE_90_MODE_I_I_UINT8_UINT8;
}

template <>
ROTATE_90_MODE Rotate90<uint16_t>::get_serialization_mode() const {
  return ROTATE_90_MODE_I_I_UINT16_UINT16;
}

template <>
ROTATE_90_MODE Rotate90<uint32_t>::get_serialization_mode() const {
  return ROTATE_90_MODE_I_I_UINT32_UINT32;
}

template <>
ROTATE_90_MODE Rotate90<int8_t>::get_serialization_mode() const {
  return ROTATE_90_MODE_I_I_INT8_INT8;
}

template <>
ROTATE_90_MODE Rotate90<int16_t>::get_serialization_mode() const {
  return ROTATE_90_MODE_I_I_INT16_INT16;
}

template <>
ROTATE_90_MODE Rotate90<int32_t>::get_serialization_mode() const {
  return ROTATE_90_MODE_I_I_INT32_INT32;
}

template <>
ROTATE_90_MODE Rotate90<float>::get_serialization_mode() const {
  return ROTATE_90_MODE_I_I_FLOAT32_FLOAT32;
}
