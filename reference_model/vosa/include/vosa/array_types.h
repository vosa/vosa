/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa_generated.h"
#include <memory>
#include <unsupported/Eigen/CXX11/Tensor>

#ifndef VOSA_CODE_TYPES_H
#define VOSA_CODE_TYPES_H

namespace vosa {

template <typename T>
using EigenImage = Eigen::Tensor<T, 3, Eigen::RowMajor | Eigen::DontAlign>;

template <typename T>
using EigenPlane = Eigen::Tensor<T, 2, Eigen::RowMajor | Eigen::DontAlign>;

template <typename T>
using EigenVector = Eigen::Tensor<T, 1, Eigen::RowMajor | Eigen::DontAlign>;

template <typename T>
using EigenScalar = Eigen::Tensor<T, 0, Eigen::RowMajor | Eigen::DontAlign>;

template <typename T, size_t dim>
using EigenArray = Eigen::Tensor<T, dim, Eigen::RowMajor | Eigen::DontAlign>;

class ArrayBase {
public:
  enum AType { IMAGE, PLANE, VECTOR, SCALAR, ARRAY, DATA };

  enum DType {
    BOOL,
    UINT8,
    UINT16,
    UINT32,
    INT8,
    INT16,
    INT32,
    FLOAT16,
    FLOAT32,
    FLOAT64,
    UNDEFINED
  };

  ArrayBase() = default;

  template <typename T>
  explicit ArrayBase(const EigenArray<T, 4> &array);

  template <typename T>
  explicit ArrayBase(const EigenImage<T> &image);

  template <typename T>
  explicit ArrayBase(const EigenPlane<T> &plane);

  template <typename T>
  explicit ArrayBase(const EigenVector<T> &vector);

  template <typename T>
  explicit ArrayBase(const EigenScalar<T> &scalar);

  template <typename T>
  explicit ArrayBase(const T *data, uint32_t data_size);

  ArrayBase(AType atype, DType dtype, std::vector<uint32_t> dimensions,
            std::vector<uint8_t> data)
      : a_type(atype), d_type(dtype), dimensions(std::move(dimensions)) {
    raw_data_ptr = std::shared_ptr<uint8_t>(new uint8_t[data.size()],
                                            std::default_delete<uint8_t[]>());
    std::memcpy(raw_data_ptr.get(), data.data(), data.size());
  };

  template <typename T>
  EigenArray<T, 4> get_array() const;

  template <typename T>
  EigenImage<T> get_image() const;

  template <typename T>
  EigenPlane<T> get_plane() const;

  template <typename T>
  EigenVector<T> get_vector() const;

  template <typename T>
  EigenScalar<T> get_scalar() const;

  template <typename T>
  const T *get_data() const;

  [[nodiscard]] std::shared_ptr<uint8_t> get_raw_pointer() const {
    return raw_data_ptr;
  }

  [[nodiscard]] std::vector<uint32_t> get_dimensions() const {
    return dimensions;
  }

  [[nodiscard]] AType get_atype() const { return a_type; }
  [[nodiscard]] DType get_dtype() const { return d_type; }

  static vosa_serialization::DTYPE serialize_dtype(DType dtype);

private:
  std::shared_ptr<uint8_t> raw_data_ptr;
  AType a_type;
  DType d_type;
  std::vector<uint32_t> dimensions;
};

class ArrayBaseConversionError : public std::runtime_error {
public:
  explicit ArrayBaseConversionError(const ArrayBase::AType &expected,
                                    const ArrayBase::AType &actual)
      : std::runtime_error(build_message(expected, actual)) {}
  explicit ArrayBaseConversionError(const ArrayBase::DType &expected,
                                    const ArrayBase::DType &actual)
      : std::runtime_error(build_message(expected, actual)) {}

private:
  static std::string build_message(const ArrayBase::AType &expected,
                                   const ArrayBase::AType &actual) {
    return "ArrayBase tried to get a AType " + std::to_string(expected) +
           " value but has a AType " + std::to_string(actual) + " value";
  }
  static std::string build_message(const ArrayBase::DType &expected,
                                   const ArrayBase::DType &actual) {
    return "ArrayBase tried to get a DType " + std::to_string(expected) +
           " value but has a DType " + std::to_string(actual) + " value";
  }
};

} // namespace vosa

#endif // VOSA_CODE_TYPES_H
