/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_ROTATE90_H
#define VOSA_ROTATE90_H

#include "vosa/op.h"

namespace vosa {
class Rotate90Base : public Op {
public:
  static std::shared_ptr<Op> parse_serialized(
      const vosa_serialization::VosaOperatorUnion &vosa_operator_union);
};

template <typename T>
class Rotate90 : public Rotate90Base {
public:
  // Bring any signed number to a choice of {0, 1, 2, 3 (i.e. -1)}
  explicit Rotate90(int32_t rotate) : rotate(((rotate % 4) + 4) % 4) {}

  EigenImage<T> runme(const EigenImage<T> &input) const;

  [[nodiscard]] std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &inputs) const final;
  [[nodiscard]] ArrayBase::DType output_dtype() const final;
  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

private:
  int32_t rotate;

  [[nodiscard]] vosa_serialization::ROTATE_90_MODE
  get_serialization_mode() const;

  EigenImage<T> rotate90_clockwise(const EigenImage<T> &input) const;
  EigenImage<T> rotate90_counterclockwise(const EigenImage<T> &input) const;
  EigenImage<T> rotate180(const EigenImage<T> &input) const;
};
} // namespace vosa

#endif // VOSA_ROTATE90_H
