include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../dialect/include)
include_directories(${CMAKE_CURRENT_BINARY_DIR}/../dialect/include)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-rtti")

add_subdirectory(lib)
add_subdirectory(test)
add_subdirectory(vosa-compile)
add_subdirectory(vosa-fbs-builder)

