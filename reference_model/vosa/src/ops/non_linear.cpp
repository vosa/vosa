/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "non_linear.h"

template <typename T>
vosa::ArrayBase::DType vosa::NonLinearTemplatedBase<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <>
vosa_serialization::DILATE_MODE
vosa::Dilate<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::DILATE_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::DILATE_MODE
vosa::Dilate<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::DILATE_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::DILATE_MODE
vosa::Dilate<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::DILATE_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::DILATE_MODE
vosa::Dilate<int8_t>::get_serialization_mode() const {
  return vosa_serialization::DILATE_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::DILATE_MODE
vosa::Dilate<int16_t>::get_serialization_mode() const {
  return vosa_serialization::DILATE_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::DILATE_MODE
vosa::Dilate<int32_t>::get_serialization_mode() const {
  return vosa_serialization::DILATE_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::DILATE_MODE
vosa::Dilate<float>::get_serialization_mode() const {
  return vosa_serialization::DILATE_MODE_I_I_FLOAT32_FLOAT32;
}

template <>
vosa_serialization::ERODE_MODE
vosa::Erode<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::ERODE_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::ERODE_MODE
vosa::Erode<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::ERODE_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::ERODE_MODE
vosa::Erode<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::ERODE_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::ERODE_MODE
vosa::Erode<int8_t>::get_serialization_mode() const {
  return vosa_serialization::ERODE_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::ERODE_MODE
vosa::Erode<int16_t>::get_serialization_mode() const {
  return vosa_serialization::ERODE_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::ERODE_MODE
vosa::Erode<int32_t>::get_serialization_mode() const {
  return vosa_serialization::ERODE_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::ERODE_MODE
vosa::Erode<float>::get_serialization_mode() const {
  return vosa_serialization::ERODE_MODE_I_I_FLOAT32_FLOAT32;
}

template <>
vosa_serialization::MEDIAN_FILTER_MODE
vosa::MedianFilter<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::MEDIAN_FILTER_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::MEDIAN_FILTER_MODE
vosa::MedianFilter<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::MEDIAN_FILTER_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::MEDIAN_FILTER_MODE
vosa::MedianFilter<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::MEDIAN_FILTER_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::MEDIAN_FILTER_MODE
vosa::MedianFilter<int8_t>::get_serialization_mode() const {
  return vosa_serialization::MEDIAN_FILTER_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::MEDIAN_FILTER_MODE
vosa::MedianFilter<int16_t>::get_serialization_mode() const {
  return vosa_serialization::MEDIAN_FILTER_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::MEDIAN_FILTER_MODE
vosa::MedianFilter<int32_t>::get_serialization_mode() const {
  return vosa_serialization::MEDIAN_FILTER_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::MEDIAN_FILTER_MODE
vosa::MedianFilter<float>::get_serialization_mode() const {
  return vosa_serialization::MEDIAN_FILTER_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Dilate<T>::serialize(const std::string &op_name,
                           const std::vector<std::string> &input_names) const {
  auto dilate_attribute =
      std::make_unique<vosa_serialization::DILATE_ATTRIBUTET>();
  dilate_attribute->filter_dimension = {(uint32_t)this->filter.dimension(0),
                                        (uint32_t)this->filter.dimension(1)};
  auto filter_size = this->filter.size();
  std::vector<uint8_t> value_byte_vector;
  auto byte_filter_ptr = (const uint8_t *)this->filter.data();
  value_byte_vector.insert(value_byte_vector.end(), byte_filter_ptr,
                           byte_filter_ptr + sizeof(T) * filter_size);
  dilate_attribute->filter_value = value_byte_vector;

  vosa_serialization::DILATET dilate;
  dilate.mode = this->get_serialization_mode();
  dilate.attr = std::move(dilate_attribute);

  auto dilate_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  dilate_operator->op.Set(std::move(dilate));

  dilate_operator->name = op_name;
  dilate_operator->inputs = input_names;
  return dilate_operator;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Erode<T>::serialize(const std::string &op_name,
                          const std::vector<std::string> &input_names) const {
  auto erode_attribute =
      std::make_unique<vosa_serialization::ERODE_ATTRIBUTET>();
  erode_attribute->filter_dimension = {(uint32_t)this->filter.dimension(0),
                                       (uint32_t)this->filter.dimension(1)};
  auto filter_size = this->filter.size();
  std::vector<uint8_t> value_byte_vector;
  auto byte_filter_ptr = (const uint8_t *)this->filter.data();
  value_byte_vector.insert(value_byte_vector.end(), byte_filter_ptr,
                           byte_filter_ptr + sizeof(T) * filter_size);
  erode_attribute->filter_value = value_byte_vector;

  vosa_serialization::ERODET erode;
  erode.mode = this->get_serialization_mode();
  erode.attr = std::move(erode_attribute);

  auto erode_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  erode_operator->op.Set(std::move(erode));

  erode_operator->name = op_name;
  erode_operator->inputs = input_names;
  return erode_operator;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::MedianFilter<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {
  auto median_filter_attribute =
      std::make_unique<vosa_serialization::MEDIAN_FILTER_ATTRIBUTET>();
  median_filter_attribute->filter_dimension = {
      (uint32_t)this->filter.dimension(0), (uint32_t)this->filter.dimension(1)};
  auto filter_size = this->filter.size();
  std::vector<uint8_t> value_byte_vector;
  auto byte_filter_ptr = (const uint8_t *)this->filter.data();
  value_byte_vector.insert(value_byte_vector.end(), byte_filter_ptr,
                           byte_filter_ptr + sizeof(T) * filter_size);
  median_filter_attribute->filter_value = value_byte_vector;

  vosa_serialization::MEDIAN_FILTERT median_filter;
  median_filter.mode = this->get_serialization_mode();
  median_filter.attr = std::move(median_filter_attribute);

  auto median_filter_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  median_filter_operator->op.Set(std::move(median_filter));

  median_filter_operator->name = op_name;
  median_filter_operator->inputs = input_names;
  return median_filter_operator;
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::NonLinearTemplatedBase<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <typename T>
T vosa::erode(const std::vector<T> &sorted_vector) {
  return sorted_vector[0];
}

template <typename T>
T vosa::median(const std::vector<T> &sorted_vector) {
  return sorted_vector[sorted_vector.size() / 2];
}

template <typename T>
T vosa::dilate(const std::vector<T> &sorted_vector) {
  return sorted_vector[sorted_vector.size() - 1];
}

template <typename T>
vosa::EigenImage<T>
vosa::non_linear_filter(const EigenImage<T> &input,
                        const EigenPlane<T> &eigen_matrix,
                        non_linear_function_pointer<T> func) {

  uint32_t mask_y_dimension = eigen_matrix.dimension(0);
  uint32_t mask_x_dimension = eigen_matrix.dimension(1);

  uint32_t top_border = (mask_y_dimension + 1) / 2;
  uint32_t left_border = (mask_x_dimension + 1) / 2;

  uint32_t bottom_border = mask_y_dimension - top_border - 1;
  uint32_t right_border = mask_x_dimension - left_border - 1;

  uint32_t height = input.dimension(0);
  uint32_t width = input.dimension(1);
  uint32_t channels = input.dimension(2);

  uint32_t image_bottom_end = height - bottom_border;
  uint32_t image_right_end = width - right_border;

  uint32_t output_height = height - top_border - bottom_border;
  uint32_t output_width = width - left_border - right_border;
  auto eigen_dst =
      vosa::EigenImage<uint8_t>(output_height, output_width, channels);

  for (uint32_t height_index = top_border; height_index < image_bottom_end;
       height_index++) {
    for (uint32_t width_index = left_border; width_index < image_right_end;
         width_index++) {
      for (uint32_t channel_index = 0; channel_index < channels;
           channel_index++) {

        std::vector<uint8_t> values_to_check{};

        for (uint32_t mask_height_index = height_index - top_border;
             mask_height_index < height_index + bottom_border + 1;
             mask_height_index++) {
          for (uint32_t mask_width_index = width_index - left_border;
               mask_width_index < width_index + right_border + 1;
               mask_width_index++) {

            uint32_t mask_index_l =
                mask_height_index - height_index + top_border;
            uint32_t mask_index_m =
                mask_width_index - width_index + left_border;

            uint8_t mask_value = eigen_matrix(mask_index_l, mask_index_m);
            uint8_t pixel_value =
                input(mask_height_index, mask_width_index, channel_index);
            if (mask_value > 0) {
              values_to_check.push_back(pixel_value);
            }
          }
        }

        std::sort(values_to_check.begin(), values_to_check.end());
        T res_val = func(values_to_check);

        uint32_t output_x = height_index - top_border;
        uint32_t output_y = width_index - left_border;
        eigen_dst(output_x, output_y, channel_index) = res_val;
      }
    }
  }
  return eigen_dst;
}

template <typename T>
vosa::EigenImage<T> vosa::Erode<T>::runme(const EigenImage<T> &input) const {
  return non_linear_filter(input, this->filter, erode);
}

template <typename T>
vosa::EigenImage<T> vosa::Dilate<T>::runme(const EigenImage<T> &input) const {
  return non_linear_filter(input, this->filter, dilate);
}

template <typename T>
vosa::EigenImage<T>
vosa::MedianFilter<T>::runme(const EigenImage<T> &input) const {
  return non_linear_filter(input, this->filter, median);
}

template class vosa::Erode<uint8_t>;

template class vosa::Dilate<uint8_t>;

template class vosa::MedianFilter<uint8_t>;