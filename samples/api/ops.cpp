/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <mlir/IR/Verifier.h>
#include <mlir/Parser/Parser.h>

#include "vosa/api/ops.h"

namespace vosa::api {

template <typename T>
ArrayBase::DType convert_cpp_dtype_to_api_dtype();

template <>
ArrayBase::DType convert_cpp_dtype_to_api_dtype<bool>() {
  return ArrayBase::DType::BOOL;
}

template <>
ArrayBase::DType convert_cpp_dtype_to_api_dtype<uint8_t>() {
  return ArrayBase::DType::UINT8;
}

template <>
ArrayBase::DType convert_cpp_dtype_to_api_dtype<uint16_t>() {
  return ArrayBase::DType::UINT16;
}

template <>
ArrayBase::DType convert_cpp_dtype_to_api_dtype<uint32_t>() {
  return ArrayBase::DType::UINT32;
}

template <>
ArrayBase::DType convert_cpp_dtype_to_api_dtype<int8_t>() {
  return ArrayBase::DType::INT8;
}

template <>
ArrayBase::DType convert_cpp_dtype_to_api_dtype<int16_t>() {
  return ArrayBase::DType::INT16;
}

template <>
ArrayBase::DType convert_cpp_dtype_to_api_dtype<int32_t>() {
  return ArrayBase::DType::INT32;
}

template <>
ArrayBase::DType convert_cpp_dtype_to_api_dtype<float>() {
  return ArrayBase::DType::FLOAT32;
}

std::optional<ArrayBase::DType>
convert_builder_dtype_to_api_dtype(dtype_t dtype) {
  switch (dtype) {
  case dtype_t::VOSA_BOOL:
    return ArrayBase::DType::BOOL;
  case dtype_t::VOSA_UINT8:
    return ArrayBase::DType::UINT8;
  case dtype_t::VOSA_UINT16:
    return ArrayBase::DType::UINT16;
  case dtype_t::VOSA_UINT32:
    return ArrayBase::DType::UINT32;
  case dtype_t::VOSA_INT8:
    return ArrayBase::DType::INT8;
  case dtype_t::VOSA_INT16:
    return ArrayBase::DType::INT16;
  case dtype_t::VOSA_INT32:
    return ArrayBase::DType::INT32;
  case dtype_t::VOSA_FLOAT32:
    return ArrayBase::DType::FLOAT32;
  case dtype_t::VOSA_NONE:
    return std::nullopt;
  }
  return std::nullopt;
}

std::optional<dtype_t>
convert_api_dtype_to_builder_dtype(ArrayBase::DType api_dtype) {
  switch (api_dtype) {
  case ArrayBase::DType::BOOL:
    return dtype_t::VOSA_BOOL;
  case (ArrayBase::DType::UINT8):
    return dtype_t::VOSA_UINT8;
  case ArrayBase::DType::UINT16:
    return dtype_t::VOSA_UINT16;
  case ArrayBase::DType::UINT32:
    return dtype_t::VOSA_UINT32;
  case ArrayBase::DType::INT8:
    return dtype_t::VOSA_INT8;
  case ArrayBase::DType::INT16:
    return dtype_t::VOSA_INT16;
  case ArrayBase::DType::INT32:
    return dtype_t::VOSA_INT32;
  case ArrayBase::DType::FLOAT32:
    return dtype_t::VOSA_FLOAT32;
  default:
    return std::nullopt;
  }
}

std::optional<rounding_mode_t>
convert_api_rounding_mode_to_builder_rounding_mode(
    CastBase::rounding_mode_t rounding_mode) {
  switch (rounding_mode) {
  case CastBase::rounding_mode_t::VOSA_ROUNDING_MODE_SATURATE:
    return rounding_mode_t::VOSA_ROUNDING_MODE_SATURATE;
  case CastBase::rounding_mode_t::VOSA_ROUNDING_MODE_WRAP:
    return rounding_mode_t::VOSA_ROUNDING_MODE_WRAP;
  }
  return std::nullopt;
}

std::optional<round_method_t>
convert_api_rounding_method_to_builder_rounding_method(
    RoundBase::round_method_t round_method) {
  switch (round_method) {
  case RoundBase::round_method_t::AWAY_FROM_ZERO:
    return round_method_t::VOSA_ROUND_METHOD_AWAY_FROM_ZERO;
  case RoundBase::round_method_t::CEIL:
    return round_method_t::VOSA_ROUND_METHOD_CEIL;
  case RoundBase::round_method_t::FLOOR:
    return round_method_t::VOSA_ROUND_METHOD_FLOOR;
  case RoundBase::round_method_t::HALF_AWAY_FROM_ZERO:
    return round_method_t::VOSA_ROUND_METHOD_HALF_AWAY_FROM_ZERO;
  case RoundBase::round_method_t::HALF_DOWN:
    return round_method_t::VOSA_ROUND_METHOD_HALF_DOWN;
  case RoundBase::round_method_t::HALF_TO_EVEN:
    return round_method_t::VOSA_ROUND_METHOD_HALF_TO_EVEN;
  case RoundBase::round_method_t::HALF_TO_ODD:
    return round_method_t::VOSA_ROUND_METHOD_HALF_TO_ODD;
  case RoundBase::round_method_t::HALF_TOWARDS_ZERO:
    return round_method_t::VOSA_ROUND_METHOD_HALF_TOWARDS_ZERO;
  case RoundBase::round_method_t::HALF_UP:
    return round_method_t::VOSA_ROUND_METHOD_HALF_UP;
  case RoundBase::round_method_t::TOWARDS_ZERO:
    return round_method_t::VOSA_ROUND_METHOD_TOWARDS_ZERO;
  }
  return std::nullopt;
}

ArrayBase::DType matching_dtype_pair(const std::shared_ptr<Op> &input_1,
                                     const std::shared_ptr<Op> &input_2) {
  auto input_1_dtype = input_1->get_dtype();
  [[maybe_unused]] auto input_2_dtype = input_2->get_dtype();
  assert(input_1_dtype == input_2_dtype);
  return input_1_dtype;
}

std::shared_ptr<vosa::api::Op> input(vosa::api::GraphBuilder &graph_builder,
                                     const ArrayBase::DType dtype,
                                     const std::vector<int64_t> &shape) {

  auto input = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(input);

  llvm::ArrayRef<int64_t> llvm_shape(shape);
  graph_builder.get_mlir_builder().Input(
      name, *convert_api_dtype_to_builder_dtype(dtype), llvm_shape);
  return input;
}

std::shared_ptr<vosa::api::Op> abs(vosa::api::GraphBuilder &graph_builder,
                                   const std::shared_ptr<Op> &input) {
  auto dtype = input->get_dtype();
  auto abs = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(abs);
  graph_builder.get_mlir_builder().Abs(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)});
  return abs;
}

std::shared_ptr<vosa::api::Op> abs_diff(vosa::api::GraphBuilder &graph_builder,
                                        const std::shared_ptr<Op> &input_1,
                                        const std::shared_ptr<Op> &input_2) {
  ArrayBase::DType dtype = matching_dtype_pair(input_1, input_2);
  auto abs_diff = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(abs_diff);
  graph_builder.get_mlir_builder().AbsDiff(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input_1),
       graph_builder.get_name_for_op(input_2)});
  return abs_diff;
}

std::shared_ptr<vosa::api::Op> add(vosa::api::GraphBuilder &graph_builder,
                                   const std::shared_ptr<Op> &input_1,
                                   const std::shared_ptr<Op> &input_2) {
  ArrayBase::DType dtype = matching_dtype_pair(input_1, input_2);
  auto add = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(add);
  graph_builder.get_mlir_builder().Add(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input_1),
       graph_builder.get_name_for_op(input_2)});
  return add;
}

std::shared_ptr<vosa::api::Op>
argmin_channelwise(vosa::api::GraphBuilder &graph_builder,
                   const std::shared_ptr<Op> &input) {
  auto dtype = ArrayBase::DType::UINT32;
  auto argmin_channelwise = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(argmin_channelwise);
  graph_builder.get_mlir_builder().ArgMinChannelwise(
      name, {graph_builder.get_name_for_op(input)});
  return argmin_channelwise;
}

std::shared_ptr<vosa::api::Op>
arithmetic_shift_right(vosa::api::GraphBuilder &graph_builder,
                       const std::shared_ptr<Op> &input, const uint32_t shift) {
  auto dtype = input->get_dtype();
  auto arithmetic_shift_right = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(arithmetic_shift_right);
  graph_builder.get_mlir_builder().ArithmeticShiftRight(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, shift);
  return arithmetic_shift_right;
}

std::shared_ptr<vosa::api::Op> atan2(vosa::api::GraphBuilder &graph_builder,
                                     const std::shared_ptr<Op> &input_1,
                                     const std::shared_ptr<Op> &input_2) {
  return {};
}

std::shared_ptr<vosa::api::Op>
broadcast_channelwise(vosa::api::GraphBuilder &graph_builder,
                      const std::shared_ptr<Op> &input,
                      const uint32_t channels) {
  auto dtype = input->get_dtype();
  auto broadcast_channelwise = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(broadcast_channelwise);
  graph_builder.get_mlir_builder().BroadcastChannelwise(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, channels);
  return broadcast_channelwise;
}

std::shared_ptr<vosa::api::Op>
broadcast_single_pixel(vosa::api::GraphBuilder &graph_builder,
                       const std::shared_ptr<Op> &input,
                       const std::vector<uint32_t> &size) {
  auto dtype = input->get_dtype();
  auto broadcast_scalar = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(broadcast_scalar);
  graph_builder.get_mlir_builder().BroadcastPlanewise(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, size);
  return broadcast_scalar;
}

template <typename T>
std::shared_ptr<vosa::api::Op>
cast(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input,
     const vosa::api::CastBase::rounding_mode_t rounding_mode) {

  ArrayBase::DType dtype = convert_cpp_dtype_to_api_dtype<T>();
  auto cast = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(cast);
  graph_builder.get_mlir_builder().Cast(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)},
      *convert_api_rounding_mode_to_builder_rounding_mode(rounding_mode));
  return cast;
}

// Template function explicit instantiation
template std::shared_ptr<vosa::api::Op>
cast<uint8_t>(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<Op> &input,
              vosa::api::CastBase::rounding_mode_t rounding_mode);
template std::shared_ptr<vosa::api::Op>
cast<uint16_t>(vosa::api::GraphBuilder &graph_builder,
               const std::shared_ptr<Op> &input,
               vosa::api::CastBase::rounding_mode_t rounding_mode);
template std::shared_ptr<vosa::api::Op>
cast<uint32_t>(vosa::api::GraphBuilder &graph_builder,
               const std::shared_ptr<Op> &input,
               vosa::api::CastBase::rounding_mode_t rounding_mode);
template std::shared_ptr<vosa::api::Op>
cast<int8_t>(vosa::api::GraphBuilder &graph_builder,
             const std::shared_ptr<Op> &input,
             vosa::api::CastBase::rounding_mode_t rounding_mode);
template std::shared_ptr<vosa::api::Op>
cast<int16_t>(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<Op> &input,
              vosa::api::CastBase::rounding_mode_t rounding_mode);
template std::shared_ptr<vosa::api::Op>
cast<int32_t>(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<Op> &input,
              vosa::api::CastBase::rounding_mode_t rounding_mode);
template std::shared_ptr<vosa::api::Op>
cast<float>(vosa::api::GraphBuilder &graph_builder,
            const std::shared_ptr<Op> &input,
            vosa::api::CastBase::rounding_mode_t rounding_mode);

std::shared_ptr<vosa::api::Op>
channel_extract(vosa::api::GraphBuilder &graph_builder,
                const std::shared_ptr<Op> &input,
                const std::vector<uint32_t> &channels) {
  auto dtype = input->get_dtype();
  auto channel_extract = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(channel_extract);
  graph_builder.get_mlir_builder().ChannelExtract(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, channels);
  return channel_extract;
}

std::shared_ptr<vosa::api::Op>
channel_extract(vosa::api::GraphBuilder &graph_builder,
                const std::shared_ptr<Op> &input, const uint32_t channel) {
  std::vector<uint32_t> channels = {channel};
  return channel_extract(graph_builder, input, channels);
}

std::shared_ptr<vosa::api::Op> clamp(vosa::api::GraphBuilder &graph_builder,
                                     const std::shared_ptr<Op> &input,
                                     const std::shared_ptr<Op> &min_value,
                                     const std::shared_ptr<Op> &max_value) {
  auto dtype = input->get_dtype();
  auto clamp = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(clamp);
  graph_builder.get_mlir_builder().Clamp(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input),
       graph_builder.get_name_for_op(min_value),
       graph_builder.get_name_for_op(max_value)});
  return clamp;
}

std::shared_ptr<vosa::api::Op>
concat(vosa::api::GraphBuilder &graph_builder,
       const std::vector<std::shared_ptr<Op>> &input) {
  std::vector<std::string> input_names;
  std::vector<vosa::api::ArrayBase::DType> dtypes;
  bool dtype_set = false;
  vosa::api::ArrayBase::DType dtype;
  for (const auto &inp : input) {
    input_names.push_back(graph_builder.get_name_for_op(inp));
    dtypes.push_back(inp->get_dtype());
    if (!dtype_set) {
      dtype = inp->get_dtype();
      dtype_set = true;
    } else
      assert(inp->get_dtype() == dtype);
  }
  auto concat = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(concat);
  graph_builder.get_mlir_builder().Concat(
      name, *convert_api_dtype_to_builder_dtype(dtype), input_names);
  return concat;
}

template <typename T>
std::shared_ptr<vosa::api::Op>
const_image(vosa::api::GraphBuilder &graph_builder,
            const std::vector<uint32_t> &dimensions, const T value) {
  auto dtype = convert_cpp_dtype_to_api_dtype<T>();
  auto const_image = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(const_image);
  graph_builder.get_mlir_builder().ConstImage(
      name, *convert_api_dtype_to_builder_dtype(dtype), dimensions, value);
  return const_image;
}

template <typename T>
std::shared_ptr<vosa::api::Op>
const_image(vosa::api::GraphBuilder &graph_builder,
            const EigenImage<T> &image) {
  auto dtype = convert_cpp_dtype_to_api_dtype<T>();
  auto const_image = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(const_image);

  uint32_t image_height = image.dimension(0);
  uint32_t image_width = image.dimension(1);
  uint32_t image_channels = image.dimension(2);

  const std::vector<uint32_t> filter_dimension = {image_height, image_width,
                                                  image_channels};
  std::vector<T> image_values;

  for (uint32_t image_height_index = 0; image_height_index < image_height;
       image_height_index++) {
    for (uint32_t image_width_index = 0; image_width_index < image_width;
         image_width_index++) {
      for (uint32_t image_channel_index = 0;
           image_channel_index < image_channels; image_channel_index++) {
        T value =
            image(image_height_index, image_width_index, image_channel_index);
        image_values.push_back(value);
      }
    }
  }

  graph_builder.get_mlir_builder().ConstImage(
      name, *convert_api_dtype_to_builder_dtype(dtype), filter_dimension,
      image_values);
  return const_image;
}

// Template function explicit instantiation
template std::shared_ptr<vosa::api::Op>
const_image<uint8_t>(vosa::api::GraphBuilder &graph_builder,
                     const std::vector<uint32_t> &dimensions, uint8_t value);
template std::shared_ptr<vosa::api::Op>
const_image<uint16_t>(vosa::api::GraphBuilder &graph_builder,
                      const std::vector<uint32_t> &dimensions, uint16_t value);
template std::shared_ptr<vosa::api::Op>
const_image<uint32_t>(vosa::api::GraphBuilder &graph_builder,
                      const std::vector<uint32_t> &dimensions, uint32_t value);
template std::shared_ptr<vosa::api::Op>
const_image<int8_t>(vosa::api::GraphBuilder &graph_builder,
                    const std::vector<uint32_t> &dimensions, int8_t value);
template std::shared_ptr<vosa::api::Op>
const_image<int16_t>(vosa::api::GraphBuilder &graph_builder,
                     const std::vector<uint32_t> &dimensions, int16_t value);
template std::shared_ptr<vosa::api::Op>
const_image<int32_t>(vosa::api::GraphBuilder &graph_builder,
                     const std::vector<uint32_t> &dimensions, int32_t value);
template std::shared_ptr<vosa::api::Op>
const_image<float>(vosa::api::GraphBuilder &graph_builder,
                   const std::vector<uint32_t> &dimensions, float value);

template std::shared_ptr<vosa::api::Op>
const_image<uint8_t>(vosa::api::GraphBuilder &graph_builder,
                     const EigenImage<uint8_t> &image);
template std::shared_ptr<vosa::api::Op>
const_image<uint16_t>(vosa::api::GraphBuilder &graph_builder,
                      const EigenImage<uint16_t> &image);
template std::shared_ptr<vosa::api::Op>
const_image<uint32_t>(vosa::api::GraphBuilder &graph_builder,
                      const EigenImage<uint32_t> &image);
template std::shared_ptr<vosa::api::Op>
const_image<int8_t>(vosa::api::GraphBuilder &graph_builder,
                    const EigenImage<int8_t> &image);
template std::shared_ptr<vosa::api::Op>
const_image<int16_t>(vosa::api::GraphBuilder &graph_builder,
                     const EigenImage<int16_t> &image);
template std::shared_ptr<vosa::api::Op>
const_image<int32_t>(vosa::api::GraphBuilder &graph_builder,
                     const EigenImage<int32_t> &image);
template std::shared_ptr<vosa::api::Op>
const_image<float>(vosa::api::GraphBuilder &graph_builder,
                   const EigenImage<float> &image);

template <typename T>
std::shared_ptr<vosa::api::Op>
const_plane(vosa::api::GraphBuilder &graph_builder,
            const std::vector<uint32_t> &dimensions, const T value) {
  auto dtype = convert_cpp_dtype_to_api_dtype<T>();
  auto const_plane = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(const_plane);
  auto image_dimensions = dimensions;
  image_dimensions.push_back(1);
  graph_builder.get_mlir_builder().ConstImage(
      name, *convert_api_dtype_to_builder_dtype(dtype), image_dimensions,
      value);
  return const_plane;
}

// Template function explicit instantiation
template std::shared_ptr<vosa::api::Op>
const_plane<uint8_t>(vosa::api::GraphBuilder &graph_builder,
                     const std::vector<uint32_t> &dimensions, uint8_t value);
template std::shared_ptr<vosa::api::Op>
const_plane<uint16_t>(vosa::api::GraphBuilder &graph_builder,
                      const std::vector<uint32_t> &dimensions, uint16_t value);
template std::shared_ptr<vosa::api::Op>
const_plane<uint32_t>(vosa::api::GraphBuilder &graph_builder,
                      const std::vector<uint32_t> &dimensions, uint32_t value);
template std::shared_ptr<vosa::api::Op>
const_plane<int8_t>(vosa::api::GraphBuilder &graph_builder,
                    const std::vector<uint32_t> &dimensions, int8_t value);
template std::shared_ptr<vosa::api::Op>
const_plane<int16_t>(vosa::api::GraphBuilder &graph_builder,
                     const std::vector<uint32_t> &dimensions, int16_t value);
template std::shared_ptr<vosa::api::Op>
const_plane<int32_t>(vosa::api::GraphBuilder &graph_builder,
                     const std::vector<uint32_t> &dimensions, int32_t value);
template std::shared_ptr<vosa::api::Op>
const_plane<float>(vosa::api::GraphBuilder &graph_builder,
                   const std::vector<uint32_t> &dimensions, float value);

template <typename T>
std::shared_ptr<vosa::api::Op>
const_scalar(vosa::api::GraphBuilder &graph_builder, const T value) {
  auto dtype = convert_cpp_dtype_to_api_dtype<T>();
  auto const_scalar = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(const_scalar);
  graph_builder.get_mlir_builder().ConstScalar(
      name, *convert_api_dtype_to_builder_dtype(dtype), value);
  return const_scalar;
}

// Template function explicit instantiation
template std::shared_ptr<vosa::api::Op>
const_scalar<uint8_t>(vosa::api::GraphBuilder &graph_builder, uint8_t value);
template std::shared_ptr<vosa::api::Op>
const_scalar<uint16_t>(vosa::api::GraphBuilder &graph_builder, uint16_t value);
template std::shared_ptr<vosa::api::Op>
const_scalar<uint32_t>(vosa::api::GraphBuilder &graph_builder, uint32_t value);
template std::shared_ptr<vosa::api::Op>
const_scalar<int8_t>(vosa::api::GraphBuilder &graph_builder, int8_t value);
template std::shared_ptr<vosa::api::Op>
const_scalar<int16_t>(vosa::api::GraphBuilder &graph_builder, int16_t value);
template std::shared_ptr<vosa::api::Op>
const_scalar<int32_t>(vosa::api::GraphBuilder &graph_builder, int32_t value);
template std::shared_ptr<vosa::api::Op>
const_scalar<float>(vosa::api::GraphBuilder &graph_builder, float value);

template <typename T>
std::shared_ptr<vosa::api::Op> conv(vosa::api::GraphBuilder &graph_builder,
                                    const std::shared_ptr<Op> &input,
                                    const EigenPlane<T> &filter) {
  ArrayBase::DType dtype = input->get_dtype();
  auto conv = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(conv);

  uint32_t filter_height = filter.dimension(0);
  uint32_t filter_width = filter.dimension(1);

  const std::vector<uint32_t> filter_dimension = {filter_height, filter_width};
  std::vector<T> filter_value;

  for (uint32_t filter_height_index = 0; filter_height_index < filter_height;
       filter_height_index++) {
    for (uint32_t filter_width_index = 0; filter_width_index < filter_width;
         filter_width_index++) {
      T value = filter(filter_height_index, filter_width_index);
      filter_value.push_back(value);
    }
  }

  graph_builder.get_mlir_builder().Conv2D(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, filter_dimension, filter_value);
  return conv;
}

// Template function explicit instantiation
template std::shared_ptr<vosa::api::Op>
conv<uint8_t>(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<Op> &input,
              const EigenPlane<uint8_t> &filter);
template std::shared_ptr<vosa::api::Op>
conv<uint16_t>(vosa::api::GraphBuilder &graph_builder,
               const std::shared_ptr<Op> &input,
               const EigenPlane<uint16_t> &filter);
template std::shared_ptr<vosa::api::Op>
conv<uint32_t>(vosa::api::GraphBuilder &graph_builder,
               const std::shared_ptr<Op> &input,
               const EigenPlane<uint32_t> &filter);
template std::shared_ptr<vosa::api::Op>
conv<int8_t>(vosa::api::GraphBuilder &graph_builder,
             const std::shared_ptr<Op> &input,
             const EigenPlane<int8_t> &filter);
template std::shared_ptr<vosa::api::Op>
conv<int16_t>(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<Op> &input,
              const EigenPlane<int16_t> &filter);
template std::shared_ptr<vosa::api::Op>
conv<int32_t>(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<Op> &input,
              const EigenPlane<int32_t> &filter);
template std::shared_ptr<vosa::api::Op>
conv<float>(vosa::api::GraphBuilder &graph_builder,
            const std::shared_ptr<Op> &input, const EigenPlane<float> &filter);

std::shared_ptr<vosa::api::Op> crop(vosa::api::GraphBuilder &graph_builder,
                                    const std::shared_ptr<Op> &input,
                                    const std::vector<uint32_t> &crop_window) {
  return {};
}

std::shared_ptr<vosa::api::Op>
custom_scalar(vosa::api::GraphBuilder &graph_builder,
              const std::vector<std::shared_ptr<Op>> &inputs,
              const std::string &op_name) {
  std::vector<std::string> input_names;
  for (const auto &input : inputs)
    input_names.push_back(graph_builder.get_name_for_op(input));

  auto builder_output_dtype =
      graph_builder.get_mlir_builder().CustomScalarOutputType(op_name);
  auto dtype = convert_builder_dtype_to_api_dtype(builder_output_dtype);
  auto custom_scalar = std::make_shared<Op>(*dtype);
  auto name = graph_builder.add_op(custom_scalar);
  graph_builder.get_mlir_builder().CustomScalar(name, input_names, op_name);
  return custom_scalar;
}

std::shared_ptr<vosa::api::Op> div(vosa::api::GraphBuilder &graph_builder,
                                   const std::shared_ptr<Op> &input_1,
                                   const std::shared_ptr<Op> &input_2) {
  ArrayBase::DType dtype = matching_dtype_pair(input_1, input_2);
  auto div = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(div);
  graph_builder.get_mlir_builder().Div(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input_1),
       graph_builder.get_name_for_op(input_2)});
  return div;
}

std::shared_ptr<vosa::api::Op>
decimate(vosa::api::GraphBuilder &graph_builder,
         const std::shared_ptr<Op> &input,
         const std::vector<uint32_t> &decimate_window,
         const std::vector<uint32_t> &offsets) {
  auto dtype = input->get_dtype();
  auto decimate = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(decimate);
  graph_builder.get_mlir_builder().Decimate(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, decimate_window, offsets);
  return decimate;
}

std::shared_ptr<vosa::api::Op> equal(vosa::api::GraphBuilder &graph_builder,
                                     const std::shared_ptr<Op> &input_1,
                                     const std::shared_ptr<Op> &input_2) {
  ArrayBase::DType dtype = ArrayBase::DType::BOOL;
  auto equal = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(equal);
  graph_builder.get_mlir_builder().Equal(
      name, {graph_builder.get_name_for_op(input_1),
             graph_builder.get_name_for_op(input_2)});
  return equal;
}

template <typename T>
std::shared_ptr<vosa::api::Op>
expand(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input,
       const std::shared_ptr<Op> &expand_kernel, const T fill_value,
       const EigenPlane<uint8_t> &gather_kernel) {
  auto dtype = input->get_dtype();
  auto expand = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(expand);
  std::vector<uint32_t> gather_kernel_dimension = {
      (uint32_t)gather_kernel.dimension(0),
      (uint32_t)gather_kernel.dimension(1)};
  std::vector<uint8_t> gather_kernel_values;
  for (uint32_t height_index = 0; height_index < gather_kernel.dimension(0);
       height_index++) {
    for (uint32_t width_index = 0; width_index < gather_kernel.dimension(1);
         width_index++) {
      gather_kernel_values.push_back(gather_kernel(height_index, width_index));
    }
  }
  graph_builder.get_mlir_builder().Expand(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input),
       graph_builder.get_name_for_op(expand_kernel)},
      fill_value, gather_kernel_dimension, gather_kernel_values);
  return expand;
}

// Template function explicit instantiation
template std::shared_ptr<vosa::api::Op>
expand<uint8_t>(vosa::api::GraphBuilder &graph_builder,
                const std::shared_ptr<Op> &input,
                const std::shared_ptr<Op> &expand_kernel, uint8_t fill_value,
                const EigenPlane<uint8_t> &gather_kernel);
template std::shared_ptr<vosa::api::Op>
expand<uint16_t>(vosa::api::GraphBuilder &graph_builder,
                 const std::shared_ptr<Op> &input,
                 const std::shared_ptr<Op> &expand_kernel, uint16_t fill_value,
                 const EigenPlane<uint8_t> &gather_kernel);
template std::shared_ptr<vosa::api::Op>
expand<uint32_t>(vosa::api::GraphBuilder &graph_builder,
                 const std::shared_ptr<Op> &input,
                 const std::shared_ptr<Op> &expand_kernel, uint32_t fill_value,
                 const EigenPlane<uint8_t> &gather_kernel);
template std::shared_ptr<vosa::api::Op>
expand<int8_t>(vosa::api::GraphBuilder &graph_builder,
               const std::shared_ptr<Op> &input,
               const std::shared_ptr<Op> &expand_kernel, int8_t fill_value,
               const EigenPlane<uint8_t> &gather_kernel);
template std::shared_ptr<vosa::api::Op>
expand<int16_t>(vosa::api::GraphBuilder &graph_builder,
                const std::shared_ptr<Op> &input,
                const std::shared_ptr<Op> &expand_kernel, int16_t fill_value,
                const EigenPlane<uint8_t> &gather_kernel);
template std::shared_ptr<vosa::api::Op>
expand<int32_t>(vosa::api::GraphBuilder &graph_builder,
                const std::shared_ptr<Op> &input,
                const std::shared_ptr<Op> &expand_kernel, int32_t fill_value,
                const EigenPlane<uint8_t> &gather_kernel);
template std::shared_ptr<vosa::api::Op>
expand<float>(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<Op> &input,
              const std::shared_ptr<Op> &expand_kernel, float fill_value,
              const EigenPlane<uint8_t> &gather_kernel);

std::shared_ptr<vosa::api::Op>
gamma_correction(vosa::api::GraphBuilder &graph_builder,
                 const std::shared_ptr<Op> &input, const float gamma) {
  auto dtype = input->get_dtype();
  auto gamma_correction = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(gamma_correction);
  graph_builder.get_mlir_builder().GammaCorrection(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, gamma);
  return gamma_correction;
}

std::shared_ptr<vosa::api::Op> greater(vosa::api::GraphBuilder &graph_builder,
                                       const std::shared_ptr<Op> &input_1,
                                       const std::shared_ptr<Op> &input_2) {
  ArrayBase::DType dtype = ArrayBase::DType::BOOL;
  auto greater = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(greater);
  graph_builder.get_mlir_builder().Greater(
      name, {graph_builder.get_name_for_op(input_1),
             graph_builder.get_name_for_op(input_2)});
  return greater;
}

std::shared_ptr<vosa::api::Op>
greater_equal(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<Op> &input_1,
              const std::shared_ptr<Op> &input_2) {
  ArrayBase::DType dtype = ArrayBase::DType::BOOL;
  auto greater_equal = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(greater_equal);
  graph_builder.get_mlir_builder().GreaterEqual(
      name, {graph_builder.get_name_for_op(input_1),
             graph_builder.get_name_for_op(input_2)});
  return greater_equal;
}

std::shared_ptr<vosa::api::Op>
import_channel(vosa::api::GraphBuilder &graph_builder,
               const std::shared_ptr<Op> &input, const uint32_t stride,
               const uint32_t offset, const std::vector<uint32_t> &shape) {
  auto dtype = input->get_dtype();
  auto import_channel = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(import_channel);
  graph_builder.get_mlir_builder().ImportChannel(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, stride, offset, shape);
  return import_channel;
}

std::shared_ptr<vosa::api::Op>
logical_shift_right(vosa::api::GraphBuilder &graph_builder,
                    const std::shared_ptr<Op> &input, const uint32_t shift) {
  auto dtype = input->get_dtype();
  auto logical_shift_right = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(logical_shift_right);
  graph_builder.get_mlir_builder().LogicalShiftRight(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, shift);
  return logical_shift_right;
}

std::shared_ptr<vosa::api::Op> max(vosa::api::GraphBuilder &graph_builder,
                                   const std::shared_ptr<Op> &input_1,
                                   const std::shared_ptr<Op> &input_2) {
  ArrayBase::DType dtype = matching_dtype_pair(input_1, input_2);
  auto max = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(max);
  graph_builder.get_mlir_builder().Max(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input_1),
       graph_builder.get_name_for_op(input_2)});
  return max;
}

std::shared_ptr<vosa::api::Op>
mesh_grid(vosa::api::GraphBuilder &graph_builder,
          const std::vector<uint32_t> &dimensions) {
  ArrayBase::DType dtype = ArrayBase::DType::UINT32;
  auto min = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(min);
  graph_builder.get_mlir_builder().MeshGrid(
      name, *convert_api_dtype_to_builder_dtype(dtype), {}, dimensions);
  return min;
}

std::shared_ptr<vosa::api::Op> min(vosa::api::GraphBuilder &graph_builder,
                                   const std::shared_ptr<Op> &input_1,
                                   const std::shared_ptr<Op> &input_2) {
  ArrayBase::DType dtype = matching_dtype_pair(input_1, input_2);
  auto min = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(min);
  graph_builder.get_mlir_builder().Min(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input_1),
       graph_builder.get_name_for_op(input_2)});
  return min;
}

std::shared_ptr<vosa::api::Op> mod(vosa::api::GraphBuilder &graph_builder,
                                   const std::shared_ptr<Op> &input_1,
                                   const std::shared_ptr<Op> &input_2) {
  ArrayBase::DType dtype = matching_dtype_pair(input_1, input_2);
  auto mod = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(mod);
  graph_builder.get_mlir_builder().Mod(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input_1),
       graph_builder.get_name_for_op(input_2)});
  return mod;
}

std::shared_ptr<vosa::api::Op> mult(vosa::api::GraphBuilder &graph_builder,
                                    const std::shared_ptr<Op> &input_1,
                                    const std::shared_ptr<Op> &input_2) {
  ArrayBase::DType dtype = matching_dtype_pair(input_1, input_2);
  auto mult = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(mult);
  graph_builder.get_mlir_builder().Mult(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input_1),
       graph_builder.get_name_for_op(input_2)});
  return mult;
}

std::shared_ptr<vosa::api::Op> not_(vosa::api::GraphBuilder &graph_builder,
                                    const std::shared_ptr<Op> &input) {
  auto dtype = input->get_dtype();
  auto not_ = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(not_);
  graph_builder.get_mlir_builder().Not(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)});
  return not_;
}

std::shared_ptr<vosa::api::Op> or_(vosa::api::GraphBuilder &graph_builder,
                                   const std::shared_ptr<Op> &input_1,
                                   const std::shared_ptr<Op> &input_2) {
  ArrayBase::DType dtype = matching_dtype_pair(input_1, input_2);
  auto or_ = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(or_);
  graph_builder.get_mlir_builder().Or(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input_1),
       graph_builder.get_name_for_op(input_2)});
  return or_;
}

pad_mode_t convert_api_pad_mode_to_builder_pad_mode(
    vosa::api::PadBase::pad_mode_t pad_mode) {
  switch (pad_mode) {
  case vosa::api::PadBase::pad_mode_t::VOSA_PAD_MODE_REFLECT:
    return pad_mode_t::VOSA_PAD_MODE_REFLECT;
  case vosa::api::PadBase::pad_mode_t::VOSA_PAD_MODE_CONSTANT:
    return pad_mode_t::VOSA_PAD_MODE_CONSTANT;
  case vosa::api::PadBase::pad_mode_t::VOSA_PAD_MODE_MIRROR:
    return pad_mode_t::VOSA_PAD_MODE_MIRROR;
  case vosa::api::PadBase::pad_mode_t::VOSA_PAD_MODE_REPLICATE:
    return pad_mode_t::VOSA_PAD_MODE_REPLICATE;
  default:
    return pad_mode_t::VOSA_PAD_MODE_REPLICATE;
  }
}

template <typename T>
std::shared_ptr<vosa::api::Op>
vosa::api::pad(vosa::api::GraphBuilder &graph_builder,
               const std::shared_ptr<Op> &input, const T pad_constant,
               const vosa::api::PadBase::pad_mode_t pad_mode,
               const std::vector<uint32_t> &padding) {
  auto dtype = input->get_dtype();
  auto pad = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(pad);
  graph_builder.get_mlir_builder().Pad(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, pad_constant,
      convert_api_pad_mode_to_builder_pad_mode(pad_mode), padding);
  return pad;
}

// Template function explicit instantiation
template std::shared_ptr<vosa::api::Op>
pad<uint8_t>(vosa::api::GraphBuilder &graph_builder,
             const std::shared_ptr<Op> &, uint8_t,
             vosa::api::PadBase::pad_mode_t, const std::vector<uint32_t> &);
template std::shared_ptr<vosa::api::Op>
pad<uint16_t>(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<Op> &, uint16_t,
              vosa::api::PadBase::pad_mode_t, const std::vector<uint32_t> &);
template std::shared_ptr<vosa::api::Op>
pad<uint32_t>(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<Op> &, uint32_t,
              vosa::api::PadBase::pad_mode_t, const std::vector<uint32_t> &);
template std::shared_ptr<vosa::api::Op>
pad<int8_t>(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &,
            int8_t, vosa::api::PadBase::pad_mode_t,
            const std::vector<uint32_t> &);
template std::shared_ptr<vosa::api::Op>
pad<int16_t>(vosa::api::GraphBuilder &graph_builder,
             const std::shared_ptr<Op> &, int16_t,
             vosa::api::PadBase::pad_mode_t, const std::vector<uint32_t> &);
template std::shared_ptr<vosa::api::Op>
pad<int32_t>(vosa::api::GraphBuilder &graph_builder,
             const std::shared_ptr<Op> &, int32_t,
             vosa::api::PadBase::pad_mode_t, const std::vector<uint32_t> &);
template std::shared_ptr<vosa::api::Op>
pad<float>(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &,
           float, vosa::api::PadBase::pad_mode_t,
           const std::vector<uint32_t> &);

template <typename T>
std::shared_ptr<vosa::api::Op>
piecewise_linear(vosa::api::GraphBuilder &graph_builder,
                 const std::shared_ptr<Op> &input,
                 const vosa::api::EigenVector<T> &nodes,
                 const vosa::api::EigenVector<T> &values) {
  auto dtype = input->get_dtype();
  auto piecewise_linear = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(piecewise_linear);

  uint32_t NNodes = nodes.dimension(0);
  uint32_t NValues = values.dimension(0);

  std::vector<T> vector_nodes, vector_values;

  for (uint32_t node_index = 0; node_index < NNodes; node_index++) {
    vector_nodes.push_back(nodes(node_index));
  }
  for (uint32_t value_index = 0; value_index < NValues; value_index++) {
    vector_values.push_back(values(value_index));
  }
  graph_builder.get_mlir_builder().PiecewiseLinear<T>(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, vector_nodes, vector_values);
  return piecewise_linear;
}

// Template function explicit instantiation
template std::shared_ptr<vosa::api::Op>
piecewise_linear<int8_t>(vosa::api::GraphBuilder &graph_builder,
                         const std::shared_ptr<Op> &input,
                         const vosa::api::EigenVector<int8_t> &nodes,
                         const vosa::api::EigenVector<int8_t> &values);
template std::shared_ptr<vosa::api::Op>
piecewise_linear<int16_t>(vosa::api::GraphBuilder &graph_builder,
                          const std::shared_ptr<Op> &input,
                          const vosa::api::EigenVector<int16_t> &nodes,
                          const vosa::api::EigenVector<int16_t> &values);
template std::shared_ptr<vosa::api::Op>
piecewise_linear<int32_t>(vosa::api::GraphBuilder &graph_builder,
                          const std::shared_ptr<Op> &input,
                          const vosa::api::EigenVector<int32_t> &nodes,
                          const vosa::api::EigenVector<int32_t> &values);
template std::shared_ptr<vosa::api::Op>
piecewise_linear<float>(vosa::api::GraphBuilder &graph_builder,
                        const std::shared_ptr<Op> &input,
                        const vosa::api::EigenVector<float> &nodes,
                        const vosa::api::EigenVector<float> &values);

template <typename T>
std::shared_ptr<vosa::api::Op> pointwise_matrix_multiply(
    vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input,
    const EigenPlane<T> &matrix, const EigenVector<T> &inner_offsets,
    const EigenVector<T> &outer_offsets) {
  auto dtype = input->get_dtype();
  auto pointwise_matrix_multiply = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(pointwise_matrix_multiply);
  uint32_t C1 = inner_offsets.dimension(0);
  uint32_t C2 = outer_offsets.dimension(0);

  std::vector<T> M, K1, K2;

  for (uint32_t C2_index = 0; C2_index < C2; C2_index++) {
    for (uint32_t C1_index = 0; C1_index < C1; C1_index++) {
      M.push_back(matrix(C2_index, C1_index));
    }
  }
  for (uint32_t C1_index = 0; C1_index < C1; C1_index++) {
    K1.push_back(inner_offsets(C1_index));
  }
  for (uint32_t C2_index = 0; C2_index < C2; C2_index++) {
    K2.push_back(outer_offsets(C2_index));
  }
  graph_builder.get_mlir_builder().PointwiseMatrixMultiply(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, M, K1, K2);
  return pointwise_matrix_multiply;
}

// Template function explicit instantiation
template std::shared_ptr<vosa::api::Op>
pointwise_matrix_multiply(vosa::api::GraphBuilder &graph_builder,
                          const std::shared_ptr<Op> &input,
                          const EigenPlane<uint8_t> &matrix,
                          const EigenVector<uint8_t> &inner_offsets,
                          const EigenVector<uint8_t> &outer_offsets);

template std::shared_ptr<vosa::api::Op>
pointwise_matrix_multiply(vosa::api::GraphBuilder &graph_builder,
                          const std::shared_ptr<Op> &input,
                          const EigenPlane<uint16_t> &matrix,
                          const EigenVector<uint16_t> &inner_offsets,
                          const EigenVector<uint16_t> &outer_offsets);

template std::shared_ptr<vosa::api::Op>
pointwise_matrix_multiply(vosa::api::GraphBuilder &graph_builder,
                          const std::shared_ptr<Op> &input,
                          const EigenPlane<uint32_t> &matrix,
                          const EigenVector<uint32_t> &inner_offsets,
                          const EigenVector<uint32_t> &outer_offsets);

template std::shared_ptr<vosa::api::Op> pointwise_matrix_multiply(
    vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input,
    const EigenPlane<int8_t> &matrix, const EigenVector<int8_t> &inner_offsets,
    const EigenVector<int8_t> &outer_offsets);

template std::shared_ptr<vosa::api::Op>
pointwise_matrix_multiply(vosa::api::GraphBuilder &graph_builder,
                          const std::shared_ptr<Op> &input,
                          const EigenPlane<int16_t> &matrix,
                          const EigenVector<int16_t> &inner_offsets,
                          const EigenVector<int16_t> &outer_offsets);
template std::shared_ptr<vosa::api::Op>
pointwise_matrix_multiply(vosa::api::GraphBuilder &graph_builder,
                          const std::shared_ptr<Op> &input,
                          const EigenPlane<int32_t> &matrix,
                          const EigenVector<int32_t> &inner_offsets,
                          const EigenVector<int32_t> &outer_offsets);
template std::shared_ptr<vosa::api::Op> pointwise_matrix_multiply(
    vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input,
    const EigenPlane<float> &matrix, const EigenVector<float> &inner_offsets,
    const EigenVector<float> &outer_offsets);

std::shared_ptr<vosa::api::Op> power(vosa::api::GraphBuilder &graph_builder,
                                     const std::shared_ptr<Op> &input,
                                     const float base) {
  auto dtype = vosa::api::ArrayBase::DType::FLOAT32;
  auto power = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(power);
  graph_builder.get_mlir_builder().Power(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, base);
  return power;
}

std::shared_ptr<vosa::api::Op>
print(vosa::api::GraphBuilder &graph_builder,
      const std::shared_ptr<vosa::api::Op> &input, const std::string &message) {
  return input;
}

std::shared_ptr<vosa::api::Op>
reduce_interp_channelwise(vosa::api::GraphBuilder &graph_builder,
                          const std::shared_ptr<Op> &input_1,
                          const std::shared_ptr<Op> &input_2) {
  auto dtype = vosa::api::ArrayBase::DType::FLOAT32;
  auto reduce_interp_channelwise = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(reduce_interp_channelwise);
  graph_builder.get_mlir_builder().ReduceInterpChannelwise(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input_1),
       graph_builder.get_name_for_op(input_2)});
  return reduce_interp_channelwise;
}

std::shared_ptr<vosa::api::Op>
reduce_max_channelwise(vosa::api::GraphBuilder &graph_builder,
                       const std::shared_ptr<Op> &input) {
  auto dtype = input->get_dtype();
  auto reduce_max_channelwise = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(reduce_max_channelwise);
  graph_builder.get_mlir_builder().ReduceMaxChannelwise(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)});
  return reduce_max_channelwise;
}

std::shared_ptr<vosa::api::Op>
reduce_max_planewise(vosa::api::GraphBuilder &graph_builder,
                     const std::shared_ptr<Op> &input) {
  auto dtype = input->get_dtype();
  auto reduce_max_planewise = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(reduce_max_planewise);
  graph_builder.get_mlir_builder().ReduceMaxPlanewise(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)});
  return reduce_max_planewise;
}

std::shared_ptr<vosa::api::Op>
reduce_min_planewise(vosa::api::GraphBuilder &graph_builder,
                     const std::shared_ptr<Op> &input) {
  auto dtype = input->get_dtype();
  auto reduce_min_planewise = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(reduce_min_planewise);
  graph_builder.get_mlir_builder().ReduceMinPlanewise(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)});
  return reduce_min_planewise;
}

std::shared_ptr<vosa::api::Op>
reduce_sum_channelwise(vosa::api::GraphBuilder &graph_builder,
                       const std::shared_ptr<Op> &input) {
  auto dtype = input->get_dtype();
  auto reduce_sum_channelwise = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(reduce_sum_channelwise);
  graph_builder.get_mlir_builder().ReduceSumChannelwise(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)});
  return reduce_sum_channelwise;
}

std::shared_ptr<vosa::api::Op>
reduce_sum_planewise(vosa::api::GraphBuilder &graph_builder,
                     const std::shared_ptr<Op> &input) {
  auto dtype = input->get_dtype();
  auto reduce_sum_planewise = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(reduce_sum_planewise);
  graph_builder.get_mlir_builder().ReduceSumPlanewise(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)});
  return reduce_sum_planewise;
}

std::shared_ptr<vosa::api::Op>
resize_bilinear(vosa::api::GraphBuilder &graph_builder,
                const std::shared_ptr<Op> &input,
                const std::vector<uint32_t> &size) {
  auto dtype = input->get_dtype();
  auto resize_bilinear = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(resize_bilinear);
  graph_builder.get_mlir_builder().ResizeBilinear(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, size);
  return resize_bilinear;
}

std::shared_ptr<vosa::api::Op>
resize_nearest_neighbour(vosa::api::GraphBuilder &graph_builder,
                         const std::shared_ptr<Op> &input,
                         const std::vector<uint32_t> &size) {
  auto dtype = input->get_dtype();
  auto resize_nearest_neighbour = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(resize_nearest_neighbour);
  graph_builder.get_mlir_builder().ResizeNearestNeighbour(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, size);
  return resize_nearest_neighbour;
}

std::shared_ptr<vosa::api::Op> rotate90(vosa::api::GraphBuilder &graph_builder,
                                        const std::shared_ptr<Op> &input,
                                        const int rotation) {
  auto dtype = input->get_dtype();
  auto rotate90 = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(rotate90);
  graph_builder.get_mlir_builder().Rotate90(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, rotation);
  return rotate90;
}

std::shared_ptr<vosa::api::Op>
round(vosa::api::GraphBuilder &graph_builder, const std::shared_ptr<Op> &input,
      const vosa::api::RoundBase::round_method_t round_method) {
  auto dtype = vosa::api::ArrayBase::DType::INT32;
  auto round = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(round);
  auto round_mode =
      convert_api_rounding_method_to_builder_rounding_method(round_method);
  graph_builder.get_mlir_builder().Round(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)}, *round_mode);
  return round;
}

std::shared_ptr<vosa::api::Op> sqrt(vosa::api::GraphBuilder &graph_builder,
                                    const std::shared_ptr<Op> &input) {
  ArrayBase::DType dtype = input->get_dtype();
  auto sqrt = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(sqrt);
  graph_builder.get_mlir_builder().Sqrt(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input)});
  return sqrt;
}

std::shared_ptr<vosa::api::Op> sub(vosa::api::GraphBuilder &graph_builder,
                                   const std::shared_ptr<Op> &input_1,
                                   const std::shared_ptr<Op> &input_2) {
  ArrayBase::DType dtype = matching_dtype_pair(input_1, input_2);
  auto sub = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(sub);
  graph_builder.get_mlir_builder().Sub(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input_1),
       graph_builder.get_name_for_op(input_2)});
  return sub;
}

std::shared_ptr<vosa::api::Op>
tosa_graph(vosa::api::GraphBuilder &graph_builder,
           const std::vector<std::shared_ptr<Op>> &inputs,
           const std::string &tosa_mlir_path, const std::string &tosa_func_name,
           const std::vector<uint32_t> &output_shape) {

  std::vector<std::string> input_names;
  for (const auto &input : inputs)
    input_names.push_back(graph_builder.get_name_for_op(input));

  mlir::OwningOpRef<mlir::ModuleOp> tosa_module =
      mlir::parseSourceFile<mlir::ModuleOp>(tosa_mlir_path,
                                            &graph_builder.get_context());
  if (!tosa_module) {
    llvm::errs() << "Error can't load tosa mlir file " << tosa_mlir_path
                 << "\n";
    return {};
  }
  mlir::OwningOpRef<mlir::ModuleOp> &module =
      graph_builder.get_custom_ops_reference();
  mlir::OpBuilder b(module->getBodyRegion());

  auto new_tosa_func_name = graph_builder.get_name_for_tosa_graph();
  auto func = tosa_module->lookupSymbol<mlir::func::FuncOp>(tosa_func_name);
  func = mlir::cast<mlir::func::FuncOp>(b.clone(*func.getOperation()));
  func.setSymName(new_tosa_func_name);
  func.setPrivate();

  auto verified = mlir::verify(graph_builder.get_custom_ops_reference().get());
  if (verified.failed()) {
    llvm::errs() << "Failed to verify Custom Ops module after TOSA import\n";
    graph_builder.get_custom_ops_reference()->dump();
  }

  auto builder_output_dtype =
      graph_builder.get_mlir_builder().CustomScalarOutputType(
          new_tosa_func_name);
  auto dtype = convert_builder_dtype_to_api_dtype(builder_output_dtype);
  auto tosa_graph = std::make_shared<Op>(*dtype);
  auto name = graph_builder.add_op(tosa_graph);
  graph_builder.get_mlir_builder().TosaGraph(name, input_names,
                                             new_tosa_func_name, output_shape);
  return tosa_graph;
}

std::shared_ptr<vosa::api::Op> where(vosa::api::GraphBuilder &graph_builder,
                                     const std::shared_ptr<Op> &input_1,
                                     const std::shared_ptr<Op> &input_2,
                                     const std::shared_ptr<Op> &where_input) {
  ArrayBase::DType dtype = matching_dtype_pair(input_1, input_2);
  auto where = std::make_shared<Op>(dtype);
  auto name = graph_builder.add_op(where);
  graph_builder.get_mlir_builder().Where(
      name, *convert_api_dtype_to_builder_dtype(dtype),
      {graph_builder.get_name_for_op(input_1),
       graph_builder.get_name_for_op(input_2),
       graph_builder.get_name_for_op(where_input)});
  return where;
}
} // namespace vosa::api