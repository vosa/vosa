/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <iostream>

#include "gtest/gtest.h"

#include "data_utils.h"
#include "ops/pad.h"
#include "test_bench.h"
#include "vosa/vosa.h"

using namespace std;

TEST(test_pad, test_pad_replicate) {

  vosa::EigenImage<uint8_t> input{3, 6, 1};
  input.setValues({{{1}, {2}, {3}, {4}, {5}, {6}},
                   {{7}, {8}, {9}, {10}, {11}, {12}},
                   {{13}, {14}, {15}, {16}, {17}, {18}}});

  vosa::EigenScalar<uint8_t> pad_constant{};
  pad_constant.setZero();

  std::vector<uint32_t> pad_size = {2, 2, 2, 2};

  auto output_as_image =
      vosa::Pad<uint8_t>(0, vosa::Pad<uint8_t>::VOSA_PAD_MODE_REPLICATE,
                         pad_size)
          .runme(input);
  auto zero_32 = vosa::utils::DataUtils<uint32_t>::scalarFromCPP(0);
  auto output_as_plane =
      vosa::test::Packing<uint8_t>::extract_plane(output_as_image, 0);

  vosa::EigenPlane<uint8_t> expected_output{7, 10};
  expected_output.setValues({{1, 1, 1, 2, 3, 4, 5, 6, 6, 6},
                             {1, 1, 1, 2, 3, 4, 5, 6, 6, 6},
                             {1, 1, 1, 2, 3, 4, 5, 6, 6, 6},
                             {7, 7, 7, 8, 9, 10, 11, 12, 12, 12},
                             {13, 13, 13, 14, 15, 16, 17, 18, 18, 18},
                             {13, 13, 13, 14, 15, 16, 17, 18, 18, 18},
                             {13, 13, 13, 14, 15, 16, 17, 18, 18, 18}});

  bool compare0 = vosa::test::TestBench<uint8_t, int32_t>::compareEigenPlanes(
      output_as_plane, expected_output);
  EXPECT_TRUE(compare0);
}

TEST(test_pad, test_pad_asymmetric) {
  vosa::EigenImage<uint8_t> input{3, 6, 1};
  input.setValues({{{1}, {2}, {3}, {4}, {5}, {6}},
                   {{7}, {8}, {9}, {10}, {11}, {12}},
                   {{13}, {14}, {15}, {16}, {17}, {18}}});

  vosa::EigenScalar<uint8_t> pad_constant{};
  pad_constant.setZero();

  std::vector<uint32_t> pad_size = {0, 2, 0, 2};

  auto output_as_image =
      vosa::Pad<uint8_t>(0, vosa::Pad<uint8_t>::VOSA_PAD_MODE_REPLICATE,
                         pad_size)
          .runme(input);
  auto zero_32 = vosa::utils::DataUtils<uint32_t>::scalarFromCPP(0);
  auto output_as_plane =
      vosa::test::Packing<uint8_t>::extract_plane(output_as_image, 0);

  vosa::EigenPlane<uint8_t> expected_output{5, 8};
  expected_output.setValues({{1, 2, 3, 4, 5, 6, 6, 6},
                             {7, 8, 9, 10, 11, 12, 12, 12},
                             {13, 14, 15, 16, 17, 18, 18, 18},
                             {13, 14, 15, 16, 17, 18, 18, 18},
                             {13, 14, 15, 16, 17, 18, 18, 18}});

  bool compare0 = vosa::test::TestBench<uint8_t, int32_t>::compareEigenPlanes(
      output_as_plane, expected_output);
  EXPECT_TRUE(compare0);
}

TEST(test_pad, test_pad_reflect) {

  // test reflect
  vosa::EigenImage<uint8_t> input{3, 6, 1};
  input.setValues({{{1}, {2}, {3}, {4}, {5}, {6}},
                   {{7}, {8}, {9}, {10}, {11}, {12}},
                   {{13}, {14}, {15}, {16}, {17}, {18}}});

  std::vector<uint32_t> pad_size = {2, 2, 3, 3};
  auto output_as_image =
      vosa::Pad<uint8_t>(0, vosa::Pad<uint8_t>::VOSA_PAD_MODE_REFLECT, pad_size)
          .runme(input);
  auto zero_32 = vosa::utils::DataUtils<uint32_t>::scalarFromCPP(0);

  auto output_as_plane =
      vosa::test::Packing<uint8_t>::extract_plane(output_as_image, 0);

  vosa::EigenPlane<uint8_t> expected_output{7, 12};
  expected_output.setValues({{16, 15, 14, 13, 14, 15, 16, 17, 18, 17, 16, 15},
                             {10, 9, 8, 7, 8, 9, 10, 11, 12, 11, 10, 9},
                             {4, 3, 2, 1, 2, 3, 4, 5, 6, 5, 4, 3},
                             {10, 9, 8, 7, 8, 9, 10, 11, 12, 11, 10, 9},
                             {16, 15, 14, 13, 14, 15, 16, 17, 18, 17, 16, 15},
                             {10, 9, 8, 7, 8, 9, 10, 11, 12, 11, 10, 9},
                             {4, 3, 2, 1, 2, 3, 4, 5, 6, 5, 4, 3}});

  bool compare1 = vosa::test::TestBench<uint8_t, int32_t>::compareEigenPlanes(
      output_as_plane, expected_output);

  EXPECT_EQ(compare1, true);
}