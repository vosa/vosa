/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_PPM_UTILS_H
#define VOSA_PPM_UTILS_H

#include <fstream>
#include <iostream>

/*
  Utility functions used to load/save PPM (and PGM) file.
     - Only supports "P5"(binary gray) and "P6"(binary color) format.
     - Supports uint8 and uint16 as data type
     - Assumes the byte order of frame data is "big endian".

  Refer to http://netpbm.sourceforge.net/doc/ppm.html
 */
namespace vosa::api::ppm {

template <typename T>
[[maybe_unused]] bool parseHeader(std::ifstream &ifs, int n_channels,
                                  int &width, int &height);

template <typename T>
[[maybe_unused]] bool createHeader(std::ofstream &ofs, int n_channels,
                                   int width, int height);

template <typename T>
inline T swapByte(T a) {
  return a;
}

template <>
inline uint8_t swapByte(uint8_t a) {
  return a;
}

template <>
inline uint16_t swapByte<uint16_t>(uint16_t a) {
  return __builtin_bswap16(a);
}

template <typename T>
[[maybe_unused]] inline T readVal(std::ifstream &ifs) {
  // read data and swap bytes from big endian to little endian
  T dataRead;
  ifs.read(reinterpret_cast<char *>(&dataRead), sizeof(T));
  return swapByte<T>(dataRead);
}

template <typename T>
[[maybe_unused]] inline void writeVal(std::ofstream &ofs, T val) {
  // swap bytes from little endian to big endian, then write
  T dataWrite = swapByte<T>(val);
  ofs.write(reinterpret_cast<char *>(&dataWrite), sizeof(T));
}

template <typename T>
bool read_input(const std::string &input, std::vector<T> &data, int &width,
                int &height, int n_channels = 1) {
  std::ifstream ifs(input, std::ifstream::in);
  if (!ifs) {
    std::cout << "Failed to open " << input << " for reading " << std::endl;
    return false;
  }
  bool parse_success =
      vosa::api::ppm::parseHeader<T>(ifs, n_channels, width, height);
  if (!parse_success)
    return false;
  data.clear();
  data.resize(width * height * n_channels);
  for (uint32_t index = 0; index < width * height * n_channels; index++)
    data[index] = vosa::api::ppm::readVal<T>(ifs);
  return true;
}

template <typename T>
bool write_output(const std::string &output, const std::vector<T> &data,
                  const int &width, const int &height,
                  const int &channels = 1) {
  std::ofstream ofs(output, std::ofstream::out);
  if (!ofs) {
    std::cout << "Failed to open " << output << " for writing " << std::endl;
    return false;
  }
  bool createSuccess =
      vosa::api::ppm::createHeader<T>(ofs, channels, width, height);
  if (!createSuccess)
    return false;
  for (uint32_t index = 0; index < width * height * channels; index++) {
    vosa::api::ppm::writeVal(ofs, data[index]);
  }
  return true;
}

} // namespace vosa::api::ppm

#endif // VOSA_PPM_UTILS_H
