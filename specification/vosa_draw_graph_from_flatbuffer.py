"""
License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import argparse
import os

import vosa


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("flatbuffer_file", help="Flatbuffer graph file")
    args = parser.parse_args()
    output_path = args.flatbuffer_file + ".gv"
    graph = vosa.graph.Graph.from_flatbuffer(args.flatbuffer_file)
    graph.dot(output_path)
    ops_list_file = args.flatbuffer_file + ".txt"
    ops_list = vosa.graph.GraphOpList.from_pipeline(args.flatbuffer_file, graph)
    print(ops_list)
    with open(ops_list_file, "w") as fp:
        fp.write(str(ops_list))


if __name__ == "__main__":
    main()
