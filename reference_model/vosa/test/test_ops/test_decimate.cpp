/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/decimate.h"
#include "test_bench.h"
#include "vosa/vosa.h"

using namespace std;
using namespace vosa;

template <typename T>
static EigenImage<T> decimate(EigenImage<T> &input_array,
                              const vector<uint32_t> &decimate_window = {1, 1},
                              const vector<uint32_t> &offsets = {0, 0}) {
  auto Op = Decimate<T>(decimate_window, offsets);

  auto input = make_shared<ArrayBase>(input_array);
  auto output = Op.eval({input});

  return output->template get_image<T>();
}

static void assertDecimate(const vector<uint32_t> &sizes,
                           const vector<uint32_t> &decimate_window) {
  assert(sizes.size() == 2);

  const uint32_t channels = 1, height = sizes[0], width = sizes[1];
  auto input = EigenImage<uint32_t>(height, width, channels);

  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      for (uint32_t c = 0; c < channels; c++)
        input(y, x, c) = x + y * height;

  uint32_t expected_height =
      height / decimate_window[0] + (height % decimate_window[0] == 0 ? 0 : 1);
  uint32_t expected_width =
      width / decimate_window[1] + (width % decimate_window[1] == 0 ? 0 : 1);
  auto expected =
      EigenImage<uint32_t>(expected_height, expected_width, channels);

  for (uint32_t y = 0; y < expected_height; y++)
    for (uint32_t x = 0; x < expected_width; x++)
      for (uint32_t c = 0; c < channels; c++)
        expected(y, x, c) =
            x * decimate_window[1] + y * height * decimate_window[0];

  auto actual = decimate(input, decimate_window);

  bool result =
      test::TestBench<uint32_t, int32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}

TEST(test_decimate, complex_decimates) {
  vector<uint32_t> sizes = {100, 111};
  vector<uint32_t> decimate_windows = {2, 3, 4};
  for (uint32_t height : sizes)
    for (uint32_t width : sizes)
      for (uint32_t decimate_height : decimate_windows)
        for (uint32_t decimate_width : decimate_windows)
          assertDecimate({height, width}, {decimate_height, decimate_width});
}

TEST(test_decimate, simple_square_image_downscale_2x) {
  EigenImage<uint32_t> input(5, 5, 1);
  input.setValues({{{1}, {2}, {3}, {4}, {5}},
                   {{6}, {7}, {8}, {9}, {10}},
                   {{11}, {12}, {13}, {14}, {15}},
                   {{16}, {17}, {18}, {19}, {20}},
                   {{21}, {22}, {23}, {24}, {25}}});

  EigenImage<uint32_t> expected(3, 3, 1);
  expected.setValues({{{1}, {3}, {5}}, {{11}, {13}, {15}}, {{21}, {23}, {25}}});

  auto actual = decimate(input, {2, 2});

  bool result =
      test::TestBench<uint32_t, int32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}

TEST(test_decimate, simple_image_downscale_rectangle) {
  EigenImage<uint32_t> input(4, 6, 1);
  input.setValues({{{1}, {2}, {3}, {4}, {5}, {6}},
                   {{7}, {8}, {9}, {10}, {11}, {12}},
                   {{13}, {14}, {15}, {16}, {17}, {18}},
                   {{19}, {20}, {21}, {22}, {23}, {24}}});

  EigenImage<uint32_t> expected(2, 2, 1);
  expected.setValues({{{1}, {4}}, {{13}, {16}}});

  auto actual = decimate(input, {2, 3});

  bool result =
      test::TestBench<uint32_t, int32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}

TEST(test_decimate, no_decimate) {
  EigenImage<uint32_t> input(4, 6, 1);
  input.setValues({{{1}, {2}, {3}, {4}, {5}, {6}},
                   {{7}, {8}, {9}, {10}, {11}, {12}},
                   {{13}, {14}, {15}, {16}, {17}, {18}},
                   {{19}, {20}, {21}, {22}, {23}, {24}}});

  EigenImage<uint32_t> expected = input;

  auto actual = decimate(input);
  bool result =
      test::TestBench<uint32_t, int32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}

TEST(test_decimate, simple_image_downscale_rectangle_with_offset) {
  EigenImage<uint32_t> input(4, 6, 1);
  input.setValues({{{1}, {2}, {3}, {4}, {5}, {6}},
                   {{7}, {8}, {9}, {10}, {11}, {12}},
                   {{13}, {14}, {15}, {16}, {17}, {18}},
                   {{19}, {20}, {21}, {22}, {23}, {24}}});

  EigenImage<uint32_t> expected(2, 2, 1);
  expected.setValues({{{8}, {11}}, {{20}, {23}}});

  auto actual = decimate(input, {2, 3}, {1, 1});

  bool result =
      test::TestBench<uint32_t, int32_t>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}