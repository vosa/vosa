/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <fstream>

#ifndef VOSA_FLATBUFFER_UTILS_H
#define VOSA_FLATBUFFER_UTILS_H

std::shared_ptr<char> load_flatbuffer(const std::string &flatbuffer_filename) {
  std::ifstream filestream(flatbuffer_filename, std::ios::binary);
  if (!filestream.is_open()) {
    throw std::runtime_error("Error opening " + flatbuffer_filename +
                             " for reading");
  }
  filestream.seekg(0, std::ios::end);
  std::streamoff length = filestream.tellg();
  filestream.seekg(0, std::ios::beg);
  std::shared_ptr<char> data(new char[length]);
  filestream.read(data.get(), length);
  filestream.close();
  return data;
}

#endif // VOSA_FLATBUFFER_UTILS_H
