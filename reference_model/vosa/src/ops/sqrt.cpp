/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "sqrt.h"

std::shared_ptr<vosa::Op> vosa::SqrtBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_sqrt = vosa_operator_union.AsSQRT();
  assert(serialized_sqrt);
  auto mode = serialized_sqrt->mode;

  switch (mode) {
  case vosa_serialization::SQRT_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::Sqrt<float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <>
vosa_serialization::SQRT_MODE
vosa::Sqrt<float>::get_serialization_mode() const {
  return vosa_serialization::SQRT_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Sqrt<T>::serialize(const std::string &op_name,
                         const std::vector<std::string> &input_names) const {
  vosa_serialization::SQRTT sqrt_;
  sqrt_.mode = get_serialization_mode();

  auto sqrt_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  sqrt_operator->op.Set(sqrt_);
  sqrt_operator->name = op_name;
  sqrt_operator->inputs = input_names;
  return sqrt_operator;
}

template <typename T>
vosa::ArrayBase::DType vosa::Sqrt<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::Sqrt<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {

  const EigenImage<T> &input_image = inputs[0]->template get_image<T>();

  uint32_t input_height = input_image.dimension(0);
  uint32_t input_width = input_image.dimension(1);
  uint32_t channels = input_image.dimension(2);

  auto output_image = EigenImage<T>(input_height, input_width, channels);

  for (uint32_t height_index = 0; height_index < input_height; height_index++) {
    for (uint32_t width_index = 0; width_index < input_width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < channels;
           channel_index++) {
        T input_value = input_image(height_index, width_index, channel_index);
        output_image(height_index, width_index, channel_index) =
            sqrtf(input_value);
      }
    }
  }
  return std::make_shared<vosa::ArrayBase>(output_image);
}

template class vosa::Sqrt<float>;