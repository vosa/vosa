add_library(vosa_api
        graph_builder.cpp
        ops.cpp
        )

target_include_directories(vosa_api
        PUBLIC
        $<INSTALL_INTERFACE:include>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../include>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../../tools/include>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../../dialect/include>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/../../dialect/include>)

target_link_libraries(vosa_api
        PUBLIC
        VosaBuilder
        VosaCompiler
        )

target_include_directories(vosa_api
        SYSTEM
        PUBLIC
        ${EIGEN_INCLUDE_DIR})

set(API_PUBLIC_HEADERS)
list(APPEND API_PUBLIC_HEADERS
        ${CMAKE_CURRENT_SOURCE_DIR}/../include/vosa/api/array_types.h
        ${CMAKE_CURRENT_SOURCE_DIR}/../include/vosa/api/graph_builder.h
        ${CMAKE_CURRENT_SOURCE_DIR}/../include/vosa/api/op.h
        ${CMAKE_CURRENT_SOURCE_DIR}/../include/vosa/api/ops.h
        ${CMAKE_CURRENT_SOURCE_DIR}/../include/vosa/api/sample.h
        )

set_target_properties(vosa_api PROPERTIES PUBLIC_HEADER "${API_PUBLIC_HEADERS}")

install(TARGETS vosa_api
        PUBLIC_HEADER DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/vosa/api")
