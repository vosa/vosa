/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_CODE_NON_LINEAR_H
#define VOSA_CODE_NON_LINEAR_H

#include "vosa/op.h"

namespace vosa {

template <typename T>
using non_linear_function_pointer = T (*)(const std::vector<T> &vector);

template <typename T>
T erode(const std::vector<T> &sorted_vector);

template <typename T>
T median(const std::vector<T> &sorted_vector);

template <typename T>
T dilate(const std::vector<T> &sorted_vector);

template <typename T>
EigenImage<T> non_linear_filter(const EigenImage<T> &input,
                                const EigenPlane<T> &filter,
                                non_linear_function_pointer<T>);

class NonLinearBase : public Op {
public:
};

template <typename T>
class NonLinearTemplatedBase : public NonLinearBase {
public:
  explicit NonLinearTemplatedBase(const EigenPlane<T> &filter)
      : filter(filter){};

  [[nodiscard]] std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &) const final;

  virtual EigenImage<T> runme(const EigenImage<T> &input) const = 0;

  [[nodiscard]] vosa::ArrayBase::DType output_dtype() const final;

protected:
  EigenPlane<T> filter;
};

template <typename T>
class Dilate : public NonLinearTemplatedBase<T> {
public:
  explicit Dilate(const EigenPlane<T> &filter)
      : NonLinearTemplatedBase<T>(filter){};

  EigenImage<T> runme(const EigenImage<T> &input) const;

  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

private:
  [[nodiscard]] vosa_serialization::DILATE_MODE get_serialization_mode() const;
};

template <typename T>
class Erode : public NonLinearTemplatedBase<T> {
public:
  explicit Erode(const EigenPlane<T> &filter)
      : NonLinearTemplatedBase<T>(filter){};

  EigenImage<T> runme(const EigenImage<T> &input) const;

  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

private:
  [[nodiscard]] vosa_serialization::ERODE_MODE get_serialization_mode() const;
};

template <typename T>
class MedianFilter : public NonLinearTemplatedBase<T> {
public:
  explicit MedianFilter(const EigenPlane<T> &filter)
      : NonLinearTemplatedBase<T>(filter){};

  EigenImage<T> runme(const EigenImage<T> &input) const;

  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

private:
  [[nodiscard]] vosa_serialization::MEDIAN_FILTER_MODE
  get_serialization_mode() const;
};

} // namespace vosa

#endif // VOSA_CODE_NON_LINEAR_H
