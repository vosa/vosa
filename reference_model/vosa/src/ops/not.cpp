/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "not.h"

std::shared_ptr<vosa::Op> vosa::NotBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_not = vosa_operator_union.AsNOT();
  assert(serialized_not);
  auto mode = serialized_not->mode;
  switch (mode) {
  case vosa_serialization::NOT_MODE_I_I_BOOL_BOOL: {
    return std::make_shared<vosa::Not<bool>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::ArrayBase::DType vosa::Not<T>::output_dtype() const {
  return vosa::ArrayBase::BOOL;
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::Not<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();

  uint32_t input_height = input.dimension(0);
  uint32_t input_width = input.dimension(1);
  uint32_t input_channels = input.dimension(2);

  auto output_image =
      EigenImage<bool>(input_height, input_width, input_channels);

  for (uint32_t height_index = 0; height_index < input_height; height_index++) {
    for (uint32_t width_index = 0; width_index < input_width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < input_channels;
           channel_index++) {

        output_image(height_index, width_index, channel_index) =
            not input(height_index, width_index, channel_index);
      }
    }
  }
  return std::make_shared<vosa::ArrayBase>(output_image);
}

template <>
vosa_serialization::NOT_MODE vosa::Not<bool>::get_serialization_mode() const {
  return vosa_serialization::NOT_MODE_I_I_BOOL_BOOL;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Not<T>::serialize(const std::string &op_name,
                        const std::vector<std::string> &input_names) const {

  vosa_serialization::NOTT not_;
  not_.mode = get_serialization_mode();

  auto not_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  not_operator->op.Set(std::move(not_));
  not_operator->name = op_name;
  not_operator->inputs = input_names;
  return not_operator;
}

template class vosa::Not<bool>;
