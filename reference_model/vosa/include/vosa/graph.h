/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_CODE_GRAPH_H
#define VOSA_CODE_GRAPH_H

#include <unordered_map>
#include <unsupported/Eigen/CXX11/Tensor>

#include "op.h"
#include "vosa_generated.h"

namespace vosa {

class Input : public Op {
public:
  explicit Input(ArrayBase::DType dtype) : dtype(dtype) {}

  [[nodiscard]] std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &) const final;

  [[nodiscard]] vosa::ArrayBase::DType output_dtype() const final;

  void set(const ArrayBase &);

  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

  static std::shared_ptr<Op>
  parse_serialized(const vosa_serialization::VosaOperatorUnion &);

private:
  ArrayBase::DType dtype;
  std::shared_ptr<vosa::ArrayBase> data;
};

class GraphCreationError : public std::runtime_error {
public:
  explicit GraphCreationError(const std::string &what = "")
      : std::runtime_error(what) {}
};

class Graph {
public:
  Graph() = default;

  explicit Graph(uint8_t *flatbuffer_data);

  explicit Graph(const vosa_serialization::VosaGraphT &serialized_graph) {
    this->init(serialized_graph);
  }

  const std::unordered_map<std::string, std::shared_ptr<vosa::Op>> &
  get_ops() const;

  std::shared_ptr<vosa::Op>
  add_op(const std::shared_ptr<vosa::Op> &,
         const std::vector<std::shared_ptr<vosa::Op>> &);

  std::shared_ptr<vosa::Op> add_op(const std::shared_ptr<vosa::Op> &,
                                   const std::vector<std::string> &);

  void add_default_output(const std::string &default_output);
  void add_default_input(const std::string &default_input);

  std::vector<std::string> get_default_outputs() const;
  std::vector<std::string> get_default_inputs() const;

  std::vector<std::shared_ptr<ArrayBase>>
  execute(const std::unordered_map<std::string, ArrayBase> &,
          const std::vector<std::string> &);

  std::vector<std::shared_ptr<ArrayBase>>
  execute(const std::unordered_map<std::shared_ptr<vosa::Input>, ArrayBase> &,
          const std::vector<std::shared_ptr<vosa::Op>> &);

  vosa_serialization::VosaGraphT serialize() const;

  std::string get_name_for_op(const std::shared_ptr<vosa::Op> &) const;

  void write_flatbuffer(const std::string &filename) const;

  void write_json(const std::string &filename) const;

  std::string dump() const;

  static vosa::Graph load_from_flatbuffer(const std::string &flatbuffer_path);

private:
  void init(const vosa_serialization::VosaGraphT &);

  std::vector<std::shared_ptr<vosa::ArrayBase>> complete_execution(
      std::unordered_map<std::string, std::shared_ptr<ArrayBase>> &,
      const std::vector<std::string> &);
  static std::string flatbuffer_to_json(uint8_t *buffer);

  std::unordered_map<std::string, std::shared_ptr<vosa::Op>> ops;
  std::unordered_map<std::string, std::vector<std::string>> inputs;

  std::vector<std::string> default_inputs;
  std::vector<std::string> default_outputs;
  uint32_t op_count = 0;
};

} // namespace vosa

#endif // VOSA_CODE_GRAPH_H
