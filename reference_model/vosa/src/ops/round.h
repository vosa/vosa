/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_CODE_ROUND_H
#define VOSA_CODE_ROUND_H

#include "vosa/op.h"

namespace vosa {

class RoundBase : public Op {
public:
  enum round_method_t {
    FLOOR,
    CEIL,
    TOWARDS_ZERO,
    AWAY_FROM_ZERO,
    HALF_UP,
    HALF_DOWN,
    HALF_TOWARDS_ZERO,
    HALF_AWAY_FROM_ZERO, // default C++ round() function
    HALF_TO_EVEN,
    HALF_TO_ODD,
  };

  static std::shared_ptr<Op> parse_serialized(
      const vosa_serialization::VosaOperatorUnion &vosa_operator_union);
};

template <typename T>
class Round : public RoundBase {
public:
  explicit Round(const round_method_t &round_method)
      : round_method(round_method) {}

  EigenImage<int32_t> runme(const EigenImage<T> &input) const;
  [[nodiscard]] std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &) const final;
  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;
  [[nodiscard]] vosa::ArrayBase::DType output_dtype() const final;

private:
  round_method_t round_method = round_method_t::HALF_AWAY_FROM_ZERO;

  [[nodiscard]] vosa_serialization::ROUND_MODE get_serialization_mode() const;
};

} // namespace vosa

#endif // VOSA_CODE_ROUND_H
