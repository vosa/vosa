// run command without FileCheck to see output (or add "--dump-input always" FileCheck option)
// RUN: vosa-opt -vosa-data-format=chw -vosa-to-tosa %s | %test_src_root/e2e-runner | FileCheck %s

func.func @main() {
    // generate using numpy:
    //   np.reshape(np.arange(0,300),(3, 10,10))
    %input = vosa.constant dense<[
       [[  0,   1,   2,   3,   4,   5,   6,   7,   8,   9],
        [ 10,  11,  12,  13,  14,  15,  16,  17,  18,  19],
        [ 20,  21,  22,  23,  24,  25,  26,  27,  28,  29],
        [ 30,  31,  32,  33,  34,  35,  36,  37,  38,  39],
        [ 40,  41,  42,  43,  44,  45,  46,  47,  48,  49],
        [ 50,  51,  52,  53,  54,  55,  56,  57,  58,  59],
        [ 60,  61,  62,  63,  64,  65,  66,  67,  68,  69],
        [ 70,  71,  72,  73,  74,  75,  76,  77,  78,  79],
        [ 80,  81,  82,  83,  84,  85,  86,  87,  88,  89],
        [ 90,  91,  92,  93,  94,  95,  96,  97,  98,  99]],

       [[100, 101, 102, 103, 104, 105, 106, 107, 108, 109],
        [110, 111, 112, 113, 114, 115, 116, 117, 118, 119],
        [120, 121, 122, 123, 124, 125, 126, 127, 128, 129],
        [130, 131, 132, 133, 134, 135, 136, 137, 138, 139],
        [140, 141, 142, 143, 144, 145, 146, 147, 148, 149],
        [150, 151, 152, 153, 154, 155, 156, 157, 158, 159],
        [160, 161, 162, 163, 164, 165, 166, 167, 168, 169],
        [170, 171, 172, 173, 174, 175, 176, 177, 178, 179],
        [180, 181, 182, 183, 184, 185, 186, 187, 188, 189],
        [190, 191, 192, 193, 194, 195, 196, 197, 198, 199]],

       [[200, 201, 202, 203, 204, 205, 206, 207, 208, 209],
        [210, 211, 212, 213, 214, 215, 216, 217, 218, 219],
        [220, 221, 222, 223, 224, 225, 226, 227, 228, 229],
        [230, 231, 232, 233, 234, 235, 236, 237, 238, 239],
        [240, 241, 242, 243, 244, 245, 246, 247, 248, 249],
        [250, 251, 252, 253, 254, 255, 256, 257, 258, 259],
        [260, 261, 262, 263, 264, 265, 266, 267, 268, 269],
        [270, 271, 272, 273, 274, 275, 276, 277, 278, 279],
        [280, 281, 282, 283, 284, 285, 286, 287, 288, 289],
        [290, 291, 292, 293, 294, 295, 296, 297, 298, 299]]
        ]> : tensor<3x10x10xsi32>

    // CHECK: sizes = [2, 5]
    // CHECK: [0, 2, 4, 6, 8]
    // CHECK: [50, 52, 54, 56, 58]
    // CHECK: sizes = [2, 5]
    // CHECK: [100, 102, 104, 106, 108]
    // CHECK: [150, 152, 154, 156, 158]
    // CHECK: sizes = [2, 5]
    // CHECK: [200, 202, 204, 206, 208]
    // CHECK: [250, 252, 254, 256, 258]
    %res = vosa.decimate %input  { N = [5 : i32, 2 : i32], offsets = [0 : i32, 0 : i32] } : tensor<3x10x10xsi32> -> tensor<3x2x5xsi32>
    %i32_res = builtin.unrealized_conversion_cast %res : tensor<3x2x5xsi32> to tensor<3x2x5xi32>
    %dyn_res = tensor.cast %i32_res : tensor<3x2x5xi32> to tensor<?x?x?xi32>
    func.call @print_img_i32(%dyn_res) : (tensor<?x?x?xi32>) -> ()

    // CHECK: sizes = [2, 5]
    // CHECK: [21, 23, 25, 27, 29]
    // CHECK: [71, 73, 75, 77, 79]
    // CHECK: sizes = [2, 5]
    // CHECK: [121, 123, 125, 127, 129]
    // CHECK: [171, 173, 175, 177, 179]
    // CHECK: sizes = [2, 5]
    // CHECK: [221, 223, 225, 227, 229]
    // CHECK: [271, 273, 275, 277, 279]
    %res1 = vosa.decimate %input  { N = [5 : i32, 2 : i32], offsets = [2 : i32, 1 : i32] } : tensor<3x10x10xsi32> -> tensor<3x2x5xsi32>
    %i32_res1 = builtin.unrealized_conversion_cast %res1 : tensor<3x2x5xsi32> to tensor<3x2x5xi32>
    %dyn_res1 = tensor.cast %i32_res1 : tensor<3x2x5xi32> to tensor<?x?x?xi32>
    func.call @print_img_i32(%dyn_res1) : (tensor<?x?x?xi32>) -> ()

    return
}

func.func @print_img_i32(%a : tensor<?x?x?xi32>) {
  // %unranked_all = tensor.cast %a : tensor<?x?x?xi32> to tensor<*xi32>
  // func.call @printMemrefI32(%unranked_all) : (tensor<*xi32>) -> ()
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c1 : tensor<?x?x?xi32>
  %w = tensor.dim %a, %c2 : tensor<?x?x?xi32>
  %nch = tensor.dim %a, %c0 : tensor<?x?x?xi32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[%c, 0, 0][1, %h, %w][1, 1, 1]  : tensor<?x?x?xi32> to tensor<1x?x?xi32>
     %chdata_c = tensor.collapse_shape %chdata [[0, 1], [2]] : tensor<1x?x?xi32> into tensor<?x?xi32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xi32> to tensor<*xi32>
     func.call @printMemrefI32(%unranked_chdata) : (tensor<*xi32>) -> ()
  }
  return
}

func.func private @printMemrefI32(%ptr : tensor<*xi32> {bufferization.access = "read"})
