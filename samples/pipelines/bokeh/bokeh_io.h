/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_BOKEH_IO_H
#define VOSA_BOKEH_IO_H

#include "ppm_utils.h"

bool write_outputs(const std::string &depth_map_output_file,
                   const std::string &u_channel_output_file,
                   const std::string &v_channel_output_file,
                   const std::string &y_channel_output_file,
                   const std::vector<uint8_t> &depth_map_output,
                   const std::vector<uint8_t> &uv_channels_output,
                   const std::vector<uint8_t> &y_channel_output) {

  auto depth_map_success = vosa::api::ppm::write_output(
      depth_map_output_file, depth_map_output, 960, 540);
  if (!depth_map_success)
    return false;
  std::vector<uint8_t> u_channel_output(960 * 540);
  std::vector<uint8_t> v_channel_output(960 * 540);
  for (uint32_t itr = 0; itr < 960 * 540; itr++) {
    u_channel_output[itr] = uv_channels_output[2 * itr];
    v_channel_output[itr] = uv_channels_output[2 * itr + 1];
  }
  auto u_channel_success = vosa::api::ppm::write_output(
      u_channel_output_file, u_channel_output, 960, 540);
  if (!u_channel_success)
    return false;
  auto v_channel_success = vosa::api::ppm::write_output(
      v_channel_output_file, v_channel_output, 960, 540);
  if (!v_channel_success)
    return false;
  auto y_channel_success = vosa::api::ppm::write_output(
      y_channel_output_file, y_channel_output, 1920, 1080);
  if (!y_channel_success)
    return false;
  return true;
}

bool read_inputs(const std::string &binary_mask_input_file,
                 const std::string &depth_map_input_file,
                 const std::string &u_channel_input_file,
                 const std::string &v_channel_input_file,
                 const std::string &y_channel_input_file,
                 std::vector<uint8_t> &binary_mask_input,
                 std::vector<uint8_t> &depth_map_input,
                 std::vector<uint8_t> &uv_channels_input,
                 std::vector<uint8_t> &y_channel_input) {

  int binary_mask_width, binary_mask_height;
  auto binary_mask_success = vosa::api::ppm::read_input<uint8_t>(
      binary_mask_input_file, binary_mask_input, binary_mask_width,
      binary_mask_height);
  if (!binary_mask_success)
    return false;

  int depth_map_width, depth_map_height;
  auto depth_map_success = vosa::api::ppm::read_input<uint8_t>(
      depth_map_input_file, depth_map_input, depth_map_width, depth_map_height);
  if (!depth_map_success)
    return false;

  int u_channel_width, u_channel_height;
  std::vector<uint8_t> u_channel_input;
  auto u_channel_success = vosa::api::ppm::read_input<uint8_t>(
      u_channel_input_file, u_channel_input, u_channel_width, u_channel_height);
  if (!u_channel_success)
    return false;

  int v_channel_width, v_channel_height;
  std::vector<uint8_t> v_channel_input;
  auto v_channel_success = vosa::api::ppm::read_input<uint8_t>(
      v_channel_input_file, v_channel_input, v_channel_width, v_channel_height);
  if (!v_channel_success)
    return false;

  int y_channel_width, y_channel_height;
  auto y_channel_success = vosa::api::ppm::read_input<uint8_t>(
      y_channel_input_file, y_channel_input, y_channel_width, y_channel_height);
  if (!y_channel_success)
    return false;

  auto uv_channels_size =
      u_channel_width * u_channel_height + v_channel_width * v_channel_height;
  uv_channels_input.clear();
  uv_channels_input.resize(uv_channels_size);
  std::memcpy(uv_channels_input.data(), u_channel_input.data(),
              u_channel_width * u_channel_height);
  std::memcpy(uv_channels_input.data() + u_channel_width * u_channel_height,
              v_channel_input.data(), v_channel_width * v_channel_height);
  for (uint32_t itr = 0; itr < 960 * 540; itr++) {
    uv_channels_input[2 * itr] = u_channel_input[itr];
    uv_channels_input[2 * itr + 1] = v_channel_input[itr];
  }
  return true;
}

#endif // VOSA_BOKEH_IO_H
