/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <cstdlib>
#include <sstream>
#include <string>
#include <vector>

#include "ppm_utils.h"

template <typename T>
bool vosa::api::ppm::parseHeader(std::ifstream &ifs, int n_channels, int &width,
                                 int &height) {
  if (ifs.fail()) {
    std::cerr << "*** Error: cannot open file" << std::endl;
    return false;
  }

  auto readLineWithCommentSkip = [&ifs]() {
    std::string line;
    while (!ifs.fail()) {
      std::getline(ifs, line);
      if (line[0] != '#')
        break;
    }
    return line;
  };

  std::vector<std::string> tokens;
  while (tokens.size() < 4 && !ifs.fail()) {
    std::istringstream iss(readLineWithCommentSkip());
    for (std::string s; iss >> s;)
      tokens.emplace_back(s);
  }
  std::string magic = tokens[0];
  int w = atoi(
      tokens[1]
          .c_str()); // NOTE: stoi/stoull not found in releaseLLVM toolchain
  int h = atoi(tokens[2].c_str());
  size_t maxval = atol(tokens[3].c_str());

  if (!((n_channels == 1 && magic == "P5") ||
        (n_channels == 3 && magic == "P6"))) {
    std::cerr << "*** Error: mismatched n_channels " << n_channels << " for "
              << magic << std::endl;
    return false;
  }
  size_t bSize = (maxval == 255) ? 1 : (maxval == 65535) ? 2 : 4;
  if (bSize != sizeof(T)) {
    std::cerr << "*** Error: mismatched byte size: " << bSize
              << " != " << sizeof(T) << std::endl;
    return false;
  }
  width = w;
  height = h;
  return true;
}

template <typename T>
bool vosa::api::ppm::createHeader(std::ofstream &ofs, int n_channels, int width,
                                  int height) {
  if (ofs.fail()) {
    std::cerr << "*** Error: cannot open file" << std::endl;
    return false;
  }
  std::string magic = (n_channels == 1) ? "P5" : "P6";
  size_t maxval = (sizeof(T) == 1) ? 255 : 65535;
  ofs << magic << std::endl;
  ofs << width << " " << height << std::endl;
  ofs << maxval << std::endl;
  return true;
}

template uint8_t vosa::api::ppm::readVal<uint8_t>(std::ifstream &ifs);
template void vosa::api::ppm::writeVal<uint8_t>(std::ofstream &ofs,
                                                uint8_t val);
template bool vosa::api::ppm::parseHeader<uint8_t>(std::ifstream &ifs,
                                                   int n_channels, int &width,
                                                   int &height);
template bool vosa::api::ppm::createHeader<uint8_t>(std::ofstream &ofs,
                                                    int n_channels, int width,
                                                    int height);
