// RUN: vosa-opt -vosa-data-format=chw -split-input-file -verify-diagnostics %s | FileCheck %s

// CHECK-LABEL: func.func @decimate()
module {
    func.func @decimate() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.decimate %{{.*}} : tensor<3x100x200xsi8> -> tensor<3x50x50xsi8>
        %res0 = vosa.decimate %t_si8 { N = [ 2 : i32, 4 : i32 ], offsets = [ 0 : i32, 0 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x50x50xsi8>
    
        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.decimate %{{.*}} : tensor<3x100x200xsi32> -> tensor<3x25x100xsi32>
        %res1 = vosa.decimate %t_si32 { N = [ 4 : i32, 2 : i32 ], offsets = [ 1 : i32, 1 : i32 ] } : tensor<3x100x200xsi32> -> tensor<3x25x100xsi32>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.decimate %{{.*}} : tensor<3x100x200xf32> -> tensor<3x34x67xf32>
        %res2 = vosa.decimate %t_f32 { N = [ 3 : i32, 3 : i32 ], offsets = [ 2 : i32, 2 : i32 ] } : tensor<3x100x200xf32> -> tensor<3x34x67xf32>
    
        return
    }
}

// -----

module {
    func.func @decimate_bad_out_ty() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.decimate' op invalid output type}}
        %res0 = vosa.decimate %t_si8 { N = [ 2 : i32, 4 : i32 ], offsets = [ 0 : i32, 0 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x50x50xsi32>
    
        return
    }
}

// -----

module {
    func.func @decimate_bad_out_sz() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.decimate' op invalid output shape}}
        %res0 = vosa.decimate %t_si8 { N = [ 2 : i32, 4 : i32 ], offsets = [ 0 : i32, 0 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x50x51xsi8>
    
        return
    }
}

// -----

module {
    func.func @decimate_bad_offset() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.decimate' op invalid offsets}}
        %res0 = vosa.decimate %t_si8 { N = [ 2 : i32, 4 : i32 ], offsets = [ 2 : i32, 0 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x50x50xsi8>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @pad_constant()
module {
    func.func @pad_constant() {
        // constant pad for si32, f16
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.pad %{{.*}} : tensor<3x100x200xsi8> -> tensor<3x103x212xsi8>
        %res0 = vosa.pad %t_si8 constant { pad_constant = 0 : si8, pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x103x212xsi8>
    
        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.pad %{{.*}} : tensor<3x100x200xsi32> -> tensor<3x103x212xsi32>
        %res1 = vosa.pad %t_si32 constant { pad_constant = 0 : si32, pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xsi32> -> tensor<3x103x212xsi32>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.pad %{{.*}} : tensor<3x100x200xf32> -> tensor<3x103x212xf32>
        %res2 = vosa.pad %t_f32 constant { pad_constant = 0.0 : f32, pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xf32> -> tensor<3x103x212xf32>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @pad_reflect()
module {
    func.func @pad_reflect() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.pad %{{.*}} {{.*}} : tensor<3x100x200xsi8> -> tensor<3x103x212xsi8>
        %res6 = vosa.pad %t_si8 reflect { pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x103x212xsi8>
    
        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.pad %{{.*}} {{.*}} : tensor<3x100x200xsi32> -> tensor<3x103x212xsi32>
        %res7 = vosa.pad %t_si32 reflect { pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xsi32> -> tensor<3x103x212xsi32>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.pad %{{.*}} {{.*}} : tensor<3x100x200xf32> -> tensor<3x103x212xf32>
        %res8 = vosa.pad %t_f32 reflect { pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xf32> -> tensor<3x103x212xf32>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @pad_replicate()
module {
    func.func @pad_replicate() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.pad %{{.*}} {{.*}} : tensor<3x100x200xsi8> -> tensor<3x103x212xsi8>
        %res6 = vosa.pad %t_si8 replicate { pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x103x212xsi8>
    
        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.pad %{{.*}} {{.*}} : tensor<3x100x200xsi32> -> tensor<3x103x212xsi32>
        %res7 = vosa.pad %t_si32 replicate { pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xsi32> -> tensor<3x103x212xsi32>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.pad %{{.*}} {{.*}} : tensor<3x100x200xf32> -> tensor<3x103x212xf32>
        %res8 = vosa.pad %t_f32 replicate { pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xf32> -> tensor<3x103x212xf32>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @pad_mirror()
module {
    func.func @pad_mirror() {
        // other pad modes - no constant required
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.pad %{{.*}} {{.*}} : tensor<3x100x200xsi8> -> tensor<3x103x212xsi8>
        %res3 = vosa.pad %t_si8 mirror { pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x103x212xsi8>
    
        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.pad %{{.*}} {{.*}} : tensor<3x100x200xsi32> -> tensor<3x103x212xsi32>
        %res4 = vosa.pad %t_si32 mirror { pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xsi32> -> tensor<3x103x212xsi32>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.pad %{{.*}} {{.*}} : tensor<3x100x200xf32> -> tensor<3x103x212xf32>
        %res5 = vosa.pad %t_f32 mirror { pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xf32> -> tensor<3x103x212xf32>
    
        return
    }
}

// -----

// CHECK-LABEL: func.func @pad_any(%{{.*}})
module {
    func.func @pad_any(%t : tensor<?x?x?xsi8>) {
        %p_si8 = vosa.constant 0 : si8
        // CHECK: %{{.*}} = vosa.pad %{{.*}} : tensor<?x?x?xsi8> -> tensor<?x?x?xsi8>
        %res0 = vosa.pad %t constant { pad_constant = 0 : si8, pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<?x?x?xsi8> -> tensor<?x?x?xsi8>
    
        return
    }
}

// -----

// constant value not present
module {
    func.func @pad_no_constval() {
        // constant pad for si32, f16
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.pad' op constant pad attribute not specified}}
        %res0 = vosa.pad %t_si8 constant { pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x103x212xsi8>
    
        return
    }
}

// -----

// padding type different
module {
    func.func @pad_bad_pad_type() {
        // constant pad for si32, f16
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.pad' op pad type does not match input}}
        %res0 = vosa.pad %t_si8 constant { pad_constant = 0 : si32, pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x103x212xsi8>
    
        return
    }
}

// -----

// padding type different
module {
    func.func @pad_bad_output_shape() {
        // constant pad for si32, f16
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.pad' op invalid output shape [3, 113, 212], expected [3, 103, 212]}}
        %res0 = vosa.pad %t_si8 constant { pad_constant = 0 : si8, pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x113x212xsi8>
    
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @pointwise_matrix_multiply()
    func.func @pointwise_matrix_multiply() {
        %t0_ui8 = vosa.constant dense<42> : tensor<3x100x200xui8>
        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<3xui8>, K_2 = dense<{{.*}}> : tensor<3xui8>, M = dense<{{.*}}> : tensor<3x3xui8>} : tensor<3x100x200xui8> -> tensor<3x100x200xui8>
        %res_ui8 = vosa.pointwise_matrix_multiply %t0_ui8 {M=dense<[[1, 0, 0], [0, 1, 0], [0, 0, 1]]> : tensor<3x3xui8>, K_1=dense<[0, 0, 0]> : tensor<3xui8>, K_2=dense<[0, 0, 0]> : tensor<3xui8>} : tensor<3x100x200xui8> -> tensor<3x100x200xui8>

        %t0_ui16 = vosa.constant dense<42> : tensor<3x100x200xui16>
        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<3xui16>, K_2 = dense<{{.*}}> : tensor<3xui16>, M = dense<{{.*}}> : tensor<3x3xui16>} : tensor<3x100x200xui16> -> tensor<3x100x200xui16>
        %res_ui16 = vosa.pointwise_matrix_multiply %t0_ui16 {M=dense<[[1, 0, 0], [0, 1, 0], [0, 0, 1]]> : tensor<3x3xui16>, K_1=dense<[0, 0, 0]> : tensor<3xui16>, K_2=dense<[0, 0, 0]> : tensor<3xui16>} : tensor<3x100x200xui16> -> tensor<3x100x200xui16>

        %t0_ui32 = vosa.constant dense<42> : tensor<3x100x200xui32>
        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<3xui32>, K_2 = dense<{{.*}}> : tensor<3xui32>, M = dense<{{.*}}> : tensor<3x3xui32>} : tensor<3x100x200xui32> -> tensor<3x100x200xui32>
        %res_ui32 = vosa.pointwise_matrix_multiply %t0_ui32 {M=dense<[[1, 0, 0], [0, 1, 0], [0, 0, 1]]> : tensor<3x3xui32>, K_1=dense<[0, 0, 0]> : tensor<3xui32>, K_2=dense<[0, 0, 0]> : tensor<3xui32>} : tensor<3x100x200xui32> -> tensor<3x100x200xui32>

        %t0_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<3xsi8>, K_2 = dense<{{.*}}> : tensor<3xsi8>, M = dense<{{.*}}> : tensor<3x3xsi8>} : tensor<3x100x200xsi8> -> tensor<3x100x200xsi8>
        %res_si8 = vosa.pointwise_matrix_multiply %t0_si8 {M=dense<[[1, 0, 0], [0, 1, 0], [0, 0, 1]]> : tensor<3x3xsi8>, K_1=dense<[0, 0, 0]> : tensor<3xsi8>, K_2=dense<[0, 0, 0]> : tensor<3xsi8>} : tensor<3x100x200xsi8> -> tensor<3x100x200xsi8>

        %t0_si16 = vosa.constant dense<42> : tensor<3x100x200xsi16>
        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<3xsi16>, K_2 = dense<{{.*}}> : tensor<3xsi16>, M = dense<{{.*}}> : tensor<3x3xsi16>} : tensor<3x100x200xsi16> -> tensor<3x100x200xsi16>
        %res_si16 = vosa.pointwise_matrix_multiply %t0_si16 {M=dense<[[1, 0, 0], [0, 1, 0], [0, 0, 1]]> : tensor<3x3xsi16>, K_1=dense<[0, 0, 0]> : tensor<3xsi16>, K_2=dense<[0, 0, 0]> : tensor<3xsi16>} : tensor<3x100x200xsi16> -> tensor<3x100x200xsi16>

        %t0_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<3xsi32>, K_2 = dense<{{.*}}> : tensor<3xsi32>, M = dense<{{.*}}> : tensor<3x3xsi32>} : tensor<3x100x200xsi32> -> tensor<3x100x200xsi32>
        %res_si32 = vosa.pointwise_matrix_multiply %t0_si32 {M=dense<[[1, 0, 0], [0, 1, 0], [0, 0, 1]]> : tensor<3x3xsi32>, K_1=dense<[0, 0, 0]> : tensor<3xsi32>, K_2=dense<[0, 0, 0]> : tensor<3xsi32>} : tensor<3x100x200xsi32> -> tensor<3x100x200xsi32>

        %t0_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<3xf16>, K_2 = dense<{{.*}}> : tensor<3xf16>, M = dense<{{.*}}> : tensor<3x3xf16>} : tensor<3x100x200xf16> -> tensor<3x100x200xf16>
        %res_f16 = vosa.pointwise_matrix_multiply %t0_f16 {M=dense<[[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]> : tensor<3x3xf16>, K_1=dense<[0.0, 0.0, 0.0]> : tensor<3xf16>, K_2=dense<[0.0, 0.0, 0.0]> : tensor<3xf16>} : tensor<3x100x200xf16> -> tensor<3x100x200xf16>

        %t0_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<3xf32>, K_2 = dense<{{.*}}> : tensor<3xf32>, M = dense<{{.*}}> : tensor<3x3xf32>} : tensor<3x100x200xf32> -> tensor<3x100x200xf32>
        %res_f32 = vosa.pointwise_matrix_multiply %t0_f32 {M=dense<[[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]> : tensor<3x3xf32>, K_1=dense<[0.0, 0.0, 0.0]> : tensor<3xf32>, K_2=dense<[0.0, 0.0, 0.0]> : tensor<3xf32>} : tensor<3x100x200xf32> -> tensor<3x100x200xf32>

        // Sizes other than 3x3
        %t2_f32 = vosa.constant dense<42.0> : tensor<2x100x200xf32>
        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<2xf32>, K_2 = dense<{{.*}}> : tensor<2xf32>, M = dense<{{.*}}> : tensor<2x2xf32>} : tensor<2x100x200xf32> -> tensor<2x100x200xf32>
        %res2_f32 = vosa.pointwise_matrix_multiply %t2_f32 {M=dense<[[1.0, 0.0], [0.0, 1.0]]> : tensor<2x2xf32>, K_1=dense<[0.0, 0.0]> : tensor<2xf32>, K_2=dense<[0.0, 0.0]> : tensor<2xf32>} : tensor<2x100x200xf32> -> tensor<2x100x200xf32>

        // different in / out channels
        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<2xf32>, K_2 = dense<{{.*}}> : tensor<3xf32>, M = dense<{{.*}}> : tensor<3x2xf32>} : tensor<2x100x200xf32> -> tensor<3x100x200xf32>
        %res3_f32 = vosa.pointwise_matrix_multiply %t2_f32 {M=dense<[[1.0, 0.0], [0.0, 1.0], [0.5, 0.5]]> : tensor<3x2xf32>, K_1=dense<[0.0, 0.0]> : tensor<2xf32>, K_2=dense<[0.0, 0.0, 0.0]> : tensor<3xf32>} : tensor<2x100x200xf32> -> tensor<3x100x200xf32>

        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<2xf32>, K_2 = dense<{{.*}}> : tensor<1xf32>, M = dense<{{.*}}> : tensor<1x2xf32>} : tensor<2x100x200xf32> -> tensor<1x100x200xf32>
        %res4_f32 = vosa.pointwise_matrix_multiply %t2_f32 {M=dense<[[1.0, 0.0]]> : tensor<1x2xf32>, K_1=dense<[0.0, 0.0]> : tensor<2xf32>, K_2=dense<[0.0]> : tensor<1xf32>} : tensor<2x100x200xf32> -> tensor<1x100x200xf32>

        return
    }
}

// -----

module {
    func.func @pointwise_matrix_multiply_bad_M() {
        %t2_f32 = vosa.constant dense<42.0> : tensor<2x100x200xf32>
        // expected-error @below {{'vosa.pointwise_matrix_multiply' op invalid shape for M}}
        %res2_f32 = vosa.pointwise_matrix_multiply %t2_f32 {M=dense<[[1.0, 0.0]]> : tensor<1x2xf32>, K_1=dense<[0.0, 0.0]> : tensor<2xf32>, K_2=dense<[0.0, 0.0]> : tensor<2xf32>} : tensor<2x100x200xf32> -> tensor<2x100x200xf32>
        return
    }
}

// -----

module {
    func.func @pointwise_matrix_multiply_bad_K_1() {
        %t2_f32 = vosa.constant dense<42.0> : tensor<2x100x200xf32>
        // expected-error @below {{'vosa.pointwise_matrix_multiply' op invalid shape for K_1}}
        %res2_f32 = vosa.pointwise_matrix_multiply %t2_f32 {M=dense<[[1.0, 0.0], [0.0, 1.0]]> : tensor<2x2xf32>, K_1=dense<[0.0]> : tensor<1xf32>, K_2=dense<[0.0, 0.0]> : tensor<2xf32>} : tensor<2x100x200xf32> -> tensor<2x100x200xf32>
        return
    }
}

// -----

module {
    func.func @pointwise_matrix_multiply_bad_K_2() {
        %t2_f32 = vosa.constant dense<42.0> : tensor<2x100x200xf32>
        // expected-error @below {{'vosa.pointwise_matrix_multiply' op invalid shape for K_1}}
        %res2_f32 = vosa.pointwise_matrix_multiply %t2_f32 {M=dense<[[1.0, 0.0], [0.0, 1.0], [0.0, 1.0]]> : tensor<3x2xf32>, K_1=dense<[0.0]> : tensor<1xf32>, K_2=dense<[0.0, 0.0]> : tensor<2xf32>} : tensor<2x100x200xf32> -> tensor<3x100x200xf32>
        return
    }
}

// -----

module {
    func.func @resize_nearest_neighbour() {
        %t_i1 = vosa.constant dense<true> : tensor<3x100x200xi1>
        // CHECK: %{{.*}} = vosa.resize_nearest_neighbour %{{.*}} {size = [150 : i32, 300 : i32]} : tensor<3x100x200xi1> -> tensor<3x150x300xi1>
        %res_i1 = vosa.resize_nearest_neighbour %t_i1 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xi1> -> tensor<3x150x300xi1>
    
        %t_ui8 = vosa.constant dense<42> : tensor<3x100x200xui8>
        // CHECK: %{{.*}} = vosa.resize_nearest_neighbour %{{.*}} {size = [150 : i32, 300 : i32]} : tensor<3x100x200xui8> -> tensor<3x150x300xui8>
        %res_ui8 = vosa.resize_nearest_neighbour %t_ui8 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xui8> -> tensor<3x150x300xui8>
    
        %t_ui16 = vosa.constant dense<42> : tensor<3x100x200xui16>
        // CHECK: %{{.*}} = vosa.resize_nearest_neighbour %{{.*}} {size = [150 : i32, 300 : i32]} : tensor<3x100x200xui16> -> tensor<3x150x300xui16>
        %res_ui16 = vosa.resize_nearest_neighbour %t_ui16 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xui16> -> tensor<3x150x300xui16>
    
        %t_ui32 = vosa.constant dense<42> : tensor<3x100x200xui32>
        // CHECK: %{{.*}} = vosa.resize_nearest_neighbour %{{.*}} {size = [150 : i32, 300 : i32]} : tensor<3x100x200xui32> -> tensor<3x150x300xui32>
        %res_ui32 = vosa.resize_nearest_neighbour %t_ui32 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xui32> -> tensor<3x150x300xui32>
    
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.resize_nearest_neighbour %{{.*}} {size = [150 : i32, 300 : i32]} : tensor<3x100x200xsi8> -> tensor<3x150x300xsi8>
        %res_si8 = vosa.resize_nearest_neighbour %t_si8 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x150x300xsi8>
    
        %t_si16 = vosa.constant dense<42> : tensor<3x100x200xsi16>
        // CHECK: %{{.*}} = vosa.resize_nearest_neighbour %{{.*}} {size = [150 : i32, 300 : i32]} : tensor<3x100x200xsi16> -> tensor<3x150x300xsi16>
        %res_si16 = vosa.resize_nearest_neighbour %t_si16 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xsi16> -> tensor<3x150x300xsi16>
    
        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.resize_nearest_neighbour %{{.*}} {size = [150 : i32, 300 : i32]} : tensor<3x100x200xsi32> -> tensor<3x150x300xsi32>
        %res_si32 = vosa.resize_nearest_neighbour %t_si32 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xsi32> -> tensor<3x150x300xsi32>
    
        %t_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // CHECK: %{{.*}} = vosa.resize_nearest_neighbour %{{.*}} {size = [150 : i32, 300 : i32]} : tensor<3x100x200xf16> -> tensor<3x150x300xf16>
        %res_f16 = vosa.resize_nearest_neighbour %t_f16 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xf16> -> tensor<3x150x300xf16>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.resize_nearest_neighbour %{{.*}} {size = [150 : i32, 300 : i32]} : tensor<3x100x200xf32> -> tensor<3x150x300xf32>
        %res_f32 = vosa.resize_nearest_neighbour %t_f32 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xf32> -> tensor<3x150x300xf32>
    
        return
    }
}

// -----

module {
    func.func @resize_nearest_neighbour_type_mismatch() {
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // expected-error @below {{'vosa.resize_nearest_neighbour' op output type does not match input}}
        %res = vosa.resize_nearest_neighbour %t_f32 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xf32> -> tensor<3x100x200xi1>
    
        return
    }
}

// -----

module {
    func.func @resize_nearest_neighbour_shape_mismatch() {
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // expected-error @below {{'vosa.resize_nearest_neighbour' op invalid output shape [3, 100, 200], expected [3, 150, 300]}}
        %res = vosa.resize_nearest_neighbour %t_f32 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xf32> -> tensor<3x100x200xf32>
    
        return
    }
}

// -----

module {
    func.func @resize_bilinear() {
        %t_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // CHECK: %{{.*}} = vosa.resize_bilinear %{{.*}} {size = [150 : i32, 300 : i32]} : tensor<3x100x200xf16> -> tensor<3x150x300xf16>
        %res0 = vosa.resize_bilinear %t_f16 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xf16> -> tensor<3x150x300xf16>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.resize_bilinear %{{.*}} {size = [150 : i32, 300 : i32]} : tensor<3x100x200xf32> -> tensor<3x150x300xf32>
        %res1 = vosa.resize_bilinear %t_f32 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xf32> -> tensor<3x150x300xf32>
    
        return
    }
}

// -----

module {
    func.func @resize_bilinear_bad_type_si8() {
        %t = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.resize_bilinear' op }}
        %res0 = vosa.resize_bilinear %t { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xsi8> -> tensor<3x150x300xsi8>
    
        return
    }
}

// -----

module {
    func.func @resize_bilinear_bad_type_si32() {
        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // expected-error @below {{'vosa.resize_bilinear' op }}
        %res1 = vosa.resize_bilinear %t_si32 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xsi32> -> tensor<3x150x300xsi32>
    
        return
    }
}

// -----

module {
    func.func @resize_bilinear_bad_output() {
        %t_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // expected-error @below {{'vosa.resize_bilinear' op output type does not match input}}
        %res0 = vosa.resize_bilinear %t_f16 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xf16> -> tensor<3x150x300xf32>
    
        return
    }
}

// -----

module {
    func.func @resize_bilinear_bad_output_shape() {
        %t_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // expected-error @below {{'vosa.resize_bilinear' op invalid output shape [3, 151, 300], expected [3, 150, 300]}}
        %res0 = vosa.resize_bilinear %t_f16 { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xf16> -> tensor<3x151x300xf16>
    
        return
    }
}

// -----
module {
    // CHECK-LABEL: func.func @expand()
    func.func @expand() {
        %expand_kernel = vosa.constant dense<[[[1, 0, 0, 0, 0], [0, 1, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 1, 0], [0, 0, 0, 0, 2]]]> : tensor<1x5x5xui8>
        %t_ui8 = vosa.constant dense<42> : tensor<3x100x200xui8>
        // CHECK: %{{.*}} = vosa.expand %{{.*}}, %{{.*}} {{.*}} : tensor<3x100x200xui8>, tensor<1x5x5xui8> -> tensor<3x125x250xui8>
        %res_ui8 = vosa.expand %t_ui8, %expand_kernel {gather_kernel = dense<[[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 2]]> : tensor<4x4xui8>, fill_value = 0 : ui8} : tensor<3x100x200xui8>, tensor<1x5x5xui8> -> tensor<3x125x250xui8>

        %t_ui16 = vosa.constant dense<42> : tensor<3x100x200xui16>
        // CHECK: %{{.*}} = vosa.expand %{{.*}}, %{{.*}} {{.*}} : tensor<3x100x200xui16>, tensor<1x5x5xui8> -> tensor<3x125x250xui16>
        %res_ui16 = vosa.expand %t_ui16, %expand_kernel {gather_kernel = dense<[[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 2]]> : tensor<4x4xui8>, fill_value = 0 : ui16} : tensor<3x100x200xui16>, tensor<1x5x5xui8> -> tensor<3x125x250xui16>

        %t_ui32 = vosa.constant dense<42> : tensor<3x100x200xui32>
        // CHECK: %{{.*}} = vosa.expand %{{.*}}, %{{.*}} {{.*}} : tensor<3x100x200xui32>, tensor<1x5x5xui8> -> tensor<3x125x250xui32>
        %res_ui32 = vosa.expand %t_ui32, %expand_kernel {gather_kernel = dense<[[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 2]]> : tensor<4x4xui8>, fill_value = 0 : ui32} : tensor<3x100x200xui32>, tensor<1x5x5xui8> -> tensor<3x125x250xui32>

        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.expand %{{.*}}, %{{.*}} {{.*}} : tensor<3x100x200xsi8>, tensor<1x5x5xui8> -> tensor<3x125x250xsi8>
        %res_si8 = vosa.expand %t_si8, %expand_kernel {gather_kernel = dense<[[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 2]]> : tensor<4x4xui8>, fill_value = 0 : si8} : tensor<3x100x200xsi8>, tensor<1x5x5xui8> -> tensor<3x125x250xsi8>

        %t_si16 = vosa.constant dense<42> : tensor<3x100x200xsi16>
        // CHECK: %{{.*}} = vosa.expand %{{.*}}, %{{.*}} {{.*}} : tensor<3x100x200xsi16>, tensor<1x5x5xui8> -> tensor<3x125x250xsi16>
        %res_si16 = vosa.expand %t_si16, %expand_kernel {gather_kernel = dense<[[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 2]]> : tensor<4x4xui8>, fill_value = 0 : si16} : tensor<3x100x200xsi16>, tensor<1x5x5xui8> -> tensor<3x125x250xsi16>

        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.expand %{{.*}}, %{{.*}} {{.*}} : tensor<3x100x200xsi32>, tensor<1x5x5xui8> -> tensor<3x125x250xsi32>
        %res_si32 = vosa.expand %t_si32, %expand_kernel {gather_kernel = dense<[[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 2]]> : tensor<4x4xui8>, fill_value = 0 : si32} : tensor<3x100x200xsi32>, tensor<1x5x5xui8> -> tensor<3x125x250xsi32>

        %t_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // CHECK: %{{.*}} = vosa.expand %{{.*}}, %{{.*}} {{.*}} : tensor<3x100x200xf16>, tensor<1x5x5xui8> -> tensor<3x125x250xf16>
        %res_f16 = vosa.expand %t_f16, %expand_kernel {gather_kernel = dense<[[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 2]]> : tensor<4x4xui8>, fill_value = 0.0 : f16} : tensor<3x100x200xf16>, tensor<1x5x5xui8> -> tensor<3x125x250xf16>

        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.expand %{{.*}}, %{{.*}} {{.*}} : tensor<3x100x200xf32>, tensor<1x5x5xui8> -> tensor<3x125x250xf32>
        %res_f32 = vosa.expand %t_f32, %expand_kernel {gather_kernel = dense<[[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 2]]> : tensor<4x4xui8>, fill_value = 0.0 : f32} : tensor<3x100x200xf32>, tensor<1x5x5xui8> -> tensor<3x125x250xf32>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @expand_shape()
    func.func @expand_shape() {
        %t0 = vosa.constant dense<42> : tensor<3x100x200xui8>
        %exp0 = vosa.constant dense<42> : tensor<1x4x2xui8>
        // CHECK: %{{.*}} = vosa.expand %{{.*}}, %{{.*}} {{.*}} : tensor<3x100x200xui8>, tensor<1x4x2xui8> -> tensor<3x200x200xui8>
        %res0 = vosa.expand %t0, %exp0 {gather_kernel = dense<[[1, 0], [0, 1]]> : tensor<2x2xui8>, fill_value = 0 : ui8} : tensor<3x100x200xui8>, tensor<1x4x2xui8> -> tensor<3x200x200xui8>

        // CHECK: %{{.*}} = vosa.expand %{{.*}}, %{{.*}} {{.*}} : tensor<3x100x200xui8>, tensor<1x4x2xui8> -> tensor<3x100x200xui8>
        %res1 = vosa.expand %t0, %exp0 {gather_kernel = dense<[[1, 0], [0, 1], [1, 0], [0, 1]]> : tensor<4x2xui8>, fill_value = 0 : ui8} : tensor<3x100x200xui8>, tensor<1x4x2xui8> -> tensor<3x100x200xui8>

        %t1 = vosa.constant dense<42> : tensor<3x2x2xui8>
        //CHECK: %{{.*}} = vosa.expand %{{.*}}, %{{.*}} {{.*}} : tensor<3x2x2xui8>, tensor<1x4x2xui8> -> tensor<3x4x2xui8>
        %res2 = vosa.expand %t1, %exp0 {gather_kernel = dense<[[1, 0], [0, 1]]> : tensor<2x2xui8>, fill_value = 0 : ui8} : tensor<3x2x2xui8>, tensor<1x4x2xui8> -> tensor<3x4x2xui8>
        return
    }
}

// -----

module {
    func.func @expand_gather_mismatch() {
        %t0 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        %expand_kernel = vosa.constant dense<42> : tensor<1x5x5xui8>
        // expected-error @below {{'vosa.expand' op gather kernel type must be 2D tensor of ui8}}
        %res = vosa.expand %t0, %expand_kernel {gather_kernel = dense<[0, 1, 2, 3]> : tensor<4xui8>, fill_value = 0.0 : f32} : tensor<3x100x200xf32>, tensor<1x5x5xui8> -> tensor<3x125x250xf32>

        return
    }
}

// -----

module {
    func.func @expand_gather_mismatch() {
        %t0 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        %expand_kernel = vosa.constant dense<42> : tensor<1x5x5xui8>
        // expected-error @below {{'vosa.expand' op gather kernel type must be 2D tensor of ui8}}
        %res = vosa.expand %t0, %expand_kernel {gather_kernel = dense<[[0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3]]> : tensor<4x4xui16>, fill_value = 0.0 : f32} : tensor<3x100x200xf32>, tensor<1x5x5xui8> -> tensor<3x125x250xf32>

        return
    }
}

// -----

module {
    func.func @expand_size_mismatch() {
        %t0 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        %expand_kernel = vosa.constant dense<42> : tensor<1x5x5xui8>
        // expected-error @below {{'vosa.expand' op invalid output shape [3, 251, 500], expected [3, 250, 500]}}
        %res = vosa.expand %t0, %expand_kernel {gather_kernel = dense<[[1, 0], [0, 1]]> : tensor<2x2xui8>, fill_value = 0.0 : f32} : tensor<3x100x200xf32>, tensor<1x5x5xui8> -> tensor<3x251x500xf32>

        return
    }
}

// -----

module {
    func.func @expand_type_mismatch() {
        %t0 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        %expand_kernel = vosa.constant dense<42> : tensor<1x5x5xui8>
        // expected-error @below {{'vosa.expand' op input and output element types do not match}}
        %res = vosa.expand %t0, %expand_kernel {gather_kernel = dense<[[0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3]]> : tensor<4x4xui8>, fill_value = 0.0 : f32} : tensor<3x100x200xf32>, tensor<1x5x5xui8> -> tensor<3x125x250xf16>

        return
    }
}

// -----

module {
    func.func @expand_fill_type_mismatch() {
        %t0 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        %expand_kernel = vosa.constant dense<42> : tensor<1x5x5xui8>
        // expected-error @below {{'vosa.expand' op fill value type must match input and output element types}}
        %res = vosa.expand %t0, %expand_kernel {gather_kernel = dense<[[0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3]]> : tensor<4x4xui8>, fill_value = 0.0 : f16} : tensor<3x100x200xf32>, tensor<1x5x5xui8> -> tensor<3x125x250xf32>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @rotate90()
    func.func @rotate90() {
	%t0_ui8 = vosa.constant dense<42> : tensor<3x100x200xui8>
	// CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {{.*}} : tensor<3x100x200xui8> -> tensor<3x100x200xui8>
	%res_ui8 = vosa.rotate_90 %t0_ui8 { rotate = -2 : si32 } : tensor<3x100x200xui8> -> tensor<3x100x200xui8>

	%t0_ui16 = vosa.constant dense<42> : tensor<3x100x200xui16>
	// CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {{.*}} : tensor<3x100x200xui16> -> tensor<3x100x200xui16>
	%res_ui16 = vosa.rotate_90 %t0_ui16 { rotate = -2 : si32 } : tensor<3x100x200xui16> -> tensor<3x100x200xui16>

	%t0_ui32 = vosa.constant dense<42> : tensor<3x100x200xui32>
	// CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {{.*}} : tensor<3x100x200xui32> -> tensor<3x100x200xui32>
	%res_ui32 = vosa.rotate_90 %t0_ui32 { rotate = -2 : si32 } : tensor<3x100x200xui32> -> tensor<3x100x200xui32>

	%t0_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
	// CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {{.*}} : tensor<3x100x200xsi8> -> tensor<3x100x200xsi8>
	%res_si8 = vosa.rotate_90 %t0_si8 { rotate = -2 : si32 } : tensor<3x100x200xsi8> -> tensor<3x100x200xsi8>

	%t0_si16 = vosa.constant dense<42> : tensor<3x100x200xsi16>
	// CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {{.*}} : tensor<3x100x200xsi16> -> tensor<3x100x200xsi16>
	%res_si16 = vosa.rotate_90 %t0_si16 { rotate = -2 : si32 } : tensor<3x100x200xsi16> -> tensor<3x100x200xsi16>

	%t0_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
	// CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {{.*}} : tensor<3x100x200xsi32> -> tensor<3x100x200xsi32>
	%res_si32 = vosa.rotate_90 %t0_si32 { rotate = -2 : si32 } : tensor<3x100x200xsi32> -> tensor<3x100x200xsi32>

	%t0_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
	// CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {{.*}} : tensor<3x100x200xf16> -> tensor<3x100x200xf16>
	%res_f16 = vosa.rotate_90 %t0_f16 { rotate = -2 : si32 } : tensor<3x100x200xf16> -> tensor<3x100x200xf16>

	%t0_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
	// CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {{.*}} : tensor<3x100x200xf32> -> tensor<3x100x200xf32>
	%res_f32 = vosa.rotate_90 %t0_f32 { rotate = -2 : si32 } : tensor<3x100x200xf32> -> tensor<3x100x200xf32>
	
	return
    }
}

// -----

module {
    func.func @rotate90_type_mismatch() {
	%t0_ui8 = vosa.constant dense<42> : tensor<3x100x200xui8>
	// expected-error @below {{'vosa.rotate_90' op input and output element types do not match}}
	%res_ui16 = vosa.rotate_90 %t0_ui8 { rotate = 0 : si32 } : tensor<3x100x200xui8> -> tensor<3x100x200xui16>

	return
    }
}

// -----

module {
    func.func @rotate90_shape_mismatch() {
	%t0_ui8 = vosa.constant dense<42> : tensor<3x100x200xui8>
	// expected-error @below {{'vosa.rotate_90' op invalid output shape [3, 100, 200], expected [3, 200, 100]}}
	%res_ui8 = vosa.rotate_90 %t0_ui8 { rotate = 1 : si32 } : tensor<3x100x200xui8> -> tensor<3x100x200xui8>

	return
    }
}
