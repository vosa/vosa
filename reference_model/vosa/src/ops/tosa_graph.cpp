/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "tosa_graph.h"

#include <fstream>

#include "model_common.h"
#include "subgraph_traverser.h"
#include "tosa_serialization_handler.h"

using namespace std;

using vosa::ArrayBase;

// NOTE: These must be globally-defined as they are required by TOSA

// Note: These are hidden in a class to avoid pollution
class TOSA_Graph_Helper {
public:
  unique_ptr<tosa::TosaSerializationHandler> tsh;
  unique_ptr<TosaReference::SubgraphTraverser> graph;

  explicit TOSA_Graph_Helper(const vector<uint8_t> &buffer) {
    // Load the flatbuffer and deserialise
    tsh = make_unique<tosa::TosaSerializationHandler>();
    tsh->LoadFileTosaFlatbuffer(buffer.data(), (int)buffer.size());

    // Traverse the TOSA Graph
    graph = make_unique<TosaReference::SubgraphTraverser>(
        tsh->GetMainRegion()->GetBlockByName("main"), tsh.get(), nullptr);
    graph->initializeGraph();
    graph->linkTensorsAndNodes();
  }
};

template <typename T>
static shared_ptr<ArrayBase> tosa_to_vosa(TosaReference::ETensor0<T> &tensor) {
  vosa::EigenScalar<T> scalar;
  scalar() = tensor();
  return make_shared<ArrayBase>(scalar);
}

template <typename T>
static shared_ptr<ArrayBase> tosa_to_vosa(TosaReference::ETensor1<T> &tensor) {
  const uint32_t width = tensor.dimension(1);

  vosa::EigenVector<T> vector(width);
  for (uint32_t x = 0; x < width; x++)
    vector(x) = tensor(x);

  return make_shared<ArrayBase>(vector);
}

template <typename T>
static shared_ptr<ArrayBase> tosa_to_vosa(TosaReference::ETensor2<T> &tensor) {
  const uint32_t height = tensor.dimension(0), width = tensor.dimension(1);

  vosa::EigenPlane<T> plane(height, width);
  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      plane(y, x) = tensor(y, x);

  return make_shared<ArrayBase>(plane);
}

template <typename T>
static shared_ptr<ArrayBase> tosa_to_vosa(TosaReference::ETensor3<T> &tensor) {
  const uint32_t height = tensor.dimension(0), width = tensor.dimension(1),
                 channels = tensor.dimension(2);

  vosa::EigenImage<T> image(height, width, channels);
  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      for (uint32_t c = 0; c < channels; c++)
        image(y, x, c) = tensor(y, x, c);

  return make_shared<ArrayBase>(image);
}

template <typename T>
static shared_ptr<ArrayBase> tosa_to_vosa(TosaReference::ETensor4<T> &tensor) {
  const uint32_t batches = tensor.dimension(0), height = tensor.dimension(1),
                 width = tensor.dimension(2), channels = tensor.dimension(3);

  vosa::EigenImage<T> image(height, width, channels * batches);
  for (uint32_t b = 0; b < batches; b++)
    for (uint32_t y = 0; y < height; y++)
      for (uint32_t x = 0; x < width; x++)
        for (uint32_t c = 0; c < channels; c++)
          image(y, x, b * channels + c) = tensor(b, y, x, c);

  return make_shared<ArrayBase>(image);
}

template <typename T>
static shared_ptr<ArrayBase> tosa_to_vosa(TosaReference::ETensor5<T> &tensor) {

  const uint32_t batches = tensor.dimension(0);
  assert(batches == 1); // NOTE: For the time being, we'll only accept a 4D
                        // Tensor masked as a 5D Tensor
  const uint32_t height = tensor.dimension(1), width = tensor.dimension(2),
                 channels = tensor.dimension(3), slices = tensor.dimension(4);

  vosa::EigenImage<T> image(height, width, channels * slices);
  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      for (uint32_t c = 0; c < channels; c++)
        for (uint32_t s = 0; s < slices; s++)
          image(y, x, c * slices + s) = tensor(0, y, x, c, s);

  return make_shared<ArrayBase>(image);
}

template <typename T, int rank>
static shared_ptr<ArrayBase> tosa_to_vosa(TosaReference::Tensor *tensor_ptr) {
  auto tensor_template =
      dynamic_cast<TosaReference::TensorTemplate<Eigen::Tensor<T, rank>> *>(
          tensor_ptr);
  auto tensor = tensor_template->getTensor();
  return tosa_to_vosa<T>(tensor);
}

template <int rank>
static shared_ptr<ArrayBase> tosa_to_vosa(TosaReference::Tensor *tensor_ptr) {
  const TosaReference::TOSA_REF_TYPE dtype = tensor_ptr->getDtype();
  switch (dtype) {
  case TosaReference::TOSA_REF_TYPE_UINT8:
    return tosa_to_vosa<uint8_t, rank>(tensor_ptr);
  case TosaReference::TOSA_REF_TYPE_UINT16:
    return tosa_to_vosa<uint16_t, rank>(tensor_ptr);
    //        case tosa::DType::DType_UINT32: return tosa_to_vosa<uint32_t,
    //        rank>(tensor_ptr); NOTE: TOSA does NOT support UINT32 !
  case TosaReference::TOSA_REF_TYPE_INT8:
    return tosa_to_vosa<int8_t, rank>(tensor_ptr);
  case TosaReference::TOSA_REF_TYPE_INT16:
    return tosa_to_vosa<int16_t, rank>(tensor_ptr);
  case TosaReference::TOSA_REF_TYPE_INT32:
    return tosa_to_vosa<int32_t, rank>(tensor_ptr);
  case TosaReference::TOSA_REF_TYPE_FP32:
    return tosa_to_vosa<float, rank>(tensor_ptr);
  case TosaReference::TOSA_REF_TYPE_BOOL:
    return tosa_to_vosa<bool, rank>(tensor_ptr);
  default:
    throw runtime_error("DType NOT supported !");
  }
}

static shared_ptr<ArrayBase> tosa_to_vosa(TosaReference::Tensor *tensor_ptr) {
  const int rank = tensor_ptr->getRank();
  switch (rank) {
  case 0:
    return tosa_to_vosa<0>(tensor_ptr);
  case 1:
    return tosa_to_vosa<1>(tensor_ptr);
  case 2:
    return tosa_to_vosa<2>(tensor_ptr);
  case 3:
    return tosa_to_vosa<3>(tensor_ptr);
  case 4:
    return tosa_to_vosa<4>(tensor_ptr);
  case 5:
    return tosa_to_vosa<5>(tensor_ptr);
    //        case 6: return tosa_to_vosa<6>(tensor_ptr); // NOTE: For the time
    //        being, we are not supporting 6D Tensors !
  default:
    throw runtime_error("Cannot convert tensor of rank " + to_string(rank));
  }
}

shared_ptr<vosa::Op> vosa::TOSA_Graph_Base::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_tosa_graph = vosa_operator_union.AsTOSA_GRAPH();
  assert(serialized_tosa_graph);

  vector<uint8_t> buffer = serialized_tosa_graph->attr->buffer;

  return make_shared<TOSA_Graph>(buffer);
}

vosa::TOSA_Graph::TOSA_Graph(const vector<uint8_t> &buffer)
    : buffer(buffer),
      tosa_graph_helper(make_unique<TOSA_Graph_Helper>(buffer)) {

  // Set up configuration and debug subsystems
  g_func_debug.init_debug(0);

  // NOTE: Perform the validation here or `g_func_config` | `g_func_debug` will
  // complain
  if (tosa_graph_helper->graph->validateGraph())
    throw std::runtime_error("Failed to validate TOSA Graph !");
  tosa_graph_helper->graph->allocateTensor();
}

vosa::TOSA_Graph::~TOSA_Graph() = default;

template <typename T>
void vosa_to_tosa(uint32_t num_values, T *data, TosaReference::Tensor *tensor);

template <>
void vosa_to_tosa<bool>(uint32_t num_values, bool *data,
                        TosaReference::Tensor *tensor) {
  tensor->setTensorValueBool(num_values, data);
}

template <>
void vosa_to_tosa<int32_t>(uint32_t num_values, int32_t *data,
                           TosaReference::Tensor *tensor) {
  tensor->setTensorValueInt32(num_values, data);
}

template <>
void vosa_to_tosa<float>(uint32_t num_values, float *data,
                         TosaReference::Tensor *tensor) {
  tensor->setTensorValueFloat(num_values, data);
}

template <typename T>
static void vosa_to_tosa(const shared_ptr<ArrayBase> &input,
                         TosaReference::Tensor *tensor) {

  const vector<uint32_t> dimensions = input->get_dimensions();
  uint32_t num_values = std::accumulate(dimensions.begin(), dimensions.end(), 1,
                                        std::multiplies());

  ArrayBase::AType atype = input->get_atype();
  switch (atype) {
  case ArrayBase::AType::SCALAR:
    vosa_to_tosa<T>(num_values, input->template get_scalar<T>().data(), tensor);
    break;
  case ArrayBase::AType::VECTOR:
    vosa_to_tosa<T>(num_values, input->template get_vector<T>().data(), tensor);
    break;
  case ArrayBase::AType::PLANE:
    vosa_to_tosa<T>(num_values, input->template get_plane<T>().data(), tensor);
    break;
  case ArrayBase::AType::IMAGE:
    vosa_to_tosa<T>(num_values, input->template get_image<T>().data(), tensor);
    break;
  default:
    throw runtime_error("AType NOT supported !");
  }
}

void vosa_to_tosa(const shared_ptr<ArrayBase> &input,
                  TosaReference::Tensor *tensor) {
  const vector<uint32_t> vosa_shape = input->get_dimensions();
  vector<int> tosa_shape = tensor->getShape();

  // Allow for n-D arrays masked as (n+1)-D arrays if the first dimension is
  // equal to 0
  assert(vosa_shape.size() == tosa_shape.size() ||
         (vosa_shape.size() + 1 == tosa_shape.size() && tosa_shape[0] == 1));

  // Enforce identical dimensions for the Tensors
  if (vosa_shape.size() != tosa_shape.size())
    tosa_shape.erase(tosa_shape.begin());
  for (uint32_t i = 0; i < tosa_shape.size(); i++)
    assert(vosa_shape[i] == (uint32_t)tosa_shape[i]);

  ArrayBase::DType dtype = input->get_dtype();
  switch (dtype) {
  case ArrayBase::DType::BOOL:
    vosa_to_tosa<bool>(input, tensor);
    break;
    //        case ArrayBase::UINT8: vosa_to_tosa<uint8_t>(input, tensor); //
    //        NOTE: NO way to directly set UINT8 values in TOSA
    //                               break;
    //        case ArrayBase::UINT16: vosa_to_tosa<uint16_t>(input, tensor); //
    //        NOTE: NO way to directly set UINT16 values in TOSA
    //                                break;
    //        case ArrayBase::UINT32: vosa_to_tosa<uint32_t>(input, tensor); //
    //        NOTE: NO way to directly set UINT32 values in TOSA
    //                                break;
    //        case ArrayBase::INT8: vosa_to_tosa<int8_t>(input, tensor); //
    //        NOTE: NO way to directly set INT8 values in TOSA
    //                              break;
    //        case ArrayBase::INT16: vosa_to_tosa<int16_t>(input, tensor); //
    //        NOTE: NO way to directly set INT16 values in TOSA
    //                               break;
  case ArrayBase::INT32:
    vosa_to_tosa<int32_t>(input, tensor);
    break;
    //        case ArrayBase::FLOAT16: vosa_to_tosa<float16_t>(input, tensor);
    //        // NOTE: No FP16 support yet !
    //                                 break;
  case ArrayBase::FLOAT32:
    vosa_to_tosa<float>(input, tensor);
    break;
    //        case ArrayBase::FLOAT64: vosa_to_tosa<double>(input, tensor); //
    //        // NOTE: NO way to directly set FP64 values in TOSA
    //                                 break;
  default:
    throw std::runtime_error("DType NOT Supported !");
  }

  // Mark Tensor as valid
  tensor->setIsValid();
}

shared_ptr<ArrayBase>
vosa::TOSA_Graph::eval(const vector<shared_ptr<ArrayBase>> &inputs) const {

  const uint32_t numTensors = inputs.size();
  assert(numTensors ==
         (uint32_t)tosa_graph_helper->graph->getNumInputTensors());

  // Set Inputs
  for (uint32_t i = 0; i < inputs.size(); i++) {
    auto tensor = tosa_graph_helper->graph->getInputTensor(i);
    vosa_to_tosa(inputs[i], tensor);

    for (auto graph_node : tensor->getConsumers())
      if (graph_node->hasAllInputsReady() && !graph_node->getOnNextNodeList())
        tosa_graph_helper->graph->addToNextNodeList(graph_node);
  }

  // Run TOSA Graph
  tosa_graph_helper->graph->evaluateAll();
  if (tosa_graph_helper->graph->getGraphStatus() != GraphStatus::TOSA_VALID)
    throw runtime_error("Something happened while running the TOSA Graph");

  g_func_debug.fini_debug();

  // NOTE: For the time being, we can only support TOSA Graphs with only 1
  // Output
  assert(tosa_graph_helper->graph->getNumOutputTensors() == 1);

  // Get Outputs
  shared_ptr<ArrayBase> output =
      tosa_to_vosa(tosa_graph_helper->graph->getOutputTensor(0));
  return output;
}

ArrayBase::DType vosa::TOSA_Graph::output_dtype() const {
  const auto tosa_dtype =
      tosa_graph_helper->graph->getOutputTensor(0)->getDtype();
  switch (tosa_dtype) {
  case TosaReference::TOSA_REF_TYPE_UINT8:
    return ArrayBase::DType::UINT8;
  case TosaReference::TOSA_REF_TYPE_UINT16:
    return ArrayBase::DType::UINT16;
    //        case tosa::DType::DType_UINT32 : return ArrayBase::DType::UINT32;
    //        // NOTE: TOSA does NOT support UINT32 Tensors
  case TosaReference::TOSA_REF_TYPE_INT8:
    return ArrayBase::DType::INT8;
  case TosaReference::TOSA_REF_TYPE_INT16:
    return ArrayBase::DType::INT16;
  case TosaReference::TOSA_REF_TYPE_INT32:
    return ArrayBase::DType::INT32;
  case TosaReference::TOSA_REF_TYPE_FP32:
    return ArrayBase::DType::FLOAT32;
  case TosaReference::TOSA_REF_TYPE_BOOL:
    return ArrayBase::DType::BOOL;
  default:
    throw runtime_error("TOSA Dtype NOT Supported");
  }
}

unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::TOSA_Graph::serialize(const string &op_name,
                            const vector<string> &input_names) const {

  auto tosa_graph_attribute =
      make_unique<vosa_serialization::TOSA_GRAPH_ATTRIBUTET>();
  tosa_graph_attribute->buffer = buffer;

  vosa_serialization::TOSA_GRAPHT tosa_graph;
  tosa_graph.output_type = ArrayBase::serialize_dtype(this->output_dtype());
  tosa_graph.attr = std::move(tosa_graph_attribute);

  auto tosa_graph_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  tosa_graph_operator->op.Set(std::move(tosa_graph));
  tosa_graph_operator->name = op_name;
  tosa_graph_operator->inputs = input_names;

  return tosa_graph_operator;
}

[[maybe_unused]] void vosa::TOSA_Graph::to_json(const string &filename) {
  tosa_graph_helper->tsh->LoadFileSchema(
      "reference_model/external/tosa_reference_model/thirdparty/"
      "serialization_lib/schema/tosa.fbs");
  tosa_graph_helper->tsh->SaveFileJson(filename.c_str());
}
