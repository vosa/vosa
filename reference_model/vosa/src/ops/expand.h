/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_EXPAND_H
#define VOSA_EXPAND_H

#include "vosa/op.h"

namespace vosa {

class ExpandBase : public Op {
public:
  static std::shared_ptr<Op>
  parse_serialized(const vosa_serialization::VosaOperatorUnion &);
};

template <typename T>
class Expand : public ExpandBase {
public:
  Expand(T fill_value, const vosa::EigenPlane<uint8_t> &gather_kernel)
      : fill_value(fill_value), gather_kernel(gather_kernel) {}

  [[nodiscard]] std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &) const final;

  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

  [[nodiscard]] ArrayBase::DType output_dtype() const final;

private:
  [[nodiscard]] vosa_serialization::EXPAND_MODE get_serialization_mode() const;
  T fill_value;
  vosa::EigenPlane<uint8_t> gather_kernel;
};

} // namespace vosa

#endif // VOSA_EXPAND_H
