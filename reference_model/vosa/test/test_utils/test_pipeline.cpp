/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "test_pipeline.h"

namespace vosa::test {

std::vector<std::shared_ptr<ArrayBase>> TestPipeline::execute() {
  return graph.execute(inputs_map, output_names);
}

std::vector<ArrayBase>
TestPipeline::execute_and_save_serialized(const std::string &base_path,
                                          const bool save_json) {
  // Serialize and de-serialize graph
  const std::string graph_path = base_path + "_graph.flatbuffer";
  graph.write_flatbuffer(graph_path);
  graph = Graph::load_from_flatbuffer(graph_path);

  // Serialize and de-serialize inputs
  vosa_test_serialization::TestArtifactsT test_artifacts;
  this->serialize_and_deserialize_inputs(test_artifacts);

  // Run the VOSA graph
  std::vector<std::shared_ptr<ArrayBase>> graph_results = this->execute();

  // Serialize and de-serialize outputs
  std::vector<ArrayBase> serialized_outputs(graph_results.size());
  for (uint32_t i = 0; i < serialized_outputs.size(); i++) {
    const auto array = graph_results[i];

    const ArrayBase::AType atype = array->get_atype();
    const ArrayBase::DType dtype = array->get_dtype();
    const std::vector<uint32_t> dimensions = array->get_dimensions();
    const size_t size =
        std::accumulate(dimensions.begin(), dimensions.end(),
                        vosa::test::get_dtype_size(dtype), std::multiplies());
    const std::vector<uint8_t> data(array->get_raw_pointer().get(),
                                    array->get_raw_pointer().get() + size);

    serialized_outputs[i] = ArrayBase(atype, dtype, dimensions, data);
  }
  serialize_and_deserialize_outputs(test_artifacts, serialized_outputs);

  // Save (rest of) pipeline data
  vosa::test::write_artifacts_flatbuffer(
      test_artifacts, base_path + "_test_artifacts.flatbuffer");
  if (save_json) {
    graph.write_json(base_path + "_graph.json");
    vosa::test::write_artifacts_json(test_artifacts,
                                     base_path + "_test_artifacts.json");
  }

  return serialized_outputs;
}

void TestPipeline::serialize_and_deserialize_inputs(
    vosa_test_serialization::TestArtifactsT &test_artifacts) {
  std::vector<std::string> input_names;
  for (auto const &pair : inputs_map) {
    // Save name to preserve order later-on
    input_names.emplace_back(pair.first);

    // Serialize
    vosa::test::serialize_array(test_artifacts, pair.first, pair.second);
  }

  flatbuffers::FlatBufferBuilder artifacts_fbb;
  artifacts_fbb.Finish(vosa_test_serialization::TestArtifacts::Pack(
      artifacts_fbb, &test_artifacts));

  uint8_t *buffer = artifacts_fbb.GetBufferPointer();
  vosa_test_serialization::TestArtifactsT unpacked_test_artifacts;
  vosa_test_serialization::GetTestArtifacts(buffer)->UnPackTo(
      &unpacked_test_artifacts);

  // Update inputs_map with new ArrayBase's
  for (uint32_t i = 0; i < input_names.size(); i++) {
    ArrayBase deserialised_array =
        vosa::test::deserialize(*unpacked_test_artifacts.inputs[i]);
    inputs_map.at(input_names[i]) = deserialised_array;
  }
}

void TestPipeline::serialize_and_deserialize_outputs(
    vosa_test_serialization::TestArtifactsT &test_artifacts,
    std::vector<ArrayBase> &graph_results) {
  assert(output_names.size() == graph_results.size());

  // Serialise
  for (uint32_t i = 0; i < output_names.size(); i++)
    vosa::test::serialize_array(test_artifacts, output_names[i],
                                graph_results[i], false);

  flatbuffers::FlatBufferBuilder artifacts_fbb;
  artifacts_fbb.Finish(vosa_test_serialization::TestArtifacts::Pack(
      artifacts_fbb, &test_artifacts));

  uint8_t *buffer = artifacts_fbb.GetBufferPointer();
  vosa_test_serialization::TestArtifactsT unpacked_test_artifacts;
  vosa_test_serialization::GetTestArtifacts(buffer)->UnPackTo(
      &unpacked_test_artifacts);

  // Deserialise
  for (uint32_t i = 0; i < output_names.size(); i++)
    graph_results[i] =
        vosa::test::deserialize(*unpacked_test_artifacts.outputs[i]);
}

} // namespace vosa::test
