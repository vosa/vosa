/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "broadcast_planewise.h"

std::shared_ptr<vosa::Op> vosa::BroadcastPlanewiseBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_broadcast_planewise =
      vosa_operator_union.AsBROADCAST_PLANEWISE();
  assert(serialized_broadcast_planewise);
  auto mode = serialized_broadcast_planewise->mode;
  auto serialized_attributes = &serialized_broadcast_planewise->attr;
  std::vector<uint32_t> plane_size = serialized_attributes->get()->size;
  switch (mode) {
  case vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::BroadcastPlanewise<uint8_t>>(plane_size);
  }
  case vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::BroadcastPlanewise<uint16_t>>(plane_size);
  }
  case vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::BroadcastPlanewise<uint32_t>>(plane_size);
  }
  case vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::BroadcastPlanewise<int8_t>>(plane_size);
  }
  case vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::BroadcastPlanewise<int16_t>>(plane_size);
  }
  case vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::BroadcastPlanewise<int32_t>>(plane_size);
  }
  case vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::BroadcastPlanewise<float>>(plane_size);
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::EigenImage<T>
vosa::BroadcastPlanewise<T>::runme(const EigenImage<T> &input) const {

  uint32_t height = size[0];
  uint32_t width = size[1];

  EigenImage<T> output{height, width, 1};
  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      output(height_index, width_index, 0) = input(0, 0, 0);
    }
  }
  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::BroadcastPlanewise<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::BroadcastPlanewise<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::BROADCAST_PLANEWISE_MODE
vosa::BroadcastPlanewise<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::BROADCAST_PLANEWISE_MODE
vosa::BroadcastPlanewise<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::BROADCAST_PLANEWISE_MODE
vosa::BroadcastPlanewise<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::BROADCAST_PLANEWISE_MODE
vosa::BroadcastPlanewise<int8_t>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::BROADCAST_PLANEWISE_MODE
vosa::BroadcastPlanewise<int16_t>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::BROADCAST_PLANEWISE_MODE
vosa::BroadcastPlanewise<int32_t>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::BROADCAST_PLANEWISE_MODE
vosa::BroadcastPlanewise<float>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_FLOAT32_FLOAT32;
}

template <>
vosa_serialization::BROADCAST_PLANEWISE_MODE
vosa::BroadcastPlanewise<double>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_PLANEWISE_MODE_I_I_FLOAT64_FLOAT64;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::BroadcastPlanewise<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {
  auto broadcast_planewise_attribute =
      std::make_unique<vosa_serialization::BROADCAST_PLANEWISE_ATTRIBUTET>();
  broadcast_planewise_attribute->size = size;

  vosa_serialization::BROADCAST_PLANEWISET broadcast_planewise;
  broadcast_planewise.mode = get_serialization_mode();
  broadcast_planewise.attr = std::move(broadcast_planewise_attribute);

  auto broadcast_single_pixel_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  broadcast_single_pixel_operator->op.Set(std::move(broadcast_planewise));
  broadcast_single_pixel_operator->name = op_name;
  broadcast_single_pixel_operator->inputs = input_names;
  return broadcast_single_pixel_operator;
}

template class vosa::BroadcastPlanewise<int8_t>;

template class vosa::BroadcastPlanewise<int16_t>;

template class vosa::BroadcastPlanewise<int32_t>;

template class vosa::BroadcastPlanewise<uint8_t>;

template class vosa::BroadcastPlanewise<uint16_t>;

template class vosa::BroadcastPlanewise<uint32_t>;

template class vosa::BroadcastPlanewise<float>;

template class vosa::BroadcastPlanewise<double>;