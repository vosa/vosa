/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "min.h"

using namespace std;
using namespace vosa;
using namespace vosa_serialization;

// Template instantiation
template class vosa::Min<uint8_t>;

template class vosa::Min<uint16_t>;

template class vosa::Min<uint32_t>;

template class vosa::Min<int8_t>;

template class vosa::Min<int16_t>;

template class vosa::Min<int32_t>;

template class vosa::Min<float>;

shared_ptr<Op>
MinBase::parse_serialized(const VosaOperatorUnion &vosa_operator_union) {
  auto serialized_min = vosa_operator_union.AsMIN_();
  switch (serialized_min->mode) {
  case MIN__MODE_I_I_UINT8_UINT8:
    return make_shared<Min<uint8_t>>();
  case MIN__MODE_I_I_UINT16_UINT16:
    return make_shared<Min<uint16_t>>();
  case MIN__MODE_I_I_UINT32_UINT32:
    return make_shared<Min<uint32_t>>();
  case MIN__MODE_I_I_INT8_INT8:
    return make_shared<Min<int8_t>>();
  case MIN__MODE_I_I_INT16_INT16:
    return make_shared<Min<int16_t>>();
  case MIN__MODE_I_I_INT32_INT32:
    return make_shared<Min<int32_t>>();
  case MIN__MODE_I_I_FLOAT32_FLOAT32:
    return make_shared<Min<float>>();
  default:
    throw OperatorModeNotImplementedError();
  }
}

template <typename T>
shared_ptr<ArrayBase>
Min<T>::eval(const vector<shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input_0 = inputs[0]->template get_image<T>();
  const EigenImage<T> &input_1 = inputs[1]->template get_image<T>();

  return make_shared<ArrayBase>(this->runme(input_0, input_1));
}

template <typename T>
EigenImage<T> Min<T>::runme(const EigenImage<T> &input1,
                            const EigenImage<T> &input2) const {
  const uint32_t height = input1.dimension(0);
  const uint32_t width = input1.dimension(1);
  const uint32_t channels = input1.dimension(2);

  assert(height == input2.dimension(0));
  assert(width == input2.dimension(1));
  assert(channels == input2.dimension(2));

  EigenImage<T> output(height, width, channels);

  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      for (uint32_t c = 0; c < channels; c++)
        output(y, x, c) = std::min(input1(y, x, c), input2(y, x, c));

  return output;
}

template <typename T>
ArrayBase::DType Min<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
unique_ptr<VosaGraphOperatorT>
Min<T>::serialize(const string &op_name,
                  const vector<string> &input_names) const {
  MIN_T min;
  min.mode = get_serialization_mode();

  auto min_operator = make_unique<VosaGraphOperatorT>();
  min_operator->op.Set(min);
  min_operator->name = op_name;
  min_operator->inputs = input_names;

  return min_operator;
}

template <>
MIN__MODE vosa::Min<uint8_t>::get_serialization_mode() const {
  return MIN__MODE_I_I_UINT8_UINT8;
}

template <>
MIN__MODE vosa::Min<uint16_t>::get_serialization_mode() const {
  return MIN__MODE_I_I_UINT16_UINT16;
}

template <>
MIN__MODE vosa::Min<uint32_t>::get_serialization_mode() const {
  return MIN__MODE_I_I_UINT32_UINT32;
}

template <>
MIN__MODE vosa::Min<int8_t>::get_serialization_mode() const {
  return MIN__MODE_I_I_INT8_INT8;
}

template <>
MIN__MODE vosa::Min<int16_t>::get_serialization_mode() const {
  return MIN__MODE_I_I_INT16_INT16;
}

template <>
MIN__MODE vosa::Min<int32_t>::get_serialization_mode() const {
  return MIN__MODE_I_I_INT32_INT32;
}

template <>
MIN__MODE vosa::Min<float>::get_serialization_mode() const {
  return MIN__MODE_I_I_FLOAT32_FLOAT32;
}
