// run command without FileCheck to see output (or add "--dump-input always" FileCheck option)
// RUN: vosa-opt -vosa-data-format=chw -vosa-to-tosa %s | %test_src_root/e2e-runner | FileCheck %s

func.func @main() {
  %input_si = vosa.constant dense<
      [[[ 11, 12, 13, 14 ],
        [ 21, 22, 23, 24 ],
        [ 31, 32, 33, 34 ]],
       [[ 111, 112, 113, 114 ],
        [ 121, 122, 123, 124 ],
        [ 131, 132, 133, 134 ]]]> : tensor<2x3x4xsi32>
  %input_ui = vosa.constant dense<
      [[[ 11, 12, 13, 14 ],
        [ 21, 22, 23, 24 ],
        [ 31, 32, 33, 34 ]],
       [[ 111, 112, 113, 114 ],
        [ 121, 122, 123, 124 ],
        [ 131, 132, 133, 134 ]]]> : tensor<2x3x4xui16>
  %input_f  = vosa.constant dense<
      [[[ 11., 12., 13., 14. ],
        [ 21., 22., 23., 24. ],
        [ 31., 32., 33., 34. ]],
       [[ 111., 112., 113., 114. ],
        [ 121., 122., 123., 124. ],
        [ 131., 132., 133., 134. ]]]> : tensor<2x3x4xf32>

  // identity - should get same output as input
  // CHECK: sizes = [3, 4]
  // CHECK-NEXT:   [11,    12,    13,    14],
  // CHECK-NEXT:   [21,    22,    23,    24],
  // CHECK-NEXT:   [31,    32,    33,    34]
  // CHECK: sizes = [3, 4]
  // CHECK-NEXT:   [111,    112,    113,    114],
  // CHECK-NEXT:   [121,    122,    123,    124],
  // CHECK-NEXT:   [131,    132,    133,    134]
  %res = vosa.conv_2d %input_si { filter = dense<[[1]]> : tensor<1x1xsi32> } : tensor<2x3x4xsi32> -> tensor<2x3x4xsi32>
  %resb = builtin.unrealized_conversion_cast %res : tensor<2x3x4xsi32> to tensor<2x3x4xi32>
  %dyn_res = tensor.cast %resb : tensor<2x3x4xi32> to tensor<?x?x?xi32>
  func.call @print_img_i32(%dyn_res) : (tensor<?x?x?xi32>) -> ()

  // 3x3 kernel
  // CHECK: sizes = [1, 2]
  // CHECK-NEXT:  [220,    230]
  // CHECK: sizes = [1, 2]
  // CHECK-NEXT:  [1220,    1230]
  %res3 = vosa.conv_2d %input_si { filter = dense<[[1, 1, 1], [1, 2, 1], [1, 1, 1]]> : tensor<3x3xsi32> } : tensor<2x3x4xsi32> -> tensor<2x1x2xsi32>
  %res3b = builtin.unrealized_conversion_cast %res3 : tensor<2x1x2xsi32> to tensor<2x1x2xi32>
  %dyn_res3 = tensor.cast %res3b : tensor<2x1x2xi32> to tensor<?x?x?xi32>
  func.call @print_img_i32(%dyn_res3) : (tensor<?x?x?xi32>) -> ()

  // unsigned data
  // CHECK: sizes = [1, 2]
  // CHECK-NEXT:  [220,    230]
  // CHECK: sizes = [1, 2]
  // CHECK-NEXT:  [1220,    1230]
  %res_ui_3 = vosa.conv_2d %input_ui { filter = dense<[[1, 1, 1], [1, 2, 1], [1, 1, 1]]> : tensor<3x3xui16> } : tensor<2x3x4xui16> -> tensor<2x1x2xui16>
  %res_ui_3a = vosa.cast %res_ui_3 wrap : tensor<2x1x2xui16> -> tensor<2x1x2xui32>
  %res_ui_3b = builtin.unrealized_conversion_cast %res_ui_3a : tensor<2x1x2xui32> to tensor<2x1x2xi32>
  %dyn_res_ui_3 = tensor.cast %res_ui_3b : tensor<2x1x2xi32> to tensor<?x?x?xi32>
  func.call @print_img_i32(%dyn_res_ui_3) : (tensor<?x?x?xi32>) -> ()

  // floating point
  // CHECK: sizes = [1, 2]
  // CHECK-NEXT:  [55, 57.5]
  // CHECK: sizes = [1, 2]
  // CHECK-NEXT:  [305, 307.5]
  %res_f1 = vosa.conv_2d %input_f { filter = dense<[[0.125, 0.25, 0.125], [0.25, 1.0, 0.25], [0.125, 0.25, 0.125]]> : tensor<3x3xf32> } : tensor<2x3x4xf32> -> tensor<2x1x2xf32>
  %dyn_res_f1 = tensor.cast %res_f1 : tensor<2x1x2xf32> to tensor<?x?x?xf32>
  func.call @print_img_f32(%dyn_res_f1) : (tensor<?x?x?xf32>) -> ()

  return
}


func.func @print_img_i32(%a : tensor<?x?x?xi32>) {
  // %unranked_all = tensor.cast %a : tensor<?x?x?xi32> to tensor<*xi32>
  // func.call @printMemrefI32(%unranked_all) : (tensor<*xi32>) -> ()
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c1 : tensor<?x?x?xi32>
  %w = tensor.dim %a, %c2 : tensor<?x?x?xi32>
  %nch = tensor.dim %a, %c0 : tensor<?x?x?xi32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[%c, 0, 0][1, %h, %w][1, 1, 1]  : tensor<?x?x?xi32> to tensor<1x?x?xi32>
     %chdata_c = tensor.collapse_shape %chdata [[0, 1], [2]] : tensor<1x?x?xi32> into tensor<?x?xi32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xi32> to tensor<*xi32>
     func.call @printMemrefI32(%unranked_chdata) : (tensor<*xi32>) -> ()
  }
  return
}

func.func @print_img_f32(%a : tensor<?x?x?xf32>) {
  // %unranked_all = tensor.cast %a : tensor<?x?x?xf32> to tensor<*xf32>
  // func.call @printMemrefF32(%unranked_all) : (tensor<*xf32>) -> ()
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c1 : tensor<?x?x?xf32>
  %w = tensor.dim %a, %c2 : tensor<?x?x?xf32>
  %nch = tensor.dim %a, %c0 : tensor<?x?x?xf32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[%c, 0, 0][1, %h, %w][1, 1, 1]  : tensor<?x?x?xf32> to tensor<1x?x?xf32>
     %chdata_c = tensor.collapse_shape %chdata [[0, 1], [2]] : tensor<1x?x?xf32> into tensor<?x?xf32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xf32> to tensor<*xf32>
     func.call @printMemrefF32(%unranked_chdata) : (tensor<*xf32>) -> ()
  }
  return
}

func.func private @printMemrefI32(%ptr : tensor<*xi32> {bufferization.access = "read"})
func.func private @printMemrefF32(%ptr : tensor<*xf32> {bufferization.access = "read"})
