/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "sobel.h"

namespace vosa::samples::sobel {

std::shared_ptr<vosa::api::Op>
make_sobel_graph(vosa::api::GraphBuilder &graph_builder) {

  auto input = vosa::api::input(
      graph_builder, vosa::api::ArrayBase::DType::UINT8, {1080, 1920, 1});

  auto input_16 = vosa::api::cast<int16_t>(graph_builder, input);

  vosa::api::EigenPlane<int16_t> sobel_kernel_x{3, 3};
  vosa::api::EigenPlane<int16_t> sobel_kernel_y{3, 3};
  sobel_kernel_x.setValues({{1, 0, -1}, {2, 0, -2}, {1, 0, -1}});
  sobel_kernel_y.setValues({{1, 2, 1}, {0, 0, 0}, {-1, -2, -1}});

  auto input_16_padded = vosa::api::pad<int16_t>(
      graph_builder, input_16, 0, vosa::api::PadBase::VOSA_PAD_MODE_CONSTANT,
      {1, 1, 1, 1});
  auto sobel_x =
      vosa::api::conv<int16_t>(graph_builder, input_16_padded, sobel_kernel_x);
  sobel_x = vosa::api::abs(graph_builder, sobel_x);
  auto sobel_y =
      vosa::api::conv<int16_t>(graph_builder, input_16_padded, sobel_kernel_y);
  sobel_y = vosa::api::abs(graph_builder, sobel_y);

  auto sobel_total = vosa::api::add(graph_builder, sobel_x, sobel_y);

  return vosa::api::cast<uint8_t>(graph_builder, sobel_total);
}

} // namespace vosa::samples::sobel