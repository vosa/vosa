// RUN: vosa-opt -split-input-file -verify-diagnostics %s | FileCheck %s

func.func @foo() -> si32 {
    %v = vosa.constant 1234 : si32
    return %v : si32
}

func.func @bar(%arg0: si32) -> si32 {
    %a = arith.constant 1234 : i32
    %b = builtin.unrealized_conversion_cast %arg0 : si32 to i32
    %v = arith.addi %a, %b : i32
    %r = builtin.unrealized_conversion_cast %v : i32 to si32
    return %r : si32
}

func.func @baz(%arg0: si32, %arg1: f32) -> si32 {
    %a = builtin.unrealized_conversion_cast %arg0 : si32 to i32
    %b = arith.fptosi %arg1 : f32 to i32
    %v = arith.addi %a, %b : i32
    %r = builtin.unrealized_conversion_cast %v : i32 to si32
    return %r : si32
}

func.func @custom_scalar() {
    %i = vosa.constant 1234 : si32
    %f = vosa.constant 1234.0 : f32

    // CHECK: %{{.*}} = vosa.custom_scalar @foo() : () -> si32
    %a = vosa.custom_scalar @foo() : () -> si32
    // CHECK: %{{.*}} = vosa.custom_scalar @bar(%{{.*}}) : (si32) -> si32
    %b = vosa.custom_scalar @bar(%i) : (si32) -> si32
    // CHECK: %{{.*}} = vosa.custom_scalar @baz(%{{.*}}, %{{.*}}) : (si32, f32) -> si32
    %c = vosa.custom_scalar @baz(%i, %f) : (si32, f32) -> si32
    
    return
}

// -----
