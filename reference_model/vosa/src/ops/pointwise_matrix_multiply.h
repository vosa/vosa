/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_POINTWISE_MATRIX_MULTIPLY_H
#define VOSA_POINTWISE_MATRIX_MULTIPLY_H

#include "vosa/op.h"

namespace vosa {

class PointwiseMatrixMultiplyBase : public Op {
public:
  static std::shared_ptr<Op>
  parse_serialized(const vosa_serialization::VosaOperatorUnion &);
};

template <typename inputT, typename outputT>
class PointwiseMatrixMultiply final : public PointwiseMatrixMultiplyBase {
public:
  PointwiseMatrixMultiply(const EigenPlane<inputT> &matrix,
                          const EigenVector<inputT> &inner_offsets,
                          const EigenVector<inputT> &outer_offsets)
      : matrix(matrix), inner_offsets(inner_offsets),
        outer_offsets(outer_offsets) {
    assert(matrix.dimension(0) == outer_offsets.dimension(0));
    assert(matrix.dimension(1) == inner_offsets.dimension(0));
  }

  [[nodiscard]] std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &) const final;

  [[nodiscard]] ArrayBase::DType output_dtype() const final;

  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

private:
  [[nodiscard]] vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE
  get_serialization_mode() const;

private:
  EigenPlane<inputT> matrix;
  EigenVector<inputT> inner_offsets;
  EigenVector<inputT> outer_offsets;
};

} // namespace vosa

#endif // VOSA_POINTWISE_MATRIX_MULTIPLY_H
