/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/non_max_suppression.h"
#include "test_bench.h"
#include "vosa/vosa.h"

TEST(test_non_max_suppression, test_non_max_suppression) {

  vosa::EigenImage<uint8_t> input{3, 4, 1};
  input.setValues(
      {{{0}, {1}, {2}, {0}}, {{2}, {1}, {0}, {0}}, {{3}, {2}, {1}, {1}}});

  vosa::EigenPlane<bool> mask{3, 3};
  mask.setValues(
      {{false, true, false}, {true, true, true}, {false, true, false}});

  vosa::EigenPlane<uint8_t> expected_output{3, 4};
  expected_output.setValues({{0, 0, 2, 0}, {0, 0, 0, 0}, {3, 0, 0, 1}});

  auto output_as_image = vosa::NonMaxSuppression<uint8_t>(mask).runme(input);
  auto output_as_plane =
      vosa::test::Packing<uint8_t>::extract_plane(output_as_image, 0);

  bool compare = vosa::test::TestBench<uint8_t, int32_t>::compareEigenPlanes(
      output_as_plane, expected_output);
  EXPECT_EQ(compare, true);
}