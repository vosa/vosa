/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "reduce_min_planewise.h"

using namespace std;
using namespace vosa;
using namespace vosa_serialization;

// Template instantiation
template class vosa::ReduceMinPlanewise<uint8_t>;
template class vosa::ReduceMinPlanewise<uint16_t>;
template class vosa::ReduceMinPlanewise<uint32_t>;
template class vosa::ReduceMinPlanewise<int8_t>;
template class vosa::ReduceMinPlanewise<int16_t>;
template class vosa::ReduceMinPlanewise<int32_t>;
template class vosa::ReduceMinPlanewise<float>;

shared_ptr<Op> ReduceMinPlanewiseBase::parse_serialized(
    const VosaOperatorUnion &vosa_operator_union) {
  auto serialized_reduce_min_planewise =
      vosa_operator_union.AsREDUCE_MIN_PLANEWISE();
  switch (serialized_reduce_min_planewise->mode) {
  case REDUCE_MIN_PLANEWISE_MODE_I_I_UINT8_UINT8:
    return make_shared<ReduceMinPlanewise<uint8_t>>();
  case REDUCE_MIN_PLANEWISE_MODE_I_I_UINT16_UINT16:
    return make_shared<ReduceMinPlanewise<uint16_t>>();
  case REDUCE_MIN_PLANEWISE_MODE_I_I_UINT32_UINT32:
    return make_shared<ReduceMinPlanewise<uint32_t>>();
  case REDUCE_MIN_PLANEWISE_MODE_I_I_INT8_INT8:
    return make_shared<ReduceMinPlanewise<int8_t>>();
  case REDUCE_MIN_PLANEWISE_MODE_I_I_INT16_INT16:
    return make_shared<ReduceMinPlanewise<int16_t>>();
  case REDUCE_MIN_PLANEWISE_MODE_I_I_INT32_INT32:
    return make_shared<ReduceMinPlanewise<int32_t>>();
  case REDUCE_MIN_PLANEWISE_MODE_I_I_FLOAT32_FLOAT32:
    return make_shared<ReduceMinPlanewise<float>>();
  default:
    throw OperatorModeNotImplementedError();
  }
}

template <typename T>
shared_ptr<ArrayBase>
ReduceMinPlanewise<T>::eval(const vector<shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();

  return make_shared<ArrayBase>(this->runme(input));
}

template <typename T>
EigenImage<T> ReduceMinPlanewise<T>::runme(const EigenImage<T> &image) const {

  const uint32_t height = image.dimension(0);
  const uint32_t width = image.dimension(1);

  assert(image.dimension(2) == 1);

  // Find the minimum over the plane
  T minValue = image(0, 0, 0);

  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      minValue = std::min(minValue, image(y, x, 0));

  // Wrap in EigenScalar
  EigenImage<T> S_min{1, 1, 1};
  S_min.setConstant(minValue);

  return S_min;
}

template <typename T>
ArrayBase::DType ReduceMinPlanewise<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
unique_ptr<VosaGraphOperatorT>
ReduceMinPlanewise<T>::serialize(const string &op_name,
                                 const vector<string> &input_names) const {
  REDUCE_MIN_PLANEWISET reduce_min_planewise;
  reduce_min_planewise.mode = get_serialization_mode();

  auto reduce_min_planewise_operator = make_unique<VosaGraphOperatorT>();
  reduce_min_planewise_operator->op.Set(reduce_min_planewise);
  reduce_min_planewise_operator->name = op_name;
  reduce_min_planewise_operator->inputs = input_names;

  return reduce_min_planewise_operator;
}

template <>
REDUCE_MIN_PLANEWISE_MODE
vosa::ReduceMinPlanewise<uint8_t>::get_serialization_mode() const {
  return REDUCE_MIN_PLANEWISE_MODE_I_I_UINT8_UINT8;
}

template <>
REDUCE_MIN_PLANEWISE_MODE
vosa::ReduceMinPlanewise<uint16_t>::get_serialization_mode() const {
  return REDUCE_MIN_PLANEWISE_MODE_I_I_UINT16_UINT16;
}

template <>
REDUCE_MIN_PLANEWISE_MODE
vosa::ReduceMinPlanewise<uint32_t>::get_serialization_mode() const {
  return REDUCE_MIN_PLANEWISE_MODE_I_I_UINT32_UINT32;
}

template <>
REDUCE_MIN_PLANEWISE_MODE
vosa::ReduceMinPlanewise<int8_t>::get_serialization_mode() const {
  return REDUCE_MIN_PLANEWISE_MODE_I_I_INT8_INT8;
}

template <>
REDUCE_MIN_PLANEWISE_MODE
vosa::ReduceMinPlanewise<int16_t>::get_serialization_mode() const {
  return REDUCE_MIN_PLANEWISE_MODE_I_I_INT16_INT16;
}

template <>
REDUCE_MIN_PLANEWISE_MODE
vosa::ReduceMinPlanewise<int32_t>::get_serialization_mode() const {
  return REDUCE_MIN_PLANEWISE_MODE_I_I_INT32_INT32;
}

template <>
REDUCE_MIN_PLANEWISE_MODE
vosa::ReduceMinPlanewise<float>::get_serialization_mode() const {
  return REDUCE_MIN_PLANEWISE_MODE_I_I_FLOAT32_FLOAT32;
}
