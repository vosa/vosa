/* -*- c++ -*-
 *
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_MLIR_BUILDER_H
#define VOSA_MLIR_BUILDER_H

#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/MLIRContext.h"

#include "vosa/Dialect/Vosa/IR/VosaDialect.h"
#include "vosa/Dialect/Vosa/IR/VosaOps.h"
#include "vosa/Dialect/Vosa/Utils/DataFormat.h"

#include "mlir/Dialect/Func/IR/FuncOps.h"

namespace vosa {

enum class dtype_t {
  VOSA_NONE,
  VOSA_BOOL,
  VOSA_UINT8,
  VOSA_UINT16,
  VOSA_UINT32,
  VOSA_INT8,
  VOSA_INT16,
  VOSA_INT32,
  VOSA_FLOAT32
};

enum class pad_mode_t {
  VOSA_PAD_MODE_CONSTANT,
  VOSA_PAD_MODE_REPLICATE,
  VOSA_PAD_MODE_REFLECT,
  VOSA_PAD_MODE_MIRROR
};

enum class rounding_mode_t {
  VOSA_ROUNDING_MODE_SATURATE,
  VOSA_ROUNDING_MODE_WRAP
};

enum class round_method_t {
  VOSA_ROUND_METHOD_FLOOR,
  VOSA_ROUND_METHOD_CEIL,
  VOSA_ROUND_METHOD_TOWARDS_ZERO,
  VOSA_ROUND_METHOD_AWAY_FROM_ZERO,
  VOSA_ROUND_METHOD_HALF_UP,
  VOSA_ROUND_METHOD_HALF_DOWN,
  VOSA_ROUND_METHOD_HALF_TOWARDS_ZERO,
  VOSA_ROUND_METHOD_HALF_AWAY_FROM_ZERO,
  VOSA_ROUND_METHOD_HALF_TO_EVEN,
  VOSA_ROUND_METHOD_HALF_TO_ODD
};

class MLIRBuilder {
public:
  MLIRBuilder(mlir::MLIRContext &context, const std::string &inFile,
              const std::string &pipelineName,
              mlir::OwningOpRef<mlir::ModuleOp> &customOps);

  mlir::ModuleOp finalize(const std::vector<std::string> &outputs);

  bool Input(const std::string &name, dtype_t ty);

  bool Input(const std::string &name, dtype_t ty,
             llvm::ArrayRef<int64_t> shape);

  bool Equal(const std::string &name, const std::vector<std::string> &inputs);

  bool Greater(const std::string &name, const std::vector<std::string> &inputs);

  bool GreaterEqual(const std::string &name,
                    const std::vector<std::string> &inputs);

  bool BroadcastChannelwise(const std::string &name, dtype_t outTy,
                            const std::vector<std::string> &inputs,
                            uint32_t size);

  bool BroadcastPlanewise(const std::string &name, dtype_t outTy,
                          const std::vector<std::string> &inputs,
                          std::vector<uint32_t> size);

  bool Cast(const std::string &name, dtype_t outTy,
            const std::vector<std::string> &inputs,
            rounding_mode_t rounding_mode);

  bool ChannelExtract(const std::string &name, dtype_t outTy,
                      const std::vector<std::string> &inputs,
                      const std::vector<uint32_t> &channels);

  bool Concat(const std::string &name, dtype_t outTy,
              const std::vector<std::string> &inputs);

  template <typename T>
  bool ConstImage(const std::string &name, dtype_t outTy,
                  const std::vector<uint32_t> &dimensions, T value) {
    return ConstImageImpl(name, outTy, dimensions, scalar_attr<T>(value));
  }

  template <typename T>
  bool ConstImage(const std::string &name, dtype_t outTy,
                  const std::vector<uint32_t> &dimensions,
                  const std::vector<T> &value) {
    return ConstImageImpl(name, outTy, dimensions, attr_vector<T>(value));
  }

  template <typename T>
  bool ConstScalar(const std::string &name, dtype_t outTy, T value) {
    return ConstScalarImpl(name, outTy, scalar_attr<T>(value));
  }

  bool ImportChannel(const std::string &name, dtype_t outTy,
                     const std::vector<std::string> &inputs, uint32_t stride,
                     uint32_t offset, const std::vector<uint32_t> &shape);

  bool MeshGrid(const std::string &name, dtype_t outTy,
                const std::vector<std::string> &inputs,
                const std::vector<uint32_t> &shape);

  template <typename T>
  bool PiecewiseLinear(const std::string &name, dtype_t outTy,
                       const std::vector<std::string> &inputs,
                       const std::vector<T> &nodes,
                       const std::vector<T> &vals) {
    return PiecewiseLinearImpl(name, outTy, inputs, attr_vector<T>(nodes),
                               attr_vector<T>(vals));
  }

  bool TosaGraph(const std::string &name,
                 const std::vector<std::string> &inputs,
                 const std::string &op_name,
                 const std::vector<uint32_t> &output_shape);

  bool TosaGraph(const std::string &name, dtype_t outTy,
                 const std::vector<std::string> &inputs,
                 const std::vector<uint8_t> &buffer);

  bool Where(const std::string &name, dtype_t outTy,
             const std::vector<std::string> &inputs);

  bool AbsDiff(const std::string &name, dtype_t outTy,
               const std::vector<std::string> &inputs);

  bool Add(const std::string &name, dtype_t outTy,
           const std::vector<std::string> &inputs);

  bool Div(const std::string &name, dtype_t outTy,
           const std::vector<std::string> &inputs);

  bool Max(const std::string &name, dtype_t outTy,
           const std::vector<std::string> &inputs);

  bool Min(const std::string &name, dtype_t outTy,
           const std::vector<std::string> &inputs);

  bool Mod(const std::string &name, dtype_t outTy,
           const std::vector<std::string> &inputs);

  bool Mult(const std::string &name, dtype_t outTy,
            const std::vector<std::string> &inputs);

  bool Or(const std::string &name, dtype_t outTy,
          const std::vector<std::string> &inputs);

  bool Sub(const std::string &name, dtype_t outTy,
           const std::vector<std::string> &inputs);

  bool Abs(const std::string &name, dtype_t outTy,
           const std::vector<std::string> &inputs);

  bool ArithmeticShiftRight(const std::string &name, dtype_t outTy,
                            const std::vector<std::string> &inputs,
                            uint32_t shift);

  bool Clamp(const std::string &name, dtype_t outTy,
             const std::vector<std::string> &inputs);

  bool GammaCorrection(const std::string &name, dtype_t outTy,
                       const std::vector<std::string> &inputs, float gamma);

  bool LogicalShiftRight(const std::string &name, dtype_t outTy,
                         const std::vector<std::string> &inputs,
                         uint32_t shift);

  bool Not(const std::string &name, dtype_t outTy,
           const std::vector<std::string> &inputs);

  bool Power(const std::string &name, dtype_t outTy,
             const std::vector<std::string> &inputs, float base);

  bool Round(const std::string &name, dtype_t outTy,
             const std::vector<std::string> &inputs,
             round_method_t round_method);

  bool Sqrt(const std::string &name, dtype_t outTy,
            const std::vector<std::string> &inputs);

  template <typename I>
  bool Conv2D(const std::string &name, dtype_t inTy, dtype_t outTy,
              const std::vector<std::string> &inputs,
              const std::vector<uint32_t> &filter_dimension,
              const std::vector<I> &filter_value) {
    return Conv2DImpl(name, inTy, outTy, inputs, filter_dimension,
                      attr_vector(filter_value));
  }

  bool Decimate(const std::string &name, dtype_t outTy,
                const std::vector<std::string> &inputs,
                const std::vector<uint32_t> &N,
                const std::vector<uint32_t> &offsets);

  template <typename T>
  bool Expand(const std::string &name, dtype_t outTy,
              const std::vector<std::string> &inputs, T fill_value,
              const std::vector<uint32_t> &gather_kernel_dimension,
              const std::vector<uint8_t> &gather_kernel) {
    return ExpandImpl(name, outTy, inputs, scalar_attr<T>(fill_value),
                      gather_kernel_dimension, gather_kernel);
  }

  template <typename T>
  bool Pad(const std::string &name, dtype_t outTy,
           const std::vector<std::string> &inputs, T pad_constant,
           pad_mode_t pad_mode, const std::vector<uint32_t> &pad_size) {
    return PadImpl(name, outTy, inputs, scalar_attr<T>(pad_constant), pad_mode,
                   pad_size);
  }

  template <typename T>
  bool PointwiseMatrixMultiply(const std::string &name, dtype_t outTy,
                               const std::vector<std::string> &inputs,
                               const std::vector<T> &M,
                               const std::vector<T> &K_1,
                               const std::vector<T> &K_2) {
    return PointwiseMatrixMultiplyImpl(name, outTy, inputs, attr_vector<T>(M),
                                       attr_vector<T>(K_1),
                                       attr_vector<T>(K_2));
  }

  bool ResizeBilinear(const std::string &name, dtype_t outTy,
                      const std::vector<std::string> &inputs,
                      const std::vector<uint32_t> &size);

  bool ResizeNearestNeighbour(const std::string &name, dtype_t outTy,
                              const std::vector<std::string> &inputs,
                              const std::vector<uint32_t> &size);

  bool Rotate90(const std::string &name, dtype_t outTy,
                const std::vector<std::string> &inputs, int32_t rotate);

  bool ArgMaxChannelwise(const std::string &name,
                         const std::vector<std::string> &inputs);

  bool ArgMaxPlanewise(const std::string &name,
                       const std::vector<std::string> &inputs);

  bool ArgMinChannelwise(const std::string &name,
                         const std::vector<std::string> &inputs);

  bool ArgMinPlanewise(const std::string &name,
                       const std::vector<std::string> &inputs);

  bool ReduceInterpChannelwise(const std::string &name, dtype_t outTy,
                               const std::vector<std::string> &inputs);

  bool ReduceMaxChannelwise(const std::string &name, dtype_t outTy,
                            const std::vector<std::string> &inputs);

  bool ReduceMaxPlanewise(const std::string &name, dtype_t outTy,
                          const std::vector<std::string> &inputs);

  bool ReduceMinChannelwise(const std::string &name, dtype_t outTy,
                            const std::vector<std::string> &inputs);

  bool ReduceMinPlanewise(const std::string &name, dtype_t outTy,
                          const std::vector<std::string> &inputs);

  bool ReduceSumChannelwise(const std::string &name, dtype_t outTy,
                            const std::vector<std::string> &inputs);

  bool ReduceSumPlanewise(const std::string &name, dtype_t outTy,
                          const std::vector<std::string> &inputs);

  bool CustomScalar(const std::string &name,
                    const std::vector<std::string> &inputs,
                    const std::string &custom_op_name);

  dtype_t CustomScalarOutputType(const std::string &custom_op_name);

  bool ReportError(const std::string &error);

private:
  bool ConstImageImpl(const std::string &name, dtype_t outTy,
                      const std::vector<uint32_t> &dimensions,
                      mlir::Attribute value);

  bool ConstImageImpl(const std::string &name, dtype_t outTy,
                      const std::vector<uint32_t> &dimensions,
                      llvm::ArrayRef<mlir::Attribute> value);

  bool ConstScalarImpl(const std::string &name, dtype_t outTy,
                       mlir::Attribute value);

  bool PiecewiseLinearImpl(const std::string &name, dtype_t outTy,
                           const std::vector<std::string> &inputs,
                           llvm::ArrayRef<mlir::Attribute> nodes,
                           llvm::ArrayRef<mlir::Attribute> vals);

  bool Conv2DImpl(const std::string &name, dtype_t inTy, dtype_t outTy,
                  const std::vector<std::string> &inputs,
                  const std::vector<uint32_t> &filter_dimension,
                  llvm::ArrayRef<mlir::Attribute> filter_value);

  bool ExpandImpl(const std::string &name, dtype_t outTy,
                  const std::vector<std::string> &inputs,
                  mlir::Attribute fill_value,
                  const std::vector<uint32_t> &gather_kernel_dimension,
                  llvm::ArrayRef<uint8_t> gather_kernel);

  bool PadImpl(const std::string &name, dtype_t outTy,
               const std::vector<std::string> &inputs,
               mlir::Attribute pad_constant, pad_mode_t pad_mode,
               const std::vector<uint32_t> &pad_size);

  bool PointwiseMatrixMultiplyImpl(const std::string &name, dtype_t outTy,
                                   const std::vector<std::string> &inputs,
                                   llvm::ArrayRef<mlir::Attribute> M,
                                   llvm::ArrayRef<mlir::Attribute> K_1,
                                   llvm::ArrayRef<mlir::Attribute> K_2);

  template <typename O>
  void addOp(const std::string &name, mlir::Type outTy,
             const std::vector<std::string> &inputs,
             ::llvm::ArrayRef<::mlir::NamedAttribute> attrs = {}) {
    int opIdx = ++opCount;
    auto input_vals = getInputs(inputs);
    auto loc = mlir::NameLoc::get(builder.getStringAttr(name), fileLoc(opIdx));
    auto op = builder.create<O>(loc, mlir::TypeRange{outTy}, input_vals, attrs);
    values[name] = *op.getODSResults(0).begin();
  }

  mlir::Location l();

  mlir::Location fileLoc(int line);

  mlir::Value getInput(const std::string &name);

  llvm::SmallVector<mlir::Value> getInputs(llvm::ArrayRef<std::string> names);

  mlir::Type mlir_scalar_type(dtype_t ty);

  mlir::ShapedType mlir_single_pixel_type(dtype_t ty);

  mlir::ShapedType mlir_plane_type(dtype_t ty);

  mlir::ShapedType mlir_plane_type(dtype_t ty, const std::vector<uint32_t> &sz);

  mlir::ShapedType mlir_image_type(dtype_t ty, llvm::ArrayRef<uint32_t> sz);

  mlir::ShapedType mlir_image_type(dtype_t ty, int32_t ch,
                                   llvm::ArrayRef<uint32_t> sz);

  mlir::ShapedType mlir_image_type(dtype_t ty, int32_t ch,
                                   llvm::ArrayRef<int32_t> sz);

  mlir::ShapedType mlir_image_type(dtype_t ty);

  mlir::NamedAttribute namedAttr(llvm::StringRef name, mlir::Attribute value);

  template <typename T>
  mlir::Attribute scalar_attr(T value);

  mlir::Attribute i32_attr(uint32_t value);

  mlir::Attribute dense_elements_attr(dtype_t elTy,
                                      const llvm::ArrayRef<int64_t> dimensions,
                                      llvm::ArrayRef<mlir::Attribute> val);

  mlir::ArrayAttr i32_array_attr(llvm::ArrayRef<uint32_t> value);

  dtype_t builder_type(mlir::Type mlir_type);

  template <typename T>
  llvm::SmallVector<mlir::Attribute, 8> attr_vector(const std::vector<T> &val) {
    return llvm::to_vector<8>(llvm::map_range(
        val, [this](T v) -> mlir::Attribute { return scalar_attr<T>(v); }));
  }

  std::string inputFile;
  mlir::OpBuilder builder;
  mlir::ModuleOp module;
  mlir::func::FuncOp function;
  llvm::StringMap<mlir::Value> values;
  mlir::OwningOpRef<mlir::ModuleOp> &customOpsModule;
  int opCount;
};

} // namespace vosa

#endif // VOSA_MLIR_BUILDER_H
