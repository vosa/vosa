// RUN: vosa-opt -vosa-data-format=chw -split-input-file -verify-diagnostics %s | FileCheck %s

module {
    // CHECK-LABEL: func.func @broadcast_channelwise()
    func.func @broadcast_channelwise() {
        %c0_si8 = vosa.constant dense<1> : tensor<1x100x200xsi8>
        // CHECK: %{{.*}} = vosa.broadcast_channelwise %{{.*}} : tensor<1x100x200xsi8> -> tensor<3x100x200xsi8>
        %res_si8 = vosa.broadcast_channelwise %c0_si8 { size = 3 : i32 } : tensor<1x100x200xsi8> -> tensor<3x100x200xsi8>

        %c0_si16 = vosa.constant dense<1> : tensor<1x100x200xsi16>
        // CHECK: %{{.*}} = vosa.broadcast_channelwise %{{.*}} : tensor<1x100x200xsi16> -> tensor<3x100x200xsi16>
        %res_si16 = vosa.broadcast_channelwise %c0_si16 { size = 3 : i32 } : tensor<1x100x200xsi16> -> tensor<3x100x200xsi16>

        %c0_si32 = vosa.constant dense<1> : tensor<1x100x200xsi32>
        // CHECK: %{{.*}} = vosa.broadcast_channelwise %{{.*}} : tensor<1x100x200xsi32> -> tensor<3x100x200xsi32>
        %res_si32 = vosa.broadcast_channelwise %c0_si32 { size = 3 : i32 } : tensor<1x100x200xsi32> -> tensor<3x100x200xsi32>

        %c0_ui8 = vosa.constant dense<1> : tensor<1x100x200xui8>
        // CHECK: %{{.*}} = vosa.broadcast_channelwise %{{.*}} : tensor<1x100x200xui8> -> tensor<3x100x200xui8>
        %res_ui8 = vosa.broadcast_channelwise %c0_ui8 { size = 3 : i32 } : tensor<1x100x200xui8> -> tensor<3x100x200xui8>

        %c0_ui16 = vosa.constant dense<1> : tensor<1x100x200xui16>
        // CHECK: %{{.*}} = vosa.broadcast_channelwise %{{.*}} : tensor<1x100x200xui16> -> tensor<3x100x200xui16>
        %res_ui16 = vosa.broadcast_channelwise %c0_ui16 { size = 3 : i32 } : tensor<1x100x200xui16> -> tensor<3x100x200xui16>

        %c0_ui32 = vosa.constant dense<1> : tensor<1x100x200xui32>
        // CHECK: %{{.*}} = vosa.broadcast_channelwise %{{.*}} : tensor<1x100x200xui32> -> tensor<3x100x200xui32>
        %res_ui32 = vosa.broadcast_channelwise %c0_ui32 { size = 3 : i32 } : tensor<1x100x200xui32> -> tensor<3x100x200xui32>

        %c0_f16 = vosa.constant dense<1.0> : tensor<1x100x200xf16>
        // CHECK: %{{.*}} = vosa.broadcast_channelwise %{{.*}} : tensor<1x100x200xf16> -> tensor<3x100x200xf16>
        %res_f16 = vosa.broadcast_channelwise %c0_f16 { size = 3 : i32 } : tensor<1x100x200xf16> -> tensor<3x100x200xf16>

        %c0_f32 = vosa.constant dense<1.0> : tensor<1x100x200xf32>
        // CHECK: %{{.*}} = vosa.broadcast_channelwise %{{.*}} : tensor<1x100x200xf32> -> tensor<3x100x200xf32>
        %res_f32 = vosa.broadcast_channelwise %c0_f32 { size = 3 : i32 } : tensor<1x100x200xf32> -> tensor<3x100x200xf32>

        return
    }
}

// -----

module {
    func.func @broadcast_channelwise_bad_type() {
        %c0_i7 = arith.constant dense<1> : tensor<1x100x200xi7>
        // expected-error @below {{'vosa.broadcast_channelwise' op operand #0 must be}}
        %res_si7 = vosa.broadcast_channelwise %c0_i7 { size = 3 : i32 } : tensor<1x100x200xi7> -> tensor<3x100x200xsi7>

        return
    }
}

// -----

module {
    func.func @broadcast_channelwise_bad_input_planes() {
        %c0_si8 = vosa.constant dense<1> : tensor<2x100x200xsi8>
        // expected-error @below {{'vosa.broadcast_channelwise' op operand #0 must be single channel 3D tensor of}}
        %res_si8 = vosa.broadcast_channelwise %c0_si8 { size = 3 : i32 } : tensor<2x100x200xsi8> -> tensor<3x100x200xsi8>

        return
    }
}

// -----

module {
    func.func @broadcast_channelwise_bad_out_type() {
        %c0_si8 = vosa.constant dense<1> : tensor<1x100x200xsi8>
        // expected-error @below {{'vosa.broadcast_channelwise' op output type does not match input}}
        %res_si8 = vosa.broadcast_channelwise %c0_si8 { size = 3 : i32 } : tensor<1x100x200xsi8> -> tensor<3x100x200xsi32>

        return
    }
}

// -----

module {
    func.func @broadcast_channelwise_bad_out_type_sign_mismatch() {
        %c0_si8 = vosa.constant dense<1> : tensor<1x100x200xsi8>
        // expected-error @below {{'vosa.broadcast_channelwise' op output type does not match input}}
        %res_si8 = vosa.broadcast_channelwise %c0_si8 { size = 3 : i32 } : tensor<1x100x200xsi8> -> tensor<3x100x200xui8>

        return
    }
}

// -----

module {
    func.func @broadcast_channelwise_bad_out_size() {
        %c0_si8 = vosa.constant dense<1> : tensor<1x100x200xsi8>
        // expected-error @below {{'vosa.broadcast_channelwise' op invalid output shape}}
        %res_si8 = vosa.broadcast_channelwise %c0_si8 { size = 3 : i32 } : tensor<1x100x200xsi8> -> tensor<3x101x200xsi8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @broadcast_planewise()
    func.func @broadcast_planewise() {
        %c0_si8 = vosa.constant dense<1> : tensor<1x1x1xsi8>
        // CHECK: %{{.*}} = vosa.broadcast_planewise %{{.*}} : tensor<1x1x1xsi8> -> tensor<?x?x?xsi8>
        %res_si8 = vosa.broadcast_planewise %c0_si8 { size = [ 100 : i32, 200 : i32 ] } : tensor<1x1x1xsi8> -> tensor<?x?x?xsi8>

        %c0_si16 = vosa.constant dense<1> : tensor<1x1x1xsi16>
        // CHECK: %{{.*}} = vosa.broadcast_planewise %{{.*}} : tensor<1x1x1xsi16> -> tensor<?x?x?xsi16>
        %res_si16 = vosa.broadcast_planewise %c0_si16 { size = [ 100 : i32, 200 : i32 ] } : tensor<1x1x1xsi16> -> tensor<?x?x?xsi16>

        %c0_si32 = vosa.constant dense<1> : tensor<1x1x1xsi32>
        // CHECK: %{{.*}} = vosa.broadcast_planewise %{{.*}} : tensor<1x1x1xsi32> -> tensor<?x?x?xsi32>
        %res_si32 = vosa.broadcast_planewise %c0_si32 { size = [ 100 : i32, 200 : i32 ] } : tensor<1x1x1xsi32> -> tensor<?x?x?xsi32>

        %c0_f16 = vosa.constant dense<1.0> : tensor<1x1x1xf16>
        // CHECK: %{{.*}} = vosa.broadcast_planewise %{{.*}} : tensor<1x1x1xf16> -> tensor<?x?x?xf16>
        %res_f16 = vosa.broadcast_planewise %c0_f16 { size = [ 100 : i32, 200 : i32 ] } : tensor<1x1x1xf16> -> tensor<?x?x?xf16>

        %c0_f32 = vosa.constant dense<1.0> : tensor<1x1x1xf32>
        // CHECK: %{{.*}} = vosa.broadcast_planewise %{{.*}} : tensor<1x1x1xf32> -> tensor<?x?x?xf32>
        %res_f32 = vosa.broadcast_planewise %c0_f32 { size = [ 100 : i32, 200 : i32 ] } : tensor<1x1x1xf32> -> tensor<?x?x?xf32>

        return
    }
}

// -----

module {
    func.func @broadcast_planewise_type_mismatch_si18_si16() {
        %c0_si8 = vosa.constant dense<1> : tensor<1x1x1xsi8>
        // expected-error @below {{'vosa.broadcast_planewise' op result element width does not match input width}}
        %res_x1 = vosa.broadcast_planewise %c0_si8 { size = [ 100 : i32, 200 : i32 ] } : tensor<1x1x1xsi8> -> tensor<?x?x?xsi16>
        return
    }
}

// -----

module {
    func.func @broadcast_planewise_type_mismatch_si18_si32() {
        %c0_si8 = vosa.constant dense<1> : tensor<1x1x1xsi8>
        // expected-error @below {{'vosa.broadcast_planewise' op result element width does not match input width}}
        %res_x1 = vosa.broadcast_planewise %c0_si8 { size = [ 100 : i32, 200 : i32 ] } : tensor<1x1x1xsi8> -> tensor<?x?x?xsi32>
        return
    }
}

// -----

module {
    func.func @broadcast_planewise_type_mismatch_si18_f32() {
        %c0_si8 = vosa.constant dense<1> : tensor<1x1x1xsi8>
        // expected-error @below {{'vosa.broadcast_planewise' op unsupported result element type}}
        %res_x1 = vosa.broadcast_planewise %c0_si8 { size = [ 100 : i32, 200 : i32 ] } : tensor<1x1x1xsi8> -> tensor<?x?x?xf32>
        return
    }
}

// -----

module {
    func.func @broadcast_planewise_type_mismatch_f16_si32() {
        %c0_f16 = vosa.constant dense<1.0> : tensor<1x1x1xf16>

        // expected-error @below {{'vosa.broadcast_planewise' op unsupported result element type}}
        %res_x1 = vosa.broadcast_planewise %c0_f16 { size = [ 100 : i32, 200 : i32 ] } : tensor<1x1x1xf16> -> tensor<?x?x?xsi32>

        return
    }
}

// -----

module {
    func.func @broadcast_planewise_type_mismatch_f16_f32() {
        %c0_f16 = vosa.constant dense<1.0> : tensor<1x1x1xf16>

        // expected-error @below {{'vosa.broadcast_planewise' op result element width does not match input width}}
        %res_x1 = vosa.broadcast_planewise %c0_f16 { size = [ 100 : i32, 200 : i32 ] } : tensor<1x1x1xf16> -> tensor<?x?x?xf32>

        return
    }
}

// -----

module {
    func.func @broadcast_planewise_type_mismatch_f32_f16() {
        %c0_f16 = vosa.constant dense<1.0> : tensor<1x1x1xf32>

        // expected-error @below {{'vosa.broadcast_planewise' op result element width does not match input width}}
        %res_x1 = vosa.broadcast_planewise %c0_f16 { size = [ 100 : i32, 200 : i32 ] } : tensor<1x1x1xf32> -> tensor<?x?x?xf16>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @cast()
    func.func @cast() {
        %c0_bool = vosa.constant dense<1> : tensor<3x100x200xi1>
        %c0_si8 = vosa.constant dense<1> : tensor<3x100x200xsi8>
        %c0_si16 = vosa.constant dense<1> : tensor<3x100x200xsi16>
        %c0_si32 = vosa.constant dense<1> : tensor<3x100x200xsi32>
        %c0_ui8 = vosa.constant dense<1> : tensor<3x100x200xui8>
        %c0_ui16 = vosa.constant dense<1> : tensor<3x100x200xui16>
        %c0_ui32 = vosa.constant dense<1> : tensor<3x100x200xui32>
        %c0_f32 = vosa.constant dense<1.0> : tensor<3x100x200xf32>

        // bool to int
        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x100x200xi1> -> tensor<3x100x200xui8>
        %res_bool_u8 = vosa.cast %c0_bool wrap : tensor<3x100x200xi1> -> tensor<3x100x200xui8>
        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x100x200xi1> -> tensor<3x100x200xsi32>
        %res_bool_s32 = vosa.cast %c0_bool wrap : tensor<3x100x200xi1> -> tensor<3x100x200xsi32>

        // signed to signed
        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x100x200xsi8> -> tensor<3x100x200xsi32>
        %res_s8_s32 = vosa.cast %c0_si8 wrap : tensor<3x100x200xsi8> -> tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x100x200xsi32> -> tensor<3x100x200xsi16>
        %res_s32_s16 = vosa.cast %c0_si32 wrap : tensor<3x100x200xsi32> -> tensor<3x100x200xsi16>

        // signed to unsigned
        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x100x200xsi8> -> tensor<3x100x200xui32>
        %res_s8_u32 = vosa.cast %c0_si8 wrap : tensor<3x100x200xsi8> -> tensor<3x100x200xui32>
        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x100x200xsi32> -> tensor<3x100x200xui16>
        %res_s32_u16 = vosa.cast %c0_si32 wrap : tensor<3x100x200xsi32> -> tensor<3x100x200xui16>

        // signed to float
        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x100x200xsi8> -> tensor<3x100x200xf16>
        %res_si8_f16 = vosa.cast %c0_si8 wrap : tensor<3x100x200xsi8> -> tensor<3x100x200xf16>
        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x100x200xsi32> -> tensor<3x100x200xf32>
        %res_si32_f32 = vosa.cast %c0_si32 wrap : tensor<3x100x200xsi32> -> tensor<3x100x200xf32>

        // unsigned to unsigned
        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x100x200xui8> -> tensor<3x100x200xui32>
        %res_u8_u32 = vosa.cast %c0_ui8 wrap : tensor<3x100x200xui8> -> tensor<3x100x200xui32>
        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x100x200xui32> -> tensor<3x100x200xui16>
        %res_u32_u16 = vosa.cast %c0_ui32 wrap : tensor<3x100x200xui32> -> tensor<3x100x200xui16>

        // unsigned to signed
        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x100x200xui8> -> tensor<3x100x200xsi32>
        %res_u8_s32 = vosa.cast %c0_ui8 wrap : tensor<3x100x200xui8> -> tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x100x200xui32> -> tensor<3x100x200xsi16>
        %res_u32_s16 = vosa.cast %c0_ui32 wrap : tensor<3x100x200xui32> -> tensor<3x100x200xsi16>

        // unsigned to float
        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x100x200xui8> -> tensor<3x100x200xf16>
        %res_u8_f16 = vosa.cast %c0_ui8 wrap : tensor<3x100x200xui8> -> tensor<3x100x200xf16>
        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x100x200xui32> -> tensor<3x100x200xf32>
        %res2_u32 = vosa.cast %c0_ui32 wrap : tensor<3x100x200xui32> -> tensor<3x100x200xf32>

        // other modes
        // CHECK: %{{.*}} = vosa.cast %{{.*}} saturate : tensor<3x100x200xf32> -> tensor<3x100x200xsi8>
        %res3 = vosa.cast %c0_f32 saturate : tensor<3x100x200xf32> -> tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.cast %{{.*}} reinterpret : tensor<3x100x200xf32> -> tensor<3x100x200xsi32>
        %res4 = vosa.cast %c0_f32 reinterpret : tensor<3x100x200xf32> -> tensor<3x100x200xsi32>

        return
    }
}

// -----

module {
    func.func @cast_bad_sz() {
        %c0_si8 = vosa.constant dense<1> : tensor<3x100x200xsi8>
    
        // expected-error @below {{'vosa.cast' op output shape does not match input}}
        %res0 = vosa.cast %c0_si8 wrap : tensor<3x100x200xsi8> -> tensor<3x101x200xsi32>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @channel_extract()
    func.func @channel_extract() {

        %c0_si8 = vosa.constant dense<1> : tensor<3x200x100xsi8>
        // CHECK: %{{.*}} = vosa.channel_extract %{{.*}} {channels = [0 : i32]} : tensor<3x200x100xsi8> -> tensor<1x200x100xsi8>
        %res0 = vosa.channel_extract %c0_si8 { channels = [ 0 : i32 ] } : tensor<3x200x100xsi8> -> tensor<1x200x100xsi8>

        %c0_si16 = vosa.constant dense<1> : tensor<3x200x100xsi16>
        // CHECK: %{{.*}} = vosa.channel_extract %{{.*}} {channels = [1 : i32]} : tensor<3x200x100xsi16> -> tensor<1x200x100xsi16>
        %res1 = vosa.channel_extract %c0_si16 { channels = [ 1 : i32 ] } : tensor<3x200x100xsi16> -> tensor<1x200x100xsi16>

        %c0_si32 = vosa.constant dense<1> : tensor<3x200x100xsi32>
        // CHECK: %{{.*}} = vosa.channel_extract %{{.*}} {channels = [2 : i32]} : tensor<3x200x100xsi32> -> tensor<1x200x100xsi32>
        %res2 = vosa.channel_extract %c0_si32 { channels = [ 2 : i32 ] } : tensor<3x200x100xsi32> -> tensor<1x200x100xsi32>

        // CHECK: %{{.*}} = vosa.channel_extract %{{.*}} {channels = [1 : i32, 2 : i32]} : tensor<3x200x100xsi32> -> tensor<2x200x100xsi32>
        %res3 = vosa.channel_extract %c0_si32 { channels = [ 1 : i32, 2 : i32 ] } : tensor<3x200x100xsi32> -> tensor<2x200x100xsi32>

        // CHECK: %{{.*}} = vosa.channel_extract %{{.*}} {channels = [0 : i32, 2 : i32]} : tensor<3x200x100xsi32> -> tensor<2x200x100xsi32>
        %res4 = vosa.channel_extract %c0_si32 { channels = [ 0 : i32, 2 : i32 ] } : tensor<3x200x100xsi32> -> tensor<2x200x100xsi32>

        %c0_f16 = vosa.constant dense<1.0> : tensor<3x200x100xf16>
        // CHECK: %{{.*}} = vosa.channel_extract %{{.*}} {channels = [0 : i32]} : tensor<3x200x100xf16> -> tensor<1x200x100xf16>
        %res5 = vosa.channel_extract %c0_f16 { channels = [ 0 : i32 ] } : tensor<3x200x100xf16> -> tensor<1x200x100xf16>

        %c0_f32 = vosa.constant dense<1.0> : tensor<3x200x100xf32>
        // CHECK: %{{.*}} = vosa.channel_extract %{{.*}} {channels = [1 : i32]} : tensor<3x200x100xf32> -> tensor<1x200x100xf32>
        %res6 = vosa.channel_extract %c0_f32 { channels = [ 1 : i32 ] } : tensor<3x200x100xf32> -> tensor<1x200x100xf32>

        return
    }
}

// -----

func.func @channel_extract_bad_sz() {
    %c0_si8 = vosa.constant dense<1> : tensor<3x200x100xsi8>

    // expected-error @below {{'vosa.channel_extract' op input size != output size}}
    %res0 = vosa.channel_extract %c0_si8 { channels = [ 0 : i32 ] } : tensor<3x200x100xsi8> -> tensor<1x200x101xsi8>
    return
}

// -----

func.func @channel_extract_bad_out_ty() {
    %c0_si8 = vosa.constant dense<1> : tensor<3x200x100xsi8>

    // expected-error @below {{'vosa.channel_extract' op output type does not match input}}
    %res0 = vosa.channel_extract %c0_si8 { channels = [ 0 : i32 ] } : tensor<3x200x100xsi8> -> tensor<1x200x100xsi32>
    return
}

// -----

func.func @channel_extract_bad_channel_a() {
    %c0_si8 = vosa.constant dense<1> : tensor<3x200x100xsi8>

    // expected-error @below {{'vosa.channel_extract' op invalid channel index}}
    %res0 = vosa.channel_extract %c0_si8 { channels = [ 3 : i32 ] } : tensor<3x200x100xsi8> -> tensor<1x200x100xsi8>
    return
}

// -----

func.func @channel_extract_bad_channel_a() {
    %c0_si8 = vosa.constant dense<1> : tensor<3x200x100xsi8>

    // expected-error @below {{'vosa.channel_extract' op invalid channel index}}
    %res0 = vosa.channel_extract %c0_si8 { channels = [ -1 : i32 ] } : tensor<3x200x100xsi8> -> tensor<1x200x100xsi8>
    return
}

// -----

func.func @channel_extract_bad_channel_c() {
    %c0_si8 = vosa.constant dense<1> : tensor<3x200x100xsi8>

    // expected-error @below {{'vosa.channel_extract' op invalid channel index}}
    %res0 = vosa.channel_extract %c0_si8 { channels = [ 0 : i32, 3 : i32 ] } : tensor<3x200x100xsi8> -> tensor<2x200x100xsi8>
    return
}

// -----

module {
    // CHECK-LABEL: func.func @concat()
    func.func @concat() {

        %c1_si8 = vosa.constant dense<1> : tensor<1x100x200xsi8>
        %c2_si8 = vosa.constant dense<1> : tensor<2x100x200xsi8>
        %c3_si8 = vosa.constant dense<1> : tensor<3x100x200xsi8>

        // CHECK: %{{.*}} = vosa.concat %{{.*}} : tensor<3x100x200xsi8> -> tensor<3x100x200xsi8>
        %res0 = vosa.concat %c3_si8 : tensor<3x100x200xsi8> -> tensor<3x100x200xsi8>

        // CHECK: %{{.*}} = vosa.concat %{{.*}}, %{{.*}} : tensor<1x100x200xsi8>, tensor<2x100x200xsi8> -> tensor<3x100x200xsi8>
        %res1 = vosa.concat %c1_si8, %c2_si8 : tensor<1x100x200xsi8>, tensor<2x100x200xsi8> -> tensor<3x100x200xsi8>

        // CHECK: %{{.*}} = vosa.concat %{{.*}}, %{{.*}}, %{{.*}} : tensor<1x100x200xsi8>, tensor<2x100x200xsi8>, tensor<3x100x200xsi8> -> tensor<6x100x200xsi8>
        %res2 = vosa.concat %c1_si8, %c2_si8, %c3_si8 : tensor<1x100x200xsi8>, tensor<2x100x200xsi8>, tensor<3x100x200xsi8> -> tensor<6x100x200xsi8>

        %c1_si32 = vosa.constant dense<1> : tensor<1x100x200xsi32>
        %c2_si32 = vosa.constant dense<1> : tensor<2x100x200xsi32>
        %c3_si32 = vosa.constant dense<1> : tensor<3x100x200xsi32>

        // CHECK: %{{.*}} = vosa.concat %{{.*}} : tensor<3x100x200xsi32> -> tensor<3x100x200xsi32>
        %res3 = vosa.concat %c3_si32 : tensor<3x100x200xsi32> -> tensor<3x100x200xsi32>

        // CHECK: %{{.*}} = vosa.concat %{{.*}}, %{{.*}} : tensor<1x100x200xsi32>, tensor<2x100x200xsi32> -> tensor<3x100x200xsi32>
        %res4 = vosa.concat %c1_si32, %c2_si32 : tensor<1x100x200xsi32>, tensor<2x100x200xsi32> -> tensor<3x100x200xsi32>

        // CHECK: %{{.*}} = vosa.concat %{{.*}}, %{{.*}}, %{{.*}} : tensor<1x100x200xsi32>, tensor<2x100x200xsi32>, tensor<3x100x200xsi32> -> tensor<6x100x200xsi32>
        %res5 = vosa.concat %c1_si32, %c2_si32, %c3_si32 : tensor<1x100x200xsi32>, tensor<2x100x200xsi32>, tensor<3x100x200xsi32> -> tensor<6x100x200xsi32>

        %c1_f32 = vosa.constant dense<1.0> : tensor<1x100x200xf32>
        %c2_f32 = vosa.constant dense<1.0> : tensor<2x100x200xf32>
        %c3_f32 = vosa.constant dense<1.0> : tensor<3x100x200xf32>

        // CHECK: %{{.*}} = vosa.concat %{{.*}} : tensor<3x100x200xf32> -> tensor<3x100x200xf32>
        %res6 = vosa.concat %c3_f32 : tensor<3x100x200xf32> -> tensor<3x100x200xf32>

        // CHECK: %{{.*}} = vosa.concat %{{.*}}, %{{.*}} : tensor<1x100x200xf32>, tensor<2x100x200xf32> -> tensor<3x100x200xf32>
        %res7 = vosa.concat %c1_f32, %c2_f32 : tensor<1x100x200xf32>, tensor<2x100x200xf32> -> tensor<3x100x200xf32>

        // CHECK: %{{.*}} = vosa.concat %{{.*}}, %{{.*}}, %{{.*}} : tensor<1x100x200xf32>, tensor<2x100x200xf32>, tensor<3x100x200xf32> -> tensor<6x100x200xf32>
        %res8 = vosa.concat %c1_f32, %c2_f32, %c3_f32 : tensor<1x100x200xf32>, tensor<2x100x200xf32>, tensor<3x100x200xf32> -> tensor<6x100x200xf32>

        return
    }
}

// -----

module {
    func.func @concat_bad_sz() {

        %c1_si8 = vosa.constant dense<1> : tensor<1x100x200xsi8>
        %c2_si8 = vosa.constant dense<1> : tensor<2x101x200xsi8>

        // expected-error @below {{'vosa.concat' op input size != output size}}
        %res1 = vosa.concat %c1_si8, %c2_si8 : tensor<1x100x200xsi8>, tensor<2x101x200xsi8> -> tensor<3x100x200xsi8>

        return
    }
}

// -----

module {
    func.func @concat_bad_ty() {

        %c1_si8 = vosa.constant dense<1> : tensor<1x100x200xsi8>
        %c2_si32 = vosa.constant dense<1> : tensor<2x100x200xsi32>

        // expected-error @below {{'vosa.concat' op output type does not match input}}
        %res1 = vosa.concat %c1_si8, %c2_si32 : tensor<1x100x200xsi8>, tensor<2x100x200xsi32> -> tensor<3x100x200xsi8>

        return
    }
}

// -----

module {
    func.func @concat_bad_ch() {

        %c1_si8 = vosa.constant dense<1> : tensor<1x100x200xsi8>
        %c2_si8 = vosa.constant dense<1> : tensor<2x100x200xsi8>

        // expected-error @below {{'vosa.concat' op output channel count does not match total input channel count}}
        %res1 = vosa.concat %c1_si8, %c2_si8 : tensor<1x100x200xsi8>, tensor<2x100x200xsi8> -> tensor<4x100x200xsi8>

        return
    }
}

// -----

module {
    func.func @export_channel(%d_20000_si8: tensor<20000xsi8>, %d_40122_si8: tensor<40122xsi8>, %d_20000_ui32: tensor<20000xui32>, %d_40122_ui32: tensor<40122xui32>, %d_40123_ui32: tensor<40123xui32>, %d_20000_f16: tensor<20000xf16>, %d_40122_f16: tensor<40122xf16>) {
        %c1_si8 = vosa.constant dense<1> : tensor<1x100x200xsi8>

        // CHECK: %{{.*}} = vosa.export_channel %{{.*}}, %{{.*}} {offset = 0 : i32, stride = 1 : i32} : tensor<1x100x200xsi8>, tensor<20000xsi8> -> tensor<20000xsi8>
        %res_si8 = vosa.export_channel %c1_si8, %d_20000_si8 { stride = 1 : i32, offset = 0 : i32 } : tensor<1x100x200xsi8>, tensor<20000xsi8> -> tensor<20000xsi8>

        // CHECK: %{{.*}} = vosa.export_channel %{{.*}}, %{{.*}} {offset = 123 : i32, stride = 2 : i32} : tensor<1x100x200xsi8>, tensor<40122xsi8> -> tensor<40122xsi8>
        %res_si8b = vosa.export_channel %c1_si8, %d_40122_si8 { stride = 2 : i32, offset = 123 : i32 } : tensor<1x100x200xsi8>, tensor<40122xsi8> -> tensor<40122xsi8>

        %c1_ui32 = vosa.constant dense<1> : tensor<1x100x200xui32>

        // CHECK: %{{.*}} = vosa.export_channel %{{.*}}, %{{.*}} {offset = 0 : i32, stride = 4 : i32} : tensor<1x100x200xui32>, tensor<20000xui32> -> tensor<20000xui32>
        %res_ui32 = vosa.export_channel %c1_ui32, %d_20000_ui32 { stride = 4 : i32, offset = 0 : i32 } : tensor<1x100x200xui32>, tensor<20000xui32> -> tensor<20000xui32>

        // CHECK: %{{.*}} = vosa.export_channel %{{.*}}, %{{.*}} {offset = 492 : i32, stride = 8 : i32} : tensor<1x100x200xui32>, tensor<40122xui32> -> tensor<40122xui32>
        %res_ui32b = vosa.export_channel %c1_ui32, %d_40122_ui32 { stride = 8 : i32, offset = 492 : i32 } : tensor<1x100x200xui32>, tensor<40122xui32> -> tensor<40122xui32>

        // can export into larger destination tensor
        // CHECK: %{{.*}} = vosa.export_channel %{{.*}}, %{{.*}} {offset = 492 : i32, stride = 8 : i32} : tensor<1x100x200xui32>, tensor<40123xui32> -> tensor<40123xui32>
        %res_ui32c = vosa.export_channel %c1_ui32, %d_40123_ui32 { stride = 8 : i32, offset = 492 : i32 } : tensor<1x100x200xui32>, tensor<40123xui32> -> tensor<40123xui32>

        %c1_f16 = vosa.constant dense<1.0> : tensor<1x100x200xf16>

        // CHECK: %{{.*}} = vosa.export_channel %{{.*}}, %{{.*}} {offset = 0 : i32, stride = 2 : i32} : tensor<1x100x200xf16>, tensor<20000xf16> -> tensor<20000xf16>
        %res_f16 = vosa.export_channel %c1_f16, %d_20000_f16 { stride = 2 : i32, offset = 0 : i32 } : tensor<1x100x200xf16>, tensor<20000xf16> -> tensor<20000xf16>

        // CHECK: %{{.*}} = vosa.export_channel %{{.*}}, %{{.*}} {offset = 246 : i32, stride = 4 : i32} : tensor<1x100x200xf16>, tensor<40122xf16> -> tensor<40122xf16>
        %res_f16b = vosa.export_channel %c1_f16, %d_40122_f16 { stride = 4 : i32, offset = 246 : i32 } : tensor<1x100x200xf16>, tensor<40122xf16> -> tensor<40122xf16>

        return
    }
}

// -----

module {
    func.func @export_channel_bad_ty(%d_20000_ui8: tensor<20000xui8>) {
        %c1_si8 = vosa.constant dense<1> : tensor<1x100x200xsi8>
        // expected-error @below {{'vosa.export_channel' op output element type does not match input}}
        %res_si8 = vosa.export_channel %c1_si8, %d_20000_ui8 { stride = 1 : i32, offset = 0 : i32 } : tensor<1x100x200xsi8>, tensor<20000xui8> -> tensor<20000xui8>

        return
    }
}

// -----

module {
    func.func @export_channel_bad_outsz(%d_19999_si8: tensor<19999xsi8>) {
        %c1_si8 = vosa.constant dense<1> : tensor<1x100x200xsi8>
        // expected-error @below {{'vosa.export_channel' op invalid output size}}
        %res_si8 = vosa.export_channel %c1_si8, %d_19999_si8 { stride = 1 : i32, offset = 0 : i32 } : tensor<1x100x200xsi8>, tensor<19999xsi8> -> tensor<19999xsi8>

        return
    }
}

// -----

module {
    func.func @export_channel_bad_outsz2(%d_59999_si8: tensor<59999xsi8>) {
        %c1_si8 = vosa.constant dense<1> : tensor<1x100x200xsi8>
        // expected-error @below {{'vosa.export_channel' op invalid output size}}
        %res_si8 = vosa.export_channel %c1_si8, %d_59999_si8 { stride = 3 : i32, offset = 2 : i32 } : tensor<1x100x200xsi8>, tensor<59999xsi8> -> tensor<59999xsi8>

        return
    }
}

// -----

module {
    func.func @import_channel() {
        %c1_si8 = vosa.constant dense<1> : tensor<20000xsi8>
        %c1_si8b = vosa.constant dense<1> : tensor<40000xsi8>
        %c1_si8c = vosa.constant dense<1> : tensor<40122xsi8>

        // CHECK: %{{.*}} = vosa.import_channel %{{.*}} {offset = 0 : i32, shape = [100 : i32, 200 : i32], stride = 1 : i32} : tensor<20000xsi8> -> tensor<1x100x200xsi8>
        %res_si8 = vosa.import_channel %c1_si8 { stride = 1 : i32, offset = 0 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<20000xsi8> -> tensor<1x100x200xsi8>

        // CHECK: %{{.*}} = vosa.import_channel %{{.*}} {offset = 1 : i32, shape = [100 : i32, 200 : i32], stride = 2 : i32} : tensor<40000xsi8> -> tensor<1x100x200xsi8>
        %res_si8c = vosa.import_channel %c1_si8b { stride = 2 : i32, offset = 1 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<40000xsi8> -> tensor<1x100x200xsi8>

        // can import from larger tensor
        // CHECK: %{{.*}} = vosa.import_channel %{{.*}} {offset = 123 : i32, shape = [100 : i32, 200 : i32], stride = 2 : i32} : tensor<40122xsi8> -> tensor<1x100x200xsi8>
        %res_si8b = vosa.import_channel %c1_si8c { stride = 2 : i32, offset = 123 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<40122xsi8> -> tensor<1x100x200xsi8>

        %c1_ui32 = vosa.constant dense<1> : tensor<20000xui32>
        %c1_ui32b = vosa.constant dense<1> : tensor<40122xui32>

        // CHECK: %{{.*}} = vosa.import_channel %{{.*}} {offset = 0 : i32, shape = [100 : i32, 200 : i32], stride = 4 : i32} : tensor<20000xui32> -> tensor<1x100x200xui32>
        %res_ui32 = vosa.import_channel %c1_ui32 { stride = 4 : i32, offset = 0 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<20000xui32> -> tensor<1x100x200xui32>

        // CHECK: %{{.*}} = vosa.import_channel %{{.*}} {offset = 492 : i32, shape = [100 : i32, 200 : i32], stride = 8 : i32} : tensor<40122xui32> -> tensor<1x100x200xui32>
        %res_ui32b = vosa.import_channel %c1_ui32b { stride = 8 : i32, offset = 492 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<40122xui32> -> tensor<1x100x200xui32>

        %c1_f16 = vosa.constant dense<1.0> : tensor<20000xf16>
        %c1_f16b = vosa.constant dense<1.0> : tensor<40122xf16>

        // CHECK: %{{.*}} = vosa.import_channel %{{.*}} {offset = 0 : i32, shape = [100 : i32, 200 : i32], stride = 2 : i32} : tensor<20000xf16> -> tensor<1x100x200xf16>
        %res_f16 = vosa.import_channel %c1_f16 { stride = 2 : i32, offset = 0 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<20000xf16> -> tensor<1x100x200xf16>

        // CHECK: %{{.*}} = vosa.import_channel %{{.*}} {offset = 246 : i32, shape = [100 : i32, 200 : i32], stride = 4 : i32} : tensor<40122xf16> -> tensor<1x100x200xf16>
        %res_f16b = vosa.import_channel %c1_f16b { stride = 4 : i32, offset = 246 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<40122xf16> -> tensor<1x100x200xf16>

        return
    }
}

// -----

module {
    func.func @import_channel_bad_ty() {
        %c1_si8 = vosa.constant dense<1> : tensor<20000xsi8>
        // expected-error @below {{'vosa.import_channel' op output element type does not match input}}
        %res_si8 = vosa.import_channel %c1_si8 { stride = 1 : i32, offset = 0 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<20000xsi8> -> tensor<1x100x200xui8>

        return
    }
}

// -----

module {
    func.func @import_channel_bad_sz() {
        %c1_si8 = vosa.constant dense<1> : tensor<19999xsi8>
        // expected-error @below {{'vosa.import_channel' op invalid input size}}
        %res_si8 = vosa.import_channel %c1_si8 { stride = 1 : i32, offset = 0 : i32, shape = [ 100 : i32, 200 : i32 ] } : tensor<19999xsi8> -> tensor<1x100x200xsi8>

        return
    }
}

// -----

module {
    func.func @import_channel_bad_outsz2() {
        %c1_si8 = vosa.constant dense<1> : tensor<20099xsi8>
        // expected-error @below {{'vosa.import_channel' op output shape does not match shape attr}}
        %res_si8 = vosa.import_channel %c1_si8 { stride = 1 : i32, offset = 0 : i32, shape = [ 100 : i32, 201 : i32 ] } : tensor<20099xsi8> -> tensor<1x100x200xsi8>

        return
    }
}

// -----

module {
    func.func @mesh_grid() {
        // CHECK: %{{.*}} = vosa.mesh_grid {shape = [100 : i32, 200 : i32]} : tensor<2x100x200xui32>
        %res = vosa.mesh_grid {shape = [ 100 : i32, 200 : i32 ]} : tensor<2x100x200xui32>

        return
    }
}

// -----

module {
    func.func @mesh_grid_bad_ty_si32() {
        // expected-error @below {{'vosa.mesh_grid' op result #0 must be}}
        %res = vosa.mesh_grid { shape = [ 100 : i32, 200 : i32 ] } : tensor<2x100x200xsi32>
        
        return
    }
}

// -----

module {
    func.func @piecewise_linear() {
        %t0_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // check: %{{.*}} = vosa.piecewise_linear %{{.*}} {nodes = dense<{{.*}}> : tensor<3xsi8>, values = dense<{{.*}}> : tensor<3xsi8>} : tensor<3x100x200xsi8> -> tensor<3x100x200xsi8>
        %res_si8 = vosa.piecewise_linear %t0_si8 { values = dense<[10, 22, 53]> : tensor<3xsi8>, nodes = dense<[1, 2, 3]> : tensor<3xsi8> } : tensor<3x100x200xsi8> -> tensor<3x100x200xsi8>

        %t0_si16 = vosa.constant dense<42> : tensor<3x100x200xsi16>
        // CHECK: %{{.*}} = vosa.piecewise_linear %{{.*}} {nodes = dense<{{.*}}> : tensor<3xsi16>, values = dense<{{.*}}> : tensor<3xsi16>} : tensor<3x100x200xsi16> -> tensor<3x100x200xsi16>
        %res_si16 = vosa.piecewise_linear %t0_si16 { values = dense<[10, 22, 53]> : tensor<3xsi16>, nodes = dense<[1, 2, 3]> : tensor<3xsi16> } : tensor<3x100x200xsi16> -> tensor<3x100x200xsi16>

        %t0_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.piecewise_linear %{{.*}} {nodes = dense<{{.*}}> : tensor<3xsi32>, values = dense<{{.*}}> : tensor<3xsi32>} : tensor<3x100x200xsi32> -> tensor<3x100x200xsi32>
        %res_si32 = vosa.piecewise_linear %t0_si32 { values = dense<[10, 22, 53]> : tensor<3xsi32>, nodes = dense<[1, 2, 3]> : tensor<3xsi32> } : tensor<3x100x200xsi32> -> tensor<3x100x200xsi32>

        %t0_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // CHECK: %{{.*}} = vosa.piecewise_linear %{{.*}} {nodes = dense<{{.*}}> : tensor<3xf16>, values = dense<{{.*}}> : tensor<3xf16>} : tensor<3x100x200xf16> -> tensor<3x100x200xf16>
        %res_f16 = vosa.piecewise_linear %t0_f16 {nodes=dense<[1.0,2.0,3.0]> : tensor<3xf16>, values=dense<[10.0, 22.0, 53.0]> : tensor<3xf16>} : tensor<3x100x200xf16> -> tensor<3x100x200xf16>

        %t0_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.piecewise_linear %{{.*}} {nodes = dense<{{.*}}> : tensor<3xf32>, values = dense<{{.*}}> : tensor<3xf32>} : tensor<3x100x200xf32> -> tensor<3x100x200xf32>
        %res_f32 = vosa.piecewise_linear %t0_f32 {nodes=dense<[1.0,2.0,3.0]> : tensor<3xf32>, values=dense<[10.0, 22.0, 53.0]> : tensor<3xf32>} : tensor<3x100x200xf32> -> tensor<3x100x200xf32>

        return
    }
}

// -----

module {
   func.func @piecewise_linear_type_mismatch() {
        %t0_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.piecewise_linear' op output type does not match input}}
        %res_si8 = vosa.piecewise_linear %t0_si8 { values = dense<[10, 22, 53]> : tensor<3xsi8>, nodes = dense<[1, 2, 3]> : tensor<3xsi8> } : tensor<3x100x200xsi8> -> tensor<3x100x200xsi16>

        return
   }
}

// -----

module {
    func.func @piecewise_linear_attribute_mismatch() { 
        %t0_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.piecewise_linear' op attributes do not have matching sizes}}
        %res_si8 = vosa.piecewise_linear %t0_si8 { values = dense<[10, 22]> : tensor<2xsi8>, nodes = dense<[1, 2, 3]> : tensor<3xsi8> } : tensor<3x100x200xsi8> -> tensor<3x100x200xsi8>

        return
    }
}

// -----

module {
    func.func @piecewise_linear_type_mismatch() {
        %t0_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.piecewise_linear' op attributes and input element types do not match}}
        %res_si8 = vosa.piecewise_linear %t0_si8 { values = dense<[10, 22, 53]> : tensor<3xsi16>, nodes = dense<[1, 2, 3]> : tensor<3xsi16> } : tensor<3x100x200xsi8> -> tensor<3x100x200xsi8>

        return
    }
}
