/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "arithmetic_shift_right.h"

std::shared_ptr<vosa::Op> vosa::ArithmeticShiftRightBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_arithmetic_shift_right =
      vosa_operator_union.AsARITHMETIC_SHIFT_RIGHT();
  assert(serialized_arithmetic_shift_right);
  auto mode = serialized_arithmetic_shift_right->mode;
  auto serialized_attributes = &serialized_arithmetic_shift_right->attr;
  uint32_t shift = serialized_attributes->get()->shift;
  switch (mode) {
  case vosa_serialization::ARITHMETIC_SHIFT_RIGHT_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::ArithmeticShiftRight<int8_t>>(shift);
  }
  case vosa_serialization::ARITHMETIC_SHIFT_RIGHT_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::ArithmeticShiftRight<int16_t>>(shift);
  }
  case vosa_serialization::ARITHMETIC_SHIFT_RIGHT_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::ArithmeticShiftRight<int32_t>>(shift);
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::ArithmeticShiftRight<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::ARITHMETIC_SHIFT_RIGHT_MODE
vosa::ArithmeticShiftRight<int8_t>::get_serialization_mode() const {
  return vosa_serialization::ARITHMETIC_SHIFT_RIGHT_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::ARITHMETIC_SHIFT_RIGHT_MODE
vosa::ArithmeticShiftRight<int16_t>::get_serialization_mode() const {
  return vosa_serialization::ARITHMETIC_SHIFT_RIGHT_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::ARITHMETIC_SHIFT_RIGHT_MODE
vosa::ArithmeticShiftRight<int32_t>::get_serialization_mode() const {
  return vosa_serialization::ARITHMETIC_SHIFT_RIGHT_MODE_I_I_INT32_INT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::ArithmeticShiftRight<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {
  auto arithmetic_shift_right_attribute =
      std::make_unique<vosa_serialization::ARITHMETIC_SHIFT_RIGHT_ATTRIBUTET>();
  arithmetic_shift_right_attribute->shift = shift;

  vosa_serialization::ARITHMETIC_SHIFT_RIGHTT artihmetic_shift_right;
  artihmetic_shift_right.mode = get_serialization_mode();
  artihmetic_shift_right.attr = std::move(arithmetic_shift_right_attribute);

  auto arithmetic_shift_right_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  arithmetic_shift_right_operator->op.Set(std::move(artihmetic_shift_right));
  arithmetic_shift_right_operator->name = op_name;
  arithmetic_shift_right_operator->inputs = input_names;
  return arithmetic_shift_right_operator;
}

template <typename T>
vosa::EigenImage<T>
vosa::ArithmeticShiftRight<T>::runme(const vosa::EigenImage<T> &input) const {

  uint32_t input_height = input.dimension(0);
  uint32_t input_width = input.dimension(1);
  uint32_t num_channels = input.dimension(2);

  auto output = vosa::EigenImage<T>(input_height, input_width, num_channels);

  for (uint32_t height_index = 0; height_index < input_height; height_index++) {
    for (uint32_t width_index = 0; width_index < input_width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < num_channels;
           channel_index++) {
        output(height_index, width_index, channel_index) =
            input(height_index, width_index, channel_index) >> shift;
      }
    }
  }
  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::ArithmeticShiftRight<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template class vosa::ArithmeticShiftRight<int8_t>;

template class vosa::ArithmeticShiftRight<int16_t>;

template class vosa::ArithmeticShiftRight<int32_t>;