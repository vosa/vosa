/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "arg_min_planewise.h"

std::shared_ptr<vosa::Op> vosa::ArgMinPlanewiseBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_arg_min_planewise = vosa_operator_union.AsARG_MIN_PLANEWISE();
  assert(serialized_arg_min_planewise);
  auto mode = serialized_arg_min_planewise->mode;
  switch (mode) {
  case vosa_serialization::ARG_MIN_PLANEWISE_MODE_I_V_UINT8_UINT32: {
    return std::make_shared<vosa::ArgMinPlanewise<uint8_t>>();
  }
  case vosa_serialization::ARG_MIN_PLANEWISE_MODE_I_V_UINT16_UINT32: {
    return std::make_shared<vosa::ArgMinPlanewise<uint16_t>>();
  }
  case vosa_serialization::ARG_MIN_PLANEWISE_MODE_I_V_UINT32_UINT32: {
    return std::make_shared<vosa::ArgMinPlanewise<uint32_t>>();
  }
  case vosa_serialization::ARG_MIN_PLANEWISE_MODE_I_V_INT8_UINT32: {
    return std::make_shared<vosa::ArgMinPlanewise<int8_t>>();
  }
  case vosa_serialization::ARG_MIN_PLANEWISE_MODE_I_V_INT16_UINT32: {
    return std::make_shared<vosa::ArgMinPlanewise<int16_t>>();
  }
  case vosa_serialization::ARG_MIN_PLANEWISE_MODE_I_V_INT32_UINT32: {
    return std::make_shared<vosa::ArgMinPlanewise<int16_t>>();
  }
  case vosa_serialization::ARG_MIN_PLANEWISE_MODE_I_V_FLOAT32_UINT32: {
    return std::make_shared<vosa::ArgMinPlanewise<float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <>
vosa_serialization::ARG_MIN_PLANEWISE_MODE
vosa::ArgMinPlanewise<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::ARG_MIN_PLANEWISE_MODE_I_V_UINT8_UINT32;
}

template <>
vosa_serialization::ARG_MIN_PLANEWISE_MODE
vosa::ArgMinPlanewise<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::ARG_MIN_PLANEWISE_MODE_I_V_UINT16_UINT32;
}

template <>
vosa_serialization::ARG_MIN_PLANEWISE_MODE
vosa::ArgMinPlanewise<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::ARG_MIN_PLANEWISE_MODE_I_V_UINT32_UINT32;
}

template <>
vosa_serialization::ARG_MIN_PLANEWISE_MODE
vosa::ArgMinPlanewise<int8_t>::get_serialization_mode() const {
  return vosa_serialization::ARG_MIN_PLANEWISE_MODE_I_V_INT8_UINT32;
}

template <>
vosa_serialization::ARG_MIN_PLANEWISE_MODE
vosa::ArgMinPlanewise<int16_t>::get_serialization_mode() const {
  return vosa_serialization::ARG_MIN_PLANEWISE_MODE_I_V_INT16_UINT32;
}

template <>
vosa_serialization::ARG_MIN_PLANEWISE_MODE
vosa::ArgMinPlanewise<int32_t>::get_serialization_mode() const {
  return vosa_serialization::ARG_MIN_PLANEWISE_MODE_I_V_INT32_UINT32;
}

template <>
vosa_serialization::ARG_MIN_PLANEWISE_MODE
vosa::ArgMinPlanewise<float>::get_serialization_mode() const {
  return vosa_serialization::ARG_MIN_PLANEWISE_MODE_I_V_FLOAT32_UINT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::ArgMinPlanewise<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {
  vosa_serialization::ARG_MIN_PLANEWISET arg_min_planewise;
  arg_min_planewise.mode = get_serialization_mode();

  auto arg_min_planewise_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  arg_min_planewise_operator->op.Set(arg_min_planewise);
  arg_min_planewise_operator->name = op_name;
  arg_min_planewise_operator->inputs = input_names;
  return arg_min_planewise_operator;
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::ArgMinPlanewise<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();

  assert(input.dimension(2) == 1);
  const uint32_t height = input.dimension(0);
  const uint32_t width = input.dimension(1);

  T min_value = input(0, 0, 0);
  auto arg_min = EigenVector<uint32_t>(2);
  arg_min.setValues({0, 0});
  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      if (min_value > input(y, x, 0)) {
        min_value = input(y, x, 0);
        arg_min.setValues({y, x});
      }

  return std::make_shared<vosa::ArrayBase>(arg_min);
}

template <typename T>
vosa::ArrayBase::DType vosa::ArgMinPlanewise<T>::output_dtype() const {
  return vosa::ArrayBase::UINT32;
}

template class vosa::ArgMinPlanewise<uint8_t>;

template class vosa::ArgMinPlanewise<uint16_t>;

template class vosa::ArgMinPlanewise<uint32_t>;

template class vosa::ArgMinPlanewise<int8_t>;

template class vosa::ArgMinPlanewise<int16_t>;

template class vosa::ArgMinPlanewise<int32_t>;

template class vosa::ArgMinPlanewise<float>;
