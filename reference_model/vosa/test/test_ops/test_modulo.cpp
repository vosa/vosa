/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/mod.h"
#include "test_bench.h"
#include "vosa/vosa.h"

TEST(test_modulo, test_modulo) {

  vosa::EigenImage<uint32_t> input_0(1, 3, 3);
  vosa::EigenImage<uint32_t> input_1(1, 3, 3);
  vosa::EigenImage<uint32_t> expected_output(1, 3, 3);

  input_0.setValues({{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}});
  input_1.setValues({{{2, 2, 2}, {3, 3, 3}, {4, 4, 4}}});
  expected_output.setValues({{{1, 0, 1}, {1, 2, 0}, {3, 0, 1}}});

  auto modulo = vosa::Mod<uint32_t>();

  auto output = modulo.eval({std::make_shared<vosa::ArrayBase>(input_0),
                             std::make_shared<vosa::ArrayBase>(input_1)});

  auto typed_output = output->get_image<uint32_t>();
  vosa::test::TestBench<uint32_t, int32_t>::compareEigenImages(typed_output,
                                                               expected_output);
}
