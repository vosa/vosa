/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_GRAPH_BUILDER_H
#define VOSA_GRAPH_BUILDER_H

#include "vosa/Builder/mlir_builder.h"

#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/Dialect/Tosa/IR/TosaOps.h"

#include "vosa/api/op.h"

namespace vosa::api {

class GraphBuilder {
private:
  mlir::MLIRContext context;
  mlir::OwningOpRef<mlir::ModuleOp> customOps;
  std::string pipeline_name;
  vosa::MLIRBuilder mlir_builder;

  uint32_t op_count = 0;
  std::unordered_map<std::string, std::shared_ptr<Op>> ops;

  uint32_t tosa_graph_count = 0;

public:
  GraphBuilder(const std::string &infile, const std::string &pipeline_name)
      : context(),
        customOps(mlir::ModuleOp::create(mlir::UnknownLoc::get(&context))),
        pipeline_name(pipeline_name),
        mlir_builder(context, infile, pipeline_name, customOps) {

    context.getOrLoadDialect<mlir::vosa::VosaDialect>();
    context.getOrLoadDialect<mlir::func::FuncDialect>();
    context.getOrLoadDialect<mlir::tensor::TensorDialect>();
    context.getOrLoadDialect<mlir::tosa::TosaDialect>();
  }
  //    void register_custom_ops(mlir::OwningOpRef<mlir::ModuleOp> &custom_ops);
  vosa::MLIRBuilder &get_mlir_builder() { return mlir_builder; }
  mlir::MLIRContext &get_context() { return context; }

  std::string get_name_for_tosa_graph() {
    std::string name = "tosa_graph_" + std::to_string(tosa_graph_count);
    tosa_graph_count++;
    return name;
  }

  mlir::OwningOpRef<mlir::ModuleOp> &get_custom_ops_reference() {
    return customOps;
  }
  std::string add_op(std::shared_ptr<vosa::api::Op> op);
  std::string get_name_for_op(const std::shared_ptr<vosa::api::Op> &op);

  bool compile(const std::vector<std::string> &outputs,
               const std::string &output_filename);
};

} // namespace vosa::api

#endif // VOSA_GRAPH_BUILDER_H
