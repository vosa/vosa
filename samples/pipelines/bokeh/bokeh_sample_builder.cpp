/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "bokeh.h"
#include "bokeh_custom.h"
#include "vosa/api/sample.h"

class BokehSample : public vosa::api::sample::SampleBuilder<BokehSample> {

public:
  int run(const std::string &pipeline_name,
          const std::string &vmfb_file) final {
    vosa::api::GraphBuilder graph_builder("", pipeline_name);

    vosa::samples::bokeh::populate_custom_ops(
        graph_builder.get_context(), graph_builder.get_custom_ops_reference());

    auto depth_map_input = vosa::api::input(
        graph_builder, vosa::api::ArrayBase::DType::UINT8, {540 * 960});
    auto binary_mask_input = vosa::api::input(
        graph_builder, vosa::api::ArrayBase::DType::UINT8, {540 * 960});
    auto uv_input = vosa::api::input(
        graph_builder, vosa::api::ArrayBase::DType::UINT8, {540 * 960 * 2});
    auto y_input = vosa::api::input(
        graph_builder, vosa::api::ArrayBase::DType::UINT8, {1920 * 1080});

    auto depth_map = vosa::api::import_channel(graph_builder, depth_map_input,
                                               1, 0, {540, 960});
    auto binary_mask = vosa::api::import_channel(
        graph_builder, binary_mask_input, 1, 0, {540, 960});

    auto u_channel =
        vosa::api::import_channel(graph_builder, uv_input, 2, 0, {540, 960});
    auto v_channel =
        vosa::api::import_channel(graph_builder, uv_input, 2, 1, {540, 960});
    auto uv = vosa::api::concat(graph_builder, {u_channel, v_channel});

    auto y =
        vosa::api::import_channel(graph_builder, y_input, 1, 0, {1080, 1920});

    auto outputs_ops = vosa::samples::bokeh::make_test_graph(
        graph_builder, depth_map, binary_mask, uv, y);
    std::vector<std::string> output_op_names = {
        graph_builder.get_name_for_op(outputs_ops[0]),
        graph_builder.get_name_for_op(outputs_ops[1]),
        graph_builder.get_name_for_op(outputs_ops[2])};
    return graph_builder.compile(output_op_names, vmfb_file);
  }
};

VOSA_SAMPLE(BokehSample)