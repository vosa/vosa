/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "piecewise_linear.h"

std::shared_ptr<vosa::Op> vosa::PiecewiseLinearBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_piecewise_linear = vosa_operator_union.AsPIECEWISE_LINEAR();
  assert(serialized_piecewise_linear);
  auto mode = serialized_piecewise_linear->mode;
  auto serialized_attributes = &serialized_piecewise_linear->attr;
  std::vector<uint8_t> raw_nodes = serialized_attributes->get()->nodes;
  std::vector<uint8_t> raw_values = serialized_attributes->get()->values;
  switch (mode) {
  case vosa_serialization::PIECEWISE_LINEAR_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::PiecewiseLinear<int8_t>>(
        unpack_vector<int8_t>(raw_nodes), unpack_vector<int8_t>(raw_values));
  }
  case vosa_serialization::PIECEWISE_LINEAR_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::PiecewiseLinear<int16_t>>(
        unpack_vector<int16_t>(raw_nodes), unpack_vector<int16_t>(raw_values));
  }
  case vosa_serialization::PIECEWISE_LINEAR_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::PiecewiseLinear<int32_t>>(
        unpack_vector<int32_t>(raw_nodes), unpack_vector<int32_t>(raw_values));
  }
  case vosa_serialization::PIECEWISE_LINEAR_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::PiecewiseLinear<float>>(
        unpack_vector<float>(raw_nodes), unpack_vector<float>(raw_values));
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

uint32_t calc_right_shift(uint32_t value) {
  if (value == 0)
    return 0;
  int shift = 0;
  while (value >>= 1)
    ++shift;
  return shift;
}

template <typename T>
T calc_quotient(T delta, T offset, T norm, bool) {
  T product = delta * offset;
  //    uint32_t shift = calc_right_shift(norm);
  T quotient = (product << 1);
  quotient = quotient / norm;
  //    quotient = quotient >> shift;
  quotient += 1;
  quotient = quotient >> 1;
  return quotient;
}

template <>
float calc_quotient(float delta, float offset, float norm, bool) {
  return delta * offset / norm;
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::PiecewiseLinear<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();

  uint32_t height = input.dimension(0);
  uint32_t width = input.dimension(1);
  uint32_t channels = input.dimension(2);

  auto image_output = vosa::EigenImage<T>(height, width, channels);

  uint32_t num_nodes = nodes.size();

  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < channels;
           channel_index++) {
        T input_value = input(height_index, width_index, channel_index);
        T output_value;
        if (input_value <= nodes(0)) {
          output_value = values(0);
        } else if (nodes(num_nodes - 1) < input_value) {
          output_value = values(num_nodes - 1);
        } else {
          bool found_interval = false;
          for (uint32_t node_index = 0; node_index < (num_nodes - 1);
               node_index++) {
            if ((nodes(node_index) < input_value) &&
                (input_value <= nodes(node_index + 1))) {

              T norm = nodes(node_index + 1) - nodes(node_index);
              T offset = input_value - nodes(node_index);

              bool increasing = values(node_index + 1) > values(node_index);

              T delta;

              delta = values(node_index + 1) - values(node_index);
              //                            else
              //                                delta = values(node_index) -
              //                                values(node_index+1);

              T segment_start = values(node_index);
              T quotient = calc_quotient(delta, offset, norm, increasing);

              //                            if (increasing)
              output_value = segment_start + quotient;
              //                            else
              //                                output_value = segment_start -
              //                                quotient;
              found_interval = true;
              break;
            }
          }
          if (!found_interval) {
            throw std::runtime_error("");
          }
        }
        image_output(height_index, width_index, channel_index) = output_value;
      }
    }
  }

  auto output = std::make_shared<vosa::ArrayBase>(image_output);
  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::PiecewiseLinear<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <>
vosa_serialization::PIECEWISE_LINEAR_MODE
vosa::PiecewiseLinear<int8_t>::get_serialization_mode() const {
  return vosa_serialization::PIECEWISE_LINEAR_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::PIECEWISE_LINEAR_MODE
vosa::PiecewiseLinear<int16_t>::get_serialization_mode() const {
  return vosa_serialization::PIECEWISE_LINEAR_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::PIECEWISE_LINEAR_MODE
vosa::PiecewiseLinear<int32_t>::get_serialization_mode() const {
  return vosa_serialization::PIECEWISE_LINEAR_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::PIECEWISE_LINEAR_MODE
vosa::PiecewiseLinear<float>::get_serialization_mode() const {
  return vosa_serialization::PIECEWISE_LINEAR_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::PiecewiseLinear<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {
  auto piecewise_linear_attribute =
      std::make_unique<vosa_serialization::PIECEWISE_LINEAR_ATTRIBUTET>();
  piecewise_linear_attribute->nodes = this->pack_vector(nodes);
  piecewise_linear_attribute->values = this->pack_vector(values);

  vosa_serialization::PIECEWISE_LINEART piecewise_linear;
  piecewise_linear.mode = get_serialization_mode();
  piecewise_linear.attr = std::move(piecewise_linear_attribute);

  auto piecewise_linear_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  piecewise_linear_operator->op.Set(std::move(piecewise_linear));
  piecewise_linear_operator->name = op_name;
  piecewise_linear_operator->inputs = input_names;
  return piecewise_linear_operator;
}

template class vosa::PiecewiseLinear<int8_t>;

template class vosa::PiecewiseLinear<int16_t>;

template class vosa::PiecewiseLinear<int32_t>;

template class vosa::PiecewiseLinear<float>;