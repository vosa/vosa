/* -*- c++ -*-
 *
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_TRANSFORMS_PASSDETAIL_H
#define VOSA_TRANSFORMS_PASSDETAIL_H

#include "mlir/IR/Dialect.h"
#include "mlir/Pass/Pass.h"

namespace mlir {

namespace func {
class FuncDialect;
} // namespace func
  //
namespace linalg {
class LinalgDialect;
} // end namespace linalg

namespace tensor {
class TensorDialect;
} // namespace tensor

namespace scf {
class SCFDialect;
} // namespace scf

namespace vosa {
class VosaDialect;
#define GEN_PASS_CLASSES
#include "vosa/Dialect/Vosa/Transforms/Passes.h.inc"
} // namespace vosa

} // namespace mlir

#endif // VOSA_TRANSFORMS_PASSDETAIL_H
