/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "pointwise_matrix_multiply.h"

std::shared_ptr<vosa::Op> vosa::PointwiseMatrixMultiplyBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialised_pointwise_matrix_multiply =
      vosa_operator_union.AsPOINTWISE_MATRIX_MULTIPLY();
  assert(serialised_pointwise_matrix_multiply);
  auto mode = serialised_pointwise_matrix_multiply->mode;
  auto serialized_attributes = &serialised_pointwise_matrix_multiply->attr;
  std::vector<uint8_t> K_1 = serialized_attributes->get()->K_1;
  std::vector<uint8_t> K_2 = serialized_attributes->get()->K_2;
  std::vector<uint8_t> M = serialized_attributes->get()->M;
  std::vector<uint32_t> M_dimensions =
      serialized_attributes->get()->M_dimensions;
  switch (mode) {
  case vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::PointwiseMatrixMultiply<uint8_t, uint8_t>>(
        unpack_plane<uint8_t>(M, M_dimensions), unpack_vector<uint8_t>(K_1),
        unpack_vector<uint8_t>(K_2));
  }
  case vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::PointwiseMatrixMultiply<uint16_t, uint16_t>>(
        unpack_plane<uint16_t>(M, M_dimensions), unpack_vector<uint16_t>(K_1),
        unpack_vector<uint16_t>(K_2));
  }
  case vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::PointwiseMatrixMultiply<uint32_t, uint32_t>>(
        unpack_plane<uint32_t>(M, M_dimensions), unpack_vector<uint32_t>(K_1),
        unpack_vector<uint32_t>(K_2));
  }
  case vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::PointwiseMatrixMultiply<int8_t, int8_t>>(
        unpack_plane<int8_t>(M, M_dimensions), unpack_vector<int8_t>(K_1),
        unpack_vector<int8_t>(K_2));
  }
  case vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::PointwiseMatrixMultiply<int16_t, int16_t>>(
        unpack_plane<int16_t>(M, M_dimensions), unpack_vector<int16_t>(K_1),
        unpack_vector<int16_t>(K_2));
  }
  case vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::PointwiseMatrixMultiply<int32_t, int32_t>>(
        unpack_plane<int32_t>(M, M_dimensions), unpack_vector<int32_t>(K_1),
        unpack_vector<int32_t>(K_2));
  }
  case vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::PointwiseMatrixMultiply<float, float>>(
        unpack_plane<float>(M, M_dimensions), unpack_vector<float>(K_1),
        unpack_vector<float>(K_2));
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename inputT, typename outputT>
vosa::ArrayBase::DType
vosa::PointwiseMatrixMultiply<inputT, outputT>::output_dtype() const {
  return output_dtype_helper<outputT>();
}

template <typename inputT, typename outputT>
std::shared_ptr<vosa::ArrayBase>
vosa::PointwiseMatrixMultiply<inputT, outputT>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {

  const EigenImage<inputT> &input = inputs[0]->template get_image<inputT>();

  uint32_t height = input.dimension(0);
  uint32_t width = input.dimension(1);
  uint32_t output_channels = matrix.dimension(0);
  uint32_t input_channels = input.dimension(2);
  assert(matrix.dimension(1) == input_channels);

  auto output_image = EigenImage<outputT>(height, width, output_channels);

  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      for (uint32_t output_channel_index = 0;
           output_channel_index < output_channels; output_channel_index++) {
        auto output_value = (outputT)outer_offsets(output_channel_index);
        for (uint32_t input_channel_index = 0;
             input_channel_index < input_channels; input_channel_index++) {
          outputT input_as_outputT =
              input(height_index, width_index, input_channel_index);
          outputT inner_offset_as_outputT = inner_offsets(input_channel_index);
          outputT offset_input = input_as_outputT - inner_offset_as_outputT;

          outputT matrix_value =
              matrix(output_channel_index, input_channel_index);
          output_value += matrix_value * offset_input;
        }
        output_image(height_index, width_index, output_channel_index) =
            output_value;
      }
    }
  }

  auto output = std::make_shared<vosa::ArrayBase>(output_image);
  return output;
}

template <>
vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE
vosa::PointwiseMatrixMultiply<uint8_t, uint8_t>::get_serialization_mode()
    const {
  return vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE
vosa::PointwiseMatrixMultiply<uint16_t, uint16_t>::get_serialization_mode()
    const {
  return vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE
vosa::PointwiseMatrixMultiply<uint32_t, uint32_t>::get_serialization_mode()
    const {
  return vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE
vosa::PointwiseMatrixMultiply<int8_t, int8_t>::get_serialization_mode() const {
  return vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE
vosa::PointwiseMatrixMultiply<int16_t, int16_t>::get_serialization_mode()
    const {
  return vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE
vosa::PointwiseMatrixMultiply<int32_t, int32_t>::get_serialization_mode()
    const {
  return vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE
vosa::PointwiseMatrixMultiply<float, float>::get_serialization_mode() const {
  return vosa_serialization::POINTWISE_MATRIX_MULTIPLY_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename inputT, typename outputT>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::PointwiseMatrixMultiply<inputT, outputT>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  auto pointwise_matrix_multiply_attribute = std::make_unique<
      vosa_serialization::POINTWISE_MATRIX_MULTIPLY_ATTRIBUTET>();
  pointwise_matrix_multiply_attribute->K_1 = pack_vector(inner_offsets);
  pointwise_matrix_multiply_attribute->K_2 = pack_vector(outer_offsets);
  pointwise_matrix_multiply_attribute->M = pack_plane(matrix);
  pointwise_matrix_multiply_attribute->M_dimensions = {
      (uint32_t)matrix.dimension(0), (uint32_t)matrix.dimension(1)};

  vosa_serialization::POINTWISE_MATRIX_MULTIPLYT pointwise_matrix_multiply;
  pointwise_matrix_multiply.attr =
      std::move(pointwise_matrix_multiply_attribute);
  pointwise_matrix_multiply.mode = get_serialization_mode();

  auto pointwise_matrix_multiply_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  pointwise_matrix_multiply_operator->op.Set(
      std::move(pointwise_matrix_multiply));
  pointwise_matrix_multiply_operator->name = op_name;
  pointwise_matrix_multiply_operator->inputs = input_names;

  return pointwise_matrix_multiply_operator;
}

template class vosa::PointwiseMatrixMultiply<uint8_t, uint8_t>;

template class vosa::PointwiseMatrixMultiply<uint16_t, uint16_t>;

template class vosa::PointwiseMatrixMultiply<uint32_t, uint32_t>;

template class vosa::PointwiseMatrixMultiply<int8_t, int8_t>;

template class vosa::PointwiseMatrixMultiply<int16_t, int16_t>;

template class vosa::PointwiseMatrixMultiply<int32_t, int32_t>;

template class vosa::PointwiseMatrixMultiply<float, float>;