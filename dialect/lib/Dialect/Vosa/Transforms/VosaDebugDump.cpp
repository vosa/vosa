/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/Dialect/Vosa/IR/VosaDialect.h"
#include "vosa/Dialect/Vosa/Transforms/Passes.h"

#include "mlir/Dialect/Bufferization/IR/Bufferization.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/LLVMIR/LLVMDialect.h"
#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/IR/Builders.h"

#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#define DEBUG_TYPE "vosa-debug-dump"

#include "PassDetail.h"

using namespace mlir;
using namespace mlir::vosa;

namespace cl = llvm::cl;

static cl::list<std::string> vosaDebugDump(
    "vosa-debug-dump-nodes", cl::desc("VOSA pipeline nodes to dump for debug"),
    cl::value_desc("node name"), cl::ZeroOrMore, cl::CommaSeparated);

namespace {

// find or create the function to dump a data value of the given type
mlir::func::FuncOp getOrCreateDumpImplFn(mlir::Type ty,
                                         mlir::OpBuilder &builder,
                                         mlir::ModuleOp module) {
  // create function name: dump_tensor_${elementType} or dump_${scalarType}
  std::string dumpImplFn;
  llvm::raw_string_ostream dumpImplFnS(dumpImplFn);
  dumpImplFnS << "dump_";
  if (auto shapedTy = ty.dyn_cast<ShapedType>()) {
    dumpImplFnS << "tensor_";
    shapedTy.getElementType().print(dumpImplFnS);
  } else {
    ty.print(dumpImplFnS);
  }

  mlir::func::FuncOp dumpFnOp;
  if (!(dumpFnOp = module.lookupSymbol<mlir::func::FuncOp>(dumpImplFn))) {
    // no existing defintion found - create
    OpBuilder::InsertionGuard insertGuard(builder);
    builder.setInsertionPointToStart(module.getBody());
    llvm::SmallVector<mlir::Type, 4> argTypes;
    auto bob = IntegerType::get(builder.getContext(), 8);
    argTypes.push_back(ty);
    argTypes.push_back(mlir::LLVM::LLVMPointerType::get(builder.getContext()));
    llvm::SmallVector<mlir::Type, 4> resTypes;
    auto func_type = builder.getFunctionType(argTypes, resTypes);
    dumpFnOp = mlir::func::FuncOp::create(builder.getUnknownLoc(), dumpImplFn,
                                          func_type);
    dumpFnOp->setAttr("llvm.emit_c_interface", builder.getUnitAttr());
    dumpFnOp.setPrivate();

    Attribute bufferizationAccessType = builder.getStringAttr("read");
    dumpFnOp.setArgAttr(
        0, bufferization::BufferizationDialect::kBufferAccessAttrName,
        bufferizationAccessType);

    module.push_back(dumpFnOp);
  }
  return dumpFnOp;
}

// create the function to dump the value of the given operator
mlir::func::FuncOp createOperatorValueDumpFn(llvm::StringRef opName,
                                             mlir::Type ty,
                                             mlir::ModuleOp module) {
  OpBuilder builder(module);
  auto loc = builder.getUnknownLoc();

  // find unique name for this value - some ops may have multiple values to dump
  std::string baseName;
  for (int suffix = 0;; suffix++) {
    std::string tryName;
    llvm::raw_string_ostream baseNameS(tryName);
    baseNameS << opName;
    if (suffix > 0)
      baseNameS << "_" << suffix;
    if (!module.lookupSymbol<mlir::func::FuncOp>("dump_" + tryName)) {
      baseName = tryName;
      break;
    }
  }

  // create names for dump function, output filename and symbol name
  std::string dumpFn;
  llvm::raw_string_ostream dumpFnS(dumpFn);
  dumpFnS << "dump_" << baseName;

  std::string opNameSym;
  llvm::raw_string_ostream opNameS(opNameSym);
  opNameS << "op_" << baseName;

  llvm::SmallString<20> opNameVal(baseName);
  opNameVal.push_back('\0'); // Null terminate for C
  size_t opNameValSize = opNameVal.size_in_bytes();

  OpBuilder::InsertionGuard insertGuard(builder);
  builder.setInsertionPointToStart(module.getBody());

  // create global for operatorName
  mlir::LLVM::GlobalOp globalOpName;
  if (!(globalOpName = module.lookupSymbol<mlir::LLVM::GlobalOp>(opNameSym))) {
    auto type = mlir::LLVM::LLVMArrayType::get(
        IntegerType::get(builder.getContext(), 8), opNameValSize);
    globalOpName = builder.create<mlir::LLVM::GlobalOp>(
        loc, type, /*isConstant=*/true, mlir::LLVM::Linkage::Internal,
        opNameSym, builder.getStringAttr(opNameVal),
        /*alignment=*/0);
  }

  // create operator value dump function: dump_${name}(val: ty) -> ()
  llvm::SmallVector<mlir::Type, 4> argTypes;
  argTypes.push_back(ty);
  llvm::SmallVector<mlir::Type, 4> resTypes;
  auto func_type = builder.getFunctionType(argTypes, resTypes);
  auto dumpFnOp = mlir::func::FuncOp::create(loc, dumpFn, func_type);
  dumpFnOp.setPrivate();
  auto &entryBlock = *dumpFnOp.addEntryBlock();
  builder.setInsertionPointToStart(&entryBlock);

  mlir::Value arg = entryBlock.getArguments()[0];

  // cast / convert to type required by external dump function
  if (auto shapedTy = ty.dyn_cast<ShapedType>()) {
    // convert to unranked / signless tensor

    if (auto intTy = shapedTy.getElementType().dyn_cast<IntegerType>()) {
      // convert to signless type
      auto signlessIntTy =
          IntegerType::get(intTy.getContext(), intTy.getIntOrFloatBitWidth());
      shapedTy = shapedTy.clone(signlessIntTy);
      auto cast =
          builder.create<UnrealizedConversionCastOp>(loc, shapedTy, arg);
      arg = cast.getResult(0);
    }

    // cast to unranked tensor
    auto unrankedTy = UnrankedTensorType::get(shapedTy.getElementType());
    arg = builder.create<tensor::CastOp>(loc, unrankedTy, arg);
  }

  // get or create declaration of external dump function
  mlir::func::FuncOp dumpImplFn =
      getOrCreateDumpImplFn(arg.getType(), builder, module);

  // get pointer to operator name string
  auto opNamePtr = builder.create<mlir::LLVM::AddressOfOp>(loc, globalOpName);
  auto cst0 = builder.create<mlir::LLVM::ConstantOp>(
      loc, IntegerType::get(builder.getContext(), 64),
      builder.getIntegerAttr(builder.getIndexType(), 0));
  auto opNameCPtr = builder.create<mlir::LLVM::GEPOp>(
      loc, mlir::LLVM::LLVMPointerType::get(builder.getContext()),
      mlir::LLVM::LLVMPointerType::get(builder.getContext()), opNamePtr,
      ArrayRef<mlir::Value>({cst0, cst0}));

  // call the external dump function
  builder.create<mlir::func::CallOp>(loc, dumpImplFn,
                                     std::vector<mlir::Value>{arg, opNameCPtr});

  // add return operator
  builder.create<mlir::func::ReturnOp>(loc, std::vector<mlir::Value>{});

  module.push_back(dumpFnOp);

  return dumpFnOp;
}

class DebugDumpPass : public mlir::vosa::VosaDebugDumpBase<DebugDumpPass> {
public:
  void runOnOperation() override {
    auto func = getOperation();
    auto module = func->getParentOfType<ModuleOp>();
    OpBuilder builder(func);
    bool dumpAll = std::find(vosaDebugDump.begin(), vosaDebugDump.end(), "*") !=
                   vosaDebugDump.end();

    // Walk over all operations in the function - if it's location is one
    // indicated by the "vosa-debug-dump-nodes" option, create a function to
    // dump that value and insert a call to it.
    func.walk([&](mlir::Operation *op) {
      // if (op->getDialect()->getNamespace() !=
      //     VosaDialect::getDialectNamespace())
      //   return;
      if (auto nameLoc = op->getLoc().dyn_cast<NameLoc>()) {
        auto opName = nameLoc.getName().getValue();
        if (dumpAll || std::find(vosaDebugDump.begin(), vosaDebugDump.end(),
                                 opName) != vosaDebugDump.end()) {
          for (auto value : op->getResults()) {
            auto dumpFn =
                createOperatorValueDumpFn(opName, value.getType(), module);
            builder.setInsertionPointAfter(op);
            builder.create<mlir::func::CallOp>(op->getLoc(), dumpFn, value);
          }
        }
      }
    });
  }
};
} // namespace

std::unique_ptr<mlir::Pass> mlir::vosa::createVosaDebugDumpPass() {
  return std::make_unique<DebugDumpPass>();
}
