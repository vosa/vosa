"""
License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import typing
import enum

import vosa.imgproc.comparison_operators as comp
import vosa.imgproc.data_manipulation_operators
import vosa.imgproc.elementwise_binary_operators
import vosa.imgproc.image_transformation_operators
import vosa.imgproc.reduction_operators
from vosa import types as types
import vosa.op as _ops

import vosa.imgproc.image_filtering_operators as filtering
import vosa.imgproc.elementwise_binary_operators as elementwise_binary
import vosa.imgproc.image_transformation_operators as image_transformation
import vosa.imgproc.reduction_operators as reduction
import vosa.imgproc.data_manipulation_operators as data
import vosa.imgproc.elementwise_unary_operators as elementwise_unary
import vosa.imgproc.statistics_operators as statistics

from vosa.op import Op, INPUT_MODES, INPUT


def abs(*operands: Op, operator_mode: elementwise_unary.ABS_MODE, name=None):
    return elementwise_unary.ABS.create(
        *operands, operator_mode=operator_mode, name=name
    )


def abs_diff(
    *operands: Op, operator_mode: elementwise_binary.ABS_DIFF_MODES, name=None
):
    return elementwise_binary.ABS_DIFF.create(
        *operands, operator_mode=operator_mode, name=name
    )


def add(*operands, operator_mode: elementwise_binary.ADD_MODES, name=None):
    return elementwise_binary.ADD.create(
        *operands, operator_mode=operator_mode, name=name
    )


def atan2(*operands, operator_mode: elementwise_binary.ATAN2_MODES, name=None):
    return elementwise_binary.ATAN2.create(
        *operands, operator_mode=operator_mode, name=name
    )


def argmax_channel(
    *operands, operator_mode: reduction.ARG_MAX_CHANNELWISE_MODES, name=None
):
    return reduction.ARG_MAX_CHANNELWISE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def argmax_planewise(
    *operands, operator_mode: reduction.ARG_MAX_PLANEWISE_MODES, name=None
):
    return reduction.ARG_MAX_PLANEWISE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def argmin_channel(
    *operands, operator_mode: reduction.ARG_MIN_CHANNELWISE_MODES, name=None
):
    return reduction.ARG_MIN_CHANNELWISE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def argmin_planewise(
    *operands, operator_mode: reduction.ARG_MIN_PLANEWISE_MODES, name=None
):
    return reduction.ARG_MIN_PLANEWISE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def greater_than(
    *operands, operator_mode: vosa.imgproc.comparison_operators.GREATER_MODES, name=None
):
    return vosa.imgproc.comparison_operators.GREATER.create(
        *operands, operator_mode=operator_mode, name=name
    )


def greater_equal(
    *operands,
    operator_mode: vosa.imgproc.comparison_operators.GREATER_EQUAL_MODES,
    name=None
):
    return vosa.imgproc.comparison_operators.GREATER_EQUAL.create(
        *operands, operator_mode=operator_mode, name=name
    )


def arithmetic_shift_right(
    *operands,
    operator_mode: elementwise_unary.ARITHMETIC_SHIFT_RIGHT_MODES,
    attributes=None,
    name=None
):
    return elementwise_unary.ARITHMETIC_SHIFT_RIGHT.create(
        *operands, operator_mode=operator_mode, attributes=attributes, name=name
    )


def bilateral_filter(
    *operands, operator_mode: filtering.BILATERAL_FILTER_MODES, name=None
):
    return filtering.BILATERAL_FILTER.create(
        *operands, operator_mode=operator_mode, name=name
    )


def bitwise_and(*operands, operator_mode: elementwise_binary.AND_MODES, name=None):
    return elementwise_binary.AND.create(
        *operands, operator_mode=operator_mode, name=name
    )


def bitwise_not(*operands, operator_mode: elementwise_unary.NOT_MODES, name=None):
    return elementwise_unary.NOT.create(
        *operands, operator_mode=operator_mode, name=name
    )


def bitwise_or(*operands, operator_mode: elementwise_binary.OR_MODES, name=None):
    return elementwise_binary.OR.create(
        *operands, operator_mode=operator_mode, name=name
    )


def bitwise_xor(*operands, operator_mode: elementwise_binary.XOR_MODES, name=None):
    return elementwise_binary.XOR.create(
        *operands, operator_mode=operator_mode, name=name
    )


def broadcast_channelwise(
    *operands, operator_mode: data.BROADCAST_CHANNELWISE_MODES, name=None
):
    return data.BROADCAST_CHANNELWISE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def broadcast_scalar(
    *operands, operator_mode: data.BROADCAST_PLANEWISE_MODES, name=None, attributes=None
):
    return data.BROADCAST_PLANEWISE.create(
        *operands, operator_mode=operator_mode, name=name, attributes=attributes
    )


def cast_image(*operands: Op, output_dtype: types.DType, name=None, attributes=None):
    class CastExecutionPattern(_ops.ExecutionPattern):
        def __init__(self, output_dtype):
            input_types = [operand.get_return_data() for operand in operands]
            assert len(input_types) == 1
            super().__init__(
                input_pattern=_ops.InputPattern(*input_types),
                output_pattern=types.DataType(output_dtype, types.AType.IMAGE),
            )

    class CastOpModes(_ops.OPERATOR_MODES):
        GenericCastOpMode = enum.auto()

    class CAST(vosa.ops.data.CAST):
        def to_flatbuffer(self):
            import vosa_serialization.CAST
            import vosa_serialization.CAST_METHOD
            import vosa_serialization.CAST_ATTRIBUTE

            cast_methods = {
                CAST.ROUNDING_MODE.WRAP: vosa_serialization.CAST_METHOD.CAST_METHOD.WRAP,
                CAST.ROUNDING_MODE.SATURATE: vosa_serialization.CAST_METHOD.CAST_METHOD.SATURATE,
                # FIXME – REINTERPET has a typo and not in vosa.fbs!
                # CAST.ROUNDING_MODE.REINTERPET: vosa_serialization.CAST_METHOD.CAST_METHOD.REINTERPET
            }

            cast_t = vosa_serialization.CAST.CASTT()
            cast_t.inputType = types.DType.to_flatbuffer(
                operands[0].get_return_data().dtype()
            )
            cast_t.outputType = types.DType.to_flatbuffer(output_dtype)
            cast_t.attr = vosa_serialization.CAST_ATTRIBUTE.CAST_ATTRIBUTET()
            cast_t.attr.castMethod = cast_methods[self._attributes[self.ROUNDING_MODE]]

            return cast_t

        @classmethod
        def get_pattern_dict(cls) -> typing.Dict[CastOpModes, _ops.ExecutionPattern]:
            return {CastOpModes.GenericCastOpMode: CastExecutionPattern(output_dtype)}

    return CAST.create(
        *operands,
        operator_mode=CastOpModes.GenericCastOpMode,
        name=name,
        attributes=attributes
    )


def channel_extract(*operands, operator_mode: data.CHANNEL_EXTRACT_MODES, name=None):
    return data.CHANNEL_EXTRACT.create(
        *operands, operator_mode=operator_mode, name=name
    )


def clamp(*operands, operator_mode: elementwise_unary.CLAMP_MODES, name=None):
    return elementwise_unary.CLAMP.create(
        *operands, operator_mode=operator_mode, name=name
    )


def pointwise_matrix_multiply(
    *operands,
    operator_mode: image_transformation.POINTWISE_MATRIX_MULTIPLY_MODES,
    name=None
):
    return image_transformation.POINTWISE_MATRIX_MULTIPLY.create(
        *operands, operator_mode=operator_mode, name=name
    )


def concat(*operands, operator_mode: data.CONCAT_MODES, name=None):
    return data.CONCAT.create(*operands, operator_mode=operator_mode, name=name)


def const_scalar(
    *operands, operator_mode: data.CONST_SCALAR_MODES, name=None, attributes=None
):
    return data.CONST_SCALAR.create(
        *operands, operator_mode=operator_mode, name=name, attributes=attributes
    )


def const_image(
    *operands, operator_mode: data.CONST_IMAGE_MODES, name=None, attributes=None
):
    return data.CONST_IMAGE.create(
        *operands, operator_mode=operator_mode, name=name, attributes=attributes
    )


def const_plane(*operands, operator_mode: data.CONST_PLANE_MODES, name=None):
    return data.CONST_PLANE.create(*operands, operator_mode=operator_mode, name=name)


def mask2index(*operands, operator_mode: data.MASK_TO_INDEX_MODES, name=None):
    return data.MASK_TO_INDEX.create(*operands, operator_mode=operator_mode, name=name)


def conv2d(input: _ops.Op, kernel_size, name=None, dtype=types.DType.INT16):
    input_dtype = input.dtype()
    if input_dtype == types.DType.UINT8:
        operator_mode = filtering.CONV_2D_MODES.I_I_UINT8_UINT8
    elif input_dtype == types.DType.UINT16:
        operator_mode = filtering.CONV_2D_MODES.I_I_UINT16_UINT16
    elif input_dtype == types.DType.UINT32:
        operator_mode = filtering.CONV_2D_MODES.I_I_UINT32_UINT32
    elif input_dtype == types.DType.INT16:
        operator_mode = filtering.CONV_2D_MODES.I_I_INT16_INT16
    elif input_dtype == types.DType.FLOAT32:
        operator_mode = filtering.CONV_2D_MODES.I_I_FLOAT32_FLOAT32
    else:
        operator_mode = (
            filtering.CONV_2D_MODES.I_I_UINT8_UINT8
        )  # This wont work but it allows graph creation to proceed.
    attributes = {
        filtering.CONV_2D.FILTER_ATTRIBUTE: types.DataType(
            ttype=types.AType.PLANE, dtype=input_dtype
        )
    }
    return filtering.CONV_2D.create(
        input, operator_mode=operator_mode, name=name, attributes=attributes
    )


def crop(*operands: Op, operator_mode: image_transformation.CROP_MODES, name=None):
    return image_transformation.CROP.create(
        *operands, operator_mode=operator_mode, name=name
    )


def custom_image_op(*operands, output_dtype: vosa.types.DType, name=None):
    class CustomOpExecutionPattern(_ops.ExecutionPattern):
        def __init__(self, output_dtype):
            input_types = [operand.get_return_data() for operand in operands]
            super().__init__(
                input_pattern=_ops.InputPattern(*input_types),
                output_pattern=types.DataType(output_dtype, types.AType.IMAGE),
            )

    class CustomOpModes(_ops.OPERATOR_MODES):
        CustomOpMode = enum.auto()

    class CustomImageOp(Op):
        @classmethod
        def get_pattern_dict(cls) -> typing.Dict[CustomOpModes, _ops.ExecutionPattern]:
            return {CustomOpModes.CustomOpMode: CustomOpExecutionPattern(output_dtype)}

    return CustomImageOp.create(
        *operands, operator_mode=CustomOpModes.CustomOpMode, name=name
    )


def custom_planar_op(*operands, output_dtype: vosa.types.DType, name=None):
    class CustomOpExecutionPattern(_ops.ExecutionPattern):
        def __init__(self, output_dtype):
            input_types = [operand.get_return_data() for operand in operands]
            super().__init__(
                input_pattern=_ops.InputPattern(*input_types),
                output_pattern=types.DataType(output_dtype, types.AType.PLANE),
            )

    class CustomOpModes(_ops.OPERATOR_MODES):
        CustomOpMode = enum.auto()

    class CustomPlanarOp(Op):
        @classmethod
        def get_pattern_dict(cls) -> typing.Dict[CustomOpModes, _ops.ExecutionPattern]:
            return {CustomOpModes.CustomOpMode: CustomOpExecutionPattern(output_dtype)}

    return CustomPlanarOp.create(
        *operands, operator_mode=CustomOpModes.CustomOpMode, name=name
    )


def custom_scalar_op(*operands, output_dtype: vosa.types.DType, name=None):
    class CustomOpExecutionPattern(_ops.ExecutionPattern):
        def __init__(self, output_dtype):
            input_types = [operand.get_return_data() for operand in operands]
            super().__init__(
                input_pattern=_ops.InputPattern(*input_types),
                output_pattern=types.DataType(output_dtype, types.AType.SCALAR),
            )

    class CustomOpModes(_ops.OPERATOR_MODES):
        CustomOpMode = enum.auto()

    class CustomScalarOp(Op):
        @classmethod
        def get_pattern_dict(cls) -> typing.Dict[CustomOpModes, _ops.ExecutionPattern]:
            return {CustomOpModes.CustomOpMode: CustomOpExecutionPattern(output_dtype)}

    return CustomScalarOp.create(
        *operands, operator_mode=CustomOpModes.CustomOpMode, name=name
    )


def dilate(*operands, operator_mode: filtering.NON_LINEAR_MODES, name=None):
    return filtering.DILATE.create(*operands, operator_mode=operator_mode, name=name)


def div(*operands: Op, operator_mode: elementwise_binary.DIV_MODES, name=None):
    return elementwise_binary.DIV.create(
        *operands, operator_mode=operator_mode, name=name
    )


def decimate(
    *operands: Op, operator_mode: image_transformation.DECIMATE_MODES, name=None
):
    return image_transformation.DECIMATE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def erode(*operands, operator_mode: filtering.NON_LINEAR_MODES, name=None):
    return filtering.ERODE.create(*operands, operator_mode=operator_mode, name=name)


def equals(
    *operands, operator_mode: vosa.imgproc.comparison_operators.EQUAL_MODES, name=None
):
    return comp.EQUAL.create(*operands, operator_mode=operator_mode, name=name)


def expand(*operands, operator_mode: image_transformation.EXPAND_MODES, name=None):
    return image_transformation.EXPAND.create(
        *operands, operator_mode=operator_mode, name=name
    )


def gamma_correction(
    *operands, operator_mode: elementwise_unary.GAMMA_CORRECTION_MODES, name=None
):
    return elementwise_unary.GAMMA_CORRECTION.create(
        *operands, operator_mode=operator_mode, name=name
    )


def histogram(*operands, operator_mode: statistics.UPDATE_HISTOGRAM_MODES, name=None):
    return statistics.UPDATE_HISTOGRAM.create(
        *operands, operator_mode=operator_mode, name=name
    )


def import_channel(
    *operands, operator_mode: data.IMPORT_CHANNEL_EXECUTION_MODES, name=None
):
    return data.IMPORT_CHANNEL.create(*operands, operator_mode=operator_mode, name=name)


def input(operator_mode: INPUT_MODES, name=None):
    return INPUT.create(operator_mode=operator_mode, name=name)


def logical_shift_right(
    *operands: Op, operator_mode: elementwise_unary.LOGICAL_SHIFT_RIGHT_MODES, name=None
):
    return elementwise_unary.LOGICAL_SHIFT_RIGHT.create(
        *operands, operator_mode=operator_mode, name=name
    )


def mult(*operands: Op, operator_mode: elementwise_binary.MULT_MODES, name=None):
    return elementwise_binary.MULT.create(
        *operands, operator_mode=operator_mode, name=name
    )


def pad(
    *operands: Op,
    operator_mode: vosa.imgproc.image_transformation_operators.PAD_MODES,
    name=None
):
    return vosa.imgproc.image_transformation_operators.PAD.create(
        *operands, operator_mode=operator_mode, name=name
    )


def piecewise_linear(
    *operands: Op,
    operator_mode: vosa.imgproc.data_manipulation_operators.PIECEWISE_LINEAR_MODES,
    name=None
):
    return vosa.imgproc.data_manipulation_operators.PIECEWISE_LINEAR.create(
        *operands, operator_mode=operator_mode, name=name
    )


def power(
    *operands: Op,
    operator_mode: vosa.imgproc.elementwise_unary_operators.POWER_MODES,
    name=None
):
    return vosa.imgproc.elementwise_unary_operators.POWER.create(
        *operands, operator_mode=operator_mode, name=name
    )


def reduce_max_planewise(
    *operands, operator_mode: reduction.REDUCE_MAX_PLANEWISE_MODES, name=None
):
    return reduction.REDUCE_MAX_PLANEWISE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def reduce_max_channelwise(
    *operands, operator_mode: reduction.REDUCE_MAX_CHANNELWISE_MODES, name=None
):
    return reduction.REDUCE_MAX_CHANNELWISE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def reduce_min_channelwise(
    *operands, operator_mode: reduction.REDUCE_MIN_CHANNELWISE_MODES, name=None
):
    return reduction.REDUCE_MIN_CHANNELWISE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def reduce_min_planewise(
    *operands, operator_mode: reduction.REDUCE_MIN_PLANEWISE_MODES, name=None
):
    return reduction.REDUCE_MIN_PLANEWISE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def reduce_sum_channelwise(
    *operands: Op, operator_mode: reduction.REDUCE_SUM_CHANNELWISE_MODES, name=None
):
    return reduction.REDUCE_SUM_CHANNELWISE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def reduce_sum_planewise(
    *operands: Op, operator_mode: reduction.REDUCE_SUM_PLANEWISE_MODES, name=None
):
    return reduction.REDUCE_SUM_PLANEWISE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def round(*operands: Op, operator_mode: elementwise_unary.ROUND_MODES, name=None):
    return elementwise_unary.ROUND.create(
        *operands, operator_mode=operator_mode, name=name
    )


def sqrt(*operands: Op, operator_mode: elementwise_unary.SQRT_MODES, name=None):
    return elementwise_unary.SQRT.create(
        *operands, operator_mode=operator_mode, name=name
    )


def sub(*operands: Op, operator_mode: elementwise_binary.SUB_MODES, name=None):
    return elementwise_binary.SUB.create(
        *operands, operator_mode=operator_mode, name=name
    )


def CONV(*operands, operator_mode: filtering.CONV_2D_MODES, name=None):
    return filtering.CONV_2D.create(*operands, operator_mode=operator_mode, name=name)


def median_filter(*operands, operator_mode: filtering.NON_LINEAR_MODES, name=None):
    return filtering.MEDIAN_FILTER.create(
        *operands, operator_mode=operator_mode, name=name
    )


def max(*operands, operator_mode: elementwise_binary.MAX_MODES, name=None):
    return elementwise_binary.MAX.create(
        *operands, operator_mode=operator_mode, name=name
    )


def min(*operands, operator_mode: elementwise_binary.MIN_MODES, name=None):
    return elementwise_binary.MIN.create(
        *operands, operator_mode=operator_mode, name=name
    )


def non_max_suppression(
    *operands, operator_mode: filtering.NON_LINEAR_MODES, name=None
):
    return filtering.NON_MAX_SUPPRESSION.create(
        *operands, operator_mode=operator_mode, name=name
    )


def reduce_interp_channelwise(
    *operands,
    operator_mode: vosa.imgproc.reduction_operators.REDUCE_INTERP_CHANNELWISE_MODES,
    name=None
):
    return vosa.imgproc.reduction_operators.REDUCE_INTERP_CHANNELWISE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def remap_bilinear(
    *operands, operator_mode: image_transformation.REMAP_BILINEAR_MODES, name=None
):
    return image_transformation.REMAP_BILINEAR.create(
        *operands, operator_mode=operator_mode, name=name
    )


def remap_nearest_neighbour(
    *operands,
    operator_mode: image_transformation.REMAP_NEAREAST_NEIGHBOUR_MODES,
    name=None
):
    return image_transformation.REMAP_NEAREST_NEIGHBOUR.create(
        *operands, operator_mode=operator_mode, name=name
    )


def resize_area(*operands, operator_mode: image_transformation.RESIZE_MODES, name=None):
    return image_transformation.RESIZE_AREA.create(
        *operands, operator_mode=operator_mode, name=name
    )


def resize_bilinear(
    *operands, operator_mode: image_transformation.RESIZE_MODES, name=None
):
    return image_transformation.RESIZE_BILINEAR.create(
        *operands, operator_mode=operator_mode, name=name
    )


def resize_nearest_neighbour(
    *operands, operator_mode: image_transformation.RESIZE_MODES, name=None
):
    return image_transformation.RESIZE_NEAREST_NEIGHBOUR.create(
        *operands, operator_mode=operator_mode, name=name
    )


def resize_lanczos(
    *operands, operator_mode: image_transformation.RESIZE_LANCZOS_MODES, name=None
):
    return image_transformation.RESIZE_LANCZOS.create(
        *operands, operator_mode=operator_mode, name=name
    )


def rotate90(*operands, operator_mode: image_transformation.ROTATE_90_MODES, name=None):
    return image_transformation.ROTATE_90.create(
        *operands, operator_mode=operator_mode, name=name
    )


def shape(*operands, operator_mode: data.SHAPE_MODES, name=None):
    return data.SHAPE.create(*operands, operator_mode=operator_mode, name=name)


def tosa_graph(*operands: Op, output_dtype: types.DType, name=None):
    class TosaExecutionPattern(_ops.ExecutionPattern):
        def __init__(self, output_dtype):
            input_types = [operand.get_return_data() for operand in operands]
            super().__init__(
                input_pattern=_ops.InputPattern(*input_types),
                output_pattern=types.DataType(output_dtype, types.AType.IMAGE),
            )  # FIXME: Change AType to a generic thing !

    class TOSA_GRAPH_MODES(_ops.OPERATOR_MODES):
        GenericTOSAOpMode = enum.auto()

    class TOSA_GRAPH(vosa.ops.data.TOSA_GRAPH):
        def to_flatbuffer(self):
            import vosa_serialization.TOSA_GRAPH
            import vosa_serialization.TOSA_GRAPH_ATTRIBUTE

            tosa_graph_t = vosa_serialization.TOSA_GRAPH.TOSA_GRAPHT()
            tosa_graph_t.outputType = types.DType.to_flatbuffer(
                self.get_return_data().dtype()
            )
            tosa_graph_t.attr = (
                vosa_serialization.TOSA_GRAPH_ATTRIBUTE.TOSA_GRAPH_ATTRIBUTET()
            )
            tosa_graph_t.attr.buffer = self._attributes[self.BUFFER_ATTRIBUTE].value()

            return tosa_graph_t

        @classmethod
        def get_pattern_dict(
            cls,
        ) -> typing.Dict[TOSA_GRAPH_MODES, _ops.ExecutionPattern]:
            return {
                TOSA_GRAPH_MODES.GenericTOSAOpMode: TosaExecutionPattern(output_dtype)
            }

    return TOSA_GRAPH.create(
        *operands, operator_mode=TOSA_GRAPH_MODES.GenericTOSAOpMode, name=name
    )


def warp_affine(
    *operands, operator_mode: image_transformation.WARP_AFFINE_MODES, name=None
):
    return image_transformation.WARP_AFFINE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def warp_perspective(
    *operands, operator_mode: image_transformation.WARP_PERSPECTIVE_MODES, name=None
):
    return image_transformation.WARP_PERSPECTIVE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def warp_flow(
    *operands, operator_mode: image_transformation.WARP_FLOW_MODES, name=None
):
    return image_transformation.WARP_FLOW.create(
        *operands, operator_mode=operator_mode, name=name
    )


def where(
    *operands,
    operator_mode: vosa.imgproc.data_manipulation_operators.WHERE_MODES,
    name=None
):
    return vosa.imgproc.data_manipulation_operators.WHERE.create(
        *operands, operator_mode=operator_mode, name=name
    )


def mod(*operands, operator_mode: elementwise_binary.MOD_MODES, name=None):
    return elementwise_binary.MOD.create(
        *operands, operator_mode=operator_mode, name=name
    )


def mesh_grid(*operands, operator_mode: data.MESH_GRID_EXECUTION_MODES, name=None):
    return data.MESH_GRID.create(*operands, operator_mode=operator_mode, name=name)


def or_(*operands, operator_mode: elementwise_binary.OR_MODES, name=None):
    return elementwise_binary.OR.create(
        *operands, operator_mode=operator_mode, name=name
    )


def not_(*operands, operator_mode: elementwise_unary.NOT_MODES, name=None):
    return elementwise_unary.NOT.create(
        *operands, operator_mode=operator_mode, name=name
    )
