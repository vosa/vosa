/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "cast.h"
#include <limits>
#include <memory>
#include <unordered_map>

template <typename inputT, typename outputT>
std::shared_ptr<vosa::Op>
makeCast(vosa::CastBase::rounding_mode_t rounding_mode) {
  return std::make_shared<vosa::Cast<inputT, outputT>>(rounding_mode);
}

std::shared_ptr<vosa::Op> vosa::CastBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_cast = vosa_operator_union.AsCAST();
  assert(serialized_cast);
  const auto &attr = serialized_cast->attr;
  const auto &cast_method = attr->cast_method;
  const auto &input_dtype = serialized_cast->input_type;
  const auto &output_dtype = serialized_cast->output_type;

  vosa::CastBase::rounding_mode_t rounding_mode;
  switch (cast_method) {
  case (vosa_serialization::CAST_METHOD_SATURATE): {
    rounding_mode = vosa::CastBase::VOSA_ROUNDING_MODE_SATURATE;
    break;
  }
  case (vosa_serialization::CAST_METHOD_WRAP): {
    rounding_mode = vosa::CastBase::VOSA_ROUNDING_MODE_WRAP;
    break;
  }
  default: {
    throw std::logic_error("");
  }
  }

  typedef std::shared_ptr<vosa::Op> (*factory_ptr)(
      vosa::CastBase::rounding_mode_t);

  std::unordered_map<vosa_serialization::DTYPE, factory_ptr> bool_table = {
      {vosa_serialization::DTYPE_BOOL, makeCast<bool, bool>},
      {vosa_serialization::DTYPE_UINT8, makeCast<bool, uint8_t>},
      {vosa_serialization::DTYPE_UINT16, makeCast<bool, uint16_t>},
      {vosa_serialization::DTYPE_UINT32, makeCast<bool, uint32_t>},
      {vosa_serialization::DTYPE_INT8, makeCast<bool, int8_t>},
      {vosa_serialization::DTYPE_INT16, makeCast<bool, int16_t>},
      {vosa_serialization::DTYPE_INT32, makeCast<bool, int32_t>},
      {vosa_serialization::DTYPE_FLOAT32, makeCast<bool, float>}};

  std::unordered_map<vosa_serialization::DTYPE, factory_ptr> uint8_table = {
      {vosa_serialization::DTYPE_BOOL, makeCast<uint8_t, bool>},
      {vosa_serialization::DTYPE_UINT8, makeCast<uint8_t, uint8_t>},
      {vosa_serialization::DTYPE_UINT16, makeCast<uint8_t, uint16_t>},
      {vosa_serialization::DTYPE_UINT32, makeCast<uint8_t, uint32_t>},
      {vosa_serialization::DTYPE_INT8, makeCast<uint8_t, int8_t>},
      {vosa_serialization::DTYPE_INT16, makeCast<uint8_t, int16_t>},
      {vosa_serialization::DTYPE_INT32, makeCast<uint8_t, int32_t>},
      {vosa_serialization::DTYPE_FLOAT32, makeCast<uint8_t, float>}};

  std::unordered_map<vosa_serialization::DTYPE, factory_ptr> uint16_table = {
      {vosa_serialization::DTYPE_BOOL, makeCast<uint16_t, bool>},
      {vosa_serialization::DTYPE_UINT8, makeCast<uint16_t, uint8_t>},
      {vosa_serialization::DTYPE_UINT16, makeCast<uint16_t, uint16_t>},
      {vosa_serialization::DTYPE_UINT32, makeCast<uint16_t, uint32_t>},
      {vosa_serialization::DTYPE_INT8, makeCast<uint16_t, int8_t>},
      {vosa_serialization::DTYPE_INT16, makeCast<uint16_t, int16_t>},
      {vosa_serialization::DTYPE_INT32, makeCast<uint16_t, int32_t>},
      {vosa_serialization::DTYPE_FLOAT32, makeCast<uint16_t, float>}};

  std::unordered_map<vosa_serialization::DTYPE, factory_ptr> uint32_table = {
      {vosa_serialization::DTYPE_BOOL, makeCast<uint32_t, bool>},
      {vosa_serialization::DTYPE_UINT8, makeCast<uint32_t, uint8_t>},
      {vosa_serialization::DTYPE_UINT16, makeCast<uint32_t, uint16_t>},
      {vosa_serialization::DTYPE_UINT32, makeCast<uint32_t, uint32_t>},
      {vosa_serialization::DTYPE_INT8, makeCast<uint32_t, int8_t>},
      {vosa_serialization::DTYPE_INT16, makeCast<uint32_t, int16_t>},
      {vosa_serialization::DTYPE_INT32, makeCast<uint32_t, int32_t>},
      {vosa_serialization::DTYPE_FLOAT32, makeCast<uint32_t, float>}};

  std::unordered_map<vosa_serialization::DTYPE, factory_ptr> int8_table = {
      {vosa_serialization::DTYPE_BOOL, makeCast<int8_t, bool>},
      {vosa_serialization::DTYPE_UINT8, makeCast<int8_t, uint8_t>},
      {vosa_serialization::DTYPE_UINT16, makeCast<int8_t, uint16_t>},
      {vosa_serialization::DTYPE_UINT32, makeCast<int8_t, uint32_t>},
      {vosa_serialization::DTYPE_INT8, makeCast<int8_t, int8_t>},
      {vosa_serialization::DTYPE_INT16, makeCast<int8_t, int16_t>},
      {vosa_serialization::DTYPE_INT32, makeCast<int8_t, int32_t>},
      {vosa_serialization::DTYPE_FLOAT32, makeCast<int8_t, float>}};

  std::unordered_map<vosa_serialization::DTYPE, factory_ptr> int16_table = {
      {vosa_serialization::DTYPE_BOOL, makeCast<int16_t, bool>},
      {vosa_serialization::DTYPE_UINT8, makeCast<int16_t, uint8_t>},
      {vosa_serialization::DTYPE_UINT16, makeCast<int16_t, uint16_t>},
      {vosa_serialization::DTYPE_UINT32, makeCast<int16_t, uint32_t>},
      {vosa_serialization::DTYPE_INT8, makeCast<int16_t, int8_t>},
      {vosa_serialization::DTYPE_INT16, makeCast<int16_t, int16_t>},
      {vosa_serialization::DTYPE_INT32, makeCast<int16_t, int32_t>},
      {vosa_serialization::DTYPE_FLOAT32, makeCast<int16_t, float>}};

  std::unordered_map<vosa_serialization::DTYPE, factory_ptr> int32_table = {
      {vosa_serialization::DTYPE_BOOL, makeCast<int32_t, bool>},
      {vosa_serialization::DTYPE_UINT8, makeCast<int32_t, uint8_t>},
      {vosa_serialization::DTYPE_UINT16, makeCast<int32_t, uint16_t>},
      {vosa_serialization::DTYPE_UINT32, makeCast<int32_t, uint32_t>},
      {vosa_serialization::DTYPE_INT8, makeCast<int32_t, int8_t>},
      {vosa_serialization::DTYPE_INT16, makeCast<int32_t, int16_t>},
      {vosa_serialization::DTYPE_INT32, makeCast<int32_t, int32_t>},
      {vosa_serialization::DTYPE_FLOAT32, makeCast<int32_t, float>}};

  std::unordered_map<vosa_serialization::DTYPE, factory_ptr> float_table = {
      {vosa_serialization::DTYPE_BOOL, makeCast<float, bool>},
      {vosa_serialization::DTYPE_UINT8, makeCast<float, uint8_t>},
      {vosa_serialization::DTYPE_UINT16, makeCast<float, uint16_t>},
      {vosa_serialization::DTYPE_UINT32, makeCast<float, uint32_t>},
      {vosa_serialization::DTYPE_INT8, makeCast<float, int8_t>},
      {vosa_serialization::DTYPE_INT16, makeCast<float, int16_t>},
      {vosa_serialization::DTYPE_INT32, makeCast<float, int32_t>},
      {vosa_serialization::DTYPE_FLOAT32, makeCast<float, float>}};

  std::unordered_map<vosa_serialization::DTYPE,
                     std::unordered_map<vosa_serialization::DTYPE, factory_ptr>>
      lookup;
  lookup[vosa_serialization::DTYPE_BOOL] = bool_table;
  lookup[vosa_serialization::DTYPE_UINT8] = uint8_table;
  lookup[vosa_serialization::DTYPE_UINT16] = uint16_table;
  lookup[vosa_serialization::DTYPE_UINT32] = uint32_table;
  lookup[vosa_serialization::DTYPE_INT8] = int8_table;
  lookup[vosa_serialization::DTYPE_INT16] = int16_table;
  lookup[vosa_serialization::DTYPE_INT32] = int32_table;
  lookup[vosa_serialization::DTYPE_FLOAT32] = float_table;

  return lookup.at(input_dtype).at(output_dtype)(rounding_mode);
  //    return makeCast<uint8_t, uint8_t>();
}

template <typename inputT, typename outputT>
std::shared_ptr<vosa::ArrayBase> vosa::Cast<inputT, outputT>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<inputT> &input = inputs[0]->template get_image<inputT>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <typename inputT, typename outputT>
vosa::EigenImage<outputT>
vosa::Cast<inputT, outputT>::runme(const EigenImage<inputT> &input) const {

  uint32_t height = input.dimension(0);
  uint32_t width = input.dimension(1);
  uint32_t num_channels = input.dimension(2);

  outputT limit_above = std::numeric_limits<outputT>::max();
  outputT limit_below = std::numeric_limits<outputT>::lowest();

  inputT limit_above_as_input_type = limit_above;
  inputT limit_below_as_input_type = limit_below;

  vosa::EigenImage<outputT> output{height, width, num_channels};
  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < num_channels;
           channel_index++) {
        inputT this_input = input(height_index, width_index, channel_index);

        outputT this_output;
        if (rounding_mode == VOSA_ROUNDING_MODE_WRAP)
          this_output = (outputT)this_input;
        else if (rounding_mode == VOSA_ROUNDING_MODE_SATURATE) {
          inputT input_limited_above =
              std::min(this_input, limit_above_as_input_type);
          inputT input_limited =
              std::max(input_limited_above, limit_below_as_input_type);
          this_output = (outputT)input_limited;
        } else
          throw vosa::CastException();
        output(height_index, width_index, channel_index) = this_output;
      }
    }
  }
  return output;
}

template <>
vosa::EigenImage<int16_t>
vosa::Cast<uint16_t, int16_t>::runme(const EigenImage<uint16_t> &input) const {

  uint32_t num_channels = input.dimension(0);
  uint32_t height = input.dimension(1);
  uint32_t width = input.dimension(2);

  int16_t limit_above = std::numeric_limits<int16_t>::max();
  //    outputT limit_below = std::numeric_limits<outputT>::lowest();

  uint16_t limit_above_as_input_type = limit_above;
  //    inputT limit_below_as_input_type = limit_below;

  vosa::EigenImage<int16_t> output{num_channels, height, width};
  for (uint32_t channel_index = 0; channel_index < num_channels;
       channel_index++) {
    for (uint32_t height_index = 0; height_index < height; height_index++) {
      for (uint32_t width_index = 0; width_index < width; width_index++) {
        uint16_t this_input = input(channel_index, height_index, width_index);

        int16_t this_output;
        if (rounding_mode == VOSA_ROUNDING_MODE_WRAP)
          this_output = (int16_t)this_input;
        else if (rounding_mode == VOSA_ROUNDING_MODE_SATURATE) {
          if (this_input > limit_above_as_input_type) {
            this_output = limit_above;
          } else {
            this_output = (int16_t)this_input;
          }

        } else
          throw vosa::CastException();
        output(channel_index, height_index, width_index) = this_output;
      }
    }
  }
  return output;
}

template <typename inputT, typename outputT>
vosa::ArrayBase::DType vosa::Cast<inputT, outputT>::output_dtype() const {
  return output_dtype_helper<outputT>();
}

template <typename inputT, typename outputT>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Cast<inputT, outputT>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  auto cast_attribute = std::make_unique<vosa_serialization::CAST_ATTRIBUTET>();
  switch (rounding_mode) {
  case vosa::CastBase::VOSA_ROUNDING_MODE_WRAP: {
    cast_attribute->cast_method = vosa_serialization::CAST_METHOD_WRAP;
    break;
  }
  case vosa::CastBase::VOSA_ROUNDING_MODE_SATURATE: {
    cast_attribute->cast_method = vosa_serialization::CAST_METHOD_SATURATE;
    break;
  }
  default: {
    throw std::logic_error("");
  }
  }

  vosa_serialization::CASTT cast;
  cast.input_type =
      ArrayBase::serialize_dtype(vosa::Op::output_dtype_helper<inputT>());
  cast.output_type =
      ArrayBase::serialize_dtype(vosa::Op::output_dtype_helper<outputT>());
  cast.attr = std::move(cast_attribute);

  auto cast_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  cast_operator->op.Set(std::move(cast));
  cast_operator->name = op_name;
  cast_operator->inputs = input_names;
  return cast_operator;
}

template class vosa::Cast<bool, bool>;

template class vosa::Cast<bool, uint8_t>;

template class vosa::Cast<bool, uint16_t>;

template class vosa::Cast<bool, uint32_t>;

template class vosa::Cast<bool, int8_t>;

template class vosa::Cast<bool, int16_t>;

template class vosa::Cast<bool, int32_t>;

template class vosa::Cast<bool, float>;

template class vosa::Cast<bool, double>;

template class vosa::Cast<uint8_t, bool>;

template class vosa::Cast<uint8_t, uint8_t>;

template class vosa::Cast<uint8_t, uint16_t>;

template class vosa::Cast<uint8_t, uint32_t>;

template class vosa::Cast<uint8_t, int8_t>;

template class vosa::Cast<uint8_t, int16_t>;

template class vosa::Cast<uint8_t, int32_t>;

template class vosa::Cast<uint8_t, float>;

template class vosa::Cast<uint8_t, double>;

template class vosa::Cast<uint16_t, bool>;

template class vosa::Cast<uint16_t, uint8_t>;

template class vosa::Cast<uint16_t, uint16_t>;

template class vosa::Cast<uint16_t, uint32_t>;

template class vosa::Cast<uint16_t, int8_t>;

template class vosa::Cast<uint16_t, int16_t>;

template class vosa::Cast<uint16_t, int32_t>;

template class vosa::Cast<uint16_t, float>;

template class vosa::Cast<uint16_t, double>;

template class vosa::Cast<uint32_t, bool>;

template class vosa::Cast<uint32_t, uint8_t>;

template class vosa::Cast<uint32_t, uint16_t>;

template class vosa::Cast<uint32_t, uint32_t>;

template class vosa::Cast<uint32_t, int8_t>;

template class vosa::Cast<uint32_t, int16_t>;

template class vosa::Cast<uint32_t, int32_t>;

template class vosa::Cast<uint32_t, float>;

template class vosa::Cast<uint32_t, double>;

template class vosa::Cast<int8_t, bool>;

template class vosa::Cast<int8_t, uint8_t>;

template class vosa::Cast<int8_t, uint16_t>;

template class vosa::Cast<int8_t, uint32_t>;

template class vosa::Cast<int8_t, int8_t>;

template class vosa::Cast<int8_t, int16_t>;

template class vosa::Cast<int8_t, int32_t>;

template class vosa::Cast<int8_t, float>;

template class vosa::Cast<int8_t, double>;

template class vosa::Cast<int16_t, bool>;

template class vosa::Cast<int16_t, uint8_t>;

template class vosa::Cast<int16_t, uint16_t>;

template class vosa::Cast<int16_t, uint32_t>;

template class vosa::Cast<int16_t, int8_t>;

template class vosa::Cast<int16_t, int16_t>;

template class vosa::Cast<int16_t, int32_t>;

template class vosa::Cast<int16_t, float>;

template class vosa::Cast<int16_t, double>;

template class vosa::Cast<int32_t, bool>;

template class vosa::Cast<int32_t, uint8_t>;

template class vosa::Cast<int32_t, uint16_t>;

template class vosa::Cast<int32_t, uint32_t>;

template class vosa::Cast<int32_t, int8_t>;

template class vosa::Cast<int32_t, int16_t>;

template class vosa::Cast<int32_t, int32_t>;

template class vosa::Cast<int32_t, float>;

template class vosa::Cast<int32_t, double>;

template class vosa::Cast<float, bool>;

template class vosa::Cast<float, uint8_t>;

template class vosa::Cast<float, uint16_t>;

template class vosa::Cast<float, uint32_t>;

template class vosa::Cast<float, int8_t>;

template class vosa::Cast<float, int16_t>;

template class vosa::Cast<float, int32_t>;

template class vosa::Cast<float, float>;

template class vosa::Cast<float, double>;

template class vosa::Cast<double, bool>;

template class vosa::Cast<double, uint8_t>;

template class vosa::Cast<double, uint16_t>;

template class vosa::Cast<double, uint32_t>;

template class vosa::Cast<double, int8_t>;

template class vosa::Cast<double, int16_t>;

template class vosa::Cast<double, int32_t>;

template class vosa::Cast<double, float>;

template class vosa::Cast<double, double>;
