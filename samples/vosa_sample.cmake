function(convert_tflite_to_tosa_mlir TFLITE_FILENAME MLIR_OUTPUT_FILENAME)

    find_program(IREE_IMPORT_TFLITE
            iree-import-tflite
            HINTS ${CMAKE_SOURCE_DIR}/venv/bin)

    if (NOT IREE_IMPORT_TFLITE)
        message(FATAL_ERROR "Failed to find iree-import-tflite (maybe try activating a venv with it available or otherwise getting it onto your path)")
    endif ()

    string(REGEX REPLACE ".tflite" ".mlir" MLIR_FILENAME ${TFLITE_FILENAME})
    message(STATUS "Creating target to create MLIR file at ${MLIR_FILENAME}")
    add_custom_command(
            OUTPUT ${MLIR_FILENAME}
            COMMAND ${IREE_IMPORT_TFLITE}
            ARGS ${TFLITE_FILENAME} -o ${MLIR_FILENAME} --output-format=mlir-ir
            DEPENDS ${TFLITE_FILENAME}
    )
    set(${MLIR_OUTPUT_FILENAME} ${MLIR_FILENAME} PARENT_SCOPE)
endfunction()

function(vosa_sample)

    set(options BAREMETAL)
    set(oneValueArgs NAME PIPELINE_NAME RMFB_FILENAME RETURN_VMFB_FILENAME RETURN_DOT_FILENAME RETURN_BAREMETAL_HEADER RETURN_BAREMETAL_SOURCE RETURN_BAREMETAL_VMFB_FILENAME RETURN_BAREMETAL_SVE_VMFB_FILENAME RETURN_BAREMETAL_SME_VMFB_FILENAME)
    set(multiValueArgs SOURCES TFLITE_ARTIFACTS GENERIC_ARTIFACTS)
    cmake_parse_arguments(
            VOSA_SAMPLE
            "${options}"
            "${oneValueArgs}"
            "${multiValueArgs}"
            ${ARGN}
    )

    if (NOT VOSA_SAMPLE_NAME)
        message(FATAL_ERROR "No NAME provided to vosa_sample")
    endif ()
    message(STATUS "Creating VOSA Library with name: ${VOSA_SAMPLE_NAME}")

    if (NOT VOSA_SAMPLE_PIPELINE_NAME)
        message(FATAL_ERROR "No PIPELINE_NAME provided to vosa_sample")
    endif ()
    message(STATUS "Creating MLIR module with pipeline name: ${VOSA_SAMPLE_PIPELINE_NAME}")

    if (NOT VOSA_SAMPLE_SOURCES)
        message(FATAL_ERROR "No SOURCES provided to vosa_sample")
    endif ()
    string(REPLACE ";" ", " HOST_SOURCES_STRING "${VOSA_SAMPLE_SOURCES}")

    if (VOSA_SAMPLE_RMFB_FILENAME)
        message(STATUS "Creating reference model flatbuffer at ${VOSA_SAMPLE_RMFB_FILENAME}")
    endif ()

    set(INPUT_ARTIFACTS_TARGETS)
    foreach (INPUT_ARTIFACT IN LISTS VOSA_SAMPLE_TFLITE_ARTIFACTS)
        message(STATUS "Creating target to move ${INPUT_ARTIFACT} to build dir")
        add_custom_command(
                OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${INPUT_ARTIFACT}
                COMMAND cp ${CMAKE_CURRENT_SOURCE_DIR}/${INPUT_ARTIFACT} ${CMAKE_CURRENT_BINARY_DIR}/${INPUT_ARTIFACT}
        )
        convert_tflite_to_tosa_mlir(
                ${CMAKE_CURRENT_BINARY_DIR}/${INPUT_ARTIFACT}
                MLIR_FILENAME)
        message(STATUS "Adding target for ${MLIR_FILENAME}")
        list(APPEND INPUT_ARTIFACTS_TARGETS ${MLIR_FILENAME})
    endforeach ()
    message(STATUS "TFLite input artifacts for ${VOSA_SAMPLE_NAME}: ${INPUT_ARTIFACTS_TARGETS}")

    set(GENERIC_INPUT_ARTIFACTS_TARGETS)
    foreach(INPUT_ARTIFACT IN LISTS VOSA_SAMPLE_GENERIC_ARTIFACTS)
        message(STATUS "Creating target to move ${INPUT_ARTIFACT} to build dir")
        add_custom_command(
                OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${INPUT_ARTIFACT}
                COMMAND cp ${CMAKE_CURRENT_SOURCE_DIR}/${INPUT_ARTIFACT} ${CMAKE_CURRENT_BINARY_DIR}/${INPUT_ARTIFACT}
        )
        list(APPEND GENERIC_INPUT_ARTIFACTS_TARGETS ${CMAKE_CURRENT_BINARY_DIR}/${INPUT_ARTIFACT})
    endforeach ()
    message(STATUS "Generic input artifacts for ${VOSA_SAMPLE_NAME}: ${GENERIC_INPUT_ARTIFACTS_TARGETS}")

    set(EXECUTABLE_NAME "${VOSA_SAMPLE_NAME}_executable")
    add_executable(${EXECUTABLE_NAME}
            ${VOSA_SAMPLE_SOURCES})

    target_link_libraries(${EXECUTABLE_NAME}
            PUBLIC
            vosa_api)

    set(VMFB_FILE "${VOSA_SAMPLE_NAME}.vmfb")
    set(RETURN_VMFB_FILENAME "${CMAKE_CURRENT_BINARY_DIR}/${VMFB_FILE}")
    set(RETURN_VMFB_FILENAME "${RETURN_VMFB_FILENAME}" PARENT_SCOPE)

    set(DOT_FILE "${VOSA_SAMPLE_NAME}.dot")
    set(RETURN_DOT_FILENAME "${CMAKE_CURRENT_BINARY_DIR}/${DOT_FILE}")
    set(RETURN_DOT_FILENAME "${RETURN_DOT_FILENAME}" PARENT_SCOPE)

    set(COMPILE_ARGS
            ${VOSA_SAMPLE_PIPELINE_NAME}
            ${VMFB_FILE}
            "--iree-flow-dump-dispatch-graph"
            "--iree-flow-dump-dispatch-graph-output-file=${DOT_FILE}"
            )
    set(OUTPUTS
            ${VMFB_FILE}
            ${DOT_FILE}
            )
    if (VOSA_SAMPLE_RMFB_FILENAME)
        list(APPEND COMPILE_ARGS "--rmfb=${VOSA_SAMPLE_RMFB_FILENAME}")
        list(APPEND OUTPUTS ${VOSA_SAMPLE_RMFB_FILENAME})
    endif ()
    if (VOSA_SAMPLE_BAREMETAL)
        set(IREE_INPUT_FILE "${VOSA_SAMPLE_NAME}.iree_input.mlir")
        list(APPEND COMPILE_ARGS "--iree-input=${IREE_INPUT_FILE}")
        list(APPEND OUTPUTS ${IREE_INPUT_FILE})
    endif ()
    add_custom_command(
            OUTPUT ${OUTPUTS}
            COMMAND ${EXECUTABLE_NAME}
            ARGS ${COMPILE_ARGS}
            DEPENDS ${EXECUTABLE_NAME} ${INPUT_ARTIFACTS_TARGETS} ${GENERIC_INPUT_ARTIFACTS_TARGETS}
    )

    set(EMBED_HEADER_FILE "${VOSA_SAMPLE_NAME}_embedding.h")
    set(EMBED_SOURCE_FILE "${VOSA_SAMPLE_NAME}_embedding.c")

    set(GENERATE_EMBED_DATA_ARGS)
    list(APPEND GENERATE_EMBED_DATA_ARGS "--output_header=${EMBED_HEADER_FILE}")
    list(APPEND GENERATE_EMBED_DATA_ARGS "--output_impl=${EMBED_SOURCE_FILE}")
    list(APPEND GENERATE_EMBED_DATA_ARGS "--identifier=${VOSA_SAMPLE_NAME}")
    list(APPEND GENERATE_EMBED_DATA_ARGS "--flatten")
    list(APPEND GENERATE_EMBED_DATA_ARGS "${VMFB_FILE}")

    # Embed VM bytecode module into c source file
    add_custom_command(
            OUTPUT
            ${EMBED_HEADER_FILE}
            ${EMBED_SOURCE_FILE}
            COMMAND $<TARGET_FILE:generate_embed_data> ${GENERATE_EMBED_DATA_ARGS}
            DEPENDS ${VMFB_FILE}
    )

    add_library(${VOSA_SAMPLE_NAME}
            ${EMBED_HEADER_FILE}
            ${EMBED_SOURCE_FILE})

    target_include_directories(${VOSA_SAMPLE_NAME}
            PUBLIC
            ${CMAKE_CURRENT_BINARY_DIR})

    if (VOSA_SAMPLE_BAREMETAL)
        set(BAREMETAL_VMFB_FILE "${VOSA_SAMPLE_NAME}.baremetal.vmfb")
        set(BAREMETAL_SVE_VMFB_FILE "${VOSA_SAMPLE_NAME}.baremetal.sve.vmfb")
        set(BAREMETAL_SME_VMFB_FILE "${VOSA_SAMPLE_NAME}.baremetal.sme.vmfb")

        set(BAREMETAL_IREE_COMPILE_FLAGS
                "${IREE_INPUT_FILE}"
                "--iree-llvmcpu-target-triple=aarch64"
                "--output-format=vm-bytecode"
                "--iree-hal-target-backends=llvm-cpu"
                "-o"
                "${BAREMETAL_VMFB_FILE}")
        set(BAREMETAL_SVE_IREE_COMPILE_FLAGS
                "${IREE_INPUT_FILE}"
                "--iree-llvmcpu-target-triple=aarch64"
                "--output-format=vm-bytecode"
                "--iree-hal-target-backends=llvm-cpu"
                "--iree-llvmcpu-target-cpu-features=+sve"
                "-o"
                "${BAREMETAL_SVE_VMFB_FILE}")
        set(BAREMETAL_SME_IREE_COMPILE_FLAGS
                "${IREE_INPUT_FILE}"
                "--iree-llvmcpu-target-triple=aarch64"
                "--output-format=vm-bytecode"
                "--iree-hal-target-backends=llvm-cpu"
                "--iree-llvmcpu-target-cpu-features=+sve,+sme"
                "-o"
                "${BAREMETAL_SME_VMFB_FILE}")
        add_custom_command(
                OUTPUT
                ${BAREMETAL_VMFB_FILE}
                COMMAND $<TARGET_FILE:iree-compile> ${BAREMETAL_IREE_COMPILE_FLAGS}
                DEPENDS ${IREE_INPUT_FILE}
        )
        add_custom_command(
                OUTPUT
                ${BAREMETAL_SVE_VMFB_FILE}
                COMMAND $<TARGET_FILE:iree-compile> ${BAREMETAL_SVE_IREE_COMPILE_FLAGS}
                DEPENDS ${IREE_INPUT_FILE}
        )
        add_custom_command(
                OUTPUT
                ${BAREMETAL_SME_VMFB_FILE}
                COMMAND $<TARGET_FILE:iree-compile> ${BAREMETAL_SME_IREE_COMPILE_FLAGS}
                DEPENDS ${IREE_INPUT_FILE}
        )

        set(BAREMETAL_EMBED_HEADER_FILE "${VOSA_SAMPLE_NAME}_embedding.baremetal.h")
        set(BAREMETAL_EMBED_SOURCE_FILE "${VOSA_SAMPLE_NAME}_embedding.baremetal.c")

        set(BAREMETAL_GENERATE_EMBED_DATA_ARGS)
        list(APPEND BAREMETAL_GENERATE_EMBED_DATA_ARGS "--output_header=${BAREMETAL_EMBED_HEADER_FILE}")
        list(APPEND BAREMETAL_GENERATE_EMBED_DATA_ARGS "--output_impl=${BAREMETAL_EMBED_SOURCE_FILE}")
        list(APPEND BAREMETAL_GENERATE_EMBED_DATA_ARGS "--identifier=${VOSA_SAMPLE_NAME}")
        list(APPEND BAREMETAL_GENERATE_EMBED_DATA_ARGS "--flatten")
        list(APPEND BAREMETAL_GENERATE_EMBED_DATA_ARGS "${BAREMETAL_VMFB_FILE}")

        # Embed VM bytecode module into c source file
        add_custom_command(
                OUTPUT
                ${BAREMETAL_EMBED_HEADER_FILE}
                ${BAREMETAL_EMBED_SOURCE_FILE}
                COMMAND $<TARGET_FILE:generate_embed_data> ${BAREMETAL_GENERATE_EMBED_DATA_ARGS}
                DEPENDS ${BAREMETAL_VMFB_FILE}
        )

        set(RETURN_BAREMETAL_HEADER "${CMAKE_CURRENT_BINARY_DIR}/${BAREMETAL_EMBED_HEADER_FILE}")
        set(RETURN_BAREMETAL_HEADER "${RETURN_BAREMETAL_HEADER}" PARENT_SCOPE)

        set(RETURN_BAREMETAL_SOURCE "${CMAKE_CURRENT_BINARY_DIR}/${BAREMETAL_EMBED_SOURCE_FILE}")
        set(RETURN_BAREMETAL_SOURCE "${RETURN_BAREMETAL_SOURCE}" PARENT_SCOPE)

        set(RETURN_BAREMETAL_VMFB_FILENAME "${CMAKE_CURRENT_BINARY_DIR}/${BAREMETAL_VMFB_FILE}")
        set(RETURN_BAREMETAL_VMFB_FILENAME "${RETURN_BAREMETAL_VMFB_FILENAME}" PARENT_SCOPE)

        set(RETURN_BAREMETAL_SVE_VMFB_FILENAME "${CMAKE_CURRENT_BINARY_DIR}/${BAREMETAL_SVE_VMFB_FILE}")
        set(RETURN_BAREMETAL_SVE_VMFB_FILENAME "${RETURN_BAREMETAL_SVE_VMFB_FILENAME}" PARENT_SCOPE)

        set(RETURN_BAREMETAL_SME_VMFB_FILENAME "${CMAKE_CURRENT_BINARY_DIR}/${BAREMETAL_SME_VMFB_FILE}")
        set(RETURN_BAREMETAL_SME_VMFB_FILENAME "${RETURN_BAREMETAL_SME_VMFB_FILENAME}" PARENT_SCOPE)

        add_custom_target(${VOSA_SAMPLE_NAME}_baremetal ALL
                DEPENDS
                ${BAREMETAL_EMBED_HEADER_FILE}
                ${BAREMETAL_EMBED_SOURCE_FILE}
                ${BAREMETAL_VMFB_FILE}
                ${BAREMETAL_SVE_VMFB_FILE}
                ${BAREMETAL_SME_VMFB_FILE})
    endif ()

endfunction()