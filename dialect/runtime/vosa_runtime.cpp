/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "mlir/ExecutionEngine/CRunnerUtils.h"

#include <fstream>
#include <iostream>
#include <sstream>

// TODO: Use environment variables for output directory, file name
// prefix/suffix
// TODO: Ensure filenames are valid - node names are freeform

template <typename T>
void dump_memref_dim(std::ostream &out, T *base, int64_t dim, int64_t offset,
                     const int64_t *sizes, const int64_t *strides) {
  if (dim == 0) {
    out.write(reinterpret_cast<const char *>(&base[offset]), sizeof(T));
  } else {
    for (unsigned i = 0; i < sizes[0]; ++i) {
      dump_memref_dim(out, base, dim - 1, offset + i * strides[0], sizes + 1,
                      strides + 1);
    }
  }
}

/**
 * Dump a memref to a file
 *
 * Writes the contents of the memref to "dump_${name}.raw"
 * Also print information about the shape of the memref to "dump_${name}.txt"
 */
template <typename T>
void dump_memref(UnrankedMemRefType<T> *buf, const char *name) {
  // unknown rank not supported
  if (buf->rank <= 0) {
    return;
  }

  DynamicMemRefType<T> memref(*buf);

  std::ostringstream dataFnS;
  dataFnS << "dump_" << name << ".raw";
  std::string dataFn = dataFnS.str();

  std::ostringstream infoFnS;
  infoFnS << "dump_" << name << ".json";
  std::string infoFn = infoFnS.str();

  std::ofstream infoOut(infoFn.c_str());
  infoOut << "{ \"id\": \"" << name << "\"," << std::endl;
  infoOut << "  \"rank\": " << buf->rank << "," << std::endl;
  infoOut << "  \"offset\": " << memref.offset << "," << std::endl;
  size_t elementCount = 0;
  infoOut << "  \"dimensions\": [" << memref.sizes[0];
  elementCount = memref.sizes[0];
  for (int i = 1; i < buf->rank; ++i) {
    infoOut << ", " << memref.sizes[i];
    elementCount *= memref.sizes[i];
  }
  infoOut << "]," << std::endl;
  infoOut << "  \"strides\": [" << memref.strides[0];
  for (int i = 1; i < buf->rank; ++i) {
    infoOut << ", " << memref.strides[i];
  }
  infoOut << "]," << std::endl;
  infoOut << "  \"element_count\":" << elementCount << "," << std::endl;
  infoOut << "  \"element_size\": " << sizeof(T) << "," << std::endl;
  size_t dataSize = elementCount * sizeof(T);
  infoOut << "  \"data_size\":" << dataSize << " }" << std::endl;
  infoOut.close();

  if (dataSize > 0) {
    // write data
    std::ofstream fout;
    fout.open(dataFn.c_str(), std::ofstream::binary | std::ofstream::out);
    if (!fout.good()) {
      std::cerr << "Failed to open: " << dataFn << std::endl;
      return;
    }
    dump_memref_dim(fout, memref.data, buf->rank, memref.offset, memref.sizes,
                    memref.strides);
    fout.close();
  }
}

/**
 * Dump a value to a text file
 *
 * Write the text form of the value to "dump_${name}.txt"
 */
template <typename T>
void dump_value(T val, const char *name) {
  std::ostringstream infoFnS;
  infoFnS << "dump_" << name << ".json";
  std::string infoFn = infoFnS.str();

  std::ofstream infoOut(infoFn.c_str());
  infoOut << "{ \"id\": \"" << name << "\", \"value\": \"" << val << "\" }"
          << std::endl;
  infoOut.close();
}

// Typedefs of UnrankedMemRefType for U8, U16, U32, F32
typedef UnrankedMemRefType<uint8_t> UnrankedMemRefU1Type;
typedef UnrankedMemRefType<uint8_t> UnrankedMemRefU8Type;
typedef UnrankedMemRefType<uint16_t> UnrankedMemRefU16Type;
typedef UnrankedMemRefType<uint32_t> UnrankedMemRefU32Type;
typedef UnrankedMemRefType<float> UnrankedMemRefF32Type;

// MLIR C interface to dump i8 tensor (signed / unsigned integer)
extern "C" void _mlir_ciface_dump_tensor_i1(UnrankedMemRefU1Type *buf,
                                            const char *name) {
  dump_memref(buf, name);
}

// MLIR C interface to dump i8 tensor (signed / unsigned integer)
extern "C" void _mlir_ciface_dump_tensor_i8(UnrankedMemRefU8Type *buf,
                                            const char *name) {
  dump_memref(buf, name);
}

// MLIR C interface to dump i16 tensor (signed / unsigned integer)
extern "C" void _mlir_ciface_dump_tensor_i16(UnrankedMemRefU16Type *buf,
                                             const char *name) {
  dump_memref(buf, name);
}

// MLIR C interface to dump i32 tensor (signed / unsigned integer)
extern "C" void _mlir_ciface_dump_tensor_i32(UnrankedMemRefU32Type *buf,
                                             const char *name) {
  dump_memref(buf, name);
}

// MLIR C interface to dump f32 tensor
extern "C" void _mlir_ciface_dump_tensor_f32(UnrankedMemRefF32Type *buf,
                                             const char *name) {
  dump_memref(buf, name);
}

// MLIR C interface to dump si8 value
extern "C" void _mlir_ciface_dump_ui8(uint8_t val, const char *name) {
  dump_value(val, name);
}

// MLIR C interface to dump ui16 value
extern "C" void _mlir_ciface_dump_ui16(uint16_t val, const char *name) {
  dump_value(val, name);
}

// MLIR C interface to dump ui32 value
extern "C" void _mlir_ciface_dump_ui32(uint32_t val, const char *name) {
  dump_value(val, name);
}

// MLIR C interface to dump si8 value
extern "C" void _mlir_ciface_dump_si8(int8_t val, const char *name) {
  dump_value(val, name);
}

// MLIR C interface to dump si16 value
extern "C" void _mlir_ciface_dump_si16(int16_t val, const char *name) {
  dump_value(val, name);
}

// MLIR C interface to dump si32 value
extern "C" void _mlir_ciface_dump_si32(int32_t val, const char *name) {
  dump_value(val, name);
}

// MLIR C interface to dump f32 value
extern "C" void _mlir_ciface_dump_f32(float val, const char *name) {
  dump_value(val, name);
}
