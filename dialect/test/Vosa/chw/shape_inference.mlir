// RUN: vosa-opt -vosa-data-format=chw -vosa-infer-shapes -split-input-file -verify-diagnostics %s | FileCheck %s

module {
    // CHECK-LABEL: func.func @broadcast_channelwise({{.*}})
    func.func @broadcast_channelwise(%argA: tensor<1x960x540xui8>) {

        // CHECK: %{{.*}} = vosa.broadcast_channelwise %{{.*}} : tensor<1x960x540xui8> -> tensor<3x960x540xui8>
        %res1 = vosa.broadcast_channelwise %argA { size = 3 : i32 } : tensor<1x960x540xui8> -> tensor<?x?x?xui8>

        // CHECK: %{{.*}} = vosa.broadcast_channelwise %{{.*}} : tensor<1x960x540xui8> -> tensor<3x960x540xui8>
        %res2 = vosa.broadcast_channelwise %argA { size = 3 : i32 } : tensor<1x960x540xui8> -> tensor<3x?x?xui8>

        // CHECK: %{{.*}} = vosa.broadcast_channelwise %{{.*}} : tensor<1x960x540xui8> -> tensor<3x960x540xui8>
        %res3 = vosa.broadcast_channelwise %argA { size = 3 : i32 } : tensor<1x960x540xui8> -> tensor<?x960x?xui8>

        // TODO: can't infer partially specified inputs
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @broadcast_planewise()
    func.func @broadcast_planewise() {
        %c0_si8 = vosa.constant dense<1> : tensor<1x1x1xsi8>

        // CHECK: %{{.*}} = vosa.broadcast_planewise %{{.*}} : tensor<1x1x1xsi8> -> tensor<1x100x200xsi8>
        %res_si8 = vosa.broadcast_planewise %c0_si8 { size = [ 100 : i32, 200 : i32 ] } : tensor<1x1x1xsi8> -> tensor<?x?x?xsi8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @cast({{.*}})
    func.func @cast(%argA: tensor<3x540x960xui8>) {

        // CHECK: %{{.*}} = vosa.cast %{{.*}} wrap : tensor<3x540x960xui8> -> tensor<3x540x960xsi32>
        %res0 = vosa.cast %argA wrap : tensor<3x540x960xui8> -> tensor<?x?x?xsi32>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @channel_extract({{.*}})
    func.func @channel_extract(%argA: tensor<3x540x960xui8>) {

        // CHECK: %{{.*}} = vosa.channel_extract %{{.*}} {channels = [0 : i32]} : tensor<3x540x960xui8> -> tensor<1x540x960xui8>
        %res0 = vosa.channel_extract %argA {channels = [ 0 : i32 ]} : tensor<3x540x960xui8> -> tensor<?x?x?xui8>

        // CHECK: %{{.*}} = vosa.channel_extract %{{.*}} {channels = [0 : i32, 2 : i32]} : tensor<3x540x960xui8> -> tensor<2x540x960xui8>
        %res1 = vosa.channel_extract %argA {channels = [ 0 : i32, 2: i32 ]} : tensor<3x540x960xui8> -> tensor<?x?x?xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @concat()
    func.func @concat() {

        %c1_si8 = vosa.constant dense<1> : tensor<1x100x200xsi8>
        %c2_si8 = vosa.constant dense<1> : tensor<2x100x200xsi8>
        %c3_si8 = vosa.constant dense<1> : tensor<3x100x200xsi8>

        // CHECK: %{{.*}} = vosa.concat %{{.*}} : tensor<1x100x200xsi8> -> tensor<1x100x200xsi8>
        %res1 = vosa.concat %c1_si8 : tensor<1x100x200xsi8> -> tensor<?x?x?xsi8>

        // CHECK: %{{.*}} = vosa.concat %{{.*}}, %{{.*}} : tensor<1x100x200xsi8>, tensor<2x100x200xsi8> -> tensor<3x100x200xsi8>
        %res2 = vosa.concat %c1_si8, %c2_si8 : tensor<1x100x200xsi8>, tensor<2x100x200xsi8> -> tensor<?x?x?xsi8>

        // CHECK: %{{.*}} = vosa.concat %{{.*}}, %{{.*}}, %{{.*}} : tensor<1x100x200xsi8>, tensor<2x100x200xsi8>, tensor<3x100x200xsi8> -> tensor<6x100x200xsi8>
        %res3 = vosa.concat %c1_si8, %c2_si8, %c3_si8 : tensor<1x100x200xsi8>, tensor<2x100x200xsi8>, tensor<3x100x200xsi8> -> tensor<?x?x?xsi8>

        return
    }
}

// -----

module {
    func.func @mesh_grid() {
        // CHECK: %{{.*}} = vosa.mesh_grid {shape = [100 : i32, 200 : i32]} : tensor<2x100x200xui32>
        %res = vosa.mesh_grid {shape = [ 100 : i32, 200 : i32 ]} : tensor<?x?x?xui32>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @add({{.*}})
    func.func @add(%argA: tensor<3x960x540xui8>, %argB: tensor<3x960x540xui8>) {

        // CHECK: %{{.*}} = vosa.add %{{.*}}, %{{.*}} : tensor<3x960x540xui8>, tensor<3x960x540xui8> -> tensor<3x960x540xui8>
        %res0 = vosa.add %argA, %argB : tensor<3x960x540xui8>, tensor<3x960x540xui8> -> tensor<?x?x?xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @div({{.*}})
    func.func @div(%argA: tensor<3x960x540xf32>, %argB: tensor<3x960x540xf32>) {

        // CHECK: %{{.*}} = vosa.div %{{.*}}, %{{.*}} : tensor<3x960x540xf32>, tensor<3x960x540xf32> -> tensor<3x960x540xf32>
        %res0 = vosa.div %argA, %argB : tensor<3x960x540xf32>, tensor<3x960x540xf32> -> tensor<?x?x?xf32>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @mult({{.*}})
    func.func @mult(%argA: tensor<3x960x540xf32>, %argB: tensor<3x960x540xf32>) {

        // CHECK: %{{.*}} = vosa.mult %{{.*}}, %{{.*}} : tensor<3x960x540xf32>, tensor<3x960x540xf32> -> tensor<3x960x540xf32>
        %res0 = vosa.mult %argA, %argB : tensor<3x960x540xf32>, tensor<3x960x540xf32> -> tensor<?x?x?xf32>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @sub({{.*}})
    func.func @sub(%argA: tensor<3x960x540xf32>, %argB: tensor<3x960x540xf32>) {

        // CHECK: %{{.*}} = vosa.sub %{{.*}}, %{{.*}} : tensor<3x960x540xf32>, tensor<3x960x540xf32> -> tensor<3x960x540xf32>
        %res0 = vosa.sub %argA, %argB : tensor<3x960x540xf32>, tensor<3x960x540xf32> -> tensor<?x?x?xf32>

        return
    }

    // CHECK-LABEL: func.func @sub_i({{.*}})
    func.func @sub_i(%argA: tensor<3x960x540xui8>, %argB: tensor<3x960x540xui8>) {

        // CHECK: %{{.*}} = vosa.sub %{{.*}}, %{{.*}} : tensor<3x960x540xui8>, tensor<3x960x540xui8> -> tensor<3x960x540xui8>
        %res0 = vosa.sub %argA, %argB : tensor<3x960x540xui8>, tensor<3x960x540xui8> -> tensor<?x?x?xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @and({{.*}})
    func.func @and(%argA: tensor<3x960x540xui8>, %argB: tensor<3x960x540xui8>) {

        // CHECK: %{{.*}} = vosa.and %{{.*}}, %{{.*}} : tensor<3x960x540xui8>, tensor<3x960x540xui8> -> tensor<3x960x540xui8>
        %res0 = vosa.and %argA, %argB : tensor<3x960x540xui8>, tensor<3x960x540xui8> -> tensor<?x?x?xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @abs_diff({{.*}})
    func.func @abs_diff(%argA: tensor<3x960x540xui8>, %argB: tensor<3x960x540xui8>) {

        // CHECK: %{{.*}} = vosa.abs_diff %{{.*}}, %{{.*}} : tensor<3x960x540xui8>, tensor<3x960x540xui8> -> tensor<3x960x540xui8>
        %res0 = vosa.abs_diff %argA, %argB : tensor<3x960x540xui8>, tensor<3x960x540xui8> -> tensor<?x?x?xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @where({{.*}})
    func.func @where(%argA: tensor<3x100x200xui8>, %argB: tensor<3x100x200xui8>, %mask: tensor<3x100x200xi1>) {
        // CHECK: %{{.*}} = vosa.where %{{.*}}, %{{.*}}, %{{.*}} : tensor<3x100x200xui8>, tensor<3x100x200xui8>, tensor<3x100x200xi1> -> tensor<3x100x200xui8>
        %res_ui8 = vosa.where %argA, %argB, %mask : tensor<3x100x200xui8>, tensor<3x100x200xui8>, tensor<3x100x200xi1> -> tensor<?x?x?xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @abs({{.*}})
    func.func @abs(%argA: tensor<3x960x540xsi8>) {

        // CHECK: %{{.*}} = vosa.abs %{{.*}} : tensor<3x960x540xsi8> -> tensor<3x960x540xsi8>
        %res0 = vosa.abs %argA : tensor<3x960x540xsi8> -> tensor<?x?x?xsi8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @arithmetic_shift_right({{.*}})
    func.func @arithmetic_shift_right(%argA: tensor<3x960x540xsi8>) {

        // CHECK: %{{.*}} = vosa.arithmetic_shift_right %{{.*}} : tensor<3x960x540xsi8> -> tensor<3x960x540xsi8>
        %res0 = vosa.arithmetic_shift_right %argA { shift = 2 : i32 } : tensor<3x960x540xsi8> -> tensor<?x?x?xsi8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @logical_shift_right({{.*}})
    func.func @logical_shift_right(%argA: tensor<3x960x540xui8>) {

        // CHECK: %{{.*}} = vosa.logical_shift_right %{{.*}} : tensor<3x960x540xui8> -> tensor<3x960x540xui8>
        %res0 = vosa.logical_shift_right %argA { shift = 2 : i32 } : tensor<3x960x540xui8> -> tensor<?x?x?xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @clamp({{.*}})
    func.func @clamp(%argA: tensor<3x960x540xsi8>) {
        %min_si8 = vosa.constant 10 : si8
        %max_si8 = vosa.constant 10 : si8

        // CHECK: %{{.*}} = vosa.clamp %{{.*}}, %{{.*}}, %{{.*}} : tensor<3x960x540xsi8>, si8, si8 -> tensor<3x960x540xsi8>
        %res0 = vosa.clamp %argA, %min_si8, %max_si8 : tensor<3x960x540xsi8>, si8, si8 -> tensor<?x?x?xsi8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @conv_2d({{.*}})
    func.func @conv_2d(%argA: tensor<3x960x540xsi32>) {
        // CHECK: %{{.*}} = vosa.conv_2d %{{.*}} {filter = {{.*}}} : tensor<3x960x540xsi32> -> tensor<3x958x538xsi32>
        %res_si32 = vosa.conv_2d %argA { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi32> } : tensor<3x960x540xsi32> -> tensor<?x?x?xsi32>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @decimate()
    func.func @decimate() {
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.decimate %{{.*}} : tensor<3x100x200xsi8> -> tensor<3x50x50xsi8>
        %res0 = vosa.decimate %t_si8 { N = [ 2 : i32, 4 : i32 ], offsets = [ 0 : i32, 0 : i32 ] } : tensor<3x100x200xsi8> -> tensor<?x?x?xsi8>

        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.decimate %{{.*}} : tensor<3x100x200xsi32> -> tensor<3x25x100xsi32>
        %res1 = vosa.decimate %t_si32 { N = [ 4 : i32, 2 : i32 ], offsets = [ 1 : i32, 1 : i32 ] } : tensor<3x100x200xsi32> -> tensor<?x?x?xsi32>

        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.decimate %{{.*}} : tensor<3x100x200xf32> -> tensor<3x34x67xf32>
        %res2 = vosa.decimate %t_f32 { N = [ 3 : i32, 3 : i32 ], offsets = [ 2 : i32, 2 : i32 ] } : tensor<3x100x200xf32> -> tensor<?x?x?xf32>

        return
    }
}
// -----

module {
    // CHECK-LABEL: func.func @pad({{.*}})
    func.func @pad(%argA: tensor<3x960x540xsi8>) {
        // CHECK: %{{.*}} = vosa.pad {{.*}} : tensor<3x960x540xsi8> -> tensor<3x963x552xsi8>
        %res_si8 = vosa.pad %argA constant { pad_constant = 0 : si8, pad_size = [ 1 : i32, 2 : i32, 4 : i32, 8 : i32 ] } : tensor<3x960x540xsi8> -> tensor<?x?x?xsi8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @resize_bilinear({{.*}})
    func.func @resize_bilinear(%argA: tensor<3x100x200xf16>) {

        // CHECK: %{{.*}} = vosa.resize_bilinear %{{.*}} {size = [150 : i32, 300 : i32]} : tensor<3x100x200xf16> -> tensor<3x150x300xf16>
        %res0 = vosa.resize_bilinear %argA { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xf16> -> tensor<?x?x?xf16>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @resize_nearest_neighbour({{.*}})
    func.func @resize_nearest_neighbour(%argA: tensor<3x100x200xf32>) {
	// CHECK: %{{.*}} vosa.resize_nearest_neighbour %{{.*}} {size = [150 : i32, 300 : i32]} : tensor<3x100x200xf32> -> tensor<3x150x300xf32>
	%res0 = vosa.resize_nearest_neighbour %argA { size = [ 150 : i32, 300 : i32 ] } : tensor<3x100x200xf32> -> tensor<?x?x?xf32>

	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @arg_max_channelwise({{.*}})
    func.func @arg_max_channelwise(%argA: tensor<3x100x200xsi8>) {

        // CHECK: %{{.*}} = vosa.arg_max_channelwise %{{.*}} : tensor<3x100x200xsi8> -> tensor<1x100x200xui32>
        %res0 = vosa.arg_max_channelwise %argA : tensor<3x100x200xsi8> -> tensor<?x?x?xui32>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @arg_min_channelwise({{.*}})
    func.func @arg_min_channelwise(%argA: tensor<3x100x200xsi8>) {

        // CHECK: %{{.*}} = vosa.arg_min_channelwise %{{.*}} : tensor<3x100x200xsi8> -> tensor<1x100x200xui32>
        %res0 = vosa.arg_min_channelwise %argA : tensor<3x100x200xsi8> -> tensor<?x?x?xui32>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @reduce_interp_channelwise({{.*}})
    func.func @reduce_interp_channelwise(%argA: tensor<3x100x200xsi8>, %s : tensor<1x100x200xf32>) {

        // CHECK: %{{.*}} = vosa.reduce_interp_channelwise %{{.*}}, %{{.*}} : tensor<3x100x200xsi8>, tensor<1x100x200xf32> -> tensor<1x100x200xf32>
        %res0 = vosa.reduce_interp_channelwise %argA, %s : tensor<3x100x200xsi8>, tensor<1x100x200xf32> -> tensor<?x?x?xf32>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @reduce_max_channelwise({{.*}})
    func.func @reduce_max_channelwise(%argA: tensor<3x100x200xsi8>) {

        // CHECK: %{{.*}} = vosa.reduce_max_channelwise %{{.*}} : tensor<3x100x200xsi8> -> tensor<1x100x200xsi8>
        %res0 = vosa.reduce_max_channelwise %argA : tensor<3x100x200xsi8> -> tensor<?x?x?xsi8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @reduce_min_channelwise({{.*}})
    func.func @reduce_min_channelwise(%argA: tensor<3x100x200xsi8>) {

        // CHECK: %{{.*}} = vosa.reduce_min_channelwise %{{.*}} : tensor<3x100x200xsi8> -> tensor<1x100x200xsi8>
        %res0 = vosa.reduce_min_channelwise %argA : tensor<3x100x200xsi8> -> tensor<?x?x?xsi8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @reduce_sum_channelwise({{.*}})
    func.func @reduce_sum_channelwise(%argA: tensor<3x100x200xsi8>) {

        // CHECK: %{{.*}} = vosa.reduce_sum_channelwise %{{.*}} : tensor<3x100x200xsi8> -> tensor<1x100x200xsi8>
        %res0 = vosa.reduce_sum_channelwise %argA : tensor<3x100x200xsi8> -> tensor<?x?x?xsi8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @greater_equal({{.*}})
    func.func @greater_equal(%argA: tensor<3x100x200xsi8>, %argB: tensor<3x100x200xsi8>) {
        //CHECK: %{{.*}} = vosa.greater_equal %{{.*}}, %{{.*}}: tensor<3x100x200xsi8>, tensor<3x100x200xsi8> -> tensor<3x100x200xi1>
        %res0 = vosa.greater_equal %argA, %argB : tensor<3x100x200xsi8>, tensor<3x100x200xsi8> -> tensor<?x?x?xi1>
        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @max({{.*}})
    func.func @max(%argA: tensor<3x100x200xsi8>, %argB: tensor<3x100x200xsi8>) {
    	//CHECK: %{{.*}} = vosa.max %{{.*}}, %{{.*}}: tensor<3x100x200xsi8>, tensor<3x100x200xsi8> -> tensor<3x100x200xsi8>
	%res0 = vosa.max %argA, %argB : tensor<3x100x200xsi8>, tensor<3x100x200xsi8> -> tensor<?x?x?xsi8>
	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @min({{.*}})
    func.func @min(%argA: tensor<3x100x200xsi8>, %argB: tensor<3x100x200xsi8>) {
    	//CHECK: %{{.*}} = vosa.min %{{.*}}, %{{.*}}: tensor<3x100x200xsi8>, tensor<3x100x200xsi8> -> tensor<3x100x200xsi8>
	%res0 = vosa.min %argA, %argB : tensor<3x100x200xsi8>, tensor<3x100x200xsi8> -> tensor<?x?x?xsi8>
	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @sqrt({{.*}})
    func.func @sqrt(%argA: tensor<3x100x200xf32>) {
	//CHECK: %{{.*}} = vosa.sqrt %{{.*}} : tensor<3x100x200xf32> -> tensor<3x100x200xf32>
  	%res0 = vosa.sqrt %argA : tensor<3x100x200xf32> -> tensor<?x?x?xf32>
	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @gamma_correction({{.*}})
    func.func @gamma_correction(%argA: tensor<3x100x200xf32>) {
	//CHECK: %{{.*}} = vosa.gamma_correction %{{.*}} {{.*}} : tensor<3x100x200xf32> -> tensor<3x100x200xf32>
	%res0 = vosa.gamma_correction %argA { gamma = 2.2 : f32 } : tensor<3x100x200xf32> -> tensor<?x?x?xf32>
	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @power({{.*}})
    func.func @power(%argA: tensor<3x100x200xf32>) {
	//CHECK: %{{.*}} = vosa.power %{{.*}} {{.*}}: tensor<3x100x200xf32> -> tensor<3x100x200xf32>
	%res0 = vosa.power %argA { base = 2.0 : f32 } : tensor<3x100x200xf32> -> tensor<?x?x?xf32>
	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @rotate90({{.*}})
    func.func @rotate90(%argA: tensor<3x100x200xf32>) {
	//CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {rotate = 0 : si32} : tensor<3x100x200xf32> -> tensor<3x100x200xf32>
        %res0 = vosa.rotate_90 %argA {rotate = 0 : si32} : tensor<3x100x200xf32> -> tensor<?x?x?xf32>
	//CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {rotate = 1 : si32} : tensor<3x100x200xf32> -> tensor<3x200x100xf32>
	%res1 = vosa.rotate_90 %argA {rotate = 1 : si32} : tensor<3x100x200xf32> -> tensor<?x?x?xf32>
	//CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {rotate = 2 : si32} : tensor<3x100x200xf32> -> tensor<3x100x200xf32>
        %res2 = vosa.rotate_90 %argA {rotate = 2 : si32} : tensor<3x100x200xf32> -> tensor<?x?x?xf32>
	//CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {rotate = 3 : si32} : tensor<3x100x200xf32> -> tensor<3x200x100xf32>
	%res3 = vosa.rotate_90 %argA {rotate = 3 : si32} : tensor<3x100x200xf32> -> tensor<?x?x?xf32>
	//CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {rotate = -1 : si32} : tensor<3x100x200xf32> -> tensor<3x200x100xf32>
	%res4 = vosa.rotate_90 %argA {rotate = -1 : si32} : tensor<3x100x200xf32> -> tensor<?x?x?xf32>
	//CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {rotate = -2 : si32} : tensor<3x100x200xf32> -> tensor<3x100x200xf32>
        %res5 = vosa.rotate_90 %argA {rotate = -2 : si32} : tensor<3x100x200xf32> -> tensor<?x?x?xf32>
	//CHECK: %{{.*}} = vosa.rotate_90 %{{.*}} {rotate = -3 : si32} : tensor<3x100x200xf32> -> tensor<3x200x100xf32>
	%res6 = vosa.rotate_90 %argA {rotate = -3 : si32} : tensor<3x100x200xf32> -> tensor<?x?x?xf32>
	return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @expand()
    func.func @expand() {
        %expand_kernel = vosa.constant dense<[[[1, 0, 0, 0, 0], [0, 1, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 1, 0], [0, 0, 0, 0, 2]]]> : tensor<1x5x5xui8>
        %t_ui8 = vosa.constant dense<42> : tensor<3x100x200xui8>
        // CHECK: %{{.*}} = vosa.expand %{{.*}}, %{{.*}} {{.*}} : tensor<3x100x200xui8>, tensor<1x5x5xui8> -> tensor<3x125x250xui8>
        %res_ui8 = vosa.expand %t_ui8, %expand_kernel {gather_kernel = dense<[[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 2]]> : tensor<4x4xui8>, fill_value = 0 : ui8} : tensor<3x100x200xui8>, tensor<1x5x5xui8> -> tensor<?x?x?xui8>

        return
    }
}

// -----

module {
    // CHECK-LABEL: func.func @pointwise_matrix_multiply()
    func.func @pointwise_matrix_multiply() {

        %t0_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        %t2_f32 = vosa.constant dense<42.0> : tensor<2x100x200xf32>

        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<3xf32>, K_2 = dense<{{.*}}> : tensor<3xf32>, M = dense<{{.*}}> : tensor<3x3xf32>} : tensor<3x100x200xf32> -> tensor<3x100x200xf32>
        %res_f32 = vosa.pointwise_matrix_multiply %t0_f32 {M=dense<[[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]> : tensor<3x3xf32>, K_1=dense<[0.0, 0.0, 0.0]> : tensor<3xf32>, K_2=dense<[0.0, 0.0, 0.0]> : tensor<3xf32>} : tensor<3x100x200xf32> -> tensor<?x?x?xf32>

        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<2xf32>, K_2 = dense<{{.*}}> : tensor<2xf32>, M = dense<{{.*}}> : tensor<2x2xf32>} : tensor<2x100x200xf32> -> tensor<2x100x200xf32>
        %res2_f32 = vosa.pointwise_matrix_multiply %t2_f32 {M=dense<[[1.0, 0.0], [0.0, 1.0]]> : tensor<2x2xf32>, K_1=dense<[0.0, 0.0]> : tensor<2xf32>, K_2=dense<[0.0, 0.0]> : tensor<2xf32>} : tensor<2x100x200xf32> -> tensor<?x?x?xf32>

        // different in / out channels
        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<2xf32>, K_2 = dense<{{.*}}> : tensor<3xf32>, M = dense<{{.*}}> : tensor<3x2xf32>} : tensor<2x100x200xf32> -> tensor<3x100x200xf32>
        %res3_f32 = vosa.pointwise_matrix_multiply %t2_f32 {M=dense<[[1.0, 0.0], [0.0, 1.0], [0.5, 0.5]]> : tensor<3x2xf32>, K_1=dense<[0.0, 0.0]> : tensor<2xf32>, K_2=dense<[0.0, 0.0, 0.0]> : tensor<3xf32>} : tensor<2x100x200xf32> -> tensor<?x?x?xf32>

        // CHECK: %{{.*}} = vosa.pointwise_matrix_multiply %{{.*}} {K_1 = dense<{{.*}}> : tensor<2xf32>, K_2 = dense<{{.*}}> : tensor<1xf32>, M = dense<{{.*}}> : tensor<1x2xf32>} : tensor<2x100x200xf32> -> tensor<1x100x200xf32>
        %res4_f32 = vosa.pointwise_matrix_multiply %t2_f32 {M=dense<[[1.0, 0.0]]> : tensor<1x2xf32>, K_1=dense<[0.0, 0.0]> : tensor<2xf32>, K_2=dense<[0.0]> : tensor<1xf32>} : tensor<2x100x200xf32> -> tensor<?x?x?xf32>

        return
    }
}
