/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <vector>

#include "iree/base/api.h"
#include "iree/hal/api.h"
#include "iree/runtime/api.h"

#include "bokeh_io.h"
#include "ppm_utils.h"

#include "bokeh_sample_builder_embedding.h"

int main(int argc, char *argv[]) {

  std::string binary_mask_input_file(argv[1]);
  std::string depth_map_input_file(argv[2]);
  std::string u_channel_input_file(argv[3]);
  std::string v_channel_input_file(argv[4]);
  std::string y_channel_input_file(argv[5]);

  std::string depth_map_output_file(argv[6]);
  std::string u_channel_output_file(argv[7]);
  std::string v_channel_output_file(argv[8]);
  std::string y_channel_output_file(argv[9]);

  std::vector<uint8_t> binary_mask_input;
  std::vector<uint8_t> depth_map_input;
  std::vector<uint8_t> uv_channels_input;
  std::vector<uint8_t> y_channel_input;
  auto input_read_success = read_inputs(
      binary_mask_input_file, depth_map_input_file, u_channel_input_file,
      v_channel_input_file, y_channel_input_file, binary_mask_input,
      depth_map_input, uv_channels_input, y_channel_input);
  if (!input_read_success)
    return 1;

  std::vector<uint8_t> depth_map_output(depth_map_input.size());
  std::vector<uint8_t> uv_channels_output(uv_channels_input.size());
  std::vector<uint8_t> y_channel_output(y_channel_input.size());

  iree_runtime_instance_options_t instance_options;
  iree_runtime_instance_options_initialize(&instance_options);
  iree_runtime_instance_options_use_all_available_drivers(&instance_options);
  iree_runtime_instance_t *instance = nullptr;
  IREE_CHECK_OK(iree_runtime_instance_create(
      &instance_options, iree_allocator_system(), &instance));

  iree_hal_device_t *device = nullptr;
  IREE_CHECK_OK(iree_runtime_instance_try_create_default_device(
      instance, iree_make_cstring_view("local-sync"), &device));

  iree_runtime_session_options_t session_options;
  iree_runtime_session_options_initialize(&session_options);
  iree_runtime_session_t *session = nullptr;
  IREE_CHECK_OK(iree_runtime_session_create_with_device(
      instance, &session_options, device,
      iree_runtime_instance_host_allocator(instance), &session));
  iree_hal_device_release(device);

  const struct iree_file_toc_t *module_file = bokeh_sample_builder_create();

  IREE_CHECK_OK(iree_runtime_session_append_bytecode_module_from_memory(
      session, iree_make_const_byte_span(module_file->data, module_file->size),
      iree_allocator_null()));

  iree_runtime_call_t call;
  IREE_CHECK_OK(iree_runtime_call_initialize_by_name(
      session, iree_make_cstring_view("module.bokeh_pipeline"), &call));

  iree_hal_buffer_params_t input_buffer_params;
  input_buffer_params.usage = IREE_HAL_BUFFER_USAGE_DEFAULT;
  input_buffer_params.type = IREE_HAL_MEMORY_TYPE_DEVICE_LOCAL;

  iree_hal_dim_t input_shapes[4][1] = {{depth_map_input.size()},
                                       {binary_mask_input.size()},
                                       {uv_channels_input.size()},
                                       {y_channel_input.size()}};
  uint8_t *input_ptrs[] = {depth_map_input.data(), binary_mask_input.data(),
                           uv_channels_input.data(), y_channel_input.data()};

  for (uint32_t i = 0; i < 4; i++) {
    iree_hal_buffer_view_t *input_buffer_view;
    IREE_CHECK_OK(iree_hal_buffer_view_allocate_buffer_copy(
        device, iree_hal_device_allocator(device),
        IREE_ARRAYSIZE(input_shapes[i]), input_shapes[i],
        IREE_HAL_ELEMENT_TYPE_UINT_8, IREE_HAL_ENCODING_TYPE_DENSE_ROW_MAJOR,
        input_buffer_params,
        iree_make_const_byte_span(input_ptrs[i], input_shapes[i][0]),
        &input_buffer_view));

    IREE_CHECK_OK(iree_runtime_call_inputs_push_back_buffer_view(
        &call, input_buffer_view));
    iree_hal_buffer_view_release(input_buffer_view);
  }

  IREE_CHECK_OK(iree_runtime_call_invoke(&call, /*flags=*/0))

  uint8_t *output_ptrs[] = {depth_map_output.data(), uv_channels_output.data(),
                            y_channel_output.data()};
  size_t output_sizes[] = {depth_map_output.size(), uv_channels_output.size(),
                           y_channel_output.size()};

  for (uint32_t i = 0; i < 3; i++) {
    iree_hal_buffer_view_t *ret_buffer_view = nullptr;
    IREE_CHECK_OK(iree_runtime_call_outputs_pop_front_buffer_view(
        &call, &ret_buffer_view));

    IREE_CHECK_OK(iree_hal_device_transfer_d2h(
        iree_runtime_session_device(session),
        iree_hal_buffer_view_buffer(ret_buffer_view), 0, output_ptrs[i],
        output_sizes[i], IREE_HAL_TRANSFER_BUFFER_FLAG_DEFAULT,
        iree_infinite_timeout()));
  }
  auto write_success =
      write_outputs(depth_map_output_file, u_channel_output_file,
                    v_channel_output_file, y_channel_output_file,
                    depth_map_output, uv_channels_output, y_channel_output);
  if (!write_success)
    return 1;
  return 0;
}