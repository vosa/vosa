/* -*- c++ -*-
 *
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_DIALECT_VOSA_UTILS_CONVERSION_H
#define VOSA_DIALECT_VOSA_UTILS_CONVERSION_H

#include "mlir/Dialect/Arith/IR/Arith.h"
#include "mlir/Dialect/Linalg/IR/Linalg.h"
#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/Transforms/DialectConversion.h"

namespace mlir {
namespace vosa {

inline Type convertIntegerTy(IntegerType intTy) {
  // convert to signless integer
  return IntegerType::get(intTy.getContext(), intTy.getIntOrFloatBitWidth());
}

inline Type convertShapedTy(ShapedType shapedTy) {
  if (auto intTy = shapedTy.getElementType().dyn_cast<IntegerType>())
    return shapedTy.clone(convertIntegerTy(intTy));
  return shapedTy;
}

/// Convert VOSA tensors using explicitly signed/unsigned integers to signless
/// tensors
class VosaTypeConverter : public TypeConverter {
public:
  VosaTypeConverter() {
    addConversion([](Type type) { return type; });
    addConversion(convertIntegerTy);
    addConversion(convertShapedTy);

    auto addUnrealizedCast = [](OpBuilder &builder, Type type,
                                ValueRange inputs, Location loc) {
      auto cast = builder.create<UnrealizedConversionCastOp>(loc, type, inputs);
      return std::optional<Value>(cast.getResult(0));
    };
    addArgumentMaterialization(addUnrealizedCast);
    addSourceMaterialization(addUnrealizedCast);
    addTargetMaterialization(addUnrealizedCast);
  }
};

} // namespace vosa
} // namespace mlir

#endif // VOSA_DIALECT_VOSA_UTILS_CONVERSION_H
