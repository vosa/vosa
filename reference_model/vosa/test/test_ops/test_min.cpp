/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/min.h"
#include "test_bench.h"
#include "vosa/vosa.h"

using namespace std;
using namespace vosa;

template <typename T>
static EigenImage<T> test_min(EigenImage<T> &input_1, EigenImage<T> &input_2) {
  auto Op = Min<T>();

  return Op
      .eval({make_shared<ArrayBase>(input_1), make_shared<ArrayBase>(input_2)})
      ->template get_image<T>();
}

TEST(test_min, test_min) {
  EigenImage<uint32_t> input_0(1, 3, 3);
  EigenImage<uint32_t> input_1(1, 3, 3);
  EigenImage<uint32_t> input_2(1, 3, 3);

  input_0.setValues({{{1, 2, 0}, {3, 5, 3}, {7, 8, 9}}});
  input_1.setValues({{{0, 7, 2}, {4, 3, 6}, {4, 5, 4}}});
  input_2.setValues({{{1, 10, 15}, {2, 1, 0}, {3, 7, 9}}});

  EigenImage<uint32_t> expected_0_1(1, 3, 3);
  EigenImage<uint32_t> expected_0_2(1, 3, 3);
  EigenImage<uint32_t> expected_1_2(1, 3, 3);
  EigenImage<uint32_t> expected_1_2_3(1, 3, 3);
  expected_0_1.setValues({{{0, 2, 0}, {3, 3, 3}, {4, 5, 4}}});
  expected_0_2.setValues({{{1, 2, 0}, {2, 1, 0}, {3, 7, 9}}});
  expected_1_2.setValues({{{0, 7, 2}, {2, 1, 0}, {3, 5, 4}}});
  expected_1_2_3.setValues({{{0, 2, 0}, {2, 1, 0}, {3, 5, 4}}});

  auto actual_0_1 = test_min(input_0, input_1);
  bool result = test::TestBench<uint32_t, int32_t>::compareEigenImages(
      actual_0_1, expected_0_1);
  EXPECT_TRUE(result);

  result = test::TestBench<uint32_t, int32_t>::compareEigenImages(
      test_min(input_2, input_1), expected_1_2);
  EXPECT_TRUE(result);

  result = test::TestBench<uint32_t, int32_t>::compareEigenImages(
      test_min(input_0, input_2), expected_0_2);
  EXPECT_TRUE(result);

  result = test::TestBench<uint32_t, int32_t>::compareEigenImages(
      test_min(input_0, input_0), input_0);
  EXPECT_TRUE(result);

  result = test::TestBench<uint32_t, int32_t>::compareEigenImages(
      test_min(input_2, actual_0_1), expected_1_2_3);
  EXPECT_TRUE(result);
}
