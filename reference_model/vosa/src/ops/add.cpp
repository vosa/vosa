/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "add.h"
#include <iostream>

std::shared_ptr<vosa::Op> vosa::AddBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_add = vosa_operator_union.AsADD();
  assert(serialized_add);
  auto mode = serialized_add->mode;
  switch (mode) {
  case vosa_serialization::ADD_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::Add<uint8_t, uint8_t>>();
  }
  case vosa_serialization::ADD_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::Add<uint16_t, uint16_t>>();
  }
  case vosa_serialization::ADD_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::Add<uint32_t, uint32_t>>();
  }
  case vosa_serialization::ADD_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::Add<int8_t, int8_t>>();
  }
  case vosa_serialization::ADD_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::Add<int16_t, int16_t>>();
  }
  case vosa_serialization::ADD_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::Add<int32_t, int32_t>>();
  }
  case vosa_serialization::ADD_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::Add<float, float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename inputT, typename outputT>
vosa::EigenImage<outputT> vosa::Add<inputT, outputT>::runme(
    const vosa::EigenImage<inputT> &input_1,
    const vosa::EigenImage<inputT> &input_2) const {
  EigenImage<outputT> input_1_as_output = input_1.template cast<outputT>();
  EigenImage<outputT> input_2_as_output = input_2.template cast<outputT>();

  const uint32_t height = input_1_as_output.dimension(0);
  const uint32_t width = input_1_as_output.dimension(1);
  const uint32_t channels = input_1_as_output.dimension(2);

  assert(height == input_2_as_output.dimension(0));
  assert(width == input_2_as_output.dimension(1));
  assert(channels == input_2_as_output.dimension(2));

  EigenImage<outputT> output(height, width, channels);

  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      for (uint32_t c = 0; c < channels; c++)
        output(y, x, c) =
            input_1_as_output(y, x, c) + input_2_as_output(y, x, c);

  return output;
}

template <typename inputT, typename outputT>
vosa::ArrayBase::DType vosa::Add<inputT, outputT>::output_dtype() const {
  return output_dtype_helper<outputT>();
}

template <typename inputT, typename outputT>
std::shared_ptr<vosa::ArrayBase> vosa::Add<inputT, outputT>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<inputT> &input_1 = inputs[0]->template get_image<inputT>();
  const EigenImage<inputT> &input_2 = inputs[1]->template get_image<inputT>();
  auto typed_output = this->runme(input_1, input_2);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::ADD_MODE
vosa::Add<uint8_t, uint8_t>::get_serialization_mode() const {
  return vosa_serialization::ADD_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::ADD_MODE
vosa::Add<uint16_t, uint16_t>::get_serialization_mode() const {
  return vosa_serialization::ADD_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::ADD_MODE
vosa::Add<uint32_t, uint32_t>::get_serialization_mode() const {
  return vosa_serialization::ADD_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::ADD_MODE
vosa::Add<int8_t, int8_t>::get_serialization_mode() const {
  return vosa_serialization::ADD_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::ADD_MODE
vosa::Add<int16_t, int16_t>::get_serialization_mode() const {
  return vosa_serialization::ADD_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::ADD_MODE
vosa::Add<int32_t, int32_t>::get_serialization_mode() const {
  return vosa_serialization::ADD_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::ADD_MODE
vosa::Add<float, float>::get_serialization_mode() const {
  return vosa_serialization::ADD_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename inputT, typename outputT>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Add<inputT, outputT>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  vosa_serialization::ADDT add;
  add.mode = get_serialization_mode();

  auto add_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  add_operator->op.Set(add);
  add_operator->name = op_name;
  add_operator->inputs = input_names;
  return add_operator;
}

template class vosa::Add<uint8_t, uint8_t>;

template class vosa::Add<uint16_t, uint16_t>;

template class vosa::Add<uint32_t, uint32_t>;

template class vosa::Add<int8_t, int8_t>;

template class vosa::Add<int16_t, int16_t>;

template class vosa::Add<int32_t, int32_t>;

template class vosa::Add<float, float>;