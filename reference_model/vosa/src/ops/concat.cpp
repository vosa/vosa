/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "concat.h"

std::shared_ptr<vosa::Op> vosa::ConcatBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_concat = vosa_operator_union.AsCONCAT();
  assert(serialized_concat);
  auto mode = serialized_concat->mode;
  switch (mode) {
  case vosa_serialization::CONCAT_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::Concat<uint8_t>>();
  }
  case vosa_serialization::CONCAT_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::Concat<uint16_t>>();
  }
  case vosa_serialization::CONCAT_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::Concat<uint32_t>>();
  }
  case vosa_serialization::CONCAT_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::Concat<int8_t>>();
  }
  case vosa_serialization::CONCAT_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::Concat<int16_t>>();
  }
  case vosa_serialization::CONCAT_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::Concat<int32_t>>();
  }
  case vosa_serialization::CONCAT_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::Concat<float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::EigenImage<T>
vosa::Concat<T>::runme(const std::vector<EigenImage<T>> &images) const {

  uint32_t input_channels = 0;

  uint32_t input_height = images[0].dimension(0);
  uint32_t input_width = images[0].dimension(1);
  for (const auto &image_pointer : images) {
    input_channels += image_pointer.dimension(2);
    assert(input_height == image_pointer.dimension(0));
    assert(input_width == image_pointer.dimension(1));
  }

  vosa::EigenImage<T> output{input_height, input_width, input_channels};

  for (uint32_t height_index = 0; height_index < input_height; height_index++) {
    for (uint32_t width_index = 0; width_index < input_width; width_index++) {

      uint32_t output_channel_index = 0;
      for (const auto &image_pointer : images) {
        uint32_t this_input_channels = image_pointer.dimension(2);
        for (uint32_t this_input_channels_index = 0;
             this_input_channels_index < this_input_channels;
             this_input_channels_index++, output_channel_index++) {

          assert(output_channel_index < input_channels);
          output(height_index, width_index, output_channel_index) =
              image_pointer(height_index, width_index,
                            this_input_channels_index);
        }
      }
    }
  }

  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::Concat<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::Concat<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  std::vector<vosa::EigenImage<T>> eigen_inputs;
  for (const auto &input : inputs) {
    const EigenImage<T> &eigen_input = input->template get_image<T>();
    eigen_inputs.push_back(eigen_input);
  }
  auto typed_output = this->runme(eigen_inputs);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::CONCAT_MODE
vosa::Concat<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::CONCAT_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::CONCAT_MODE
vosa::Concat<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::CONCAT_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::CONCAT_MODE
vosa::Concat<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::CONCAT_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::CONCAT_MODE
vosa::Concat<int8_t>::get_serialization_mode() const {
  return vosa_serialization::CONCAT_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::CONCAT_MODE
vosa::Concat<int16_t>::get_serialization_mode() const {
  return vosa_serialization::CONCAT_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::CONCAT_MODE
vosa::Concat<int32_t>::get_serialization_mode() const {
  return vosa_serialization::CONCAT_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::CONCAT_MODE
vosa::Concat<float>::get_serialization_mode() const {
  return vosa_serialization::CONCAT_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Concat<T>::serialize(const std::string &op_name,
                           const std::vector<std::string> &input_names) const {
  vosa_serialization::CONCATT concat;
  concat.mode = get_serialization_mode();

  auto concat_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  concat_operator->op.Set(concat);
  concat_operator->name = op_name;
  concat_operator->inputs = input_names;
  return concat_operator;
}

template class vosa::Concat<uint8_t>;

template class vosa::Concat<uint16_t>;

template class vosa::Concat<uint32_t>;

template class vosa::Concat<int8_t>;

template class vosa::Concat<int16_t>;

template class vosa::Concat<int32_t>;

template class vosa::Concat<float>;