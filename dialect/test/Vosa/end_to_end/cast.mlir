// run command without FileCheck to see output (or add "--dump-input always" FileCheck option)
// RUN: vosa-opt -vosa-to-tosa %s | %test_src_root/e2e-runner | FileCheck %s

func.func @main() {
    func.call @si_to_si() : () -> ()
    func.call @si_to_ui() : () -> ()
    func.call @ui_to_si() : () -> ()
    func.call @ui_to_ui() : () -> ()

    return
}

func.func @si_to_si() {
    %input_si16 = vosa.constant dense<[
       [[-1000], [-129], [-128], [-127], [-100], [  0 ], [100], [200], [254], [255], [256], [1000] ]
        ]> : tensor<1x12x1xsi16>

    // SI -> SI

    //  same
    // CHECK: sizes = [1, 12]
    // CHECK: [-1000, -129, -128, -127, -100, 0, 100, 200, 254, 255, 256, 1000]
    %res_si16_si16 = vosa.cast %input_si16 saturate : tensor<1x12x1xsi16> -> tensor<1x12x1xsi16>
    %i_res_si16_si16 = builtin.unrealized_conversion_cast %res_si16_si16 : tensor<1x12x1xsi16> to tensor<1x12x1xi16>
    %dyn_res_si16_si16 = tensor.cast %i_res_si16_si16 : tensor<1x12x1xi16> to tensor<?x?x?xi16>
    func.call @print_img_si16(%dyn_res_si16_si16) : (tensor<?x?x?xi16>) -> ()

    //  widening
    // CHECK: sizes = [1, 12]
    // CHECK: [-1000, -129, -128, -127, -100, 0, 100, 200, 254, 255, 256, 1000]
    %res_si16_si32 = vosa.cast %input_si16 saturate : tensor<1x12x1xsi16> -> tensor<1x12x1xsi32>
    %i_res_si16_si32 = builtin.unrealized_conversion_cast %res_si16_si32 : tensor<1x12x1xsi32> to tensor<1x12x1xi32>
    %dyn_res_si16_si32 = tensor.cast %i_res_si16_si32 : tensor<1x12x1xi32> to tensor<?x?x?xi32>
    func.call @print_img_i32(%dyn_res_si16_si32) : (tensor<?x?x?xi32>) -> ()

    //  narrowing
    // CHECK: sizes = [1, 12]
    // CHECK: [-128, -128, -128, -127, -100, 0, 100, 127, 127, 127, 127, 127]
    %res_si16_si8 = vosa.cast %input_si16 saturate : tensor<1x12x1xsi16> -> tensor<1x12x1xsi8>
    %i_res_si16_si8 = builtin.unrealized_conversion_cast %res_si16_si8 : tensor<1x12x1xsi8> to tensor<1x12x1xi8>
    %dyn_res_si16_si8 = tensor.cast %i_res_si16_si8 : tensor<1x12x1xi8> to tensor<?x?x?xi8>
    func.call @print_img_si8(%dyn_res_si16_si8) : (tensor<?x?x?xi8>) -> ()

    return
}

func.func @si_to_ui() {
    %input_si16 = vosa.constant dense<[
       [[-1000], [-129], [-128], [-127], [-100], [  0 ], [100], [200], [254], [255], [256], [1000] ]
        ]> : tensor<1x12x1xsi16>

    // SI -> UI

    //  same
    // CHECK: sizes = [1, 12]
    // CHECK: [0, 0, 0, 0, 0, 0, 100, 200, 254, 255, 256, 1000]
    %res_si16_ui16 = vosa.cast %input_si16 saturate : tensor<1x12x1xsi16> -> tensor<1x12x1xui16>
    %i_res_si16_ui16 = builtin.unrealized_conversion_cast %res_si16_ui16 : tensor<1x12x1xui16> to tensor<1x12x1xi16>
    %dyn_res_si16_ui16 = tensor.cast %i_res_si16_ui16 : tensor<1x12x1xi16> to tensor<?x?x?xi16>
    func.call @print_img_ui16(%dyn_res_si16_ui16) : (tensor<?x?x?xi16>) -> ()

    //  widening
    // CHECK: sizes = [1, 12]
    // CHECK: [0, 0, 0, 0, 0, 0, 100, 200, 254, 255, 256, 1000]
    %res_si16_ui32 = vosa.cast %input_si16 saturate : tensor<1x12x1xsi16> -> tensor<1x12x1xui32>
    %i_res_si16_ui32 = builtin.unrealized_conversion_cast %res_si16_ui32 : tensor<1x12x1xui32> to tensor<1x12x1xi32>
    %dyn_res_si16_ui32 = tensor.cast %i_res_si16_ui32 : tensor<1x12x1xi32> to tensor<?x?x?xi32>
    func.call @print_img_i32(%dyn_res_si16_ui32) : (tensor<?x?x?xi32>) -> ()

    //  narrowing
    // CHECK: sizes = [1, 12]
    // CHECK: [0, 0, 0, 0, 0, 0, 100, 200, 254, 255, 255, 255]
    %res_si16_ui8 = vosa.cast %input_si16 saturate : tensor<1x12x1xsi16> -> tensor<1x12x1xui8>
    %i_res_si16_ui8 = builtin.unrealized_conversion_cast %res_si16_ui8 : tensor<1x12x1xui8> to tensor<1x12x1xi8>
    %dyn_res_si16_ui8 = tensor.cast %i_res_si16_ui8 : tensor<1x12x1xi8> to tensor<?x?x?xi8>
    func.call @print_img_ui8(%dyn_res_si16_ui8) : (tensor<?x?x?xi8>) -> ()

    return
}

func.func @ui_to_si() {

    %input_ui16 = vosa.constant dense<[
       [[  0 ], [100], [200], [254], [255], [256], [1000], [32767], [32768], [65535] ]
        ]> : tensor<1x10x1xui16>

    // UI -> SI

    //  same
    // CHECK: sizes = [1, 10]
    // CHECK: [0, 100, 200, 254, 255, 256, 1000, 32767, 32767, 32767]
    %res_ui16_si16 = vosa.cast %input_ui16 saturate : tensor<1x10x1xui16> -> tensor<1x10x1xsi16>
    %i_res_ui16_si16 = builtin.unrealized_conversion_cast %res_ui16_si16 : tensor<1x10x1xsi16> to tensor<1x10x1xi16>
    %dyn_res_ui16_si16 = tensor.cast %i_res_ui16_si16 : tensor<1x10x1xi16> to tensor<?x?x?xi16>
    func.call @print_img_si16(%dyn_res_ui16_si16) : (tensor<?x?x?xi16>) -> ()

    //  widening
    // CHECK: sizes = [1, 10]
    // CHECK: [0, 100, 200, 254, 255, 256, 1000, 32767, 32768, 65535]
    %res_ui16_si32 = vosa.cast %input_ui16 saturate : tensor<1x10x1xui16> -> tensor<1x10x1xsi32>
    %i_res_ui16_si32 = builtin.unrealized_conversion_cast %res_ui16_si32 : tensor<1x10x1xsi32> to tensor<1x10x1xi32>
    %dyn_res_ui16_si32 = tensor.cast %i_res_ui16_si32 : tensor<1x10x1xi32> to tensor<?x?x?xi32>
    func.call @print_img_i32(%dyn_res_ui16_si32) : (tensor<?x?x?xi32>) -> ()

    //  narrowing
    // CHECK: sizes = [1, 10]
    // CHECK: [0, 100, 127, 127, 127, 127, 127, 127, 127, 127]
    %res_ui16_si8 = vosa.cast %input_ui16 saturate : tensor<1x10x1xui16> -> tensor<1x10x1xsi8>
    %i_res_ui16_si8 = builtin.unrealized_conversion_cast %res_ui16_si8 : tensor<1x10x1xsi8> to tensor<1x10x1xi8>
    %dyn_res_ui16_si8 = tensor.cast %i_res_ui16_si8 : tensor<1x10x1xi8> to tensor<?x?x?xi8>
    func.call @print_img_si8(%dyn_res_ui16_si8) : (tensor<?x?x?xi8>) -> ()

    return
}


// UI -> UI
func.func @ui_to_ui() {

    %input_ui16 = vosa.constant dense<[
       [[  0 ], [100], [200], [254], [255], [256], [1000], [32767], [32768], [65535] ]
        ]> : tensor<1x10x1xui16>

    //  same
    // CHECK: sizes = [1, 10]
    // CHECK: [0, 100, 200, 254, 255, 256, 1000, 32767, 32768, 65535]
    %res_ui16_ui16 = vosa.cast %input_ui16 saturate : tensor<1x10x1xui16> -> tensor<1x10x1xui16>
    %i_res_ui16_ui16 = builtin.unrealized_conversion_cast %res_ui16_ui16 : tensor<1x10x1xui16> to tensor<1x10x1xi16>
    %dyn_res_ui16_ui16 = tensor.cast %i_res_ui16_ui16 : tensor<1x10x1xi16> to tensor<?x?x?xi16>
    func.call @print_img_ui16(%dyn_res_ui16_ui16) : (tensor<?x?x?xi16>) -> ()

    //  widening
    // CHECK: sizes = [1, 10]
    // CHECK: [0, 100, 200, 254, 255, 256, 1000, 32767, 32768, 65535]
    %res_ui16_ui32 = vosa.cast %input_ui16 saturate : tensor<1x10x1xui16> -> tensor<1x10x1xui32>
    %i_res_ui16_ui32 = builtin.unrealized_conversion_cast %res_ui16_ui32 : tensor<1x10x1xui32> to tensor<1x10x1xi32>
    %dyn_res_ui16_ui32 = tensor.cast %i_res_ui16_ui32 : tensor<1x10x1xi32> to tensor<?x?x?xi32>
    func.call @print_img_i32(%dyn_res_ui16_ui32) : (tensor<?x?x?xi32>) -> ()

    //  narrowing
    // CHECK: sizes = [1, 10]
    // CHECK: [0, 100, 200, 254, 255, 255, 255, 255, 255, 255]
    %res_ui16_ui8 = vosa.cast %input_ui16 saturate : tensor<1x10x1xui16> -> tensor<1x10x1xui8>
    %i_res_ui16_ui8 = builtin.unrealized_conversion_cast %res_ui16_ui8 : tensor<1x10x1xui8> to tensor<1x10x1xi8>
    %dyn_res_ui16_ui8 = tensor.cast %i_res_ui16_ui8 : tensor<1x10x1xi8> to tensor<?x?x?xi8>
    func.call @print_img_ui8(%dyn_res_ui16_ui8) : (tensor<?x?x?xi8>) -> ()

    return
}

func.func @print_img_si8(%a : tensor<?x?x?xi8>) {
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c0 : tensor<?x?x?xi8>
  %w = tensor.dim %a, %c1 : tensor<?x?x?xi8>
  %nch = tensor.dim %a, %c2 : tensor<?x?x?xi8>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[0, 0, %c][%h, %w, 1][1, 1, 1]  : tensor<?x?x?xi8> to tensor<?x?x1xi8>
     %chdata_c = tensor.collapse_shape %chdata [[0], [1, 2]] : tensor<?x?x1xi8> into tensor<?x?xi8>
     %i32_data_init = tensor.empty(%h, %w) : tensor<?x?xi32>
     %i32_data = linalg.map
      ins(%chdata_c:tensor<?x?xi8>)
      outs(%i32_data_init:tensor<?x?xi32>)
      (%input_elem: i8) {
        %v = arith.extsi %input_elem: i8 to i32
        linalg.yield %v: i32
      }
     %unranked_chdata = tensor.cast %i32_data : tensor<?x?xi32> to tensor<*xi32>
     func.call @printMemrefI32(%unranked_chdata) : (tensor<*xi32>) -> ()
  }
  return
}

func.func @print_img_si16(%a : tensor<?x?x?xi16>) {
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c0 : tensor<?x?x?xi16>
  %w = tensor.dim %a, %c1 : tensor<?x?x?xi16>
  %nch = tensor.dim %a, %c2 : tensor<?x?x?xi16>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[0, 0, %c][%h, %w, 1][1, 1, 1]  : tensor<?x?x?xi16> to tensor<?x?x1xi16>
     %chdata_c = tensor.collapse_shape %chdata [[0], [1, 2]] : tensor<?x?x1xi16> into tensor<?x?xi16>
     %i32_data_init = tensor.empty(%h, %w) : tensor<?x?xi32>
     %i32_data = linalg.map
      ins(%chdata_c:tensor<?x?xi16>)
      outs(%i32_data_init:tensor<?x?xi32>)
      (%input_elem: i16) {
        %v = arith.extsi %input_elem: i16 to i32
        linalg.yield %v: i32
      }
     %unranked_chdata = tensor.cast %i32_data : tensor<?x?xi32> to tensor<*xi32>
     func.call @printMemrefI32(%unranked_chdata) : (tensor<*xi32>) -> ()
  }
  return
}

func.func @print_img_ui8(%a : tensor<?x?x?xi8>) {
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c0 : tensor<?x?x?xi8>
  %w = tensor.dim %a, %c1 : tensor<?x?x?xi8>
  %nch = tensor.dim %a, %c2 : tensor<?x?x?xi8>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[0, 0, %c][%h, %w, 1][1, 1, 1]  : tensor<?x?x?xi8> to tensor<?x?x1xi8>
     %chdata_c = tensor.collapse_shape %chdata [[0], [1, 2]] : tensor<?x?x1xi8> into tensor<?x?xi8>
     %i32_data_init = tensor.empty(%h, %w) : tensor<?x?xi32>
     %i32_data = linalg.map
      ins(%chdata_c:tensor<?x?xi8>)
      outs(%i32_data_init:tensor<?x?xi32>)
      (%input_elem: i8) {
        %v = arith.extui %input_elem: i8 to i32
        linalg.yield %v: i32
      }
     %unranked_chdata = tensor.cast %i32_data : tensor<?x?xi32> to tensor<*xi32>
     func.call @printMemrefI32(%unranked_chdata) : (tensor<*xi32>) -> ()
  }
  return
}

func.func @print_img_ui16(%a : tensor<?x?x?xi16>) {
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c0 : tensor<?x?x?xi16>
  %w = tensor.dim %a, %c1 : tensor<?x?x?xi16>
  %nch = tensor.dim %a, %c2 : tensor<?x?x?xi16>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[0, 0, %c][%h, %w, 1][1, 1, 1]  : tensor<?x?x?xi16> to tensor<?x?x1xi16>
     %chdata_c = tensor.collapse_shape %chdata [[0], [1, 2]] : tensor<?x?x1xi16> into tensor<?x?xi16>
     %i32_data_init = tensor.empty(%h, %w) : tensor<?x?xi32>
     %i32_data = linalg.map
      ins(%chdata_c:tensor<?x?xi16>)
      outs(%i32_data_init:tensor<?x?xi32>)
      (%input_elem: i16) {
        %v = arith.extui %input_elem: i16 to i32
        linalg.yield %v: i32
      }
     %unranked_chdata = tensor.cast %i32_data : tensor<?x?xi32> to tensor<*xi32>
     func.call @printMemrefI32(%unranked_chdata) : (tensor<*xi32>) -> ()
  }
  return
}

func.func @print_img_i32(%a : tensor<?x?x?xi32>) {
  %c0 = arith.constant 0 : index
  %c1 = arith.constant 1 : index
  %c2 = arith.constant 2 : index
  %h = tensor.dim %a, %c0 : tensor<?x?x?xi32>
  %w = tensor.dim %a, %c1 : tensor<?x?x?xi32>
  %nch = tensor.dim %a, %c2 : tensor<?x?x?xi32>
  scf.for %c = %c0 to %nch step %c1 {
     %chdata = tensor.extract_slice %a[0, 0, %c][%h, %w, 1][1, 1, 1]  : tensor<?x?x?xi32> to tensor<?x?x1xi32>
     %chdata_c = tensor.collapse_shape %chdata [[0], [1, 2]] : tensor<?x?x1xi32> into tensor<?x?xi32>
     %unranked_chdata = tensor.cast %chdata_c : tensor<?x?xi32> to tensor<*xi32>
     func.call @printMemrefI32(%unranked_chdata) : (tensor<*xi32>) -> ()
  }
  return
}

func.func private @printMemrefI32(%ptr : tensor<*xi32> {bufferization.access = "read"})
