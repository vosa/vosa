"""
License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import os
import typing
import vosa.types as types


overview_literal = """
Vision Operator Set Architecture (VOSA) is a distilled collection of low-level image/sensor array operators. It is intended to be complementary to Tensor Operator Set Architecture (TOSA) and therefore specifies a collection of classic (non-ML) vision and imaging operators.
VOSA is intended to be a unified intermediate representation for classical vision and imaging compute. In that role, targetting VOSA should lead to deploying computer vision and imaging pipelines on a wide range of hardware targets.
As a complement to TOSA, VOSA aims to support complete computer vision and imaging pipelines, including pre, intermediate and post processing operations commonly encountered adjacent and interleaved with ML processing.
The VOSA specification should be sufficiently precise to enable implementations of integer components to be bit exact with each other, enabling consistency across hardware platforms.
VOSA deliberately minimizes any mandating of implementation details of the specification, but where possible is written with an eye toward enabling efficient software and/or hardware implementations.
"""


def overview(adoc_path, intro_path):
    overview_file = "overview.adoc"
    overview_path = os.path.join(adoc_path, intro_path, overview_file)
    lines = ["=== Overview"]
    lines.append(overview_literal)
    with open(overview_path, "w") as f:
        for line in lines:
            f.write("%s\n" % line)
    return os.path.join(intro_path, overview_file)


principles_literal = """
[%autowidth.stretch]
|===
| ID | Principle

| P0 a|
VOSA aims to define most operators on an Image to Image basis, that is one or more Images as input and an Image output

However VOSA may also include (for example),

* Array to vector, vector to array operators,
* Array to scalar, scalar to array operators,
* ... ,

where the existence of such operators enables generalized decomposition of imaging and vision pipelines. 

| P1 | An operator should constitute a primitive operation, or a building block (node), when thinking about a graph-based realization of an imaging or vision algorithm.

| P2 | Except for explicitly defined, most primitive, elementwise MULT-ADD operators the VOSA set MUST NOT aim to get a vision operation/kernel to be lowered to a level of just multiply-add operations, as this prevents building more energy-efficient implementations of VOSA operator set. 

| P3 | An operator should be a usable, generic component, out of which more complex algorithm pipelines can be constructed.

| P4 | The VOSA specification provides exact functional/behavioral definition of an operator, taking into account bit-precision, input, output ranges for all operands. Hardware and/or software implementations of VOSA operators with integer data types shall be bit-exact.

| P5 | VOSA is designed and specified to enable energy-efficient accelerated computing for vision. While the VOSA operator definition must not conflate hardware implementation details, the VOSA abstraction layer is proposed having in mind different hardware backends.
|===

"""


def principles(adoc_path, intro_path):
    principles_file = "principles.adoc"
    principles_path = os.path.join(adoc_path, intro_path, principles_file)
    lines = ["=== Operator Selection Principles"]
    lines.append(principles_literal)
    with open(principles_path, "w") as f:
        for line in lines:
            f.write("%s\n" % line)
    return os.path.join(intro_path, principles_file)


array_types_literal = """
VOSA defines a number of data types, which are used to describe inputs, attributes and outputs to Operators.

The most fundamental type is Array. For many operations specific shapes of Array are required as any of the inputs, attributes or outputs.

The following table itemizes these specific shapes, these types are used in the operator specification to make said specification more casually readable and the intent more clear.

For the purposes of possible external interactions with these Array Types, or the implementation of custom operators, Array Types must implement the `[]` operator. 
This serves as both getter and setter for individual values in the Array, with number of arguments equal to the dimension of the array. 

VOSA does not mandate anything further about the implementation of these types, or, by extension, the implementation of the Operators which use them.
"""


def array_types() -> typing.List[str]:
    lines = ["==== Array Types"]
    lines.append(array_types_literal)
    lines.extend(types.aenum_to_table(types.AType))
    return lines


dtypes_literal = """
Necessarily associated with each Array based data type is a DType of the data contained within.

For each VOSA operator there exists one or more operating modes where every input, attribute and output has a precisely defined ArrayType and DType.

VOSA mandates that an implemntation of operators exists only for those precise combinations of types.
"""


def dtypes() -> typing.List[str]:
    lines = ["==== Data Types"]
    lines.append(dtypes_literal)
    lines.extend(types.aenum_to_table((types.DType)))
    return lines


image_types_literal = """
In some cases it is desirable for VOSA operators to interface directly with some common image formats defining specific bit packings.

In general VOSA mandates nothing about the implementation of storage for data objects, but in this specific case VOSA defines special DTypes to support usage of these bit packings.

Most VOSA operators do not directly support these image DTypes. However we note that it is possible for an implementation to indirectly support a wider range of operations on these formats, if desired, by detecting subgraphs which accept these ImageDTypes as inputs and creating fused implementations with following operators.
"""


def image_types() -> typing.List[str]:
    lines = []
    lines.append(image_types_literal)
    lines.extend(types.aenum_to_table(types.ImageDtype))
    return lines


special_types_literal = """
Most operators in VOSA operate on Array types of one form or another. 
In some cases, however, it is beneficial to augment an ArrayType with additional metadata. 
This is in order that all data which defines how to work with that object is encapsulated together rather than being communicated independently.
In other cases (principally interacting with external data) we may make use of a raw data type, which has no metadata, shape, or DType, consisting simply of a byte array.  
"""


def special_types() -> typing.List[str]:
    lines = ["==== Special Types"]
    lines.append(special_types_literal)
    lines.extend(types.aenum_to_table(types.SpecialType))
    return lines


modes_literal = """
VOSA Operators may be potentially applied to a wide variety of input and output Array and Data Types. In fact, VOSA mandates implementations of operators for only a subset of types, selected to support types commonly used in computer vision and imaging applications.

A combinations of input, attribute and output types which should be implemented for a given operator is termed an "Operator Mode". In this specification we adopt a naming convention for such modes of the form:

`{Input Array Type First Letter}_{Output Array Type First Letter}_{Input Data Type}_{Output Data Type}` 

Ergo an Operator Mode for an operator which receives as input an `IMAGE` of type `UINT8` and outputs a `SCALAR` of type `INT16` would be designated `I_S_UINT8_INT16`.

In cases where operators require multiple inputs, the designation is derived, by convention, from the first listed input only.
"""


def modes() -> typing.List[str]:
    lines = ["==== Operator Modes"]
    lines.append(modes_literal)
    return lines


def features(adoc_path, intro_path):
    features_file = "features.adoc"
    features_path = os.path.join(adoc_path, intro_path, features_file)
    lines = ["=== Supported Features"]
    lines.extend(array_types())
    lines.extend(dtypes())
    # lines.extend(image_types())
    lines.extend(special_types())
    lines.extend(modes())
    with open(features_path, "w") as f:
        for line in lines:
            f.write("%s\n" % line)
    return os.path.join(intro_path, features_file)


def adoc(adoc_path):
    introduction_path = "introduction"
    introduction_folder = os.path.join(adoc_path, introduction_path)
    os.makedirs(introduction_folder, exist_ok=True)
    lines = ["== Introduction"]

    lines.append("include::%s[]" % (overview(adoc_path, introduction_path)))
    lines.append("include::%s[]" % (principles(adoc_path, introduction_path)))
    lines.append("include::%s[]" % (features(adoc_path, introduction_path)))

    introduction_file = "introduction.adoc"
    adoc_introduction_path = os.path.join(adoc_path, introduction_file)
    with open(adoc_introduction_path, "w") as f:
        for line in lines:
            f.write("%s\n" % line)
    return introduction_file
