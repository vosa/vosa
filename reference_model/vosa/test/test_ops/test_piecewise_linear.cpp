/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/piecewise_linear.h"
#include "test_bench.h"
#include "vosa/vosa.h"

TEST(test_piecewise_linear, test_piecewise_linear) {

  vosa::Graph graph;

  auto image_array = vosa::EigenImage<float>(1, 1, 3);
  image_array.setValues({{{-1.0, 6.0, 17.0}}});

  auto nodes = vosa::EigenVector<float>(5);
  nodes.setValues({0.0, 2.0, 4.0, 8.0, 16.0});
  auto values = vosa::EigenVector<float>(5);
  values.setValues({1.0, 2.0, 3.0, 4.0, 5.0});

  auto piecewise_linear = vosa::PiecewiseLinear(nodes, values);

  auto input = std::make_shared<vosa::ArrayBase>(image_array);
  auto output = piecewise_linear.eval({input})->get_image<float>();

  auto expected_output = vosa::EigenImage<float>(1, 1, 3);
  expected_output.setValues({{{1.0, 3.5, 5.0}}});

  bool success = vosa::test::TestBench<float, int32_t>::compareEigenImages(
      output, expected_output);
  EXPECT_TRUE(success);
}