# VOSA

VOSA (Vision Operator Set Architecture)

## Introduction

The VOSA repository represents the ongoing output of a technology project around **Mixed CV/ML Codegen**.

Axiomatically we believe that today many computer vision applications (in particular in areas such as camera/media processing), that Machine Learning operations may be pre/post pended and/or interleaved with deterministic **Imaging and Computer Vision Operations**.

Today these pipelines may be implemented by mixtures of technologies, in particular perhaps quite different techologies for the **Computer Vision** and **Machine Learning** components.

The central goal of this repository is to explore utilizing existing **Machine Learning** compiler technology ([MLIR](https://mlir.llvm.org)/[IREE](https://github.com/openxla/iree)) to codegen complete **Mixed CV/ML Pipelines**. The essential idea is to have a complete, monolithic, unified MLIR representation of a complete pipeline which can be frictionlessly compiled to the pipeline developer's desired target. 

To that end we have developed **VOSA**. VOSA is an operator set which might complement [TOSA](https://www.mlplatform.org/tosa/) by providing operators which can support the representation of the **Computer Vision** components of camera/media processing (as opposed to the core ML components).

The VOSA operator set and its implementations remain, as a technology project, very much exploratory and a work in progress. The set and its constitutent operators are liable to change over time. Operators might be
* Added,
* Subtracted,
* Substantially modified

as we explore issues around
* Representations of pipelines,
* Performance,
* Interaction with other MLIR dialects and operator sets,
* Interactions with possible front ends.

Moreover implementations of all operators are not complete, operators are typically implemented as needed when generating sample pipelines.

That being said we hope that this work serves to stimulate activity and discussion around the code generation of complete **Mixed CV/ML Pipelines**.

## Repository Structure

This repository contains a number of components which together constitute the VOSA project.

Those familiar with TOSA will recognize that these are intended to align with some of the components of the TOSA project. However in the case of the VOSA project we contain all components within a single (this) repository.

We hope that this enables readers to easily grok the concept as a whole as well as enable a streamlined one shot build process, for those who wish to experiment with the code.

### Vosa Specification

The VOSA specification describes the current working state of the VOSA operator set. The set is intended to support a baseline of computer vision operators. It is a living document and is expected to evolve over time.

The VOSA specification is constructed by a simple python library where each VOSA operation is described by a python class.

The library directly creates an [asciidoctor](https://asciidoctor.org/) version of the specification which is then also thereafter converted to `.html` format.

The specification is built by executing the python executable [vosa_make_spec.py](specification/vosa_make_spec.py). However usually this does not need to be performed manually as the specification is built (and installed) by cmake. Following execution of cmake install the specification should be found at `${CMAKE_INSTALL_PREFIX}/doc/vosa.html`. See [Building](#building) for more guidance on building the repository.

### VOSA Dialect

The VOSA MLIR dialect and associated compiler passes are implemented in `dialect/`. 

The dialect represents the main implementation of the VOSA specification, intended to (with the use of a further MLIR backend such as IREE) enable the compilation of VOSA (Compute Vision) operations to targets as supported by the backend.

Tablegen files which define the VOSA dialect are available at [VosaDialect.td](dialect/include/vosa/Dialect/Vosa/IR/VosaDialect.td), [VosaOps.td](dialect/include/vosa/Dialect/Vosa/IR/VosaOps.td) and [VosaTypes.td](dialect/include/vosa/Dialect/Vosa/IR/VosaTypes.td).

In order to enable IREE to compile the VOSA dialect a lowering pass `VosaToTosa` is defined at [Passes.td](dialect/include/vosa/Conversion/Passes.td). This pass lowers VOSA IR to a mixture of dialects, but predominantly `TOSA` and `linalg`. The pass then realizes CV operations described in VOSA in a form which may be ingested by IREE for onward compilation (making use of the IREE TOSA import mechanism).

See [Dialect](dialect/docs/dialect.md) for more details on the implementation of the dialect, and how to work with it.

### Vosa Reference Model

The VOSA Reference Model consists of simple C++ implementations of VOSA operations (explicitly intended to be human readable) as well as a simple graph based engine to execute pipelines composed of VOSA operations.

The primary mechanism of interacting with the reference model is feeding the engine a [reference model flatbuffer](reference_model/serialization/schema/vosa.fbs). Which can be parsed and executed by the graph engine.

Creation of such flatbuffers can be performed by utilizing the [serialization pass](reference_model/serialization_pass).

### Tools

The tools directory contains libraries and tools to assemble a complete end to end compilation of VOSA pipelines utilizing the [Dialect](#vosa-dialect) for VOSA &rarr; TOSA/Linalg lowerings and [IREE](https://github.com/openxla/iree) for onward lowerings to the desired target.

The dialect lowerings are performed directly in MLIR, while IREE is invoked using it's c api.

The `Compiler` library directly enables the code generation of the [Samples](#samples).

### Samples

The samples directory contains a simple [API](samples/api) for creating VOSA graphs. This api is intended to enable the demonstration of the mixed Computer Vision/ML codegen concept and is not expected to find a use in production. However, it does enable us to instantiate VOSA functionality in order to perform onward codegen.

The build system for the samples consists of a two stage process where the sample API is first traced to generate MLIR, after which a compiler executable is constructed and executed to generate an IREE `.vmfb` file, with associated header. These outputs are then included/linked into the final executable. This is all managed by the `vosa_sample` cmake function.

Optionally (where supported) this compiler executable also generates a VOSA reference model flatbuffer which can be used to execute the pipeline through the reference model as opposed to the compiler stack.

The [pipelines](samples/pipelines) directory contains some examples of VOSA pipelines.

#### Sobel
A very simple pipeline which demonstrates Sobel filter like functionality (edge detection).

#### Bokeh
A pipeline which ingests
* A YUV format image
* A foreground/background segmented image (as created by an ML model)
* A depth map image (as created by either an ML model or a sensor)

And thereafter performs degrees of blurring of the background component of the image (degree based on depth) using separable blur convolution kernels.

#### Bluedog
A pipeline which ingets a bayer format image, debayers and prepares for neural network execution (Deeplabv3) and thereafter post process to return a substantially greyscale image where all pixels classified as a dog are shaded blue.


## Getting Started

### Building

This repository is structured without use of git submodules, rather using cmake FetchContent functionality to retrive dependencies such as [IREE](https://github.com/openxla/iree) and the [TOSA Reference Model](https://github.com/openxla/iree).

The ambition is that it is as simple as possible to clone this repository and build and execute the samples.

However this does mean that the initial execution of cmake **may take a little time** as necessary dependencies are pulled. Furthermore the initial build also takes a little time as LLVM etc. are built.

If either the cmake execution or the build itself fail, it is hoped that a meaningful error message is generated. It may be constructive to read the [Dockerfile](docker/Dockerfile) to see the hermetic creation of an environment where the project is known to build successfully. Reports of issues completing the build are welcome.

#### Python

Building the project depends on the availability of a python interpreter with some modules that can be installed via Python3 pip packages. These are used to
* Support the building of the specification
* Support the conversion of `.tflite` files to `tosa.mlir`. 

The list of required packages is available in the [requirements.txt](requirements.txt). Perhaps the simplest way to make these executables available is using a venv:
```
- python3 -m venv venv
- source venv/bin/activate
- python -m pip install --upgrade pip
- pip install -r requirements.txt
- export PATH_TO_PYTHON_EXECUTABLE=$PWD/venv/bin/python
```

#### CMake

The project is built using the standard cmake flow:
```
- mkdir -p build
- mkdir -p install
- cd build
- cmake
  -GNinja
  -DCMAKE_BUILD_TYPE=Release
  -DCMAKE_INSTALL_PREFIX=../install
  -DPython3_EXECUTABLE=$PATH_TO_PYTHON_EXECUTABLE
  ..
- cmake --build .
- cmake --install .
```

The default iree repository and tag to pull from is stored in [CMakeLists.txt](CMakeLists.txt) and will be updated periodically.

If one prefers to build against a **specific remote** version of IREE then provide cmake with additional arguments:

```
- mkdir -p build
- mkdir -p install
- cd build
- cmake
  -GNinja
  -DIREE_GIT_REPOSITORY=$IREE_GIT_REPOSITORY
  -DIREE_GIT_TAG=$IREE_GIT_TAG
  -DCMAKE_BUILD_TYPE=Release
  -DCMAKE_INSTALL_PREFIX=../install
  -DPython3_EXECUTABLE=$PATH_TO_PYTHON_EXECUTABLE
  ..
- cmake --build .
- cmake --install .
```

If one prefers to build against a **local copy** of iree (perhaps to work with downstream or in development versions) then provide cmake with the path to the local source tree, cmake will still locate the build tree within this project's build folder

```
- mkdir -p build
- mkdir -p install
- cd build
- cmake
  -GNinja
  -DIREE_LOCAL_COPY=$PATH_TO_LOCAL_IREE
  -DCMAKE_BUILD_TYPE=Release
  -DCMAKE_INSTALL_PREFIX=../install
  -DPython3_EXECUTABLE=$PATH_TO_PYTHON_EXECUTABLE
  ..
- cmake --build .
- cmake --install .
```

#### Dependencies

This project is built by default on `Ubuntu 20.04` using the `GNU 9.4.0` toolchain (the default on that operating system). A number of relatively straightforward `.deb` packages should be installed using the `apt` package manager (see the [Dockerfile](docker/Dockerfile)) for details. A newer version of `cmake` than is installed by default on that OS is required. Finally the `gem` package `asciidoctor` is needed to build the specification.

It is expected that due to the relatively narrow number of dependencies it should be possible to successfully build the project on a wider variety of OS/toolchain combinations but this has not been tested.


### Installation

The principle artifacts of the cmake installation are

* `doc/vosa.html` The html rendering of the specification
* Sample executables at e.g. `test/*_sample` which have been compiled using the prototype compiler stack. Please refer to the CI for any disambiguation about how to execute them.
* Various VOSA/IREE executables and libraries in `bin/` and `lib/`

### Experimentation

It is hoped that the samples provided give sufficient working examples to experiment with this compiler stack if desired. Due to the experimental nature of this project it is highly possible to execute code paths which are not robustly tested. Again reports of issues are welcome.

## Ongoing Plans And Activities

The intended focus of activity in this repository over the coming months are
* Explore performance of existing samples and where possible and appropriate make or suggest changes in this repository and/or IREE to improve performance of computer vision functionality.
* (Potentially) add further samples to widen the coverage of the compiler stack and performance improvements

## Feedback and Discussion

In general feedback and discussion will be greatly appreciated and well recieved. We anticipate two principle types
* Feedback on the concept of mixed CV/ML code generation
* Feedback on the detail of the direction of the implementation of above

Please don't be reticent to raise issues and share with your interested colleagues. 