/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/Dialect/Vosa/IR/VosaOps.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/OpImplementation.h"
#include "mlir/IR/TypeUtilities.h"
#include "vosa/Dialect/Vosa/IR/VosaDialect.h"
#include "vosa/Dialect/Vosa/Utils/DataFormat.h"

#include "vosa/Dialect/Vosa/IR/VosaOpsEnums.cpp.inc"

#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#define DEBUG_TYPE "vosa-ops"

using namespace mlir;
using namespace mlir::vosa;

static void getIntegerArrayAttr(const ArrayAttr &a, SmallVector<int64_t> &o) {
  for (Attribute val : a.getValue()) {
    o.push_back(val.cast<IntegerAttr>().getValue().getSExtValue());
  }
}

static bool isSameElementType(const ShapedType &a, const ShapedType &b) {
  return (a.getElementType() == b.getElementType());
}

static bool isSameShape(const ShapedType &a, const ShapedType &b) {
  if (!a.hasStaticShape() || !b.hasStaticShape())
    return true;
  if (a.getShape() != b.getShape())
    return false;
  return true;
}

static bool isSameSizeAndType(const ShapedType &a, const ShapedType &b) {
  return isSameElementType(a, b) && isSameShape(a, b);
}

template <typename O>
static LogicalResult checkImageOutputShape(O &op, const ShapedType &outputType,
                                           int64_t h, int64_t w, int64_t c) {
  if (outputType.getShape().size() != 3) {
    return op.emitOpError("invalid output shape");
  }

  SmallVector<int64_t> outputShape;
  outputShape.push_back(
      outputType.isDynamicDim(getHeightDimension()) ? ShapedType::kDynamic : h);
  outputShape.push_back(
      outputType.isDynamicDim(getWidthDimension()) ? ShapedType::kDynamic : w);
  outputShape.push_back(outputType.isDynamicDim(getChannelDimension())
                            ? ShapedType::kDynamic
                            : c);
  outputShape = hwcToDataFormatShape(outputShape);
  if (outputShape != outputType.getShape()) {
    return op.emitOpError("invalid output shape [")
           << outputType.getShape() << "], expected [" << outputShape << "]";
  }

  return success();
}

// match height and width, ignoring dynamic dimensions
static bool checkHxW(int64_t h1, int64_t w1, int64_t h2, int64_t w2) {
  return (h1 == ShapedType::kDynamic || h2 == ShapedType::kDynamic ||
          h1 == h2) &&
         (w1 == ShapedType::kDynamic || w2 == ShapedType::kDynamic || w1 == w2);
}

static LogicalResult SameShapeReturnType(
    const ValueShapeRange &operands,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {
  ShapeAdaptor inputShape = operands.getShape(0);
  llvm::SmallVector<int64_t> outShape;
  inputShape.getDims(outShape);
  inferredReturnShapes.push_back(ShapedTypeComponents(outShape));
  return success();
}

#define SAME_SHAPE_INFER(OP)                                                   \
  LogicalResult OP::inferReturnTypeComponents(                                 \
      MLIRContext *context, std::optional<Location> location,                  \
      ValueShapeRange operands, DictionaryAttr attributes,                     \
      OpaqueProperties properties, RegionRange regions,                        \
      SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {           \
    return SameShapeReturnType(operands, inferredReturnShapes);                \
  }

OpFoldResult vosa::ConstantOp::fold(FoldAdaptor adaptor) { return getValue(); }

//===----------------------------------------------------------------------===//
// Operations defined in the VOSA specification
//
// NOTE: Keep these in the same order as the specification
//===----------------------------------------------------------------------===//

//===----------------------------------------------------------------------===//
// Comparison operations.
//===----------------------------------------------------------------------===//

template <typename O>
static LogicalResult verifyComparisonOp(O op) {
  // each input and result needs to be same sized/element type image
  auto in0Type = op.getInput0().getType().template cast<ShapedType>();
  auto in1Type = op.getInput1().getType().template cast<ShapedType>();
  auto outType = op.getOutput().getType().template cast<ShapedType>();

  if (!isSameSizeAndType(in0Type, in1Type)) {
    return op.emitOpError("input types do not match");
  }
  if (!isSameShape(in0Type, outType)) {
    return op.emitOpError("output type does not match input");
  }

  return success();
}

LogicalResult vosa::EqualOp::verify() { return verifyComparisonOp(*this); }

SAME_SHAPE_INFER(vosa::EqualOp)

LogicalResult vosa::GreaterOp::verify() { return verifyComparisonOp(*this); }

SAME_SHAPE_INFER(vosa::GreaterOp)

LogicalResult vosa::GreaterEqualOp::verify() {
  return verifyComparisonOp(*this);
}

SAME_SHAPE_INFER(vosa::GreaterEqualOp)

//===----------------------------------------------------------------------===//
// Data Manipulation operations.
//===----------------------------------------------------------------------===//

LogicalResult vosa::BroadcastChannelwiseOp::verify() {
  // ensure data type of result tensor matches the input data type
  auto inType = getInput().getType().template cast<ShapedType>();
  auto outType = getOutput().getType().template dyn_cast<TensorType>();
  auto sz = getSize();

  if (!isSameElementType(inType, outType)) {
    return emitOpError("output type does not match input");
  }

  return checkImageOutputShape(*this, outType, getHeight(inType),
                               getWidth(inType), sz);
}

LogicalResult mlir::vosa::BroadcastChannelwiseOp::inferReturnTypeComponents(
    MLIRContext *context, std::optional<Location> location,
    ValueShapeRange operands, DictionaryAttr attributes,
    OpaqueProperties properties, RegionRange regions,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {

  ShapeAdaptor inputShape = operands.getShape(0);
  if (inputShape.hasStaticShape()) {
    llvm::SmallVector<int64_t> outShape;
    inputShape.getDims(outShape);
    auto size = attributes.getAs<IntegerAttr>("size").getInt();
    outShape[getChannelDimension()] = size;
    inferredReturnShapes.push_back(ShapedTypeComponents(outShape));
  }

  return success();
}

LogicalResult vosa::BroadcastPlanewiseOp::verify() {
  // ensure data type of result tensor matches the input data type
  auto resType = getOutput().getType().template dyn_cast<TensorType>();
  if (!resType)
    return emitOpError("unsupported result type");

  auto inputShapedType = getInput().getType().cast<mlir::ShapedType>();
  if (auto inputType =
          inputShapedType.getElementType().template dyn_cast<IntegerType>()) {
    auto w = inputType.getWidth();
    if (!(w == 8 || w == 16 || w == 32))
      return emitOpError("unsupported input width");
    auto outTy = resType.getElementType().template dyn_cast<IntegerType>();
    if (!outTy)
      return emitOpError("unsupported result element type");
    if (outTy.getWidth() != w)
      return emitOpError("result element width does not match input width");
  } else if (auto inputType = inputShapedType.getElementType()
                                  .template dyn_cast<FloatType>()) {
    auto w = inputType.getWidth();
    if (!(w == 16 || w == 32))
      return emitOpError("unsupported input width");
    auto outTy = resType.getElementType().template dyn_cast<FloatType>();
    if (!outTy)
      return emitOpError("unsupported result element type");
    if (outTy.getWidth() != w)
      return emitOpError("result element width does not match input width");
  }

  return success();
}

LogicalResult mlir::vosa::BroadcastPlanewiseOp::inferReturnTypeComponents(
    MLIRContext *context, std::optional<Location> location,
    ValueShapeRange operands, DictionaryAttr attributes,
    OpaqueProperties properties, RegionRange regions,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {

  auto sizeAttr = attributes.getAs<ArrayAttr>("size");
  SmallVector<int64_t> size;
  for (Attribute val : sizeAttr.getValue()) {
    size.push_back(val.cast<IntegerAttr>().getValue().getSExtValue());
  }
  size.insert(size.begin() + getChannelDimension(), 1);

  inferredReturnShapes.push_back(ShapedTypeComponents(size));

  return success();
}

LogicalResult vosa::CastOp::verify() {
  // each input and result needs to be same sized/element type image
  auto inType = getInput().getType().template cast<ShapedType>();
  auto outType = getOutput().getType().template cast<ShapedType>();

  if (!isSameShape(inType, outType)) {
    return emitOpError("output shape does not match input");
  }

  return success();
}

LogicalResult mlir::vosa::CastOp::inferReturnTypeComponents(
    MLIRContext *context, std::optional<Location> location,
    ValueShapeRange operands, DictionaryAttr attributes,
    OpaqueProperties properties, RegionRange regions,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {

  ShapeAdaptor inputShape = operands.getShape(0);
  if (inputShape.hasStaticShape()) {
    llvm::SmallVector<int64_t> outShape;
    inputShape.getDims(outShape);
    inferredReturnShapes.push_back(ShapedTypeComponents(outShape));
  }

  return success();
}

LogicalResult vosa::ChannelExtractOp::verify() {
  // each input and result needs to be same sized/element type image
  auto inType = getInput().getType().template cast<ShapedType>();
  auto outType = getOutput().getType().template cast<ShapedType>();

  SmallVector<int64_t> channels;
  getIntegerArrayAttr(getChannels(), channels);

  if (!isSameElementType(inType, outType)) {
    return emitOpError("output type does not match input");
  }

  // check all channel indexes are valid for the input
  auto nChannels = ::getChannels(inType);
  if (nChannels != ShapedType::kDynamic) {
    for (auto c : channels) {
      if (c < 0 || c >= nChannels) {
        return emitOpError("invalid channel index");
      }
    }
  }

  // check input & output are of same HxW
  if (!checkHxW(getHeight(inType), getWidth(inType), getHeight(outType),
                getWidth(outType))) {
    return emitOpError("input size != output size");
  }

  return success();
}

LogicalResult mlir::vosa::ChannelExtractOp::inferReturnTypeComponents(
    MLIRContext *context, std::optional<Location> location,
    ValueShapeRange operands, DictionaryAttr attributes,
    OpaqueProperties properties, RegionRange regions,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {

  ShapeAdaptor inputShape = operands.getShape(0);
  if (inputShape.hasStaticShape()) {
    auto channelsAttr = attributes.getAs<ArrayAttr>("channels");
    int64_t nChannels = channelsAttr.getValue().size();

    llvm::SmallVector<int64_t> outShape;
    inputShape.getDims(outShape);
    outShape[getChannelDimension()] = nChannels;
    inferredReturnShapes.push_back(ShapedTypeComponents(outShape));
  }

  return success();
}

LogicalResult vosa::ConcatOp::verify() {
  // each input and result needs to be same sized/element type image
  auto outType = getOutput().getType().template cast<ShapedType>();

  int64_t totalChannels = 0;
  for (auto input : getInput()) {
    auto inType = input.getType().template cast<ShapedType>();

    if (!isSameElementType(inType, outType)) {
      return emitOpError("output type does not match input");
    }

    // check input & output are of same HxW - channels can vary
    if (!checkHxW(getHeight(inType), getWidth(inType), getHeight(outType),
                  getWidth(outType))) {
      return emitOpError("input size != output size");
    }

    int64_t channels = getChannels(inType);
    if (totalChannels != ShapedType::kDynamic &&
        channels != ShapedType::kDynamic) {
      totalChannels += channels;
    } else {
      totalChannels = ShapedType::kDynamic;
    }
  }

  if (totalChannels != ShapedType::kDynamic &&
      getChannels(outType) != ShapedType::kDynamic &&
      totalChannels != getChannels(outType)) {
    return emitOpError(
        "output channel count does not match total input channel count");
  }

  return success();
}

LogicalResult mlir::vosa::ConcatOp::inferReturnTypeComponents(
    MLIRContext *context, std::optional<Location> location,
    ValueShapeRange operands, DictionaryAttr attributes,
    OpaqueProperties properties, RegionRange regions,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {

  int64_t planes = 0;
  for (const auto &o : operands) {
    ShapeAdaptor opShape = operands.getShape(o);
    if (opShape.hasStaticShape())
      planes += opShape.getDimSize(getChannelDimension());
    else {
      // can't infer with any dynamic dimensions
      planes = ShapedType::kDynamic;
      break;
    }
  }

  if (planes != ShapedType::kDynamic) {
    ShapeAdaptor inputShape = operands.getShape(0);
    llvm::SmallVector<int64_t> outShape;
    inputShape.getDims(outShape);
    outShape[getChannelDimension()] = planes;
    inferredReturnShapes.push_back(ShapedTypeComponents(outShape));
  }

  return success();
}

LogicalResult vosa::ExportChannelOp::verify() {
  auto inType = getInput().getType().template cast<ShapedType>();
  auto destType = getDest().getType().template cast<ShapedType>();
  auto outType = getOutput().getType().template cast<ShapedType>();
  auto elBytes = outType.getElementType().getIntOrFloatBitWidth() / 8;

  if (!isSameElementType(inType, outType)) {
    return emitOpError("output element type does not match input");
  }

  if (!isSameSizeAndType(destType, outType)) {
    return emitOpError("output type does not match dest type");
  }

  if (!inType.hasStaticShape() || !outType.hasStaticShape()) {
    // can't verify dynamic shapes at compile time
    return success();
  }

  // max index is ( H-1, W-1 ) which has unstrided offset ( (H-1)*W + (W-1) ) =
  // (H*W-1)
  auto reqdOutSize =
      (getHeight(inType) * getWidth(inType) - 1) * getStride() / elBytes +
      getOffset() / elBytes + 1;

  if (outType.getDimSize(0) < reqdOutSize) {
    return emitOpError("invalid output size");
  }

  return success();
}

LogicalResult vosa::ImportChannelOp::verify() {
  auto inType = getInput().getType().template cast<ShapedType>();
  auto outType = getOutput().getType().template cast<ShapedType>();
  auto elBytes = inType.getElementType().getIntOrFloatBitWidth() / 8;

  if (!isSameElementType(inType, outType)) {
    return emitOpError("output element type does not match input");
  }

  // check shape attr matches output
  SmallVector<int64_t> outShape;
  getIntegerArrayAttr(getShape(), outShape);
  if (!checkHxW(getHeight(outType), getWidth(outType), outShape[0],
                outShape[1])) {
    return emitOpError("output shape does not match shape attr");
  }

  if (!inType.hasStaticShape()) {
    // can't verify dynamic shapes at compile time
    return success();
  }

  // max index is ( H-1, W-1 ) which has unstrided offset ( (H-1)*W + (W-1) ) =
  // (H*W-1)
  auto reqdInSize = (outShape[0] * outShape[1] - 1) * getStride() / elBytes +
                    getOffset() / elBytes + 1;

  if (inType.getDimSize(0) < reqdInSize) {
    return emitOpError("invalid input size");
  }

  return success();
}

LogicalResult vosa::MeshGridOp::verify() {
  auto outType = getOutput().getType().template cast<ShapedType>();

  // check shape attr matches output
  SmallVector<int64_t> outShape;
  getIntegerArrayAttr(getShape(), outShape);
  return checkImageOutputShape(*this, outType, outShape[0], outShape[1], 2);
}

LogicalResult mlir::vosa::MeshGridOp::inferReturnTypeComponents(
    MLIRContext *context, std::optional<Location> location,
    ValueShapeRange operands, DictionaryAttr attributes,
    OpaqueProperties properties, RegionRange regions,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {

  // 2D image with HW defined by shape attr
  SmallVector<int64_t> shape;
  auto shapeAttr = attributes.getAs<ArrayAttr>("shape");
  for (Attribute val : shapeAttr.getValue()) {
    shape.push_back(val.cast<IntegerAttr>().getValue().getSExtValue());
  }
  shape.insert(shape.begin() + getChannelDimension(), 2);

  inferredReturnShapes.push_back(ShapedTypeComponents(shape));

  return success();
}

LogicalResult vosa::PiecewiseLinearOp::verify() {
  auto inputType = getInput().getType().template cast<ShapedType>();
  auto outputType = getOutput().getType().template cast<ShapedType>();
  auto valuesType = getValues().getType().template dyn_cast<ShapedType>();
  auto nodesType = getNodes().getType().template dyn_cast<ShapedType>();
  if (!valuesType || !nodesType) {
    return emitOpError("missing attribute type info");
  }
  if (!isSameSizeAndType(inputType, outputType)) {
    return emitOpError("output type does not match input");
  }
  int valuesSize = valuesType.getDimSize(0);
  int nodesSize = nodesType.getDimSize(0);
  if (valuesSize != nodesSize) {
    return emitOpError("attributes do not have matching sizes");
  }
  auto inputElementType = inputType.getElementType();
  auto valuesElementType = valuesType.getElementType();
  auto nodesElementType = nodesType.getElementType();
  if (valuesElementType != inputElementType ||
      nodesElementType != inputElementType) {
    return emitOpError("attributes and input element types do not match");
  }

  return success();
}

SAME_SHAPE_INFER(vosa::PiecewiseLinearOp)

LogicalResult vosa::WhereOp::verify() {
  auto in0Type = getInput0().getType().template cast<ShapedType>();
  auto in1Type = getInput1().getType().template cast<ShapedType>();
  auto in2Type = getInput2().getType().template cast<ShapedType>();
  auto outType = getOutput().getType().template cast<ShapedType>();

  if (!isSameSizeAndType(in0Type, in1Type)) {
    return emitOpError("input types do not match");
  }
  if (!isSameSizeAndType(in1Type, outType)) {
    return emitOpError("output type does not match input");
  }

  if (!isSameShape(in0Type, in2Type)) { // If where mask isn't the same shape as
                                        // first two input tensors
    return emitOpError("input shapes do not match");
  }

  return success();
}

SAME_SHAPE_INFER(vosa::WhereOp)

//===----------------------------------------------------------------------===//
// Elementwise binary operations.
//===----------------------------------------------------------------------===//

template <typename O>
static LogicalResult verifyBinaryOp(O op) {
  // each input and result needs to be same sized/element type image
  auto in0Type = op.getInput0().getType().template cast<ShapedType>();
  auto in1Type = op.getInput1().getType().template cast<ShapedType>();
  auto outType = op.getOutput().getType().template cast<ShapedType>();

  if (!isSameSizeAndType(in0Type, in1Type)) {
    return op.emitOpError("input types do not match");
  }
  if (!isSameSizeAndType(in0Type, outType)) {
    return op.emitOpError("output type does not match input");
  }

  return success();
}

LogicalResult vosa::AbsDiffOp::verify() {
  auto in0Type = getInput0().getType().template cast<ShapedType>();
  auto in1Type = getInput1().getType().template cast<ShapedType>();
  auto in0ElementTypeInt =
      in0Type.getElementType().template dyn_cast<IntegerType>();
  auto in0ElementTypeFloat =
      in0Type.getElementType().template dyn_cast<FloatType>();

  auto outType = getOutput().getType().template cast<ShapedType>();

  if (!isSameSizeAndType(in0Type, in1Type)) {
    return emitOpError("input types do not match");
  }
  if (!isSameShape(in0Type, outType)) {
    return emitOpError("output shape does not match input");
  }

  if (in0ElementTypeFloat &&
      !isSameElementType(in0Type,
                         outType)) { // If input and output types are floating
                                     // point and not the same size
    return emitOpError("output type does not match input");
  }

  if (in0ElementTypeInt &&
      in0ElementTypeInt.isSigned()) { // If input types are signed integers
    // int8, int8 -> int16
    // int16, int16 -> int32
    // int32, int32 -> int32
    auto outElementType =
        outType.getElementType().template dyn_cast<IntegerType>();
    if (!outElementType ||
        !outElementType
             .isSignedInteger()) { // If output type is not a signed integer
      return emitOpError("invalid output type");
    }

    int inElementWidth = in0ElementTypeInt.getIntOrFloatBitWidth();
    int outElementWidth = outElementType.getIntOrFloatBitWidth();
    if ((inElementWidth == 8 && outElementWidth != 16) ||
        (inElementWidth != 8 && outElementWidth != 32)) {
      return emitOpError("invalid output type");
    }
  }

  if (in0ElementTypeInt && !in0ElementTypeInt.isSigned() &&
      !isSameElementType(in0Type, outType)) {
    return emitOpError("output type does not match input");
  }

  return success();
}

SAME_SHAPE_INFER(vosa::AbsDiffOp)

LogicalResult vosa::AddOp::verify() { return verifyBinaryOp(*this); }

SAME_SHAPE_INFER(vosa::AddOp)

LogicalResult vosa::AndOp::verify() { return verifyBinaryOp(*this); }

SAME_SHAPE_INFER(vosa::AndOp)

LogicalResult vosa::DivOp::verify() { return verifyBinaryOp(*this); }

SAME_SHAPE_INFER(vosa::DivOp)

LogicalResult vosa::OrOp::verify() { return verifyBinaryOp(*this); }

SAME_SHAPE_INFER(vosa::OrOp)

LogicalResult vosa::MaxOp::verify() { return verifyBinaryOp(*this); }

SAME_SHAPE_INFER(vosa::MaxOp)

LogicalResult vosa::MinOp::verify() { return verifyBinaryOp(*this); }

SAME_SHAPE_INFER(vosa::MinOp)

LogicalResult vosa::ModOp::verify() { return verifyBinaryOp(*this); }

SAME_SHAPE_INFER(vosa::ModOp)

LogicalResult vosa::MultOp::verify() { return verifyBinaryOp(*this); }

SAME_SHAPE_INFER(vosa::MultOp)

LogicalResult vosa::SubOp::verify() { return verifyBinaryOp(*this); }

SAME_SHAPE_INFER(vosa::SubOp)

//===----------------------------------------------------------------------===//
// Elementwise unary operations.
//===----------------------------------------------------------------------===//

template <typename O>
static LogicalResult verifyUnaryOp(O op) {
  // each input and result needs to be same sized/element type image
  auto inType = op.getInput().getType().template cast<ShapedType>();
  auto outType = op.getOutput().getType().template cast<ShapedType>();

  if (!isSameSizeAndType(inType, outType)) {
    return op.emitOpError("output type does not match input");
  }

  return success();
}

LogicalResult vosa::AbsOp::verify() { return verifyUnaryOp(*this); }

SAME_SHAPE_INFER(vosa::AbsOp)

LogicalResult vosa::ArithmeticShiftRightOp::verify() {
  return verifyUnaryOp(*this);
}

SAME_SHAPE_INFER(vosa::ArithmeticShiftRightOp)

LogicalResult vosa::ClampOp::verify() {
  // each input and result needs to be same sized/element type image
  auto in0Type = getInput0().getType().template cast<ShapedType>();
  auto in1Type = getInput1().getType();
  auto in2Type = getInput1().getType();
  auto outType = getOutput().getType().template cast<ShapedType>();

  if (in0Type.getElementType() != in1Type) {
    return emitOpError("input / min types do not match");
  }
  if (in0Type.getElementType() != in2Type) {
    return emitOpError("input / max types do not match");
  }
  if (!isSameSizeAndType(in0Type, outType)) {
    return emitOpError("output type does not match input");
  }

  return success();
}

SAME_SHAPE_INFER(vosa::ClampOp)

LogicalResult vosa::GammaCorrectionOp::verify() {
  auto gammaAttr = getGamma().dyn_cast<FloatAttr>();
  auto inputType = getInput().getType().cast<ShapedType>().getElementType();
  if (!gammaAttr) {
    return emitOpError("missing or invalid gamma attribute");
  } else if (gammaAttr.getType() != inputType) {
    return emitOpError("gamma attribute and input element types do not match");
  } else
    return verifyUnaryOp(*this);
}

SAME_SHAPE_INFER(vosa::GammaCorrectionOp)

LogicalResult vosa::LogicalShiftRightOp::verify() {
  return verifyUnaryOp(*this);
}

SAME_SHAPE_INFER(vosa::LogicalShiftRightOp)

LogicalResult vosa::NotOp::verify() { return verifyUnaryOp(*this); }

SAME_SHAPE_INFER(vosa::NotOp)

LogicalResult vosa::PowerOp::verify() {
  auto baseAttr = getBase().dyn_cast<FloatAttr>();
  auto inputType = getInput().getType().cast<ShapedType>().getElementType();
  if (!baseAttr) {
    return emitOpError("missing or invalid base attribute");
  } else if (baseAttr.getType() != inputType) {
    return emitOpError("base attribute and input element types do not match");
  } else
    return verifyUnaryOp(*this);
}

SAME_SHAPE_INFER(vosa::PowerOp)

LogicalResult vosa::RoundOp::verify() {
  auto inType = getInput().getType().template cast<ShapedType>();
  auto outType = getOutput().getType().template cast<ShapedType>();
  if (!isSameShape(inType, outType)) {
    return emitOpError("output shape does not match input");
  }

  return success();
}

SAME_SHAPE_INFER(vosa::RoundOp)

LogicalResult vosa::SqrtOp::verify() { return verifyUnaryOp(*this); }

SAME_SHAPE_INFER(vosa::SqrtOp)

//===----------------------------------------------------------------------===//
// Image Filtering operations.
//===----------------------------------------------------------------------===//

LogicalResult vosa::Conv2DOp::verify() {
  auto inType = getInput().getType().template cast<ShapedType>();
  auto filtType = getFilter().getType().template cast<ShapedType>();
  auto outType = getOutput().getType().template cast<ShapedType>();

  // Check filter & output are of same element type. The VOSA spec also
  // requires the input element type to match the output & filter element type,
  // but we are relaxing that to remove the need for a dedicated vosa.cast op
  // before invoking vosa.conv_2d.
  if (!isSameElementType(filtType, outType)) {
    return emitOpError("filter and output type don't match");
  }

  // check output is sized H-FH, W-FW
  if (inType.getRank() != 3) {
    return emitOpError("invalid input shape");
  }
  auto filterShape = filtType.getShape();
  if (filterShape.size() != 2) {
    return emitOpError("invalid filter shape");
  }

  return checkImageOutputShape(
      *this, outType, getHeight(inType) - filterShape[0] + 1,
      getWidth(inType) - filterShape[1] + 1, getChannels(inType));
}

LogicalResult mlir::vosa::Conv2DOp::inferReturnTypeComponents(
    MLIRContext *context, std::optional<Location> location,
    ValueShapeRange operands, DictionaryAttr attributes,
    OpaqueProperties properties, RegionRange regions,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {
  ShapeAdaptor inputShape = operands.getShape(0);
  auto filterAttr = attributes.getAs<DenseElementsAttr>("filter");
  auto filterShape = filterAttr.getType().getShape();
  // out is in - kernel + 1
  if (inputShape.hasStaticShape() && filterShape.size() == 2) {
    llvm::SmallVector<int64_t> outShape;
    outShape.push_back(getHeight(inputShape) - filterShape[0] + 1);
    outShape.push_back(getWidth(inputShape) - filterShape[1] + 1);
    outShape.push_back(getChannels(inputShape));
    outShape = hwcToDataFormatShape(outShape);
    inferredReturnShapes.push_back(ShapedTypeComponents(outShape));
  }

  return success();
}

//===----------------------------------------------------------------------===//
// Image Transformation operations.
//===----------------------------------------------------------------------===//

LogicalResult vosa::DecimateOp::verify() {
  auto inType = getInput().getType().template cast<ShapedType>();
  auto outType = getOutput().getType().template cast<ShapedType>();

  if (!isSameElementType(inType, outType)) {
    return emitOpError("invalid output type");
  }

  SmallVector<int64_t> N;
  getIntegerArrayAttr(this->getN(), N);
  SmallVector<int64_t> offsets;
  getIntegerArrayAttr(this->getOffsets(), offsets);
  if (offsets[0] >= N[0] || offsets[0] >= N[0]) {
    return emitOpError("invalid offsets");
  }

  // check output is sized [(H + N[0] - 1)/N[0], (W + N[1] - 1)/N[1], C]
  return checkImageOutputShape(
      *this, outType, (getHeight(inType) + N[0] - 1) / N[0],
      (getWidth(inType) + N[1] - 1) / N[1], getChannels(inType));
}

LogicalResult mlir::vosa::DecimateOp::inferReturnTypeComponents(
    MLIRContext *context, std::optional<Location> location,
    ValueShapeRange operands, DictionaryAttr attributes,
    OpaqueProperties properties, RegionRange regions,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {
  ShapeAdaptor inputShape = operands.getShape(0);
  SmallVector<int64_t> N;
  auto NAttr = attributes.getAs<ArrayAttr>("N");
  for (Attribute val : NAttr.getValue()) {
    N.push_back(val.cast<IntegerAttr>().getValue().getSExtValue());
  }

  // out is in + padding
  if (inputShape.hasStaticShape()) {
    llvm::SmallVector<int64_t> outShape;
    outShape.push_back((getHeight(inputShape) + N[0] - 1) / N[0]);
    outShape.push_back((getWidth(inputShape) + N[1] - 1) / N[1]);
    outShape.push_back(getChannels(inputShape));
    outShape = hwcToDataFormatShape(outShape);
    inferredReturnShapes.push_back(ShapedTypeComponents(outShape));
  }

  return success();
}

LogicalResult vosa::ExpandOp::verify() {
  auto inputType = getInput().getType().template cast<ShapedType>();
  auto inputElType = inputType.getElementType();
  auto outputType = getOutput().getType().template cast<ShapedType>();
  auto outputElType = outputType.getElementType();
  auto expandKernelType =
      getExpandKernel().getType().template cast<ShapedType>();
  auto gatherKernelType =
      getGatherKernel().getType().template dyn_cast<ShapedType>();
  if (!gatherKernelType) {
    return emitOpError("missing attribute type info");
  }
  auto expandType = expandKernelType.getElementType();
  auto gatherType = gatherKernelType.getElementType();
  auto gatherRank = gatherKernelType.getRank();

  if (gatherType != expandType || gatherRank != 2) {
    return emitOpError("gather kernel type must be 2D tensor of ui8");
  }

  if (inputElType != outputElType) {
    return emitOpError("input and output element types do not match");
  }

  Type fillValueType;
  if (auto integerFillValueAttr = getFillValue().dyn_cast<IntegerAttr>()) {
    fillValueType = integerFillValueAttr.getType();
  } else if (auto floatFillValueAttr = getFillValue().dyn_cast<FloatAttr>()) {
    fillValueType = floatFillValueAttr.getType();
  }
  if (fillValueType != inputElType) {
    return emitOpError(
        "fill value type must match input and output element types");
  }

  return checkImageOutputShape(
      *this, getOutput().getType().template cast<ShapedType>(),
      (getHeight(inputType) * getHeight(expandKernelType)) /
          gatherKernelType.getDimSize(0),
      (getWidth(inputType) * getWidth(expandKernelType)) /
          gatherKernelType.getDimSize(1),
      getChannels(inputType));
}

LogicalResult mlir::vosa::ExpandOp::inferReturnTypeComponents(
    MLIRContext *context, std::optional<Location> locatioon,
    ValueShapeRange operands, DictionaryAttr attributes,
    OpaqueProperties properties, RegionRange regions,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {
  ShapeAdaptor inputShape = operands.getShape(0);
  ShapeAdaptor expandShape = operands.getShape(1);
  auto gatherAttr = attributes.getAs<DenseElementsAttr>("gather_kernel");
  auto gatherShape = gatherAttr.getType().template cast<ShapedType>();
  int outputHeight = (getHeight(inputShape) * getHeight(expandShape)) /
                     gatherShape.getDimSize(0);
  int outputWidth = (getWidth(inputShape) * getWidth(expandShape)) /
                    gatherShape.getDimSize(1);
  if (inputShape.hasStaticShape()) {
    llvm::SmallVector<int64_t> outShape;
    outShape.push_back(outputHeight);
    outShape.push_back(outputWidth);
    outShape.push_back(getChannels(inputShape));
    outShape = hwcToDataFormatShape(outShape);
    inferredReturnShapes.push_back(ShapedTypeComponents(outShape));
  }
  return success();
}

LogicalResult vosa::PadOp::verify() {
  auto inType = getInput().getType().template cast<ShapedType>();
  auto outType = getOutput().getType().template cast<ShapedType>();
  auto pad_mode = getMode();

  if (pad_mode == PadMode::constant) {
    // check pad_constant is present and of same element type as input
    auto pad_val = getPadConstant();
    if (!pad_val) {
      return emitOpError("constant pad attribute not specified");
    }
    Type padType;
    if (auto integerPadValueAttr = pad_val.value().dyn_cast<IntegerAttr>()) {
      padType = integerPadValueAttr.getType();
    } else if (auto floatPadValueAttr = pad_val.value().dyn_cast<FloatAttr>()) {
      padType = floatPadValueAttr.getType();
    }
    if (inType.getElementType() != padType) {
      return emitOpError("pad type does not match input");
    }
  }

  // check output is sized H+pad, W+pad
  if (inType.getRank() != 3) {
    return emitOpError("invalid input shape");
  }
  SmallVector<int64_t> pad_sz;
  getIntegerArrayAttr(getPadSize(), pad_sz);
  if (pad_sz.size() != 4) {
    return emitOpError("invalid pad_size");
  }

  return checkImageOutputShape(
      *this, outType, getHeight(inType) + pad_sz[0] + pad_sz[1],
      getWidth(inType) + pad_sz[2] + pad_sz[3], getChannels(inType));
}

LogicalResult mlir::vosa::PadOp::inferReturnTypeComponents(
    MLIRContext *context, std::optional<Location> location,
    ValueShapeRange operands, DictionaryAttr attributes,
    OpaqueProperties properties, RegionRange regions,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {
  ShapeAdaptor inputShape = operands.getShape(0);
  SmallVector<int64_t> padSize;
  auto padSizeAttr = attributes.getAs<ArrayAttr>("pad_size");
  for (Attribute val : padSizeAttr.getValue()) {
    padSize.push_back(val.cast<IntegerAttr>().getValue().getSExtValue());
  }

  // out is in + padding
  if (inputShape.hasStaticShape()) {
    llvm::SmallVector<int64_t> outShape;
    outShape.push_back(getHeight(inputShape) + padSize[0] + padSize[1]);
    outShape.push_back(getWidth(inputShape) + padSize[2] + padSize[3]);
    outShape.push_back(getChannels(inputShape));
    outShape = hwcToDataFormatShape(outShape);
    inferredReturnShapes.push_back(ShapedTypeComponents(outShape));
  }

  return success();
}

LogicalResult vosa::PointwiseMatrixMultiplyOp::verify() {
  auto inType = getInput().getType().template cast<ShapedType>();
  auto outType = getOutput().getType().template cast<ShapedType>();
  auto MType = getM().getType().template dyn_cast<ShapedType>();
  auto K1Type = getK_1().getType().template dyn_cast<ShapedType>();
  auto K2Type = getK_2().getType().template dyn_cast<ShapedType>();
  if (!MType || !K1Type || !K2Type) {
    return emitOpError("missing attribute type info");
  }
  int64_t inChannels = getChannels(inType);
  int64_t outChannels = getChannels(outType);
  if (!checkHxW(MType.getDimSize(0), MType.getDimSize(1), outChannels,
                inChannels)) {
    return emitOpError("invalid shape for M");
  }
  if (K1Type.getRank() != 1 ||
      (inChannels != ShapedType::kDynamic &&
       K1Type.getDimSize(0) != inChannels) ||
      (K1Type.getDimSize(0) != MType.getDimSize(1))) {
    return emitOpError("invalid shape for K_1");
  }
  if (K2Type.getRank() != 1 ||
      (outChannels != ShapedType::kDynamic &&
       K2Type.getDimSize(0) != outChannels) ||
      (K2Type.getDimSize(0) != MType.getDimSize(0))) {
    return emitOpError("invalid shape for K_2");
  }
  auto inElementType = inType.getElementType();
  if (MType.getElementType() != inElementType ||
      K1Type.getElementType() != inElementType ||
      K2Type.getElementType() != inElementType) {
    return emitOpError("attributes and input element types do not match");
  }
  if (!isSameElementType(inType, outType)) {
    return emitOpError("output type does not match input");
  }
  return checkImageOutputShape(*this, outType, getHeight(inType),
                               getWidth(inType), outChannels);
}

LogicalResult mlir::vosa::PointwiseMatrixMultiplyOp::inferReturnTypeComponents(
    MLIRContext *context, std::optional<Location> locatioon,
    ValueShapeRange operands, DictionaryAttr attributes,
    OpaqueProperties properties, RegionRange regions,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {
  ShapeAdaptor inputShape = operands.getShape(0);
  auto k2Shape =
      attributes.getAs<DenseElementsAttr>("K_2").getType().getShape();
  auto outChannels = k2Shape[0];
  if (inputShape.hasStaticShape()) {
    llvm::SmallVector<int64_t> outShape;
    outShape.push_back(getHeight(inputShape));
    outShape.push_back(getWidth(inputShape));
    outShape.push_back(outChannels);
    outShape = hwcToDataFormatShape(outShape);
    inferredReturnShapes.push_back(ShapedTypeComponents(outShape));
  }
  return success();
}

LogicalResult vosa::ResizeBilinearOp::verify() {
  auto inType = getInput().getType().template cast<ShapedType>();
  auto outType = getOutput().getType().template cast<ShapedType>();

  if (!isSameElementType(inType, outType)) {
    return emitOpError("output type does not match input");
  }

  SmallVector<int64_t> sz;
  getIntegerArrayAttr(getSize(), sz);

  return checkImageOutputShape(*this, outType, sz[0], sz[1],
                               getChannels(inType));
}

LogicalResult mlir::vosa::ResizeBilinearOp::inferReturnTypeComponents(
    MLIRContext *context, std::optional<Location> location,
    ValueShapeRange operands, DictionaryAttr attributes,
    OpaqueProperties properties, RegionRange regions,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {
  ShapeAdaptor inputShape = operands.getShape(0);

  // out is size attr
  SmallVector<int64_t> size;
  auto sizeAttr = attributes.getAs<ArrayAttr>("size");
  for (Attribute val : sizeAttr.getValue()) {
    size.push_back(val.cast<IntegerAttr>().getValue().getSExtValue());
  }

  if (inputShape.hasStaticShape()) {
    llvm::SmallVector<int64_t> outShape;
    outShape.push_back(size[0]);
    outShape.push_back(size[1]);
    outShape.push_back(getChannels(inputShape));
    outShape = hwcToDataFormatShape(outShape);
    inferredReturnShapes.push_back(ShapedTypeComponents(outShape));
  }

  return success();
}

LogicalResult vosa::ResizeNearestNeighbourOp::verify() {
  auto inType = getInput().getType().template cast<ShapedType>();
  auto outType = getOutput().getType().template cast<ShapedType>();

  if (!isSameElementType(inType, outType)) {
    return emitOpError("output type does not match input");
  }

  SmallVector<int64_t> sz;
  getIntegerArrayAttr(getSize(), sz);

  return checkImageOutputShape(*this, outType, sz[0], sz[1],
                               getChannels(inType));
}

LogicalResult mlir::vosa::ResizeNearestNeighbourOp::inferReturnTypeComponents(
    MLIRContext *context, std::optional<Location> location,
    ValueShapeRange operands, DictionaryAttr attributes,
    OpaqueProperties properties, RegionRange regions,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {
  ShapeAdaptor inputShape = operands.getShape(0);

  SmallVector<int64_t> size;
  auto sizeAttr = attributes.getAs<ArrayAttr>("size");
  for (Attribute val : sizeAttr.getValue()) {
    size.push_back(val.cast<IntegerAttr>().getValue().getSExtValue());
  }

  if (inputShape.hasStaticShape()) {
    llvm::SmallVector<int64_t> outShape;
    outShape.push_back(size[0]);
    outShape.push_back(size[1]);
    outShape.push_back(getChannels(inputShape));
    outShape = hwcToDataFormatShape(outShape);
    inferredReturnShapes.push_back(ShapedTypeComponents(outShape));
  }

  return success();
}

LogicalResult vosa::Rotate90Op::verify() {
  // Verify input and output types match
  // Check output shape
  auto inputType = getInput().getType().template cast<ShapedType>();
  auto outputType = getOutput().getType().template cast<ShapedType>();
  if (!isSameElementType(inputType, outputType)) {
    return emitOpError("input and output element types do not match");
  }

  bool changeDim = (getRotate() % 2) != 0;
  int64_t inH = getHeight(inputType);
  int64_t inW = getWidth(inputType);
  int64_t inC = getChannels(inputType);
  auto expectedH = changeDim ? inW : inH;
  auto expectedW = changeDim ? inH : inW;
  return checkImageOutputShape(*this, outputType, expectedH, expectedW, inC);
}

LogicalResult mlir::vosa::Rotate90Op::inferReturnTypeComponents(
    MLIRContext *context, std::optional<Location> location,
    ValueShapeRange operands, DictionaryAttr attributes,
    OpaqueProperties properties, RegionRange regions,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {
  auto rotateAttr = attributes.getAs<IntegerAttr>("rotate");
  ShapeAdaptor inputShape = operands.getShape(0);
  bool changeDim = rotateAttr.getValue().srem(2) != 0;

  int64_t inH = getHeight(inputShape);
  int64_t inW = getWidth(inputShape);
  int64_t inC = getChannels(inputShape);
  auto outputHeight = changeDim ? inW : inH;
  auto outputWidth = changeDim ? inH : inW;
  if (inputShape.hasStaticShape()) {
    llvm::SmallVector<int64_t> outShape;
    outShape.push_back(outputHeight);
    outShape.push_back(outputWidth);
    outShape.push_back(inC);
    outShape = hwcToDataFormatShape(outShape);
    inferredReturnShapes.push_back(ShapedTypeComponents(outShape));
  }
  return success();
}

//===----------------------------------------------------------------------===//
// Reduction operations.
//===----------------------------------------------------------------------===//

template <typename O>
static LogicalResult verifyArgChannelwiseOp(O op) {
  auto inType = op.getInput().getType().template cast<ShapedType>();
  auto outType = op.getOutput().getType().template cast<ShapedType>();

  if (!checkHxW(getHeight(inType), getWidth(inType), getHeight(outType),
                getWidth(outType))) {
    return op.emitOpError("input size != output size");
  }

  return success();
}

template <typename O>
static LogicalResult verifyReduceChannelwiseOp(O op) {
  auto inType = op.getInput().getType().template cast<ShapedType>();
  auto outType = op.getOutput().getType().template cast<ShapedType>();

  if (inType.getElementType() != outType.getElementType()) {
    return op.emitOpError(
        "output element type does not match input element type");
  }
  if (!checkHxW(getHeight(inType), getWidth(inType), getHeight(outType),
                getWidth(outType))) {
    return op.emitOpError("input size != output size");
  }

  return success();
}

template <typename O>
static LogicalResult verifyReducePlanewiseOp(O op) {
  auto inType = op.getInput().getType().template cast<ShapedType>();
  auto outType = op.getOutput().getType().template cast<ShapedType>();

  if (inType.getElementType() != outType.getElementType()) {
    return op.emitOpError("output type does not match input element type");
  }

  return success();
}

#define REDUCE_CHANNELWISE_SHAPE_INFER(OP)                                     \
  LogicalResult OP::inferReturnTypeComponents(                                 \
      MLIRContext *context, std::optional<Location> location,                  \
      ValueShapeRange operands, DictionaryAttr attributes,                     \
      OpaqueProperties properties, RegionRange regions,                        \
      SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {           \
    return SingleChannelReturnType(operands, inferredReturnShapes);            \
  }

LogicalResult SingleChannelReturnType(
    ValueShapeRange operands,
    SmallVectorImpl<ShapedTypeComponents> &inferredReturnShapes) {
  ShapeAdaptor inputShape = operands.getShape(0);
  if (inputShape.hasStaticShape()) {
    llvm::SmallVector<int64_t> outShape;
    inputShape.getDims(outShape);
    outShape[getChannelDimension()] = 1;
    inferredReturnShapes.push_back(ShapedTypeComponents(outShape));
  }

  return success();
}

LogicalResult vosa::ArgMaxChannelwiseOp::verify() {
  return verifyArgChannelwiseOp(*this);
}

REDUCE_CHANNELWISE_SHAPE_INFER(ArgMaxChannelwiseOp)

LogicalResult vosa::ArgMinChannelwiseOp::verify() {
  return verifyArgChannelwiseOp(*this);
}

REDUCE_CHANNELWISE_SHAPE_INFER(ArgMinChannelwiseOp)

LogicalResult vosa::ReduceInterpChannelwiseOp::verify() {
  auto in0Type = getInput0().getType().template cast<ShapedType>();
  auto in1Type = getInput1().getType().template cast<ShapedType>();
  auto outType = getOutput().getType().template cast<ShapedType>();

  // check input0, input1, output are of same HxW
  if (!checkHxW(getHeight(in0Type), getWidth(in0Type), getHeight(in1Type),
                getWidth(in1Type))) {
    return emitOpError("input0 size != input1 size");
  }
  if (!checkHxW(getHeight(in0Type), getWidth(in0Type), getHeight(outType),
                getWidth(outType))) {
    return emitOpError("input0 size != output size");
  }

  return success();
}

REDUCE_CHANNELWISE_SHAPE_INFER(ReduceInterpChannelwiseOp)

LogicalResult vosa::ReduceMaxChannelwiseOp::verify() {
  return verifyReduceChannelwiseOp(*this);
}

REDUCE_CHANNELWISE_SHAPE_INFER(ReduceMaxChannelwiseOp)

LogicalResult vosa::ReduceMaxPlanewiseOp::verify() {
  return verifyReducePlanewiseOp(*this);
}

LogicalResult vosa::ReduceMinChannelwiseOp::verify() {
  return verifyReduceChannelwiseOp(*this);
}

REDUCE_CHANNELWISE_SHAPE_INFER(ReduceMinChannelwiseOp)

LogicalResult vosa::ReduceMinPlanewiseOp::verify() {
  return verifyReducePlanewiseOp(*this);
}

LogicalResult vosa::ReduceSumChannelwiseOp::verify() {
  return verifyReduceChannelwiseOp(*this);
}

REDUCE_CHANNELWISE_SHAPE_INFER(ReduceSumChannelwiseOp)

LogicalResult vosa::ReduceSumPlanewiseOp::verify() {
  return verifyReducePlanewiseOp(*this);
}

#define GET_OP_CLASSES
#include "vosa/Dialect/Vosa/IR/VosaOps.cpp.inc"
