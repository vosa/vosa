/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "reduce_max_planewise.h"

std::shared_ptr<vosa::Op> vosa::ReduceMaxPlanewiseBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_reduce_max_planewise =
      vosa_operator_union.AsREDUCE_MAX_PLANEWISE();
  assert(serialized_reduce_max_planewise);
  auto mode = serialized_reduce_max_planewise->mode;
  switch (mode) {
  case vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::ReduceMaxPlanewise<uint8_t>>();
  }
  case vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::ReduceMaxPlanewise<uint16_t>>();
  }
  case vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::ReduceMaxPlanewise<uint32_t>>();
  }
  case vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::ReduceMaxPlanewise<int8_t>>();
  }
  case vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::ReduceMaxPlanewise<int16_t>>();
  }
  case vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::ReduceMaxPlanewise<int32_t>>();
  }
  case vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::ReduceMaxPlanewise<float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::EigenImage<T>
vosa::ReduceMaxPlanewise<T>::runme(const EigenImage<T> &input) const {

  uint32_t height = input.dimension(0);
  uint32_t width = input.dimension(1);
  uint32_t num_channels = input.dimension(2);
  assert(num_channels == 1);

  T max_value = std::numeric_limits<T>::lowest();
  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      T raw_input = input(height_index, width_index, 0);
      max_value = std::max(raw_input, max_value);
    }
  }
  vosa::EigenImage<T> output{1, 1, 1};
  output(0, 0, 0) = max_value;
  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::ReduceMaxPlanewise<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::ReduceMaxPlanewise<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::REDUCE_MAX_PLANEWISE_MODE
vosa::ReduceMaxPlanewise<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::REDUCE_MAX_PLANEWISE_MODE
vosa::ReduceMaxPlanewise<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::REDUCE_MAX_PLANEWISE_MODE
vosa::ReduceMaxPlanewise<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::REDUCE_MAX_PLANEWISE_MODE
vosa::ReduceMaxPlanewise<int8_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::REDUCE_MAX_PLANEWISE_MODE
vosa::ReduceMaxPlanewise<int16_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::REDUCE_MAX_PLANEWISE_MODE
vosa::ReduceMaxPlanewise<int32_t>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::REDUCE_MAX_PLANEWISE_MODE
vosa::ReduceMaxPlanewise<float>::get_serialization_mode() const {
  return vosa_serialization::REDUCE_MAX_PLANEWISE_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::ReduceMaxPlanewise<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  vosa_serialization::REDUCE_MAX_PLANEWISET reduce_max_planewise;
  reduce_max_planewise.mode = get_serialization_mode();

  auto reduce_max_planewise_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  reduce_max_planewise_operator->op.Set(reduce_max_planewise);
  reduce_max_planewise_operator->name = op_name;
  reduce_max_planewise_operator->inputs = input_names;
  return reduce_max_planewise_operator;
}

template class vosa::ReduceMaxPlanewise<uint8_t>;

template class vosa::ReduceMaxPlanewise<uint16_t>;

template class vosa::ReduceMaxPlanewise<uint32_t>;

template class vosa::ReduceMaxPlanewise<int8_t>;

template class vosa::ReduceMaxPlanewise<int16_t>;

template class vosa::ReduceMaxPlanewise<int32_t>;

template class vosa::ReduceMaxPlanewise<float>;
