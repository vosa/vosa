/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <fstream>

#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/Dialect/Tosa/IR/TosaOps.h"

#include "mlir/IR/Verifier.h"
#include "mlir/InitAllPasses.h"
#include "mlir/Pass/Pass.h"
#include "mlir/Pass/PassManager.h"

#include "VosaSerialization.h"
#include "vosa/Conversion/Passes.h"
#include "vosa/Dialect/Vosa/IR/VosaOps.h"
#include "vosa/Dialect/Vosa/Transforms/Passes.h"

#include "vosa/Builder/mlir_builder.h"

#include "vosa/Compiler/vosa_compiler.h"

#include "vosa/IreeCompilerInterface/iree_compiler_interface.h"

static llvm::cl::OptionCategory
    CompilerCategory("Compiler Options",
                     "Options for controlling the compilation process.");

llvm::cl::OptionCategory &vosa::Compiler::get_compiler_category() {
  return CompilerCategory;
}

static llvm::cl::opt<std::string>
    RefModelFilename("rmfb", llvm::cl::desc("<Output Ref Model Flatbuffer>"),
                     llvm::cl::Optional,
                     llvm::cl::cat(vosa::Compiler::get_compiler_category()));

static llvm::cl::opt<std::string> VOSAInputFilename(
    "vosa-input", llvm::cl::desc("<Filename to write VOSA input IR>"),
    llvm::cl::Optional, llvm::cl::cat(vosa::Compiler::get_compiler_category()));

static llvm::cl::opt<std::string> IREEInputFilename(
    "iree-input", llvm::cl::desc("<Filename to write IREE input IR>"),
    llvm::cl::Optional, llvm::cl::cat(vosa::Compiler::get_compiler_category()));

bool vosa::Compiler::compileMain(int argc, char *argv[]) {
  //  auto compiler = VosaCompiler();
  return true;
}

bool compile_vosa(mlir::MLIRContext &context,
                  mlir::OwningOpRef<mlir::ModuleOp> &module,
                  std::string &output) {
  mlir::registerPassManagerCLOptions();

  context.getOrLoadDialect<mlir::vosa::VosaDialect>();
  context.getOrLoadDialect<mlir::func::FuncDialect>();
  context.getOrLoadDialect<mlir::tensor::TensorDialect>();
  context.getOrLoadDialect<mlir::tosa::TosaDialect>();

  mlir::OwningOpRef<mlir::ModuleOp> customOpsModule;

  auto verified = mlir::verify(module.get());
  if (verified.failed()) {
    llvm::errs() << "Failed to verify VOSA module\n";
    module->dump();
    return true;
  }

  mlir::registerAllPasses();
  mlir::vosa::registerConversionPasses();
  mlir::vosa::registerVosaOptPasses();
  mlir::vosa::registerVosaSerializationPasses();

  mlir::PassManager pass_manager(module.get()->getName());

  if (failed(applyPassManagerCLOptions(pass_manager))) {
    llvm::errs() << "Failed to apply pass manager CL options\n";
    return true;
  }

  mlir::OpPassManager &func_pass_manager =
      pass_manager.nest<mlir::func::FuncOp>();
  func_pass_manager.addPass(mlir::vosa::createVosaInferShapesPass());
  if (!RefModelFilename.empty()) {
    llvm::outs() << "Creating reference model flatbuffer at "
                 << RefModelFilename << "\n";
    func_pass_manager.addPass(
        mlir::vosa::createVosaSerializationPass(RefModelFilename));
  }
  func_pass_manager.addPass(mlir::createCSEPass());
  func_pass_manager.addPass(mlir::createCanonicalizerPass());
  func_pass_manager.addPass(mlir::vosa::createVosaSplitChannelPass());

  func_pass_manager.addPass(mlir::vosa::createVosaToTosa());
  func_pass_manager.addPass(mlir::createCSEPass());
  func_pass_manager.addPass(mlir::createCanonicalizerPass());

  auto run_result = pass_manager.run(module.get());

  if (run_result.failed()) {
    llvm::errs() << "Failed to verify compiled TOSA/Linalg module\n";
    module->dump();
    return true;
  }

  auto run_verified = mlir::verify(module.get());
  if (run_verified.failed())
    return true;
  llvm::raw_string_ostream sstream(output);
  module->print(sstream);
  return false;
}

bool vosa::Compiler::compile(mlir::MLIRContext &context,
                             mlir::OwningOpRef<mlir::ModuleOp> &module,
                             const std::string &pipeline_name,
                             const std::string &output_filename) {
  if (!VOSAInputFilename.empty()) {
    llvm::outs() << "Writing VOSA input out at " << VOSAInputFilename << "\n";
    std::string string_filename = VOSAInputFilename;
    ::std::ofstream vosa_input_file(string_filename);
    std::string vosa_input;
    llvm::raw_string_ostream sstream(vosa_input);
    module->print(sstream);
    vosa_input_file << vosa_input;
    vosa_input_file.close();
  }
  std::string iree_input;
  if (::compile_vosa(context, module, iree_input))
    return true;
  if (!IREEInputFilename.empty()) {
    llvm::outs() << "Writing IREE input out at " << IREEInputFilename << "\n";
    std::string string_filename = IREEInputFilename;
    ::std::ofstream iree_input_file(string_filename);
    iree_input_file << iree_input;
    iree_input_file.close();
  }
  if (vosa::Compiler::compile_iree(iree_input, "", output_filename))
    return true;
  return false;
}
