/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "non_max_suppression.h"

template <typename T>
vosa::ArrayBase::DType vosa::NonMaxSuppression<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::NonMaxSuppression<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::NON_MAX_SUPPRESSION_MODE
vosa::NonMaxSuppression<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::NON_MAX_SUPPRESSION_MODE_I_I_UINT8_UINT8;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::NonMaxSuppression<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  auto non_max_suppression_attribute =
      std::make_unique<vosa_serialization::NON_MAX_SUPPRESSION_ATTRIBUTET>();
  non_max_suppression_attribute->mask_dimension = {(uint32_t)mask.dimension(0),
                                                   (uint32_t)mask.dimension(1)};
  auto mask_size = mask.size();
  std::vector<uint8_t> value_byte_vector;
  auto byte_filter_ptr = (const uint8_t *)mask.data();
  value_byte_vector.insert(value_byte_vector.end(), byte_filter_ptr,
                           byte_filter_ptr + sizeof(T) * mask_size);
  non_max_suppression_attribute->mask_value = value_byte_vector;

  vosa_serialization::NON_MAX_SUPPRESSIONT non_max_suppression;
  non_max_suppression.mode = get_serialization_mode();
  non_max_suppression.attr = std::move(non_max_suppression_attribute);

  auto non_max_suppression_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  non_max_suppression_operator->op.Set(std::move(non_max_suppression));
  non_max_suppression_operator->name = op_name;
  non_max_suppression_operator->inputs = input_names;
  return non_max_suppression_operator;
}

template <typename T>
vosa::EigenImage<T>
vosa::NonMaxSuppression<T>::runme(const EigenImage<T> &input) const {

  uint32_t height = input.dimension(0);
  uint32_t width = input.dimension(1);
  uint32_t channels = input.dimension(2);

  uint32_t mask_height = mask.dimension(0);
  uint32_t mask_width = mask.dimension(1);

  uint32_t mh = mask_height / 2;
  uint32_t mw = mask_width / 2;

  EigenImage<T> output{height, width, channels};

  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < channels;
           channel_index++) {

        std::vector<T> above_left_pixels{};
        std::vector<T> below_right_pixels{};

        for (uint32_t mask_height_index = 0; mask_height_index < mask_height;
             mask_height_index++) {
          for (uint32_t mask_width_index = 0; mask_width_index < mask_width;
               mask_width_index++) {

            int32_t neighbour_pixel_height_index =
                (int32_t)height_index + mask_height_index - mh;
            int32_t neighbour_pixel_width_index =
                (int32_t)width_index + mask_width_index - mw;

            bool in_height = neighbour_pixel_height_index >= 0 &&
                             neighbour_pixel_height_index < (int32_t)height;
            bool in_width = neighbour_pixel_width_index >= 0 &&
                            neighbour_pixel_width_index < (int32_t)width;

            bool mask_true = mask(mask_height_index, mask_width_index);

            if (in_height && in_width && mask_true) {

              T neighbour_value =
                  input(neighbour_pixel_height_index,
                        neighbour_pixel_width_index, channel_index);

              bool below_right =
                  mask_height_index > mh || mask_width_index > mw;
              if (below_right) {
                below_right_pixels.push_back(neighbour_value);
              } else {
                above_left_pixels.push_back(neighbour_value);
              }
            }
          }
        }
        bool is_max = true;
        T my_value = input(height_index, width_index, channel_index);
        for (const T &above_left_value : above_left_pixels) {
          if (my_value < above_left_value)
            is_max = false;
        }
        for (const T &below_right_value : below_right_pixels) {
          if (my_value <= below_right_value)
            is_max = false;
        }

        if (is_max) {
          output(height_index, width_index, channel_index) = my_value;
        } else {
          output(height_index, width_index, channel_index) = 0;
        }
      }
    }
  }
  return output;
}

template class vosa::NonMaxSuppression<uint8_t>;