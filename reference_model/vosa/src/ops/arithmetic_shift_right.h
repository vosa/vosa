/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_CODE_ARITHMETIC_SHIFT_RIGHT_H
#define VOSA_CODE_ARITHMETIC_SHIFT_RIGHT_H

#include "vosa/op.h"

namespace vosa {

class ArithmeticShiftRightBase : public Op {
public:
  static std::shared_ptr<Op>
  parse_serialized(const vosa_serialization::VosaOperatorUnion &);
};

template <typename T>
class ArithmeticShiftRight : public ArithmeticShiftRightBase {
public:
  explicit ArithmeticShiftRight(uint32_t shift) : shift(shift) {}

  [[nodiscard]] std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &) const final;

  [[nodiscard]] vosa::ArrayBase::DType output_dtype() const final;

  vosa::EigenImage<T> runme(const vosa::EigenImage<T> &input) const;

  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

private:
  [[nodiscard]] vosa_serialization::ARITHMETIC_SHIFT_RIGHT_MODE
  get_serialization_mode() const;
  uint32_t shift;
};

} // namespace vosa

#endif // VOSA_CODE_ARITHMETIC_SHIFT_RIGHT_H
