/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "const_scalar.h"

template <typename T>
T vosa::ConstScalarBase::unpackValue(const std::vector<uint8_t> &raw_value) {

  auto uint8_ptr = new uint8_t[sizeof(T)];
  std::copy(raw_value.begin(), raw_value.end(), uint8_ptr);
  T output = *((T *)uint8_ptr);
  delete[] uint8_ptr;
  return output;
}

std::shared_ptr<vosa::Op> vosa::ConstScalarBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_const_scalar = vosa_operator_union.AsCONST_SCALAR();
  assert(serialized_const_scalar);
  auto mode = serialized_const_scalar->mode;
  auto serialized_attributes = &serialized_const_scalar->attr;
  std::vector<uint8_t> raw_value = serialized_attributes->get()->value;
  switch (mode) {
  case vosa_serialization::CONST_SCALAR_MODE_S_UINT8: {
    return std::make_shared<vosa::ConstScalar<uint8_t>>(
        unpackValue<uint8_t>(raw_value));
  }
  case vosa_serialization::CONST_SCALAR_MODE_S_UINT16: {
    return std::make_shared<vosa::ConstScalar<uint16_t>>(
        unpackValue<uint16_t>(raw_value));
  }
  case vosa_serialization::CONST_SCALAR_MODE_S_UINT32: {
    return std::make_shared<vosa::ConstScalar<uint32_t>>(
        unpackValue<uint32_t>(raw_value));
  }
  case vosa_serialization::CONST_SCALAR_MODE_S_INT8: {
    return std::make_shared<vosa::ConstScalar<int8_t>>(
        unpackValue<int8_t>(raw_value));
  }
  case vosa_serialization::CONST_SCALAR_MODE_S_INT16: {
    return std::make_shared<vosa::ConstScalar<int16_t>>(
        unpackValue<int16_t>(raw_value));
  }
  case vosa_serialization::CONST_SCALAR_MODE_S_INT32: {
    return std::make_shared<vosa::ConstScalar<int32_t>>(
        unpackValue<int32_t>(raw_value));
  }
  case vosa_serialization::CONST_SCALAR_MODE_S_FLOAT32: {
    return std::make_shared<vosa::ConstScalar<float>>(
        unpackValue<float>(raw_value));
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::ArrayBase::DType vosa::ConstScalar<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::ConstScalar<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &) const {
  vosa::EigenScalar<T> eigen_output;
  eigen_output.setConstant(value);
  return std::make_shared<vosa::ArrayBase>(eigen_output);
}

template <>
vosa_serialization::CONST_SCALAR_MODE
vosa::ConstScalar<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::CONST_SCALAR_MODE_S_UINT8;
}

template <>
vosa_serialization::CONST_SCALAR_MODE
vosa::ConstScalar<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::CONST_SCALAR_MODE_S_UINT16;
}

template <>
vosa_serialization::CONST_SCALAR_MODE
vosa::ConstScalar<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::CONST_SCALAR_MODE_S_UINT32;
}

template <>
vosa_serialization::CONST_SCALAR_MODE
vosa::ConstScalar<int8_t>::get_serialization_mode() const {
  return vosa_serialization::CONST_SCALAR_MODE_S_INT8;
}

template <>
vosa_serialization::CONST_SCALAR_MODE
vosa::ConstScalar<int16_t>::get_serialization_mode() const {
  return vosa_serialization::CONST_SCALAR_MODE_S_INT16;
}

template <>
vosa_serialization::CONST_SCALAR_MODE
vosa::ConstScalar<int32_t>::get_serialization_mode() const {
  return vosa_serialization::CONST_SCALAR_MODE_S_INT32;
}

template <>
vosa_serialization::CONST_SCALAR_MODE
vosa::ConstScalar<float>::get_serialization_mode() const {
  return vosa_serialization::CONST_SCALAR_MODE_S_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::ConstScalar<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {
  auto const_scalar_attribute =
      std::make_unique<vosa_serialization::CONST_SCALAR_ATTRIBUTET>();
  std::vector<uint8_t> value_byte_vector;
  value_byte_vector.insert(value_byte_vector.end(), (uint8_t *)&value,
                           ((uint8_t *)&value) + sizeof(T));
  const_scalar_attribute->value = value_byte_vector;

  vosa_serialization::CONST_SCALART const_scalar;
  const_scalar.mode = get_serialization_mode();
  const_scalar.attr = std::move(const_scalar_attribute);

  auto const_scalar_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  const_scalar_operator->op.Set(std::move(const_scalar));
  const_scalar_operator->name = op_name;
  const_scalar_operator->inputs = input_names;
  return const_scalar_operator;
}

template class vosa::ConstScalar<uint8_t>;

template class vosa::ConstScalar<uint16_t>;

template class vosa::ConstScalar<uint32_t>;

template class vosa::ConstScalar<int8_t>;

template class vosa::ConstScalar<int16_t>;

template class vosa::ConstScalar<int32_t>;

template class vosa::ConstScalar<float>;