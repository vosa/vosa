/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/reduce_sum_channelwise.h"
#include "test_bench.h"
#include "vosa/vosa.h"

using namespace std;
using namespace vosa;

TEST(test_reduce_sum_channelwise, test_reduce_sum_channelwise) {

  auto input = EigenImage<uint32_t>(2, 2, 2);
  input.setValues({{{0, 127}, {128, 255}}, {{127, 127}, {127, 127}}});

  auto expected = EigenPlane<uint32_t>(2, 2);
  expected.setValues({{127, 383}, {254, 254}});

  auto Op = ReduceSumChannelwise<uint32_t, uint32_t>();
  auto actual_image =
      Op.eval({make_shared<ArrayBase>(input)})->get_image<uint32_t>();
  auto actual = test::Packing<uint32_t>::extract_plane(actual_image, 0);

  bool result =
      test::TestBench<uint32_t, int32_t>::compareEigenPlanes(actual, expected);
  EXPECT_TRUE(result);
}