/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "broadcast_channelwise.h"
#include "../data_utils.h"

std::shared_ptr<vosa::Op> vosa::BroadcastChannelwiseBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {

  auto serialized_broadcast_plane =
      vosa_operator_union.AsBROADCAST_CHANNELWISE();
  assert(serialized_broadcast_plane);
  auto mode = serialized_broadcast_plane->mode;
  auto serialized_attributes = &serialized_broadcast_plane->attr;
  uint32_t channel_size = serialized_attributes->get()->size;
  switch (mode) {
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_BOOL_BOOL: {
    return std::make_shared<vosa::BroadcastChannelwise<bool>>(channel_size);
  }
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::BroadcastChannelwise<uint8_t>>(channel_size);
  }
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::BroadcastChannelwise<uint16_t>>(channel_size);
  }
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::BroadcastChannelwise<uint32_t>>(channel_size);
  }
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::BroadcastChannelwise<int8_t>>(channel_size);
  }
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::BroadcastChannelwise<int16_t>>(channel_size);
  }
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::BroadcastChannelwise<int32_t>>(channel_size);
  }
  case vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::BroadcastChannelwise<float>>(channel_size);
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::BroadcastChannelwise<T>::BroadcastChannelwise(uint32_t size) {
  this->size = vosa::utils::DataUtils<uint32_t>::scalarFromCPP(size);
}

template <typename T>
vosa::EigenImage<T>
vosa::BroadcastChannelwise<T>::runme(const EigenImage<T> &input) const {

  uint32_t input_height = input.dimension(0);
  uint32_t input_width = input.dimension(1);
  uint32_t input_channels = input.dimension(2);
  assert(input_channels == 1);

  auto output = vosa::EigenImage<T>{input_height, input_width, size()};

  for (uint32_t height_index = 0; height_index < input_height; height_index++) {
    for (uint32_t width_index = 0; width_index < input_width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < size();
           channel_index++) {
        output(height_index, width_index, channel_index) =
            input(height_index, width_index, 0);
      }
    }
  }
  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::BroadcastChannelwise<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::BroadcastChannelwise<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::BROADCAST_CHANNELWISE_MODE
vosa::BroadcastChannelwise<bool>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_BOOL_BOOL;
}

template <>
vosa_serialization::BROADCAST_CHANNELWISE_MODE
vosa::BroadcastChannelwise<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::BROADCAST_CHANNELWISE_MODE
vosa::BroadcastChannelwise<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::BROADCAST_CHANNELWISE_MODE
vosa::BroadcastChannelwise<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::BROADCAST_CHANNELWISE_MODE
vosa::BroadcastChannelwise<int8_t>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::BROADCAST_CHANNELWISE_MODE
vosa::BroadcastChannelwise<int16_t>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::BROADCAST_CHANNELWISE_MODE
vosa::BroadcastChannelwise<int32_t>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::BROADCAST_CHANNELWISE_MODE
vosa::BroadcastChannelwise<float>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_FLOAT32_FLOAT32;
}

template <>
vosa_serialization::BROADCAST_CHANNELWISE_MODE
vosa::BroadcastChannelwise<double>::get_serialization_mode() const {
  return vosa_serialization::BROADCAST_CHANNELWISE_MODE_I_I_FLOAT64_FLOAT64;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::BroadcastChannelwise<T>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {
  auto broadcast_plane_attribute =
      std::make_unique<vosa_serialization::BROADCAST_CHANNELWISE_ATTRIBUTET>();
  broadcast_plane_attribute->size = size();

  vosa_serialization::BROADCAST_CHANNELWISET broadcast_plane;
  broadcast_plane.mode = get_serialization_mode();
  broadcast_plane.attr = std::move(broadcast_plane_attribute);

  auto broadcast_plane_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  broadcast_plane_operator->op.Set(std::move(broadcast_plane));
  broadcast_plane_operator->name = op_name;
  broadcast_plane_operator->inputs = input_names;
  return broadcast_plane_operator;
}

template class vosa::BroadcastChannelwise<bool>;

template class vosa::BroadcastChannelwise<uint8_t>;

template class vosa::BroadcastChannelwise<uint16_t>;

template class vosa::BroadcastChannelwise<uint32_t>;

template class vosa::BroadcastChannelwise<int8_t>;

template class vosa::BroadcastChannelwise<int16_t>;

template class vosa::BroadcastChannelwise<int32_t>;

template class vosa::BroadcastChannelwise<float>;

template class vosa::BroadcastChannelwise<double>;
