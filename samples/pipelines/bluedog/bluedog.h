/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_BLUEDOG_H
#define VOSA_BLUEDOG_H

#include "vosa/api/graph_builder.h"
#include "vosa/api/ops.h"

namespace vosa::samples::bluedog {

struct CONSTANTS {
  static const inline std::vector<uint32_t> input_image_shape_hwc = {1080, 1920,
                                                                     1};
  static const inline std::vector<uint32_t> output_image_shape_hwc = {1080,
                                                                      1920, 3};
  static const inline std::vector<uint32_t> tosa_input_shape_hwc = {257, 257,
                                                                    3};
  static const inline std::vector<uint32_t> tosa_output_shape_hwc = {257, 257,
                                                                     21};
  static constexpr int class_index = 12;
};

std::vector<std::shared_ptr<vosa::api::Op>>
preprocessing(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<vosa::api::Op> &image);
std::shared_ptr<vosa::api::Op>
run_inference(vosa::api::GraphBuilder &graph_builder,
              const std::shared_ptr<vosa::api::Op> &image,
              const std::string &tosa_mlir_path);
std::shared_ptr<vosa::api::Op>
postprocessing(vosa::api::GraphBuilder &graph_builder,
               const std::shared_ptr<vosa::api::Op> &image_bgr,
               const std::shared_ptr<vosa::api::Op> &logits);
std::vector<std::shared_ptr<vosa::api::Op>>
make_test_graph(vosa::api::GraphBuilder &);
} // namespace vosa::samples::bluedog

#endif // VOSA_BLUEDOG_H
