/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_CODE_OPS_H
#define VOSA_CODE_OPS_H

#include "ops/abs.h"
#include "ops/abs_diff.h"
#include "ops/add.h"
#include "ops/arg_max_channelwise.h"
#include "ops/arg_max_planewise.h"
#include "ops/arg_min_channelwise.h"
#include "ops/arg_min_planewise.h"
#include "ops/arithmetic_shift_right.h"
#include "ops/atan2.h"
#include "ops/broadcast_channelwise.h"
#include "ops/broadcast_planewise.h"
#include "ops/cast.h"
#include "ops/channel_extract.h"
#include "ops/clamp.h"
#include "ops/concat.h"
#include "ops/const_image.h"
#include "ops/const_plane.h"
#include "ops/const_scalar.h"
#include "ops/conv.h"
#include "ops/crop.h"
#include "ops/custom_scalar.h"
#include "ops/decimate.h"
#include "ops/div.h"
#include "ops/equal.h"
#include "ops/expand.h"
#include "ops/gamma_correction.h"
#include "ops/greater.h"
#include "ops/greater_equal.h"
#include "ops/import_channel.h"
#include "ops/logical_shift_right.h"
#include "ops/max.h"
#include "ops/mesh_grid.h"
#include "ops/min.h"
#include "ops/mod.h"
#include "ops/mult.h"
#include "ops/non_linear.h"
#include "ops/not.h"
#include "ops/or.h"
#include "ops/pad.h"
#include "ops/piecewise_linear.h"
#include "ops/pointwise_matrix_multiply.h"
#include "ops/power.h"
#include "ops/print.h"
#include "ops/reduce_interp_channelwise.h"
#include "ops/reduce_max_channelwise.h"
#include "ops/reduce_max_planewise.h"
#include "ops/reduce_min_planewise.h"
#include "ops/reduce_sum_channelwise.h"
#include "ops/reduce_sum_planewise.h"
#include "ops/resize_bilinear.h"
#include "ops/resize_nearest_neighbour.h"
#include "ops/rotate90.h"
#include "ops/round.h"
#include "ops/sqrt.h"
#include "ops/sub.h"
#include "ops/tosa_graph.h"
#include "ops/where.h"

#endif // VOSA_CODE_OPS_H
