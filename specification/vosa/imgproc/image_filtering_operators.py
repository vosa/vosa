"""Operations to apply types of filtering to an Image.

Filtering operations may be categorized in the general sense by applying a linear or non-linear function to a moving window across an Image.

License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import enum
import typing
from vosa import types as types
from vosa.op import OPERATOR_MODES, Op, ExecutionPattern, InputPattern


class BilateralFilterExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image.",
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                shape=["H - fs[0]", "W - fs[1]", "C"],
                description="Output Image",
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            BILATERAL_FILTER.SIGMA_R_ATTRIBUTE: types.DataType(
                types.DType.FLOAT32,
                types.AType.SCALAR,
                shape=[],
                description="Sigma for the Gaussian kernel in the planewise directions.",
            ),
            BILATERAL_FILTER.SIGMA_C_ATTRIBUTE: types.DataType(
                types.DType.FLOAT32,
                types.AType.SCALAR,
                shape=[],
                description="Sigma for the Gaussian kernel in the channelwise direction.",
            ),
            BILATERAL_FILTER.FILTER_SIZE_ATTRIBUTE: types.DataType(
                types.DType.UINT32,
                types.AType.VECTOR,
                shape=[2],
                description="Size of the filter in the planewise directions.",
            ),
        }


class BILATERAL_FILTER_MODES(OPERATOR_MODES):
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class BILATERAL_FILTER(Op):
    """
    Apply a Bilateral filter with Gaussian smoothing kernels,
    [latexmath]
    ++++
    f_h = fs[0]/2, \\\\
    f_w = fs[1]/2, \\\\
    W_{y,x,c}[ y^*, x^*, c^*] = \\textrm{exp}\\left( -\\frac{(y - y^*)^2 + (x-x^*)^2)}{2\\sigma^2_r} - \\frac{ (input[y,x,c] - input[y^*, x^*, c^*])^2 }{2\\sigma^2_c} \\right), \\\\
    output[y,x,c] = \\frac{\sum_{n=0,l=-f_h, m=-f_w}^{C, f_h, f_w}  input[l, m, n] \\cdot W_{y,x,c}[n, l,m]}{\sum_{n=0,l=-f_h, m=-f_w}^{C, f_h, f_w}  W_{y,x,c}[l,m,n]}.
    ++++
    """

    SIGMA_R_ATTRIBUTE = "sigma_r"
    SIGMA_C_ATTRIBUTE = "sigma_c"
    FILTER_SIZE_ATTRIBUTE = "fs"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[BILATERAL_FILTER_MODES, ExecutionPattern]:
        return {
            BILATERAL_FILTER_MODES.I_I_FLOAT16_FLOAT16: BilateralFilterExecutionPattern(
                types.DType.FLOAT16
            ),
            BILATERAL_FILTER_MODES.I_I_FLOAT32_FLOAT32: BilateralFilterExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class ConvExecutionPattern(ExecutionPattern):
    def __init__(self, input_dtype, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    input_dtype,
                    types.AType.IMAGE,
                    description="Input Image.",
                    shape=["H", "W", "C"],
                )
            ),
            output_pattern=types.DataType(
                output_dtype,
                types.AType.IMAGE,
                description="Output Image.",
                shape=["H - FH + 1", "W - FW + 1", "C"],
            ),
            dynamic_attribute_pattern={
                CONV_2D.FILTER_ATTRIBUTE: types.DataType(
                    input_dtype,
                    types.AType.PLANE,
                    description="2D Array to be used as the filter in the convolution.",
                    shape=["FH", "FW"],
                )
            },
        )


class CONV_2D_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class CONV_2D(Op):
    """

    Convolves the input Image with the supplied Plane of 2D filter coefficients.

    [latexmath]
    ++++
    f_h = FH/2, \\\\
    f_w = FW/2, \\\\
    output[y,x,c] = \sum_{l=-f_h, m=-f_w}^{f_h, f_w} filter[l + f_h, m+f_w] \cdot input[y + l,x + m,c].
    ++++
    In all cases arithmetic is performed in the output DType. In Integer cases, should any operation overflow the expected behaviour is to wrap.

    In the case where the input Image consists of multiple channels, the filter is applied to each channel independently.
    """

    FILTER_ATTRIBUTE = "filter"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[CONV_2D_MODES, ConvExecutionPattern]:
        return {
            CONV_2D_MODES.I_I_UINT8_UINT8: ConvExecutionPattern(
                types.DType.UINT8, types.DType.UINT8
            ),
            CONV_2D_MODES.I_I_UINT16_UINT16: ConvExecutionPattern(
                types.DType.UINT16, types.DType.UINT16
            ),
            CONV_2D_MODES.I_I_UINT32_UINT32: ConvExecutionPattern(
                types.DType.UINT32, types.DType.UINT32
            ),
            CONV_2D_MODES.I_I_INT8_INT8: ConvExecutionPattern(
                types.DType.INT8, types.DType.INT8
            ),
            CONV_2D_MODES.I_I_INT16_INT16: ConvExecutionPattern(
                types.DType.INT16, types.DType.INT16
            ),
            CONV_2D_MODES.I_I_INT32_INT32: ConvExecutionPattern(
                types.DType.INT32, types.DType.INT32
            ),
            CONV_2D_MODES.I_I_FLOAT16_FLOAT16: ConvExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            CONV_2D_MODES.I_I_FLOAT32_FLOAT32: ConvExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class NonLinearFilterExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    description="Input Image.",
                    shape=["H", "W", "C"],
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                description="Output Image.",
                shape=["H", "W", "C"],
            ),
        )

    def static_attribute_pattern(self) -> typing.Dict[str, types.DataType]:
        return {
            NON_LINEAR_FILTER.MASK_ATTRIBUTE: types.DataType(
                types.DType.BOOL,
                types.AType.PLANE,
                shape=["MH", "MW"],
                description="2D Array to be used as the mask for the non linear function",
            )
        }


class NON_LINEAR_MODES(OPERATOR_MODES):
    I_I_BOOL_BOOL = enum.auto()
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class NON_LINEAR_FILTER(Op):
    _MASK_DESC = """
    For a given pixel the mask is applied to area around the pixel to create a set,
    """

    _VALID_PIXELS_DESC = """
    [latexmath]
    ++++
    m_h = MH/2 \\\\
    m_w = MW/2 \\\\
    valid\_pixels_{y,x,c} = \\bigcup_{l=-m_h, m=-m_w}^{m_h, m_w} input[y+l,x+m,c] \; \\textrm{where} \; mask[l+m_h,m+m_h].
    ++++
    In any case where this this definition would access an input value outside of the valid extent of the input Image, said value is undefined and is not added to the set.
    Following the construction of the set, 
    """

    _EXAMPLE_DESC = """
    For example, for an input Image, 
    [latexmath]
    ++++
    \\left[\\begin{array}{cccc}
    0 & 1 & 2 & 0 \\\\
    2 & 1 & 0 & 0 \\\\
    3 & 2 & 1 & 1 
    \\end{array}\\right],
    ++++
    and a mask,
    [latexmath]
    ++++
    \\left[\\begin{array}{ccc}
    0 & 1 & 0 \\\\
    1 & 1 & 1 \\\\
    0 & 1 & 0
    \\end{array}\\right],
    ++++
    the output is,
    """

    MASK_ATTRIBUTE = "mask"

    @classmethod
    def include_in_specification(cls):
        return False

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[NON_LINEAR_MODES, ExecutionPattern]:
        return {
            NON_LINEAR_MODES.I_I_BOOL_BOOL: NonLinearFilterExecutionPattern(
                types.DType.BOOL
            ),
            NON_LINEAR_MODES.I_I_UINT8_UINT8: NonLinearFilterExecutionPattern(
                types.DType.UINT8
            ),
            NON_LINEAR_MODES.I_I_UINT16_UINT16: NonLinearFilterExecutionPattern(
                types.DType.UINT16
            ),
            NON_LINEAR_MODES.I_I_UINT32_UINT32: NonLinearFilterExecutionPattern(
                types.DType.UINT32
            ),
            NON_LINEAR_MODES.I_I_INT8_INT8: NonLinearFilterExecutionPattern(
                types.DType.INT8
            ),
            NON_LINEAR_MODES.I_I_INT16_INT16: NonLinearFilterExecutionPattern(
                types.DType.INT16
            ),
            NON_LINEAR_MODES.I_I_INT32_INT32: NonLinearFilterExecutionPattern(
                types.DType.INT32
            ),
            NON_LINEAR_MODES.I_I_FLOAT16_FLOAT16: NonLinearFilterExecutionPattern(
                types.DType.FLOAT16
            ),
            NON_LINEAR_MODES.I_I_FLOAT32_FLOAT32: NonLinearFilterExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class DILATE(NON_LINEAR_FILTER):
    @classmethod
    def include_in_specification(cls):
        return True


DILATE.__doc__ = """
    Grow the non zero space of an Image.
    
    %s
    %s
    
    [latexmath]
    ++++
    output[y,x,c] = \max( valid\_pixels_{y,x,c}).
    ++++
    %s
    [latexmath]
    ++++
    \\left[\\begin{array}{cccc}
    2 & 2 & 2 & 2 \\\\
    3 & 2 & 2 & 1 \\\\
    3 & 3 & 2 & 1 
    \\end{array}\\right].
    ++++
    """ % (
    NON_LINEAR_FILTER._MASK_DESC,
    NON_LINEAR_FILTER._VALID_PIXELS_DESC,
    NON_LINEAR_FILTER._EXAMPLE_DESC,
)


class EdgeTracingExecutionPattern(ExecutionPattern):
    def __init__(self, dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image",
                )
            ),
            output_pattern=types.DataType(
                dtype,
                types.AType.IMAGE,
                shape=["H", "W", "C"],
                description="Output Image with all none edge pixels suppressed",
            ),
            dynamic_attribute_pattern={
                EDGE_TRACING.UPPER_BOUND_ATTRIBUTE: types.DataType(
                    dtype,
                    types.AType.SCALAR,
                    shape=[],
                    description="Upper bound from which to begin edge tracing.",
                ),
                EDGE_TRACING.LOWER_BOUND_ATTRIBUTE: types.DataType(
                    dtype,
                    types.AType.SCALAR,
                    shape=[],
                    description="Lower bound at which to stop edge tracing.",
                ),
            },
        )


class EDGE_TRACING_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT8 = enum.auto()
    I_I_UINT16_UINT16 = enum.auto()
    I_I_UINT32_UINT32 = enum.auto()
    I_I_INT8_INT8 = enum.auto()
    I_I_INT16_INT16 = enum.auto()
    I_I_INT32_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class EDGE_TRACING(Op):
    """
    Suppress all non-edge pixels in an Image (set them to zero).

    All pixels having a value greater than `u` are automatically added to the edge set.

    The edge set is then recursively grown by adding pixels to the set, which neighbour an existing edge pixel, and have a greater value than `l`.

    Once the set becomes static all edge pixels have been located. Edge pixels retain their value in the original Image.

    For an input Image,
    [latexmath]
    ++++
    \\left[\\begin{array}{cccc}
    1 & 3 & 1 & 0 \\\\
    1 & 1 & 2 & 0 \\\\
    2 & 0 & 4 & 3
    \\end{array} \\right],
    ++++
    with `u=3` and `l=1`,
    the initial edge set is,
    [latexmath]
    ++++
    \\left[\\begin{array}{cccc}
    0 & 0 & 0 & 0 \\\\
    0 & 0 & 0 & 0 \\\\
    0 & 0 & 1 & 0
    \\end{array} \\right].
    ++++
    The iterations to convergence are,
    [latexmath]
    ++++
    \\left[\\begin{array}{cccc}
    0 & 0 & 0 & 0 \\\\
    0 & 0 & 1 & 0 \\\\
    0 & 0 & 1 & 1
    \\end{array}\\right],
    ++++
    and
    [latexmath]
    ++++
    \\left[\\begin{array}{cccc}
    0 & 1 & 0 & 0 \\\\
    0 & 0 & 1 & 0 \\\\
    0 & 0 & 1 & 1
    \\end{array}\\right].
    ++++
    At this point no further pixels will be added to the edge set, the output is therefore,
    [latexmath]
    ++++
    \\left[\\begin{array}{cccc}
    0 & 3 & 0 & 0 \\\\
    0 & 0 & 2 & 0 \\\\
    0 & 0 & 4 & 3
    \\end{array} \\right].
    ++++
    """

    UPPER_BOUND_ATTRIBUTE = "u"
    LOWER_BOUND_ATTRIBUTE = "l"

    @classmethod
    def get_pattern_dict(cls) -> typing.Dict[EDGE_TRACING_MODES, ExecutionPattern]:
        return {
            EDGE_TRACING_MODES.I_I_UINT8_UINT8: EdgeTracingExecutionPattern(
                types.DType.UINT8
            ),
            EDGE_TRACING_MODES.I_I_UINT16_UINT16: EdgeTracingExecutionPattern(
                types.DType.UINT16
            ),
            EDGE_TRACING_MODES.I_I_UINT32_UINT32: EdgeTracingExecutionPattern(
                types.DType.UINT32
            ),
            EDGE_TRACING_MODES.I_I_INT8_INT8: EdgeTracingExecutionPattern(
                types.DType.INT8
            ),
            EDGE_TRACING_MODES.I_I_INT16_INT16: EdgeTracingExecutionPattern(
                types.DType.INT16
            ),
            EDGE_TRACING_MODES.I_I_INT32_INT32: EdgeTracingExecutionPattern(
                types.DType.INT32
            ),
            EDGE_TRACING_MODES.I_I_FLOAT16_FLOAT16: EdgeTracingExecutionPattern(
                types.DType.FLOAT16
            ),
            EDGE_TRACING_MODES.I_I_FLOAT32_FLOAT32: EdgeTracingExecutionPattern(
                types.DType.FLOAT32
            ),
        }


class ERODE(NON_LINEAR_FILTER):
    @classmethod
    def include_in_specification(cls):
        return True


ERODE.__doc__ = """
    Shrink the non zero space of an Image.

    %s
    %s

    [latexmath]
    ++++
    output[y,x,c] = \min( valid\_pixels_{y,x,c}).
    ++++
    %s
    [latexmath]
    ++++
    \\left[\\begin{array}{cccc}
    0 & 1 & 0 & 0 \\\\
    0 & 0 & 0 & 0 \\\\
    2 & 1 & 0 & 0 
    \\end{array}\\right].
    ++++
    """ % (
    NON_LINEAR_FILTER._MASK_DESC,
    NON_LINEAR_FILTER._VALID_PIXELS_DESC,
    NON_LINEAR_FILTER._EXAMPLE_DESC,
)


class IntegralImageExecutionPattern(ExecutionPattern):
    def __init__(self, input_dtype, output_dtype):
        super().__init__(
            input_pattern=InputPattern(
                types.DataType(
                    input_dtype,
                    types.AType.IMAGE,
                    shape=["H", "W", "C"],
                    description="Input Image.",
                )
            ),
            output_pattern=types.DataType(
                output_dtype,
                types.AType.IMAGE,
                shape=["H", "W", "C"],
                description="Integral Image.",
            ),
        )


class INTEGRAL_IMAGE_MODES(OPERATOR_MODES):
    I_I_UINT8_UINT32 = enum.auto()
    I_I_INT16_INT32 = enum.auto()
    I_I_FLOAT16_FLOAT16 = enum.auto()
    I_I_FLOAT32_FLOAT32 = enum.auto()


class INTEGRAL_IMAGE(Op):
    """
    Compute the integral of an Image,
    [latexmath]
    ++++
    output[y,x,c] = \\sum_{m,l=0}^{l=y,m=x} input[l,m,c].
    ++++
    In integer modes, should the summation overflow the output type, the summation should wrap.
    """

    @classmethod
    def get_pattern_dict(
        cls,
    ) -> typing.Dict[INTEGRAL_IMAGE_MODES, IntegralImageExecutionPattern]:
        return {
            INTEGRAL_IMAGE_MODES.I_I_UINT8_UINT32: IntegralImageExecutionPattern(
                types.DType.UINT8, types.DType.UINT32
            ),
            INTEGRAL_IMAGE_MODES.I_I_INT16_INT32: IntegralImageExecutionPattern(
                types.DType.INT16, types.DType.INT32
            ),
            INTEGRAL_IMAGE_MODES.I_I_FLOAT16_FLOAT16: IntegralImageExecutionPattern(
                types.DType.FLOAT16, types.DType.FLOAT16
            ),
            INTEGRAL_IMAGE_MODES.I_I_FLOAT32_FLOAT32: IntegralImageExecutionPattern(
                types.DType.FLOAT32, types.DType.FLOAT32
            ),
        }


class MEDIAN_FILTER(NON_LINEAR_FILTER):
    @classmethod
    def include_in_specification(cls):
        return True


MEDIAN_FILTER.__doc__ = """
    Find the median pixel value over a window in the input Image.

    %s
    %s

    [latexmath]
    ++++
    output[y,x,c] = \\textrm{median}( valid\_pixels_{y,x,c}).
    ++++
    %s
    [latexmath]
    ++++
    \\left[\\begin{array}{cccc}
    1 & 1 & 0 & 0 \\\\
    1 & 1 & 1 & 0 \\\\
    2 & 1 & 1 & 1 
    \\end{array}\\right].
    ++++
    """ % (
    NON_LINEAR_FILTER._MASK_DESC,
    NON_LINEAR_FILTER._VALID_PIXELS_DESC,
    NON_LINEAR_FILTER._EXAMPLE_DESC,
)


class NON_MAX_SUPPRESSION(NON_LINEAR_FILTER):
    @classmethod
    def include_in_specification(cls):
        return True


NON_MAX_SUPPRESSION.__doc__ = """
    Suppress any pixel values in an image which are not local maxima.
    
    Local maxima are defined as any pixels which are greater or equal than any masked pixel above or to the left, and greater than any other adjacent pixel

    %s
    
    [latexmath]
    ++++
    upper\_left\_pixels_{y,x,c} = \\bigcup_{l=-m_h, m=-m_w}^{m_h, m_w} input[y+l,x+m,c] \; \\textrm{where} \; mask[l+m_h,m+m_h] \land (l \leq y \land m \leq x)
    ++++
    [latexmath]
    ++++
    lower\_right\_pixels_{y,x,c} = \\bigcup_{l=-m_h, m=-m_w}^{m_h, m_w} input[y+l,x+m,c] \; \\textrm{where} \; mask[l+m_h,m+m_h] \land (l \gt y \lor m \gt x)]
    ++++
    In any case where this these definitions would access an input value outside of the valid extent of the input Image, said value is undefined and is not added to the set.
    [latexmath]
    ++++
    output[y,x,c] = \\left\{  \\begin{array}{ll} input[y,x,c] & (input[y,x,c] \geq upper\_left\_pixels_{y,x,c}) \land (input[y,x,c] \gt lower\_right\_pixels_{y,x,c}) \\\ 0 & \\textrm{otherwise} \\end{array} \\right.
    ++++
    %s
    [latexmath]
    ++++
    \\left[\\begin{array}{cccc}
    0 & 0 & 2 & 0 \\\\
    0 & 0 & 0 & 0 \\\\
    3 & 0 & 0 & 1 
    \\end{array}\\right].
    ++++
    """ % (
    NON_LINEAR_FILTER._MASK_DESC,
    NON_LINEAR_FILTER._EXAMPLE_DESC,
)
