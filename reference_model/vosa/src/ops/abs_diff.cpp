/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "abs_diff.h"

std::shared_ptr<vosa::Op> vosa::AbsDiffBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_abs_diff = vosa_operator_union.AsABS_DIFF();
  assert(serialized_abs_diff);
  auto mode = serialized_abs_diff->mode;
  switch (mode) {
  case vosa_serialization::ABS_DIFF_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::AbsDiff<uint8_t, uint8_t>>();
  }
  case vosa_serialization::ABS_DIFF_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::AbsDiff<uint16_t, uint16_t>>();
  }
  case vosa_serialization::ABS_DIFF_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::AbsDiff<uint32_t, uint32_t>>();
  }
  case vosa_serialization::ABS_DIFF_MODE_I_I_INT8_UINT8: {
    return std::make_shared<vosa::AbsDiff<int8_t, uint8_t>>();
  }
  case vosa_serialization::ABS_DIFF_MODE_I_I_INT16_UINT16: {
    return std::make_shared<vosa::AbsDiff<int16_t, uint16_t>>();
  }
  case vosa_serialization::ABS_DIFF_MODE_I_I_INT32_UINT32: {
    return std::make_shared<vosa::AbsDiff<int32_t, uint32_t>>();
  }
  case vosa_serialization::ABS_DIFF_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::AbsDiff<float, float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename inputT, typename outputT>
vosa::EigenImage<outputT> vosa::AbsDiff<inputT, outputT>::runme(
    const vosa::EigenImage<inputT> &input_1,
    const vosa::EigenImage<inputT> &input_2) const {

  EigenImage<outputT> output = vosa::EigenImage<outputT>{
      input_1.dimension(0), input_1.dimension(1), input_1.dimension(2)};

  for (uint32_t h = 0; h < output.dimension(0); ++h) {
    for (uint32_t w = 0; w < output.dimension(1); ++w) {
      for (uint32_t ch = 0; ch < output.dimension(2); ++ch) {
        if (input_1(h, w, ch) > input_2(h, w, ch)) {
          output(h, w, ch) = (outputT)(input_1(h, w, ch) - input_2(h, w, ch));
        } else {
          output(h, w, ch) = (outputT)(input_2(h, w, ch) - input_1(h, w, ch));
        }
      }
    }
  }

  return output;
}

template <typename inputT, typename outputT>
vosa::ArrayBase::DType vosa::AbsDiff<inputT, outputT>::output_dtype() const {
  return output_dtype_helper<outputT>();
}

template <typename inputT, typename outputT>
std::shared_ptr<vosa::ArrayBase> vosa::AbsDiff<inputT, outputT>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<inputT> &input_1 = inputs[0]->template get_image<inputT>();
  const EigenImage<inputT> &input_2 = inputs[1]->template get_image<inputT>();
  auto typed_output = this->runme(input_1, input_2);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::ABS_DIFF_MODE
vosa::AbsDiff<uint8_t, uint8_t>::get_serialization_mode() const {
  return vosa_serialization::ABS_DIFF_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::ABS_DIFF_MODE
vosa::AbsDiff<uint16_t, uint16_t>::get_serialization_mode() const {
  return vosa_serialization::ABS_DIFF_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::ABS_DIFF_MODE
vosa::AbsDiff<uint32_t, uint32_t>::get_serialization_mode() const {
  return vosa_serialization::ABS_DIFF_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::ABS_DIFF_MODE
vosa::AbsDiff<int8_t, uint8_t>::get_serialization_mode() const {
  return vosa_serialization::ABS_DIFF_MODE_I_I_INT8_UINT8;
}

template <>
vosa_serialization::ABS_DIFF_MODE
vosa::AbsDiff<int16_t, uint16_t>::get_serialization_mode() const {
  return vosa_serialization::ABS_DIFF_MODE_I_I_INT16_UINT16;
}

template <>
vosa_serialization::ABS_DIFF_MODE
vosa::AbsDiff<int32_t, uint32_t>::get_serialization_mode() const {
  return vosa_serialization::ABS_DIFF_MODE_I_I_INT32_UINT32;
}

template <>
vosa_serialization::ABS_DIFF_MODE
vosa::AbsDiff<float, float>::get_serialization_mode() const {
  return vosa_serialization::ABS_DIFF_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename inputT, typename outputT>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::AbsDiff<inputT, outputT>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {
  vosa_serialization::ABS_DIFFT absdiff;
  absdiff.mode = get_serialization_mode();

  auto mult_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  mult_operator->op.Set(absdiff);
  mult_operator->name = op_name;
  mult_operator->inputs = input_names;
  return mult_operator;
}

template class vosa::AbsDiff<uint8_t, uint8_t>;

template class vosa::AbsDiff<uint16_t, uint16_t>;

template class vosa::AbsDiff<uint32_t, uint32_t>;

template class vosa::AbsDiff<int8_t, uint8_t>;

template class vosa::AbsDiff<int16_t, uint16_t>;

template class vosa::AbsDiff<int32_t, uint32_t>;

template class vosa::AbsDiff<float, float>;