/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/abs_diff.h"
#include "test_bench.h"
#include "vosa/vosa.h"

TEST(test_binary_operators, test_absdiff_uint8) {
  vosa::EigenImage<uint8_t> input1{2, 2, 1};
  input1.setValues({{{255}, {255}}, {{0}, {0}}});

  vosa::EigenImage<uint8_t> input2{2, 2, 1};
  input2.setValues({{{0}, {0}}, {{255}, {255}}});

  vosa::EigenImage<uint8_t> output =
      vosa::AbsDiff<uint8_t, uint8_t>().runme(input1, input2);

  vosa::EigenImage<uint8_t> expected_output{2, 2, 1};
  expected_output.setValues({{{255}, {255}}, {{255}, {255}}});

  bool compare = vosa::test::TestBench<uint8_t, uint8_t>::compareEigenImages(
      output, expected_output);

  EXPECT_TRUE(compare);
}

TEST(test_binary_operators, test_absdiff_int8) {
  vosa::EigenImage<int8_t> input1{2, 2, 1};
  input1.setValues({{{127}, {127}}, {{-128}, {-128}}});

  vosa::EigenImage<int8_t> input2{2, 2, 1};
  input2.setValues({{{-128}, {-128}}, {{127}, {127}}});

  vosa::EigenImage<uint8_t> output =
      vosa::AbsDiff<int8_t, uint8_t>().runme(input1, input2);

  vosa::EigenImage<uint8_t> expected_output{2, 2, 1};
  expected_output.setValues({{{255}, {255}}, {{255}, {255}}});

  bool compare = vosa::test::TestBench<uint8_t, uint8_t>::compareEigenImages(
      output, expected_output);

  EXPECT_TRUE(compare);
}

TEST(test_binary_operators, test_absdiff_int16) {
  vosa::EigenImage<int16_t> input1{2, 2, 1};
  input1.setValues({{{32767}, {32767}}, {{-32768}, {-32768}}});

  vosa::EigenImage<int16_t> input2{2, 2, 1};
  input2.setValues({{{-32768}, {-32768}}, {{32767}, {32767}}});

  vosa::EigenImage<uint16_t> output =
      vosa::AbsDiff<int16_t, uint16_t>().runme(input1, input2);

  vosa::EigenImage<uint16_t> expected_output{2, 2, 1};
  expected_output.setValues({{{65535}, {65535}}, {{65535}, {65535}}});

  bool compare = vosa::test::TestBench<uint16_t, uint16_t>::compareEigenImages(
      output, expected_output);

  EXPECT_TRUE(compare);
}
