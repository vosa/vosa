/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vosa/Dialect/Vosa/IR/VosaDialect.h"
#include "vosa/Dialect/Vosa/Transforms/Passes.h"

#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/LLVMIR/LLVMDialect.h"
#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/IR/Builders.h"

#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/LineIterator.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/Path.h"
#include "llvm/Support/raw_ostream.h"
#define DEBUG_TYPE "vosa-op-profile"

#include "PassDetail.h"

using namespace mlir;
using namespace mlir::vosa;

namespace {

struct OpInfo {
  std::string opName;
  mlir::Type outType;
  std::string srcLoc;
  std::string csvVals;
};

std::string fileLineLocToString(FileLineColLoc fileLoc) {
  std::string locStr;
  llvm::raw_string_ostream locStrOS(locStr);
  locStrOS << llvm::sys::path::filename(fileLoc.getFilename()) << ":"
           << fileLoc.getLine();
  return locStr;
}

struct OpProfilePass : public mlir::vosa::VosaOpProfileBase<OpProfilePass> {
  explicit OpProfilePass(raw_ostream &os) : os(os) {}

  explicit OpProfilePass(raw_ostream &os, llvm::StringRef profileFilename)
      : os(os) {
    this->profileFilename = profileFilename.str();
  }

  void runOnOperation() override {
    auto func = getOperation();
    llvm::StringMap<std::string> srcLocOps;
    llvm::StringMap<OpInfo> opInfo;

    // Walk over all operations in the function - map the file/lineNo to the
    // operator name and store the operator name and type
    func->walk([&](mlir::Operation *op) {
      if (op->getDialect()->getNamespace() !=
          VosaDialect::getDialectNamespace())
        return;
      if (auto nameLoc = op->getLoc().dyn_cast<NameLoc>()) {
        if (auto fileLoc = nameLoc.getChildLoc().dyn_cast<FileLineColLoc>()) {
          // have op name, flatbuffer file, index
          auto opName = nameLoc.getName().getValue();
          auto locStr = fileLineLocToString(fileLoc);
          srcLocOps[locStr] = opName.str();
          opInfo[opName.str()] =
              OpInfo{op->getName().getStringRef().str(),
                     op->getOpResult(0).getType(), locStr, std::string()};
        }
      } else if (auto fileLoc = op->getLoc().dyn_cast<FileLineColLoc>()) {
        // only have filename (likely MLIR) and line number - use file/line as
        // operator name
        auto locStr = fileLineLocToString(fileLoc);
        srcLocOps[locStr] = locStr;
        opInfo[locStr] =
            OpInfo{op->getName().getStringRef().str(),
                   op->getOpResult(0).getType(), locStr, std::string()};
      }
    });

    // parse the CSV input and map the file locs and counters to operators
    if (profileFilename.empty()) {
      llvm::errs() << "profile not set\n";
      return signalPassFailure();
    }
    llvm::ErrorOr<std::unique_ptr<llvm::MemoryBuffer>> FileOrErr =
        llvm::MemoryBuffer::getFile(profileFilename);
    if (std::error_code ec = FileOrErr.getError()) {
      llvm::errs() << "Could not open input file: " << ec.message() << "\n";
      return signalPassFailure();
    }
    auto &Buf = FileOrErr.get();
    for (llvm::line_iterator I(*Buf, true), E; I != E; ++I) {
      auto kv = I->split(',');
      auto srcLoc = kv.first.trim();
      auto csvVals = kv.second.trim();
      auto opName = srcLocOps.find(srcLoc);
      std::string op;
      if (opName != srcLocOps.end()) {
        op = opName->getValue();
      } else {
        op = std::string("<<unknown>>@") + srcLoc.str();
        opInfo[op].srcLoc = srcLoc.str();
      }
      opInfo[op].csvVals = csvVals.str();
    }

    // write the operator -> stats CSV
    os << "\"Operator Name\",\"Source Loc\",\"Operator\",\"Count...\"\n";
    for (const auto &kv : opInfo) {
      const auto &v = kv.getValue();
      os << kv.getKey() << "," << v.srcLoc << "," << v.opName << ",";
      v.outType.print(os);
      os << "," << v.csvVals << "\n";
    }
  }

private:
  raw_ostream &os;
};
} // namespace

std::unique_ptr<mlir::Pass>
mlir::vosa::createVosaOpProfilePass(raw_ostream &os, llvm::StringRef profile) {
  return std::make_unique<OpProfilePass>(os, profile);
}
std::unique_ptr<mlir::Pass>
mlir::vosa::createVosaOpProfilePass(raw_ostream &os) {
  return std::make_unique<OpProfilePass>(os);
}
