// RUN: vosa-opt -vosa-data-format=chw -split-input-file -verify-diagnostics %s | FileCheck %s

// CHECK-LABEL: func.func @conv_2d()
module {
    func.func @conv_2d() {
        // integer types are preserved
        %t_si8 = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // CHECK: %{{.*}} = vosa.conv_2d %{{.*}} {filter = {{.*}}} : tensor<3x100x200xsi8> -> tensor<3x98x198xsi8>
        %res_si8 = vosa.conv_2d %t_si8 { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi8> } : tensor<3x100x200xsi8> -> tensor<3x98x198xsi8>
    
        %t_si16 = vosa.constant dense<42> : tensor<3x100x200xsi16>
        // CHECK: %{{.*}} = vosa.conv_2d %{{.*}} {filter = {{.*}}} : tensor<3x100x200xsi16> -> tensor<3x98x198xsi16>
        %res_si16 = vosa.conv_2d %t_si16 { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi16> } : tensor<3x100x200xsi16> -> tensor<3x98x198xsi16>
    
        %t_si32 = vosa.constant dense<42> : tensor<3x100x200xsi32>
        // CHECK: %{{.*}} = vosa.conv_2d %{{.*}} {filter = {{.*}}} : tensor<3x100x200xsi32> -> tensor<3x98x198xsi32>
        %res_si32 = vosa.conv_2d %t_si32 { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi32> } : tensor<3x100x200xsi32> -> tensor<3x98x198xsi32>
    
        // FP 16 is not upgraded
        %t_f16 = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // CHECK: %{{.*}} = vosa.conv_2d %{{.*}} {filter = {{.*}}} : tensor<3x100x200xf16> -> tensor<3x98x198xf16>
        %res_f16 = vosa.conv_2d %t_f16 { filter = dense<[[0.125, 0.25, 0.125], [0.25, 1.0, 0.25], [0.125, 0.25, 0.125]]> : tensor<3x3xf16> } : tensor<3x100x200xf16> -> tensor<3x98x198xf16>
    
        %t_f32 = vosa.constant dense<42.0> : tensor<3x100x200xf32>
        // CHECK: %{{.*}} = vosa.conv_2d %{{.*}} {filter = {{.*}}} : tensor<3x100x200xf32> -> tensor<3x98x198xf32>
        %res_f32 = vosa.conv_2d %t_f32 { filter = dense<[[0.125, 0.25, 0.125], [0.25, 1.0, 0.25], [0.125, 0.25, 0.125]]> : tensor<3x3xf32> } : tensor<3x100x200xf32> -> tensor<3x98x198xf32>
    
        return
    }
}

// CHECK-LABEL: func.func @conv_2d_any(%{{.*}})
module {
    func.func @conv_2d_any(%t : tensor<?x?x?xsi8>) {
        // CHECK: %{{.*}} = vosa.conv_2d %{{.*}}  {filter = {{.*}}} : tensor<?x?x?xsi8> -> tensor<?x?x?xsi8>
        %res0 = vosa.conv_2d %t { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi8> } : tensor<?x?x?xsi8> -> tensor<?x?x?xsi8>
        return
    }
}

// -----

module {
    func.func @conv_2d_filter_type_mismatch_f16() {
        %t = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // expected-error @below {{'vosa.conv_2d' op filter and output type don't match}}
        %res0 = vosa.conv_2d %t { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi32> } : tensor<3x100x200xf16> -> tensor<3x98x198xf32>
        return
    }
}

// -----

module {
    func.func @conv_2d_output_type_mismatch_si8() {
        %t = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.conv_2d' op filter and output type don't match}}
        %res0 = vosa.conv_2d %t { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi8> } : tensor<3x100x200xsi8> -> tensor<3x98x198xsi16>
        return
    }
}

// -----

module {
    func.func @conv_2d_output_type_mismatch_f16() {
        %t = vosa.constant dense<42.0> : tensor<3x100x200xf16>
        // expected-error @below {{'vosa.conv_2d' op filter and output type don't match}}
        %res0 = vosa.conv_2d %t { filter = dense<[[0.125, 0.25, 0.125], [0.25, 1.0, 0.25], [0.125, 0.25, 0.125]]> : tensor<3x3xf16> } : tensor<3x100x200xf16> -> tensor<3x98x198xf32>
        return
    }
}

// -----

module {
    func.func @conv_2d_output_size_mismatch_si8() {
        %t = vosa.constant dense<42> : tensor<3x100x200xsi8>
        // expected-error @below {{'vosa.conv_2d' op invalid output shape [3, 97, 198], expected [3, 98, 198]}}
        %res0 = vosa.conv_2d %t { filter = dense<[[1, 2, 1], [2, 4, 2], [1, 2, 1]]> : tensor<3x3xsi8> } : tensor<3x100x200xsi8> -> tensor<3x97x198xsi8>
        return
    }
}

