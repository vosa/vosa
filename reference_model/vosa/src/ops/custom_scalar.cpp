/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "custom_scalar.h"
#include "custom.h"

std::shared_ptr<vosa::Op> vosa::CustomScalarBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_custom_scalar = vosa_operator_union.AsCUSTOM_SCALAR();
  assert(serialized_custom_scalar);
  auto op_name = serialized_custom_scalar->custom_op_name;

  op_creation_function_t op_creation_function = MasterRegistry::get_op(op_name);
  auto raw_op = op_creation_function();
  auto custom_scalar_op =
      std::static_pointer_cast<vosa::CustomScalarBase>(raw_op);
  custom_scalar_op->set_op_name(op_name);
  return raw_op;
}

std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::CustomScalarBase::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  vosa_serialization::CUSTOM_SCALART custom_scalar;
  custom_scalar.custom_op_name = custom_op_name;

  auto logical_shift_right_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  logical_shift_right_operator->op.Set(custom_scalar);
  logical_shift_right_operator->name = op_name;
  logical_shift_right_operator->inputs = input_names;
  return logical_shift_right_operator;
}