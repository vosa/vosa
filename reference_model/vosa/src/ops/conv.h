/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_CODE_CONV_H
#define VOSA_CODE_CONV_H

#include "vosa/op.h"

namespace vosa {

class ConvBase : public Op {
public:
  static std::shared_ptr<Op>
  parse_serialized(const vosa_serialization::VosaOperatorUnion &);

private:
  template <typename T>
  static vosa::EigenPlane<T> unpackValue(const std::vector<uint32_t> &dimension,
                                         const std::vector<uint8_t> &raw_value);
};

template <typename inputT, typename outputT>
class Conv : public ConvBase {
public:
  explicit Conv(const EigenPlane<inputT> &filter) : filter(filter) {}

  EigenImage<outputT> runme(const EigenImage<inputT> &) const;

  [[nodiscard]] ArrayBase::DType output_dtype() const final;

  [[nodiscard]] std::shared_ptr<ArrayBase>
  eval(const std::vector<std::shared_ptr<ArrayBase>> &) const final;

  [[nodiscard]] std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
  serialize(const std::string &op_name,
            const std::vector<std::string> &input_names) const final;

private:
  [[nodiscard]] vosa_serialization::CONV_2D_MODE get_serialization_mode() const;
  EigenPlane<inputT> filter;
};

} // namespace vosa

#endif // VOSA_CODE_CONV_H
