"""
License:
   SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
   SPDX-License-Identifier: Apache-2.0

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import copy
from tabulate import tabulate

import graphviz

import vosa
import vosa.imgproc.elementwise_binary_operators
import vosa.op


class _GraphStack(object):
    __global = None
    __stack = None

    def get_current(self):
        if self.__stack:
            return self.__stack[-1]
        elif self.__global:
            return self.__global
        else:
            self.__global = Graph()
            return self.__global

    def put(self, graph):
        if not self.__stack:
            self.__stack = [graph]
        else:
            self.__stack.append(graph)

    def pop(self):
        self.__stack.pop()


_graph_stack = _GraphStack()


class _NameStack:
    def __init__(self):
        self.__stack = None

    def put(self, name: str):
        if not self.__stack:
            self.__stack = [name]
        else:
            self.__stack.append(name)

    def get_name(self, name):
        if not self.__stack:
            candidate_name = name
        else:
            candidate_name = "/".join(self.__stack + [name])

        return candidate_name

    def pop(self):
        self.__stack.pop()


class NameScope:
    def __init__(self, name: str):
        self.__name = name

    def __enter__(self):
        graph = _graph_stack.get_current()
        graph.add_name(self.__name)

    def __exit__(self, exc_type, exc_val, exc_tb):
        graph = _graph_stack.get_current()
        graph.remove_name()


class Graph:
    def __init__(self, allow_failed_validation=True):
        self.__ops = {}
        self.__op_name_stack = _NameStack()
        self.__data_name_stack = _NameStack()
        self.__allow_failed_validation = allow_failed_validation

    @staticmethod
    def __create_op_from_serialized(op, inputs, name):
        current_op_dict = _graph_stack.get_current().ops()
        input_ops = [current_op_dict[inp] for inp in inputs]
        import vosa_serialization.DTYPE
        import vosa_serialization.INPUT
        import vosa_serialization.ABS
        import vosa_serialization.ABS_MODE
        import vosa_serialization.ABS_DIFF
        import vosa_serialization.ABS_DIFF_MODE
        import vosa_serialization.ADD
        import vosa_serialization.ADD_MODE
        import vosa_serialization.ARITHMETIC_SHIFT_RIGHT
        import vosa_serialization.ARITHMETIC_SHIFT_RIGHT_MODE
        import vosa_serialization.ARG_MIN_CHANNELWISE
        import vosa_serialization.ARG_MIN_CHANNELWISE_MODE
        import vosa_serialization.BROADCAST_CHANNELWISE
        import vosa_serialization.BROADCAST_CHANNELWISE_MODE
        import vosa_serialization.BROADCAST_SCALAR
        import vosa_serialization.BROADCAST_SCALAR_MODE
        import vosa_serialization.CAST
        import vosa_serialization.CHANNEL_EXTRACT
        import vosa_serialization.CHANNEL_EXTRACT_MODE
        import vosa_serialization.CLAMP
        import vosa_serialization.CLAMP_MODE
        import vosa_serialization.CONCAT
        import vosa_serialization.CONCAT_MODE
        import vosa_serialization.CONST_IMAGE
        import vosa_serialization.CONST_IMAGE_MODE
        import vosa_serialization.CONST_PLANE
        import vosa_serialization.CONST_PLANE_MODE
        import vosa_serialization.CONST_SCALAR
        import vosa_serialization.CONST_SCALAR_MODE
        import vosa_serialization.CONV_2D
        import vosa_serialization.CONV_2D_MODE
        import vosa_serialization.CUSTOM_SCALAR
        import vosa_serialization.DECIMATE
        import vosa_serialization.DECIMATE_MODE
        import vosa_serialization.DIV
        import vosa_serialization.DIV_MODE
        import vosa_serialization.EQUAL
        import vosa_serialization.EQUAL_MODE
        import vosa_serialization.EXPAND
        import vosa_serialization.EXPAND_MODE
        import vosa_serialization.IMPORT_CHANNEL
        import vosa_serialization.IMPORT_CHANNEL_MODE
        import vosa_serialization.GAMMA_CORRECTION
        import vosa_serialization.GAMMA_CORRECTION_MODE
        import vosa_serialization.GREATER
        import vosa_serialization.GREATER_MODE
        import vosa_serialization.GREATER_EQUAL
        import vosa_serialization.GREATER_EQUAL_MODE
        import vosa_serialization.LOGICAL_SHIFT_RIGHT
        import vosa_serialization.LOGICAL_SHIFT_RIGHT_MODE
        import vosa_serialization.MESH_GRID
        import vosa_serialization.MESH_GRID_MODE
        import vosa_serialization.MAX_
        import vosa_serialization.MAX__MODE
        import vosa_serialization.MIN_
        import vosa_serialization.MIN__MODE
        import vosa_serialization.MOD
        import vosa_serialization.MOD_MODE
        import vosa_serialization.MULT
        import vosa_serialization.MULT_MODE
        import vosa_serialization.NOT
        import vosa_serialization.NOT_MODE
        import vosa_serialization.OR
        import vosa_serialization.OR_MODE
        import vosa_serialization.PAD
        import vosa_serialization.PAD_MODE
        import vosa_serialization.POWER
        import vosa_serialization.POWER_MODE
        import vosa_serialization.PIECEWISE_LINEAR
        import vosa_serialization.PIECEWISE_LINEAR_MODE
        import vosa_serialization.POINTWISE_MATRIX_MULTIPLY
        import vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE
        import vosa_serialization.REDUCE_INTERP_CHANNELWISE
        import vosa_serialization.REDUCE_INTERP_CHANNELWISE_MODE
        import vosa_serialization.REDUCE_MAX_CHANNELWISE
        import vosa_serialization.REDUCE_MAX_CHANNELWISE_MODE
        import vosa_serialization.REDUCE_MAX_PLANEWISE
        import vosa_serialization.REDUCE_MAX_PLANEWISE_MODE
        import vosa_serialization.REDUCE_MIN_PLANEWISE
        import vosa_serialization.REDUCE_MIN_PLANEWISE_MODE
        import vosa_serialization.REDUCE_SUM_CHANNELWISE
        import vosa_serialization.REDUCE_SUM_CHANNELWISE_MODE
        import vosa_serialization.REDUCE_SUM_PLANEWISE
        import vosa_serialization.REDUCE_SUM_PLANEWISE_MODE
        import vosa_serialization.RESIZE_BILINEAR
        import vosa_serialization.RESIZE_BILINEAR_MODE
        import vosa_serialization.RESIZE_NEAREST_NEIGHBOUR
        import vosa_serialization.RESIZE_NEAREST_NEIGHBOUR_MODE
        import vosa_serialization.ROTATE_90
        import vosa_serialization.ROTATE_90_MODE
        import vosa_serialization.ROUND
        import vosa_serialization.ROUND_MODE
        import vosa_serialization.SQRT
        import vosa_serialization.SQRT_MODE
        import vosa_serialization.TOSA_GRAPH
        import vosa_serialization.SUB
        import vosa_serialization.SUB_MODE
        import vosa_serialization.WHERE
        import vosa_serialization.WHERE_MODE

        if isinstance(op, vosa_serialization.INPUT.INPUTT):
            # FIXME -- Needs better handling: AType + DType
            dtype_dict = {
                vosa_serialization.DTYPE.DTYPE.UINT8: vosa.op.INPUT_MODES.D_UINT8,
                vosa_serialization.DTYPE.DTYPE.UINT16: vosa.op.INPUT_MODES.D_UINT16,
                vosa_serialization.DTYPE.DTYPE.UINT32: vosa.op.INPUT_MODES.D_UINT32,
                vosa_serialization.DTYPE.DTYPE.INT8: vosa.op.INPUT_MODES.D_INT8,
                vosa_serialization.DTYPE.DTYPE.INT16: vosa.op.INPUT_MODES.D_INT16,
                vosa_serialization.DTYPE.DTYPE.INT32: vosa.op.INPUT_MODES.D_INT32,
                vosa_serialization.DTYPE.DTYPE.FLOAT16: vosa.op.INPUT_MODES.D_FLOAT16,
                vosa_serialization.DTYPE.DTYPE.FLOAT32: vosa.op.INPUT_MODES.D_FLOAT32
                # vosa_serialization.ATYPE.ATYPE.P_UINT8: vosa.op.INPUT_MODES.P_UINT8,
                # vosa_serialization.ATYPE.ATYPE.P_UINT16: vosa.op.INPUT_MODES.P_UINT16,
                # vosa_serialization.ATYPE.ATYPE.I_UINT8: vosa.op.INPUT_MODES.I_UINT8,
                # vosa_serialization.ATYPE.ATYPE.I_BOOL: vosa.op.INPUT_MODES.I_BOOL,
                # vosa_serialization.ATYPE.ATYPE.I_INT16: vosa.op.INPUT_MODES.I_INT16,
                # vosa_serialization.ATYPE.ATYPE.I_FLOAT16: vosa.op.INPUT_MODES.I_FLOAT16,
                # vosa_serialization.ATYPE.ATYPE.I_FLOAT32: vosa.op.INPUT_MODES.I_FLOAT32,
                # vosa_serialization.ATYPE.ATYPE.P_FLOAT32: vosa.op.INPUT_MODES.P_FLOAT32,
                # vosa_serialization.ATYPE.ATYPE.S_FLOAT32: vosa.op.INPUT_MODES.S_FLOAT32,
                # vosa_serialization.ATYPE.ATYPE.S_FLOAT16: vosa.op.INPUT_MODES.S_FLOAT16,
                # vosa_serialization.ATYPE.ATYPE.S_INT16: vosa.op.INPUT_MODES.S_INT16,
                # vosa_serialization.ATYPE.ATYPE.S_INT32: vosa.op.INPUT_MODES.S_INT32,
                # vosa_serialization.ATYPE.ATYPE.V_UINT32: vosa.op.INPUT_MODES.V_UINT32,
                # vosa_serialization.ATYPE.ATYPE.V_FLOAT16: vosa.op.INPUT_MODES.V_FLOAT16,
                # vosa_serialization.ATYPE.ATYPE.I_NV12: vosa.op.INPUT_MODES.I_NV12,
                # vosa_serialization.ATYPE.ATYPE.I_NV21: vosa.op.INPUT_MODES.I_NV21,
                # vosa_serialization.ATYPE.ATYPE.I_UYVY: vosa.op.INPUT_MODES.I_UYVY,
                # vosa_serialization.ATYPE.ATYPE.I_YUYV: vosa.op.INPUT_MODES.I_YUYV,
                # vosa_serialization.ATYPE.ATYPE.I_IYUV: vosa.op.INPUT_MODES.I_IYUV,
                # vosa_serialization.ATYPE.ATYPE.I_YUV4: vosa.op.INPUT_MODES.I_YUV4,
                # vosa_serialization.ATYPE.ATYPE.I_RGB: vosa.op.INPUT_MODES.I_RGB,
                # vosa_serialization.ATYPE.ATYPE.I_RGBX: vosa.op.INPUT_MODES.I_RGBX,
                # vosa_serialization.ATYPE.ATYPE.S_HISTOGRAM: vosa.op.INPUT_MODES.S_HISTOGRAM,
            }
            vosa.ops.input(dtype_dict[op.dtype], name=name)
        elif isinstance(op, vosa_serialization.ABS.ABST):
            dtype_dict = {
                vosa_serialization.ABS_MODE.ABS_MODE.I_I_INT8_INT8: vosa.ops.elementwise_unary.ABS_MODE.I_I_INT8_INT8,
                vosa_serialization.ABS_MODE.ABS_MODE.I_I_INT16_INT16: vosa.ops.elementwise_unary.ABS_MODE.I_I_INT16_INT16,
                vosa_serialization.ABS_MODE.ABS_MODE.I_I_INT32_INT32: vosa.ops.elementwise_unary.ABS_MODE.I_I_INT32_INT32,
                vosa_serialization.ABS_MODE.ABS_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.elementwise_unary.ABS_MODE.I_I_FLOAT16_FLOAT16,
                vosa_serialization.ABS_MODE.ABS_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.elementwise_unary.ABS_MODE.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.abs(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.ABS_DIFF.ABS_DIFFT):
            dtype_dict = {
                vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_UINT8_UINT8: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_UINT8_UINT8,
                vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_UINT16_UINT16: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_UINT16_UINT16,
                vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_UINT32_UINT32: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_UINT32_UINT32,
                vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_INT8_UINT8: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_INT8_UINT8,
                vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_INT16_UINT16: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_INT16_UINT16,
                vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_INT32_UINT32: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_INT32_UINT32,
                vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.ABS_DIFF_MODE.ABS_DIFF_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.elementwise_binary.ABS_DIFF_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.abs_diff(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.ADD.ADDT):
            dtype_dict = {
                vosa_serialization.ADD_MODE.ADD_MODE.I_I_UINT8_UINT8: vosa.ops.elementwise_binary.ADD_MODES.I_I_UINT8_UINT8,
                vosa_serialization.ADD_MODE.ADD_MODE.I_I_UINT16_UINT16: vosa.ops.elementwise_binary.ADD_MODES.I_I_UINT16_UINT16,
                vosa_serialization.ADD_MODE.ADD_MODE.I_I_UINT32_UINT32: vosa.ops.elementwise_binary.ADD_MODES.I_I_UINT32_UINT32,
                vosa_serialization.ADD_MODE.ADD_MODE.I_I_INT8_INT8: vosa.ops.elementwise_binary.ADD_MODES.I_I_INT8_INT8,
                vosa_serialization.ADD_MODE.ADD_MODE.I_I_INT16_INT16: vosa.ops.elementwise_binary.ADD_MODES.I_I_INT16_INT16,
                vosa_serialization.ADD_MODE.ADD_MODE.I_I_INT32_INT32: vosa.ops.elementwise_binary.ADD_MODES.I_I_INT32_INT32,
                vosa_serialization.ADD_MODE.ADD_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.elementwise_binary.ADD_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.ADD_MODE.ADD_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.elementwise_binary.ADD_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.add(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(
            op, vosa_serialization.ARITHMETIC_SHIFT_RIGHT.ARITHMETIC_SHIFT_RIGHTT
        ):
            dtype_dict = {
                vosa_serialization.ARITHMETIC_SHIFT_RIGHT_MODE.ARITHMETIC_SHIFT_RIGHT_MODE.I_I_INT8_INT8: vosa.ops.elementwise_unary.ARITHMETIC_SHIFT_RIGHT_MODES.I_I_INT8_INT8,
                vosa_serialization.ARITHMETIC_SHIFT_RIGHT_MODE.ARITHMETIC_SHIFT_RIGHT_MODE.I_I_INT16_INT16: vosa.ops.elementwise_unary.ARITHMETIC_SHIFT_RIGHT_MODES.I_I_INT16_INT16,
                vosa_serialization.ARITHMETIC_SHIFT_RIGHT_MODE.ARITHMETIC_SHIFT_RIGHT_MODE.I_I_INT32_INT32: vosa.ops.elementwise_unary.ARITHMETIC_SHIFT_RIGHT_MODES.I_I_INT32_INT32,
            }
            vosa.ops.arithmetic_shift_right(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(
            op, vosa_serialization.ARG_MIN_CHANNELWISE.ARG_MIN_CHANNELWISET
        ):
            dtype_dict = {
                vosa_serialization.ARG_MIN_CHANNELWISE_MODE.ARG_MIN_CHANNELWISE_MODE.I_I_UINT8_UINT32: vosa.ops.reduction.ARG_MIN_CHANNELWISE_MODES.I_I_UINT8_UINT32,
                vosa_serialization.ARG_MIN_CHANNELWISE_MODE.ARG_MIN_CHANNELWISE_MODE.I_I_UINT16_UINT32: vosa.ops.reduction.ARG_MIN_CHANNELWISE_MODES.I_I_UINT16_UINT32,
                vosa_serialization.ARG_MIN_CHANNELWISE_MODE.ARG_MIN_CHANNELWISE_MODE.I_I_UINT32_UINT32: vosa.ops.reduction.ARG_MIN_CHANNELWISE_MODES.I_I_UINT32_UINT32,
                vosa_serialization.ARG_MIN_CHANNELWISE_MODE.ARG_MIN_CHANNELWISE_MODE.I_I_INT8_UINT32: vosa.ops.reduction.ARG_MIN_CHANNELWISE_MODES.I_I_INT8_UINT32,
                vosa_serialization.ARG_MIN_CHANNELWISE_MODE.ARG_MIN_CHANNELWISE_MODE.I_I_INT16_UINT32: vosa.ops.reduction.ARG_MIN_CHANNELWISE_MODES.I_I_INT16_UINT32,
                vosa_serialization.ARG_MIN_CHANNELWISE_MODE.ARG_MIN_CHANNELWISE_MODE.I_I_INT32_UINT32: vosa.ops.reduction.ARG_MIN_CHANNELWISE_MODES.I_I_INT32_UINT32,
                vosa_serialization.ARG_MIN_CHANNELWISE_MODE.ARG_MIN_CHANNELWISE_MODE.I_I_FLOAT16_UINT32: vosa.ops.reduction.ARG_MIN_CHANNELWISE_MODES.I_I_FLOAT16_UINT32,
                vosa_serialization.ARG_MIN_CHANNELWISE_MODE.ARG_MIN_CHANNELWISE_MODE.I_I_FLOAT32_UINT32: vosa.ops.reduction.ARG_MIN_CHANNELWISE_MODES.I_I_FLOAT32_UINT32,
            }
            vosa.ops.argmin_channel(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(op, vosa_serialization.BROADCAST_SCALAR.BROADCAST_SCALART):
            dtype_dict = {
                vosa_serialization.BROADCAST_SCALAR_MODE.BROADCAST_SCALAR_MODE.S_I_UINT8_UINT8: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_UINT8_UINT8,
                vosa_serialization.BROADCAST_SCALAR_MODE.BROADCAST_SCALAR_MODE.S_I_UINT16_UINT16: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_UINT16_UINT16,
                vosa_serialization.BROADCAST_SCALAR_MODE.BROADCAST_SCALAR_MODE.S_I_UINT32_UINT32: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_UINT32_UINT32,
                vosa_serialization.BROADCAST_SCALAR_MODE.BROADCAST_SCALAR_MODE.S_I_INT8_INT8: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_INT8_INT8,
                vosa_serialization.BROADCAST_SCALAR_MODE.BROADCAST_SCALAR_MODE.S_I_INT16_INT16: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_INT16_INT16,
                vosa_serialization.BROADCAST_SCALAR_MODE.BROADCAST_SCALAR_MODE.S_I_INT32_INT32: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_INT32_INT32,
                vosa_serialization.BROADCAST_SCALAR_MODE.BROADCAST_SCALAR_MODE.S_I_FLOAT16_FLOAT16: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_FLOAT16_FLOAT16,
                vosa_serialization.BROADCAST_SCALAR_MODE.BROADCAST_SCALAR_MODE.S_I_FLOAT32_FLOAT32: vosa.ops.data.BROADCAST_PLANEWISE_MODES.S_I_FLOAT32_FLOAT32,
            }
            vosa.ops.broadcast_scalar(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(
            op, vosa_serialization.BROADCAST_CHANNELWISE.BROADCAST_CHANNELWISET
        ):
            dtype_dict = {
                vosa_serialization.BROADCAST_CHANNELWISE_MODE.BROADCAST_CHANNELWISE_MODE.I_I_BOOL_BOOL: vosa.ops.data.BROADCAST_CHANNELWISE_MODES.I_I_BOOL_BOOL,
                vosa_serialization.BROADCAST_CHANNELWISE_MODE.BROADCAST_CHANNELWISE_MODE.I_I_UINT8_UINT8: vosa.ops.data.BROADCAST_CHANNELWISE_MODES.I_I_UINT8_UINT8,
                vosa_serialization.BROADCAST_CHANNELWISE_MODE.BROADCAST_CHANNELWISE_MODE.I_I_UINT16_UINT16: vosa.ops.data.BROADCAST_CHANNELWISE_MODES.I_I_UINT16_UINT16,
                vosa_serialization.BROADCAST_CHANNELWISE_MODE.BROADCAST_CHANNELWISE_MODE.I_I_UINT32_UINT32: vosa.ops.data.BROADCAST_CHANNELWISE_MODES.I_I_UINT32_UINT32,
                vosa_serialization.BROADCAST_CHANNELWISE_MODE.BROADCAST_CHANNELWISE_MODE.I_I_INT8_INT8: vosa.ops.data.BROADCAST_CHANNELWISE_MODES.I_I_INT8_INT8,
                vosa_serialization.BROADCAST_CHANNELWISE_MODE.BROADCAST_CHANNELWISE_MODE.I_I_INT16_INT16: vosa.ops.data.BROADCAST_CHANNELWISE_MODES.I_I_INT16_INT16,
                vosa_serialization.BROADCAST_CHANNELWISE_MODE.BROADCAST_CHANNELWISE_MODE.I_I_INT32_INT32: vosa.ops.data.BROADCAST_CHANNELWISE_MODES.I_I_INT32_INT32,
                vosa_serialization.BROADCAST_CHANNELWISE_MODE.BROADCAST_CHANNELWISE_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.data.BROADCAST_CHANNELWISE_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.BROADCAST_CHANNELWISE_MODE.BROADCAST_CHANNELWISE_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.data.BROADCAST_CHANNELWISE_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.broadcast_channelwise(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(op, vosa_serialization.CAST.CASTT):
            dtype_dict = {
                vosa_serialization.DTYPE.DTYPE.UINT8: vosa.types.DType.UINT8,
                vosa_serialization.DTYPE.DTYPE.UINT16: vosa.types.DType.UINT16,
                vosa_serialization.DTYPE.DTYPE.UINT32: vosa.types.DType.UINT32,
                vosa_serialization.DTYPE.DTYPE.INT8: vosa.types.DType.INT8,
                vosa_serialization.DTYPE.DTYPE.INT16: vosa.types.DType.INT16,
                vosa_serialization.DTYPE.DTYPE.INT32: vosa.types.DType.INT32,
                vosa_serialization.DTYPE.DTYPE.FLOAT16: vosa.types.DType.FLOAT16,
                vosa_serialization.DTYPE.DTYPE.FLOAT32: vosa.types.DType.FLOAT32,
            }
            vosa.ops.cast_image(
                *input_ops, output_dtype=dtype_dict[op.outputType], name=name
            )
        elif isinstance(op, vosa_serialization.CHANNEL_EXTRACT.CHANNEL_EXTRACTT):
            dtype_dict = {
                vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_BOOL_BOOL: vosa.ops.data.CHANNEL_EXTRACT_MODES.I_I_BOOL_BOOL,
                vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_UINT8_UINT8: vosa.ops.data.CHANNEL_EXTRACT_MODES.I_I_UINT8_UINT8,
                vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_UINT16_UINT16: vosa.ops.data.CHANNEL_EXTRACT_MODES.I_I_UINT16_UINT16,
                vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_UINT32_UINT32: vosa.ops.data.CHANNEL_EXTRACT_MODES.I_I_UINT32_UINT32,
                vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_INT8_INT8: vosa.ops.data.CHANNEL_EXTRACT_MODES.I_I_INT8_INT8,
                vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_INT16_INT16: vosa.ops.data.CHANNEL_EXTRACT_MODES.I_I_INT16_INT16,
                vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_INT32_INT32: vosa.ops.data.CHANNEL_EXTRACT_MODES.I_I_INT32_INT32,
                vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.data.CHANNEL_EXTRACT_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.CHANNEL_EXTRACT_MODE.CHANNEL_EXTRACT_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.data.CHANNEL_EXTRACT_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.channel_extract(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(op, vosa_serialization.CLAMP.CLAMPT):
            dtype_dict = {
                vosa_serialization.CLAMP_MODE.CLAMP_MODE.I_I_UINT8_UINT8: vosa.ops.elementwise_unary.CLAMP_MODES.I_I_UINT8_UINT8,
                vosa_serialization.CLAMP_MODE.CLAMP_MODE.I_I_UINT16_UINT16: vosa.ops.elementwise_unary.CLAMP_MODES.I_I_UINT16_UINT16,
                vosa_serialization.CLAMP_MODE.CLAMP_MODE.I_I_UINT32_UINT32: vosa.ops.elementwise_unary.CLAMP_MODES.I_I_UINT32_UINT32,
                vosa_serialization.CLAMP_MODE.CLAMP_MODE.I_I_INT8_INT8: vosa.ops.elementwise_unary.CLAMP_MODES.I_I_INT8_INT8,
                vosa_serialization.CLAMP_MODE.CLAMP_MODE.I_I_INT16_INT16: vosa.ops.elementwise_unary.CLAMP_MODES.I_I_INT16_INT16,
                vosa_serialization.CLAMP_MODE.CLAMP_MODE.I_I_INT32_INT32: vosa.ops.elementwise_unary.CLAMP_MODES.I_I_INT32_INT32,
                vosa_serialization.CLAMP_MODE.CLAMP_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.elementwise_unary.CLAMP_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.CLAMP_MODE.CLAMP_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.elementwise_unary.CLAMP_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.clamp(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(
            op, vosa_serialization.POINTWISE_MATRIX_MULTIPLY.POINTWISE_MATRIX_MULTIPLYT
        ):
            dtype_dict = {
                vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_UINT8_UINT8: vosa.ops.image_transformation.POINTWISE_MATRIX_MULTIPLY_MODES.I_I_UINT8_UINT8,
                vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_UINT16_UINT16: vosa.ops.image_transformation.POINTWISE_MATRIX_MULTIPLY_MODES.I_I_UINT16_UINT16,
                vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_UINT32_UINT32: vosa.ops.image_transformation.POINTWISE_MATRIX_MULTIPLY_MODES.I_I_UINT32_UINT32,
                vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_INT8_INT8: vosa.ops.image_transformation.POINTWISE_MATRIX_MULTIPLY_MODES.I_I_INT8_INT8,
                vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_INT16_INT16: vosa.ops.image_transformation.POINTWISE_MATRIX_MULTIPLY_MODES.I_I_INT16_INT16,
                vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_INT32_INT32: vosa.ops.image_transformation.POINTWISE_MATRIX_MULTIPLY_MODES.I_I_INT32_INT32,
                vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.image_transformation.POINTWISE_MATRIX_MULTIPLY_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.POINTWISE_MATRIX_MULTIPLY_MODE.POINTWISE_MATRIX_MULTIPLY_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.image_transformation.POINTWISE_MATRIX_MULTIPLY_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.pointwise_matrix_multiply(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(op, vosa_serialization.CONCAT.CONCATT):
            dtype_dict = {
                vosa_serialization.CONCAT_MODE.CONCAT_MODE.I_I_UINT8_UINT8: vosa.ops.data.CONCAT_MODES.I_I_UINT8_UINT8,
                vosa_serialization.CONCAT_MODE.CONCAT_MODE.I_I_UINT16_UINT16: vosa.ops.data.CONCAT_MODES.I_I_UINT16_UINT16,
                vosa_serialization.CONCAT_MODE.CONCAT_MODE.I_I_UINT32_UINT32: vosa.ops.data.CONCAT_MODES.I_I_UINT32_UINT32,
                vosa_serialization.CONCAT_MODE.CONCAT_MODE.I_I_INT8_INT8: vosa.ops.data.CONCAT_MODES.I_I_INT8_INT8,
                vosa_serialization.CONCAT_MODE.CONCAT_MODE.I_I_INT16_INT16: vosa.ops.data.CONCAT_MODES.I_I_INT16_INT16,
                vosa_serialization.CONCAT_MODE.CONCAT_MODE.I_I_INT32_INT32: vosa.ops.data.CONCAT_MODES.I_I_INT32_INT32,
                vosa_serialization.CONCAT_MODE.CONCAT_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.data.CONCAT_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.CONCAT_MODE.CONCAT_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.data.CONCAT_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.concat(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.CONST_SCALAR.CONST_SCALART):
            dtype_dict = {
                vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_UINT8: vosa.ops.data.CONST_SCALAR_MODES.S_UINT8,
                vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_UINT16: vosa.ops.data.CONST_SCALAR_MODES.S_UINT16,
                vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_UINT32: vosa.ops.data.CONST_SCALAR_MODES.S_UINT32,
                vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_INT8: vosa.ops.data.CONST_SCALAR_MODES.S_INT8,
                vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_INT16: vosa.ops.data.CONST_SCALAR_MODES.S_INT16,
                vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_INT32: vosa.ops.data.CONST_SCALAR_MODES.S_INT32,
                vosa_serialization.CONST_SCALAR_MODE.CONST_SCALAR_MODE.S_FLOAT32: vosa.ops.data.CONST_SCALAR_MODES.S_FLOAT32,
            }
            vosa.ops.const_scalar(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(op, vosa_serialization.CONST_IMAGE.CONST_IMAGET):
            dtype_dict = {
                vosa_serialization.CONST_IMAGE_MODE.CONST_IMAGE_MODE.I_FLOAT32: vosa.ops.data.CONST_IMAGE_MODES.I_FLOAT32
            }
            vosa.ops.const_image(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(op, vosa_serialization.CONST_PLANE.CONST_PLANET):
            dtype_dict = {
                vosa_serialization.CONST_PLANE_MODE.CONST_PLANE_MODE.P_UINT8: vosa.ops.data.CONST_PLANE_MODES.P_UINT8
            }
            vosa.ops.const_plane(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(op, vosa_serialization.CONV_2D.CONV_2DT):
            dtype_dict = {
                vosa_serialization.CONV_2D_MODE.CONV_2D_MODE.I_I_UINT8_UINT8: vosa.ops.filtering.CONV_2D_MODES.I_I_UINT8_UINT8,
                vosa_serialization.CONV_2D_MODE.CONV_2D_MODE.I_I_UINT16_UINT16: vosa.ops.filtering.CONV_2D_MODES.I_I_UINT16_UINT16,
                vosa_serialization.CONV_2D_MODE.CONV_2D_MODE.I_I_UINT32_UINT32: vosa.ops.filtering.CONV_2D_MODES.I_I_UINT32_UINT32,
                vosa_serialization.CONV_2D_MODE.CONV_2D_MODE.I_I_INT8_INT8: vosa.ops.filtering.CONV_2D_MODES.I_I_INT8_INT8,
                vosa_serialization.CONV_2D_MODE.CONV_2D_MODE.I_I_INT16_INT16: vosa.ops.filtering.CONV_2D_MODES.I_I_INT16_INT16,
                vosa_serialization.CONV_2D_MODE.CONV_2D_MODE.I_I_INT32_INT32: vosa.ops.filtering.CONV_2D_MODES.I_I_INT32_INT32,
                vosa_serialization.CONV_2D_MODE.CONV_2D_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.filtering.CONV_2D_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.CONV_2D_MODE.CONV_2D_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.filtering.CONV_2D_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.CONV(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.CUSTOM_SCALAR.CUSTOM_SCALART):
            vosa.ops.custom_scalar_op(
                *input_ops, output_dtype=vosa.types.DType.FLOAT32, name=name
            )
        elif isinstance(op, vosa_serialization.DECIMATE.DECIMATET):
            dtype_dict = {
                vosa_serialization.DECIMATE_MODE.DECIMATE_MODE.I_I_UINT8_UINT8: vosa.ops.image_transformation.DECIMATE_MODES.I_I_UINT8_UINT8,
                vosa_serialization.DECIMATE_MODE.DECIMATE_MODE.I_I_UINT16_UINT16: vosa.ops.image_transformation.DECIMATE_MODES.I_I_UINT16_UINT16,
                vosa_serialization.DECIMATE_MODE.DECIMATE_MODE.I_I_UINT32_UINT32: vosa.ops.image_transformation.DECIMATE_MODES.I_I_UINT32_UINT32,
                vosa_serialization.DECIMATE_MODE.DECIMATE_MODE.I_I_INT8_INT8: vosa.ops.image_transformation.DECIMATE_MODES.I_I_INT8_INT8,
                vosa_serialization.DECIMATE_MODE.DECIMATE_MODE.I_I_INT16_INT16: vosa.ops.image_transformation.DECIMATE_MODES.I_I_INT16_INT16,
                vosa_serialization.DECIMATE_MODE.DECIMATE_MODE.I_I_INT32_INT32: vosa.ops.image_transformation.DECIMATE_MODES.I_I_INT32_INT32,
                vosa_serialization.DECIMATE_MODE.DECIMATE_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.image_transformation.DECIMATE_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.DECIMATE_MODE.DECIMATE_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.image_transformation.DECIMATE_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.decimate(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.DIV.DIVT):
            dtype_dict = {
                vosa_serialization.DIV_MODE.DIV_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.elementwise_binary.DIV_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.DIV_MODE.DIV_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.elementwise_binary.DIV_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.div(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.EXPAND.EXPANDT):
            dtype_dict = {
                vosa_serialization.EXPAND_MODE.EXPAND_MODE.I_I_UINT8_UINT8: vosa.ops.image_transformation.EXPAND_MODES.I_I_UINT8_UINT8,
                vosa_serialization.EXPAND_MODE.EXPAND_MODE.I_I_UINT16_UINT16: vosa.ops.image_transformation.EXPAND_MODES.I_I_UINT16_UINT16,
                vosa_serialization.EXPAND_MODE.EXPAND_MODE.I_I_UINT32_UINT32: vosa.ops.image_transformation.EXPAND_MODES.I_I_UINT32_UINT32,
                vosa_serialization.EXPAND_MODE.EXPAND_MODE.I_I_INT8_INT8: vosa.ops.image_transformation.EXPAND_MODES.I_I_INT8_INT8,
                vosa_serialization.EXPAND_MODE.EXPAND_MODE.I_I_INT16_INT16: vosa.ops.image_transformation.EXPAND_MODES.I_I_INT16_INT16,
                vosa_serialization.EXPAND_MODE.EXPAND_MODE.I_I_INT32_INT32: vosa.ops.image_transformation.EXPAND_MODES.I_I_INT32_INT32,
                vosa_serialization.EXPAND_MODE.EXPAND_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.image_transformation.EXPAND_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.EXPAND_MODE.EXPAND_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.image_transformation.EXPAND_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.expand(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.IMPORT_CHANNEL.IMPORT_CHANNELT):
            dtype_dict = {
                vosa_serialization.IMPORT_CHANNEL_MODE.IMPORT_CHANNEL_MODE.D_I_UINT8_UINT8: vosa.ops.data.IMPORT_CHANNEL_EXECUTION_MODES.D_I_UINT8_UINT8,
                vosa_serialization.IMPORT_CHANNEL_MODE.IMPORT_CHANNEL_MODE.D_I_UINT16_UINT16: vosa.ops.data.IMPORT_CHANNEL_EXECUTION_MODES.D_I_UINT16_UINT16,
                vosa_serialization.IMPORT_CHANNEL_MODE.IMPORT_CHANNEL_MODE.D_I_UINT32_UINT32: vosa.ops.data.IMPORT_CHANNEL_EXECUTION_MODES.D_I_UINT32_UINT32,
                vosa_serialization.IMPORT_CHANNEL_MODE.IMPORT_CHANNEL_MODE.D_I_INT8_INT8: vosa.ops.data.IMPORT_CHANNEL_EXECUTION_MODES.D_I_INT8_INT8,
                vosa_serialization.IMPORT_CHANNEL_MODE.IMPORT_CHANNEL_MODE.D_I_INT16_INT16: vosa.ops.data.IMPORT_CHANNEL_EXECUTION_MODES.D_I_INT16_INT16,
                vosa_serialization.IMPORT_CHANNEL_MODE.IMPORT_CHANNEL_MODE.D_I_INT32_INT32: vosa.ops.data.IMPORT_CHANNEL_EXECUTION_MODES.D_I_INT32_INT32,
                vosa_serialization.IMPORT_CHANNEL_MODE.IMPORT_CHANNEL_MODE.D_I_FLOAT16_FLOAT16: vosa.ops.data.IMPORT_CHANNEL_EXECUTION_MODES.D_I_FLOAT16_FLOAT16,
                vosa_serialization.IMPORT_CHANNEL_MODE.IMPORT_CHANNEL_MODE.D_I_FLOAT32_FLOAT32: vosa.ops.data.IMPORT_CHANNEL_EXECUTION_MODES.D_I_FLOAT32_FLOAT32,
            }
            vosa.ops.import_channel(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(
            op, vosa_serialization.LOGICAL_SHIFT_RIGHT.LOGICAL_SHIFT_RIGHTT
        ):
            dtype_dict = {
                vosa_serialization.LOGICAL_SHIFT_RIGHT_MODE.LOGICAL_SHIFT_RIGHT_MODE.I_I_UINT8_UINT8: vosa.ops.elementwise_unary.LOGICAL_SHIFT_RIGHT_MODES.I_I_UINT8_UINT8,
                vosa_serialization.LOGICAL_SHIFT_RIGHT_MODE.LOGICAL_SHIFT_RIGHT_MODE.I_I_UINT16_UINT16: vosa.ops.elementwise_unary.LOGICAL_SHIFT_RIGHT_MODES.I_I_UINT16_UINT16,
                vosa_serialization.LOGICAL_SHIFT_RIGHT_MODE.LOGICAL_SHIFT_RIGHT_MODE.I_I_UINT32_UINT32: vosa.ops.elementwise_unary.LOGICAL_SHIFT_RIGHT_MODES.I_I_UINT32_UINT32,
            }
            vosa.ops.logical_shift_right(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(op, vosa_serialization.MIN_.MIN_T):
            dtype_dict = {
                vosa_serialization.MIN__MODE.MIN__MODE.I_I_UINT8_UINT8: vosa.ops.elementwise_binary.MIN_MODES.I_I_UINT8_UINT8,
                vosa_serialization.MIN__MODE.MIN__MODE.I_I_UINT16_UINT16: vosa.ops.elementwise_binary.MIN_MODES.I_I_UINT16_UINT16,
                vosa_serialization.MIN__MODE.MIN__MODE.I_I_UINT32_UINT32: vosa.ops.elementwise_binary.MIN_MODES.I_I_UINT32_UINT32,
                vosa_serialization.MIN__MODE.MIN__MODE.I_I_INT8_INT8: vosa.ops.elementwise_binary.MIN_MODES.I_I_INT8_INT8,
                vosa_serialization.MIN__MODE.MIN__MODE.I_I_INT16_INT16: vosa.ops.elementwise_binary.MIN_MODES.I_I_INT16_INT16,
                vosa_serialization.MIN__MODE.MIN__MODE.I_I_INT32_INT32: vosa.ops.elementwise_binary.MIN_MODES.I_I_INT32_INT32,
                vosa_serialization.MIN__MODE.MIN__MODE.I_I_FLOAT16_FLOAT16: vosa.ops.elementwise_binary.MIN_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.MIN__MODE.MIN__MODE.I_I_FLOAT32_FLOAT32: vosa.ops.elementwise_binary.MIN_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.min(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.MAX_.MAX_T):
            dtype_dict = {
                vosa_serialization.MAX__MODE.MAX__MODE.I_I_UINT8_UINT8: vosa.ops.elementwise_binary.MAX_MODES.I_I_UINT8_UINT8,
                vosa_serialization.MAX__MODE.MAX__MODE.I_I_UINT16_UINT16: vosa.ops.elementwise_binary.MAX_MODES.I_I_UINT16_UINT16,
                vosa_serialization.MAX__MODE.MAX__MODE.I_I_UINT32_UINT32: vosa.ops.elementwise_binary.MAX_MODES.I_I_UINT32_UINT32,
                vosa_serialization.MAX__MODE.MAX__MODE.I_I_INT8_INT8: vosa.ops.elementwise_binary.MAX_MODES.I_I_INT8_INT8,
                vosa_serialization.MAX__MODE.MAX__MODE.I_I_INT16_INT16: vosa.ops.elementwise_binary.MAX_MODES.I_I_INT16_INT16,
                vosa_serialization.MAX__MODE.MAX__MODE.I_I_INT32_INT32: vosa.ops.elementwise_binary.MAX_MODES.I_I_INT32_INT32,
                vosa_serialization.MAX__MODE.MAX__MODE.I_I_FLOAT16_FLOAT16: vosa.ops.elementwise_binary.MAX_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.MAX__MODE.MAX__MODE.I_I_FLOAT32_FLOAT32: vosa.ops.elementwise_binary.MAX_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.max(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.MULT.MULTT):
            dtype_dict = {
                vosa_serialization.MULT_MODE.MULT_MODE.I_I_UINT8_UINT8: vosa.ops.elementwise_binary.MULT_MODES.I_I_UINT8_UINT8,
                vosa_serialization.MULT_MODE.MULT_MODE.I_I_UINT16_UINT16: vosa.ops.elementwise_binary.MULT_MODES.I_I_UINT16_UINT16,
                vosa_serialization.MULT_MODE.MULT_MODE.I_I_UINT32_UINT32: vosa.ops.elementwise_binary.MULT_MODES.I_I_UINT32_UINT32,
                vosa_serialization.MULT_MODE.MULT_MODE.I_I_INT8_INT8: vosa.ops.elementwise_binary.MULT_MODES.I_I_INT8_INT8,
                vosa_serialization.MULT_MODE.MULT_MODE.I_I_INT16_INT16: vosa.ops.elementwise_binary.MULT_MODES.I_I_INT16_INT16,
                vosa_serialization.MULT_MODE.MULT_MODE.I_I_INT32_INT32: vosa.ops.elementwise_binary.MULT_MODES.I_I_INT32_INT32,
                vosa_serialization.MULT_MODE.MULT_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.elementwise_binary.MULT_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.MULT_MODE.MULT_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.elementwise_binary.MULT_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.mult(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.PAD.PADT):
            dtype_dict = {
                vosa_serialization.PAD_MODE.PAD_MODE.I_I_UINT8_UINT8: vosa.ops.image_transformation.PAD_MODES.I_I_UINT8_UINT8,
                vosa_serialization.PAD_MODE.PAD_MODE.I_I_UINT16_UINT16: vosa.ops.image_transformation.PAD_MODES.I_I_UINT16_UINT16,
                vosa_serialization.PAD_MODE.PAD_MODE.I_I_UINT32_UINT32: vosa.ops.image_transformation.PAD_MODES.I_I_UINT32_UINT32,
                vosa_serialization.PAD_MODE.PAD_MODE.I_I_INT8_INT8: vosa.ops.image_transformation.PAD_MODES.I_I_INT8_INT8,
                vosa_serialization.PAD_MODE.PAD_MODE.I_I_INT16_INT16: vosa.ops.image_transformation.PAD_MODES.I_I_INT16_INT16,
                vosa_serialization.PAD_MODE.PAD_MODE.I_I_INT32_INT32: vosa.ops.image_transformation.PAD_MODES.I_I_INT32_INT32,
                vosa_serialization.PAD_MODE.PAD_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.image_transformation.PAD_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.PAD_MODE.PAD_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.image_transformation.PAD_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.pad(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.PIECEWISE_LINEAR.PIECEWISE_LINEART):
            dtype_dict = {
                vosa_serialization.PIECEWISE_LINEAR_MODE.PIECEWISE_LINEAR_MODE.I_I_INT8_INT8: vosa.ops.data.PIECEWISE_LINEAR_MODES.I_I_INT8_INT8,
                vosa_serialization.PIECEWISE_LINEAR_MODE.PIECEWISE_LINEAR_MODE.I_I_INT16_INT16: vosa.ops.data.PIECEWISE_LINEAR_MODES.I_I_INT16_INT16,
                vosa_serialization.PIECEWISE_LINEAR_MODE.PIECEWISE_LINEAR_MODE.I_I_INT32_INT32: vosa.ops.data.PIECEWISE_LINEAR_MODES.I_I_INT32_INT32,
                vosa_serialization.PIECEWISE_LINEAR_MODE.PIECEWISE_LINEAR_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.data.PIECEWISE_LINEAR_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.PIECEWISE_LINEAR_MODE.PIECEWISE_LINEAR_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.data.PIECEWISE_LINEAR_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.piecewise_linear(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(
            op, vosa_serialization.REDUCE_INTERP_CHANNELWISE.REDUCE_INTERP_CHANNELWISET
        ):
            dtype_dict = {
                vosa_serialization.REDUCE_INTERP_CHANNELWISE_MODE.REDUCE_INTERP_CHANNELWISE_MODE.I_I_UINT8_FLOAT32: vosa.ops.reduction.REDUCE_INTERP_CHANNELWISE_MODES.I_I_UINT8_FLOAT32,
                vosa_serialization.REDUCE_INTERP_CHANNELWISE_MODE.REDUCE_INTERP_CHANNELWISE_MODE.I_I_UINT16_FLOAT32: vosa.ops.reduction.REDUCE_INTERP_CHANNELWISE_MODES.I_I_UINT16_FLOAT32,
                vosa_serialization.REDUCE_INTERP_CHANNELWISE_MODE.REDUCE_INTERP_CHANNELWISE_MODE.I_I_UINT32_FLOAT32: vosa.ops.reduction.REDUCE_INTERP_CHANNELWISE_MODES.I_I_UINT32_FLOAT32,
                vosa_serialization.REDUCE_INTERP_CHANNELWISE_MODE.REDUCE_INTERP_CHANNELWISE_MODE.I_I_INT8_FLOAT32: vosa.ops.reduction.REDUCE_INTERP_CHANNELWISE_MODES.I_I_INT8_FLOAT32,
                vosa_serialization.REDUCE_INTERP_CHANNELWISE_MODE.REDUCE_INTERP_CHANNELWISE_MODE.I_I_INT16_FLOAT32: vosa.ops.reduction.REDUCE_INTERP_CHANNELWISE_MODES.I_I_INT16_FLOAT32,
                vosa_serialization.REDUCE_INTERP_CHANNELWISE_MODE.REDUCE_INTERP_CHANNELWISE_MODE.I_I_INT32_FLOAT32: vosa.ops.reduction.REDUCE_INTERP_CHANNELWISE_MODES.I_I_INT32_FLOAT32,
                vosa_serialization.REDUCE_INTERP_CHANNELWISE_MODE.REDUCE_INTERP_CHANNELWISE_MODE.I_I_FLOAT16_FLOAT32: vosa.ops.reduction.REDUCE_INTERP_CHANNELWISE_MODES.I_I_FLOAT16_FLOAT32,
                vosa_serialization.REDUCE_INTERP_CHANNELWISE_MODE.REDUCE_INTERP_CHANNELWISE_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.reduction.REDUCE_INTERP_CHANNELWISE_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.reduce_interp_channelwise(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(
            op, vosa_serialization.REDUCE_MAX_CHANNELWISE.REDUCE_MAX_CHANNELWISET
        ):
            dtype_dict = {
                vosa_serialization.REDUCE_MAX_CHANNELWISE_MODE.REDUCE_MAX_CHANNELWISE_MODE.I_I_UINT8_UINT8: vosa.ops.reduction.REDUCE_MAX_CHANNELWISE_MODES.I_I_UINT8_UINT8,
                vosa_serialization.REDUCE_MAX_CHANNELWISE_MODE.REDUCE_MAX_CHANNELWISE_MODE.I_I_UINT16_UINT16: vosa.ops.reduction.REDUCE_MAX_CHANNELWISE_MODES.I_I_UINT16_UINT16,
                vosa_serialization.REDUCE_MAX_CHANNELWISE_MODE.REDUCE_MAX_CHANNELWISE_MODE.I_I_UINT32_UINT32: vosa.ops.reduction.REDUCE_MAX_CHANNELWISE_MODES.I_I_UINT32_UINT32,
                vosa_serialization.REDUCE_MAX_CHANNELWISE_MODE.REDUCE_MAX_CHANNELWISE_MODE.I_I_INT8_INT8: vosa.ops.reduction.REDUCE_MAX_CHANNELWISE_MODES.I_I_INT8_INT8,
                vosa_serialization.REDUCE_MAX_CHANNELWISE_MODE.REDUCE_MAX_CHANNELWISE_MODE.I_I_INT16_INT16: vosa.ops.reduction.REDUCE_MAX_CHANNELWISE_MODES.I_I_INT16_INT16,
                vosa_serialization.REDUCE_MAX_CHANNELWISE_MODE.REDUCE_MAX_CHANNELWISE_MODE.I_I_INT32_INT32: vosa.ops.reduction.REDUCE_MAX_CHANNELWISE_MODES.I_I_INT32_INT32,
                vosa_serialization.REDUCE_MAX_CHANNELWISE_MODE.REDUCE_MAX_CHANNELWISE_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.reduction.REDUCE_MAX_CHANNELWISE_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.REDUCE_MAX_CHANNELWISE_MODE.REDUCE_MAX_CHANNELWISE_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.reduction.REDUCE_MAX_CHANNELWISE_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.reduce_max_channelwise(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(
            op, vosa_serialization.REDUCE_MAX_PLANEWISE.REDUCE_MAX_PLANEWISET
        ):
            dtype_dict = {
                vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_S_UINT8_UINT8: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_UINT8_UINT8,
                vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_S_UINT16_UINT16: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_UINT16_UINT16,
                vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_S_UINT32_UINT32: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_UINT32_UINT32,
                vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_S_INT8_INT8: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_INT8_INT8,
                vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_S_INT16_INT16: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_INT16_INT16,
                vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_S_INT32_INT32: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_INT32_INT32,
                vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_S_FLOAT16_FLOAT16: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_FLOAT16_FLOAT16,
                vosa_serialization.REDUCE_MAX_PLANEWISE_MODE.REDUCE_MAX_PLANEWISE_MODE.I_S_FLOAT32_FLOAT32: vosa.ops.reduction.REDUCE_MAX_PLANEWISE_MODES.I_S_FLOAT32_FLOAT32,
            }
            vosa.ops.reduce_max_planewise(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(
            op, vosa_serialization.REDUCE_MIN_PLANEWISE.REDUCE_MIN_PLANEWISET
        ):
            dtype_dict = {
                vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_S_UINT8_UINT8: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_UINT8_UINT8,
                vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_S_UINT16_UINT16: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_UINT16_UINT16,
                vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_S_UINT32_UINT32: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_UINT32_UINT32,
                vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_S_INT8_INT8: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_INT8_INT8,
                vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_S_INT16_INT16: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_INT16_INT16,
                vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_S_INT32_INT32: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_INT32_INT32,
                vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_S_FLOAT16_FLOAT16: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_FLOAT16_FLOAT16,
                vosa_serialization.REDUCE_MIN_PLANEWISE_MODE.REDUCE_MIN_PLANEWISE_MODE.I_S_FLOAT32_FLOAT32: vosa.ops.reduction.REDUCE_MIN_PLANEWISE_MODES.I_S_FLOAT32_FLOAT32,
            }
            vosa.ops.reduce_min_planewise(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(
            op, vosa_serialization.REDUCE_SUM_CHANNELWISE.REDUCE_SUM_CHANNELWISET
        ):
            dtype_dict = {
                vosa_serialization.REDUCE_SUM_CHANNELWISE_MODE.REDUCE_SUM_CHANNELWISE_MODE.I_I_UINT8_UINT8: vosa.ops.reduction.REDUCE_SUM_CHANNELWISE_MODES.I_I_UINT8_UINT8,
                vosa_serialization.REDUCE_SUM_CHANNELWISE_MODE.REDUCE_SUM_CHANNELWISE_MODE.I_I_UINT16_UINT16: vosa.ops.reduction.REDUCE_SUM_CHANNELWISE_MODES.I_I_UINT16_UINT16,
                vosa_serialization.REDUCE_SUM_CHANNELWISE_MODE.REDUCE_SUM_CHANNELWISE_MODE.I_I_UINT32_UINT32: vosa.ops.reduction.REDUCE_SUM_CHANNELWISE_MODES.I_I_UINT32_UINT32,
                vosa_serialization.REDUCE_SUM_CHANNELWISE_MODE.REDUCE_SUM_CHANNELWISE_MODE.I_I_INT8_INT8: vosa.ops.reduction.REDUCE_SUM_CHANNELWISE_MODES.I_I_INT8_INT8,
                vosa_serialization.REDUCE_SUM_CHANNELWISE_MODE.REDUCE_SUM_CHANNELWISE_MODE.I_I_INT16_INT16: vosa.ops.reduction.REDUCE_SUM_CHANNELWISE_MODES.I_I_INT16_INT16,
                vosa_serialization.REDUCE_SUM_CHANNELWISE_MODE.REDUCE_SUM_CHANNELWISE_MODE.I_I_INT32_INT32: vosa.ops.reduction.REDUCE_SUM_CHANNELWISE_MODES.I_I_INT32_INT32,
                vosa_serialization.REDUCE_SUM_CHANNELWISE_MODE.REDUCE_SUM_CHANNELWISE_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.reduction.REDUCE_SUM_CHANNELWISE_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.REDUCE_SUM_CHANNELWISE_MODE.REDUCE_SUM_CHANNELWISE_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.reduction.REDUCE_SUM_CHANNELWISE_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.reduce_sum_channelwise(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(
            op, vosa_serialization.REDUCE_SUM_PLANEWISE.REDUCE_SUM_PLANEWISET
        ):
            dtype_dict = {
                vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_S_UINT8_UINT8: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_UINT8_UINT8,
                vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_S_UINT16_UINT16: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_UINT16_UINT16,
                vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_S_UINT32_UINT32: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_UINT32_UINT32,
                vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_S_INT8_INT8: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_INT8_INT8,
                vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_S_INT16_INT16: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_INT16_INT16,
                vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_S_INT32_INT32: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_INT32_INT32,
                vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_S_FLOAT16_FLOAT16: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_FLOAT16_FLOAT16,
                vosa_serialization.REDUCE_SUM_PLANEWISE_MODE.REDUCE_SUM_PLANEWISE_MODE.I_S_FLOAT32_FLOAT32: vosa.ops.reduction.REDUCE_SUM_PLANEWISE_MODES.I_S_FLOAT32_FLOAT32,
            }
            vosa.ops.reduce_sum_planewise(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(op, vosa_serialization.RESIZE_BILINEAR.RESIZE_BILINEART):
            dtype_dict = {
                vosa_serialization.RESIZE_BILINEAR_MODE.RESIZE_BILINEAR_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.image_transformation.RESIZE_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.RESIZE_BILINEAR_MODE.RESIZE_BILINEAR_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.image_transformation.RESIZE_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.resize_bilinear(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(
            op, vosa_serialization.RESIZE_NEAREST_NEIGHBOUR.RESIZE_NEAREST_NEIGHBOURT
        ):
            dtype_dict = {
                vosa_serialization.RESIZE_NEAREST_NEIGHBOUR_MODE.RESIZE_NEAREST_NEIGHBOUR_MODE.I_I_BOOL_BOOL: vosa.ops.image_transformation.RESIZE_MODES.I_I_BOOL_BOOL,
                vosa_serialization.RESIZE_NEAREST_NEIGHBOUR_MODE.RESIZE_NEAREST_NEIGHBOUR_MODE.I_I_UINT8_UINT8: vosa.ops.image_transformation.RESIZE_MODES.I_I_UINT8_UINT8,
                vosa_serialization.RESIZE_NEAREST_NEIGHBOUR_MODE.RESIZE_NEAREST_NEIGHBOUR_MODE.I_I_UINT16_UINT16: vosa.ops.image_transformation.RESIZE_MODES.I_I_UINT16_UINT16,
                vosa_serialization.RESIZE_NEAREST_NEIGHBOUR_MODE.RESIZE_NEAREST_NEIGHBOUR_MODE.I_I_UINT32_UINT32: vosa.ops.image_transformation.RESIZE_MODES.I_I_UINT32_UINT32,
                vosa_serialization.RESIZE_NEAREST_NEIGHBOUR_MODE.RESIZE_NEAREST_NEIGHBOUR_MODE.I_I_INT8_INT8: vosa.ops.image_transformation.RESIZE_MODES.I_I_INT8_INT8,
                vosa_serialization.RESIZE_NEAREST_NEIGHBOUR_MODE.RESIZE_NEAREST_NEIGHBOUR_MODE.I_I_INT16_INT16: vosa.ops.image_transformation.RESIZE_MODES.I_I_INT16_INT16,
                vosa_serialization.RESIZE_NEAREST_NEIGHBOUR_MODE.RESIZE_NEAREST_NEIGHBOUR_MODE.I_I_INT32_INT32: vosa.ops.image_transformation.RESIZE_MODES.I_I_INT32_INT32,
                vosa_serialization.RESIZE_NEAREST_NEIGHBOUR_MODE.RESIZE_NEAREST_NEIGHBOUR_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.image_transformation.RESIZE_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.RESIZE_NEAREST_NEIGHBOUR_MODE.RESIZE_NEAREST_NEIGHBOUR_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.image_transformation.RESIZE_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.resize_nearest_neighbour(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(op, vosa_serialization.SUB.SUBT):
            dtype_dict = {
                vosa_serialization.SUB_MODE.SUB_MODE.I_I_UINT8_UINT8: vosa.ops.elementwise_binary.SUB_MODES.I_I_UINT8_UINT8,
                vosa_serialization.SUB_MODE.SUB_MODE.I_I_UINT16_UINT16: vosa.ops.elementwise_binary.SUB_MODES.I_I_UINT16_UINT16,
                vosa_serialization.SUB_MODE.SUB_MODE.I_I_UINT32_UINT32: vosa.ops.elementwise_binary.SUB_MODES.I_I_UINT32_UINT32,
                vosa_serialization.SUB_MODE.SUB_MODE.I_I_INT8_INT8: vosa.ops.elementwise_binary.SUB_MODES.I_I_INT8_INT8,
                vosa_serialization.SUB_MODE.SUB_MODE.I_I_INT16_INT16: vosa.ops.elementwise_binary.SUB_MODES.I_I_INT16_INT16,
                vosa_serialization.SUB_MODE.SUB_MODE.I_I_INT32_INT32: vosa.ops.elementwise_binary.SUB_MODES.I_I_INT32_INT32,
                vosa_serialization.SUB_MODE.SUB_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.elementwise_binary.SUB_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.SUB_MODE.SUB_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.elementwise_binary.SUB_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.sub(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.WHERE.WHERET):
            dtype_dict = {
                vosa_serialization.WHERE_MODE.WHERE_MODE.I_I_UINT8_UINT8: vosa.ops.data.WHERE_MODES.I_I_UINT8_UINT8,
                vosa_serialization.WHERE_MODE.WHERE_MODE.I_I_UINT16_UINT16: vosa.ops.data.WHERE_MODES.I_I_UINT16_UINT16,
                vosa_serialization.WHERE_MODE.WHERE_MODE.I_I_UINT32_UINT32: vosa.ops.data.WHERE_MODES.I_I_UINT32_UINT32,
                vosa_serialization.WHERE_MODE.WHERE_MODE.I_I_INT8_INT8: vosa.ops.data.WHERE_MODES.I_I_INT8_INT8,
                vosa_serialization.WHERE_MODE.WHERE_MODE.I_I_INT16_INT16: vosa.ops.data.WHERE_MODES.I_I_INT16_INT16,
                vosa_serialization.WHERE_MODE.WHERE_MODE.I_I_INT32_INT32: vosa.ops.data.WHERE_MODES.I_I_INT32_INT32,
                vosa_serialization.WHERE_MODE.WHERE_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.data.WHERE_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.WHERE_MODE.WHERE_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.data.WHERE_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.where(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.MESH_GRID.MESH_GRIDT):
            dtype_dict = {
                vosa_serialization.MESH_GRID_MODE.MESH_GRID_MODE.I_UINT32: vosa.ops.data.MESH_GRID_EXECUTION_MODES.I_UINT32
            }
            vosa.ops.mesh_grid(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.MOD.MODT):
            dtype_dict = {
                vosa_serialization.MOD_MODE.MOD_MODE.I_I_UINT8_UINT8: vosa.ops.elementwise_binary.MOD_MODES.I_I_UINT8_UINT8,
                vosa_serialization.MOD_MODE.MOD_MODE.I_I_UINT16_UINT16: vosa.ops.elementwise_binary.MOD_MODES.I_I_UINT16_UINT16,
                vosa_serialization.MOD_MODE.MOD_MODE.I_I_UINT32_UINT32: vosa.ops.elementwise_binary.MOD_MODES.I_I_UINT32_UINT32,
                vosa_serialization.MOD_MODE.MOD_MODE.I_I_INT8_INT8: vosa.ops.elementwise_binary.MOD_MODES.I_I_INT8_INT8,
                vosa_serialization.MOD_MODE.MOD_MODE.I_I_INT16_INT16: vosa.ops.elementwise_binary.MOD_MODES.I_I_INT16_INT16,
                vosa_serialization.MOD_MODE.MOD_MODE.I_I_INT32_INT32: vosa.ops.elementwise_binary.MOD_MODES.I_I_INT32_INT32,
                vosa_serialization.MOD_MODE.MOD_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.elementwise_binary.MOD_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.MOD_MODE.MOD_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.elementwise_binary.MOD_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.mod(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.EQUAL.EQUALT):
            dtype_dict = {
                vosa_serialization.EQUAL_MODE.EQUAL_MODE.I_I_UINT8_BOOL: vosa.ops.comp.EQUAL_MODES.I_I_UINT8_BOOL,
                vosa_serialization.EQUAL_MODE.EQUAL_MODE.I_I_UINT16_BOOL: vosa.ops.comp.EQUAL_MODES.I_I_UINT16_BOOL,
                vosa_serialization.EQUAL_MODE.EQUAL_MODE.I_I_UINT32_BOOL: vosa.ops.comp.EQUAL_MODES.I_I_UINT32_BOOL,
                vosa_serialization.EQUAL_MODE.EQUAL_MODE.I_I_INT8_BOOL: vosa.ops.comp.EQUAL_MODES.I_I_INT8_BOOL,
                vosa_serialization.EQUAL_MODE.EQUAL_MODE.I_I_INT16_BOOL: vosa.ops.comp.EQUAL_MODES.I_I_INT16_BOOL,
                vosa_serialization.EQUAL_MODE.EQUAL_MODE.I_I_INT32_BOOL: vosa.ops.comp.EQUAL_MODES.I_I_INT32_BOOL,
            }
            vosa.ops.equals(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.GAMMA_CORRECTION.GAMMA_CORRECTIONT):
            dtype_dict = {
                vosa_serialization.GAMMA_CORRECTION_MODE.GAMMA_CORRECTION_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.elementwise_unary.GAMMA_CORRECTION_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.GAMMA_CORRECTION_MODE.GAMMA_CORRECTION_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.elementwise_unary.GAMMA_CORRECTION_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.gamma_correction(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(op, vosa_serialization.GREATER.GREATERT):
            dtype_dict = {
                vosa_serialization.GREATER_MODE.GREATER_MODE.I_I_UINT8_BOOL: vosa.ops.comp.GREATER_MODES.I_I_UINT8_BOOL,
                vosa_serialization.GREATER_MODE.GREATER_MODE.I_I_UINT16_BOOL: vosa.ops.comp.GREATER_MODES.I_I_UINT16_BOOL,
                vosa_serialization.GREATER_MODE.GREATER_MODE.I_I_UINT32_BOOL: vosa.ops.comp.GREATER_MODES.I_I_UINT32_BOOL,
                vosa_serialization.GREATER_MODE.GREATER_MODE.I_I_INT8_BOOL: vosa.ops.comp.GREATER_MODES.I_I_INT8_BOOL,
                vosa_serialization.GREATER_MODE.GREATER_MODE.I_I_INT16_BOOL: vosa.ops.comp.GREATER_MODES.I_I_INT16_BOOL,
                vosa_serialization.GREATER_MODE.GREATER_MODE.I_I_INT32_BOOL: vosa.ops.comp.GREATER_MODES.I_I_INT32_BOOL,
                vosa_serialization.GREATER_MODE.GREATER_MODE.I_I_FLOAT16_BOOL: vosa.ops.comp.GREATER_MODES.I_I_FLOAT16_BOOL,
                vosa_serialization.GREATER_MODE.GREATER_MODE.I_I_FLOAT32_BOOL: vosa.ops.comp.GREATER_MODES.I_I_FLOAT32_BOOL,
            }
            vosa.ops.greater_than(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(op, vosa_serialization.GREATER_EQUAL.GREATER_EQUALT):
            dtype_dict = {
                vosa_serialization.GREATER_EQUAL_MODE.GREATER_EQUAL_MODE.I_I_UINT8_BOOL: vosa.ops.comp.GREATER_EQUAL_MODES.I_I_UINT8_BOOL,
                vosa_serialization.GREATER_EQUAL_MODE.GREATER_EQUAL_MODE.I_I_UINT16_BOOL: vosa.ops.comp.GREATER_EQUAL_MODES.I_I_UINT16_BOOL,
                vosa_serialization.GREATER_EQUAL_MODE.GREATER_EQUAL_MODE.I_I_UINT32_BOOL: vosa.ops.comp.GREATER_EQUAL_MODES.I_I_UINT32_BOOL,
                vosa_serialization.GREATER_EQUAL_MODE.GREATER_EQUAL_MODE.I_I_INT8_BOOL: vosa.ops.comp.GREATER_EQUAL_MODES.I_I_INT8_BOOL,
                vosa_serialization.GREATER_EQUAL_MODE.GREATER_EQUAL_MODE.I_I_INT16_BOOL: vosa.ops.comp.GREATER_EQUAL_MODES.I_I_INT16_BOOL,
                vosa_serialization.GREATER_EQUAL_MODE.GREATER_EQUAL_MODE.I_I_INT32_BOOL: vosa.ops.comp.GREATER_EQUAL_MODES.I_I_INT32_BOOL,
                vosa_serialization.GREATER_EQUAL_MODE.GREATER_EQUAL_MODE.I_I_FLOAT16_BOOL: vosa.ops.comp.GREATER_EQUAL_MODES.I_I_FLOAT16_BOOL,
                vosa_serialization.GREATER_EQUAL_MODE.GREATER_EQUAL_MODE.I_I_FLOAT32_BOOL: vosa.ops.comp.GREATER_EQUAL_MODES.I_I_FLOAT32_BOOL,
            }
            vosa.ops.greater_equal(
                *input_ops, operator_mode=dtype_dict[op.mode], name=name
            )
        elif isinstance(op, vosa_serialization.OR.ORT):
            dtype_dict = {
                vosa_serialization.OR_MODE.OR_MODE.I_I_BOOL_BOOL: vosa.ops.elementwise_binary.OR_MODES.I_I_BOOL_BOOL
            }
            vosa.ops.or_(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.NOT.NOTT):
            dtype_dict = {
                vosa_serialization.NOT_MODE.NOT_MODE.I_I_BOOL_BOOL: vosa.ops.elementwise_unary.NOT_MODES.I_I_BOOL_BOOL
            }
            vosa.ops.not_(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.POWER.POWERT):
            dtype_dict = {
                vosa_serialization.POWER_MODE.POWER_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.elementwise_unary.POWER_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.POWER_MODE.POWER_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.elementwise_unary.POWER_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.power(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.ROTATE_90.ROTATE_90T):
            dtype_dict = {
                vosa_serialization.ROTATE_90_MODE.ROTATE_90_MODE.I_I_UINT8_UINT8: vosa.ops.image_transformation.ROTATE_90_MODES.I_I_UINT8_UINT8,
                vosa_serialization.ROTATE_90_MODE.ROTATE_90_MODE.I_I_UINT16_UINT16: vosa.ops.image_transformation.ROTATE_90_MODES.I_I_UINT16_UINT16,
                vosa_serialization.ROTATE_90_MODE.ROTATE_90_MODE.I_I_UINT32_UINT32: vosa.ops.image_transformation.ROTATE_90_MODES.I_I_UINT32_UINT32,
                vosa_serialization.ROTATE_90_MODE.ROTATE_90_MODE.I_I_INT8_INT8: vosa.ops.image_transformation.ROTATE_90_MODES.I_I_INT8_INT8,
                vosa_serialization.ROTATE_90_MODE.ROTATE_90_MODE.I_I_INT16_INT16: vosa.ops.image_transformation.ROTATE_90_MODES.I_I_INT16_INT16,
                vosa_serialization.ROTATE_90_MODE.ROTATE_90_MODE.I_I_INT32_INT32: vosa.ops.image_transformation.ROTATE_90_MODES.I_I_INT32_INT32,
            }
            vosa.ops.rotate90(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.ROUND.ROUNDT):
            dtype_dict = {
                vosa_serialization.ROUND_MODE.ROUND_MODE.I_I_FLOAT16_INT32: vosa.ops.elementwise_unary.ROUND_MODES.I_I_FLOAT16_INT32,
                vosa_serialization.ROUND_MODE.ROUND_MODE.I_I_FLOAT32_INT32: vosa.ops.elementwise_unary.ROUND_MODES.I_I_FLOAT32_INT32,
            }
            vosa.ops.round(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.SQRT.SQRTT):
            dtype_dict = {
                vosa_serialization.SQRT_MODE.SQRT_MODE.I_I_FLOAT16_FLOAT16: vosa.ops.elementwise_unary.SQRT_MODES.I_I_FLOAT16_FLOAT16,
                vosa_serialization.SQRT_MODE.SQRT_MODE.I_I_FLOAT32_FLOAT32: vosa.ops.elementwise_unary.SQRT_MODES.I_I_FLOAT32_FLOAT32,
            }
            vosa.ops.sqrt(*input_ops, operator_mode=dtype_dict[op.mode], name=name)
        elif isinstance(op, vosa_serialization.TOSA_GRAPH.TOSA_GRAPHT):
            dtype_dict = {
                vosa_serialization.DTYPE.DTYPE.UINT8: vosa.types.DType.UINT8,
                vosa_serialization.DTYPE.DTYPE.UINT16: vosa.types.DType.UINT16,
                vosa_serialization.DTYPE.DTYPE.UINT32: vosa.types.DType.UINT32,
                vosa_serialization.DTYPE.DTYPE.INT8: vosa.types.DType.INT8,
                vosa_serialization.DTYPE.DTYPE.INT16: vosa.types.DType.INT16,
                vosa_serialization.DTYPE.DTYPE.INT32: vosa.types.DType.INT32,
                vosa_serialization.DTYPE.DTYPE.FLOAT16: vosa.types.DType.FLOAT16,
                vosa_serialization.DTYPE.DTYPE.FLOAT32: vosa.types.DType.FLOAT32,
            }
            vosa.ops.tosa_graph(
                *input_ops, output_dtype=dtype_dict[op.outputType], name=name
            )
        else:
            raise RuntimeError()

    @staticmethod
    def __get_op_type_from_serialized(serialized_op):
        import vosa_serialization.VosaOperator

        import vosa_serialization.INPUT
        import vosa_serialization.ABS
        import vosa_serialization.ABS_DIFF
        import vosa_serialization.ADD
        import vosa_serialization.ARITHMETIC_SHIFT_RIGHT
        import vosa_serialization.ARG_MIN_CHANNELWISE
        import vosa_serialization.ARG_MIN_PLANEWISE
        import vosa_serialization.ARG_MAX_PLANEWISE
        import vosa_serialization.BROADCAST_CHANNELWISE
        import vosa_serialization.BROADCAST_SCALAR
        import vosa_serialization.CAST
        import vosa_serialization.CHANNEL_EXTRACT
        import vosa_serialization.CLAMP
        import vosa_serialization.CONCAT
        import vosa_serialization.CONST_IMAGE
        import vosa_serialization.CONST_PLANE
        import vosa_serialization.CONST_SCALAR
        import vosa_serialization.CONV_2D
        import vosa_serialization.CUSTOM_SCALAR
        import vosa_serialization.DECIMATE
        import vosa_serialization.DIV
        import vosa_serialization.EQUAL
        import vosa_serialization.EXPAND
        import vosa_serialization.IMPORT_CHANNEL
        import vosa_serialization.GAMMA_CORRECTION
        import vosa_serialization.GREATER
        import vosa_serialization.GREATER_EQUAL
        import vosa_serialization.LOGICAL_SHIFT_RIGHT
        import vosa_serialization.MESH_GRID
        import vosa_serialization.MAX_
        import vosa_serialization.MIN_
        import vosa_serialization.MOD
        import vosa_serialization.MULT
        import vosa_serialization.NOT
        import vosa_serialization.OR
        import vosa_serialization.PAD
        import vosa_serialization.POWER
        import vosa_serialization.PIECEWISE_LINEAR
        import vosa_serialization.POINTWISE_MATRIX_MULTIPLY
        import vosa_serialization.REDUCE_INTERP_CHANNELWISE
        import vosa_serialization.REDUCE_MAX_CHANNELWISE
        import vosa_serialization.REDUCE_MAX_PLANEWISE
        import vosa_serialization.REDUCE_MIN_PLANEWISE
        import vosa_serialization.REDUCE_SUM_CHANNELWISE
        import vosa_serialization.REDUCE_SUM_PLANEWISE
        import vosa_serialization.RESIZE_BILINEAR
        import vosa_serialization.RESIZE_NEAREST_NEIGHBOUR
        import vosa_serialization.ROTATE_90
        import vosa_serialization.ROUND
        import vosa_serialization.SQRT
        import vosa_serialization.TOSA_GRAPH
        import vosa_serialization.SUB
        import vosa_serialization.WHERE

        op_types = {
            vosa_serialization.INPUT.INPUTT: vosa_serialization.VosaOperator.VosaOperator().INPUT,
            vosa_serialization.ABS.ABST: vosa_serialization.VosaOperator.VosaOperator().ABS,
            vosa_serialization.ABS_DIFF.ABS_DIFFT: vosa_serialization.VosaOperator.VosaOperator().ABS_DIFF,
            vosa_serialization.ADD.ADDT: vosa_serialization.VosaOperator.VosaOperator().ADD,
            vosa_serialization.ARITHMETIC_SHIFT_RIGHT.ARITHMETIC_SHIFT_RIGHTT: vosa_serialization.VosaOperator.VosaOperator().ARITHMETIC_SHIFT_RIGHT,
            vosa_serialization.ARG_MIN_CHANNELWISE.ARG_MIN_CHANNELWISET: vosa_serialization.VosaOperator.VosaOperator().ARG_MIN_CHANNELWISE,
            vosa_serialization.ARG_MIN_PLANEWISE.ARG_MIN_PLANEWISET: vosa_serialization.VosaOperator.VosaOperator().ARG_MIN_PLANEWISE,
            vosa_serialization.ARG_MAX_PLANEWISE.ARG_MAX_PLANEWISET: vosa_serialization.VosaOperator.VosaOperator().ARG_MAX_PLANEWISE,
            vosa_serialization.BROADCAST_CHANNELWISE.BROADCAST_CHANNELWISET: vosa_serialization.VosaOperator.VosaOperator().BROADCAST_CHANNELWISE,
            vosa_serialization.BROADCAST_SCALAR.BROADCAST_SCALART: vosa_serialization.VosaOperator.VosaOperator().BROADCAST_PLANEWISE,
            vosa_serialization.CAST.CASTT: vosa_serialization.VosaOperator.VosaOperator().CAST,
            vosa_serialization.CHANNEL_EXTRACT.CHANNEL_EXTRACTT: vosa_serialization.VosaOperator.VosaOperator().CHANNEL_EXTRACT,
            vosa_serialization.CLAMP.CLAMPT: vosa_serialization.VosaOperator.VosaOperator().CLAMP,
            vosa_serialization.POINTWISE_MATRIX_MULTIPLY.POINTWISE_MATRIX_MULTIPLYT: vosa_serialization.VosaOperator.VosaOperator().POINTWISE_MATRIX_MULTIPLY,
            vosa_serialization.CONCAT.CONCATT: vosa_serialization.VosaOperator.VosaOperator().CONCAT,
            vosa_serialization.CONST_IMAGE.CONST_IMAGET: vosa_serialization.VosaOperator.VosaOperator().CONST_IMAGE,
            vosa_serialization.CONST_PLANE.CONST_PLANET: vosa_serialization.VosaOperator.VosaOperator().CONST_PLANE,
            vosa_serialization.CONST_SCALAR.CONST_SCALART: vosa_serialization.VosaOperator.VosaOperator().CONST_SCALAR,
            vosa_serialization.CONV_2D.CONV_2DT: vosa_serialization.VosaOperator.VosaOperator().CONV_2D,
            vosa_serialization.CUSTOM_SCALAR.CUSTOM_SCALART: vosa_serialization.VosaOperator.VosaOperator().CUSTOM_SCALAR,
            vosa_serialization.DECIMATE.DECIMATET: vosa_serialization.VosaOperator.VosaOperator().DECIMATE,
            vosa_serialization.DIV.DIVT: vosa_serialization.VosaOperator.VosaOperator().DIV,
            vosa_serialization.EQUAL.EQUALT: vosa_serialization.VosaOperator.VosaOperator().EQUAL,
            vosa_serialization.EXPAND.EXPANDT: vosa_serialization.VosaOperator.VosaOperator().EXPAND,
            vosa_serialization.IMPORT_CHANNEL.IMPORT_CHANNELT: vosa_serialization.VosaOperator.VosaOperator().IMPORT_CHANNEL,
            vosa_serialization.GAMMA_CORRECTION.GAMMA_CORRECTIONT: vosa_serialization.VosaOperator.VosaOperator().GAMMA_CORRECTION,
            vosa_serialization.GREATER.GREATERT: vosa_serialization.VosaOperator.VosaOperator().GREATER,
            vosa_serialization.GREATER_EQUAL.GREATER_EQUALT: vosa_serialization.VosaOperator.VosaOperator().GREATER_EQUAL,
            vosa_serialization.LOGICAL_SHIFT_RIGHT.LOGICAL_SHIFT_RIGHTT: vosa_serialization.VosaOperator.VosaOperator().LOGICAL_SHIFT_RIGHT,
            vosa_serialization.MESH_GRID.MESH_GRIDT: vosa_serialization.VosaOperator.VosaOperator().MESH_GRID,
            vosa_serialization.MAX_.MAX_T: vosa_serialization.VosaOperator.VosaOperator().MAX_,
            vosa_serialization.MIN_.MIN_T: vosa_serialization.VosaOperator.VosaOperator().MIN_,
            vosa_serialization.MOD.MODT: vosa_serialization.VosaOperator.VosaOperator().MOD,
            vosa_serialization.MULT.MULTT: vosa_serialization.VosaOperator.VosaOperator().MULT,
            vosa_serialization.NOT.NOTT: vosa_serialization.VosaOperator.VosaOperator().NOT,
            vosa_serialization.OR.ORT: vosa_serialization.VosaOperator.VosaOperator().OR,
            vosa_serialization.PAD.PADT: vosa_serialization.VosaOperator.VosaOperator().PAD,
            vosa_serialization.POWER.POWERT: vosa_serialization.VosaOperator.VosaOperator().POWER,
            vosa_serialization.PIECEWISE_LINEAR.PIECEWISE_LINEART: vosa_serialization.VosaOperator.VosaOperator().PIECEWISE_LINEAR,
            vosa_serialization.REDUCE_INTERP_CHANNELWISE.REDUCE_INTERP_CHANNELWISET: vosa_serialization.VosaOperator.VosaOperator().REDUCE_INTERP_CHANNELWISE,
            vosa_serialization.REDUCE_MAX_CHANNELWISE.REDUCE_MAX_CHANNELWISET: vosa_serialization.VosaOperator.VosaOperator().REDUCE_MAX_CHANNELWISE,
            vosa_serialization.REDUCE_MAX_PLANEWISE.REDUCE_MAX_PLANEWISET: vosa_serialization.VosaOperator.VosaOperator().REDUCE_MAX_PLANEWISE,
            vosa_serialization.REDUCE_MIN_PLANEWISE.REDUCE_MIN_PLANEWISET: vosa_serialization.VosaOperator.VosaOperator().REDUCE_MIN_PLANEWISE,
            vosa_serialization.REDUCE_SUM_CHANNELWISE.REDUCE_SUM_CHANNELWISET: vosa_serialization.VosaOperator.VosaOperator().REDUCE_SUM_CHANNELWISE,
            vosa_serialization.REDUCE_SUM_PLANEWISE.REDUCE_SUM_PLANEWISET: vosa_serialization.VosaOperator.VosaOperator().REDUCE_SUM_PLANEWISE,
            vosa_serialization.RESIZE_BILINEAR.RESIZE_BILINEART: vosa_serialization.VosaOperator.VosaOperator().RESIZE_BILINEAR,
            vosa_serialization.RESIZE_NEAREST_NEIGHBOUR.RESIZE_NEAREST_NEIGHBOURT: vosa_serialization.VosaOperator.VosaOperator().RESIZE_NEAREST_NEIGHBOUR,
            vosa_serialization.ROTATE_90.ROTATE_90T: vosa_serialization.VosaOperator.VosaOperator().ROTATE_90,
            vosa_serialization.ROUND.ROUNDT: vosa_serialization.VosaOperator.VosaOperator().ROUND,
            vosa_serialization.SQRT.SQRTT: vosa_serialization.VosaOperator.VosaOperator().SQRT,
            vosa_serialization.SUB.SUBT: vosa_serialization.VosaOperator.VosaOperator().SUB,
            vosa_serialization.TOSA_GRAPH.TOSA_GRAPHT: vosa_serialization.VosaOperator.VosaOperator().TOSA_GRAPH,
            vosa_serialization.WHERE.WHERET: vosa_serialization.VosaOperator.VosaOperator().WHERE,
        }

        op_type = type(serialized_op)

        return op_types[op_type]

    @staticmethod
    def from_flatbuffer(flatbuffer_file):
        import vosa_serialization.VosaGraph as serialized_vosa_graph

        with open(flatbuffer_file, "rb") as fp:
            buffer = fp.read()
        serialized_graph = serialized_vosa_graph.VosaGraph.GetRootAsVosaGraph(buffer, 0)
        serialized_grapht = serialized_vosa_graph.VosaGraphT.InitFromObj(
            serialized_graph
        )

        processed = set()
        all_op_names = set([op.name.decode() for op in serialized_grapht.operators])
        graph = Graph()
        with graph.as_default():
            can_process = True
            while can_process:
                can_process = False
                for operator in serialized_grapht.operators:
                    operator_name = operator.name.decode()
                    if operator_name in processed:
                        continue
                    can_process_operator = False
                    if operator.inputs is not None:
                        operator_input_names = [inp.decode() for inp in operator.inputs]
                        inputs_processed = [
                            inp_name in processed for inp_name in operator_input_names
                        ]
                        if all(inputs_processed):
                            can_process_operator = True
                    else:
                        can_process_operator = True
                    if can_process_operator:
                        can_process = True
                        processed.add(operator_name)
                        if operator.inputs:
                            decoded_inputs = [inp.decode() for inp in operator.inputs]
                        else:
                            decoded_inputs = []
                        Graph.__create_op_from_serialized(
                            operator.op, decoded_inputs, operator.name.decode()
                        )
            assert processed == all_op_names
        return graph

    def __get_name_for_op(self, op):
        for op_name, graph_op in self.__ops.items():
            if op == graph_op:
                return op_name
        raise RuntimeError()

    def to_flatbuffer(self):
        import vosa_serialization.VosaGraph
        import vosa_serialization.VosaGraphOperator

        flatbuffer_graph = vosa_serialization.VosaGraph.VosaGraphT()
        flatbuffer_graph.operators = []
        for op_name, op in self.__ops.items():
            op_flatbuffer = op.to_flatbuffer()
            vosa_graph_operator = (
                vosa_serialization.VosaGraphOperator.VosaGraphOperatorT()
            )
            vosa_graph_operator.op = op_flatbuffer
            vosa_graph_operator.opType = Graph.__get_op_type_from_serialized(
                op_flatbuffer
            )
            vosa_graph_operator.name = op_name
            vosa_graph_operator.inputs = [
                self.__get_name_for_op(operand) for operand in op.get_operands()
            ]
            flatbuffer_graph.operators.append(vosa_graph_operator)
        return flatbuffer_graph

    def write_flatbuffer(self, filepath) -> None:
        import flatbuffers

        # Serialise Graph
        flatbuffer_graph = self.to_flatbuffer()
        fbb = flatbuffers.Builder(initialSize=1024)
        packed = flatbuffer_graph.Pack(fbb)
        fbb.Finish(packed)
        buffer = fbb.Output()

        # Save to file
        f = open(filepath, "wb")
        f.write(buffer)
        f.close()

    def allow_failed_validation(self):
        return self.__allow_failed_validation

    def __enter__(self):
        _graph_stack.put(self)

    def __exit__(self, exc_type, exc_val, exc_tb):
        _graph_stack.pop()

    def as_default(self):
        return self

    def add_op(self, op, name=None):
        auto_name = "Op_" + str(len(self.__ops))
        name_to_use = name if name else auto_name
        op_name = self.__op_name_stack.get_name(name_to_use)
        while op_name in self.__ops:
            op_name += "I"
        assert op_name not in self.__ops
        self.__ops[op_name] = op

    def ops(self):
        return self.__ops

    def add_name(self, name: str):
        self.__op_name_stack.put(name)
        self.__data_name_stack.put(name)

    def remove_name(self):
        self.__op_name_stack.pop()
        self.__data_name_stack.pop()

    @staticmethod
    def __make_tiered_dict(flat_dict):
        nested_dict = {}
        for name, label in flat_dict.items():
            split_name = name.split("/")

            working_dict = nested_dict
            while len(split_name) > 1:
                scope = split_name.pop(0)
                try:
                    working_dict = working_dict[scope]
                except KeyError:
                    working_dict[scope] = {}
                    working_dict = working_dict[scope]
            working_dict[split_name[0]] = label
        return nested_dict

    @staticmethod
    def __add_nested_dict_to_digraph(g, nested_dict, prepend=""):
        for key, val in nested_dict.items():
            if isinstance(val, str):
                g.node(prepend + key, val)
            else:
                with g.subgraph(name="cluster" + key) as c:
                    c.attr(label=key)
                    Graph.__add_nested_dict_to_digraph(
                        c, val, prepend=prepend + key + "/"
                    )

    def dot(self, path=None):
        g = graphviz.Digraph(node_attr={"shape": "plaintext"})

        op_name_dict = {}
        flat_op_label_dict = {}
        for op_name, op in self.__ops.items():
            op_name_dict[op] = op_name
            label = op.make_label(op_name)
            flat_op_label_dict[op_name] = label

        nested_op_dict = Graph.__make_tiered_dict(flat_op_label_dict)
        Graph.__add_nested_dict_to_digraph(g, nested_op_dict)

        for op in self.__ops.values():
            for operand in op.get_operands():
                g.edge(op_name_dict[operand], op_name_dict[op])

        if path:
            g.render(path)
            g.render(path, format="svg")
        return g


class GraphOpList:
    def __init__(self):
        self.op_dict = {}

    @classmethod
    def from_pipeline(cls, pipeline_name: str, graph: Graph):
        self = cls()
        self.__add_pipeline(pipeline_name, graph)
        return self

    def __add_pipeline(self, pipeline_name: str, graph: Graph):
        for op in graph.ops().values():
            op_class = op.__class__.__name__
            try:
                op_dict = self.op_dict[op_class]
            except KeyError:
                self.op_dict[op_class] = {}
                op_dict = self.op_dict[op_class]

            op_operator_mode = op.get_operator_mode().name
            try:
                op_mode_dict = op_dict[op_operator_mode]
            except KeyError:
                op_dict[op_operator_mode] = {}
                op_mode_dict = op_dict[op_operator_mode]

            op_attributes = str(op.get_attributes())

            try:
                op_attribute_dict = op_mode_dict[op_attributes]
            except KeyError:
                op_mode_dict[op_attributes] = {}
                op_attribute_dict = op_mode_dict[op_attributes]

            try:
                op_attribute_dict[pipeline_name] += 1
            except KeyError:
                op_attribute_dict[pipeline_name] = 1

    def __add__(self, other):
        output = copy.deepcopy(self)

        for op_name, other_op_dict in other.op_dict.items():
            try:
                op_dict = output.op_dict[op_name]
            except KeyError:
                output.op_dict[op_name] = other_op_dict
                continue
            for op_mode, other_op_mode_dict in other_op_dict.items():
                try:
                    op_mode_dict = op_dict[op_mode]
                except KeyError:
                    op_dict[op_mode] = other_op_mode_dict
                    continue
                for attribute_str, attribute_dict in other_op_mode_dict.items():
                    try:
                        op_attribute_dict = op_mode_dict[attribute_str]
                    except KeyError:
                        op_mode_dict[attribute_str] = attribute_dict
                        continue
                    for pipeline_name, pipeline_value in attribute_dict.items():
                        try:
                            op_attribute_dict[pipeline_name] += pipeline_value
                        except KeyError:
                            op_attribute_dict[pipeline_name] = pipeline_value
        return output

    def table(self):
        rows = []
        for op_name, op_dict in self.op_dict.items():
            if op_name == vosa.op.INPUT.__name__:
                continue
            for op_mode, op_mode_dict in op_dict.items():
                for op_attributes, op_attributes_dict in op_mode_dict.items():
                    incidence_strs = []
                    for pipeline_name, count in op_attributes_dict.items():
                        this_incidence_str = pipeline_name + "(" + str(count) + ")"
                        incidence_strs.append(this_incidence_str)
                    incidence_str = ", ".join(incidence_strs)
                    row_str = [op_name, op_mode, op_attributes, incidence_str]
                    rows.append(row_str)
        rows.sort(key=lambda x: x[0] + x[1] + x[2])
        output = tabulate(rows, headers=["Op", "Mode", "Attributes", "Incidences"])

        return output

    def __repr__(self):
        return self.table()
