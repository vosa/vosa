/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "bluedog.h"
#include "vosa/api/sample.h"

class BlueDogSample : public vosa::api::sample::SampleBuilder<BlueDogSample> {

public:
  int run(const std::string &pipeline_name,
          const std::string &vmfb_file) final {
    vosa::api::GraphBuilder graph_builder("", pipeline_name);

    auto outputs_ops = vosa::samples::bluedog::make_test_graph(graph_builder);

    std::vector<std::string> output_op_names = {
        graph_builder.get_name_for_op(outputs_ops[0])};
    return graph_builder.compile(output_op_names, vmfb_file);
  }
};

VOSA_SAMPLE(BlueDogSample)