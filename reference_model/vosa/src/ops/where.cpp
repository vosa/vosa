/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "where.h"

std::shared_ptr<vosa::Op> vosa::WhereBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_where = vosa_operator_union.AsWHERE();
  assert(serialized_where);
  auto mode = serialized_where->mode;
  switch (mode) {
  case vosa_serialization::WHERE_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::Where<uint8_t>>();
  }
  case vosa_serialization::WHERE_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::Where<uint16_t>>();
  }
  case vosa_serialization::WHERE_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::Where<uint32_t>>();
  }
  case vosa_serialization::WHERE_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::Where<int8_t>>();
  }
  case vosa_serialization::WHERE_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::Where<int16_t>>();
  }
  case vosa_serialization::WHERE_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::Where<int32_t>>();
  }
  case vosa_serialization::WHERE_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::Where<float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::ArrayBase::DType vosa::Where<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::Where<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {
  const EigenImage<T> &input_0 = inputs[0]->template get_image<T>();
  const EigenImage<T> &input_1 = inputs[1]->template get_image<T>();
  const EigenImage<bool> &mask = inputs[2]->template get_image<bool>();

  uint32_t height = input_0.dimension(0);
  uint32_t width = input_0.dimension(1);
  uint32_t channels = input_0.dimension(2);

  uint32_t height_1 = input_1.dimension(0);
  uint32_t width_1 = input_1.dimension(1);
  uint32_t channels_1 = input_1.dimension(2);

  assert(height == height_1);
  assert(width == width_1);
  assert(channels == channels_1);

  uint32_t mask_height = mask.dimension(0);
  uint32_t mask_width = mask.dimension(1);
  uint32_t mask_channels = mask.dimension(2);

  assert(height == mask_height);
  assert(width == mask_width);
  assert(channels == mask_channels);

  auto output_image = EigenImage<T>(height, width, channels);

  for (uint32_t height_index = 0; height_index < height; height_index++) {
    for (uint32_t width_index = 0; width_index < width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < channels;
           channel_index++) {
        bool use_input = mask(height_index, width_index, channel_index);
        if (use_input) {
          output_image(height_index, width_index, channel_index) =
              input_0(height_index, width_index, channel_index);
        } else {
          output_image(height_index, width_index, channel_index) =
              input_1(height_index, width_index, channel_index);
        }
      }
    }
  }

  return std::make_shared<vosa::ArrayBase>(output_image);
}

template <>
vosa_serialization::WHERE_MODE
vosa::Where<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::WHERE_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::WHERE_MODE
vosa::Where<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::WHERE_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::WHERE_MODE
vosa::Where<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::WHERE_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::WHERE_MODE
vosa::Where<int8_t>::get_serialization_mode() const {
  return vosa_serialization::WHERE_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::WHERE_MODE
vosa::Where<int16_t>::get_serialization_mode() const {
  return vosa_serialization::WHERE_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::WHERE_MODE
vosa::Where<int32_t>::get_serialization_mode() const {
  return vosa_serialization::WHERE_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::WHERE_MODE
vosa::Where<float>::get_serialization_mode() const {
  return vosa_serialization::WHERE_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Where<T>::serialize(const std::string &op_name,
                          const std::vector<std::string> &input_names) const {

  vosa_serialization::WHERET where;
  where.mode = get_serialization_mode();

  auto where_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  where_operator->op.Set(std::move(where));
  where_operator->name = op_name;
  where_operator->inputs = input_names;
  return where_operator;
}

template class vosa::Where<uint8_t>;

template class vosa::Where<uint16_t>;

template class vosa::Where<uint32_t>;

template class vosa::Where<int8_t>;

template class vosa::Where<int16_t>;

template class vosa::Where<int32_t>;

template class vosa::Where<float>;