/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "mult.h"

std::shared_ptr<vosa::Op> vosa::MultBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_mult = vosa_operator_union.AsMULT();
  assert(serialized_mult);
  auto mode = serialized_mult->mode;
  switch (mode) {
  case vosa_serialization::MULT_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::Mult<uint8_t, uint8_t>>();
  }
  case vosa_serialization::MULT_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::Mult<uint16_t, uint16_t>>();
  }
  case vosa_serialization::MULT_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::Mult<uint32_t, uint32_t>>();
  }
  case vosa_serialization::MULT_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::Mult<int8_t, int8_t>>();
  }
  case vosa_serialization::MULT_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::Mult<int16_t, int16_t>>();
  }
  case vosa_serialization::MULT_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::Mult<int32_t, int32_t>>();
  }
  case vosa_serialization::MULT_MODE_I_I_FLOAT32_FLOAT32: {
    return std::make_shared<vosa::Mult<float, float>>();
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename inputT, typename outputT>
vosa::EigenImage<outputT> vosa::Mult<inputT, outputT>::runme(
    const vosa::EigenImage<inputT> &input_1,
    const vosa::EigenImage<inputT> &input_2) const {
  EigenImage<outputT> input_1_as_output = input_1.template cast<outputT>();
  EigenImage<outputT> input_2_as_output = input_2.template cast<outputT>();

  const uint32_t height = input_1_as_output.dimension(0);
  const uint32_t width = input_1_as_output.dimension(1);
  const uint32_t channels = input_1_as_output.dimension(2);

  assert(height == input_2_as_output.dimension(0));
  assert(width == input_2_as_output.dimension(1));
  assert(channels == input_2_as_output.dimension(2));

  EigenImage<outputT> output(height, width, channels);

  for (uint32_t y = 0; y < height; y++)
    for (uint32_t x = 0; x < width; x++)
      for (uint32_t c = 0; c < channels; c++)
        output(y, x, c) =
            input_1_as_output(y, x, c) * input_2_as_output(y, x, c);

  return output;
}

template <typename inputT, typename outputT>
vosa::ArrayBase::DType vosa::Mult<inputT, outputT>::output_dtype() const {
  return output_dtype_helper<outputT>();
}

template <typename inputT, typename outputT>
std::shared_ptr<vosa::ArrayBase> vosa::Mult<inputT, outputT>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<inputT> &input_1 = inputs[0]->template get_image<inputT>();
  const EigenImage<inputT> &input_2 = inputs[1]->template get_image<inputT>();
  auto typed_output = this->runme(input_1, input_2);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::MULT_MODE
vosa::Mult<uint8_t, uint8_t>::get_serialization_mode() const {
  return vosa_serialization::MULT_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::MULT_MODE
vosa::Mult<uint16_t, uint16_t>::get_serialization_mode() const {
  return vosa_serialization::MULT_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::MULT_MODE
vosa::Mult<uint32_t, uint32_t>::get_serialization_mode() const {
  return vosa_serialization::MULT_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::MULT_MODE
vosa::Mult<int8_t, int8_t>::get_serialization_mode() const {
  return vosa_serialization::MULT_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::MULT_MODE
vosa::Mult<int16_t, int16_t>::get_serialization_mode() const {
  return vosa_serialization::MULT_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::MULT_MODE
vosa::Mult<int32_t, int32_t>::get_serialization_mode() const {
  return vosa_serialization::MULT_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::MULT_MODE
vosa::Mult<float, float>::get_serialization_mode() const {
  return vosa_serialization::MULT_MODE_I_I_FLOAT32_FLOAT32;
}

template <>
vosa_serialization::MULT_MODE
vosa::Mult<double, double>::get_serialization_mode() const {
  return vosa_serialization::MULT_MODE_I_I_FLOAT64_FLOAT64;
}

template <typename inputT, typename outputT>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Mult<inputT, outputT>::serialize(
    const std::string &op_name,
    const std::vector<std::string> &input_names) const {

  vosa_serialization::MULTT mult;
  mult.mode = get_serialization_mode();

  auto mult_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  mult_operator->op.Set(mult);
  mult_operator->name = op_name;
  mult_operator->inputs = input_names;
  return mult_operator;
}

template class vosa::Mult<uint8_t, uint8_t>;

template class vosa::Mult<uint16_t, uint16_t>;

template class vosa::Mult<uint32_t, uint32_t>;

template class vosa::Mult<int8_t, int8_t>;

template class vosa::Mult<int16_t, int16_t>;

template class vosa::Mult<int32_t, int32_t>;

template class vosa::Mult<float, float>;

template class vosa::Mult<double, double>;