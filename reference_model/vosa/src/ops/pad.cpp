/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "pad.h"

#include "iostream"

std::shared_ptr<vosa::Op> vosa::PadBase::parse_serialized(
    const vosa_serialization::VosaOperatorUnion &vosa_operator_union) {
  auto serialized_pad = vosa_operator_union.AsPAD();
  assert(serialized_pad);
  auto mode = serialized_pad->mode;
  auto serialized_attributes = &serialized_pad->attr;
  std::vector<uint8_t> pad_constant =
      serialized_attributes->get()->pad_constant;
  std::vector<uint32_t> pad_size = serialized_attributes->get()->pad_size;
  auto serialized_pad_mode = serialized_attributes->get()->pad_method_mode;

  vosa::PadBase::pad_mode_t pad_mode;
  switch (serialized_pad_mode) {
  case vosa_serialization::PAD_METHOD_MODE_REPLICATE: {
    pad_mode = vosa::PadBase::VOSA_PAD_MODE_REPLICATE;
    break;
  }
  case vosa_serialization::PAD_METHOD_MODE_CONSTANT: {
    pad_mode = vosa::PadBase::VOSA_PAD_MODE_CONSTANT;
    break;
  }
  case vosa_serialization::PAD_METHOD_MODE_REFLECT: {
    pad_mode = vosa::PadBase::VOSA_PAD_MODE_REFLECT;
    break;
  }
  default: {
    throw std::logic_error("");
  }
  }

  switch (mode) {
  case vosa_serialization::PAD_MODE_I_I_UINT8_UINT8: {
    return std::make_shared<vosa::Pad<uint8_t>>(
        unpack_value<uint8_t>(pad_constant), pad_mode, pad_size);
  }
  case vosa_serialization::PAD_MODE_I_I_UINT16_UINT16: {
    return std::make_shared<vosa::Pad<uint16_t>>(
        unpack_value<uint16_t>(pad_constant), pad_mode, pad_size);
  }
  case vosa_serialization::PAD_MODE_I_I_UINT32_UINT32: {
    return std::make_shared<vosa::Pad<uint32_t>>(
        unpack_value<uint32_t>(pad_constant), pad_mode, pad_size);
  }
  case vosa_serialization::PAD_MODE_I_I_INT8_INT8: {
    return std::make_shared<vosa::Pad<int8_t>>(
        unpack_value<int8_t>(pad_constant), pad_mode, pad_size);
  }
  case vosa_serialization::PAD_MODE_I_I_INT16_INT16: {
    return std::make_shared<vosa::Pad<int16_t>>(
        unpack_value<int16_t>(pad_constant), pad_mode, pad_size);
  }
  case vosa_serialization::PAD_MODE_I_I_INT32_INT32: {
    return std::make_shared<vosa::Pad<int32_t>>(
        unpack_value<int32_t>(pad_constant), pad_mode, pad_size);
  }
  case vosa_serialization::PAD_MODE_I_I_FLOAT32_FLOAT32: {
    auto pad_constant_T = unpack_value<float>(pad_constant);
    return std::make_shared<vosa::Pad<float>>(pad_constant_T, pad_mode,
                                              pad_size);
  }
  default: {
    throw vosa::OperatorModeNotImplementedError();
  }
  }
}

template <typename T>
vosa::EigenImage<T> vosa::Pad<T>::runme(const EigenImage<T> &input) const {

  uint32_t input_height = input.dimension(0);
  uint32_t input_width = input.dimension(1);
  uint32_t num_channels = input.dimension(2);

  uint32_t output_height = input_height + pad_size[0] + pad_size[1];
  uint32_t output_width = input_width + pad_size[2] + pad_size[3];

  vosa::EigenImage<T> output(output_height, output_width, num_channels);

  for (uint32_t height_index = 0; height_index < output_height;
       height_index++) {
    for (uint32_t width_index = 0; width_index < output_width; width_index++) {
      for (uint32_t channel_index = 0; channel_index < num_channels;
           channel_index++) {

        bool height_below = height_index < pad_size[0];
        bool height_above = height_index >= input_height + pad_size[0];

        bool width_below = width_index < pad_size[2];
        bool width_above = width_index >= input_width + pad_size[2];

        bool outside_bulk =
            height_below || height_above || width_below || width_above;

        T input_value;
        if (outside_bulk) {
          if (mode == vosa::Pad<T>::VOSA_PAD_MODE_CONSTANT) {
            input_value = pad_constant;
          } else if (mode == vosa::Pad<T>::VOSA_PAD_MODE_REPLICATE) {

            if (height_below) {  // Above the Image
              if (width_below) { // Top left corner
                input_value = input(0, 0, channel_index);
              } else if (width_above) { // Top right corner
                input_value = input(0, input_width - 1, channel_index);
              } else { // Centre above
                input_value =
                    input(0, width_index - pad_size[2], channel_index);
              }
            } else if (height_above) { // Below the Image
              if (width_below) {       // Bottom left corner
                input_value = input(input_height - 1, 0, channel_index);
              } else if (width_above) { // Bottom right corner
                input_value =
                    input(input_height - 1, input_width - 1, channel_index);
              } else { // Centre below
                input_value = input(input_height - 1, width_index - pad_size[2],
                                    channel_index);
              }
            } else if (width_below) { // Centre left
              input_value = input(height_index - pad_size[0], 0, channel_index);
            } else { // Centre right
              input_value = input(height_index - pad_size[0], input_width - 1,
                                  channel_index);
            }

          } else if (mode == vosa::Pad<T>::VOSA_PAD_MODE_REFLECT) {

            if (height_below) {  // Above the Image
              if (width_below) { // Top left corner
                input_value = input(pad_size[0] - height_index,
                                    pad_size[2] - width_index, channel_index);
              } else if (width_above) { // Top right corner
                input_value =
                    input(pad_size[0] - height_index,
                          2 * input_width + pad_size[3] - width_index - 2,
                          channel_index);
              } else { // Centre above
                input_value = input(pad_size[0] - height_index,
                                    width_index - pad_size[2], channel_index);
              }
            } else if (height_above) { // Below the Image
              if (width_below) {       // Bottom left corner
                input_value =
                    input(2 * input_height + pad_size[1] - height_index - 2,
                          pad_size[2] - width_index, channel_index);
              } else if (width_above) { // Bottom right corner
                input_value =
                    input(2 * input_height + pad_size[1] - height_index - 2,
                          2 * input_width + pad_size[3] - width_index - 2,
                          channel_index);
              } else { // Centre below
                input_value =
                    input(2 * input_height + pad_size[1] - height_index - 2,
                          width_index - pad_size[2], channel_index);
              }
            } else if (width_below) { // Centre left
              input_value = input(height_index - pad_size[0],
                                  pad_size[2] - width_index, channel_index);
            } else { // Centre right
              input_value =
                  input(height_index - pad_size[0],
                        2 * input_width + pad_size[3] - width_index - 2,
                        channel_index);
            }

          } else if (mode == vosa::Pad<T>::VOSA_PAD_MODE_MIRROR) {
            throw std::runtime_error(
                "VOSA_PAD_MODE_MIRROR not implemented yet");
          } else {
            throw std::runtime_error("Unrecognized Padding Mode");
          }

        } else {
          uint32_t input_height_index = height_index - pad_size[0];
          uint32_t input_width_index = width_index - pad_size[2];
          input_value =
              input(input_height_index, input_width_index, channel_index);
        }
        output(height_index, width_index, channel_index) = input_value;
      }
    }
  }

  return output;
}

template <typename T>
vosa::ArrayBase::DType vosa::Pad<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::Pad<T>::eval(
    const std::vector<std::shared_ptr<ArrayBase>> &inputs) const {
  const EigenImage<T> &input = inputs[0]->template get_image<T>();
  auto typed_output = this->runme(input);
  auto output = std::make_shared<vosa::ArrayBase>(typed_output);
  return output;
}

template <>
vosa_serialization::PAD_MODE vosa::Pad<bool>::get_serialization_mode() const {
  return vosa_serialization::PAD_MODE_I_I_BOOL_BOOL;
}

template <>
vosa_serialization::PAD_MODE
vosa::Pad<uint8_t>::get_serialization_mode() const {
  return vosa_serialization::PAD_MODE_I_I_UINT8_UINT8;
}

template <>
vosa_serialization::PAD_MODE
vosa::Pad<uint16_t>::get_serialization_mode() const {
  return vosa_serialization::PAD_MODE_I_I_UINT16_UINT16;
}

template <>
vosa_serialization::PAD_MODE
vosa::Pad<uint32_t>::get_serialization_mode() const {
  return vosa_serialization::PAD_MODE_I_I_UINT32_UINT32;
}

template <>
vosa_serialization::PAD_MODE vosa::Pad<int8_t>::get_serialization_mode() const {
  return vosa_serialization::PAD_MODE_I_I_INT8_INT8;
}

template <>
vosa_serialization::PAD_MODE
vosa::Pad<int16_t>::get_serialization_mode() const {
  return vosa_serialization::PAD_MODE_I_I_INT16_INT16;
}

template <>
vosa_serialization::PAD_MODE
vosa::Pad<int32_t>::get_serialization_mode() const {
  return vosa_serialization::PAD_MODE_I_I_INT32_INT32;
}

template <>
vosa_serialization::PAD_MODE vosa::Pad<float>::get_serialization_mode() const {
  return vosa_serialization::PAD_MODE_I_I_FLOAT32_FLOAT32;
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Pad<T>::serialize(const std::string &op_name,
                        const std::vector<std::string> &input_names) const {

  auto pad_attribute = std::make_unique<vosa_serialization::PAD_ATTRIBUTET>();
  pad_attribute->pad_size = pad_size;
  switch (mode) {
  case PadBase::VOSA_PAD_MODE_CONSTANT: {
    pad_attribute->pad_method_mode =
        vosa_serialization::PAD_METHOD_MODE_CONSTANT;
    break;
  }
  case PadBase::VOSA_PAD_MODE_MIRROR: {
    pad_attribute->pad_method_mode = vosa_serialization::PAD_METHOD_MODE_MIRROR;
    break;
  }
  case PadBase::VOSA_PAD_MODE_REFLECT: {
    pad_attribute->pad_method_mode =
        vosa_serialization::PAD_METHOD_MODE_REFLECT;
    break;
  }
  case PadBase::VOSA_PAD_MODE_REPLICATE: {
    pad_attribute->pad_method_mode =
        vosa_serialization::PAD_METHOD_MODE_REPLICATE;
    break;
  }
  default:
    throw std::logic_error("");
  }

  std::vector<uint8_t> pad_constant_byte_vector;
  pad_constant_byte_vector.insert(pad_constant_byte_vector.end(), &pad_constant,
                                  &pad_constant + sizeof(T));
  pad_attribute->pad_constant = pad_constant_byte_vector;

  vosa_serialization::PADT pad;
  pad.mode = get_serialization_mode();
  pad.attr = std::move(pad_attribute);

  auto pad_operator =
      std::make_unique<vosa_serialization::VosaGraphOperatorT>();
  pad_operator->op.Set(std::move(pad));
  pad_operator->name = op_name;
  pad_operator->inputs = input_names;
  return pad_operator;
}

template class vosa::Pad<bool>;

template class vosa::Pad<uint8_t>;

template class vosa::Pad<uint16_t>;

template class vosa::Pad<uint32_t>;

template class vosa::Pad<int8_t>;

template class vosa::Pad<int16_t>;

template class vosa::Pad<int32_t>;

template class vosa::Pad<float>;