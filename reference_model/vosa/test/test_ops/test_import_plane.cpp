/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "data_utils.h"
#include "ops/import_channel.h"
#include "test_bench.h"
#include "vosa/vosa.h"

template <typename T>
void run_test() {
  T input_data[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};

  auto stride = 2 * sizeof(T);
  auto offset_1 = 0;
  auto offset_2 = sizeof(T);
  std::vector<uint32_t> shape = {3, 2};

  auto channel_1 =
      vosa::ImportChannel<T>(stride, offset_1, shape).runme(input_data);
  auto channel_2 =
      vosa::ImportChannel<T>(stride, offset_2, shape).runme(input_data);

  auto plane_1 = vosa::test::Packing<T>::extract_plane(channel_1, 0);
  auto plane_2 = vosa::test::Packing<T>::extract_plane(channel_2, 0);

  auto expected_plane_1 =
      vosa::utils::DataUtils<T>::planeFromCPP({{1, 3}, {5, 7}, {9, 11}});
  auto expected_plane_2 =
      vosa::utils::DataUtils<T>::planeFromCPP({{2, 4}, {6, 8}, {10, 12}});

  bool compare_1 = vosa::test::TestBench<T, int32_t>::compareEigenPlanes(
      plane_1, expected_plane_1);
  bool compare_2 = vosa::test::TestBench<T, int32_t>::compareEigenPlanes(
      plane_2, expected_plane_2);
  EXPECT_TRUE(compare_1);
  EXPECT_TRUE(compare_2);

  auto transpose = vosa::utils::DataUtils<T>::transpose_plane(plane_1);
  auto expected_transpose =
      vosa::utils::DataUtils<T>::planeFromCPP({{1, 5, 9}, {3, 7, 11}});
  bool compare_3 = vosa::test::TestBench<T, int32_t>::compareEigenPlanes(
      transpose, expected_transpose);
  EXPECT_TRUE(compare_3);
}

TEST(test_import_channel, test_import_channel_uint8) { run_test<uint8_t>(); }

TEST(test_import_channel, test_import_channel_uint16) { run_test<uint16_t>(); }