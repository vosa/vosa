/* -*- c++ -*-
 *
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VOSA_DIALECT_VOSA_UTILS_DATA_FORMAT_H
#define VOSA_DIALECT_VOSA_UTILS_DATA_FORMAT_H

namespace mlir {
namespace vosa {

/// \file
/// The `vosa-data-format` flag determines if VOSA uses HWC (default), or
/// CHW format for image tensors

/// convert HWC to output format
///
/// Reorders the dimensions for CHW output
inline ShapedType hwcToOutputType(ShapedType shapedTy) {
  if (getDataFormat() == DataFormat::CHW) {
    return RankedTensorType::get({shapedTy.getDimSize(2),
                                  shapedTy.getDimSize(0),
                                  shapedTy.getDimSize(1)},
                                 shapedTy.getElementType());
  } else {
    return shapedTy;
  }
}

/// convert HWC to output format
///
/// Reorders the dimensions for CHW output
inline llvm::SmallVector<int64_t>
hwcToDataFormatShape(llvm::ArrayRef<int64_t> shape) {
  if (getDataFormat() == DataFormat::CHW) {
    return llvm::SmallVector<int64_t>{shape[2], shape[0], shape[1]};
  } else {
    return llvm::SmallVector<int64_t>(shape.begin(), shape.end());
  }
}

/// Get the index of the height dimension
inline int64_t getHeightDimension() {
  if (getDataFormat() == DataFormat::CHW) {
    return 1;
  } else {
    return 0;
  }
}

/// Get the index of the width dimension
inline int64_t getWidthDimension() {
  if (getDataFormat() == DataFormat::CHW) {
    return 2;
  } else {
    return 1;
  }
}

/// Get the index of the channel dimension
inline int64_t getChannelDimension() {
  if (getDataFormat() == DataFormat::CHW) {
    return 0;
  } else {
    return 2;
  }
}

// use templates to allow ShapedType and ShapeAdaptor

/// Get the size of the height dimension
template <typename S>
int64_t getHeight(S s) {
  return s.getDimSize(getHeightDimension());
}

/// Get the size of the width dimension
template <typename S>
int64_t getWidth(S s) {
  return s.getDimSize(getWidthDimension());
}

/// Get the size of the channel dimension
template <typename S>
int64_t getChannels(S s) {
  return s.getDimSize(getChannelDimension());
}

} // namespace vosa
} // namespace mlir

#endif // VOSA_DIALECT_VOSA_UTILS_DATA_FORMAT_H
