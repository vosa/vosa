/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gtest/gtest.h"

#include "ops/reduce_interp_channelwise.h"
#include "test_bench.h"
#include "vosa/vosa.h"

using namespace std;
using namespace vosa;

TEST(test_reduce_interp_channelwise, positive_indices) {

  const uint32_t height = 2, width = 2;

  auto input = EigenImage<float>(height, width, 3);
  input.setValues({{{7.2f, 80.5f, -32.5f}, {305.2f, -756.0f, -0.1f}},
                   {{7.57f, 56.24f, 548.36f}, {127.025f, -46.56f, 65.65f}}});

  auto channel_values = EigenImage<float>(height, width, 1);
  channel_values.setValues({
      {{0.23f}, {2.5f}},
      {{1.0f}, {1.26f}},
  });

  auto expected = EigenImage<float>(height, width, 1);
  expected.setValues({
      {{24.059f}, {-0.1f}},
      {{56.24f}, {-17.3854f}},
  });

  auto Op = ReduceInterpChannelwise<float>();
  auto op_result = Op.eval(
      {make_shared<ArrayBase>(input), make_shared<ArrayBase>(channel_values)});
  auto actual = op_result->get_image<float>();

  bool result =
      test::TestBench<float, float>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}

TEST(test_reduce_interp_channelwise, negative_indices) {

  const uint32_t height = 2, width = 2;

  auto input = EigenImage<float>(height, width, 3);
  input.setValues({{{7.2f, 80.5f, -32.5f}, {305.2f, -756.0f, -0.1f}},
                   {{7.57f, 56.24f, 548.36f}, {127.025f, -46.56f, 65.65f}}});

  auto channel_values = EigenImage<float>(height, width, 1);
  channel_values.setValues({
      {{-0.23f}, {1.27f}},
      {{0.16f}, {-1.13f}},
  });

  auto expected = EigenImage<float>(height, width, 1);
  expected.setValues({
      {{7.2f}, {-551.907f}},
      {{15.3572f}, {127.025f}},
  });

  auto Op = ReduceInterpChannelwise<float>();
  auto op_result = Op.eval(
      {make_shared<ArrayBase>(input), make_shared<ArrayBase>(channel_values)});
  auto actual = op_result->get_image<float>();

  bool result =
      test::TestBench<float, float>::compareEigenImages(actual, expected);
  EXPECT_TRUE(result);
}
