/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "print.h"
#include <iostream>

template <typename T>
vosa::ArrayBase::DType vosa::Print<T>::output_dtype() const {
  return output_dtype_helper<T>();
}

template <typename T>
std::shared_ptr<vosa::ArrayBase> vosa::Print<T>::eval(
    const std::vector<std::shared_ptr<vosa::ArrayBase>> &inputs) const {
  const EigenImage<T> &input_0 = inputs[0]->template get_image<T>();

  uint32_t input_0_height = input_0.dimension(0);
  uint32_t input_0_width = input_0.dimension(1);
  uint32_t input_0_channels = input_0.dimension(2);

  auto output_image =
      EigenImage<T>(input_0_height, input_0_width, input_0_channels);

  std::cout << message << std::endl;

  for (uint32_t channel_index = 0; channel_index < input_0_channels;
       channel_index++) {
    for (uint32_t height_index = 0; height_index < input_0_height;
         height_index++) {
      for (uint32_t width_index = 0; width_index < input_0_width;
           width_index++) {
        T value = input_0(height_index, width_index, channel_index);
        std::cout << value << " ";
        output_image(height_index, width_index, channel_index) = value;
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }
  std::cout << std::endl << std::endl << std::endl;
  return std::make_shared<vosa::ArrayBase>(output_image);
}

template <typename T>
std::unique_ptr<vosa_serialization::VosaGraphOperatorT>
vosa::Print<T>::serialize(const std::string &,
                          const std::vector<std::string> &) const {
  throw std::runtime_error("Print cannot be serialized");
}

template class vosa::Print<bool>;

template class vosa::Print<uint8_t>;

template class vosa::Print<uint16_t>;

template class vosa::Print<uint32_t>;

template class vosa::Print<int8_t>;

template class vosa::Print<int16_t>;

template class vosa::Print<int32_t>;

template class vosa::Print<float>;