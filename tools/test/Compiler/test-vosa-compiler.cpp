/*
 * SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates
 * <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "mlir/Pass/PassManager.h"
#include "llvm/Support/CommandLine.h"

#include "mlir/Dialect/Tensor/IR/Tensor.h"
#include "mlir/Dialect/Tosa/IR/TosaOps.h"

#include "vosa/Builder/mlir_builder.h"
#include "vosa/Compiler/vosa_compiler.h"

mlir::OwningOpRef<mlir::ModuleOp> make_test_module(mlir::MLIRContext &context) {

  context.getOrLoadDialect<mlir::vosa::VosaDialect>();
  context.getOrLoadDialect<mlir::func::FuncDialect>();
  context.getOrLoadDialect<mlir::tensor::TensorDialect>();
  context.getOrLoadDialect<mlir::tosa::TosaDialect>();

  mlir::OwningOpRef<mlir::ModuleOp> customOpsModule;

  auto builder = vosa::MLIRBuilder(context, "", "pipeline", customOpsModule);

  llvm::SmallVector<int64_t> shape = {1920, 1080, 3};
  builder.Input("input1", vosa::dtype_t::VOSA_UINT8, shape);
  builder.Input("input2", vosa::dtype_t::VOSA_UINT8, shape);
  builder.Add("add1", vosa::dtype_t::VOSA_UINT8, {"input1", "input2"});
  builder.Add("add2", vosa::dtype_t::VOSA_UINT8, {"input1", "input2"});
  mlir::OwningOpRef<mlir::ModuleOp> module = builder.finalize({"add1"});

  return module;
}

int main(int argc, char *argv[]) {

  mlir::registerMLIRContextCLOptions();
  mlir::registerPassManagerCLOptions();
  llvm::cl::ParseCommandLineOptions(argc, argv, "");

  mlir::MLIRContext context;
  mlir::OwningOpRef<mlir::ModuleOp> module = make_test_module(context);
  vosa::Compiler::compile(context, module, "pipeline", "test_output.vmfb");

  return 0;
}
